//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//
// -----------------------------------------------------------------------------
//
//      THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
//      ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//      THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
//      PARTICULAR PURPOSE.
//  
// -----------------------------------------------------------------------------
//
//      WAVEUI : ACM user interface replacable functions
//
// -----------------------------------------------------------------------------
#include <windows.h>


// -----------------------------------------------------------------------------
//
//  waveui_Init() is called at boot when WAVEAPI is loaded. If the replacable
//  component needs any one time initialization, this is the place to do it.
//
// -----------------------------------------------------------------------------
EXTERN_C void
waveui_Init()
{
    // Default waveui does nothing here.
}


// -----------------------------------------------------------------------------
//
//  waveui_DeInit() is called at when WAVEAPI is unloaded. In normal operation,
//  this is never.
//
// -----------------------------------------------------------------------------
EXTERN_C void
waveui_DeInit()
{
    // Default waveui does nothing here.
}



// -----------------------------------------------------------------------------
//
//  waveui_BeforeDialogBox() is called right before DialogBoxParam() is called
//  to display the acmFormatChoose or acmFilterChoose dialog box. The first
//  parameter is a pointer to the chooser structure and the second parameter
//  indicates whether the structure is ACMFORMATCHOOSE (TRUE) or 
//  ACMFILTERCHOOSE (FALSE).
//
//  The replacable component could choose here, for instance, that the fdwStyle
//  element should not allow the ACMFORMATCHOOSE_STYLEF_SHOWHELP flag for this
//  project.
//
// -----------------------------------------------------------------------------
EXTERN_C void
waveui_BeforeDialogBox(
    LPVOID /*pChooseStruct*/,
    BOOL   /*fIsFormatDialog*/
    )
{
    // Default waveui does nothing here.
}



// -----------------------------------------------------------------------------
//
//  waveui_AfterDialogBox() is called after the DialogBoxParam() function
//  returns.
//
// -----------------------------------------------------------------------------
EXTERN_C void
waveui_AfterDialogBox(
    LPVOID /*pChooseStruct*/,
    BOOL   /*fIsFormatDialog*/
    )
{
    // Default waveui does nothing here.
}


// -----------------------------------------------------------------------------
//
//  waveui_AcmDlgProc is the first thing called in the Chooser Dialog Box 
//  window procedure. It follows the same format as an API replacable
//  window hook. If the function returns TRUE, then the default window proc 
//  is not used. 
//
//  This function should NOT return TRUE unless you REALLY mean it!!!!
//  The most likely case for returning TRUE here, is if you added a new control
//  to the dialog which sends private messages. In any case, be sure you don't 
//  break anything!
//
// -----------------------------------------------------------------------------
EXTERN_C BOOL
waveui_AcmDlgProc(
    HWND /*hwnd*/,
    UINT /*uMsg*/,
    WPARAM /*wParam*/,
    LPARAM /*lParam*/
    )
{
    // Default waveui does nothing here.
    return(FALSE);
}
