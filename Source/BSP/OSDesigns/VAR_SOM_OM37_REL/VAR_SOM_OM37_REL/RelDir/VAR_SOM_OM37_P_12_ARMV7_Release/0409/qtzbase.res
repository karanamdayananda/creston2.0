        ��  ��                  �@      �� ��     0 	              �  - - �  7 7 �  B B �  E F t  P P t  T T �  W X �  Z Z   ` ` �  c c X  g h �  p p �	  ~ ~ $
   �6�  8�;�P#  @�A�%  C�D��%  G�I��&  Q�S�(  U�V�h)  Y�Y��*  [�_�x+  a�b��.  d�f�H0  i�i��1  q�}�D2  ����t:  �����:  ����<?  �����?  ����(@  X  T h e   e n d   o f   t h e   l i s t   h a s   b e e n   r e a c h e d . % 0  
   �  A n   a t t e m p t   t o   a d d   a   f i l t e r   w i t h   a   d u p l i c a t e   n a m e   s u c c e e d e d   w i t h   a   m o d i f i e d   n a m e . % 0  
     \  T h e   s t a t e   t r a n s i t i o n   h a s   n o t   c o m p l e t e d . % 0  
   �  S o m e   o f   t h e   s t r e a m s   i n   t h i s   m o v i e   a r e   i n   a n   u n s u p p o r t e d   f o r m a t . % 0  
   �  T h e   f i l e   c o n t a i n e d   s o m e   p r o p e r t y   s e t t i n g s   t h a t   w e r e   n o t   u s e d . % 0  
   x  S o m e   c o n n e c t i o n s   h a v e   f a i l e d   a n d   h a v e   b e e n   d e f e r r e d . % 0  
     d  T h e   r e s o u r c e   s p e c i f i e d   i s   n o   l o n g e r   n e e d e d . % 0  
    A   c o n n e c t i o n   c o u l d   n o t   b e   m a d e   w i t h   t h e   m e d i a   t y p e   i n   t h e   p e r s i s t e n t   g r a p h , % 0  
 b u t   h a s   b e e n   m a d e   w i t h   a   n e g o t i a t e d   m e d i a   t y p e . % 0  
     �  C a n n o t   p l a y   b a c k   t h e   v i d e o   s t r e a m :   n o   s u i t a b l e   d e c o m p r e s s o r   c o u l d   b e   f o u n d . % 0  
   �  C a n n o t   p l a y   b a c k   t h e   a u d i o   s t r e a m :   n o   a u d i o   h a r d w a r e   i s   a v a i l a b l e . % 0  
     �  C a n n o t   p l a y   b a c k   t h e   v i d e o   s t r e a m :   f o r m a t   ' R P Z A '   i s   n o t   s u p p o r t e d . % 0  
     �  T h e   v a l u e   r e t u r n e d   h a d   t o   b e   e s t i m a t e d .     I t ' s   a c c u r a c y   c a n   n o t   b e   g u a r a n t e e d . % 0  
   �  T h i s   s u c c e s s   c o d e   i s   r e s e r v e d   f o r   i n t e r n a l   p u r p o s e s   w i t h i n   A c t i v e M o v i e . % 0  
   L  T h e   s t r e a m   h a s   b e e n   t u r n e d   o f f . % 0  
   �  T h e   g r a p h   c a n ' t   b e   c u e d   b e c a u s e   o f   l a c k   o f   o r   c o r r u p t   d a t a . % 0  
   `  T h e   s t o p   t i m e   f o r   t h e   s a m p l e   w a s   n o t   s e t . % 0  
   �  T h e r e   w a s   n o   p r e v i e w   p i n   a v a i l a b l e ,   s o   t h e   c a p t u r e   p i n   o u t p u t   i s   b e i n g   s p l i t   t o   p r o v i d e   b o t h   c a p t u r e   a n d   p r e v i e w . % 0  
   X  A n   i n v a l i d   m e d i a   t y p e   w a s   s p e c i f i e d . % 0  
     \  A n   i n v a l i d   m e d i a   s u b t y p e   w a s   s p e c i f i e d . % 0  
   �  T h i s   o b j e c t   c a n   o n l y   b e   c r e a t e d   a s   a n   a g g r e g a t e d   o b j e c t . % 0  
     T  T h e   e n u m e r a t o r   h a s   b e c o m e   i n v a l i d . % 0  
     �  A t   l e a s t   o n e   o f   t h e   p i n s   i n v o l v e d   i n   t h e   o p e r a t i o n   i s   a l r e a d y   c o n n e c t e d . % 0  
     �  T h i s   o p e r a t i o n   c a n n o t   b e   p e r f o r m e d   b e c a u s e   t h e   f i l t e r   i s   a c t i v e . % 0  
     t  O n e   o f   t h e   s p e c i f i e d   p i n s   s u p p o r t s   n o   m e d i a   t y p e s . % 0  
     p  T h e r e   i s   n o   c o m m o n   m e d i a   t y p e   b e t w e e n   t h e s e   p i n s . % 0  
   �  T w o   p i n s   o f   t h e   s a m e   d i r e c t i o n   c a n n o t   b e   c o n n e c t e d   t o g e t h e r . % 0  
     �  T h e   o p e r a t i o n   c a n n o t   b e   p e r f o r m e d   b e c a u s e   t h e   p i n s   a r e   n o t   c o n n e c t e d . % 0  
   `  N o   s a m p l e   b u f f e r   a l l o c a t o r   i s   a v a i l a b l e . % 0  
     D  A   r u n - t i m e   e r r o r   o c c u r r e d . % 0  
     H  N o   b u f f e r   s p a c e   h a s   b e e n   s e t . % 0  
   H  T h e   b u f f e r   i s   n o t   b i g   e n o u g h . % 0  
   T  A n   i n v a l i d   a l i g n m e n t   w a s   s p e c i f i e d . % 0  
   �  C a n n o t   c h a n g e   a l l o c a t e d   m e m o r y   w h i l e   t h e   f i l t e r   i s   a c t i v e . % 0  
     X  O n e   o r   m o r e   b u f f e r s   a r e   s t i l l   a c t i v e . % 0  
   �  C a n n o t   a l l o c a t e   a   s a m p l e   w h e n   t h e   a l l o c a t o r   i s   n o t   a c t i v e . % 0  
     x  C a n n o t   a l l o c a t e   m e m o r y   b e c a u s e   n o   s i z e   h a s   b e e n   s e t . % 0  
     �  C a n n o t   l o c k   f o r   s y n c h r o n i z a t i o n   b e c a u s e   n o   c l o c k   h a s   b e e n   d e f i n e d . % 0  
     �  Q u a l i t y   m e s s a g e s   c o u l d   n o t   b e   s e n t   b e c a u s e   n o   q u a l i t y   s i n k   h a s   b e e n   d e f i n e d . % 0  
     l  A   r e q u i r e d   i n t e r f a c e   h a s   n o t   b e e n   i m p l e m e n t e d . % 0  
     P  A n   o b j e c t   o r   n a m e   w a s   n o t   f o u n d . % 0  
     �  N o   c o m b i n a t i o n   o f   i n t e r m e d i a t e   f i l t e r s   c o u l d   b e   f o u n d   t o   m a k e   t h e   c o n n e c t i o n . % 0  
   �  N o   c o m b i n a t i o n   o f   f i l t e r s   c o u l d   b e   f o u n d   t o   r e n d e r   t h e   s t r e a m . % 0  
     X  C o u l d   n o t   c h a n g e   f o r m a t s   d y n a m i c a l l y . % 0  
   D  N o   c o l o r   k e y   h a s   b e e n   s e t . % 0  
     �  C u r r e n t   p i n   c o n n e c t i o n   i s   n o t   u s i n g   t h e   I O v e r l a y   t r a n s p o r t . % 0  
   �  C u r r e n t   p i n   c o n n e c t i o n   i s   n o t   u s i n g   t h e   I M e m I n p u t P i n   t r a n s p o r t . % 0  
   �  S e t t i n g   a   c o l o r   k e y   w o u l d   c o n f l i c t   w i t h   t h e   p a l e t t e   a l r e a d y   s e t . % 0  
     �  S e t t i n g   a   p a l e t t e   w o u l d   c o n f l i c t   w i t h   t h e   c o l o r   k e y   a l r e a d y   s e t . % 0  
     T  N o   m a t c h i n g   c o l o r   k e y   i s   a v a i l a b l e . % 0  
   @  N o   p a l e t t e   i s   a v a i l a b l e . % 0  
     L  D i s p l a y   d o e s   n o t   u s e   a   p a l e t t e . % 0  
   p  T o o   m a n y   c o l o r s   f o r   t h e   c u r r e n t   d i s p l a y   s e t t i n g s . % 0  
   |  T h e   s t a t e   c h a n g e d   w h i l e   w a i t i n g   t o   p r o c e s s   t h e   s a m p l e . % 0  
     �  T h e   o p e r a t i o n   c o u l d   n o t   b e   p e r f o r m e d   b e c a u s e   t h e   f i l t e r   i s   n o t   s t o p p e d . % 0  
   �  T h e   o p e r a t i o n   c o u l d   n o t   b e   p e r f o r m e d   b e c a u s e   t h e   f i l t e r   i s   n o t   p a u s e d . % 0  
     �  T h e   o p e r a t i o n   c o u l d   n o t   b e   p e r f o r m e d   b e c a u s e   t h e   f i l t e r   i s   n o t   r u n n i n g . % 0  
   �  T h e   o p e r a t i o n   c o u l d   n o t   b e   p e r f o r m e d   b e c a u s e   t h e   f i l t e r   i s   i n   t h e   w r o n g   s t a t e . % 0  
     t  T h e   s a m p l e   s t a r t   t i m e   i s   a f t e r   t h e   s a m p l e   e n d   t i m e . % 0  
   T  T h e   s u p p l i e d   r e c t a n g l e   i s   i n v a l i d . % 0  
     h  T h i s   p i n   c a n n o t   u s e   t h e   s u p p l i e d   m e d i a   t y p e . % 0  
     L  T h i s   s a m p l e   c a n n o t   b e   r e n d e r e d . % 0  
   �  T h i s   s a m p l e   c a n n o t   b e   r e n d e r e d   b e c a u s e   t h e   e n d   o f   t h e   s t r e a m   h a s   b e e n   r e a c h e d . % 0  
     �  A n   a t t e m p t   t o   a d d   a   f i l t e r   w i t h   a   d u p l i c a t e   n a m e   f a i l e d . % 0  
     <  A   t i m e - o u t   h a s   e x p i r e d . % 0  
   D  T h e   f i l e   f o r m a t   i s   i n v a l i d . % 0  
   X  T h e   l i s t   h a s   a l r e a d y   b e e n   e x h a u s t e d . % 0  
     H  T h e   f i l t e r   g r a p h   i s   c i r c u l a r . % 0  
   \  U p d a t e s   a r e   n o t   a l l o w e d   i n   t h i s   s t a t e . % 0  
     �  A n   a t t e m p t   w a s   m a d e   t o   q u e u e   a   c o m m a n d   f o r   a   t i m e   i n   t h e   p a s t . % 0  
     h  T h e   q u e u e d   c o m m a n d   h a s   a l r e a d y   b e e n   c a n c e l e d . % 0  
   h  C a n n o t   r e n d e r   t h e   f i l e   b e c a u s e   i t   i s   c o r r u p t . % 0  
   \  A n   o v e r l a y   a d v i s e   l i n k   a l r e a d y   e x i s t s . % 0  
     T  N o   f u l l - s c r e e n   m o d e s   a r e   a v a i l a b l e . % 0  
   �  T h i s   A d v i s e   c a n n o t   b e   c a n c e l e d   b e c a u s e   i t   w a s   n o t   s u c c e s s f u l l y   s e t . % 0  
   X  A   f u l l - s c r e e n   m o d e   i s   n o t   a v a i l a b l e . % 0  
     �  C a n n o t   c a l l   I V i d e o W i n d o w   m e t h o d s   w h i l e   i n   f u l l - s c r e e n   m o d e . % 0  
   l  T h e   m e d i a   t y p e   o f   t h i s   f i l e   i s   n o t   r e c o g n i z e d . % 0  
     x  T h e   s o u r c e   f i l t e r   f o r   t h i s   f i l e   c o u l d   n o t   b e   l o a d e d . % 0  
     P  A   f i l e   a p p e a r e d   t o   b e   i n c o m p l e t e . % 0  
   d  T h e   v e r s i o n   n u m b e r   o f   t h e   f i l e   i s   i n v a l i d . % 0  
     �  T h i s   f i l e   i s   c o r r u p t :   i t   c o n t a i n s   a n   i n v a l i d   c l a s s   i d e n t i f i e r . % 0  
     �  T h i s   f i l e   i s   c o r r u p t :   i t   c o n t a i n s   a n   i n v a l i d   m e d i a   t y p e . % 0  
     d  N o   t i m e   s t a m p   h a s   b e e n   s e t   f o r   t h i s   s a m p l e . % 0  
   p  N o   m e d i a   t i m e   s t a m p   h a s   b e e n   s e t   f o r   t h i s   s a m p l e . % 0  
   \  N o   m e d i a   t i m e   f o r m a t   h a s   b e e n   s e l e c t e d . % 0  
   �  C a n n o t   c h a n g e   b a l a n c e   b e c a u s e   a u d i o   d e v i c e   i s   m o n o   o n l y . % 0  
     �  C a n n o t   p l a y   b a c k   t h e   v i d e o   s t r e a m :   n o   s u i t a b l e   d e c o m p r e s s o r   c o u l d   b e   f o u n d . % 0  
   �  C a n n o t   p l a y   b a c k   t h e   a u d i o   s t r e a m :   n o   a u d i o   h a r d w a r e   i s   a v a i l a b l e ,   o r   t h e   h a r d w a r e   i s   n o t   r e s p o n d i n g . % 0  
   �  C a n n o t   p l a y   b a c k   t h e   v i d e o   s t r e a m :   f o r m a t   ' R P Z A '   i s   n o t   s u p p o r t e d . % 0  
     |  A c t i v e M o v i e   c a n n o t   p l a y   M P E G   m o v i e s   o n   t h i s   p r o c e s s o r . % 0  
     �  C a n n o t   p l a y   b a c k   t h e   a u d i o   s t r e a m :   t h e   a u d i o   f o r m a t   i s   n o t   s u p p o r t e d . % 0  
   �  C a n n o t   p l a y   b a c k   t h e   v i d e o   s t r e a m :   t h e   v i d e o   f o r m a t   i s   n o t   s u p p o r t e d . % 0  
   �  A c t i v e M o v i e   c a n n o t   p l a y   t h i s   v i d e o   s t r e a m   b e c a u s e   i t   f a l l s   o u t s i d e   t h e   c o n s t r a i n e d   s t a n d a r d . % 0  
     �  C a n n o t   p e r f o r m   t h e   r e q u e s t e d   f u n c t i o n   o n   a n   o b j e c t   t h a t   i s   n o t   i n   t h e   f i l t e r   g r a p h . % 0  
   �  C a n n o t   g e t   o r   s e t   t i m e   r e l a t e d   i n f o r m a t i o n   o n   a n   o b j e c t   t h a t   i s   u s i n g   a   t i m e   f o r m a t   o f   T I M E _ F O R M A T _ N O N E . % 0  
     �  T h e   c o n n e c t i o n   c a n n o t   b e   m a d e   b e c a u s e   t h e   s t r e a m   i s   r e a d   o n l y   a n d   t h e   f i l t e r   a l t e r s   t h e   d a t a . % 0  
   L  T h e   b u f f e r   i s   n o t   f u l l   e n o u g h . % 0  
     �  C a n n o t   p l a y   b a c k   t h e   f i l e .     T h e   f o r m a t   i s   n o t   s u p p o r t e d . % 0  
     �  P i n s   c a n n o t   c o n n e c t   d u e   t o   n o t   s u p p o r t i n g   t h e   s a m e   t r a n s p o r t . % 0  
   �  T h e   V i d e o   C D   c a n ' t   b e   r e a d   c o r r e c t l y   b y   t h e   d e v i c e   o r   i s   t h e   d a t a   i s   c o r r u p t . % 0  
   �  T h e r e   i s   n o t   e n o u g h   V i d e o   M e m o r y   a t   t h i s   d i s p l a y   r e s o l u t i o n   a n d   n u m b e r   o f   c o l o r s .   R e d u c i n g   r e s o l u t i o n   m i g h t   h e l p . % 0  
   �  T h e   V i d e o P o r t   c o n n e c t i o n   n e g o t i a t i o n   p r o c e s s   h a s   f a i l e d . % 0  
       E i t h e r   D i r e c t D r a w   h a s   n o t   b e e n   i n s t a l l e d   o r   t h e   V i d e o   C a r d   c a p a b i l i t i e s   a r e   n o t   s u i t a b l e .   M a k e   s u r e   t h e   d i s p l a y   i s   n o t   i n   1 6   c o l o r   m o d e . % 0  
     �  N o   V i d e o P o r t   h a r d w a r e   i s   a v a i l a b l e ,   o r   t h e   h a r d w a r e   i s   n o t   r e s p o n d i n g . % 0  
     �  N o   C a p t u r e   h a r d w a r e   i s   a v a i l a b l e ,   o r   t h e   h a r d w a r e   i s   n o t   r e s p o n d i n g . % 0  
     �  T h i s   U s e r   O p e r a t i o n   i s   i n h i b i t e d   b y   D V D   C o n t e n t   a t   t h i s   t i m e . % 0  
   |  T h i s   O p e r a t i o n   i s   n o t   p e r m i t t e d   i n   t h e   c u r r e n t   d o m a i n . % 0  
     T  R e q u e s t e d   B u t t o n   i s   n o t   a v a i l a b l e . % 0  
     p  D V D - V i d e o   p l a y b a c k   g r a p h   h a s   n o t   b e e n   b u i l t   y e t . % 0  
     `  D V D - V i d e o   p l a y b a c k   g r a p h   b u i l d i n g   f a i l e d . % 0  
   �  D V D - V i d e o   p l a y b a c k   g r a p h   c o u l d   n o t   b e   b u i l t   d u e   t o   i n s u f f i c i e n t   d e c o d e r s . % 0  
   �  V e r s i o n   n u m b e r   o f   d i r e c t   d r a w   n o t   s u i t a b l e .   M a k e   s u r e   t o   i n s t a l l   d x 5   o r   h i g h e r   v e r s i o n . % 0  
   �  C o p y   p r o t e c t i o n   c a n n o t   b e   e n a b l e d .   P l e a s e   m a k e   s u r e   a n y   o t h e r   c o p y   p r o t e c t e d   c o n t e n t   i s   n o t   b e i n g   s h o w n   n o w . % 0  
     t  F r a m e   s t e p   i s   n o t   s u p p o r t e d   o n   t h i s   c o n f i g u r a t i o n . % 0  
     �  T h e   V M R   h a s   n o t   y e t   c r e a t e d   a   m i x i n g   c o m p o n e n t .     T h a t   i s ,   I V M R F i l t e r C o n f i g : : S e t N u m b e r o f S t r e a m s   h a s   n o t   y e t   b e e n   c a l l e d . % 0  
   �  T h e   a p p l i c a t i o n   h a s   n o t   y e t   p r o v i d e d   t h e   V M R   f i l t e r   w i t h   a   v a l i d   a l l o c a t o r - p r e s e n t e r   o b j e c t . % 0  
     �  T h e   V M R   c o u l d   n o t   f i n d   a n y   d e - i n t e r l a c i n g   h a r d w a r e   o n   t h e   c u r r e n t   d i s p l a y   d e v i c e . % 0  
   �  T h e   V M R   c o u l d   n o t   f i n d   a n y   P r o c A m p   h a r d w a r e   o n   t h e   c u r r e n t   d i s p l a y   d e v i c e . % 0  
     x  V M R 9   d o e s   n o t   w o r k   w i t h   V P E - b a s e d   h a r d w a r e   d e c o d e r s . % 0  
     �  T h e   c u r r e n t   d i s p l a y   d e v i c e   d o e s   n o t   s u p p o r t   C o n t e n t   O u t p u t   P r o t e c t i o n   P r o t o c o l   ( C O P P )   H / W . % 0  
     H  A   r e g i s t r y   e n t r y   i s   c o r r u p t . % 0  
     �  T h e   s p e c i f i e d   p r o p e r t y   I D   i s   n o t   s u p p o r t e d   f o r   t h e   s p e c i f i e d   p r o p e r t y   s e t . % 0  
     h  T h e   S p e c i f i e d   p r o p e r t y   s e t   i s   n o t   s u p p o r t e d . % 0  
     