        ��  ��                  �      �� ��    0
         A p l i c a c i � n    C a r p e t a    % s   a r c h i v o    A r c h i v o    A c c e s o   d i r e c t o    C a r p e t a   d e   s i s t e m a    T i p o   d e s c o n o c i d o   R M e m o r i a   i n s u f i c i e n t e .   C i e r r e   a l g u n o s   p r o g r a m a s   e n   e j e c u c i � n   e   i n t � n t e l o   d e   n u e v o .    E r r o r   d e   m e m o r i a   i n s u f i c i e n t e    A c c e s o   d i r e c t o   a   % s                 �      �� ��    0
         A b r i r    G u a r d a r   c o m o    A r r i b a    L i s t a   	 D e t a l l e s   * E l   a r c h i v o   n o   e x i s t e .     � D e s e a   c r e a r   ' % s ' ?   c N o   s e   p u e d e   e n c o n t r a r   ' % s ' .     C o m p r u e b e   q u e   l a   r u t a   d e   a c c e s o   y   e l   n o m b r e   d e   a r c h i v o   s o n   c o r r e c t o s .   * ' % s ' 
 E s t e   n o m b r e   d e   a r c h i v o   n o   e s   v � l i d o .   ` N o   s e   t i e n e   a c c e s o   a   l a   c a r p e t a   ' % s ' . 
 C o m p r u e b e   q u e   l a   r u t a   d e   a c c e s o   e s p e c i f i c a d a   e s   c o r r e c t a .   G R u t a   d e   a c c e s o   d e m a s i a d o   l a r g a .   E l i j a   u n   n o m b r e   d e   a r c h i v o   m � s   c o r t o . 
   E ' % s '   E s t e   a r c h i v o   y a   e x i s t e .   � D e s e a   r e e m p l a z a r   e l   a r c h i v o   e x i s t e n t e ?             0      �� ��    0
         C � d i g o   d e   e r r o r   % d . 
 
 % s    C o p i a   
 % 1   -   % 3 % 2   D E l   a r c h i v o   ' % s '   e s   d e m a s i a d o   g r a n d e   p a r a   l a   P a p e l e r a   d e   r e c i c l a j e .      N u e v a   c a r p e t a    % s   ( % d )                 R M e m o r i a   i n s u f i c i e n t e .   C i e r r e   a l g u n o s   p r o g r a m a s   e n   e j e c u c i � n   e   i n t � n t e l o   d e   n u e v o .   2 
 
 L o s   a r c h i v o s   e n   m e m o r i a   R O M   n o   p u e d e n   c o p i a r s e .   � A c c e s o   d e n e g a d o . 
 
 C o m p r u e b e   q u e   e l   d i s c o   n o   e s t �   l l e n o   n i   p r o t e g i d o   c o n t r a   e s c r i t u r a   y   q u e   e l   a r c h i v o   n o   e s t �   a c t u a l m e n t e   e n   u s o .   �      �� ��    0
          � ' % s '   e s   u n a   c a r p e t a   d e   s i s t e m a   n e c e s a r i a   p a r a   q u e   W i n d o w s   s e   p u e d a   e j e c u t a r   c o r r e c t a m e n t e . 
 
 N o   s e   p u e d e   e l i m i n a r   n i   m o v e r   o   c a m b i a r   e l   n o m b r e .         M 
 
 L a   c a r p e t a   d e   d e s t i n o   y a   c o n t i e n e   u n   a r c h i v o   o   u n a   c a r p e t a   c o n   e s e   n o m b r e .                       �       �� ��    0
                R 
 
 E l   n o m b r e   d e   u n   a r c h i v o   n o   p u e d e   c o n t e n e r   l o s   s i g u i e n t e s   c a r a c t e r e s : 
 \ / :   * ? " < > |                         V      �� ��    0
        > 
 
 L a   c a r p e t a   d e   d e s t i n o   e s   l a   m i s m a   q u e   l a   c a r p e t a   d e   o r i g e n .   C 
 
 L a   c a r p e t a   d e   d e s t i n o   e s   u n a   s u b c a r p e t a   d e   l a   c a r p e t a   d e   o r i g e n .   x N o   s e   p u e d e   l e e r   d e l   a r c h i v o   o   d i s c o   d e   o r i g e n . 
 
 C o m p r u e b e   q u e   l a   r u t a   d e   a c c e s o   y   e l   n o m b r e   d e   a r c h i v o   s e a n   c o r r e c t o s .   f E s p a c i o   e n   d i s c o   i n s u f i c i e n t e . 
 
 E l i m i n e   u n o   o   m � s   a r c h i v o s   p a r a   l i b e r a r   e s p a c i o   e   i n t � n t e l o   d e   n u e v o .   ` 
 
 E l   n o m b r e   d e   a r c h i v o   e s p e c i f i c a d o   n o   e s   v � l i d o   o   e s   d e m a s i a d o   l a r g o .   E s p e c i f i q u e   o t r o   n o m b r e .   \ I n f r a c c i � n   d e   r e c u r s o   c o m p a r t i d o 
 
 E l   a r c h i v o   d e   o r i g e n   o   e l   d e   d e s t i n o   p u e d e n   e s t a r   e n   u s o .                               �� ��    0
                               N o   s e   p u e d e   c o p i a r   ' % s ' :      N o   s e   p u e d e   e l i m i n a r   ' % s ' :        N o   s e   p u e d e   m o v e r   ' % s ' :     ( N o   s e   p u e d e   c a m b i a r   e l   n o m b r e   d e   ' % s ' :       �       �� ��    0
        5 
 
 N o   s e   p u e d e n   e l i m i n a r   l o s   a r c h i v o s   e n   m e m o r i a   R O M .   2 
 
 N o   s e   p u e d e n   m o v e r   l o s   a r c h i v o s   e n   m e m o r i a   R O M .                                       �� ��	    0
                    t N o   s e   h a   p o d i d o   i n i c i a l i z a r   l a   P a p e l e r a   d e   r e c i c l a j e .   T o d a s   l a s   o p e r a c i o n e s   d e   e l i m i n a c i � n   n o   s e   g u a r d a r � n   e n   d i s c o                     *       �� ��Q    0
                               M e n �             