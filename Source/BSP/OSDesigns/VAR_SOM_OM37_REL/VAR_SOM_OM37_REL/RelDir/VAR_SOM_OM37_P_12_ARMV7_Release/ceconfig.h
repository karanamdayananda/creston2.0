#ifndef __CECONFIG_H__
#define __CECONFIG_H__
// CEFilter Component List:
#define CE_MODULES_COREDLL 1
#define CE_MODULES_KCOREDLL 1
#define CE_MODULES_NK 1
#define CE_MODULES_NKLOADER 1
#define CE_MODULES_OEM 1
#define CE_MODULES_OALIOCTL 1
#define CE_MODULES_FPCRT 1
#define CE_MODULES_IMJP31 1
#define CE_MODULES_IMJP31K 1
#define CE_MODULES_IMJP31_DICS_STD 1
#define CE_MODULES_MSKANA 1
#define CE_MODULES_LARGEKB 1
#define CE_MODULES_MSIM 1
#define CE_MODULES_SOFTKB 1
#define CE_MODULES_ERRHANDLEKB 1
#define CE_MODULES_COMPOSITOR 1
#define CE_MODULES_GDICOMPOSITOR 1
#define CE_MODULES_GLES2COMPOSITOR 1
#define CE_MODULES_REGEXTDEV 1
#define CE_MODULES_CONSOLE 1
#define CE_MODULES_CMD 1
#define CE_MODULES_COMMCTRL 1
#define CE_MODULES_COMMDLG 1
#define CE_MODULES_SHELL 1
#define CE_MODULES_LOADDBG 1
#define CE_MODULES_SHELLCELOG 1
#define CE_MODULES_CERDISP 1
#define CE_MODULES_DDI_CER 1
#define CE_MODULES_DLLHEAPINFOEXT 1
#define CE_MODULES_RELFSD 1
#define CE_MODULES_RELFSDEXT 1
#define CE_MODULES_LPCD 1
#define CE_MODULES_LPCRT 1
#define CE_MODULES_NET 1
#define CE_MODULES_SECUR32 1
#define CE_MODULES_NTLMSSP 1
#define CE_MODULES_NTLMSSP_SVC 1
#define CE_MODULES_SPNEGO 1
#define CE_MODULES_SCHANNEL 1
#define CE_MODULES_CREDMAN 1
#define CE_MODULES_PRNPORT 1
#define CE_MODULES_PRNERR 1
#define CE_MODULES_IRI 1
#define CE_MODULES_NOTIFY 1
#define CE_MODULES_RNAAPP 1
#define CE_MODULES_CONNMC 1
#define CE_MODULES_NDISPWR 1
#define CE_MODULES_NETUI 1
#define CE_MODULES_CMNET 1
#define CE_MODULES_CMSERVICE 1
#define CE_MODULES_CMCSPIP 1
#define CE_MODULES_CMCSPWWAN 1
#define CE_MODULES_CMCSPRAS 1
#define CE_MODULES_WSCMEXT 1
#define CE_MODULES_PROXYSVC 1
#define CE_MODULES_PROXYDBS 1
#define CE_MODULES_RMNET 1
#define CE_MODULES_RSPBASIC 1
#define CE_MODULES_RSPCELLULAR 1
#define CE_MODULES_ASYNCMAC 1
#define CE_MODULES_UNIMODEM 1
#define CE_MODULES_SERVICESFILTER 1
#define CE_MODULES_SERVICESSTART 1
#define CE_MODULES_SERVICESENUM 1
#define CE_MODULES_SERVICES 1
#define CE_MODULES_TCPIP 1
#define CE_MODULES_NETIO 1
#define CE_MODULES_NSI 1
#define CE_MODULES_NSIPROXY 1
#define CE_MODULES_WINNSI 1
#define CE_MODULES_NSISVC 1
#define CE_MODULES_TCPIPREG 1
#define CE_MODULES_DNSAPI 1
#define CE_MODULES_WINSOCK 1
#define CE_MODULES_AFD 1
#define CE_MODULES_NETIO 1
#define CE_MODULES_WS2 1
#define CE_MODULES_WS2INSTL 1
#define CE_MODULES_WSPM 1
#define CE_MODULES_NSPM 1
#define CE_MODULES_WS2K 1
#define CE_MODULES_WS2SERV 1
#define CE_MODULES_SSLLSP 1
#define CE_MODULES_RTL8139 1
#define CE_MODULES_E100BEX 1
#define CE_MODULES_NE2000 1
#define CE_MODULES_DHCP 1
#define CE_MODULES_DHCPV6 1
#define CE_MODULES_ETHMAN 1
#define CE_MODULES_NETMUI 1
#define CE_MODULES_NDISUIO 1
#define CE_MODULES_VEIM 1
#define CE_MODULES_DHCPSRV 1
#define CE_MODULES_NDIS 1
#define CE_MODULES_CXPORT 1
#define CE_MODULES_AUTORAS 1
#define CE_MODULES_PPP 1
#define CE_MODULES_TAPI 1
#define CE_MODULES_CRYPT32 1
#define CE_MODULES_NCRYPT 1
#define CE_MODULES_KEYISO 1
#define CE_MODULES_MSASN1 1
#define CE_MODULES_ALPCD 1
#define CE_MODULES_RPCRT4 1
#define CE_MODULES_REDIR 1
#define CE_MODULES_NETBIOS 1
#define CE_MODULES_PING 1
#define CE_MODULES_IPCONFIG 1
#define CE_MODULES_IPCONFIG6 1
#define CE_MODULES_TRACERT 1
#define CE_MODULES_ROUTE 1
#define CE_MODULES_NETSTAT 1
#define CE_MODULES_NDISCONFIG 1
#define CE_MODULES_IPV6 1
#define CE_MODULES_IPHLPAPI 1
#define CE_MODULES_KBDUS 1
#define CE_MODULES_KBDJPN 1
#define CE_MODULES_OTLS 1
#define CE_MODULES_SHAPING 1
#define CE_MODULES_XAMLRUNTIME 1
#define CE_MODULES_XAMLRUNTIMECORE 1
#define CE_MODULES_XRRENDERERDDRAW 1
#define CE_MODULES_XRRENDEREROPENGL 1
#define CE_MODULES_BLUREFFECT 1
#define CE_MODULES_BLUREFFECTSHADERS 1
#define CE_MODULES_DROPSHADOWEFFECT 1
#define CE_MODULES_DROPSHADOWEFFECTSHADERS 1
#define CE_MODULES_RIPPLEEFFECT 1
#define CE_MODULES_RIPPLEEFFECTSHADERS 1
#define CE_MODULES_SHADERS 1
#define CE_MODULES_GWES 1
#define CE_MODULES_KEYBD 1
#define CE_MODULES_POINTER 1
#define CE_MODULES_TOUCH 1
#define CE_MODULES_MGTT_O 1
#define CE_MODULES_DISPLAY 1
#define CE_MODULES_FONTS 1
#define CE_MODULES_GWEUSER 1
#define CE_MODULES_DDGUID 1
#define CE_MODULES_PHYSICSENGINE 1
#define CE_MODULES_DMSRV 1
#define CE_MODULES_GSM610 1
#define CE_MODULES_WAVEAPI 1
#define CE_MODULES_AUDIODRV 1
#define CE_MODULES_WAVESAMPLES 1
#define CE_MODULES_WAVEAPIC 1
#define CE_MODULES_UIPROXY 1
#define CE_MODULES_BATTDRVR 1
#define CE_MODULES_NLEDDRVR 1
#define CE_MODULES_TIMEZONES 1
#define CE_MODULES_DEVICE 1
#define CE_MODULES_REGENUM 1
#define CE_MODULES_BUSENUM 1
#define CE_MODULES_PM 1
#define CE_MODULES_GIISR 1
#define CE_MODULES_MMTIMER 1
#define CE_MODULES_PCI 1
#define CE_MODULES_PCMCONV 1
#define CE_MODULES_SERIAL 1
#define CE_MODULES_8042KEYBOARD 1
#define CE_MODULES_NOPKEYBOARD 1
#define CE_MODULES_SDBUS 1
#define CE_MODULES_SDMEMORY 1
#define CE_MODULES_USBHOST 1
#define CE_MODULES_USBD 1
#define CE_MODULES_USBOTG 1
#define CE_MODULES_USBHID 1
#define CE_MODULES_HIDPARSE 1
#define CE_MODULES_KBDHID 1
#define CE_MODULES_CONSHID 1
#define CE_MODULES_MOUHID 1
#define CE_MODULES_USBFN 1
#define CE_MODULES_SERIALUSBFN 1
#define CE_MODULES_CEDDK 1
#define CE_MODULES_EXFAT 1
#define CE_MODULES_FATUTIL 1
#define CE_MODULES_DISKCACHE 1
#define CE_MODULES_CEREG 1
#define CE_MODULES_MSPART 1
#define CE_MODULES_ZLIB 1
#define CE_MODULES_XMLLITE 1
#define CE_MODULES_CREATEMUI 1
#define CE_MODULES_FILESYS 1
#define CE_MODULES_FSDMGR 1
#define CE_MODULES_ROMFSD 1
#define CE_MODULES_RSAENH 1
#define CE_MODULES_BCRYPT 1
#define CE_MODULES_CCFGSVC 1
#define CE_MODULES_NLS 1
#define CE_MODULES_NLSLOCALE 1
#define CE_MODULES_NLSSORTING 1
#define CE_MODULES_NLSNORMALIZE 1
#define CE_MODULES_TOOLHELP 1
#define CE_MODULES_VMINI 1
#define CE_MODULES_LASSD 1
#define CE_MODULES_LAP_PW 1
#define CE_MODULES_RT_TESTS 1
#define CE_MODULES_CEPERF_MODULE 1
#define DCOM_MODULES_ATL 1
#define DCOM_MODULES_DLLHOST 1
#define DCOM_MODULES_DCOMSSD 1
#define DCOM_MODULES_OLE32 1
#define DCOM_MODULES_OLEAUT32 1
#define DCOM_MODULES_RPCRT4LEGACY 1
#define DCOM_MODULES_KOLE32 1
#define DCOM_MODULES_KOLEAUT32 1
#define DCOM_MODULES_UUID 1
#define GDIEX_MODULES_IMAGING 1
#define IE7_MODULES_IEFRAME 1
#define IE7_MODULES_MSXML3 1
#define IE7_MODULES_URLMON 1
#define IE7_MODULES_WININET 1
#define IE7_MODULES_WININETUI 1
#define IE7_MODULES_SHLWAPI 1
#define IE7_MODULES_MLANG 1
#define IE7_MODULES_UUID 1
#define SERVERS_MODULES_AV_UPNP 1
#define SERVERS_MODULES_AV_DLL 1
#define SERVERS_MODULES_UPNPSVC 1
#define SERVERS_MODULES_UPNPCAPI 1
#define SERVERS_MODULES_UPNPCTRL 1
#define SERVERS_MODULES_UPNPHOST 1
#define SERVERS_MODULES_UPNPLOADER 1
#define SERVERS_MODULES_SMBSERVER 1
#define SERVERS_MODULES_SMBCONFIG 1
#define SERVERS_MODULES_MSMQD 1
#define SERVERS_MODULES_MSMQRT 1
#define SERVERS_MODULES_MSMQADM 1
#define SERVERS_MODULES_TELNETD 1
#define SERVERS_MODULES_FTPD 1
#define SERVERS_MODULES_ASP 1
#define SERVERS_MODULES_MSMQADMEXT 1
#define SERVERS_MODULES_HTTPD 1
#define SERVERS_MODULES_DSTSVC 1
#define SERVERS_MODULES_RUI_COREUI 1
#define SERVERS_MODULES_RUI_HOSTNAME 1
#define SERVERS_MODULES_RUI_NETWORK 1
#define SERVERS_MODULES_RUI_PASSWORD 1
#define SERVERS_MODULES_RUI_VERSION 1
#define SERVERS_MODULES_RUI_ROOTDIRS 1
#define SERVERS_MODULES_RUI_SMBFILE 1
#define SERVERS_MODULES_RUI_SMBPRINT 1
#define SERVERS_MODULES_RUI_USERMANAGER 1
#define SERVERS_MODULES_RUI_RAS 1
#define SHELLSDK_MODULES_AYGSHELL 1
#define CELLCORE_MODULES_TOOLBOX 1
#define CELLCORE_MODULES_CELLCORE 1
#define CELLCORE_MODULES_CCOREUTL 1
#define CELLCORE_MODULES_RIL 1
#define CELLCORE_MODULES_SIM 1
#define CELLCORE_MODULES_SMS 1
#define CELLCORE_MODULES_CCORERES 1
#define NETCFV35_MODULES_DOTNETV35 1
#define SQLCOMPACT_MODULES_SQLCOMPACT 1
#define SQLCOMPACT_MODULES_EDB 1
#define OSSVCS_MODULES_NETWORKPOLICYCSP 1
#define OSSVCS_MODULES_OSSVCS 1
#define OSSVCS_MODULES_METABASESERVICE 1
#define OSSVCS_MODULES_CONFIGMANAGER 1
#define OSSVCS_MODULES_METABASEPROXY 1
#define OSSVCS_MODULES_CONFIGMETABASE2 1
#define OSSVCS_MODULES_CONFIGMANAGER2 1
#define OSSVCS_MODULES_METABASEPROXY 1
#define OSSVCS_MODULES_CONFIGMETABASE2 1
#define OSSVCS_MODULES_SYSCSPS 1
#define OSSVCS_MODULES_SYSCSPSRES 1
#define OSSVCS_MODULES_OMADMSVC 1
#define OSSVCS_MODULES_DMSAPI 1
#define SHELL_MODULES_EXPLORER 1
#define SHELL_MODULES_SHCORE 1
#define SHELL_MODULES_CESHELL 1
#define SHELL_MODULES_VGAL 1
#define WCESHELLFE_MODULES_CONTROL 1
#define WCESHELLFE_MODULES_CTLPNL 1
#define WCESHELLFE_MODULES_CPLMAIN 1
#define WCESHELLFE_MODULES_ADVBACKLIGHT 1
#define WCESHELLFE_MODULES_CONNPNL 1
#define WCESHELLFE_MODULES_INTLL 1
#define WCESHELLFE_MODULES_STGUIL 1
#define WCESHELLFE_MODULES_STDWAVEFILES 1
#define WCEAPPSFE_MODULES_PWORD 1
#define WCEAPPSFE_MODULES_PWD_RES 1
#define WCEAPPSFE_MODULES_PWWIFF 1
#define WCEAPPSFE_MODULES_OFFICE 1
#define WCEAPPSFE_MODULES_RICHED20 1
#define WCEAPPSFE_MODULES_MSLS4 1
#define WCEAPPSFE_MODULES_RICHED60 1
#define DIRECTX_MODULES_WMVDMOD 1
#define DIRECTX_MODULES_WMADMOD 1
#define DIRECTX_MODULES_WMADMOD 1
#define DIRECTX_MODULES_STRMBASE 1
#define DIRECTX_MODULES_QUARTZ 1
#define DIRECTX_MODULES_MSRLE32 1
#define DIRECTX_MODULES_ICM 1
#define DIRECTX_MODULES_MSDMO 1
#define DIRECTX_MODULES_WMVDMOE 1
#define DIRECTX_MODULES_CEDRM2 1
#define DIRECTX_MODULES_DIRECTDRAW 1
#define DIRECTX_MODULES_DDSAMPLES 1
#define DIRECTX_MODULES_MSADPCM 1
#define DIRECTX_MODULES_IMAADPCM 1
#define DIRECTX_MODULES_MSG711 1
#define DATASYNC_MODULES_REPLLOG 1
#define DATASYNC_MODULES_ASRES 1
#define DATASYNC_MODULES_RAPICLNT 1
#define DATASYNC_MODULES_RRA_STM 1
#define DATASYNC_MODULES_UDP2TCP 1
#define DATASYNC_MODULES_ASUTIL 1
#define DATASYNC_MODULES_WCELOAD 1
#define DATASYNC_MODULES_UNLOAD 1
#define DATASYNC_MODULES_SERVERSYNC 1
#define MEDIAAPPS_MODULES_CEMPENG 1
#define MEDIAAPPS_MODULES_MMPLAYER 1
#define MEDIAAPPS_MODULES_MMPLAYERDEFAULTSKIN 1
#define MEDIAAPPS_MODULES_SMPLAYER 1
#define MEDIAAPPS_MODULES_SMPLAYERDEFAULTSKIN 1
#define MEDIAAPPS_MODULES_MVPLAYER 1
#define MEDIAAPPS_MODULES_MVPLAYERDEFAULTSKIN 1
#define MEDIAAPPS_MODULES_SVPLAYER 1
#define MEDIAAPPS_MODULES_SVPLAYERDEFAULTSKIN 1
#define MEDIAAPPS_MODULES_MPVIEWER 1
#define MEDIAAPPS_MODULES_MPVIEWERDEFAULTSKIN 1
#define MEDIAAPPS_MODULES_SPVIEWER 1
#define MEDIAAPPS_MODULES_SPVIEWERDEFAULTSKIN 1
#define MEDIAAPPS_MODULES_MLIBDLL 1
#define MEDIAAPPS_MODULES_ML 1
#define MEDIAAPPS_MODULES_MLWD 1
#define MEDIAAPPS_MODULES_MLPARSER 1
#define MEDIAAPPS_MODULES_THUMBNAILSTORE 1
#define MEDIAAPPS_MODULES_WINDOWSCODECS 1
#define MEDIAAPPS_MODULES_WINDOWSCODECSEXT 1
#define MEDIAAPPS_MODULES_MEDIARENDERER 1
#define MEDIAAPPS_MODULES_WINDOWSCODECS 1
#define MEDIAAPPS_MODULES_WINDOWSCODECSEXT 1
#define APPS_MODULES_SYNCUTIL 1
#define APPS_MODULES_RETAILLOG 1
#define APPS_MODULES_ACTSYNCC 1
#define APPS_MODULES_SYNCCSP 1
#define APPS_MODULES_SYNCRES 1
#define APPS_MODULES_SYNCMGR 1
#define APPS_MODULES_MAILCSP2 1
#define APPS_MODULES_SHPTUTIL 1
#define APPS_MODULES_TSKSCHCSP 1
#define APPS_MODULES_TASKSCHEDULERV1 1
#define APPS_MODULES_TSKSCHEDULE 1
#define APPS_MODULES_CEMAPI 1
#define APPS_MODULES_CESHUTIL 1
#define APPS_MODULES_CEAYGSHELL 1
#define APPS_MODULES_SERVERSYNC 1
#define APPS_MODULES_SYNCLAP 1
#define APPS_MODULES_ARINVALID 1
#define COREDLL_COREMAIN 1
#define COREDLL_THUNKS 1
#define COREDLL_COREPOLICYSTUB 1
#define COREDLL_SHOWERR 1
#define COREDLL_CORECRT 1
#define COREDLL_CORESTRW 1
#define COREDLL_CORESIP 1
#define COREDLL_REGEXT 1
#define COREDLL_COREDLL_HANDLEARRAY 1
#define COREDLL_CCPSELECT 1
#define COREDLL_TNOTIFY 1
#define COREDLL_TAPILIB 1
#define COREDLL_RECTAPI 1
#define COREDLL_WMGR_C 1
#define COREDLL_MGDI_C 1
#define COREDLL_ACCEL_C 1
#define COREDLL_MESSAGEDIALOGBOXTHUNK 1
#define COREDLL_SHCORE 1
#define COREDLL_IDLIST 1
#define COREDLL_PATH 1
#define COREDLL_SHORTCUT 1
#define COREDLL_SHEXEC 1
#define COREDLL_SHMISC 1
#define COREDLL_FILEOPEN 1
#define COREDLL_FILEINFO 1
#define COREDLL_SHELLAPIS 1
#define COREDLL_AYGSTUBS 1
#define COREDLL_MRUSTUB 1
#define COREDLL_DSA 1
#define COREDLL_MMACM 1
#define COREDLL_MMACMUI 1
#define COREDLL_MMWAVE 1
#define COREDLL_MMSND 1
#define COREDLL_MMMIX 1
#define COREDLL_WAVEAPICFWD 1
#define COREDLL_BATTERY 1
#define COREDLL_NLED 1
#define COREDLL_ASYNCIO 1
#define COREDLL_DEVENUM 1
#define COREDLL_DEVLOAD 1
#define COREDLL_COREBUILDINFO 1
#define COREDLL_COREVERSIONSTD 1
#define COREDLL_COREIMM 1
#define COREDLL_INPUTSCOPES 1
#define COREDLL_CORESIOA 1
#define COREDLL_CORESTRA 1
#define COREDLL_MULTIUI 1
#define COREDLL_CORELOC 1
#define COREDLL_CORESIOW 1
#define COREDLL_CRYPTAPI 1
#define COREDLL_FMTMSG 1
#define COREDLL_SERDEV 1
#define COREDLL_CRT_CPP_EH_AND_RTTI 1
#define COREDLL_DELAYEDBOOTWORKNOAPI 1
#define COREDLL_FULL_CRT 1
#define COREDLL_CRYPTHASH 1
#define COREDLL_RSA32_COREDLL 1
#define KCOREDLL_KCOREMAIN 1
#define KCOREDLL_KTHUNKS 1
#define KCOREDLL_COREPOLICYSTUB 1
#define KCOREDLL_SHOWERR 1
#define KCOREDLL_CORECRT 1
#define KCOREDLL_CORESTRW 1
#define KCOREDLL_CORESIP 1
#define KCOREDLL_REGEXT 1
#define KCOREDLL_COREDLL_HANDLEARRAY 1
#define KCOREDLL_CCPSELECT 1
#define KCOREDLL_TNOTIFY 1
#define KCOREDLL_TAPILIB 1
#define KCOREDLL_RECTAPI 1
#define KCOREDLL_WMGR_C 1
#define KCOREDLL_MGDI_C 1
#define KCOREDLL_ACCEL_C 1
#define KCOREDLL_MESSAGEDIALOGBOXTHUNK 1
#define KCOREDLL_SHCORE 1
#define KCOREDLL_IDLIST 1
#define KCOREDLL_PATH 1
#define KCOREDLL_SHORTCUT 1
#define KCOREDLL_SHEXEC 1
#define KCOREDLL_SHMISC 1
#define KCOREDLL_FILEOPEN 1
#define KCOREDLL_FILEINFO 1
#define KCOREDLL_SHELLAPIS 1
#define KCOREDLL_AYGSTUBS 1
#define KCOREDLL_MRUSTUB 1
#define KCOREDLL_DSA 1
#define KCOREDLL_MMACM 1
#define KCOREDLL_MMACMUI 1
#define KCOREDLL_MMWAVE 1
#define KCOREDLL_MMSND 1
#define KCOREDLL_MMMIX 1
#define KCOREDLL_WAVEAPICFWD 1
#define KCOREDLL_BATTERY 1
#define KCOREDLL_NLED 1
#define KCOREDLL_ASYNCIO 1
#define KCOREDLL_DEVENUM 1
#define KCOREDLL_DEVLOAD 1
#define KCOREDLL_COREBUILDINFO 1
#define KCOREDLL_COREVERSIONSTD 1
#define KCOREDLL_COREIMM 1
#define KCOREDLL_INPUTSCOPES 1
#define KCOREDLL_CORESIOA 1
#define KCOREDLL_CORESTRA 1
#define KCOREDLL_MULTIUI 1
#define KCOREDLL_CORELOC 1
#define KCOREDLL_CORESIOW 1
#define KCOREDLL_CRYPTAPI 1
#define KCOREDLL_FMTMSG 1
#define KCOREDLL_SERDEV 1
#define KCOREDLL_CRT_CPP_EH_AND_RTTI 1
#define KCOREDLL_DELAYEDBOOTWORKNOAPI 1
#define KCOREDLL_FULL_CRT 1
#define KCOREDLL_CRYPTHASH 1
#define KCOREDLL_RSA32_COREDLL 1
#define NK_NKCOMPR 1
#define NK_NKTZINIT 1
#define NK_NKMAPFILE 1
#define NK_NKMSGQ 1
#define NK_OEMSTUB 1
#define NK_NKLOGGER 1
#define NKLOADER_NKLDR 1
#define OEM_NKSTUB 1
#define OEM_OEMMAIN 1
#define OEM_OEMMAIN_STATICKITL 1
#define COMMCTRL_TOOLBAR 1
#define COMMCTRL_UPDOWN 1
#define COMMCTRL_STATUS 1
#define COMMCTRL_PROPSHEET 1
#define COMMCTRL_LISTVIEW 1
#define COMMCTRL_TREEVIEW 1
#define COMMCTRL_DATE 1
#define COMMCTRL_TAB 1
#define COMMCTRL_PROGRESS 1
#define COMMCTRL_TRACKBAR 1
#define COMMCTRL_CAPEDIT 1
#define COMMCTRL_REBAR 1
#define COMMCTRL_CMDBAR 1
#define COMMCTRL_DSA 1
#define COMMCTRL_TOOLTIPS 1
#define COMMCTRL_FE 1
#define COMMCTRL_ANIMATE 1
#define COMMCTRL_LINK 1
#define COMMCTRL_SHAPIS 1
#define NOTIFY_NOTIFPUB 1
#define NOTIFY_NOTIFUI 1
#define WINSOCK_SSLSOCK 1
#define PPP_PPP2SRVSTUB 1
#define GWES_GWE1 1
#define GWES_WMBASE 1
#define GWES_GWESHARE 1
#define GWES_GWESMAIN 1
#define GWES_IMMTHUNK 1
#define GWES_MSGQUE 1
#define GWES_GSETWINLONG 1
#define GWES_CEPTR 1
#define GWES_GWEPERF 1
#define GWES_FOREGND 1
#define GWES_IDLE 1
#define GWES_KBDUI 1
#define GWES_UIBASE 1
#define GWES_MSGBEEP 1
#define GWES_AUDIO 1
#define GWES_GWE2 1
#define GWES_MGBASE 1
#define GWES_MGBITMAP 1
#define GWES_MGBLT 1
#define GWES_MGBLT2 1
#define GWES_MGDC 1
#define GWES_MGDIBSEC 1
#define GWES_MGDRAW 1
#define GWES_MGRGN 1
#define GWES_MGWINMGR 1
#define GWES_TCHUI 1
#define GWES_MGGRADFILL 1
#define GWES_MGALPHABLEND 1
#define GWES_MGTT 1
#define GWES_MGFE 1
#define GWES_MGDRWTXT 1
#define GWES_MGPRINT 1
#define GWES_MGPAL 1
#define GWES_MGPALNAT 1
#define GWES_GWE3 1
#define GWES_ACCEL 1
#define GWES_BTNCTL 1
#define GWES_CARET 1
#define GWES_CASCADE 1
#define GWES_IMECTL 1
#define GWES_CLIPBD 1
#define GWES_CMBCTL 1
#define GWES_DEFWNDPROC 1
#define GWES_DLGMGR 1
#define GWES_DLGMNEM 1
#define GWES_EDCTL 1
#define GWES_GCACHE 1
#define GWES_GWECTRL 1
#define GWES_ICON 1
#define GWES_ICONCMN 1
#define GWES_IMGCTL 1
#define GWES_LBCTL 1
#define GWES_LOADBMP 1
#define GWES_LOADIMG 1
#define GWES_MENU 1
#define GWES_MENUSCRL 1
#define GWES_MNOOVER 1
#define GWES_MENUGDI 1
#define GWES_MENURC_STUBS 1
#define GWES_MOUSEANDTOUCHCURSOR 1
#define GWES_ICONCURS 1
#define GWES_MCURSOR 1
#define GWES_MCURSOR8 1
#define GWES_CURSOR 1
#define GWES_CURSOR8 1
#define GWES_MNOTAPUI 1
#define GWES_GWE4 1
#define GWES_NCLIENT 1
#define GWES_SBCMN 1
#define GWES_SCBCTL 1
#define GWES_STARTUP 1
#define GWES_STCCTL 1
#define GWES_WINMGR 1
#define GWES_OOM 1
#define GWES_SBCMNVIEW 1
#define GWES_NCLIENTVIEW 1
#define GWES_GCACHEVIEW 1
#define GWES_BTNCTLVIEW 1
#define GWES_STCCTLVIEW 1
#define GWES_CMBCTLVIEW 1
#define GWES_EDCTLVIEW 1
#define GWES_LBCTLVIEW 1
#define GWES_MENUVIEW 1
#define GWES_EDIMEFE 1
#define GWES_BTNGDI 1
#define GWES_BTNRC_STUBS 1
#define GWES_SBUIGDI 1
#define GWES_SBUIRC_STUBS 1
#define GWES_MSGBOX 1
#define GWES_MSGBOX_HPC 1
#define GWES_DLGMGR_HPC 1
#define GWES_MENU_HPC 1
#define GWES_CMBCTL_HPC 1
#define GWES_EDITCONTROLOS 1
#define GWES_GWESCOMPOSITION_CORE 1
#define GWES_TIMER 1
#define GWES_COLUMN 1
#define GWES_ATOM 1
#define GWES_DRAWMBAR 1
#define GWES_HOTKEY 1
#define GWES_SYSCOLOR 1
#define GWES_MGALIAS 1
#define GWES_JOURNAL 1
#define GWES_DDCORE 1
#define GWES_MGTCI 1
#define MGTT_O_DECOMPDRV 1
#define FONTS_SEGOEUI 1
#define FONTS_TAHOMA 1
#define FONTS_COUR_1_30 1
#define FONTS_TIMES 1
#define FONTS_SYMBOL 1
#define GWEUSER_GWEUSERMAIN 1
#define GWEUSER_GWEUSERSTARTUI 1
#define GWEUSER_GWEUSEROOMUI 1
#define GWEUSER_GWEUSERCALIBRATEUI 1
#define WAVEAPI_WAPIWAVE 1
#define WAVEAPI_AUDEVMAN 1
#define WAVEAPI_SWMIXER 1
#define WAVEAPIC_WAVEAPIC_ACM 1
#define WAVEAPIC_WAVEAPIC_ACMUI 1
#define WAVEAPIC_WAVEAPIC_WAVE 1
#define WAVEAPIC_WAVEAPIC_SND 1
#define WAVEAPIC_WAVEAPIC_MIX 1
#define DEVICE_DEVCORE 1
#define DEVICE_IORM 1
#define DEVICE_PMIF 1
#define PM_PM_DEFAULT_PDD 1
#define PM_PM_MDD 1
#define PM_PM_PDD_COMMON 1
#define FATUTIL_FATUTIL_UI 1
#define FATUTIL_FATUTIL_MAIN 1
#define FATUTIL_EXFATUTIL 1
#define FILESYS_FSHEAP 1
#define FILESYS_FSMAIN 1
#define FILESYS_FSPROFILE 1
#define FILESYS_FSADVERTISE 1
#define FILESYS_FSEVENTLOG 1
#define FILESYS_FSYSRAM 1
#define FILESYS_FSREGHIVE 1
#define FILESYS_FSSECUREWIPE 1
#define FILESYS_FSREPLBIT 1
#define FILESYS_FSDBASE 1
#define FILESYS_FSPASS 1
#define NLS_EN_US 1
#define OLE32_DCOMOLE 1
#define OLE32_STG 1
#define OLEAUT32_OAALL 1
#define OLEAUT32_IDISPPROXY 1
#define IMAGING_IMG_ICO 1
#define IMAGING_IMG_TIFF 1
#define IMAGING_IMG_LIBTIFF 1
#define IMAGING_IMG_PNG 1
#define IMAGING_IMG_LIBPNG 1
#define IMAGING_IMG_PNGENCODER 1
#define IMAGING_IMG_PNGDECODER 1
#define IMAGING_IMG_GIF 1
#define IMAGING_IMG_LIBLZW 1
#define IMAGING_IMG_GIFENCODER 1
#define IMAGING_IMG_GIFDECODER 1
#define IMAGING_IMG_JPEG 1
#define IMAGING_IMG_JPEGFULL 1
#define IMAGING_IMG_JPEGMEM 1
#define IMAGING_IMG_JPEGENCODER 1
#define IMAGING_IMG_JPEGDECODER 1
#define IMAGING_IMG_BMP 1
#define IMAGING_IMG_BMPENCODER 1
#define IMAGING_IMG_BMPDECODER 1
#define MSXML3_XMLDOM 1
#define MSXML3_XMLMIME 1
#define MSXML3_XMLXSLT 1
#define MSXML3_XMLXQL 1
#define MSXML3_XMLNETFULL 1
#define MSXML3_XMLSAX 1
#define WININET_PPSTUBS 1
#define SHLWAPI_SHLWAPI_UI 1
#define SMBSERVER_SMB_FILEFACTORY 1
#define ASP_ASPPARSE 1
#define ASP_ASPCOLCT 1
#define HTTPD_HTTPISAPI 1
#define HTTPD_HTTPEXTN 1
#define HTTPD_HTTPAUTH 1
#define HTTPD_HTTPFILT 1
#define HTTPD_HTTPASP 1
#define AYGSHELL_AYGSHCORE 1
#define AYGSHELL_AYGSHHPC 1
#define AYGSHELL_AYGNQVGA 1
#define SYSCSPS_LEGACYCSP 1
#define SYSCSPS_POLICYCSP 1
#define SHCORE_IDLIST 1
#define SHCORE_PATH 1
#define CESHELL_CESHAPI 1
#define CESHELL_CESHUI 1
#define CESHELL_TASKBARLIST 1
#define CESHELL_FOLDERATTRIBUTES 1
#define CESHELL_CESHUISTD 1
#define CESHELL_CESHRECBIN 1
#define CPLMAIN_NETWORK 1
#define CPLMAIN_DATETIME 1
#define CPLMAIN_KEYBOARD 1
#define CPLMAIN_PASSWORD 1
#define CPLMAIN_OWNER 1
#define CPLMAIN_SYSTEM 1
#define CPLMAIN_DISPLAY 1
#define CPLMAIN_POINTERCMN 1
#define CPLMAIN_MOUSE 1
#define CPLMAIN_COMM 1
#define CPLMAIN_POWER 1
#define CPLMAIN_REMOVE 1
#define CPLMAIN_BACKLIGHT 1
#define CPLMAIN_COLORSCHEME 1
#define CPLMAIN_SOUNDS 1
#define CPLMAIN_DIALING 1
#define CPLMAIN_SIP 1
#define CPLMAIN_STYLUS 1
#define CPLMAIN_CERTS 1
#define QUARTZ_QUARTZ0 1
#define QUARTZ_AMUTIL 1
#define QUARTZ_FGCTL 1
#define QUARTZ_FILGRAPH 1
#define QUARTZ_QUARTZ1 1
#define QUARTZ_WAVEOUT 1
#define QUARTZ_WAVEMSR 1
#define QUARTZ_BUFFSTREAM 1
#define QUARTZ_AVIMSR 1
#define QUARTZ_AVIDEC 1
#define QUARTZ_MPGADEC 1
#define QUARTZ_MPGVDEC 1
#define QUARTZ_MP3FILTER 1
#define QUARTZ_MPGSPLIT 1
#define QUARTZ_BUFFSTREAM 1
#define QUARTZ_MP2DEMUX 1
#define QUARTZ_PSIPARSER 1
#define QUARTZ_ACMWRAP 1
#define QUARTZ_DMOWRAP 1
#define QUARTZ_URLRDR 1
#define QUARTZ_QUARTZ2 1
#define QUARTZ_ASYNCRDR 1
#define QUARTZ_QTZBASE 1
#define QUARTZ_IMGSINK 1
#define QUARTZ_SMARTTEE 1
#define QUARTZ_QUARTZ3 1
#define QUARTZ_VMRALLOCLIB 1
#define QUARTZ_VMRALLOCATORPRESENTER 1
#define QUARTZ_VMRWINDOWMANAGER 1
#define QUARTZ_VMRIMAGESYNC 1
#define QUARTZ_VMRMIXER 1
#define QUARTZ_VMRRENDERER 1
#define QUARTZ_ASFWRITER 1
#define QUARTZ_ASFMUXCORE 1
#define QUARTZ_WAVEIN 1
#define QUARTZ_VIDCAP 1
#define QUARTZ_FILCAP 1
#define QUARTZ_SCMDOUT 1
#define QUARTZ_FILEREND 1
#define QUARTZ_WMT_STRMCORE 1
#define QUARTZ_WMT_SPLITTER 1
#define QUARTZ_ACMOBJ 1
#define QUARTZ_ICMOBJ 1
#define QUARTZ_M3UPLAYL 1
#define QUARTZ_ASXPLAYL 1
#define QUARTZ_XMLPLAYL 1
#define QUARTZ_URLGRAB 1
#define QUARTZ_URLOBJ 1
#define QUARTZ_NSCFILEPARSER 1
#define QUARTZ_WMT_HTTPSTRM 1
#define QUARTZ_WMT_MMSSTRM 1
#define QUARTZ_WMT_MSBSTRM 1
#define QUARTZ_WMT_NETHELP 1
#define QUARTZ_WMT_FILESTRM 1
#define QUARTZ_WMDRM10PDAPI 1
#define QUARTZ_CONNECTHELPER 1
#define QUARTZ_QUARTZ4 1
#define QUARTZ_COLOR 1
#define QUARTZ_BUFFILTER 1
#define QUARTZ_COLOR 1
#define QUARTZ_QUARTZ5 1
#define QUARTZ_BUFFSTREAM 1
#define QUARTZ_HTTPLIB 1
#define QUARTZ_HTTPSTREAMER 1
#define QUARTZ_BUFFSTREAM 1
#define QUARTZ_LOCALSTREAMER 1
#define QUARTZ_QUARTZ5 1
#define QUARTZ_BUFFSTREAM 1
#define QUARTZ_HTTPLIB 1
#define QUARTZ_HTTPSTREAMER 1
#define QUARTZ_BUFFSTREAM 1
#define QUARTZ_LOCALSTREAMER 1
#define QUARTZ_QUARTZ3 1
#define QUARTZ_VMRALLOCLIB 1
#define QUARTZ_VMRALLOCATORPRESENTER 1
#define QUARTZ_VMRWINDOWMANAGER 1
#define QUARTZ_VMRIMAGESYNC 1
#define QUARTZ_VMRMIXER 1
#define QUARTZ_VMRRENDERER 1
#define QUARTZ_ASFWRITER 1
#define QUARTZ_ASFMUXCORE 1
#define QUARTZ_WAVEIN 1
#define QUARTZ_VIDCAP 1
#define QUARTZ_FILCAP 1
#define QUARTZ_SCMDOUT 1
#define QUARTZ_FILEREND 1
#define QUARTZ_WMT_STRMCORE 1
#define QUARTZ_WMT_SPLITTER 1
#define QUARTZ_ACMOBJ 1
#define QUARTZ_ICMOBJ 1
#define QUARTZ_M3UPLAYL 1
#define QUARTZ_ASXPLAYL 1
#define QUARTZ_XMLPLAYL 1
#define QUARTZ_URLGRAB 1
#define QUARTZ_URLOBJ 1
#define QUARTZ_NSCFILEPARSER 1
#define QUARTZ_WMT_HTTPSTRM 1
#define QUARTZ_WMT_MMSSTRM 1
#define QUARTZ_WMT_MSBSTRM 1
#define QUARTZ_WMT_NETHELP 1
#define QUARTZ_WMT_FILESTRM 1
#define QUARTZ_WMDRM10PDAPI 1
#define QUARTZ_CONNECTHELPER 1
#define QUARTZ_QUARTZ3 1
#define QUARTZ_VMRALLOCLIB 1
#define QUARTZ_VMRALLOCATORPRESENTER 1
#define QUARTZ_VMRWINDOWMANAGER 1
#define QUARTZ_VMRIMAGESYNC 1
#define QUARTZ_VMRMIXER 1
#define QUARTZ_VMRRENDERER 1
#define QUARTZ_ASFWRITER 1
#define QUARTZ_ASFMUXCORE 1
#define QUARTZ_WAVEIN 1
#define QUARTZ_VIDCAP 1
#define QUARTZ_FILCAP 1
#define QUARTZ_SCMDOUT 1
#define QUARTZ_FILEREND 1
#define QUARTZ_WMT_STRMCORE 1
#define QUARTZ_WMT_SPLITTER 1
#define QUARTZ_ACMOBJ 1
#define QUARTZ_ICMOBJ 1
#define QUARTZ_M3UPLAYL 1
#define QUARTZ_ASXPLAYL 1
#define QUARTZ_XMLPLAYL 1
#define QUARTZ_URLGRAB 1
#define QUARTZ_URLOBJ 1
#define QUARTZ_NSCFILEPARSER 1
#define QUARTZ_WMT_HTTPSTRM 1
#define QUARTZ_WMT_MMSSTRM 1
#define QUARTZ_WMT_MSBSTRM 1
#define QUARTZ_WMT_NETHELP 1
#define QUARTZ_WMT_FILESTRM 1
#define QUARTZ_WMDRM10PDAPI 1
#define QUARTZ_CONNECTHELPER 1

// <SYSGENS>
// SYSGEN_ACM_GSM610
// SYSGEN_ASP
// SYSGEN_ASYNCMAC
// SYSGEN_AS_AIRSYNC
// SYSGEN_AS_BASE
// SYSGEN_ATL
// SYSGEN_AUDIO
// SYSGEN_AUDIO_ACM
// SYSGEN_AUDIO_STDWAVEFILES
// SYSGEN_AUTH
// SYSGEN_AUTH_NTLM
// SYSGEN_AUTH_SCHANNEL
// SYSGEN_AUTORAS
// SYSGEN_AYGSHELL
// SYSGEN_BATTERY
// SYSGEN_CCPSELECT
// SYSGEN_CEDDK
// SYSGEN_CERDISP
// SYSGEN_CERTS
// SYSGEN_CMD
// SYSGEN_CNG_CORE
// SYSGEN_COMMCTRL
// SYSGEN_COMMCTRL_ANIMATE
// SYSGEN_COMMCTRL_LINK
// SYSGEN_COMMDLG
// SYSGEN_COMPOSITION
// SYSGEN_CONFIGMGR
// SYSGEN_CONNMC
// SYSGEN_CONNMGR2
// SYSGEN_CONSOLE
// SYSGEN_CORELOC
// SYSGEN_CORESTRA
// SYSGEN_CPP_EH_AND_RTTI
// SYSGEN_CREDMAN
// SYSGEN_CRYPTO
// SYSGEN_CTLPNL
// SYSGEN_CURSOR
// SYSGEN_CXPORT
// SYSGEN_DCOM
// SYSGEN_DDRAW
// SYSGEN_DEVICE
// SYSGEN_DHCPSRV
// SYSGEN_DISPLAY
// SYSGEN_DMSAPI
// SYSGEN_DMSRV
// SYSGEN_DNSAPI
// SYSGEN_DOTNETV35
// SYSGEN_DOTNETV35_SUPPORT
// SYSGEN_DSHOW
// SYSGEN_DSHOW_ACMWRAP
// SYSGEN_DSHOW_AVI
// SYSGEN_DSHOW_CAPTURE
// SYSGEN_DSHOW_DISPLAY
// SYSGEN_DSHOW_DMO
// SYSGEN_DSHOW_HTTPSTREAMER
// SYSGEN_DSHOW_ICM
// SYSGEN_DSHOW_IMAADPCM
// SYSGEN_DSHOW_LOCALSTREAMER
// SYSGEN_DSHOW_MP3
// SYSGEN_DSHOW_MPEG2DEMUX
// SYSGEN_DSHOW_MPEGA
// SYSGEN_DSHOW_MPEGSPLITTER
// SYSGEN_DSHOW_MPEGV
// SYSGEN_DSHOW_MSADPCM
// SYSGEN_DSHOW_MSG711
// SYSGEN_DSHOW_MSGSM610
// SYSGEN_DSHOW_MSRLE
// SYSGEN_DSHOW_URLRDR
// SYSGEN_DSHOW_VMR
// SYSGEN_DSHOW_WAV
// SYSGEN_DSHOW_WAVEOUT
// SYSGEN_DSHOW_WMA
// SYSGEN_DSHOW_WMA_VOICE
// SYSGEN_DSHOW_WMT
// SYSGEN_DSHOW_WMT_ASXV1
// SYSGEN_DSHOW_WMT_ASXV2
// SYSGEN_DSHOW_WMT_ASXV3
// SYSGEN_DSHOW_WMT_DRMOCX
// SYSGEN_DSHOW_WMT_HTTP
// SYSGEN_DSHOW_WMT_LOCAL
// SYSGEN_DSHOW_WMT_MMS
// SYSGEN_DSHOW_WMT_MULTI
// SYSGEN_DSHOW_WMT_NSC
// SYSGEN_DSHOW_WMT_WMDRM10PD
// SYSGEN_DSHOW_WMV
// SYSGEN_DSHOW_XDRMREMOTE
// SYSGEN_DSTSVC
// SYSGEN_EDB
// SYSGEN_EDIMEFE
// SYSGEN_ETHERNET
// SYSGEN_EXFAT
// SYSGEN_FATFS
// SYSGEN_FMTMSG
// SYSGEN_FONTS_COUR_1_30
// SYSGEN_FONTS_SYMBOL
// SYSGEN_FONTS_TAHOMA
// SYSGEN_FONTS_TIMES
// SYSGEN_FSDBASE
// SYSGEN_FSPASSWORD
// SYSGEN_FSRAMROM
// SYSGEN_FSREGHIVE
// SYSGEN_FSREPLBIT
// SYSGEN_FTPD
// SYSGEN_FULL_CRT
// SYSGEN_GDI_ALPHABLEND
// SYSGEN_GRADFILL
// SYSGEN_HTTPD
// SYSGEN_IABASE
// SYSGEN_IE7_IEFRAME
// SYSGEN_IE7_MLANG
// SYSGEN_IE7_SHLWAPI
// SYSGEN_IE7_URLMON
// SYSGEN_IE7_WININET
// SYSGEN_IMAGING
// SYSGEN_IMAGING_BMP_DECODE
// SYSGEN_IMAGING_BMP_ENCODE
// SYSGEN_IMAGING_GIF_DECODE
// SYSGEN_IMAGING_GIF_ENCODE
// SYSGEN_IMAGING_ICO_DECODE
// SYSGEN_IMAGING_JPG_DECODE
// SYSGEN_IMAGING_JPG_ENCODE
// SYSGEN_IMAGING_PNG_DECODE
// SYSGEN_IMAGING_PNG_ENCODE
// SYSGEN_IMAGING_TIFF_DECODE
// SYSGEN_IMAGING_TIFF_ENCODE
// SYSGEN_IMEJPN
// SYSGEN_IMEJPN_DB_STANDARD
// SYSGEN_IMM
// SYSGEN_IM_KANA
// SYSGEN_INPUTSCOPES
// SYSGEN_IPHLPAPI
// SYSGEN_KBD_JAPANESE
// SYSGEN_KBD_US
// SYSGEN_LAP_PSWD
// SYSGEN_LARGEKB
// SYSGEN_LASS
// SYSGEN_LOCALAUDIO
// SYSGEN_MEDIAAPPS_MEDIALIBRARY
// SYSGEN_MEDIAAPPS_MEDIARENDERER
// SYSGEN_MEDIAAPPS_WIC
// SYSGEN_MEDIARENDERER_MEDIAPLAYER
// SYSGEN_MGFE
// SYSGEN_MINGDI
// SYSGEN_MINGWES
// SYSGEN_MINICOM
// SYSGEN_MININPUT
// SYSGEN_MINWMGR
// SYSGEN_MODEM
// SYSGEN_MSGQUEUE
// SYSGEN_MSIM
// SYSGEN_MSMQ
// SYSGEN_MSPART
// SYSGEN_MSXML_DOM
// SYSGEN_MSXML_MIMEVIEWER
// SYSGEN_MSXML_MINI
// SYSGEN_MSXML_XQL
// SYSGEN_MSXML_XSLT
// SYSGEN_MULTIUI
// SYSGEN_MUSIC_PLAYER
// SYSGEN_NDIS
// SYSGEN_NDISUIO
// SYSGEN_NETUTILS
// SYSGEN_NETWORK_POLICY
// SYSGEN_NKCOMPR
// SYSGEN_NKMAPFILE
// SYSGEN_NKTZINIT
// SYSGEN_NLED
// SYSGEN_NLS_EN_US
// SYSGEN_NOTIFY
// SYSGEN_OLE
// SYSGEN_OLE_GUIDS
// SYSGEN_OLE_STG
// SYSGEN_OSSVCS
// SYSGEN_PHOTO_VIEWER
// SYSGEN_PHYSICSENGINE
// SYSGEN_PM
// SYSGEN_PPP
// SYSGEN_PRINTING
// SYSGEN_PWORD
// SYSGEN_REDIR
// SYSGEN_RELFSD
// SYSGEN_RPCRT4
// SYSGEN_SDBUS
// SYSGEN_SD_MEMORY
// SYSGEN_SECUREWIPE
// SYSGEN_SERDEV
// SYSGEN_SERVICES
// SYSGEN_SHELL
// SYSGEN_SMB_FILE
// SYSGEN_SMB_UI
// SYSGEN_SOFTKB
// SYSGEN_SQLCOMPACT
// SYSGEN_STANDARDSHELL
// SYSGEN_STATE_NOTIFICATIONS
// SYSGEN_STDIO
// SYSGEN_STDIOA
// SYSGEN_STOREMGR
// SYSGEN_STOREMGR_CPL
// SYSGEN_STREAMAUDIO
// SYSGEN_STRSAFE
// SYSGEN_SYSCSPS
// SYSGEN_TAPI
// SYSGEN_TCPIP
// SYSGEN_TELNETD
// SYSGEN_TFAT
// SYSGEN_TIMEZONES
// SYSGEN_TOOLHELP
// SYSGEN_TOUCH
// SYSGEN_TSKSCHCSP
// SYSGEN_UIPROXY
// SYSGEN_UNIMODEM
// SYSGEN_UPNP
// SYSGEN_UPNP_AV
// SYSGEN_UPNP_AV_CTRL
// SYSGEN_UPNP_AV_DEVICE
// SYSGEN_UPNP_CTRL
// SYSGEN_UPNP_DEVICE
// SYSGEN_USB
// SYSGEN_USBFN
// SYSGEN_USBFN_SERIAL
// SYSGEN_USB_HID
// SYSGEN_USB_HID_CLIENTS
// SYSGEN_USB_HID_KEYBOARD
// SYSGEN_USB_HID_MOUSE
// SYSGEN_VEM
// SYSGEN_VIDEO_PLAYER
// SYSGEN_WCELOAD
// SYSGEN_WININET
// SYSGEN_WINSOCK
// SYSGEN_XAML_RUNTIME
// SYSGEN_XMLLITE
// </SYSGENS>
#endif
