        ��  ��                  X  <   �� V S _ V E R S I O N _ I N F O       0         X4   V S _ V E R S I O N _ I N F O     ���               ?                         �   S t r i n g F i l e I n f o   �   0 4 0 4 0 3 B 6   J   C o m p a n y N a m e     T O D O :   < C o m p a n y   n a m e >     Z   F i l e D e s c r i p t i o n     T O D O :   < F i l e   d e s c r i p t i o n >     0   F i l e V e r s i o n     1 . 0 . 0 . 1   � 0  L e g a l C o p y r i g h t   T O D O :   ( c )   < C o m p a n y   n a m e > .     A l l   r i g h t s   r e s e r v e d .   8   I n t e r n a l N a m e   M L i b D l l . d l l   @   O r i g i n a l F i l e n a m e   M L i b D l l . d l l   J   P r o d u c t N a m e     T O D O :   < P r o d u c t   n a m e >     4   P r o d u c t V e r s i o n   1 . 0 . 0 . 1   (    O L E S e l f R e g i s t e r     D    V a r F i l e I n f o     $    T r a n s l a t i o n     �S  0   R E G I S T R Y   ��e       0         HKCR
{
	NoRemove AppID
	{
		'%APPID%' = s 'MLibDll'
		'MLibDll.DLL'
		{
			val AppID = s '%APPID%'
		}
	}
}

HKLM
{
	NoRemove SOFTWARE
	{
		NoRemove Microsoft
		{
			NoRemove MLib
			{
				ForceRemove MetadataParserPlugins
				{
					val 'Plugin1' = s '{9F4D1938-181D-4756-97E7-E60F2465E95C}'
					val 'Plugin2' = s '{89A5832E-16FB-40d7-A9F2-DD643BB84231}'
					val 'Plugin3' = s '{8A44ED0D-931D-4626-B7A2-52F4C590A00C}'
					val 'Plugin4' = s '{9ABC2CD4-3CE0-4fb3-95D5-A6DFCACFF9AA}'
					val 'Plugin5' = s '{1C19C217-6908-4390-A801-995C800BAE5F}'
;					val 'Plugin6' = s '{289CEE60-D768-4574-9BF3-E00D2EE14AD6}'
				}
				ForceRemove DataSourcePlugins
				{
					val 'Plugin1' = s '{14A8EF7D-9580-4386-A4CB-AC34B2DC6C2C}'
					val 'Plugin2' = s '{25D74430-7674-4afe-BF90-D1CE54860A16}'
				}
			}
		}
	}
}
			
   0   R E G I S T R Y   ��f       0         HKCR
{
    NoRemove CLSID
    {
        ForceRemove {CE7919AB-BF65-4ABB-A5EB-16B1B8F0DD7D} = s 'MLCore Class'
        {
            InprocServer32 = s '%MODULE%'
            {
                val ThreadingModel = s 'Both'
            }
        }
    }
}
   0   R E G I S T R Y   ��q       0         HKCR
{
    NoRemove CLSID
    {
        ForceRemove {C52555CE-7E5F-4f3c-98EF-9BDEBE8C443D} = s 'MLImageMetadataParser Class'
        {
            InprocServer32 = s '%MODULE%'
            {
                val ThreadingModel = s 'Both'
            }
        }
    }
}
    0   R E G I S T R Y   ��k       0         HKCR
{
    NoRemove CLSID
    {
        ForceRemove {14A8EF7D-9580-4386-A4CB-AC34B2DC6C2C} = s 'MLDSFileSystemPlugin Class'
        {
            InprocServer32 = s '%MODULE%'
            {
                val ThreadingModel = s 'Both'
            }
        }
    }
}
     0   R E G I S T R Y   ��z       0         HKCR
{
    NoRemove CLSID
    {
        ForceRemove {60F69E85-C73A-4827-8E40-C9861A44703A} = s 'MLDSDLNAPlugin Class'
        {
            InprocServer32 = s '%MODULE%'
            {
                val ThreadingModel = s 'Both'
            }
        }
    }
}
 �8  0   D B S C H E M A   ��x       0         --
-- Copyright (c) Microsoft Corporation.  All rights reserved.
--
--
-- Use of this source code is subject to the terms of the Microsoft
-- premium shared source license agreement under which you licensed
-- this source code. If you did not accept the terms of the license
-- agreement, you are not authorized to use this source code.
-- For the terms of the license, please see the license agreement
-- signed by you and Microsoft.
-- THE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
--

CREATE TABLE tbl_PropertiesValues(
    application nvarchar(16), 
    property nvarchar(16),
    value nvarchar(4000),
    constraint pk_PropertiesValues primary key (application, property));
CREATE TABLE tbl_music(
    music_id bigint identity,
   music_title nvarchar(255),
   music_logicalStorageID int,
   music_formatCode smallint,
   music_sampleFormatCode smallint,
   music_isDeleted bit,
   music_isDRMProtected bit,
   music_changedTime datetime,
   music_creationTime datetime,
   music_fileURL nvarchar(260),
   music_logicalFileURL nvarchar(260),
   music_fileSize bigint,
   music_fileTime datetime,
   music_parentID bigint,
   music_modifiedTime datetime,
   music_userRating smallint,
   music_folderName nvarchar(260),
   music_fileName nvarchar(260),
   music_datasourceId int,
   music_copyright nvarchar(255),
   music_sampleURL nvarchar(260),
   music_sampleWidth int,
   music_sampleHeight int,
   music_width int,
   music_height int,
   music_trackNumber smallint,
   music_composer nvarchar(255),
   music_conductor nvarchar(255),
   music_description nvarchar(255),
   music_duration int,
   music_parentalRating nvarchar(255),
   music_originalReleaseDate datetime,
   music_playCount int,
   music_audioBitRate int,
   music_language nvarchar(255),
   music_protocolInfo nvarchar(1),
   music_dlnaXML nvarchar(1),
   music_bitRateType smallint,
   music_sampleRate int,
   music_numChannels smallint,
   music_audioWaveCodec int,
   music_scanType smallint,
   music_videoFourCCCodec int,
   music_videoBitRate int,
   music_framesPerThousandSeconds int,
   music_keyFrameDistance int,
   music_encodingProfile nvarchar(255),
    music_artist_id bigint,
    music_genre_id bigint,
    music_album_id bigint,
    CONSTRAINT pk_music primary key(music_id));
CREATE TABLE tbl_album(
    album_id bigint identity,
   album_title nvarchar(255),
   album_artist_name nvarchar(255),
    CONSTRAINT pk_album primary key(album_id));
CREATE TABLE tbl_artist(
    artist_id bigint identity,
   artist_name nvarchar(255),
    CONSTRAINT pk_artist primary key(artist_id));
CREATE TABLE tbl_genre(
    genre_id bigint identity,
   genre_name nvarchar(255),
    CONSTRAINT pk_genre primary key(genre_id));
CREATE TABLE tbl_series(
    series_id bigint identity,
   series_name nvarchar(255),
    CONSTRAINT pk_series primary key(series_id));
CREATE TABLE tbl_video(
    video_id bigint identity,
   video_title nvarchar(255),
   video_logicalStorageID int,
   video_formatCode smallint,
   video_sampleFormatCode smallint,
   video_isDeleted bit,
   video_isDRMProtected bit,
   video_changedTime datetime,
   video_creationTime datetime,
   video_fileURL nvarchar(260),
   video_logicalFileURL nvarchar(260),
   video_fileSize bigint,
   video_fileTime datetime,
   video_parentID bigint,
   video_folderName nvarchar(260),
   video_fileName nvarchar(260),
   video_datasourceId int,
   video_modifiedTime datetime,
   video_copyright nvarchar(255),
   video_userRating smallint,
   video_sampleURL nvarchar(260),
   video_sampleWidth int,
   video_sampleHeight int,
   video_width int,
   video_height int,
   video_trackNumber smallint,
   video_composer nvarchar(255),
   video_description nvarchar(255),
   video_duration int,
   video_parentalRating nvarchar(255),
   video_originalReleaseDate datetime,
   video_playCount int,
   video_lastPlayedPosition int,
   video_episodeNumber int,
   video_seasonNumber int,
   video_language nvarchar(255),
   video_protocolInfo nvarchar(1),
   video_dlnaXML nvarchar(1),
   video_bitRateType smallint,
   video_sampleRate int,
   video_numChannels smallint,
   video_scanType smallint,
   video_audioWaveCodec int,
   video_audioBitRate int,
   video_videoFourCCCodec int,
   video_videoBitRate int,
   video_framesPerThousandSeconds int,
   video_keyFrameDistance int,
   video_encodingProfile nvarchar(255),
    video_artist_id bigint,
    video_genre_id bigint,
    video_album_id bigint,
    video_series_id bigint,
    CONSTRAINT pk_video primary key(video_id));
CREATE TABLE tbl_photo(
    photo_id bigint identity,
   photo_title nvarchar(255),
   photo_logicalStorageID int,
   photo_formatCode smallint,
   photo_sampleFormatCode smallint,
   photo_isDeleted bit,
   photo_isDRMProtected bit,
   photo_changedTime datetime,
   photo_creationTime datetime,
   photo_fileURL nvarchar(260),
   photo_logicalFileURL nvarchar(260),
   photo_fileSize bigint,
   photo_fileTime datetime,
   photo_parentID bigint,
   photo_folderName nvarchar(260),
   photo_fileName nvarchar(260),
   photo_datasourceId int,
   photo_modifiedTime datetime,
   photo_copyright nvarchar(255),
   photo_userRating smallint,
   photo_sampleURL nvarchar(260),
   photo_sampleWidth int,
   photo_sampleHeight int,
   photo_author nvarchar(255),
   photo_width int,
   photo_height int,
   photo_description nvarchar(255),
   photo_dateTaken datetime,
   photo_monthTaken int,
   photo_yearTaken int,
   photo_location nvarchar(255),
   photo_category nvarchar(255),
   photo_protocolInfo nvarchar(1),
   photo_dlnaXML nvarchar(1),
   photo_useCount int,
    photo_day_id bigint,
    CONSTRAINT pk_photo primary key(photo_id));
CREATE TABLE tbl_day(
    day_id bigint identity,
   day_name nvarchar(16),
    day_month_id bigint,
    CONSTRAINT pk_day primary key(day_id));
CREATE TABLE tbl_month(
    month_id bigint identity,
   month_name nvarchar(16),
    month_year_id bigint,
    CONSTRAINT pk_month primary key(month_id));
CREATE TABLE tbl_year(
    year_id bigint identity,
   year_name nvarchar(8),
    CONSTRAINT pk_year primary key(year_id));
CREATE TABLE tbl_playlistEntry(
    playlistEntry_id bigint identity,
   playlistEntry_title nvarchar(255),
   playlistEntry_track smallint,
   playlistEntry_trackID bigint,
   playlistEntry_trackEntity smallint,
    playlistEntry_playlist_id bigint,
    CONSTRAINT pk_playlistEntry primary key(playlistEntry_id));
CREATE TABLE tbl_generic_referenceEntry(
    generic_referenceEntry_id bigint identity,
   generic_referenceEntry_title nvarchar(255),
   generic_referenceEntry_track smallint,
   generic_referenceEntry_refID bigint,
   generic_referenceEntry_refEntity smallint,
    generic_referenceEntry_generic_id bigint,
    CONSTRAINT pk_generic_referenceEntry primary key(generic_referenceEntry_id));
CREATE TABLE tbl_albumArt_referenceEntry(
    albumArt_referenceEntry_id bigint identity,
   albumArt_referenceEntry_title nvarchar(255),
   albumArt_referenceEntry_track smallint,
   albumArt_referenceEntry_refID bigint,
   albumArt_referenceEntry_refEntity smallint,
    albumArt_referenceEntry_albumArt_id bigint,
    CONSTRAINT pk_albumArt_referenceEntry primary key(albumArt_referenceEntry_id));
CREATE TABLE tbl_playlist(
    playlist_id bigint identity,
   playlist_title nvarchar(255),
   playlist_logicalStorageID int,
   playlist_formatCode smallint,
   playlist_sampleFormatCode smallint,
   playlist_isDeleted bit,
   playlist_changedTime datetime,
   playlist_creationTime datetime,
   playlist_fileURL nvarchar(260),
   playlist_logicalFileURL nvarchar(260),
   playlist_fileSize bigint,
   playlist_fileTime datetime,
   playlist_parentID bigint,
   playlist_folderName nvarchar(260),
   playlist_fileName nvarchar(260),
   playlist_useCount int,
    CONSTRAINT pk_playlist primary key(playlist_id));
CREATE TABLE tbl_albumArt(
    albumArt_id bigint identity,
   albumArt_title nvarchar(255),
   albumArt_logicalStorageID int,
   albumArt_formatCode smallint,
   albumArt_isDeleted bit,
   albumArt_changedTime datetime,
   albumArt_creationTime datetime,
   albumArt_fileURL nvarchar(260),
   albumArt_logicalFileURL nvarchar(260),
   albumArt_fileSize bigint,
   albumArt_fileTime datetime,
   albumArt_parentID bigint,
   albumArt_folderName nvarchar(260),
   albumArt_fileName nvarchar(260),
   albumArt_purchaseFlag bit,
   albumArt_userRating smallint,
   albumArt_sampleFormatCode smallint,
   albumArt_sampleURL nvarchar(260),
   albumArt_sampleWidth int,
   albumArt_sampleHeight int,
   albumArt_useCount int,
    albumArt_genre_id bigint,
    albumArt_album_id bigint,
    CONSTRAINT pk_albumArt primary key(albumArt_id));
CREATE TABLE tbl_generic(
    generic_id bigint identity,
   generic_title nvarchar(255),
   generic_logicalStorageID int,
   generic_formatCode smallint,
   generic_sampleFormatCode smallint,
   generic_isDeleted bit,
   generic_isDRMProtected bit,
   generic_changedTime datetime,
   generic_creationTime datetime,
   generic_fileURL nvarchar(260),
   generic_logicalFileURL nvarchar(260),
   generic_fileSize bigint,
   generic_fileTime datetime,
   generic_folderName nvarchar(260),
   generic_fileName nvarchar(260),
   generic_parentID bigint,
   generic_isDirectory bit,
    CONSTRAINT pk_generic primary key(generic_id));
CREATE TABLE tbl_parseQueue(
    parseQueue_id bigint identity,
   parseQueue_inputURL nvarchar(260),
   parseQueue_fileSize bigint,
   parseQueue_fileTime datetime,
   parseQueue_outputURL nvarchar(260),
   parseQueue_thumbnailURL nvarchar(260),
   parseQueue_metadataState smallint,
   parseQueue_thumbnailState smallint,
    CONSTRAINT pk_parseQueue primary key(parseQueue_id));
ALTER TABLE tbl_music ADD CONSTRAINT fk_music_artist FOREIGN KEY (music_artist_id) REFERENCES tbl_artist(artist_id) ON DELETE SET NULL;
ALTER TABLE tbl_music ADD CONSTRAINT fk_music_genre FOREIGN KEY (music_genre_id) REFERENCES tbl_genre(genre_id) ON DELETE SET NULL;
ALTER TABLE tbl_music ADD CONSTRAINT fk_music_album FOREIGN KEY (music_album_id) REFERENCES tbl_album(album_id) ON DELETE SET NULL;
CREATE INDEX idx_music_title ON tbl_music(music_title);
CREATE INDEX idx_music_logicalFileURL ON tbl_music(music_logicalFileURL);
CREATE UNIQUE INDEX idx_music_uq ON tbl_music(music_fileURL);
CREATE UNIQUE INDEX idx_album_uq ON tbl_album(album_title, album_artist_name);
CREATE UNIQUE INDEX idx_artist_uq ON tbl_artist(artist_name);
CREATE UNIQUE INDEX idx_genre_uq ON tbl_genre(genre_name);
CREATE UNIQUE INDEX idx_series_uq ON tbl_series(series_name);
ALTER TABLE tbl_video ADD CONSTRAINT fk_video_artist FOREIGN KEY (video_artist_id) REFERENCES tbl_artist(artist_id) ON DELETE SET NULL;
ALTER TABLE tbl_video ADD CONSTRAINT fk_video_genre FOREIGN KEY (video_genre_id) REFERENCES tbl_genre(genre_id) ON DELETE SET NULL;
ALTER TABLE tbl_video ADD CONSTRAINT fk_video_album FOREIGN KEY (video_album_id) REFERENCES tbl_album(album_id) ON DELETE SET NULL;
ALTER TABLE tbl_video ADD CONSTRAINT fk_video_series FOREIGN KEY (video_series_id) REFERENCES tbl_series(series_id) ON DELETE SET NULL;
CREATE INDEX idx_video_title ON tbl_video(video_title);
CREATE INDEX idx_video_logicalFileURL ON tbl_video(video_logicalFileURL);
CREATE UNIQUE INDEX idx_video_uq ON tbl_video(video_fileURL);
ALTER TABLE tbl_photo ADD CONSTRAINT fk_photo_day FOREIGN KEY (photo_day_id) REFERENCES tbl_day(day_id);
CREATE INDEX idx_photo_title ON tbl_photo(photo_title);
CREATE INDEX idx_photo_logicalFileURL ON tbl_photo(photo_logicalFileURL);
CREATE UNIQUE INDEX idx_photo_uq ON tbl_photo(photo_fileURL);
ALTER TABLE tbl_day ADD CONSTRAINT fk_day_month FOREIGN KEY (day_month_id) REFERENCES tbl_month(month_id);
CREATE UNIQUE INDEX idx_day_uq ON tbl_day(day_name);
ALTER TABLE tbl_month ADD CONSTRAINT fk_month_year FOREIGN KEY (month_year_id) REFERENCES tbl_year(year_id);
CREATE UNIQUE INDEX idx_month_uq ON tbl_month(month_name);
CREATE UNIQUE INDEX idx_year_uq ON tbl_year(year_name);
ALTER TABLE tbl_playlistEntry ADD CONSTRAINT fk_playlistEntry_playlist FOREIGN KEY (playlistEntry_playlist_id) REFERENCES tbl_playlist(playlist_id) ON DELETE CASCADE;
CREATE INDEX idx_playlistEntry_title ON tbl_playlistEntry(playlistEntry_title);
CREATE UNIQUE INDEX idx_playlistEntry_uq ON tbl_playlistEntry(playlistEntry_track, playlistEntry_playlist_id);
ALTER TABLE tbl_generic_referenceEntry ADD CONSTRAINT fk_generic_referenceEntry_generic FOREIGN KEY (generic_referenceEntry_generic_id) REFERENCES tbl_generic(generic_id) ON DELETE CASCADE;
CREATE INDEX idx_generic_referenceEntry_title ON tbl_generic_referenceEntry(generic_referenceEntry_title);
CREATE UNIQUE INDEX idx_generic_referenceEntry_uq ON tbl_generic_referenceEntry(generic_referenceEntry_track, generic_referenceEntry_generic_id);
ALTER TABLE tbl_albumArt_referenceEntry ADD CONSTRAINT fk_albumArt_referenceEntry_albumArt FOREIGN KEY (albumArt_referenceEntry_albumArt_id) REFERENCES tbl_albumArt(albumArt_id) ON DELETE CASCADE;
CREATE INDEX idx_albumArt_referenceEntry_title ON tbl_albumArt_referenceEntry(albumArt_referenceEntry_title);
CREATE UNIQUE INDEX idx_albumArt_referenceEntry_uq ON tbl_albumArt_referenceEntry(albumArt_referenceEntry_track, albumArt_referenceEntry_albumArt_id);
CREATE INDEX idx_playlist_title ON tbl_playlist(playlist_title);
CREATE INDEX idx_playlist_logicalFileURL ON tbl_playlist(playlist_logicalFileURL);
CREATE UNIQUE INDEX idx_playlist_uq ON tbl_playlist(playlist_fileURL);
ALTER TABLE tbl_albumArt ADD CONSTRAINT fk_albumArt_genre FOREIGN KEY (albumArt_genre_id) REFERENCES tbl_genre(genre_id) ON DELETE SET NULL;
ALTER TABLE tbl_albumArt ADD CONSTRAINT fk_albumArt_album FOREIGN KEY (albumArt_album_id) REFERENCES tbl_album(album_id) ON DELETE SET NULL;
CREATE INDEX idx_albumArt_title ON tbl_albumArt(albumArt_title);
CREATE INDEX idx_albumArt_logicalFileURL ON tbl_albumArt(albumArt_logicalFileURL);
CREATE UNIQUE INDEX idx_albumArt_uq ON tbl_albumArt(albumArt_fileURL);
CREATE INDEX idx_generic_title ON tbl_generic(generic_title);
CREATE INDEX idx_generic_logicalFileURL ON tbl_generic(generic_logicalFileURL);
CREATE UNIQUE INDEX idx_generic_uq ON tbl_generic(generic_fileURL);
CREATE UNIQUE INDEX idx_parseQueue_uq ON tbl_parseQueue(parseQueue_inputURL);
  0   R E G I S T R Y   ��{       0         HKCR
{
    NoRemove CLSID
    {
        ForceRemove {9ABC2CD4-3CE0-4fb3-95D5-A6DFCACFF9AA} = s 'MLASFMetadataParser Class'
        {
            InprocServer32 = s '%MODULE%'
            {
                val ThreadingModel = s 'Both'
            }
        }
    }
}
  0   R E G I S T R Y   ��|       0         HKCR
{
    NoRemove CLSID
    {
        ForceRemove {289CEE60-D768-4574-9BF3-E00D2EE14AD6} = s 'MLASFMetadataParser Class'
        {
            InprocServer32 = s '%MODULE%'
            {
                val ThreadingModel = s 'Both'
            }
        }
    }
}
  0   R E G I S T R Y   ��}       0         HKCR
{
    NoRemove CLSID
    {
        ForceRemove {1C19C217-6908-4390-A801-995C800BAE5F} = s 'MLMP3MetadataParser Class'
        {
            InprocServer32 = s '%MODULE%'
            {
                val ThreadingModel = s 'Both'
            }
        }
    }
}
.       �� ��     0                 M L i b D l l                         