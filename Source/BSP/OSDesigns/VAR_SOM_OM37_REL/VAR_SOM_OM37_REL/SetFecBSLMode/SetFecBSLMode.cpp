// SetFecBSLMode.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

HINSTANCE lHndlInterfaceSDKDll;
typedef void (*SetFECBSLMode)(void);

int _tmain(int argc, TCHAR *argv[], TCHAR *envp[])
{
	int i=0;
	SetFECBSLMode pfSetFECBSLMode;
	
	_tprintf(_T("Hello World!\n"));
	

	lHndlInterfaceSDKDll = LoadLibrary(L"InterfaceSDK.dll");
	if(lHndlInterfaceSDKDll==NULL)
    {
	   _tprintf(_T("Fail to Load InterfaceSDK \n"));
       RETAILMSG(1,(TEXT("-----Fail to LOAD InterfaceSDK.DLL------ \t\r\n")));
	   return 1;
	}
	
	//Encoder DLL WriteRelayBit function Handle
	pfSetFECBSLMode= (SetFECBSLMode)GetProcAddress(lHndlInterfaceSDKDll,L"SetFECBSLMode");
		
	if( pfSetFECBSLMode==NULL)
	{
		_tprintf(_T("Fail to Open  SET FEC to BSL Mode \n"));
		   RETAILMSG(1,(TEXT("-----Fail to Open WriteLEDBit/WriteRelayBit Function Handle from EncoderSDK.DLL------ \t\r\n")));
	}


	//while(i++<5)
	{
		_tprintf(_T("SET FEC to BSL Mode \n"));
		pfSetFECBSLMode();
		Sleep(30*100);
	}

	Sleep(5000);

    return 0;
}

