// StartApp.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
BOOL Reboot(void);

int _tmain(int argc, TCHAR *argv[], TCHAR *envp[])
{
	if(!Reboot())
	{
		RETAILMSG( 1, (TEXT("Failed to reboot, ReBoot Manually\n")));		
		return FALSE;
	}
    return 0;
}

BOOL Reboot(void)
{
	HINSTANCE hIns;
	typedef char (*RebootBoard)(void);
	RebootBoard pfRebootBoard;

	hIns = LoadLibrary(L"interfacesdk.dll"); //Load lib
	if(hIns==NULL)
	{
		RETAILMSG(1,(TEXT("-----Fail to LOAD interfacesdk.DLL------ \t\r\n")));
	}
	pfRebootBoard = (RebootBoard)GetProcAddress(hIns,L"RebootBoard");

	if( pfRebootBoard==NULL )
	{
		RETAILMSG(1,(TEXT("-----Fail to Get RebootBoard Function Handle from INTERFACESDK.DLL------ \t\r\n")));	
	}
	RETAILMSG( 1, (TEXT("Rebooting Please Wait....\n")));		
	pfRebootBoard();
	Sleep(500);
	RETAILMSG( 1, (TEXT("Failed to Reboot\n")));		
	return FALSE;
}