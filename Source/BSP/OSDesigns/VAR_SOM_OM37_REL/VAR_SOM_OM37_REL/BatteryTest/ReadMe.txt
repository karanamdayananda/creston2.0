////////////////////////////////////////////////////////////////////////

FEATURE NAME:



MANUFACTURER:



CONTACT INFO:



DATE:



VERSION:



SIZE:



DESCRIPTION:



NOTES:



////////////////////////////////////////////////////////////////////////


========================================================================
       CONSOLE APPLICATION : BatteryTest
========================================================================


The Subproject Wizard has created this BatteryTest application for you.  

This file contains a summary of what you will find in each of the files that
make up your BatteryTest application.

BatteryTest.pbpxml
    This file contains information at the subproject level and
    is used to build a single subproject for an OS design.
    Other users can share the subproject (.pbpxml) file.

BatteryTest.cpp
    This is the main application source file.

/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named BatteryTest.pch and a precompiled types file named StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
Other notes:

The Subproject Wizard uses "TODO:" to indicate parts of the source code you
should add to or customize.

/////////////////////////////////////////////////////////////////////////////
