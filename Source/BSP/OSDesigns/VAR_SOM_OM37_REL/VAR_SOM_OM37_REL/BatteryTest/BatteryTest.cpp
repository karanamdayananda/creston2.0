// BatteryTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

typedef struct 
{
	BOOL bBatteryCharging;
	BOOL bACPresent;
	BOOL bBatteryPresent;
	BOOL bPowerFail;
	BOOL bAlarmInhibited;
	BOOL bChargeInhibited;
	BOOL bBatteryFull;
	DWORD batCapacity; //0 to 100%
	DWORD batCycleCount;
}BATTERY_INFO;

#define BAT_DRV_NAME L"BAT1:"

int _tmain(int argc, TCHAR *argv[], TCHAR *envp[])
{
      HANDLE      hDrvContext;
	  DWORD size = 2;
	  HANDLE hApplicationEvent;
	  DWORD i = 0;
	  DWORD nbRead;
	  BATTERY_INFO *pBatteryInfo;
	  // Open the driver
      hDrvContext = CreateFile(_T("BAT1:"),  GENERIC_READ|GENERIC_WRITE,             // desired access
            FILE_SHARE_READ | FILE_SHARE_WRITE,     // sharing mode
            NULL,                                   // security attributes (ignored)
            OPEN_EXISTING,                          // creation disposition
            FILE_FLAG_RANDOM_ACCESS,                // flags/attributes
            NULL);                   

      if (hDrvContext == NULL) {
            RETAILMSG(1, (TEXT("ERROR: Can't open driver\r\n")));
            return -1;
      }
	  
	  
	//Create Event for Application
	hApplicationEvent =  CreateEvent(NULL, FALSE, FALSE, TEXT("ReadBatteryStatusEvent"));
	if (hApplicationEvent == NULL)
	{
		RETAILMSG(1, (L"ERROR: BatteryPDDInitialize:Failed create Application event [ReadBatteryStatusEvent]\r\n"));

	}
	pBatteryInfo = (BATTERY_INFO *)LocalAlloc(LPTR, sizeof(BATTERY_INFO));;
	  
	  while(i++<10)
	  {
		  // Wait for event
		  WaitForSingleObject(hApplicationEvent, INFINITE);
		  memset(pBatteryInfo,0x00, sizeof(BATTERY_INFO));
		  RETAILMSG(1, (TEXT("BatteryTest: Something changed Read Battery\r\n")));
		  _tprintf(_T("BatteryTest: Something changed Read Battery\r\n"));
		  ReadFile(hDrvContext, pBatteryInfo, size, &nbRead,NULL);

		  if(pBatteryInfo->bBatteryPresent)
		  {
			  RETAILMSG(1, (L"BatteryTest Battery	: Present\r\n"));
		  _tprintf(_T("BatteryTest Battery	: Present\r\n"));
		  }
		  else
		  {	  RETAILMSG(1, (L"BatteryTest Battery	: Not Present\r\n"));
		  _tprintf(_T("BatteryTest Battery	: Not Present\r\n"));
		  }
		  
		  if(pBatteryInfo->bBatteryCharging)
		  {	
			  RETAILMSG(1, (L"-BatteryTest Battery : Charging\r\n"));
			  _tprintf(_T("-BatteryTest Battery : Charging\r\n"));
		  }
		  else
		  {
			  RETAILMSG(1, (L"-BatteryTest Battery : NOT Charging\r\n"));
			  _tprintf(_T("-BatteryTest Battery : NOT Charging\r\n"));
		  }
		  if(pBatteryInfo->bACPresent)
		  {	
			  RETAILMSG(1, (L"-BatteryTest bACPresent : TRUE\r\n"));
			  _tprintf(_T("-BatteryTest bACPresent : TRUE\r\n"));
		  }
		  else
		  {
			  RETAILMSG(1, (L"-BatteryTest bACPresent : FALSE\r\n"));
			  _tprintf(_T("-BatteryTest bACPresent : FALSE\r\n"));
		  }
		  if(pBatteryInfo->bBatteryFull)
		  {	
			  RETAILMSG(1, (L"-BatteryTest bBatteryFull : TRUE\r\n"));
			  _tprintf(_T("-BatteryTest bBatteryFull : TRUE\r\n"));
		  }
		  else
		  {
			  RETAILMSG(1, (L"-BatteryTest bBatteryFull : FALSE\r\n"));
			  _tprintf(_T("-BatteryTest bBatteryFull : FALSE\r\n"));
		  }
		
		  RETAILMSG(1, (TEXT("BatteryTest: Battery Capacity [%d %]\r\n"),pBatteryInfo->batCapacity));	
		  _tprintf(_T("BatteryTest: Battery Capacity [%d %]\r\n"),pBatteryInfo->batCapacity);	

		  RETAILMSG(1, (TEXT("BatteryTest: Battery Cycle Count [%d %]\r\n"),pBatteryInfo->batCycleCount));	
		  _tprintf(_T("BatteryTest: Battery Cycle Count [%d %]\r\n"),pBatteryInfo->batCycleCount);	

		  RETAILMSG(1, (TEXT("BatteryTest: bPowerFail 1->yes 0->no [%d ]\r\n"),pBatteryInfo->bPowerFail));	
		  _tprintf(_T("BatteryTest: bPowerFail 1->yes 0->no [%d %]\r\n"),pBatteryInfo->bPowerFail);	

		  RETAILMSG(1, (TEXT("BatteryTest: bAlarmInhibited 1->yes 0->no [%d ]\r\n"),pBatteryInfo->bAlarmInhibited));	
		  _tprintf(_T("BatteryTest: bAlarmInhibited 1->yes 0->no [%d ]\r\n"),pBatteryInfo->bAlarmInhibited);	

		  RETAILMSG(1, (TEXT("BatteryTest: bChargeInhibited 1->yes 0->no [%d ]\r\n"),pBatteryInfo->bChargeInhibited));	
		  _tprintf(_T("BatteryTest: bChargeInhibited 1->yes 0->no [%d ]\r\n"),pBatteryInfo->bChargeInhibited);	

	  }
      // close driver
      CloseHandle(hDrvContext);
 
      return 0;
}
