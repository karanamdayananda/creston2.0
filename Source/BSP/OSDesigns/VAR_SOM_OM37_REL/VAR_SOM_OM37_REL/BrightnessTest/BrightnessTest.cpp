// BrightnessTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

HINSTANCE lHndlPWMSDKDll;
typedef char (*SetDisplayBrightness)(int percentage);


int _tmain(int argc, TCHAR *argv[], TCHAR *envp[])
{
	DWORD percentage;
	//Check Application Parameters
	if(argc<2) //If there is no parameter, load default value
	{
		RETAILMSG(1, (L"No GPIO Number Specifiedr\n"));
		goto EXIT;
	}
	else if(argc>2 || wcscmp(argv[1],L"?") == 0)
	{
		RETAILMSG(1, (L"Usage:\r\n"));
		RETAILMSG(1, (L"Help: TestLED ?\r\n"));
		RETAILMSG(1, (L"TestLED <positive GPIO number >\r\n"));
		RETAILMSG(1, (L"i.e. TestLED 103 --> this will make 103 GPIO LED on and off.\r\n"));
		goto EXIT;
	}

	
	percentage = (DWORD)_wtoi(argv[1]);

		
	RETAILMSG(1, (TEXT("\r\n +-------------------------------------------------------------------+")));
	RETAILMSG(1, (TEXT("\r\n |               Setting LCD Brightness Percentagep[%d %]                                  |"), percentage));
	RETAILMSG(1, (TEXT("\r\n +-------------------------------------------------------------------+\r\n \r\n ")));

	RETAILMSG(1,(TEXT("percentage [%d %]\t\r\n"),percentage));
	
	lHndlPWMSDKDll = LoadLibrary(L"PWMSDK.dll");
	if(lHndlPWMSDKDll==NULL)
    {
       RETAILMSG(1,(TEXT("-----Fail to LOAD InterfaceSDK.DLL------ \t\r\n")));
	   goto EXIT;
	}
	
	SetDisplayBrightness pfSetDisplayBrightness;
	//Encoder DLL WriteRelayBit function Handle
	pfSetDisplayBrightness= (SetDisplayBrightness)GetProcAddress(lHndlPWMSDKDll,L"SetDisplayBrightness");

	
	if(pfSetDisplayBrightness==NULL)
	{
		   RETAILMSG(1,(TEXT("-----Fail to Open pfSetDisplayBrightness Function Handle from PWMSDK.DLL------ \t\r\n")));
	}

	pfSetDisplayBrightness(percentage);

EXIT:
    return 0;
}
