typedef char (*SetGPIO)(DWORD dwGpioNo,BOOL bState);
typedef BOOL (*IsPortableBoard)(void);
typedef BOOL (*SetActivePartiton)(int);
typedef DWORD (*GetActivePartiton)(void);


#define BSP_LED1_GREEN			(164)
#define BSP_LED1_RED			(129)  //There is problem with this GPIO
#define BSP_LED2_GREEN			(103)
#define BSP_LED2_RED			(181)
#define BSP_RELAY_5V			(55)	/*GPIO 55 AUX 5V Relay Enable */
#define BSP_RELAY_12V			(54)	/*GPIO 94 AUX 12V Relay Enable */
