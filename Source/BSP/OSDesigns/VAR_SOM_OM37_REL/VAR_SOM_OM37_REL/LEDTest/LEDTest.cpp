// LEDTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "LedTest.h"


HINSTANCE lHndlInterfaceSDKDll;
//HANDLE lHndlInterfaceSDKDll;

int _tmain(int argc, TCHAR *argv[], TCHAR *envp[])
{
	DWORD dwGpio , i=0;
	DWORD dwActivePartition = 0;
    _tprintf(_T("Hello World!\n"));\


		//Check Application Parameters
	if(argc<2) //If there is no parameter, load default value
	{
		RETAILMSG(1, (L"No GPIO Number Specifiedr\n"));
		goto EXIT;
	}
	else if(argc>2 || wcscmp(argv[1],L"?") == 0)
	{
		RETAILMSG(1, (L"Usage:\r\n"));
		RETAILMSG(1, (L"Help: TestLED ?\r\n"));
		RETAILMSG(1, (L"TestLED <positive GPIO number >\r\n"));
		RETAILMSG(1, (L"i.e. TestLED 103 --> this will make 103 GPIO LED on and off.\r\n"));
		goto EXIT;
	}

	
	dwGpio = (DWORD)_wtoi(argv[1]);

	RETAILMSG(1,(TEXT("TEST APP GPIO[%d]\t\r\n"),dwGpio));
	
	RETAILMSG(1, (TEXT("\r\n +-------------------------------------------------------------------+")));
	RETAILMSG(1, (TEXT("\r\n |                    Running Tests                                  |")));
	RETAILMSG(1, (TEXT("\r\n +-------------------------------------------------------------------+\r\n \r\n ")));

	SetGPIO pfSetGPIO;
	IsPortableBoard pfIsPortableBoard;
	GetActivePartiton pfGetActivePartiton;
	SetActivePartiton pfSetActivePartiton;

	lHndlInterfaceSDKDll = LoadLibrary(L"InterfaceSDK.dll");
	if(lHndlInterfaceSDKDll==NULL)
    {
       RETAILMSG(1,(TEXT("-----Fail to LOAD InterfaceSDK.DLL------ \t\r\n")));
	   goto EXIT;
	}
	
	//Encoder DLL WriteRelayBit function Handle
	pfSetGPIO= (SetGPIO)GetProcAddress(lHndlInterfaceSDKDll,L"SetGPIO");
	pfIsPortableBoard= (IsPortableBoard)GetProcAddress(lHndlInterfaceSDKDll,L"IsPortableBoard");
	pfGetActivePartiton= (GetActivePartiton)GetProcAddress(lHndlInterfaceSDKDll,L"GetActivePartiton");
	pfSetActivePartiton= (SetActivePartiton)GetProcAddress(lHndlInterfaceSDKDll,L"SetActivePartiton");
	
	if((pfSetGPIO==NULL) || (pfIsPortableBoard==NULL))
	{
		   RETAILMSG(1,(TEXT("-----Fail to Open WriteLEDBit/WriteRelayBit Function Handle from EncoderSDK.DLL------ \t\r\n")));
	}


	if(pfIsPortableBoard())
	{
		RETAILMSG(1,(TEXT("TestApp: Board is Portable\t\r\n")));
	}else
		RETAILMSG(1,(TEXT("TestApp: Board is Fixed\t\r\n")));


	//dwGpio = BSP_LED2_RED ;
	while(i++<5)
	{
		pfSetGPIO(dwGpio,TRUE);
		Sleep(30*100);
		pfSetGPIO(dwGpio,FALSE);
		Sleep(30*100);
	}
	pfSetGPIO(dwGpio,TRUE);
	dwActivePartition = pfGetActivePartiton();
	RETAILMSG(1,(TEXT("TestApp: Active Boot Partition [0x%x]  \t\r\n"),dwActivePartition));
	
	pfSetActivePartiton(2);
	RETAILMSG(1,(TEXT("TestApp: Make Active Boot Partition to 2  \t\r\n")));
	
	dwActivePartition = pfGetActivePartiton();
	RETAILMSG(1,(TEXT("TestApp: Active Boot Partition [0x%x]  \t\r\n"),dwActivePartition));

	
    return 0;
EXIT:
	return -1;
}

