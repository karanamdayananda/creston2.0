// StartApp.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
typedef char (*SetDisplayBrightness)(int percentage);
typedef BOOL (*IsPortableBoard)(void);

#define APP_NAME				L"\\Storage Card\\Crestone\\Crestone.exe"

bool WinExec(LPCTSTR FileName)
{
	PROCESS_INFORMATION processInfo; //
    if (!CreateProcess(FileName, NULL, NULL, NULL, NULL
      , CREATE_NEW_CONSOLE
      , NULL, NULL, NULL, &processInfo))
    {
		OutputDebugString(L"ERROR: CreateProcess failed !");
		return false;
	}
    CloseHandle(processInfo.hThread);
	CloseHandle(processInfo.hProcess);
	OutputDebugString(L" SUCCESS!");
	return true ;
}

int _tmain(int argc, TCHAR *argv[], TCHAR *envp[])
{
	HINSTANCE lHndlPWMSDKDll;
	HINSTANCE lHndlInterfaceSDKDll;

	RETAILMSG(1,(TEXT("StartApp \r\n")));
	if(lHndlPWMSDKDll==NULL)
    {
       RETAILMSG(1,(TEXT("-----Fail to LOAD PWMSDK.DLL------ \t\r\n")));
	   goto EXIT;
	}
	lHndlInterfaceSDKDll = LoadLibrary(L"InterfaceSDK.dll");

	IsPortableBoard pfIsPortableBoard;
	pfIsPortableBoard= (IsPortableBoard)GetProcAddress(lHndlInterfaceSDKDll,L"IsPortableBoard");

	if(pfIsPortableBoard==NULL)
	{
		   RETAILMSG(1,(TEXT("-----Fail to Open pfIsPortableBoard Function Handle from InterfaceSDK.DLL------ \t\r\n")));
	}

	//Set Brightness ONLY if Board is Portable
	if(pfIsPortableBoard())
	{
		lHndlPWMSDKDll = LoadLibrary(L"PWMSDK.dll");
		SetDisplayBrightness pfSetDisplayBrightness;
		//PWM DLL SetDisplayBrightness function Handle
		pfSetDisplayBrightness= (SetDisplayBrightness)GetProcAddress(lHndlPWMSDKDll,L"SetDisplayBrightness");
		
		if(pfSetDisplayBrightness==NULL)
		{
			RETAILMSG(1,(TEXT("-----Fail to Open pfSetDisplayBrightness Function Handle from PWMSDK.DLL------ \t\r\n")));
			goto EXIT;
		}
		RETAILMSG(1,(TEXT("Set Default LCD brightness to 25%\t\r\n")));

		pfSetDisplayBrightness(25); //Set Default LCD brightness to 25%
		CloseHandle(lHndlPWMSDKDll);
	}		
   CloseHandle(lHndlInterfaceSDKDll);

	Sleep(3000);

	WinExec(APP_NAME);
 
EXIT:
    return 0;
}