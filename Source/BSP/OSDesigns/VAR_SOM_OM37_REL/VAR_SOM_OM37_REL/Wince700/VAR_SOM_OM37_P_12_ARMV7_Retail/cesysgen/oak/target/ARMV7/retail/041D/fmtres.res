        ��  ��                  �     �� ��     0         }       "   �  $   $   �  &   '   �  2   H   �  P   P   �  R   Y     d   r   t  u   �   �  �   �   �(  �   �   )  �   �   D)  �   �   h)  �   �   �)  �   �   *  �   �   l*  �   �   h-  �   �   �.  �   �   �.  �   �   �/  �   �   �0  �   �   �0      \1  
    �1      �1      �2        3  *  +  D3  =  =  �3  �  �   4      @4  �  �  5  �  �  l6    :  �>  L  l  \G  t  v  �P  ~  �   R  �  �  HW    :  �c  <  s  �n  x  �  ��  �  �  |�  �  �  X�  �  �  �  �  �  $�  �  �  �  �  (   �  j  ~  \�  �  �  (�  <  <  D�  �  �  ��  �  �  �  a	  b	  �  d	  d	  ��  �  �  ر  �  �  ش      ��  �  �  8�  �  �  D�     ��       ��     (�  0 0 ��  @ @ ��  p r ��  � � ,�  � � ��  � � �  � � H�  � � p�      �    �
  �X�  @ �!@ ���   � ���   ����  ������ �   � �,�   � �D�  �����  ����0�  ������  '��/����  ������D�  ň�ƈ�l�  ψ�ψ���  ��������  J��J����  ��������   � �0�   �	 �d�   � ���   � �(�   � �\�    �! ���  P �P � �  W �W �8�  p �p �X�  � �� ���  � �	���  ��d�  ����    � ���  d �m ���   ���  ����  @�@�|�  P�U���  `�a���  p�p���  �����  ����\�  ������  ����d�  ����   � �  � �L W �W �h  �	 ��  � �H  	�! 	�� 	�	�,  	� 	�t   	�# 	�`  0	� 0	��  � �L  ��P                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    �tg�rden utf�rdes.
       Ogiltig funktion.
 $   Det g�r inte att hitta filen.
 (   Det g�r inte att hitta s�kv�gen.
  $   Det g�r inte att �ppna filen.
    �tkomst nekad.
    (   Referensen (handle) �r ogiltig.
   0   Kontrollblocken f�r lagring f�rst�rdes.
   P   Det finns inte tillr�ckligt med ledigt minne f�r att utf�ra kommandot.
    <   Adressen f�r kontrollblock f�r lagring �r ogiltig.
       Ogiltig milj�.
    L   Det gjordes ett f�rs�k att l�sa in ett program
med ogiltigt format.
      �tkomstkoden �r ogiltig.
     Ogiltiga data.
    `   Det finns inte tillr�ckligt med tillg�ngligt utrymme f�r att slutf�ra den h�r �tg�rden.
   ,   Det g�r inte att hitta angiven enhet.
 ,   Det g�r inte att ta bort katalogen.
   <   Det g�r inte att flytta filen
till en annan enhet.
   $   Det finns inga fler filer.
        Mediet �r skrivskyddat.
   ,   Det g�r inte att hitta angiven enhet.
    Enheten �r inte klar.
 ,   Enheten k�nner inte igen kommandot.
   ,   Datafel (cyklisk �verskottskontroll).
 L   Programmet utf�rdade ett kommando men
kommandots l�ngd �r ogiltig.
   D   Det g�r inte att hitta ett omr�de
eller ett sp�r p� disken.
  <   Det g�r inte att komma �t angiven disk eller diskett.
 (   Det g�r inte hitta beg�rd sektor.
 $   Skrivaren har slut p� papper.
 4   Det g�r inte att skriva till angiven enhet.
   0   Det g�r inte att l�sa fr�n angiven enhet.
 <   En enhet som �r ansluten till datorn fungerar inte.
   T   Det g�r inte att komma �t filen eftersom den
anv�nds av en annan process.
    \   Det g�r inte att komma �t filen eftersom
en annan process har l�st en del av filen.
  T   Fel diskett sitter i enheten.
S�tt in %2 (Volymserienummer: %3)
i enhet %1.
 0   F�r m�nga filer �r �ppnade f�r att delas.
 $   Slutet av filen har n�tts.
       Disken �r full.
   $   N�tverksbeg�ran st�ds inte.
   (   Fj�rrdatorn �r inte tillg�nglig.
  0   Det finns ett dubblettnamn i n�tverket.
   (   N�tverkss�kv�gen kan inte hittas.
     N�tverket �r upptaget.
    P   Angiven n�tverksresurs eller n�tverksenhet �r inte l�ngre tillg�nglig.
    8   Gr�nsen f�r n�tverks-BIOS-kommandon har uppn�tts.
 <   Det intr�ffade ett maskinvarufel i n�tverkskortet.
    @   Det g�r inte att utf�ra beg�rd �tg�rd p� angiven server.
  ,   Ett ov�ntat n�tverksfel har uppst�tt.
 (   Fj�rrkortet �r inte kompatibelt.
     Utskriftsk�n �r full.
 P   Det finns inte plats p� servern f�r att lagra filen i v�ntan p� utskrift.
 4   Filen som skulle skrivas ut har tagits bort.
  8   Angivet n�tverksnamn �r inte l�ngre tillg�ngligt.
     N�tverks�tkomst nekad.
    (   Felaktig typ av n�tverksresurs.
   0   Det g�r inte att hitta n�tverksnamnet.
    L   Namngr�nsen f�r n�tverkskortet p� den lokala datorn har �verskridits.
 <   Sessionsgr�nsen f�r n�tverks-BIOS har �verskridits.
   L   Fj�rrservern �r tillf�lligt fr�nkopplad eller h�ller p� att startas.
  p   Du kan inte skapa fler anslutningar till fj�rrdatorn, eftersom den redan har maximalt antal anslutningar.
 D   Det har gjorts en paus p� angiven skrivare eller diskenhet.
      Filen finns.
  4   Det g�r inte att skapa katalogen eller filen.
     Misslyckades p� INT 24.
   <   Det finns inte utrymme att behandla denna beg�ran.
    0   Det lokala enhetsnamnet anv�nds redan.
    ,   Felaktigt n�tverksl�senord angivet.
      Felaktig parameter.
   0   Det intr�ffade ett skrivfel i n�tverket.
  8   Det g�r inte att starta en process till just nu.
  <   Det g�r inte att skapa ytterligare en systemsemafor.
  8   Den exklusiva semaforen �gs av en annan process.
  4   Semaforen har angetts och kan inte st�ngas.
   (   Semaforen kan inte anges igen.
    @   Det g�r inte att beg�ra exklusiva semaforer vid avbrott.
  (   Den h�r semaforen �r nu ledig.
    (   S�tt in disketten f�r enhet %1.
   P   Programmet har avbrutits p� grund av att en annan diskett inte sattes in.
 @   Disken �r l�st eller s� anv�nds den av en annan process.
      Denna pipe har avslutats.
 8   Det g�r inte att �ppna angiven enhet eller fil.
       Filnamnet �r f�r l�ngt.
   <   Det finns inte tillr�ckligt med utrymme p� disken.
    D   Det finns inga fler interna filidentifierare tillg�ngliga.
    ,   Felaktig intern m�lfilsidentifierare.
 0   Felaktigt IOCTL-anrop fr�n programmet.
    <   V�rdet f�r parametern verify-on-write �r felaktigt.
   $   Beg�rt kommando st�ds inte.
   @   Den h�r funktionen �r tillg�nglig endast i Win32-l�ge.
    0   Tidsgr�nsen f�r semaforen har uppn�tts.
   H   Dataf�ltet som har skickats till ett systemanrop �r f�r litet.
    D   Felaktig syntax f�r filnamn, katalognamn eller volymetikett.
  (   Felaktig niv� f�r systemanrop.
    $   Disken saknar volymetikett.
   ,   Det g�r inte att hitta angiven modul.
 0   Det g�r inte att hitta angiven procedur.
  4   Det finns inga underprocesser att v�nta p�.
   4   Programmet %1 kan inte k�ras i Win32-l�ge.
    �   Det har gjorts ett f�rs�k att anv�nda en filreferens p� en �ppen
diskpartition f�r en annan �tg�rd �n renodlad disk-I/O.
 \   Det har gjorts ett f�rs�k att flytta filpekaren till en position f�re filens b�rjan.
  D   Filpekaren kan inte st�llas in p� angiven enhet eller fil.
    p   Ett JOIN- eller SUBST-kommando
kan inte anv�ndas f�r en enhet
som inneh�ller redan kopplade enheter.
    h   Det har gjorts ett f�rs�k att anv�nda
ett JOIN- eller SUBST-kommando p�
en redan kopplad enhet.
 h   Det har gjorts ett f�rs�k att anv�nda
ett JOIN- eller SUBST-kommando p�
en redan ersatt enhet.
  \   Det har gjorts f�rs�k att ta bort kopplingen (JOIN)
p� en enhet som inte �r kopplad.
 T   Operativsystemet f�rs�kte ta bort
SUBST fr�n en enhet som inte �r ersatt.
    X   Det har gjorts f�rs�k att koppla en enhet
till en katalog p� en kopplad enhet.
   X   Det har gjorts f�rs�k att ers�tta en
enhet till en katalog p� en ersatt enhet.
   X   Det har gjorts f�rs�k att koppla en enhet till
en katalog p� en ersatt enhet.
    `   Det har gjorts f�rs�k att ers�tta (SUBST) en
enhet till en katalog p� en kopplad enhet.
  H   Det g�r inte att utf�ra ett JOIN- eller SUBST-kommando just nu.
   `   Det g�r inte att koppla eller ers�tta en enhet
till eller f�r en katalog p� samma enhet.
 <   Katalogen �r inte en underkatalog till rotkatalogen.
      Katalogen �r inte tom.
    8   Angiven s�kv�g anv�nds av en
ers�ttning (SUBST).
 P   Det finns inte tillr�ckligt med resurser
f�r att utf�ra detta kommando.
  8   Den angivna s�kv�gen kan inte anv�ndas just nu.
   �   Det har gjorts ett f�rs�k att koppla
eller ers�tta en enhet som har en
katalog p� enheten som m�l f�r en
tidigare ers�ttning.
  p   Information f�r systemsp�rning angavs inte i
filen CONFIG.SYS, eller s� �r sp�rning inte tillg�nglig.
    D   Felaktigt antal angivna semaforh�ndelser
f�r DosMuxSemWait.
  D   DosMuxSemWait utf�rdes inte, f�r m�nga
semaforer �r angivna.
 $   Felaktig DosMuxSemWait-lista.
 T   Angiven volymetikett inneh�ller fler tecken �n
till�tet i m�lfilssystemet.
   ,   Det gick inte att skapa en tr�d till.
 <   Mottagningsprocessen har inte tagit emot signalen.
    <   Segmentet har redan ignorerats och kan inte l�sas.
    $   Segmentet �r redan uppl�st.
   $   Felaktig adress f�r tr�d-ID.
  D   Argumentstr�ngen som skickades till DosExecPgm �r felaktig.
   (   Den angivna s�kv�gen �r ogiltig.
  ,   Det finns redan en v�ntande signal.
   8   Det g�r inte att skapa fler tr�dar i systemet.
    0   Det g�r inte att l�sa en del av en fil.
   $   Beg�rd resurs anv�nds redan.
  L   Det fanns ingen uteliggande l�sbeg�ran f�r angivet avbrottsomr�de.
    8   Filsystemet st�der inte �ndringar av l�stypen.
    (   Felaktigt segmentnummer uppt�ckt.
     Det g�r inte att k�ra %1.
 8   Det g�r inte att skapa en fil som redan finns.
    ,   Flaggan som �verf�rdes �r felaktig.
   8   Det g�r inte att hitta angivet systemsemafornamn.
     Det g�r inte att k�ra %1.
     Det g�r inte att k�ra %1.
     Det g�r inte att k�ra %1.
 0   Det g�r inte att k�ra %1 i Win32-l�ge.
        Det g�r inte att k�ra %1.
 ,   %1 �r inte ett giltigt Win32-program.
     Det g�r inte att k�ra %1.
     Det g�r inte att k�ra %1.
 0   Det g�r inte att k�ra det h�r programmet.
 T   Operativsystemet �r inte konfigurerat f�r
att kunna k�ra det h�r programmet.
     Det g�r inte att k�ra %1.
 0   Det g�r inte att k�ra det h�r programmet.
 0   Kodsegmentet m�ste vara mindre �n 64 kB.
      Det g�r inte att k�ra %1.
     Det g�r inte att k�ra %1.
 8   Det g�r inte att hitta angivet
milj�alternativ.
  D   Ingen process i kommandoundertr�det
har en signalhanterare.
  4   Filnamnet eller filtill�gget �r f�r l�ngt.
        Ring 2-stacken anv�nds.
   `   Antingen har f�r m�nga jokertecken (* och ?) angetts
eller s� har de anv�nts felaktigt.
  (   Den postade signalen �r felaktig.
 0   Det g�r inte att ange signalhanteraren.
   4   Segmentet �r l�st och kan inte allokeras om.
  p   Det �r f�r m�nga DLL-moduler (dynamic link modules)
kopplade till det h�r programmet eller DLL-modulen.
  8   Det g�r inte att n�stla anrop till LoadModule.
    \   Avbildningsfilen %1 �r giltig men den kan
inte anv�ndas p� den aktuella datortypen.
  0   Tillst�ndet f�r denna pipe �r ogiltigt.
       Alla pipes �r upptagna.
   (   Denna pipe h�ller p� att st�ngas.
 D   Det finns inte n�gon process p� andra sidan av denna pipe.
    (   Det finns mera data tillg�ngliga.
     Sessionen har avbrutits.
  8   Angivet namn f�r ut�kat attribut �r felaktigt.
    0   De ut�kade attributen �r inkonsekventa.
   0   Det finns inte mera data tillg�ngliga.
    0   Kopieringsfunktionerna kan inte anv�ndas.
 $   Katalognamnet �r ogiltigt.
    8   De ut�kade attributen f�r inte plats i bufferten.
 D   Filen med ut�kade attribut p� anslutet filsystem �r skadad.
   0   Tabellfilen f�r ut�kade attribut �r full.
 D   Angiven referens (handle) f�r ut�kat attribut �r felaktig.
    8   Anslutet filsystem st�der inte ut�kade attribut.
  <   Anroparen f�rs�kte sl�ppa en mutex som den inte �gde.
    Semaforfel.
   H   Endast delar av en Read/WriteProcessMemory-beg�ran har utf�rts.
   `   Det gick inte att hitta meddelandetexten f�r meddelande 0x%1
i meddelandefilen f�r %2.
   @   Det har gjorts ett f�rs�k att anv�nda en felaktig adress.
 H   Resultatet fr�n den aritmetiska �tg�rden har fler �n 32 bitar.
    <   Det finns en process p� andra sidan av denna pipe.
    D   V�ntar p� att en process ska �ppna andra sidan av denna pipe.
 4   �tkomst nekades till det ut�kade attributet.
  h   I/O-�tg�rden har avbrutits d�rf�r att en tr�d har avslutats
eller ett program har beg�rt det.
    H   En �verlappad I/O-h�ndelse �r inte i ett signalerat tillst�nd.
    $   �verlappad I/O-�tg�rd p�g�r.
  $   Felaktig �tkomst till minnet.
 8   Ett fel intr�ffade n�r inpage-�tg�rd utf�rdes.
    P   Det uppstod dataspill p� stacken p� grund av att rekursionen �r f�r djup.
 <   F�nstret kan inte hantera meddelandet som skickades.
  ,   Det g�r inte att slutf�ra funktionen.
    Ogiltiga flaggor.
 �   Volymen inneh�ller ett ok�nt filsystem. Kontrollera att
alla n�dv�ndiga filsystemsdrivrutiner har l�sts in
och att volymen inte �r skadad.
  h   Filen som �r �ppen �r inte l�ngre giltig p� grund
av att volymen f�r en fil har �ndrats externt.
 8   Beg�rd �tg�rd kan inte utf�ras i helsk�rmsl�ge.
   L   Det har gjorts ett f�rs�k att referera till ett token som inte finns.
    Registret �r skadat.
  8   Registernyckeln f�r konfigurationen �r ogiltig.
   <   Registernyckeln f�r konfigurationen kan inte �ppnas.
  <   Registernyckeln f�r konfigurationen kan inte l�sas.
   <   Registernyckeln f�r konfigurationen kan inte skrivas.
 l   En av filerna i registerdatabasen beh�vde
�terskapas fr�n en kopia eller en logg. �tg�rden utf�rdes.
   Registret �r skadat. Skadan kan bero p� att filstrukturen f�r en av filerna
som inneh�ller registerdata �r skadad, systemets avbildning av filen i
minnet �r skadad, eller p� att det inte gick att �terst�lla filen p� grund
av att kopian eller loggen saknas eller �r skadad.
   �   Det uppstod ett o�terkalleligt fel i en I/O-�tg�rd initierad av registret.
Registret kan inte l�sa, skriva eller t�mma en av filerna som
inneh�ller operativsystemets minnesavbildning av Registret.
    t   Det gjordes ett f�rs�k att l�sa in eller �terskapa en fil i Registret,
men filen har inte korrekt format.
    t   Ett f�rs�k att utf�ra en otill�ten �tg�rd p� en registernyckel som har markerats f�r borttagning har gjorts.
  L   Det g�r inte att allokera det utrymme som kr�vs i en registerlogg.
    p   Det g�r inte att skapa en symbolisk l�nk i en registernyckel
som redan har undernycklar eller v�rden.
    L   Det g�r inte att skapa en stabil undernyckel till en tempor�r nyckel.
 �   En beg�ran om meddelande vid �ndring h�ller p� att
f�rdigst�llas och informationen returneras inte till anroparens buffert.
Anroparen m�ste numrera filerna f�r att hitta �ndringarna.
  d   En stoppinstruktion har skickats till en tj�nst
som andra aktiva tj�nster �r beroende av.
    H   Den beg�rda instruktionen �r inte giltig f�r den h�r tj�nsten.
    D   Tj�nsten svarade inte p� start- eller kontrollbeg�ran i tid.
  <   Det gick inte att skapa en tr�d f�r den h�r tj�nsten.
     Tj�nstdatabasen �r l�st.
  4   Det finns redan en aktiv instans av tj�nsten.
 4   Kontonamnet finns inte eller �r felaktigt.
    8   Angiven tj�nst �r sp�rrad och kan inte startas.
   ,   Cirkul�rt tj�nstberoende har angetts.
 ,   Angiven tj�nst �r inte installerad.
   L   Angiven tj�nst kan f�r tillf�llet inte ta emot kontrollmeddelanden.
   $   Tj�nsten har inte startats.
   D   Tj�nstprocessen kunde inte ansluta till tj�nstkontrollanten.
  H   Ett undantag uppstod i tj�nsten n�r kontrollbeg�ran behandlades.
  $   Angiven databas finns inte.
   8   Tj�nsten returnerade en tj�nstspecifik felkod.
    $   Processen avslutades ov�ntat.
 H   Det g�r inte att starta den �verordnade tj�nsten eller gruppen.
   @   Tj�nsten startades inte p� grund av ett inloggningsfel.
   D   Sedan tj�nsten startats l�ste den sig i l�get start v�ntar.
   4   Angivet l�s f�r tj�nstdatabas �r ogiltigt.
    4   Angiven tj�nst �r markerad f�r borttagning.
   $   Angiven tj�nst finns redan.
   <   Systemet k�rs med senast fungerande konfiguration.
    P   Den �verordnade tj�nsten finns inte eller �r markerad f�r
borttagning.
   l   Aktuell systemstart har redan godk�nts som kontrollupps�ttning
f�r senast fungerande konfiguration.
  X   Det har inte gjorts n�gra f�rs�k att starta tj�nsten sedan den senaste starten.
   X   Namnet anv�nds redan som ett tj�nstnamn eller som
visningsnamn f�r en tj�nst.
    �   Kontot som har angetts f�r den h�r tj�nsten skiljer sig fr�n kontot
som har angetts f�r andra tj�nster som k�rs i samma process.
 \   �tg�rder vid misslyckande kan bara anges f�r Win32-tj�nster, inte f�r drivrutiner.
    �   Den h�r tj�nsten k�rs i samma process som tj�nstehanteraren.
Tj�nstehanteraren kan inte forts�tta om processen f�r den h�r
tj�nsten ov�ntat avbryts.
    L   Inget �terst�llningsprogram har konfigurerats f�r den h�r tj�nsten.
   ,   Det fysiska slutet av bandet �r n�tt.
 0   Vid band�tg�rd n�ddes en filmarkering.
    <   B�rjan av bandet eller partitionen har p�tr�ffats.
    @   Vid en band�tg�rd n�ddes slutet p� en upps�ttning filer.
  ,   Det finns inte mera data p� bandet.
   0   Det gick inte att partitionera bandet.
    t   Det g�r inte att anv�nda aktuell blockstorlek vid �tg�rder
p� nya band som ing�r i en flervolymspartition.
   L   Det gick inte att hitta partitionsinformationen n�r bandet l�stes.
    <   Det g�r inte att l�sa mekanismen f�r mediautmatning.
  (   Det g�r inte att ta bort mediet.
  (   Mediet i enheten kan ha �ndrats.
      I/O-bussen �terst�lldes.
  0   Det finns inte n�got medium i enheten.
    L   Det finns ingen avbildning f�r Unicode-tecken i m�lteckentabellen.
    0   Initieringen av en DLL-fil misslyckades.
      Systemavst�ngning p�g�r.
  X   Det g�r inte att avbryta systemavst�ngningen, eftersom n�gon s�dan inte p�g�r.
    H   Beg�ran kunde inte utf�ras p� grund av ett fel i en I/O-enhet.
    l   Det gick inte att initiera n�gon seriell enhet. Drivrutinen f�r seriella enheter kommer att tas bort.
 �   Det g�r inte att �ppna en enhet som delar ett avbrottsnummer (IRQ)
med andra enheter. Minst en annan enhet som anv�nder
samma avbrott �r redan �ppnad.
  �   En seriell I/O-�tg�rd fullf�ljdes av en annan skrivning till den seriella porten.
(IOCTL_SERIAL_XOFF_COUNTER n�dde noll.)
    x   En seriell I/O-�tg�rd fullf�ljdes eftersom tidsgr�nsen hade n�tts.
(IOCTL_SERIAL_XOFF_COUNTER n�dde inte noll.)
  @   Det g�r inte att hitta n�got ID-adressm�rke p� disketten.
 h   Diskettens f�lt f�r sektor-ID och diskettenhetens
sp�radress f�r styrenhet st�mmer inte �verens.
 p   Styrenheten f�r diskettenheten rapporterade ett fel som inte
k�nns igen av diskettenhetens drivrutin.
    X   Styrenheten f�r diskettenheten returnerade mots�gande resultat i sitt register.
   `   Vid �tkomst av h�rddisken misslyckades en omkalibrerings�tg�rd trots upprepade f�rs�k.
    T   Vid �tkomst av h�rddisken misslyckades en disk�tg�rd trots upprepade f�rs�k.
  d   Vid �tkomst av h�rddisken kr�vdes �terst�llning av styrenheten,
men �tg�rden misslyckades.
       Fysiskt bandslut n�ddes.
  X   Det finns inte tillr�ckligt med utrymme p� servern f�r att utf�ra detta kommando.
 0   Ett m�jligt d�dl�ge har identifierats.
    D   Basadressen eller filf�rflyttningen har inte ordnats korrekt.
 l   Ett f�rs�k att �ndra systemets energisparl�ge avbr�ts av
ett annat program eller en annan drivrutin.
 T   Ett f�rs�k av BIOS-systemet att �ndra systemets energisparl�ge misslyckades.
  \   Ett f�rs�k har gjorts att skapa fler l�nkar
p� en fil �n vad filsystemet till�ter.
   <   Programmet m�ste k�ras i en nyare version av Windows.
 <   Programmet �r inte ett Windows- eller MS-DOS-program.
 8   Det g�r bara att starta en instans av programmet.
 T   Programmet som har angetts har skrivits f�r en tidigare version av Windows.
   @   En DLL-fil som beh�vs f�r att k�ra programmet �r skadad.
  H   Inget program �r associerat med angiven fil f�r den h�r �tg�rden.
 D   Det uppstod ett fel n�r kommandot skickades till programmet.
  L   Det g�r inte att hitta en DLL-fil som beh�vs f�r att k�ra programmet.
 |   Den aktuella processen har anv�nt alla de referenser (handles) som systemet tilldelat den f�r Window Manager-objekt.
  @   Meddelandet kan endast anv�ndas f�r synkrona �tg�rder.
    0   Det angivna k�llelementet har inga media.
 8   Det angivna m�lelemetet inneh�ller redan media.
   (   Det angivna elementet finns inte.
 P   Det angivna elementet �r en del av ett magasin som inte �r n�rvarande.
    H   Den angivna enheten m�ste ominitieras p� grund av maskinvarufel.
  H   Enheten m�ste reng�ras innan ytterligare �tg�rder kan utf�ras.
        Enhetens d�rr �r �ppen.
       Enheten �r inte ansluten.
 ,   Det gick inte att hitta elementet.
    L   Det gick inte att hitta en matchning f�r angiven nyckel i indexet.
    H   Den angivna upps�ttningen med egenskaper finns inte p� objektet.
  (   Angivet enhetsnamn �r ogiltigt.
   L   Enheten �r f�r n�rvarande inte ansluten men anslutningen �r sparad.
   H   Ett f�rs�k gjordes att spara en anslutning som redan �r sparad.
   D   Angiven s�kv�g accepterades inte av n�gon n�tverks-provider.
  8   Angivet namn p� n�tverks-provider �r ogiltigt.
    @   Det g�r inte att �ppna profilen med n�tverksanslutningar.
 4   Profilen f�r n�tverksanslutningen �r skadad.
  D   Det g�r inte att r�kna upp n�got som inte �r en beh�llare.
    (   Det har uppst�tt ett ut�kat fel.
  0   Angivet gruppnamn har ogiltigt format.
    0   Angivet datornamn har ogiltigt format.
    0   Angivet h�ndelsenamn har ogiltigt format.
 0   Angivet dom�nnamn har ogiltigt format.
    0   Angivet tj�nstnamn har ogiltigt format.
   0   Angivet n�tverksnamn har ogiltigt format.
 0   Angivet resursnamn har ogiltigt format.
   ,   Angivet l�senord har ogiltigt format.
 4   Angivet meddelandenamn har ogiltigt format.
   8   Angivet m�l f�r meddelande har ogiltigt format.
   P   Angivna referenser orsakar konflikt med en befintlig referensupps�ttning.
 �   Det gjordes ett f�rs�k att skapa en session med en n�tverksserver, men
det finns redan maximalt antal etablerade sessioner mot servern.
  T   Arbetsgrupps- eller dom�nnamnet anv�nds redan av
en annan dator i n�tverket.
 L   N�tverket �r antingen inte tillg�ngligt eller s� �r det inte startat.
 ,   �tg�rden har avbrutits av anv�ndaren.
 d   Den beg�rda �tg�rden g�r inte att utf�ra p� en fil med en anv�ndar-avbildad sektion �ppen.
    @   N�tverksanslutningen accepterades inte av fj�rrsystemet.
  (   N�tverksanslutningen har st�ngts.
 L   Slutpunkten f�r n�tverkstransporten har redan en associerad adress.
   D   En adress har �nnu inte associerats med n�tverksslutpunkten.
  \   Ett f�rs�k att utf�ra en �tg�rd p� en n�tverksanslutning som inte finns har gjorts.
   `   Det har gjorts ett f�rs�k att utf�ra en ogiltig �tg�rd p� en aktiv n�tverksanslutning.
    <   Fj�rrn�tverket �r inte tillg�ngligt f�r transporten.
  <   Fj�rrsystemet �r inte tillg�ngligt f�r transporten.
   8   Fj�rrsystemet st�der inte transportprotokollet.
   T   Det finns ingen tj�nst ig�ng vid m�ln�tverkets
slutpunkt p� fj�rrsystemet.
      Beg�ran avbr�ts.
  <   N�tverksanslutningen avbr�ts av det lokala systemet.
  4   �tg�rden kunde inte fullf�ljas. F�rs�k igen.
  �   Det gick inte att skapa en anslutning till servern eftersom maximalt antal
samtidiga anslutningar f�r detta konto redan har gjorts.
  X   F�rs�ker att logga in vid en tidpunkt n�r beh�righet saknas f�r det h�r kontot.
   H   Kontot har inte r�ttigheter att logga in fr�n den h�r stationen.
  <   N�tverksadressen kan inte anv�ndas f�r beg�rd �tg�rd.
 (   Tj�nsten har redan registrerats.
  $   Angiven tj�nst finns inte.
    L   �tg�rden utf�rdes inte eftersom anv�ndaren inte
har autentiserats.
   h   �tg�rden utf�rdes inte eftersom anv�ndaren inte
har loggat in i n�tverket.
Tj�nsten finns inte.
 $   Forts�tt med p�g�ende arbete.
 d   Det har gjorts ett f�rs�k att utf�ra en initierings�tg�rd
n�r initiering redan har utf�rts.
  ,   Det finns inga fler lokala enheter.
   D   Anroparen har inte f�tt alla beh�righeter som refererades.
    L   Vissa avbildningar mellan kontonamn och s�kerhets-ID utf�rdes inte.
   8   Inga systemkvoter har angetts f�r detta konto.
    `   Det finns ingen krypteringsnyckel tillg�nglig. En v�lk�nd krypteringsnyckel returnerades.
 �   NT-l�senordet �r f�r avancerat f�r att konverteras till ett
LAN Manager-l�senord. Returnerat LAN Manager-l�senord �r en tom str�ng.
      Revisionsniv�n �r ok�nd.
  8   Anger att tv� revisionsniv�er inte �r kompatibla.
 H   Detta s�kerhets-ID kan inte anges som �gare till detta objekt.
    P   Detta s�kerhets-ID kan inte anges som den prim�ra gruppen i ett objekt.
   �   Det har gjorts ett f�rs�k att utf�ra en �tg�rd p� ett personifierings-token
via en tr�d som f�r n�rvarande inte personifierar en klient.
     Gruppen kan inte sp�rras.
 t   Det finns f�r tillf�llet inte n�gra inloggningsservrar tillg�ngliga
som kan hantera inloggningsf�rfr�gan.
    X    Den angivna inloggningssessionen finns inte. Den kanske
redan har avslutats.
    ,    Den angivna r�ttigheten finns inte.
  4    Klienten har inte den angivna r�ttigheten.
   0   Angivet kontonamn har felaktigt format.
   (   Angiven anv�ndare finns redan.
    $   Angiven anv�ndare finns inte.
 $   Angiven grupp finns redan.
        Angiven grupp finns inte.
 �   Angivet anv�ndarkonto �r redan medlem i den aktuella gruppen,
eller s� kan inte gruppen tas bort d�rf�r att den inneh�ller
en medlem.
   D   Angivet anv�ndarkonto �r inte medlem i aktuellt gruppkonto.
   H   Det sista administrat�rskontot kan inte sp�rras
eller tas bort.
  X   Det g�r inte att uppdatera l�senordet. Befintligt l�senord angavs
inte korrekt.
  t   Det g�r inte att uppdatera l�senordet. Det nya l�senordet inneh�ller
tecken som inte �r till�tna i l�senord.
 h   Det g�r inte att uppdatera l�senordet d�rf�r att en uppdateringsregel
f�r l�senord har brutits.
  D   Inloggningsfel: Ok�nt anv�ndarnamn eller felaktigt l�senord.
  4   Inloggningsfel: Begr�nsning i anv�ndarkontot.
 P   Inloggningsfel: Inloggningstiderna f�r kontot till�ter inte inloggning.
   H   Inloggningsfel: Anv�ndaren f�r inte logga in p� den h�r datorn.
   D   Inloggningsfel: L�senordet f�r kontot har upph�rt att g�lla.
  8   Inloggningsfel: Kontot �r f�r tillf�llet sp�rrat.
 P   Det har inte gjorts n�gon avbildning mellan kontonamn och s�kerhets-ID.
   H   F�r m�nga lokala anv�ndaridentifierare (LUID) beg�rdes samtidigt.
 L   Det finns inte fler lokala anv�ndaridentifierare (LUID) tillg�ngliga.
 `   Den underliggande verifieringsdelen av detta s�kerhets-ID �r ogiltig f�r denna �tg�rd.
    8   ACL (Access Control List) har ogiltig struktur.
   0   Detta s�kerhets-ID har ogiltig struktur.
  4   S�kerhetsbeskrivningen har ogiltig struktur.
  d   Den �rvda ACL (Access Control List) eller ACE (Access Control Entry)
gick inte att bygga.
    0   Servern �r f�r tillf�llet inaktiverad.
    ,   Servern �r f�r tillf�llet aktiverad.
  T   Angivet v�rde �r inte ett till�tet v�rde f�r en verifieringsidentifierare.
    X   Det finns inte mer minne tillg�ngligt f�r uppdateringar av s�kerhetsinformation.
  h   De angivna attributen �r ogiltiga eller inte kompatibla med
attributen f�r gruppen som helhet.
   h   En n�dv�ndig personifieringsniv� angavs inte,
eller s� �r angiven personifieringsniv� ogiltig.
   8   Det g�r inte att �ppna anonyma s�kerhets-token.
   <   Beg�rd klass av verifieringsinformation �r ogiltig.
   <   Detta token �r inte av r�tt typ f�r t�nkt anv�ndning.
 t   Det g�r inte att utf�ra en s�kerhets�tg�rd p� ett objekt
som inte har n�gra s�kerhetsfunktioner associerade.
 �   Indikerar att en Windows NT-server inte kan anropas eller att
objekt inom dom�nen skyddas s� att n�dv�ndig
information inte kan h�mtas.
 �   Hanteraren f�r kontos�kerhet (SAM) eller servern
f�r lokal s�kerhetskontroll (LSA) befann sig i fel l�ge
f�r att utf�ra s�kerhets�tg�rden.
  @   Dom�nen var i fel l�ge f�r att utf�ra s�kerhets�tg�rden.
  T   Den beg�rda �tg�rden kan endast utf�ras av den prim�ra dom�nkontrollanten.
        Angiven dom�n finns inte.
 $   Angiven dom�n finns redan.
    T   Det har gjorts ett f�rs�k att �verskrida antalet till�tna dom�ner per server.
 |   Det g�r inte att utf�ra beg�rd �tg�rd p� grund av
ett o�terkalleligt mediafel eller skadad datastruktur p� disken.
   H   Databasen f�r kontos�kerhet inneh�ller en intern inkonsekvens.
    p   Generiska �tkomsttyper fanns i en �tkomstmask som
redan borde ha avbildats till icke-generiska typer.
    T   En s�kerhetsbeskrivning har inte r�tt format (absolut eller sj�lvrelativ).
    �   Beg�rd �tg�rd �r endast tillg�nglig f�r inloggningsprocesser.
Den anropande processen �r inte registrerad som en inloggningsprocess.
 X   Det g�r inte att starta en ny inloggningssession med ett ID som redan anv�nds.
    0   Ett angivet autentiseringspaket �r ok�nt.
 L   Inloggningssessionen �r i ett l�ge som inte till�ter
beg�rd �tg�rd.
  4   Detta inloggningssessions-ID anv�nds redan.
   L   En inloggningsbeg�ran inneh�ll ett ogiltigt v�rde f�r inloggningstyp.
 d   Det g�r inte att personifiera via en namngiven pipe f�rr�n
data har l�sts fr�n denna pipe.
   d   �verf�ringstillst�ndet f�r ett deltr�d i Registret �r inte kompatibelt
med beg�rd �tg�rd.
    0   Den interna s�kerhetsdatabasen �r skadad.
 H   Det g�r inte att utf�ra denna �tg�rd p� f�rdefinierade konton.
    P   Det g�r inte att utf�ra �tg�rden p� denna f�rdefinierade specialgrupp.
    T   Det g�r inte att utf�ra �tg�rden p� denna f�rdefinierade specialanv�ndare.
    �   Det g�r inte att ta bort anv�ndaren fr�n en grupp d�rf�r att denna grupp
f�r n�rvarande �r anv�ndarens prim�ra grupp.
    4   Detta token anv�nds redan som prim�rt token.
  (   Angiven lokal grupp finns inte.
   @   Angivet kontonamn �r inte medlem i den lokala gruppen.
    @   Angivet kontonamn �r redan medlem i den lokala gruppen.
   (   Angiven lokal grupp finns redan.
  `   Inloggningsfel: Anv�ndaren har inte beviljats beg�rd
inloggningstyp p� den h�r datorn.
   h   Det maximala antalet till�tna hemligheter som kan lagras i
ett enskilt system har �verskridits.
  ,   En hemlighet �r l�ngre �n till�tet.
   8   LSA-databasen inneh�ller en intern inkonsekvens.
  h   Vid ett inloggningsf�rs�k samlade anv�ndarens s�kerhetsomgivning p� sig
f�r m�nga s�kerhets-ID.
  h   Inloggningsfel: Anv�ndaren har inte beviljats den beg�rda
inloggningstypen p� den h�r datorn.
    L   Ett korskrypterat l�senord kr�vs f�r att �ndra ett anv�ndarl�senord.
  h   Det g�r inte att l�gga till en ny medlem i den lokala gruppen d�rf�r att
medlemmen inte finns.
   l   Det g�r inte att l�gga till en ny medlem i den lokala gruppen d�rf�r att
medlemmen har fel kontotyp.
 ,   F�r m�nga s�kerhets-ID har angetts.
   L   Ett korskrypterat l�senord kr�vs f�r att �ndra ett anv�ndarl�senord.
  d   Anger att en ACL (Access Control List) inte inneh�ller n�gra komponenter som g�r att �rva.
    4   Filen eller katalogen �r skadad och ol�sbar.
  ,   Diskstrukturen �r skadad och ol�sbar.
 P   Det finns ingen anv�ndarsessionsnyckel f�r angiven inloggningssession.
    �   Tj�nsten �r endast licensierad f�r ett visst antal anslutningar.
Det g�r inte att g�ra fler anslutningar till den h�r tj�nsten nu
eftersom till�tet antal anslutningar redan finns.
 (   Ogiltig f�nsterreferens (handle).
 (   Ogiltig menyreferens (handle).
    (   Ogiltig mark�rreferens (handle).
  D   Ogiltig referens (handle) till tabellen �ver kortkommandon.
   (   Ogiltig hook-referens (handle).
   P   Ogiltig referens (handle) till en positionsstruktur f�r flera f�nster.
    @   Det g�r inte att skapa ett underf�nster p� �versta niv�n.
 0   Det g�r inte att hitta f�nsterklassen.
    @   F�nstret �r ogiltigt eftersom det tillh�r en annan tr�d.
  ,   Snabbtangenten �r redan registrerad.
     Klassen finns redan.
     Klassen finns inte.
   0   Klassen har fortfarande f�nster �ppna.
       Ogiltigt index.
   (   Ogiltig ikonreferens (handle).
    0   Anv�nder privata ord f�r DIALOG-f�nster.
  8   det gick inte att hitta listrutans identifierare.
 0   Det g�r inte att hitta n�gra jokertecken.
 ,   Tr�den har inte �ppnat n�got urklipp.
 ,   Snabbtangenten �r inte registrerad.
   4   F�nstret �r inte ett giltigt dialogf�nster.
   0   Det g�r inte att hitta n�got kontroll-ID.
 d   Ogiltig meddelande till en kombinationsruta eftersom den inte har n�gon redigeringskontroll.
  0   F�nstret �r inte n�gon kombinationsruta.
  (   H�jden m�ste vara l�gre �n 256.
   4   Ogiltig DC-referens (Device Context handle).
      Ogiltig hook-procedurtyp.
     Ogiltig hook-procedur.
    P   Det g�r inte att ange en icke-lokal hook utan en modulreferens (handle).
  8   Denna hook-procedur kan endast s�ttas globalt.
    8   Journalens hook-procedur �r redan installerad.
    ,   Hook-proceduren �r inte installerad.
  <   Ogiltigt meddelande f�r listruta med endast ett val.
  8   LB_SETCOUNT skickades till en icke-lat listruta.
  ,   Denna listruta st�der inte tabbstopp.
 H   Det g�r inte att f�rst�ra objekt som skapats av en annan tr�d.
    (   Underf�nster kan inte ha menyer.
  (   F�nstret har ingen systemmeny.
    ,   Ogiltigt format p� meddelanderuta.
    (   Ogiltig systemparameter (SPI_*).
      Sk�rmen �r redan l�st.
    T   Alla f�nsterreferenser i en flerf�nsterstruktur m�ste
ha samma basf�nster.
   ,   F�nstret �r inte ett underf�nster.
        Ogiltigt GW_*-kommando.
   $   Ogiltig tr�didentifierare.
    |   Det g�r inte att behandla ett meddelande fr�n ett f�nster som inte �r ett
MDI-f�nster (Multiple Document Interface).
 $   Snabbmenyn �r redan aktiv.
    0   F�nstret har inte n�gra rullningslister.
  D   Omr�det f�r en rullningslist kan inte vara st�rre �n 0x7FFF.
  D   Det g�r inte att visa eller ta bort f�nstret p� angivet s�tt.
 P   Otillr�ckliga systemresurser f�r att kunna avsluta den beg�rda tj�nsten.
  P   Otillr�ckliga systemresurser f�r att kunna avsluta den beg�rda tj�nsten.
  P   Otillr�ckliga systemresurser f�r att kunna avsluta den beg�rda tj�nsten.
  L   Otillr�cklig minneskvot f�r att kunna avsluta den beg�rda tj�nsten.
   L   Otillr�cklig minneskvot f�r att kunna avsluta den beg�rda tj�nsten.
   H   Sidv�xlingsfilen �r f�r liten f�r att kunna avsluta denna �tg�rd.
     Det fanns ingen menypost.
 8   Ogiltig referens (handle) f�r tangentbordslayout.
     Hook-typen till�ts inte.
  <   Den h�r �tg�rden kr�ver en interaktiv f�nsterstation.
 D   Den h�r �tg�rden returnerades eftersom tidsgr�nsen uppn�tts.
  0   Ogiltig �vervakningsreferens (handle).
    $   H�ndelseloggfilen �r skadad.
  \   H�ndelseloggfilen kunde inte �ppnas. Tj�nsten f�r h�ndelseloggning startades inte.
    $   H�ndelseloggfilen �r full.
    8   H�ndelseloggfilen har �ndrats mellan l�sningar.
   ,   Bindningen f�r str�ngen �r ogiltig.
   8   Bindningsreferensen (handle) �r inte av r�tt typ.
 0   Bindningsreferensen (handle) �r ogiltig.
  ,   RPC-protokollsekvensen st�ds inte.
    ,   RPC-protokollsekvensen �r ogiltig.
    D   Str�ngens universalt unika identifierare (UUID) �r ogiltig.
   (   Slutpunktsformatet �r ogiltigt.
   $   N�tverksadressen �r ogiltig.
  0   Det g�r inte att hitta n�gon slutpunkt.
   $   Timeout-v�rdet �r ogiltigt.
   P   Det g�r inte att hitta objektets universalt unika identifierare (UUID).
   P   Objektets universalt unika identifierare (UUID) har redan registrerats.
   L   Typens universalt unika identifierare (UUID) har redan registrerats.
  $   RPC-servern lyssnar redan.
    0   Ingen protokollsekvens har registrerats.
      RPC-servern lyssnar inte.
 (   Den �verordnade typen �r ok�nd.
       Gr�nssnittet �r ok�nt.
    (   Det finns inte n�gra bindningar.
  0   Det finns inte n�gra protokollsekvenser.
  ,   Det g�r inte att skapa slutpunkten.
   T   Det finns inte tillr�ckligt med lediga resurser f�r att utf�ra denna �tg�rd.
  (   RPC-servern �r inte tillg�nglig.
  H   RPC-servern �r upptagen och kan inte slutf�ra den h�r �tg�rden.
   (   N�tverksalternativen �r ogiltiga.
 4   Det finns inga aktiva RPC p� den h�r tr�den.
     Ett RPC misslyckades.
 0   Ett RPC misslyckades och utf�rdes inte.
   ,   Det uppstod ett protokollfel f�r RPC.
 8   �verf�ringssyntaxen st�ds inte av RPC-servern.
    H   Den h�r typen av universalt unik identifierare (UUID) st�ds inte.
    Ogiltig tagg.
 $   Matrisgr�nserna �r ogiltiga.
  4   Bindningen inneh�ller inte n�got postnamn.
        Namnsyntaxen �r ogiltig.
      Namnsyntaxen st�ds inte.
  �   Det finns inte n�gon n�tverksadress tillg�nglig som kan anv�ndas
f�r att skapa en universalt unik identifierare (UUID).
  $   Slutpunkten �r en dubblett.
   $   Autentiseringstypen �r ok�nd.
 4   Maximalt antal till�tna anrop �r f�r litet.
      Str�ngen �r f�r l�ng.
 8   Det g�r inte att hitta RPC-protokollsekvensen.
    8   Procedurnumret ligger utanf�r till�tet intervall.
 D   Bindningen inneh�ller inte n�gon autentiseringsinformation.
   (   Autentiseringstj�nsten �r ok�nd.
  $   Autentiseringsniv�n �r ok�nd.
 (   S�kerhetsomgivningen �r ogiltig.
  (   Verifieringstj�nsten �r ok�nd.
       Posten �r ogiltig.
    4   Serverslutpunkten kan inte utf�ra �tg�rden.
   T   Det finns inte n�gra fler slutpunkter tillg�ngliga fr�n slutpunktsavbildaren.
 0   N�gra gr�nssnitt har inte exporterats.
    $   Postnamnet �r ofullst�ndigt.
  (   Versionsalternativet �r ogiltigt.
 (   Det finns inga fler medlemmar.
    (   Det g�r inte att �ngra export.
    ,   Det g�r inte att hitta gr�nssnittet.
     Posten finns redan.
   (   Det g�r inte att hitta posten.
    (   Namntj�nsten �r inte tillg�nglig.
 8   Upps�ttningen med n�tverksadresser �r ogiltig.
        Beg�rd �tg�rd st�ds inte.
 X   Det finns ingen s�kerhetsomgivning tillg�nglig f�r att till�ta personifiering.
    0   Det uppstod ett internt fel i ett RPC.
    D   Det gjordes ett f�rs�k att dividera med noll p� RPC-servern.
  8   Det uppstod ett adresseringsfel i RPC-servern.
    L   En flyttals�tg�rd i RPC-servern skapade en kvot med noll som n�mnare.
 P   Dataunderskott (underflow) uppstod vid en flyttals�tg�rd p� RPC-servern.
  L   Dataspill (overflow) uppstod vid en flyttals�tg�rd p� RPC-servern.
    h   Listan �ver RPC-servrar som �r tillg�ngliga f�r bindning av
automatiska referenser har utt�mts.
  H   Det g�r inte att �ppna filen med tabellen f�r tecken�vers�ttning.
 P   Filen som inneh�ller tecken�vers�ttningstabellen �r mindre
�n 512 byte.
  T   En tom omgivningsreferens �verf�rdes fr�n klienten
till v�rden vid ett RPC.
  4   Omgivningsreferensen �ndrades under ett RPC.
  P   Bindningsreferenserna (handles) som �verf�rdes till ett RPC matchar inte.
 H   Stub-funktionen kan inte f� tag i referensen (handle) till RPC.
   8   En null-pekare �verf�rdes till stub-funktionen.
   <   Numreringsv�rdet ligger utanf�r till�tet intervall.
       Antalet byte �r f�r l�gt.
 0   Stub-funktionen mottog felaktiga data.
    H   Tillhandah�llen anv�ndarbuffert �r inte giltig f�r beg�rd �tg�rd.
 D   Diskmediet �r av ok�nd typ. Det kanske inte �r formaterat.
    4   Arbetsstationen saknar f�rtroendefunktion.
    l   SAM-databasen p� Windows NT-servern har inte n�got datorkonto f�r
denna arbetsstations f�rtroende.
   d   Det gick inte att uppr�tta f�rtroende mellan den prim�ra dom�nen
och den betrodda dom�nen.
   h   Det gick inte att uppr�tta f�rtroende mellan den h�r arbetsstationen och
den prim�ra dom�nen.
    ,   N�tverksinloggningen misslyckades.
    0   Ett RPC utf�rs redan f�r den h�r tr�den.
  L   Ett inloggningsf�rs�k gjordes men tj�nsten NetLogon var inte ig�ng.
   0   Anv�ndarens konto har slutat att g�lla.
   4   Omdirigeraren anv�nds och kan inte tas bort.
  8   Angiven skrivardrivrutin �r redan installerad.
    $   Den angivna porten �r ok�nd.
  $   Skrivardrivrutinen �r ok�nd.
  $   Utskriftsprocessorn �r ok�nd.
 (   Angiven delningsfil �r ogiltig.
   $   Angiven prioritet �r ogiltig.
 $   Skrivarnamnet �r ogiltigt.
        Skrivaren finns redan.
    $   Skrivarkommandot �r ogiltigt.
 $   Angiven datatyp �r ogiltig.
       Angiven milj� �r ogiltig.
 ,   Det finns inte n�gra fler bindningar.
 �   Du anv�nder ett konto som anv�nds f�r f�rtroende mellan dom�ner. Anv�nd ditt globala eller lokala anv�ndarkonto f�r att f� �tkomst till servern.
  t   Du anv�nder ett datorkonto. Anv�nd ditt globala eller lokala anv�ndarkonto f�r att f� �tkomst till servern.
   �   Du anv�nder ett konto som anv�nds f�r serverf�rtroende. Anv�nd ditt globala eller lokala anv�ndarkonto f�r att f� �tkomst till servern.
   l   Namn eller s�kerhets-ID f�r angiven dom�n st�mmer inte
�verens med dom�nens f�rtroendeinformation.
   0   Servern anv�nds och kan inte tas bort.
    D   Angiven avbildningsfil inneh�ller inte n�gon resursavsnitt.
   L   Det gick inte att hitta den angivna resurstypen i avbildningsfilen.
   L   Det gick inte att hitta det angivna resursnamnet i avbildningsfilen.
  L   Det g�r inte att hitta angivet resursspr�ks-ID i avbildningsfilen.
    @   Otillr�cklig minneskvot f�r att utf�ra det h�r kommandot.
 (   Inga gr�nssnitt har registrerats.
    Ett RPC avbr�ts.
  H   Bindningsidentifieraren inneh�ller inte all beg�rd information.
   4   Ett kommunikationsfel intr�ffade vid ett RPC.
 ,   Beg�rd autentiseringsniv� st�ds inte.
 0   Det finns inga huvudnamn registrerade.
    4   Felet �r inte en giltig Windows RPC-felkod.
   d   UUID (universalt unik identifierare) som �r giltig endast f�r den h�r datorn har allokerats.
  ,   Ett s�kerhetspaketsfel har intr�ffat.
 $   Tr�den har inte avbrutits.
    8   Ogiltig �tg�rd p� kodnings-/avkodningsreferensen.
 (   Inkompatibel version av paketet.
  4   Inkompatibel version p� RPC stub-funktionen.
  4   RPC pipe-objektet �r ogiltigt eller korrupt.
  P   Ett f�rs�k gjordes att utf�ra en ogiltig �tg�rd p� ett RPC pipe-objekt.
   (   Icke st�dd version av RPC pipe.
   0   Det gick inte att hitta gruppmedlemmen.
   X   Det gick inte att skapa databasen f�r slutpunktsavbildningar (endpoint mapper).
   H   Objektets universalt unika identifierare (UUID) �r en noll-UUID.
      Angiven tid �r ogiltig.
   (   Angivet formul�rnamn �r ogiltigt.
 0   Den angivna formul�rstorleken �r ogiltig.
 0   Angiven skrivarreferens v�ntas reda p�
    0   Den angivna skrivaren har tagits bort.
    (   Skrivarens tillst�nd �r ogiltigt.
 X   Anv�ndaren m�ste byta l�senord innan hon eller han loggar in f�r f�rsta g�ngen.
   D   Det g�r inte att hitta dom�nkontrollanten f�r denna dom�n.
    D   Angivet konto �r l�st och kan inte anv�ndas f�r inloggning.
   8   Det angivna exportobjektet gick inte att hitta.
   4   Det gick inte att hitta det angivna objektet.
 L   Det gick inte att hitta den angivna upps�ttningen objektuppl�sare.
    H   Det finns fortfarande data som ska skickas i beg�randebufferten.
  0   Ogiltig asynkron RPC-referens (handle).
   D   Ogiltig asynkron RPC-referens (handle) f�r den h�r �tg�rden.
  ,   RPC pipe-objektet har redan �ppnats.
  <   RPC-anropet slutf�rdes innan alla pipes behandlades.
  @   Det finns inte mer data tillg�ngliga fr�n denna RPC pipe.
 $   Ogiltigt bildpunktsformat.
    ,   Den angivna drivrutinen �r ogiltig.
   8   Ogiltigt stil- eller klassattribut f�r �tg�rden.
  (   Beg�rd metafils�tg�rd st�ds inte.
 0   Beg�rd transformerings�tg�rd st�ds inte.
  <   Den beg�rda �tg�rden f�r att klippa ut st�ds inte.
    t   N�tverksanslutningen fungerar men anv�ndaren var tvungen att ange ett annat
l�senord �n det ursprungliga.
    (   Angivet anv�ndarnamn �r ogiltigt.
 ,   Denna n�tverksanslutning finns inte.
  L   Denna n�tverksanslutning har �ppna filer eller beg�ran som v�ntar.
    4   Det finns fortfarande aktiva anslutningar.
    L   Enheten anv�nds av en aktiv process och kan d�rf�r inte kopplas fr�n.
 4   Den angivna skrivar�vervakaren finns inte.
    0   Angiven skrivardrivrutin anv�nds redan.
   ,   Det g�r inte att hitta buffertfilen.
  4   Ett StartDocPrinter-anrop utf�rdades inte.
    (   Ett AddJob-anrop utf�rdades inte.
 <   Angiven utskriftsprocessor har redan installerats.
    <   Den angivna skrivar�vervakaren �r redan installerad.
  L   Den angivna skrivar�vervakaren har inte de n�dv�ndiga funktionerna.
   4   Den angivna skrivar�vervakaren anv�nds redan.
 T   Den beg�rda �tg�rden till�ts inte n�r det finns utskriftsjobb i skrivark�n.
   d   Den beg�rda �tg�rden lyckades. �ndringarna kommer att tr�da i kraft n�r datorn startas om.
    d   Den beg�rda �tg�rden lyckades. �ndringarna kommer att tr�da i kraft n�r tj�nsten startas om.
  <   Ett fel intr�ffade p� WINS n�r kommandot behandlades.
 $   Lokal WINS kan inte tas bort.
 ,   Importering fr�n filen misslyckades.
  `   S�kerhetskopieringen misslyckades. Gjordes det en fullst�ndig s�kerhetskopiering innan?
   h   S�kerhetskopieringen misslyckades. Kontrollera katalogen som du s�kerhetskopierar databasen till.
 ,   Namnet finns inte i WINS-databasen.
   H   Replikering med en icke-konfigurerad partner �r inte till�tet.
    �   DHCP-klientens IP-adress anv�nds redan i n�tverket. Det lokala gr�nssnittet kommer att inaktiveras tills DHCP-klienten kan erh�lla en ny adress.
  h   Klusterresursen kan inte flyttas till en annan grupp eftersom andra resurser �r beroende av den.
  <   Det gick inte att hitta klusterresursens beroende.
    p   Det g�r inte att g�ra klusterresursen beroende av den angivna resursen eftersom den redan �r beroende.
    ,   Klusterresursen finns inte online.
    D   Det finns ingen klusternod tillg�nglig f�r den h�r �tg�rden.
  ,   Klusterresursen �r inte tillg�nglig.
  0   Det gick inte att hitta klusterresursen.
     Klustret avslutas.
    P   Det g�r inte att �terta en klusternod fr�n klustret n�r det �r online.
       Objektet finns redan.
 $   Objektet �r redan i listan.
   <   Klustergruppen �r inte tillg�nglig f�r en ny beg�ran.
 0   Det gick inte att hitta klustergruppen.
   T   Det gick inte att slutf�ra �tg�rden eftersom klustergruppen inte �r online.
   0   Klusternoden �r inte �gare till resursen.
 0   Klusternoden �r inte �gare till gruppen.
  P   Det gick inte att skapa klusterresursen i den angivna resurs�vervakaren.
  L   Resurs�vervakaren kunde inte g�ra klusterresursen tillg�nglig online.
 P   �tg�rden gick inte att slutf�ra eftersom klusterresursen finns online.
    h   Det gick inte att ta bort eller koppla fr�n klusterresursen eftersom den utg�r quorum-resursen.
   x   Klustret kunde inte g�ra resursen till en quorum-resurs eftersom den inte �r kapabel att vara en quorum-resurs.
   $   Klusterprogramvaran avslutas.
 d   Gruppen eller resursen �r inte i ett giltigt tillst�nd f�r att utf�ra den beg�rda �tg�rden.
   |   Egenskaperna lagrades men alla �ndringar kommer inte att tr�da i kraft f�rr�n n�sta
g�ng som resursen finns online.
  t   Klustret kunde inte g�ra en resurs till en quorum-resurs eftersom den inte tillh�r en delad lagringsklass.
    P   Det gick inte att ta bort klusterresursen eftersom det �r en k�rnresurs.
  <   Quorum-resursen kunde inte g�ras tillg�nglig online.
  <   Det gick inte att skapa eller ansluta quorum-loggen.
      Klusterloggen �r korrupt.
 l   Det gick inte att skriva posten till klusterloggen eftersom den �verstiger den maximala storleken.
    8   Klusterloggen �verstiger den maximala storleken.
  D   Det gick inte att hitta n�gon kontrollpost i klusterloggen.
   L   Det minsta n�dv�ndiga diskutrymmet f�r loggning �r inte tillg�ngligt.
 X   Listan �ver servrar i den h�r arbetsgruppen �r f�r tillf�llet inte tillg�nglig.
   H   Den underliggande filen konverterades till sammansatt filformat.
  H   Lagrings�tg�rden b�r blockeras innan mer data �r tillg�ngliga.
    8   Lagrings�tg�rden b�r aktiveras igen omedelbart.
   T   Den meddelade h�ndelseminskningen kommer inte att p�verka lagrings�tg�rden.
   T   Det gick inte att konsolidera p g a fildelningsfel. Bekr�ftelsen lyckades.
    L   Det gick inte att konsolidera lagringsfilen. Bekr�ftelsen lyckades.
   P   Det �r ol�mpligt att konsolidera lagringsfilen. Bekr�ftelsen lyckades.
    D   Anv�nd registerdatabasen f�r att erh�lla beg�rd information
      Lyckades, men statisk
     Macintosh-urklippsformat
     Sl�pp-�tg�rd utf�rdes
 (   Dra och sl�pp-�tg�rden avbr�ts
        Anv�nd standardmark�ren
       Data har samma FORMATETC
     Bilden �r redan fryst
    FORMATETC st�ds inte
     Samma cache-minne
 0   Vissa cache-minnen har inte uppdaterats
   $   Ogiltigt verb f�r OLE-objekt
  <   Verbnumret �r korrekt men verb kan inte utf�ras nu
    4   Ogiltig f�nsterreferens (handle) skickades
    L   Meddelandet �r f�r l�ngt. Texten har trunkerats f�r att kunna visas
   <   Det g�r inte att konvertera OLESTREAM till IStorage
   (   Moniker reducerat till sig sj�lv
  (   Allm�nt prefix �r detta moniker
   (   Allm�nt prefix �r indata-moniker
  (   Allm�nt prefix �r b�da monikers
   4   Moniker �r redan registrerat i objekttabellen
 <   Alla de beg�rda gr�nssnitten var inte tillg�ngliga
        Har inte implementerats
      Slut p� ledigt minne
  ,   Ett eller flera argument �r ogiltiga
      Gr�nssnittet st�ds inte
      Ogiltig pekare
        Ogiltig referens (handle)
    �tg�rden avbr�ts
     Odefinierat fel
   $   Allm�nt fel f�r nekad �tkomst
 \   N�dv�ndiga data �r inte tillg�ngliga �nnu f�r att kunna slutf�ra den h�r �tg�rden.
        Gr�nssnittet st�ds inte
      Ogiltig pekare
       �tg�rden avbr�ts
     Odefinierat fel
       Tr�dfel vid lokal lagring
 (   Fel vid allokering av delat minne
 $   Fel vid allokering av minne
   ,   Det gick inte att starta klass-cache.
 0   Det g�r inte att initiera RPC-tj�nster
    0   Det g�r inte att ange TLS-kanalkontroll
   4   Det g�r inte att allokera TLS-kanalkontroll
   4   Den angivna minnesallokeraren accepteras inte
 (   Mutex f�r OLE-tj�nst finns redan
  0   Filavbildning f�r OLE-tj�nst finns redan
  4   Det g�r inte att avbilda fil f�r tj�nsten OLE
 ,   Det g�r inte att starta OLE-tj�nst
    T   Det gjordes ett f�rs�k att anropa Colnitialize en andra g�ng med enkel tr�d.
  4   En fj�rranslutning kr�vs men �tkomst nekas.
   P   En fj�rranslutning kr�vs men servernamnet som har angetts �r ogiltigt.
    x   Klassen �r inte konfigurerad f�r att k�ras som ett s�kerhets-ID som inte �r likadant som f�r den som ringer upp.
  H   Om OLE1-tj�nster ska anv�ndas m�ste DDE-f�nster vara inaktiva.
    d   En K�r som-angivelse m�ste ha formen <dom�nnamn>\<anv�ndarnamn> eller endast <anv�ndarnamn>.
  L   Det gick inte att ansluta till servern. S�kv�gen kan vara felaktig.
   �   Det gick inte att ansluta till servern med konfigurerat anv�ndar-ID. S�kv�gen kan vara felaktig eller inte tillg�nglig.
   �   Det gick inte att ansluta till servern p� grund av att det konfigurerade anv�ndar-ID:et �r ogiltigt. Kontrollera anv�ndarnamnet och l�senordet.
   4   Klienten nekas �tkomst till den h�r servern.
  P   Det gick inte att starta tj�nsten som tillhandah�ller den h�r servern.
    T   Den h�r datorn kunde inte kommunicera med datorn som tillhandah�ller servern.
 4   Servern svarade inte efter att ha startats.
   \   Registreringsinformationen f�r den h�r servern �r inkonsekvent eller ofullst�ndig.
    `   Registreringsinformationen f�r det h�r gr�nssnittet �r inkonsekvent eller ofullst�ndig.
      �tg�rden st�ds inte.
  4   Anrop accepterades inte av anropsmottagare.
   ,   Anrop avbr�ts av meddelandefiltret.
   x   Anroparen skickar ett meddelande mellan program via SendMessage
och kan inte skicka meddelande via PostMessage.
  T   Anroparen g�r ett asynkront anrop och kan d�rf�r inte g�ra ett externt anrop.
 H   Det g�r inte att skicka ett anrop inifr�n ett meddelandefilter.
   t   Anslutningen br�ts eller �r skadad
och kan inte anv�ndas l�ngre. Andra
anslutningar fungerar fortfarande.
   �   Anropsmottagaren (servern [inte serverprogrammet])
�r inte tillg�nglig. Alla anslutningar �r ogiltiga. Anropet
kan ha utf�rts.
  d   Anroparen (klienten) f�rsvann under tiden anropsmottagaren
(servern) behandlade ett anrop.
   <   Datapaketet med ordnade parameterdata �r felaktigt.
   `   Anropet �verf�rdes inte korrekt. Meddelandek�n
var full och t�mdes inte efter leverans.
  P   Klienten (anroparen) kan inte ordna parameterdata - ont om minne, osv.
    T   Klienten (anroparen) kan inte omordna returnerade data - ont om minne, osv.
   X   Servern (anropsmottagaren) kan inte ordna returnerade data - ont om minne, osv.
   X   Servern (anropsmottagaren) kan inte omordna parameterdata - ont om minne, osv.
    D   Mottagna data �r ogiltiga, kan vara server- eller klientdata.
 <   En viss parameter �r ogiltig och kan inte (om)ordnas.
 P   Det finns inget andra utg�ende anrop p� samma kanal i DDE-konversation.
   �   Anropsmottagaren (servern [inte serverprogrammet]) �r inte
tillg�nglig. Alla anslutningar �r ogiltiga. Anropet utf�rdes inte.
        Systemanrop misslyckades.
 `   Det gick inte att allokera vissa n�dv�ndiga resurser (till exempel minne och h�ndelser).
  T   Det har gjorts ett f�rs�k att g�ra anrop p� flera tr�dar i enkelt tr�dl�ge.
   D   N�dv�ndigt gr�nssnitt �r inte registrerat p� serverobjektet.
  X   RPC kunde inte anropa servern eller kunde inte returnera resultatet av anropet.
   (   Servern skickade ett undantag.
    D   Det g�r inte att �ndra tr�dl�ge efter att det har angetts.
    ,   Anropad metod finns inte p� servern.
  @   Det anropade objektet har kopplats fr�n sina klienter.
    P   Det anropade objektet valde att inte behandla anropet nu. F�rs�k igen.
    <   Meddelandefiltret angav att programmet �r upptaget.
   0   Meddelandefiltret tog inte emot anropet.
  @   Ett anropskontrollgr�nssnitt anropades med ogiltiga data.
 d   Det g�r inte att g�ra ett utg�ende anrop eftersom programmet g�r ett indatasynkront anrop.
    L   Programmet anropade ett gr�nssnitt som ordnades f�r en annan tr�d.
    8   CoInitialize har inte anropats p� aktuell tr�d.
   H   OLE-versionerna p� klienten och servrarna �verensst�mmer inte.
    D   OLE-tj�nsten har tagit emot ett paket med ett ogiltigt huvud.
 H   OLE-tj�nsten har tagit emot ett paket med ett ogiltigt till�gg.
   4   Beg�rt objekt eller gr�nssnitt finns inte.
        Beg�rt objekt finns inte.
 D   OLE-tj�nsten har skickat en beg�ran och v�ntar p� ett svar.
   L   OLE-tj�nsten v�ntar p� ett svar innan den f�rs�ker med en ny beg�ran.
 d   Det gick inte att f� tillg�ng till uppringningsinformationen n�r uppringningen har slutf�rts.
 4   Personifiering vid os�kra samtal st�ds inte.
  �   S�kerheten m�ste initieras innan gr�nssnitten ordnas eller
omordnas. S�kerheten kan inte �ndras n�r den v�l har initierats.
  �   Det finns inga s�kerhetspaket installerade p� den h�r datorn, anv�ndaren �r inte
p�loggad eller ocks� finns det inga kompatibla s�kerhetspaket f�r klienten och servern.
    �tkomst nekad.
    8   Fj�rrsamtal �r inte till�tna i den h�r processen.
 X   Det ordnade gr�nssnittsdatapaketet (OBJREF) har ett ogiltigt eller ok�nt format.
  ,   Det har intr�ffat ett internt fel.
       Ok�nt gr�nssnitt.
 ,   Det gick inte att hitta medlemmen.
    ,   Det gick inte att hitta parametern.
      Typblandningsfel.
    Ok�nt namn.
       Inga namngivna argument.
     Felaktig variabeltyp.
    Undantag intr�ffade.
  $   Utanf�r aktuellt intervall.
      Ogiltigt index.
      Ok�nt spr�k.
     Minnet �r l�st.
   $   Ogiltigt antal parametrar.
    $   Parametern �r inte valfri.
        Ogiltig anropsmottagare.
      St�der inte en samling.
       Bufferten �r f�r liten.
   4   Gammalt format eller ogiltigt typbibliotek.
   4   Gammalt format eller ogiltigt typbibliotek.
   (   Fel vid �tkomst av OLE-registret.
 (   Biblioteket �r inte registrerat.
      Bunden till ok�nd typ.
    (   Kvalificerat namn godk�ndes inte.
 @   Ogiltig referens eller referens till icke-kompilerad typ.
    Typblandningsfel.
 ,   Det gick inte att hitta elementet.
       Dubbeltydigt namn.
    (   Namnet finns redan i biblioteket.
    Ok�nd LCID.
   8   Funktionen �r inte definierad i angiven DLL-fil.
  (   Ogiltig modultyp f�r �tg�rden.
    0   Storleken f�r inte vara st�rre �n 64 kB.
  $   ID-dubblett i arvshierarki.
   4   Felaktigt arvsdjup f�r standard-OLE-medlem.
      Typblandningsfel.
     Ogiltigt antal argument.
     I/O-fel.
  D   Ett fel intr�ffade n�r en unik tempor�r fil skulle skapas.
    P   Ett fel intr�ffade n�r ett typbibliotek eller en DLL-fil skulle l�sas in.
 ,   Inkonsekventa egenskapsfunktioner.
    4   Cirkul�rt beroende mellan typer och moduler.
  0   Det g�r inte att utf�ra beg�rd �tg�rd.
    $   Det g�r inte att hitta %1.
    ,   Det g�r inte att hitta s�kv�gen %1.
   L   Det finns inte tillr�ckligt med resurser f�r att �ppna en annan fil.
     �tkomst nekad.
    P   Det har gjorts ett f�rs�k att utf�ra en �tg�rd p� ett ogiltigt objekt.
    P   Det finns inte tillr�ckligt med ledigt minne f�r att slutf�ra �tg�rden.
      Ogiltigt pekarfel.
    8   Det finns inte n�gra fler poster att returnera.
       Disken �r skrivskyddad.
   4   Det har intr�ffat ett fel under en s�kning.
   4   Det uppstod ett diskfel under en skriv�tg�rd.
 4   Det uppstod ett diskfel under en l�s�tg�rd.
   0   Det har intr�ffat en resurs�vertr�delse.
  ,   Det har intr�ffat en l�s�vertr�delse.
    %1 finns redan.
       Ogiltigt parameterfel.
    P   Det finns inte tillr�ckligt med diskutrymme f�r att slutf�ra �tg�rden.
    T   En icke-standardegenskap har ogiltigt skrivits till en standardupps�ttning.
   8   Ett API-anrop avslutades p� ett felaktigt s�tt.
   4   Filen %1 �r inte en giltig sammansatt fil.
    $   %1 �r inte ett giltigt namn.
  (   Ett ov�ntat fel har intr�ffat.
    0   Funktionen �r �nnu inte implementerad.
       Ogiltigt flaggfel.
    D   Det har gjorts ett f�rs�k att anv�nda ett upptaget objekt.
    <   Minnet har �ndrats sedan den senaste bekr�ftelsen.
    P   Det har gjorts ett f�rs�k att anv�nda ett objekt som inte l�ngre finns.
       Det g�r inte att spara.
   L   Den sammansatta filen %1 skapades med en inkompatibel minnesversion.
  D   Den sammansatta filen %1 skapades med en nyare minnesversion.
 @   Filen SHARE.EXE eller ekvivalent fil kr�vs f�r �tg�rden.
  L   Felaktigt �tg�rdsanrop p� ett lagringsmedium som inte �r filbaserat.
  <   Felaktigt �tg�rdsanrop p� objekt med bevarad ordning.
    DOC-filen �r skadad.
  4   Filen ole32.dll har l�sts in p� fel adress.
   <   Filh�mtningen avbr�ts ov�ntat. Filen �r ofullst�ndig.
 $   Filh�mtningen har avbrutits.
      Ogiltig OLEVERB-struktur
  $   Ogiltiga instruktionsflaggor
  P   Det g�r inte att g�ra fler numreringar eftersom associerade data saknas
   8   Implementeringen accepterar inte instruktioner
    <   Det finns ingen anslutning f�r detta anslutnings-ID
   @   Objektet m�ste k�ras f�r att �tg�rden ska kunna utf�ras.
  4   Det finns inget cache-minne att arbeta med
       Oinitierat objekt
 4   Det l�nkade objektets k�llklass har �ndrats
   4   Det gick inte att f� moniker till objektet
    ,   Det g�r inte att binda till k�llan
    8   Objektet �r statiskt. �tg�rden �r inte till�ten
       Anv�ndaren sparade inte.
     Ogiltig rektangel
 H   Filen COMPOBJ.DLL �r f�r gammal f�r den initierade OLE2.DLL-filen
 (   Ogiltig f�nsterreferens (handle).
 (   Objekt �r inte i ett aktivt l�ge
  ,   Det g�r inte att konvertera objektet
  \   Det g�r inte att utf�ra �tg�rden eftersom objektet inte har n�got tilldelat minne

   $   Ogiltig FORMATETC-struktur
    (   Ogiltig DVTARGETDEVICE-struktur
   $   Ogiltig STDGMEDIUM-struktur
       Ogiltig STATDATA-struktur
    Ogiltig lindex
       Ogiltig tymed
     Ogiltigt urklippsformat
      Ogiltig aspekt
    @   tdSize-parametern i DVTARGETDEVICE-strukturen �r ogiltig
  4   Objektet st�der inte IViewObject-gr�nssnitt
   H   F�rs�ker att �terst�lla ett m�lobjekt som inte har registrerats.
  <   F�nstret har redan registrerats som ett m�lobjekt.
    (   Ogiltig f�nsterreferens (handle).
 H   Klass st�der inte m�ngder (eller s� �r klassobjektet borttaget)
   <   N�dv�ndig klass kan inte erh�llas fr�n ClassFactory
   4   Klassen �r inte licensierad f�r anv�ndning.
   8   Fel intr�ffade n�r vyn skulle ritas/i ritningsvyn
 ,   Det g�r inte att l�sa registernyckeln
 0   Det g�r inte att skriva registernyckeln
   0   Det g�r inte att hitta registernyckeln
    $   Ogiltigt v�rde f�r Registret
  $   Klassen har inte registrerats
 ,   Gr�nssnittet har inte registrerats
       CATID finns inte.
 $   Ingen beskrivning hittades.
   (   Cache-minnet har inte uppdaterats
     Inga verb f�r OLE-objekt
  $   Ogiltigt verb f�r OLE-objekt
      Det g�r inte att �ngra
    8   Det finns inget tillg�ngligt utrymme f�r verktyg
  (   OLESTREAM Get-metod misslyckades
  (   OLESTREAM Put-metod misslyckades
  4   Inneh�llet i OLESTREAM har felaktigt format
   `   Det intr�ffade ett fel i ett Windows GDI-anrop n�r bitmappen konverterades till en DIB
    4   Inneh�llet i IStorage har felaktigt format
    4   Inneh�llet i IStorage saknar en standardstr�m
 d   Det intr�ffade ett fel i ett Windows GDI-anrop n�r en DIB konverterades till en bitmapp.

    $   OpenClipboard misslyckades
    $   EmptyClipboard misslyckades
       SetClipboard misslyckades
 $   Data i urklipp �r ogiltiga
    $   CloseClipboard misslyckades
   (   Moniker m�ste anslutas manuellt
   $   �tg�rden �versteg tidsgr�nsen
     Moniker m�ste vara allm�n
 $   �tg�rden �r inte tillg�nglig
     Ogiltig syntax
        Inga objekt f�r moniker
      Felaktigt filtill�gg
  ,   Mellanliggande �tg�rd misslyckades
        Moniker �r inte bindande
      Moniker �r inte bunden
    $   Moniker kan inte �ppna fil
    @   Anv�ndare m�ste ange indata f�r att �tg�rden ska lyckas
   (   Moniker-klass har ingen invers
    (   Moniker refererar inte till minne
    Inget vanligt prefix
  $   Moniker kunde inte r�knas upp
 (   CoInitialize har inte anropats.
   (   CoInitialize har redan anropats.
  (   Objektklassen kan inte best�mmas
     Ogiltig klasstr�ng
        Ogiltig gr�nssnittsstr�ng
 (   Det g�r inte att hitta programmet
 8   Det g�r inte att k�ra programmet mer �n en g�ng
      Fel i program
 0   Det g�r inte att hitta DLL-fil f�r klass
     Fel i DLL-filen
   X   Felaktigt operativsystem eller felaktig version av operativsystem f�r programmet
  (   Objektet har inte registrerats
    $   Objektet �r redan registrerat
 0   Objektet har inte anslutits till servern
  D   Programmet startades, men registrerade inte en klassfabrik
        Objektet har frigjorts
    4   Det gick inte att personifiera DCOM-klienten.
 <   Det gick inte att h�mta serverns s�kerhetsomgivning.
  @   Det gick inte att �ppna �tkomst-token f�r aktuell tr�d.
   L   Det gick inte att h�mta anv�ndarinformation fr�n ett �tkomst-token.
   h   Klienten som anropade IAccessControl::IsAccessPermitted var den som f�tt f�rtroende f�r metoden.
  8   Det gick inte att h�mta klientens s�kerhetsskikt.
 L   Det g�r inte att ange en �verordnad ACL i en s�kerhetsbeskrivning.
    @   Systemfunktionen AccessCheck returnerade v�rdet false.
    @   NetAccessDel eller NetAccessAdd returnerade en felkod.
    �   En av f�rtroendestr�ngarna som anv�ndaren tillhandah�ller har inte skrivits p� formen <Dom�n>\<Namn>. Det var inte "*"-str�ngen.
  P   En av s�kerhetsidentifierarna som anv�ndaren tillhandah�ller �r ogiltig.
  h   Det gick inte att konvertera en l�ng teckenf�rtroendestr�ng till en multibyte-f�rtroendestr�ng.
   |   Det gick inte att hitta en s�kerhetsidentifierare som motsvarar den f�rtroendestr�ng som anv�ndaren tillhandah�ller.
  8   Systemfunktionen LookupAccountSID misslyckades.
   |   Det gick inte att hitta ett f�rtroendenamn som motsvarar en s�kerhetsidentifierare som anv�ndaren tillhandah�ller.
    8   Systemfunktionen LookupAccountName misslyckades.
  H   Det gick inte att st�lla in eller �terst�lla en seriereferens.
    4   Det gick inte att h�mta Windows-katalogen.
       S�kv�gen �r f�r l�ng.
 ,   Det gick inte att generera ett uuid.
  (   Det gick inte att skapa filen.
    H   Det gick inte att st�nga en seriereferens eller en filreferens.
   @   Antalet ACE-poster i en ACL �verskrider systemgr�nsen.
    X   Alla ACE-posterna DENY_ACCESS finns inte f�re ACE-posterna GRANT_ACCESS i fl�det.
 d   Versionen av ACL-formatet i fl�det st�ds inte av den h�r implementeringen av IAccessControl.
  @   Det gick inte att �ppna �tkomst-token i serverprocessen.
  P   Det gick inte att avkoda ACL i fl�det som tillhandah�lls av anv�ndaren.
   8   COM IAccessControl-objektet har inte initierats.
  $   Allm�nt fel f�r nekad �tkomst
     Ogiltig referens (handle)
    Slut p� ledigt minne
  ,   Ett eller flera argument �r ogiltiga
  8   Ett f�rs�k att skapa ett klassobjekt misslyckades
 0   OLE-tj�nsten kunde inte binda objektet
    4   RPC-kommunikation med OLE-tj�nst misslyckades
 $   Felaktig s�kv�g till objekt
   $   Serverk�rning misslyckades
    <   OLE-tj�nsten kunde inte kommunicera med objektservern
 ,   Moniker-s�kv�g kan inte normaliseras
  8   Objektservern stannas vid kontakt fr�n OLE-tj�nst
 0   En ogiltig rotblockspekare har angetts
    <   En allokeringskedja inneh�ll en ogiltig l�nkpekare
    0   Beg�rd allokeringsstorlek var f�r stor
       Ogiltigt anv�ndar-ID.
    Ogiltigt hash-v�rde.
     Ogiltig nyckel.
      Ogiltig l�ngd.
       Ogiltiga data.
       Ogiltig signatur.
 (   Ogiltig version fr�n providern.
   (   En ogiltig algoritm har angetts.
  (   Ogiltiga parametrar har angetts.
  $   En ogiltig typ har angetts.
   8   Nyckeln kan inte anv�ndas i angiven omgivning.
    <   Hash-v�rdet kan inte anv�ndas i angiven omgivning.
       Nyckeln finns inte.
   T   Det finns inte tillr�ckligt med ledigt minne f�r att kunna utf�ra �tg�rden.
      Objektet finns redan.
    �tkomst nekad.
    (   Det gick inte att hitta objektet.
 $   Data har redan krypterats.
    (   En ogiltig provider har angetts.
  ,   En ogiltig provider-typ har angetts.
  8   Den offentliga nyckeln fr�n providern �r ogiltig.
 (   Nyckelupps�ttningen finns redan.
  ,   Provider-typen har inte definierats.
  4   Provider-typen som �r registrerad �r ogiltig.
 0   Nyckelupps�ttningen har inte definierats.
 <   Nyckelupps�ttningen som �r registrerad �r ogiltig.
    @   Provider-typen �verensst�mmer inte med registrerat v�rde.
     ID-nummerfilen �r skadad.
 D   Det gick inte att initiera DLL-filen f�r providern korrekt.
   8   Det gick inte att hitta DLL-filen f�r providern.
  8   Parametern f�r nyckelupps�ttningen �r ogiltig.
    ,   Det har intr�ffat ett internt fel.
        Ett basfel har intr�ffat.
 D   Ett fel uppstod under en krypterings�tg�rd i ett meddelande.
  (   Krypteringsalgoritmen �r ok�nd.
   8   Objektidentifieraren har formaterats felaktigt.
   $   Meddelandetypen �r ogiltig.
   0   Meddelandet �r inte kodat som f�rv�ntat.
  P   Meddelandet inneh�ller inte ett autentiseringsattribut som f�rv�ntats.
        Hash-v�rdet �r felaktigt.
     Indexv�rdet �r ogiltigt.
  4   Meddelandets inneh�ll har redan dekrypterats.
 8   Meddelandets inneh�ll har inte dekrypterats �nnu.
 @   Datameddelandet inneh�ller inte den angivna mottagaren.
       Kontrolltypen �r ogiltig.
 4   Utf�rdaren och/eller serienumret �r ogiltiga.
 D   Det gick inte att hitta den ursprungliga signaturs�ttaren.
    4   Meddelandet inneh�ller inte beg�rda attribut.
 H   Meddelandet som �r �ppet kan inte returnera beg�rda data �nnu.
    <   L�ngden som har angetts f�r utdata �r otillr�cklig.
   8   Ett fel uppstod under kodning eller avkodning.
    L   Ett fel uppstod under l�sning av filen eller skrivning till filen.
    <   Det gick inte att hitta objektet eller egenskapen.
    0   Objektet eller egenskapen finns redan.
    H   Ingen provider har angetts f�r lagringsutrymmet eller objektet.
   <   Det angivna certifikatet har signerats automatiskt.
   L   Det f�reg�ende certifikatet eller CRL-omgivningen har tagits bort.
    <   Ingen matchning hittades vid s�kning efter objektet.
  T   Den typ av krypteringsmeddelande som avkodas �r olikt det som f�rv�ntades.
    ,   Certifikatet har ingen privat nyckel
  P   Inget certifikat har hittats som har en privat nyckel f�r dekryptering.
   P   Meddelandet �r inte krypterat eller ocks� har det ett felaktig format.
    d   Det signerade meddelandet inneh�ller ingen signaturs�ttare fr�n angivet signaturs�ttarindex.
  X   Slutlig f�rsegling v�ntar tills ytterligare frig�randen eller st�ngningar utf�rs.
 8   Certifikatet eller signaturen har annullerats.
    h   Det gick inte att hitta en Dll eller exporterad funktion f�r att kunna bekr�fta annulleringen.
    X   Det gick inte att kontrollera om certifikatet eller signaturen har annullerats.
   |   Det gick inte att slutf�ra kontrollen om annullering har gjorts eftersom servern f�r �terkallande var fr�nkopplad.
    d   Det gick inte att hitta certifikatet eller signaturen i serverns databas f�r �terkallande.
    8   Str�ngen inneh�ller ett icke-numeriskt tecken.
    @   Str�ngen inneh�ller ett tecken som inte kan skrivas ut.
   \   Str�ngen inneh�ller ett tecken som inte finns i 7-bitars ASCII-teckenupps�ttningen.
   d   Str�ngen inneh�ller ett ogiltigt X500-namnattribut f�r nyckel, oid, v�rde eller avgr�nsare.
   �   Koddatabas f�r kodning/avkodning av certifikat i OSS

L�s asn1code.h om du vill ha en definition av k�rfelen i OSS.
Felv�rdena f�r OSS finns bredvid CRYPT_E_OSS_ERROR.
    @   Angiven f�rtroende-provider �r ok�nd i det h�r systemet.
  d   Den f�rtroendeverifierings�tg�rd som har angetts st�ds inte av angiven f�rtroende-provider.
   h   Formul�ret som har angetts f�r �mnet st�ds inte eller �r ok�nt f�r angiven f�rtroende-provider.
   0   �mnet �r inte betrott f�r angiven �tg�rd.
 D   Ett fel har uppst�tt p� grund av problem i ASN.1-kodningen.
   D   Ett fel har uppst�tt p� grund av problem i ASN.1-avkodningen.
 L   L�ser och skriver till�gg d�r attribut �r l�mpliga och vice versa.
    0   Ett ok�nt krypteringsfel har uppst�tt.
    0   Det gick inte att ber�kna datastorleken.
  T   Det gick inte att ber�kna storleken f�r den storleksobegr�nsade datam�ngden.
  @   Detta objekt kan inte l�sa och skriva selfsizing-data.
    (   Det finns ingen signatur i �mnet.
 ,   Ett beg�rt certifikat har f�rfallit.
  P   Giltighetstiderna i certifikatkedjan passar inte in i varandra korrekt.
   d   Ett certifikat som endast kan anv�ndas som slutentitet anv�nds som ett CA eller vice versa.
   @   Ett s�kl�ngdsvillkor i certifikatkedjan har inte f�ljts.
  `   Ett till�gg av ok�nd typ som har etiketterats med "kritiskt" finns i certifikatkedjan.
    H   Ett certifikat anv�nds i ett annat syfte �n vad som har avsetts.
  x   Ett modercertifikat har angetts som utf�rdare av ett undercertifikat men modercertifikatet har inte utf�rdat det.
 |   Ett certifikat saknas eller saknar ett n�dv�ndigt v�rde f�r ett f�lt, till exempel ett �mne eller en utf�rdares namn.
 �   En certifikatkedja behandlades korrekt men kedjan slutade i ett rotcertifikat som inte anses som betrott av f�rtroende-providern.
 l   En kedja av certifikat f�ljdes inte �t som de borde i ett s�rskilt program f�r kedjesammans�ttning.
   0   Ett allm�nt f�rtroendefel har uppst�tt.
   8   Ett certifikat har annullerats av dess utf�rdare.
 