//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//
//+-------------------------------------------------------------------------
//
//  Microsoft Windows Media
//
//
//  File:       wmcodecstrs.h
//
//--------------------------------------------------------------------------

#ifndef __WMCODECSTRS_H_
#define __WMCODECSTRS_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


//
// Configuration options for Windows Media Video Codecs
//

#if defined (_X86_) || defined (_ARM_)
static const WCHAR *g_wszWMVCBitRate = L"_BITRATE";
#else
static const WCHAR *g_wszWMVCComplexityEx = L"_COMPLEXITYEX";
static const WCHAR *g_wszWMVCVBRQuality = L"_VBRQUALITY";
#endif
static const WCHAR *g_wszWMVCKeyframeDistance = L"_KEYDIST";
static const WCHAR *g_wszWMVCCrisp = L"_CRISP";
static const WCHAR *g_wszWMVCAvgFrameRate = L"_AVGFRAMERATE";
static const WCHAR *g_wszWMVCWidth = L"_WIDTH";
static const WCHAR *g_wszWMVCHeight = L"_HEIGHT";

#endif  // !defined(__WMCODECSTRS_H_)
