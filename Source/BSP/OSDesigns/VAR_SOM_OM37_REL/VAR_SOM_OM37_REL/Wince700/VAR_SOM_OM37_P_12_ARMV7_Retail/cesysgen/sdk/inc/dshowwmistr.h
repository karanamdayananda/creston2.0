//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft
// premium shared source license agreement under which you licensed
// this source code. If you did not accept the terms of the license
// agreement, you are not authorized to use this source code.
// For the terms of the license, please see the license agreement
// signed by you and Microsoft.
// THE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//
//  DShow WMI Instrumentation Structures and GUIDs

#pragma once

#include <dshowwmiguid.h>

#ifdef __cplusplus
extern "C" {
#endif
//
// Define all event provider DSHOWWMITAG's here
//
// This tag allows us to identify the reporter of an event easily
// and human friendly. It could also be used to selective enable or
// disable logging of a particular events from WMILOG_*, should this
// be necessary.
//
#define DSHOWWMITAG_AUDIO_RENDERER             'ARen'  // Audio renderer
#define DSHOWWMITAG_AUDIO_CAPTURER             'ACap'  // Audio capturer
#define DSHOWWMITAG_CORE_BASE                  'CorB'  // Core_Base
#define DSHOWWMITAG_MUXER                      'Muxr'  // muxer
#define DSHOWWMITAG_SPLITTER                   'Splt'  // Splitter
#define DSHOWWMITAG_ASF_MEDIA_SINK             'AfMk'  // asf media sink
#define DSHOWWMITAG_ASF_STREAM_SINK            'AfSk'  // asf stream sink
#define DSHOWWMITAG_ASF_MEDIA_STREAM           'AfMm'  // asf media stream
#define DSHOWWMITAG_CD_MEDIA_STREAM            'CDMm'  // CD media stream
#define DSHOWWMITAG_PCMAUDIO_MEDIA_SINK        'CDSK'  // PCM audio media sink
#define DSHOWWMITAG_PCMAUDIO_STREAM_SINK       'CDSk'  // PCM audio stream sink
#define DSHOWWMITAG_M3U_MEDIA_STREAM           'M3uS'  // M3u media stream
#define DSHOWWMITAG_MP3_MEDIA_STREAM           'MP3M'  // M3u media stream
#define DSHOWMWITAG_MP3_MEDIA_SOURCE_BASE      'MP3B'  // MP3 media source base
#define DSHOWWMITAG_MP3_MEDIA_SINK             'MP3K'  // MP3 media sink
#define DSHOWWMITAG_MP3_STREAM_SINK            'MP3k'  // MP3 stream sink
#define DSHOWWMITAG_NET_SOURCE                 'NetS'  // NetSOurce output
#define DSHOWWMITAG_NET_SINK                   'Nsnk'  // Network sink
#define DSHOWWMITAG_HTTP_BYTESTREAM            'HTTP'  // HTTP Bytestream
#define DSHOWWMITAG_MEDIACACHE                 'MCch'  // MediaCache
#define DSHOWWMITAG_WIN32FILE                  'W32F'  // Win32File
#define DSHOWWMITAG_CACHEREADER                'ChRd'  // ASF/MP3 Source CacheReader
#define DSHOWWMITAG_MEDIA_PROCESSOR            'MpCr'  // MediaProcess Core
#define DSHOWWMITAG_MEDIA_PROCESSOR_TOPOLOGY   'MpTp'  // MediaProcess Topology
#define DSHOWWMITAG_MEDIA_SESSION_BITPUMP      'MsBp'  // Media Session and Bitpump
#define DSHOWWMITAG_ASF_MEDIA_SOURCE           'AfMs'  // Asf media source
#define DSHOWWMITAG_VIDEO_MEDIA_SINK           'VSnk'  // Video Renderer
#define DSHOWWMITAG_VIDEO_STREAM_SINK          'VStr'  // Video Stream
#define DSHOWWMITAG_VIDEO_PRESENTER            'VPrs'  // Video Presenter
#define DSHOWWMITAG_VIDEO_MIXER                'VMix'  // Video Mixer
#define DSHOWWMITAG_VIDEO_CAPTURER             'VCap'  // Video Capturer    
#define DSHOWWMITAG_WMAD                       'WMAD'  // WMA decoder
#define DSHOWWMITAG_WMAE                       'WMAE'  // WMA encoder
#define DSHOWWMITAG_WMSP_ENCODER               'WSPE'  // WM Speech Encoder
#define DSHOWWMITAG_WMSP_DECODER               'WSPD'  // WM Speech Decoder
#define DSHOWWMITAG_WMVD                       'WMVD'  // WMV decoder
#define DSHOWWMITAG_WMVE                       'WMVE'  // WMV encoder
#define DSHOWWMITAG_WMVSD                      'WMSD'  // WMV screen decoder
#define DSHOWWMITAG_WMVSE                      'WMSE'  // WMV screen encoder
#define DSHOWWMITAG_WMAGEQ                     'AGEQ'  // WM audio graphic equalizer
#define DSHOWWMITAG_WMASPKRCORR                'SPKR'  // WM audio speaker/room correction
#define DSHOWWMITAG_WMABASSMGMT                'BASS'  // WM audio bass management
#define DSHOWWMITAG_WMASA                      'WMSA'  // WM audio spetrum analyzer
#define DSHOWWMITAG_WMNORM                     'NORM'  // WM audio normalization
#define DSHOWWMITAG_WMHDCDDEC                  'HDCD'  // WM HDCD decoder
#define DSHOWWMITAG_WMTIMECMP                  'TCMP'  // WM time compression
#define DSHOWWMITAG_WMAUDMORPHING              'AMOR'  // WM audio morpher
#define DSHOWWMITAG_WMAMIX                     'AMIX'  // WM audio mixer
#define DSHOWWMITAG_WMAVRHPHONE                'AVRH'  // WM virtualization headphone
#define DSHOWWMITAG_WMASPKRFILL                'ESPF'  // WM audio Enhanced Speaker Filling
#define DSHOWWMITAG_WMASPDTX                   'SpdT'  // WM audio WMA-Pro S/PDIF transmitter
#define DSHOWWMITAG_AECMA                      'AECM'  // AEC and Micarray
#define DSHOWWMITAG_DVDECODER                  'DVde'  // DV Decoder
#define DSHOWWMITAG_DVENCODER                  'DVen'  // DV Encoder
#define DSHOWWMITAG_MPEG2DEC                   'MPGD'  // MPEG2 Decoder 
#define DSHOWWMITAG_AC3DEC                     'AC3D'  // AC3 Decoder 
#define DSHOWWMITAG_MEDIA_ENGINE               'MEng'  // Media Engine
#define DSHOWWMITAG_TOPOLOADER                 'TpLd'  // Topology loader
#define DSHOWWMITAG_LATENCY                    'Late'  // Component Latency
#define DSHOWWMITAG_SOURCERESOLVER             'SoRe'  // Source Resolver
#define DSHOWWMITAG_VIDEO_RECORDER             'VRec'  // Video Recorder
#define DSHOWWMITAG_TRANSFORM                  'Tran'  // Transform
#define DSHOWWMITAG_COLOR_CONTROL              'CCtr'  // Color Control
#define DSHOWWMITAG_FRAMERATE_CONVERTER        'FrCv'  // Frame Rate Converter
#define DSHOWWMITAG_SMPTE_TRANSFORMS           'Smpt'  // SMPTE Transforms
#define DSHOWWMITAG_COLOR_LEGALIZER            'CLgl'  // Color Legalizer
#define DSHOWWMITAG_VIDEO_DENOISER             'VDns'  // Video Denoiser
#define DSHOWWMITAG_COLORCONV                  'CCon'  // WM Color Conversion
#define DSHOWWMITAG_RESIZE                     'Resz'  // WM Resizer
#define DSHOWWMITAG_FRAME_INTERPOLATION        'FInp'  // WM Frame Interpolation
#define DSHOWWMITAG_INTERLACE_TELECINE         'Telc'  // WM DeInterlace and Telecine
#define DSHOWWMITAG_WMRESAMP                   'ARes'  // Audio Resampler
#define DSHOWWMITAG_SEQUENCER_SOURCE           'TSrc'  // WM Timeline Source
#define DSHOWWMITAG_QM                         'QMan'  // Quality Manager
#define DSHOWWMITAG_MEIDA_PROC_SOURCE          'MPSc'  // WM Media Proc Source
#define DSHOWWMITAG_FRAME_GRABBER              'FRGB'  // WM Frame Grabber Source
#define DSHOWWMITAG_WAV_MEDIA_SINK             'WAVK'  // WAV media sink
#define DSHOWWMITAG_WAV_STREAM_SINK            'WAVk'  // WAV stream sink
#define DSHOWWMITAG_AVI_MEDIA_SINK             'AVIK'  // AVI media sink
#define DSHOWWMITAG_AVI_STREAM_SINK            'AVIk'  // AVI stream sink
#define DSHOWWMITAG_MEDIACLIP                  'MClp'  // Media Clip
#define DSHOWWMITAG_SCRIPT_SINK                'SCRK'  // Script sink
#define DSHOWWMITAG_FIRSTPASS_SINK             '1PAK'  // FirstPass sink
#define DSHOWWMITAG_RATELESSNULL_SINK          'RLNK'  // Rateless Null sink
#define DSHOWWMITAG_FILETRANSFER_SINK          'FTRK'  // File Transfer sink
#define DSHOWWMITAG_MPG_SOURCE                 'MPGS'  // MPEG Source
#define DSHOWWMITAG_AUDIOFILE_SOURCE           'AUFS'  // Audio File Source
#define DSHOWWMITAG_SCREENCAP_SOURCE           'SCPS'  // Screen Capture Source
#define DSHOWWMITAG_IMAGE_SOURCE               'IMGS'  // Image Source
#define DSHOWWMITAG_SCRIPT_SOURCE              'SCRS'  // Script Source
#define DSHOWWMITAG_SILENCE_SOURCE             'SILS'  // Silence Source
#define DSHOWWMITAG_SOLIDCOLOR_SOURCE          'SLCS'  // WM Solid Color Source
#define DSHOWWMITAG_FILETRANSFER_SOURCE        'FTRS'  // File Transfer Source
#define DSHOWWMITAG_UNMANAGED_SINK             'UNMK'  // Unmanaged wrapper sink
#define DSHOWWMITAG_EVER_SOURCE                'EVRS'  // EVeR Source
#define DSHOWWMITAG_SBE_SOURCE                 'SBES'  // SBE Media Source
#define DSHOWWMITAG_METADATA                   'Meta'  // Metadata
#define DSHOWWMITAG_METADATA_TRANSFORM         'MXFM'  // Metadata Transform
#define DSHOWWMITAG_METADATA_RULES             'MXFR'  // Metadata Rules
#define DSHOWWMITAG_SAMI_SOURCE                'SAMI'  // SAMI Media Source
#define DSHOWWMITAG_SAMPLEGRABBER_SINK         'SGSK'  // Sample Grabber Sink
#define DSHOWWMITAG_GEN_FILE_MEDIA_SINK        'GENK'  // Generic File media sink
#define DSHOWWMITAG_GEN_FILE_STREAM_SINK       'GENk'  // Generic File stream sink
#define DSHOWWMITAG_PRESENTATION_CLOCK         'PClk'  // Presentation Clock
#define DSHOWWMITAG_DEV_PLATFORM               'Pltf'  // Development Platform
#define DSHOWWMITAG_SMX_SOURCE                 'SMXS'  // SMX Media Source
#define DSHOWWMITAG_SMX_TRANSFORM              'SMXT'  // SMX Transform
#define DSHOWWMITAG_GRAPH                      'FGRP'  // Graph
#define DSHOWWMITAG_FILTER_GRAPH_CACHE         'FGCH'  // Filter graph cache
#define DSHOWWMITAG_FILTER_GRAPH_MAPPER        'FGMP'  // Filter graph mapper
#define DSHOWWMITAG_WMP_PLAYBACK               'WMPP'  // WMP Playback
#define DSHOWWMITAG_SOURCE_PROXY               'SrPx'  // Source Proxy
#define DSHOWWMITAG_DMO_WRAPPER                'DMOW'  // DMO wrapper filter
#define DSHOWWMITAG_BUFFERING                  'BUFF'  // Buffering filter
#define DSHOWWMITAG_SMARTTEE                   'STEE'  // Smart tee
#define DSHOWWMITAG_GENERIC                    'GENF'  // Generic filter/DMO
#define DSHOWWMITAG_IMGDECODER                 'IMGD'  // ImageDecoder filter

typedef ULONGLONG DSHOWWMI_PTR;

typedef ULONGLONG DSHOWWMI_HANDLE;

// Commenting these out for now: We're not differentiating which events to
// trace on the basis of what flags (yet)
/*
#define DSHOW_WMI_FLAG_AUDIO   0x01    // audio-specific processing
#define DSHOW_WMI_FLAG_VIDEO   0x02    // video-specific processing
#define DSHOW_WMI_FLAG_DATA    0x04    // non-AV specific processing
#define DSHOW_WMI_FLAG_NETWORK 0x08    // network activity
#define DSHOW_WMI_FLAG_DISKIO  0x10    // disk activity
#define DSHOW_WMI_FLAG_SOURCE_SINK 0x20    // source and sink activity (ie endpoints)
#define DSHOW_WMI_FLAG_TRANSFORM   0x40    // transform activity
#define DSHOW_WMI_FLAG_REALTIME    0x80    // real-time related events
#define DSHOW_WMI_FLAG_CLOCK   0x100   // clock data
#define DSHOW_WMI_FLAG_SAMPLES 0x200   // all samples
#define DSHOW_WMI_FLAG_OBJECTS 0x400   // all object create/delete
#define DSHOW_WMI_FLAG_STREAMINFO  0x800   // all stream and format events
*/

#pragma pack(push, 4)

typedef struct DSHOWPERFINFO_COMMON {
    EVENT_TRACE_HEADER wmiHeader;
    DSHOWWMI_PTR       pObject;
    DWORD              dwReporterTag;
} DSHOWPERFINFO_COMMON;

//
// this is the control guid for all events
//

//==============================
// Event GUIDs are defined here.
//==============================

//------------------------
// Object event
//  This event is reported when an object is created or deleted.
//

#define OBJECT_ACTIONTYPE_CREATE 1
#define OBJECT_ACTIONTYPE_DELETE 2


typedef enum _DSHOWWMI_OBJECT_TYPE
{
     DSHOWWMI_OBJECT_TYPE_UNKNOWN                 = -1,
     DSHOWWMI_OBJECT_TYPE_LOCAL_AUDIO_RENDERER    = 1,
     DSHOWWMI_OBJECT_TYPE_LOCAL_VIDEO_RENDERER    = 2,
     DSHOWWMI_OBJECT_TYPE_BYTESTREAM_SOURCE       = 3,
     DSHOWWMI_OBJECT_TYPE_VIDEO_CAPTURE_SOURCE    = 4,
     DSHOWWMI_OBJECT_TYPE_AUDIO_CAPTURE_SOURCE    = 5,
     DSHOWWMI_OBJECT_TYPE_TV_CAPTURE              = 6,
     DSHOWWMI_OBJECT_TYPE_AUDIO_DMO               = 7,
     DSHOWWMI_OBJECT_TYPE_VIDEO_DMO               = 8,
     DSHOWWMI_OBJECT_TYPE_DEMUX                   = 9,
     DSHOWWMI_OBJECT_TYPE_MUX                     = 10,
     DSHOWWMI_OBJECT_TYPE_FILE_SINK               = 11,
     DSHOWWMI_OBJECT_TYPE_NETWORK_SINK            = 12,
     DSHOWWMI_OBJECT_TYPE_COLOR_SPACE_CONVERTOR   = 13,
     DSHOWWMI_OBJECT_TYPE_NETCLIENT               = 14,
     DSHOWWMI_OBJECT_TYPE_NETQUEUE                = 15,
     DSHOWWMI_OBJECT_TYPE_FILE_SOURCE             = 16,
     DSHOWWMI_OBJECT_TYPE_GENERIC_SOURCE          = 17,
     DSHOWWMI_OBJECT_TYPE_GENERIC_SINK            = 18,
     DSHOWWMI_OBJECT_TYPE_BYTE_SINK               = 19,
     DSHOWWMI_OBJECT_TYPE_VIDEO_RECORDER          = 20,
     DSHOWWMI_OBJECT_TYPE_NETSINK                 = 21,
     DSHOWWMI_OBJECT_TYPE_DVD_SOURCE              = 22,
     DSHOWWMI_OBJECT_TYPE_NETSINK_PACKET_QUEUE    = 23,
     DSHOWWMI_OBJECT_TYPE_CONTROL_LAYER           = 24,
     DSHOWWMI_OBJECT_TYPE_APO                     = 25,
     DSHOWWMI_OBJECT_TYPE_GENERIC_TRANSFORM       = 26,
} DSHOWWMI_OBJECT_TYPE;

typedef struct _DSHOWPERFINFO_OBJECT {
    DSHOWPERFINFO_COMMON   hdr;
    DSHOWWMI_OBJECT_TYPE   eObjectType;
} DSHOWPERFINFO_OBJECT;

//------------------------
// AudioRenderer Starvation event
//

//
// Or the flag AUDIO_GLITCH_PREVIOUS_DISCONTINUITY in the type if the starvation
// follows a discontinuity or  when a stream is just started. So we can easily
// distinguish gaps from this type of starvations which is usually in-audible, i.e.
// no perceptible glitches.
// All audio components would need be updated to report the equivalent information
// so every audio glitch events are consistent.
//
#define AUDIO_GLITCH_TYPE_START (0x01)
#define AUDIO_GLITCH_TYPE_STOP  (0x02)

#define AUDIO_GLITCH_PREVIOUS_DISCONTINUITY (0x80)

typedef struct _DSHOWPERFINFO_AUDIORENDERER_STARVATION {
    DSHOWPERFINFO_COMMON   hdr;
    ULONGLONG              ullByteCount;
} DSHOWPERFINFO_AUDIORENDERER_STARVATION;

//------------------------
// Video Frame glitch event
//

typedef struct _DSHOWPERFINFO_VIDEO_FRAME_GLITCH {
    DSHOWPERFINFO_COMMON   hdr;
    DSHOWWMI_PTR           pSample;
    DWORD                  dwObjectID;
    LONGLONG               llSampleTime;
    LONGLONG               llTargetSystemTime;
    LONGLONG               llOffset; // positive = late
} DSHOWPERFINFO_VIDEO_FRAME_GLITCH;

//------------------------
// Data Drop
//
// A module that drops samples should log this event unless it is absolutely sure
// the drop is intentional and will not cause glitches. E.g
// a video capture source dropping samples during Pause is by design. There is little
// value to log the drops but filling up the log buffer.
//
// When unsure of the glitch implication of dropping data, a module should log the drop.
// A realtime monitor or postprosessing tools can be educated to tell if the drop is
// benign or not. If the drop were not logged, we'd miss the information or a problem.
//

//
// define data types, so we know what type of data is being dropped
//
enum DSHOWWMI_DATA_TYPE {
    DSHOWWMI_DATA_TYPE_AUDIO = 1,
    DSHOWWMI_DATA_TYPE_VIDEO = 2,
    DSHOWWMI_DATA_TYPE_OTHER = 3,
    DSHOWWMI_DATA_TYPE_UNKNOWN = 4,
};

//
// define reasons for data drops, append necessay new reasons without altering existing ones.
// a module should know why it drops data.
//
#define DSHOWWMI_DATA_DROP_REASON_LATE_ARRIVAL     (0x01) // sample pastdue at receive
#define DSHOWWMI_DATA_DROP_REASON_LATE_FINISH      (0x02) // process takes too long or lacks of cpu cycles
#define DSHOWWMI_DATA_DROP_REASON_BAD_DATA         (0x04) // samples are corrupted
#define DSHOWWMI_DATA_DROP_REASON_INSUFFICIENT_BIT (0x08) // allocated bits not enough
#define DSHOWWMI_DATA_DROP_REASON_NET_DATA_LOSS    (0x10) // apply to connectionless comm
#define DSHOWWMI_DATA_DROP_REASON_OVERRUN          (0x20) // samples are not taking off capture buffer timely, or mux has had an overrun
#define DSHOWWMI_DATA_DROP_REASON_STALE            (0x40) // stale data was trimmed from the ASF sink holding queue. Received during a bad state, or received too late.
#define DSHOWWMI_DATA_DROP_REASON_MUX_FAILED       (0x80) // mux failed to consume the data
#define DSHOWWMI_DATA_DROP_REASON_NOT_FLOWING     (0x100) // data not flowing
#define DSHOWWMI_DATA_DROP_REASON_NOT_KEYFRAME    (0x200) // object is not a keyframe

typedef struct _DSHOWPERFINFO_DATA_DROP {
    DSHOWPERFINFO_COMMON   hdr;
    DWORD                  dwDataType;         // Use DSHOWWMI_DATA_TYPE values
    DWORD                  dwObjectID;
    LONG                   cbDropped;
    LONGLONG               llSampleTime;
    DWORD                  dwReasons;
} DSHOWPERFINFO_DATA_DROP;

//------------------------
// ClockGetTime Event
// Report this event when the clock is queried.


typedef struct _DSHOWPERFINFO_CLOCK_GETTIME {
    DSHOWPERFINFO_COMMON   hdr;
    LONGLONG               llClockTime;
} DSHOWPERFINFO_CLOCK_GETTIME;


//------------------------
// ClockSetTime Event
// Report this event when the clock is modified by one of the filters.

typedef struct _DSHOWPERFINFO_CLOCK_SETTIME {
    DSHOWPERFINFO_COMMON   hdr;
    LONGLONG               llClockTime;
    LONGLONG               llDelta;
} DSHOWPERFINFO_CLOCK_SETTIME;


//------------------------
// Predicted video sample event
// Report this event when sample is being delivered to the stream sink

//------------------------
// Stream event
//  This event is reported when a stream is created or deleted.
//

#define STREAM_CREATE 1
#define STREAM_DELETE 2

typedef struct _DSHOWPERFINFO_STREAM {
    DSHOWPERFINFO_COMMON   hdr;
} DSHOWPERFINFO_STREAM;

//------------------------
// MEDIATYPE change event
//

#define MAXMEDIATYPE 128

typedef struct _DSHOWPERFINFO_MEDIATYPE_CHANGE {
    DSHOWPERFINFO_COMMON   hdr;
    DSHOWWMI_PTR           pStream;
    LONG                   cbNewType;
    DWORD                  dwPadding;
    BYTE                   pbNewType[MAXMEDIATYPE];
} DSHOWPERFINFO_MEDIATYPE_CHANGE;

//------------------------
// Buffer event definition:
//  This event is reported when a component receives or sends a sample buffer.
//  These events are essential to track buffer latency which will also assist
//  in glitch analysis.
//

#define DSHOWPERFINFO_BUFFER_INPUT     1
#define DSHOWPERFINFO_BUFFER_OUTPUT    2
#define DSHOWPERFINFO_BUFFER_QUEUED    3
#define DSHOWPERFINFO_BUFFER_DROPPED   4

typedef struct _DSHOWPERFINFO_BUFFER {
    DSHOWPERFINFO_COMMON hdr;
    DWORD                dwObjectCategory;
    DWORD                cbUsedBuffer;     // used buffer space
    DSHOWWMI_PTR         pStream;
    LONGLONG             llTimestamp;
    DSHOWWMI_PTR         pBuffer;
    LONGLONG             llDuration;        // for DSHOWPERFINFO_BUFFER_OUTPUT, llDuration contains the processing time for the sample
} DSHOWPERFINFO_BUFFER;


typedef struct _DSHOWPERFINFO_UNUSUAL_STREAMING_EVENT {
    DSHOWPERFINFO_COMMON hdr;
    DWORD              dwEvent;
} DSHOWPERFINFO_UNUSUAL_STREAMING_EVENT;

#define DSHOWWMI_UNUSUAL_STREAMING_TYPE_START    1
#define DSHOWWMI_UNUSUAL_STREAMING_TYPE_END      2
#define DSHOWWMI_UNUSUAL_STREAMING_TYPE_ONESHOT  3

#define DSHOWWMI_UNUSUAL_STREAMING_EVENT_DMO_FLUSH          1
#define DSHOWWMI_UNUSUAL_STREAMING_EVENT_DMO_DISCONTINUITY  2
#define DSHOWWMI_UNUSUAL_STREAMING_EVENT_WAVEOUT_START      3
#define DSHOWWMI_UNUSUAL_STREAMING_EVENT_WAVEOUT_DELAYSTART 4

#define DSHOWWMI_DISKIO_READ  1
#define DSHOWWMI_DISKIO_WRITE 2
#define DSHOWWMI_DISKIO_SEEK  3
typedef struct DSHOWPERFINFO_DISKIO_REQUEST {
    DSHOWPERFINFO_COMMON hdr;
    DSHOWWMI_HANDLE      hFile;
    ULONGLONG            llOffset;
    ULONG                byteCount;
} DSHOWPERFINFO_DISKIO_REQUEST;


typedef struct _DSHOWPERFINFO_DISKIO_COMPLETE {
    DSHOWPERFINFO_COMMON hdr;
    DSHOWWMI_HANDLE      hFile;
} DSHOWPERFINFO_DISKIO_COMPLETE;


typedef struct _DSHOWPERFINFO_FILE_OPEN {
    DSHOWPERFINFO_COMMON hdr;
    DSHOWWMI_HANDLE      hFile;
    WCHAR                szName[1];
} DSHOWPERFINFO_FILE_OPEN;


typedef struct _DSHOWPERFINFO_FILE_CLOSE {
    DSHOWPERFINFO_COMMON hdr;
    DSHOWWMI_HANDLE      hFile;
    WCHAR                szName[1];
} DSHOWPERFINFO_FILE_CLOSE;


#define DSHOW_REND_TYPE_ALLOC_SAMPLE       0x4
#define DSHOW_REND_TYPE_RECV_SAMPLE        0x1
#define DSHOW_REND_TYPE_START_REND         0x2
#define DSHOW_REND_TYPE_END_REND           0x3
#define DSHOW_REND_TYPE_START_MIX          0x6
#define DSHOW_REND_TYPE_END_MIX            0x7
#define DSHOW_REND_TYPE_START_DECODE       0x8
#define DSHOW_REND_TYPE_END_DECODE         0x9
#define DSHOW_REND_TYPE_START_DEINTERLACE  0xA
#define DSHOW_REND_TYPE_END_DEINTERLACE    0xB
#define DSHOW_REND_TYPE_START_PROCAMP      0xC
#define DSHOW_REND_TYPE_END_PROCAMP        0xD
#define DSHOW_REND_TYPE_START_FRC          0xE
#define DSHOW_REND_TYPE_END_FRC            0xF
#define DSHOW_REND_TYPE_MID_REND          0x10 // half point between START_REND and END_REND (used by windowed mode slicer)
#define DSHOW_REND_TYPE_MAP_REND          0x11 // Mapping of presentation time to system time when a sample is received

// distinguish audio, video via dwDataType
typedef struct _DSHOWPERFINFO_RENDERER_REND {
    DSHOWPERFINFO_COMMON hdr;
    DSHOWWMI_PTR         pSample;
    LONGLONG             llSampleTime;
    LONGLONG             llSampleDuration;
    LONGLONG             llClockTime;
} DSHOWPERFINFO_RENDERER_REND, DSHOWPERFINFO_RENDERER_MIX;


typedef struct _DSHOWPERFINFO_AUDIORENDERER_BUFFERFULLNESS {
    DSHOWPERFINFO_COMMON hdr;
    DWORD                msBuffer;
} DSHOWPERFINFO_AUDIORENDERER_BUFFERFULLNESS;

#define DSHOWWMI_STATECHANGE_STOP  1
#define DSHOWWMI_STATECHANGE_PAUSE 2
#define DSHOWWMI_STATECHANGE_START 3
typedef struct _DSHOWPERFINFO_STATE_CHANGE{
    DSHOWPERFINFO_COMMON hdr;
} DSHOWPERFINFO_STATE_CHANGE;


//-------------------------
// Netsource events

#define DSHOWWMI_NETSOURCE_DISCONNECT         1
#define DSHOWWMI_NETSOURCE_RECONNECT_START    2
#define DSHOWWMI_NETSOURCE_RECONNECT_STOP     3
#define DSHOWWMI_NETSOURCE_ANNOUNCE           4
#define DSHOWWMI_NETSOURCE_EOS                5
#define DSHOWWMI_NETSOURCE_STREAMSWITCH       6
#define DSHOWWMI_NETSOURCE_STREAMSWITCH_DONE  7
#define DSHOWWMI_NETSOURCE_BUFFERING_START    8
#define DSHOWWMI_NETSOURCE_BUFFERING_STOP     9
#define DSHOWWMI_NETSOURCE_CONNECT_START      10
#define DSHOWWMI_NETSOURCE_CONNECT_STOP       11
#define DSHOWWMI_NETSOURCE_OPEN_START         12
#define DSHOWWMI_NETSOURCE_OPEN_STOP          13
#define DSHOWWMI_NETSOURCE_MCAST_JOIN_DONE    14

typedef struct DSHOWPERFINFO_NETSOURCE {
        DSHOWPERFINFO_COMMON   hdr;
        DWORD Arg1;
        DWORD Arg2;
} DSHOWPERFINFO_NETSOURCE;

//-------------------------
// HTTP Bytestream events

#define DSHOWWMI_HTTP_BYTESTREAM_OPEN_START        1
#define DSHOWWMI_HTTP_BYTESTREAM_OPEN_STOP         2

typedef struct DSHOWPERFINFO_HTTP_BYTESTREAM {
        DSHOWPERFINFO_COMMON   hdr;
} DSHOWPERFINFO_HTTP_BYTESTREAM;

//-------------------------
// SourceResolver events
// To measure how long the Source Resolver takes in its various steps

#define DSHOWWMI_SOURCERESOLUTION_START_CREATION                               1
#define DSHOWWMI_SOURCERESOLUTION_END_CREATION                                 2

#define DSHOWWMI_SOURCERESOLUTION_START_BEGINCREATEOBJECTFROMURL               3
#define DSHOWWMI_SOURCERESOLUTION_END_BEGINCREATEOBJECTFROMURL                 4

#define DSHOWWMI_SOURCERESOLUTION_START_ENDCREATEOBJECTFROMURL                 5
#define DSHOWWMI_SOURCERESOLUTION_END_ENDCREATEOBJECTFROMURL                   6

#define DSHOWWMI_SOURCERESOLUTION_START_BEGINCREATEOBJECTFROMBYTESTREAM        7
#define DSHOWWMI_SOURCERESOLUTION_END_BEGINCREATEOBJECTFROMBYTESTREAM          8

#define DSHOWWMI_SOURCERESOLUTION_START_ENDCREATEOBJECTFROMBYTESTREAM          9
#define DSHOWWMI_SOURCERESOLUTION_END_ENDCREATEOBJECTFROMBYTESTREAM            10

#define DSHOWWMI_SOURCERESOLUTION_START_LOOKUPSCHEMEHANDLER                    11
#define DSHOWWMI_SOURCERESOLUTION_END_LOOKUPSCHEMEHANDLER                      12

#define DSHOWWMI_SOURCERESOLUTION_START_LOOKUPBYTESTREAMHANDLER                13
#define DSHOWWMI_SOURCERESOLUTION_END_LOOKUPBYTESTREAMHANDLER                  14

#define DSHOWWMI_SOURCERESOLUTION_START_SCHEMEHANDLER_BEGINCREATEOBJECT        15
#define DSHOWWMI_SOURCERESOLUTION_END_SCHEMEHANDLER_BEGINCREATEOBJECT          16

#define DSHOWWMI_SOURCERESOLUTION_START_SCHEMEHANDLER_ENDCREATEOBJECT          17
#define DSHOWWMI_SOURCERESOLUTION_END_SCHEMEHANDLER_ENDCREATEOBJECT            18

#define DSHOWWMI_SOURCERESOLUTION_BYTESTREAMCREATED                            19

#define DSHOWWMI_SOURCERESOLUTION_START_BYTESTREAMHANDLER_BEGINCREATEOBJECT    20
#define DSHOWWMI_SOURCERESOLUTION_END_BYTESTREAMHANDLER_BEGINCREATEOBJECT      21

#define DSHOWWMI_SOURCERESOLUTION_START_BYTESTREAMHANDLER_ENDCREATEOBJECT      22
#define DSHOWWMI_SOURCERESOLUTION_END_BYTESTREAMHANDLER_ENDCREATEOBJECT        23

#define DSHOWWMI_SOURCERESOLUTION_MEDIASOURCECREATED                           24


typedef struct DSHOWPERFINFO_SOURCERESOLUTION {
    DSHOWPERFINFO_COMMON   hdr;
    LPCWSTR             pwszURL;
    PVOID               pObjectCreated;
    HRESULT             hr;
}   DSHOWPERFINFO_SOURCERESOLUTION;

//-------------------------
// Quality Manager events
// To indicate when quality goes up and down


#define DSHOWWMI_QM_SetQualityLevel                      1
#define DSHOWWMI_QM_SetDropMode                       2
#define DSHOWWMI_QM_SetDropTime             3

#define DSHOWWMI_QM_DECODER_KNOB   1
#define DSHOWWMI_QM_VIDEOSINK_KNOB 2
#define DSHOWWMI_QM_SOURCE_KNOB 3

typedef struct DSHOWPERFINFO_QM {
    DSHOWPERFINFO_COMMON   hdr;
    DWORD               dwKnobId;
    DWORD               dwPrevLevel;
    DWORD               dwNewLevel;    
    LONGLONG           llDropTime;
} DSHOWPERFINFO_QM;


//-------------------------
// WMILOG_ADJUST_SAMPLE_TIME
//
// If a module adjusts sample presentation times, it must log the adjustment to the samples.
// We rely heavily on sample times to detect glitches and to measure latency.
// Therefore, this event is crucial. However, rebuffering that requres new time satmps
// but involves no adjustments needs not log this event.
// E.g. a module receives a buffer of samples with timestamp s that is worth time t, i.e.
// (timestamp, duration) = (s, t). If it outputs the samples in two halves, 1st with
// (s, t/2) and 2nd with (s+t/2, t/2), there is no adjustment but new labelings, it needs
// not log this event.
// If a module applys a constant delta to sample times, it reports this event once with the starting
// sample time and type DSHOWWMI_ADJUST_SAMPLE_TIME_REPEAT until it uses a different adjustment.
// Oneshot adjustment applies to the timestamp in the event only.
//

#define DSHOWWMI_ADJUST_SAMPLE_TIME_ONESHOT (1)
#define DSHOWWMI_ADJUST_SAMPLE_TIME_REPEAT  (2)

typedef struct DSHOWPERFINFO_ADJUST_SAMPLE_TIME {
    DSHOWPERFINFO_COMMON   hdr;
    LONGLONG        llOriginalSampleTime;
    LONGLONG        llAdjustment;
}   DSHOWPERFINFO_ADJUST_SAMPLE_TIME;

//------------------------
// WMILOG_VIDEO_RENDER
//
// The renderer event for both audio and video are diverging
// in the data required. We are using a separate video render
// event here to avoid extraneous fileds for audio render.
// We need vren event to have window coordinates and Video window handle
// so we can match the bitblt events reported dxg.
// We need dxg events so we can more acuratly track actual presentation
// time of video frames. Dxg scanline events also provide us a mean to
// calculate Video clock.
//

typedef struct DSHOWPERFINFO_VIDEO_RENDER {
    DSHOWPERFINFO_COMMON   hdr;
    DSHOWWMI_PTR           pSample;
    LONGLONG            llSampleTime;
    LONGLONG            llSampleDuration;
    LONGLONG            llClockTime;
    DSHOWWMI_HANDLE        hwndVideo;
    DWORD               dwRefreshRate;
    DWORD               dwWidth;
    DWORD               dwHeight;
    DWORD               dwLeft;
    DWORD               dwTop;
    DWORD               dwRight;
    DWORD               dwBottom;
    DWORD               dwLeft1;
    DWORD               dwTop1;
    DWORD               dwRight1;
    DWORD               dwBottom1;
} DSHOWPERFINFO_VIDEO_RENDER;

//
// This event contains a superset of fileds for Renderer_Rend event. We need this
// audio specific event to have master clock and time of the sample being rendered
// so we can compute audio jitters which are needed to computer AVSync. If the audio
// renderer is chosen as the master clock, the pair of times are DONT_CARE_TIME since
// we don't need them for the AVSync calculation.
//
typedef struct _DSHOWPERFINFO_AUDIO_RENDER {
    DSHOWPERFINFO_COMMON hdr;
    DSHOWWMI_PTR         pSample;
    LONGLONG          llSampleTime;
    LONGLONG          llSampleDuration;
    LONGLONG          llMasterTime;
    LONGLONG          llDeviceTime;
} DSHOWPERFINFO_AUDIO_RENDER;


//-------------------------
// Muxer Events

#define DSHOWWMI_Muxer_ProcessSample        (1)
#define DSHOWWMI_Muxer_AddPayload           (2)
#define DSHOWWMI_Muxer_OnCompletedPacket    (3)

typedef struct DSHOWPERFINFO_MUXER
{
    DSHOWPERFINFO_COMMON   hdr;
    DWORD wStreamNumber;
    LONGLONG llSampleTime;
    DWORD cbSample;
    LONGLONG llPacketNumber;
    LONGLONG llPacketSendTime;
    DWORD cbPacket;
} DSHOWPERFINFO_MUXER;


//------------------------
// Timer Late event

typedef struct _DSHOWPERFINFO_TIMER_LATE {
    DSHOWPERFINFO_COMMON   hdr;
    LONGLONG            llEventTime;
    LONGLONG            llLateBy;
    DWORD               dwObjectID;
} DSHOWPERFINFO_TIMER_LATE;


//-------------------------
// Lock event

#define DSHOWWMI_Lock_TryAcquire   (1)
#define DSHOWWMI_Lock_Acquired     (2)
#define DSHOWWMI_Lock_Released     (3)

typedef struct DSHOWPERFINFO_LOCK
{
    DSHOWPERFINFO_COMMON   hdr;
    PVOID pLockObject;
} DSHOWPERFINFO_LOCK;


//-------------------------
// Graph events

#define DSHOWWMI_GRAPH_START_RUN                                             1
#define DSHOWWMI_GRAPH_END_RUN                                               2
#define DSHOWWMI_GRAPH_START_STOP                                            3
#define DSHOWWMI_GRAPH_END_STOP                                              4
#define DSHOWWMI_GRAPH_START_PAUSE                                           5
#define DSHOWWMI_GRAPH_END_PAUSE                                             6
#define DSHOWWMI_GRAPH_START_CACHE_LOAD                                      7
#define DSHOWWMI_GRAPH_END_CACHE_LOAD                                        8
#define DSHOWWMI_GRAPH_START_CACHE_UPDATE                                    9
#define DSHOWWMI_GRAPH_END_CACHE_UPDATE                                     10
#define DSHOWWMI_GRAPH_START_RENDERFILE                                     11
#define DSHOWWMI_GRAPH_END_RENDERFILE                                       12
#define DSHOWWMI_GRAPH_START_ADD_SOURCE_FILTER                              13
#define DSHOWWMI_GRAPH_END_ADD_SOURCE_FILTER                                14
#define DSHOWWMI_GRAPH_START_ADD_FILTER                                     15
#define DSHOWWMI_GRAPH_END_ADD_FILTER                                       16
#define DSHOWWMI_GRAPH_START_COCREATE_FILTER                                17
#define DSHOWWMI_GRAPH_END_COCREATE_FILTER                                  18
#define DSHOWWMI_GRAPH_START_GET_URL                                        19
#define DSHOWWMI_GRAPH_END_GET_URL                                          20

typedef struct DSHOWPERFINFO_GRAPH {
    DSHOWPERFINFO_COMMON   hdr;
    HRESULT             hr;
}   DSHOWPERFINFO_GRAPH;


#pragma pack(pop)

#ifdef __cplusplus
}
#endif



