//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft
// premium shared source license agreement under which you licensed
// this source code. If you did not accept the terms of the license
// agreement, you are not authorized to use this source code.
// For the terms of the license, please see the license agreement
// signed by you and Microsoft.
// THE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//
/*  Shell error codes - should this be a .mc file? */

#ifndef _MEDIAERR_H_
#define _MEDIAERR_H_

#define DMO_E_INVALIDSTREAMINDEX 0x80040201
#define DMO_E_INVALIDTYPE        0x80040202
#define DMO_E_TYPE_NOT_SET       0x80040203
#define DMO_E_NOTACCEPTING       0x80040204
#define DMO_E_TYPE_NOT_ACCEPTED  0x80040205
#define DMO_E_NO_MORE_ITEMS      0x80040206

#endif _MEDIAERR_H_
