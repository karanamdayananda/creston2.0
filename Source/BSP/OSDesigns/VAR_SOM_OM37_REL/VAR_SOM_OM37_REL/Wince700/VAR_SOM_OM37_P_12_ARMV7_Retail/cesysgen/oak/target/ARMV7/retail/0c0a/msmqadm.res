        ��  ��                  
      �� ��     0
          < U s o : 
 	 m s m q a d m   < c o m a n d o >   < p a r � m e t r o s > 
 	 o   b i e n : 
 	 m s m q a d m   h e l p   � C o m a n d o s : 
 	 h e l p :   i m p r i m i r   e s t e   t e x t o 
 	 m a n :   m o d o   d e   c o m a n d o   m a n u a l 
 	 e n u m   q u e u e s :   e n u m e r a r   l a s   c o l a s 
 	 e n u m   m e s s a g e s   < n � m e r o   d e   c o l a > :   e n u m e r a r   l o s   m e n s a j e s   d e   u n a   c o l a    E r r o r :   m e m o r i a   i n s u f i c i e n t e .   ( E r r o r :   n o   s e   p u e d e   a b r i r   e l   a r c h i v o   % s 
   ( N o   h a y   u n   s e r v i d o r   M S M Q   e n   e s t e   s i s t e m a   I N � m e r o   d e   m e n s a j e   n o   v � l i d o   % s .   C o m p r u e b e   l a   n u m e r a c i � n   d e   l o s   m e n s a j e s .   B N � m e r o   d e   c o l a   n o   v � l i d o   % s .   C o m p r u e b e   l a   n u m e r a c i � n   d e   l a s   c o l a s   M E r r o r   d e   s i n t a x i s .   C o n s u l t e   l a   a y u d a   p a r a   r e c u p e r a r   l a   s i n t a x i s   d e l   c o m a n d o .   = E r r o r   e n   l a   o p e r a c i � n .   N o   h a y   u n   c � d i g o   d e   e r r o r   d i s p o n i b l e .   , E r r o r   e n   l a   o p e r a c i � n .   C � d i g o   d e   e r r o r   % 0 8 x    C o l a s   a b i e r t a s :     & E s p e c i f i c a c i � n   d e   G U I D   i n c o r r e c t a   % s .    N o   s e   p u e d e   c r e a r   % s .    E l   a r c h i v o   % s   y a   e x i s t e .     �      �� ��     0
         S    N   � Y a   e x i s t e   e l   d i r e c t o r i o   % s   y   p u e d e   q u e   c o n t e n g a   i n f o r m a c i � n   q u e   d a � e   u n a   i n s t a l a c i � n   n u e v a .   C r e e   u n a   c o p i a   d e   s e g u r i d a d   d e l   d i r e c t o r i o   y   q u � t e l o .   = E r r o r :   n o   s e   p u e d e   c r e a r   u n a   c l a v e   d e   r e g i s t r o .   E s t a d o   % 0 8 x .   @ E r r o r :   n o   s e   p u e d e   d e f i n i r   e l   v a l o r   d e   l a   c l a v e   % s .   E s t a d o   % 0 8 x   9 E r r o r :   n o   s e   p u e d e n   i n i c i a l i z a r   l o s   s o c k e t s .   E s t a d o   % 0 8 x   1 E r r o r :   n o   s e   p u e d e   o b t e n e r   i n f o r m a c i � n   d e l   h o s t .   � U t i l i c e : 
 	 r e g i s t e r   { g u i d   < g u i d > ,   d i r   < d i r e c t o r i o   b a s e > ,   f r s   < n o m b r e   d e l   s e r v i d o r > ,   b i n a r y ,   s m t p ,   t r u s t }   p a r a   e s t a b l e c e r   p a r � m e t r o s   d e l   r e g i s t r o .   � U t i l i c e : 
 	 r e g i s t e r   c h a n g e   { p o r t   < n � m e r o > ,   p i n g p o r t   < n � m e r o > ,   n e t r e t r y   < n � m e r o > , 
 	 	 c o n n e c t r e t r y   < n � m e r o s > ,   q u o t a   < n � m e r o ,   e n   K > ,   d i r   < d i r e c t o r i o   b a s e > ,   f r s   < s e r v i d o r   f r s > }     p a r a   c a m b i a r   p a r � m e t r o s   d e l   r e g i s t r o .   ? N o   s e   p u e d e   a b r i r   l a   c l a v e   d e l   r e g i s t r o .   D e b e   c r e a r l a   p r i m e r o .   � 	 e m p t y   q u e u e   < n � m e r o   d e   c o l a > :   v a c i a r   c o l a 
 	 d e l e t e   q u e u e   < n � m e r o   d e   c o l a > :   e l i m i n a r   c o l a 
 	 d e l e t e   m e s s a g e   < n � m e r o   d e   m e n s a j e > :   e l i m i n a r   m e n s a j e   � 	 n e t   c o n n e c t :   f o r z a r   l a   b � s q u e d a   d e   c o n e x i o n e s 
 	 n e t   d i s c o n n e c t :   f o r z a r   l a   p a r a d a   d e   t o d a s   l a s   c o n e x i o n e s 
 	 n e t   t r a c k   o n / o f f :   h a b i l i t a r   o   d e s h a b i l i t a r   e l   s e g u i m i e n t o   d e   r e d   � 	 s t o p :   d e t e n e r   e l   s e r v i c i o   M S M Q 
 	 s t a r t :   i n i c i a r   e l   s e r v i c i o   M S M Q 
 	 c o n s o l e :   i n i c i a r   l a   c o n s o l a   d e   M S M Q 
 	 s t a t u s :   o b t e n e r   e l   e s t a d o   d e l   e q u i p o 
 	 t i d y :   f o r z a r   e l   m a n t e n i m i e n t o   d e   l o s   r e c u r s o s   � 	 r e g i s t e r :   c o n f i g u r a r   e l   s e r v i c i o   M S M Q 
 	 l o g s i z e   < n � m e r o > :   t a m a � o   d e l   a r c h i v o   d e   r e g i s t r o   e n   K   ( 0   e s   i n f i n i t o ) 
 . . . d o n d e   l o s   n � m e r o s   d e   m e n s a j e   o   c o l a   s o n   l o s   d e   l a   � l t i m a   e n u m e r a c i � n . 
 M a r c a s : 
 	 - s :   e j e c u c i � n   n o   i n t e r a c t i v a ,   s i n   r e s u l t a d o .   A D e b e   r e i n i c i a r   e l   s e r v i c i o   M S M Q   p a r a   q u e   e l   c a m b i o   s e a   e f e c t i v o .   * U s o : 
 	 d e b u g   [ r e s u l t a d o ]   { o n ,   o f f }   < l i s t a >   �      �� ��     0
         L i s t a   p a r a   e l   r e s u l t a d o :    L i s t a   p a r a   l o s   m o d o s :   � o   b i e n , 
 	 r e g i s t r e   [ u n ] i n s t a l l   < s e r v i c i o >   p a r a   i n s t a l a r   o   d e s i n s t a l a r   M S M Q 
 	 	   y   s i   i n s t a l a   M S M Q   c o m o   c o n t r o l a d o r   ( p r e d e t e r m i n a d o )   o   s e r v i c i o .   f o   b i e n , 
 	 r e g i s t e r   c l e a n u p   p a r a   q u i t a r   t o d o s   l o s   r a s t r o s   d e   M S M Q   ( i n c l u i d o s   l o s   m e n s a j e s )   d e l   s i s t e m a .     E r r o r :   n o m b r e   d e   F R S   n o   v � l i d o .   R E r r o r :   y a   e x i s t e   l a   c l a v e   d e l   r e g i s t r o .   U t i l i c e   " r e g i s t e r   c h a n g e "   p a r a   c a m b i a r l a .   � 	 e n a b l e   { s r m p ,   b i n a r y ,   t r u s t }   -   h a b i l i t a r   p r o t o c o l o s   M S M Q   y   n i v e l   d e   c o n f i a n z a   d e   l a   r e d 
 	 d i s a b l e   { s r m p ,   b i n a r y ,   t r u s t }   -   d e s h a b i l i t a r   p r o t o c o l o s   M S M Q   o   c o n v e r t i r   l a   r e d   e n   n o   c o n f i a b l e 
                       