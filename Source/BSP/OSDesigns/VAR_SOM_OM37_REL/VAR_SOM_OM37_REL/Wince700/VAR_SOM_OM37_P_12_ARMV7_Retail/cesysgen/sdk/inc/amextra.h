//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft
// premium shared source license agreement under which you licensed
// this source code. If you did not accept the terms of the license
// agreement, you are not authorized to use this source code.
// For the terms of the license, please see the license agreement
// signed by you and Microsoft.
// THE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//
//==========================================================================;
//
//  THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
//  KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
//  PURPOSE.
//
//
//--------------------------------------------------------------------------;

#pragma once

// Simple rendered input pin
//
// NOTE if your filter queues stuff before rendering then it may not be
// appropriate to use this class
//
// In that case queue the end of stream condition until the last sample
// is actually rendered and flush the condition appropriately

class CRenderedInputPin : public CBaseInputPin
{
public:
    CRenderedInputPin(TCHAR *pObjectName,
                      CBaseFilter *pFilter,
                      CCritSec *pLock,
                      HRESULT *phr,
                      LPCWSTR pName);

    // Override methods to track end of stream state
    STDMETHODIMP EndOfStream();
    STDMETHODIMP EndFlush();

    HRESULT Active();
    HRESULT Run(REFERENCE_TIME tStart);

protected:
    // Member variables to track state
    BOOL m_bAtEndOfStream;      // Set by EndOfStream
    BOOL m_bCompleteNotified;   // Set when we notify for EC_COMPLETE

private:
    void DoCompleteHandling();
};

class CMSWrapper : public IMediaSample
{
public:
    CMSWrapper();
    ~CMSWrapper();

    STDMETHOD_(ULONG, AddRef )( void );
    STDMETHOD_(ULONG, Release )( void );
    STDMETHOD( QueryInterface )( REFIID riid, void** ppvObject );

    STDMETHOD_( long, GetSize )( void );
    STDMETHOD_( long, GetActualDataLength )( void );
    STDMETHOD( GetPointer )( BYTE **ppBuffer );
    STDMETHOD( GetTime )( REFERENCE_TIME *pTimeStart, REFERENCE_TIME *pTimeEnd );
    STDMETHOD( SetTime )( REFERENCE_TIME *pTimeStart, REFERENCE_TIME *pTimeEnd );
    STDMETHOD( IsSyncPoint )( void );
    STDMETHOD( SetSyncPoint )( BOOL bIsSyncPoint );
    STDMETHOD( IsPreroll )( void );
    STDMETHOD( SetPreroll )( BOOL bIsPreroll );
    STDMETHOD( SetActualDataLength )( long );
    STDMETHOD( GetMediaType )( AM_MEDIA_TYPE **ppMediaType );
    STDMETHOD( SetMediaType )( AM_MEDIA_TYPE *pMediaType );
    STDMETHOD( IsDiscontinuity )( void );
    STDMETHOD( SetDiscontinuity )( BOOL bDiscontinuity );
    STDMETHOD( GetMediaTime )( LONGLONG *pTimeStart, LONGLONG *pTimeEnd );
    STDMETHOD( SetMediaTime )( LONGLONG *pTimeStart, LONGLONG *pTimeEnd );

    HRESULT SetMediaSample( IMediaSample *pSample, LONG lOffset, LONG lSize );

private:
    LONG  m_cRef;
    LONG  m_lSize;
    DWORD m_dwOffset;
    BOOL  m_fIsSyncPoint;
    BOOL  m_fTime;
    BOOL  m_fIsDiscontinuity;
    REFERENCE_TIME m_tStart, m_tEnd;
    LONGLONG m_llMediaStart, m_llMediaEnd;
    IMediaSample* m_pMediaSample;
};
