

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0543 */
/* Compiler settings for userdata.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __userdata_h__
#define __userdata_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IWMVAUserData_FWD_DEFINED__
#define __IWMVAUserData_FWD_DEFINED__
typedef interface IWMVAUserData IWMVAUserData;
#endif 	/* __IWMVAUserData_FWD_DEFINED__ */


#ifndef __IWMVBIDataConfig_FWD_DEFINED__
#define __IWMVBIDataConfig_FWD_DEFINED__
typedef interface IWMVBIDataConfig IWMVBIDataConfig;
#endif 	/* __IWMVBIDataConfig_FWD_DEFINED__ */


/* header files for imported files */
#include "unknwn.h"
#include "objidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


/* interface __MIDL_itf_userdata_0000_0000 */
/* [local] */ 

typedef 
enum WMVAUserDataLevel
    {	levelSequence	= 1,
	levelEntryPoint	= 2,
	levelFrame	= 4,
	levelField	= 8,
	levelSlice	= 16
    } 	UserDataLevel;

typedef void ( *PFNCB )( 
    BYTE *pbData,
    DWORD dwLength,
    UserDataLevel udLevel,
    DWORD dwContext);



extern RPC_IF_HANDLE __MIDL_itf_userdata_0000_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_userdata_0000_0000_v0_0_s_ifspec;

#ifndef __IWMVAUserData_INTERFACE_DEFINED__
#define __IWMVAUserData_INTERFACE_DEFINED__

/* interface IWMVAUserData */
/* [unique][helpstring][uuid][local][object] */ 


EXTERN_C const IID IID_IWMVAUserData;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("1793D585-35BF-49F9-8F84-39BD249A5B5A")
    IWMVAUserData : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RegisterCallback( 
            /* [in] */ PFNCB pfnCallback,
            /* [in] */ DWORD dwContext,
            /* [in] */ DWORD dwLevelMask) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IWMVAUserDataVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IWMVAUserData * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IWMVAUserData * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IWMVAUserData * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *RegisterCallback )( 
            IWMVAUserData * This,
            /* [in] */ PFNCB pfnCallback,
            /* [in] */ DWORD dwContext,
            /* [in] */ DWORD dwLevelMask);
        
        END_INTERFACE
    } IWMVAUserDataVtbl;

    interface IWMVAUserData
    {
        CONST_VTBL struct IWMVAUserDataVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IWMVAUserData_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IWMVAUserData_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IWMVAUserData_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IWMVAUserData_RegisterCallback(This,pfnCallback,dwContext,dwLevelMask)	\
    ( (This)->lpVtbl -> RegisterCallback(This,pfnCallback,dwContext,dwLevelMask) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IWMVAUserData_INTERFACE_DEFINED__ */


#ifndef __IWMVBIDataConfig_INTERFACE_DEFINED__
#define __IWMVBIDataConfig_INTERFACE_DEFINED__

/* interface IWMVBIDataConfig */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IWMVBIDataConfig;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("9CFF19C8-334D-4469-B5DD-042E89A09172")
    IWMVBIDataConfig : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetLevelMask( 
            /* [in] */ DWORD dwLevelMask) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetLevelMask( 
            /* [in] */ DWORD *pdwLevelMask) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IWMVBIDataConfigVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IWMVBIDataConfig * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IWMVBIDataConfig * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IWMVBIDataConfig * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetLevelMask )( 
            IWMVBIDataConfig * This,
            /* [in] */ DWORD dwLevelMask);
        
        HRESULT ( STDMETHODCALLTYPE *GetLevelMask )( 
            IWMVBIDataConfig * This,
            /* [in] */ DWORD *pdwLevelMask);
        
        END_INTERFACE
    } IWMVBIDataConfigVtbl;

    interface IWMVBIDataConfig
    {
        CONST_VTBL struct IWMVBIDataConfigVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IWMVBIDataConfig_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IWMVBIDataConfig_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IWMVBIDataConfig_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IWMVBIDataConfig_SetLevelMask(This,dwLevelMask)	\
    ( (This)->lpVtbl -> SetLevelMask(This,dwLevelMask) ) 

#define IWMVBIDataConfig_GetLevelMask(This,pdwLevelMask)	\
    ( (This)->lpVtbl -> GetLevelMask(This,pdwLevelMask) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IWMVBIDataConfig_INTERFACE_DEFINED__ */


/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


