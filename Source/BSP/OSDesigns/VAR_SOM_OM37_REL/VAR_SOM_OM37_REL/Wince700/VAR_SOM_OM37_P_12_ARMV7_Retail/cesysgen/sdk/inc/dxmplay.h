

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0543 */
/* Compiler settings for dxmplay.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __dxmplay_h__
#define __dxmplay_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IDSPlayerEx_FWD_DEFINED__
#define __IDSPlayerEx_FWD_DEFINED__
typedef interface IDSPlayerEx IDSPlayerEx;
#endif 	/* __IDSPlayerEx_FWD_DEFINED__ */


#ifndef __IMediaPlayer_FWD_DEFINED__
#define __IMediaPlayer_FWD_DEFINED__
typedef interface IMediaPlayer IMediaPlayer;
#endif 	/* __IMediaPlayer_FWD_DEFINED__ */


#ifndef __IMediaPlayer2_FWD_DEFINED__
#define __IMediaPlayer2_FWD_DEFINED__
typedef interface IMediaPlayer2 IMediaPlayer2;
#endif 	/* __IMediaPlayer2_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"
#include "enums.h"
#include "mpdvd.h"

#ifdef __cplusplus
extern "C"{
#endif 


/* interface __MIDL_itf_dxmplay_0000_0000 */
/* [local] */ 

typedef /* [public][uuid] */  DECLSPEC_UUID("66504301-BE0F-101A-8BBB-00AA00300CAB") DWORD VB_OLE_COLOR;

typedef /* [public] */ 
enum MPDisplayModeConstants
    {	mpTime	= 0,
	mpFrames	= ( mpTime + 1 ) 
    } 	MPDisplayModeConstants;

typedef /* [public] */ 
enum MPPlayStateConstants
    {	mpStopped	= 0,
	mpPaused	= ( mpStopped + 1 ) ,
	mpPlaying	= ( mpPaused + 1 ) ,
	mpWaiting	= ( mpPlaying + 1 ) ,
	mpScanForward	= ( mpWaiting + 1 ) ,
	mpScanReverse	= ( mpScanForward + 1 ) ,
	mpClosed	= ( mpScanReverse + 1 ) 
    } 	MPPlayStateConstants;

typedef /* [public] */ 
enum MPfDialogAvailability
    {	mpfFilePropertiesDlg	= 1,
	mpfGotoDlg	= 2
    } 	MPfDialogAvailability;

typedef /* [public] */ 
enum MPMoreInfoType
    {	mpShowURL	= 0,
	mpClipURL	= ( mpShowURL + 1 ) ,
	mpBannerURL	= ( mpClipURL + 1 ) 
    } 	MPMoreInfoType;

typedef /* [public] */ 
enum MPMediaInfoType
    {	mpShowFilename	= 0,
	mpShowTitle	= ( mpShowFilename + 1 ) ,
	mpShowAuthor	= ( mpShowTitle + 1 ) ,
	mpShowCopyright	= ( mpShowAuthor + 1 ) ,
	mpShowRating	= ( mpShowCopyright + 1 ) ,
	mpShowDescription	= ( mpShowRating + 1 ) ,
	mpShowLogoIcon	= ( mpShowDescription + 1 ) ,
	mpClipFilename	= ( mpShowLogoIcon + 1 ) ,
	mpClipTitle	= ( mpClipFilename + 1 ) ,
	mpClipAuthor	= ( mpClipTitle + 1 ) ,
	mpClipCopyright	= ( mpClipAuthor + 1 ) ,
	mpClipRating	= ( mpClipCopyright + 1 ) ,
	mpClipDescription	= ( mpClipRating + 1 ) ,
	mpClipLogoIcon	= ( mpClipDescription + 1 ) ,
	mpBannerImage	= ( mpClipLogoIcon + 1 ) ,
	mpBannerMoreInfo	= ( mpBannerImage + 1 ) ,
	mpWatermark	= ( mpBannerMoreInfo + 1 ) 
    } 	MPMediaInfoType;

typedef /* [public] */ 
enum MPDisplaySizeConstants
    {	mpDefaultSize	= 0,
	mpHalfSize	= ( mpDefaultSize + 1 ) ,
	mpDoubleSize	= ( mpHalfSize + 1 ) ,
	mpFullScreen	= ( mpDoubleSize + 1 ) ,
	mpFitToSize	= ( mpFullScreen + 1 ) ,
	mpOneSixteenthScreen	= ( mpFitToSize + 1 ) ,
	mpOneFourthScreen	= ( mpOneSixteenthScreen + 1 ) ,
	mpOneHalfScreen	= ( mpOneFourthScreen + 1 ) 
    } 	MPDisplaySizeConstants;

typedef /* [public] */ 
enum MPReadyStateConstants
    {	mpReadyStateUninitialized	= 0,
	mpReadyStateLoading	= ( mpReadyStateUninitialized + 1 ) ,
	mpReadyStateInteractive	= 3,
	mpReadyStateComplete	= ( mpReadyStateInteractive + 1 ) 
    } 	MPReadyStateConstants;

typedef /* [public] */ 
enum MPShowDialogConstants
    {	mpShowDialogHelp	= 0,
	mpShowDialogStatistics	= ( mpShowDialogHelp + 1 ) ,
	mpShowDialogOptions	= ( mpShowDialogStatistics + 1 ) ,
	mpShowDialogContextMenu	= ( mpShowDialogOptions + 1 ) 
    } 	MPShowDialogConstants;

/* [hidden] */ 
enum PlayerCompatibilityType
    {	cmDefault	= 0,
	cmActiveMovie	= ( cmDefault + 1 ) ,
	cmNetShow	= ( cmActiveMovie + 1 ) 
    } ;


extern RPC_IF_HANDLE __MIDL_itf_dxmplay_0000_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_dxmplay_0000_0000_v0_0_s_ifspec;

#ifndef __IDSPlayerEx_INTERFACE_DEFINED__
#define __IDSPlayerEx_INTERFACE_DEFINED__

/* interface IDSPlayerEx */
/* [unique][uuid][object] */ 


EXTERN_C const IID IID_IDSPlayerEx;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("920F0DE0-91C5-11d1-828F-00C04FC99D4C")
    IDSPlayerEx : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE PutStatusTextEx( 
            /* [in] */ BSTR wszText,
            /* [in] */ BOOL fOverwrite,
            /* [out][in] */ DWORD *pdwCookie) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE FlushStatusBar( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE NavigateBrowserToURL( 
            /* [in] */ BSTR bstrURL,
            /* [in] */ BSTR bstrFrame) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE RestartPlaylist( 
            /* [in] */ BOOL fPlay) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE PutPreviewModeHelper( 
            VARIANT_BOOL PreviewMode,
            BOOL fPlayWhenFinished) = 0;
        
        virtual /* [propput] */ HRESULT STDMETHODCALLTYPE put_UserInteraction( 
            /* [in] */ VARIANT_BOOL bVal) = 0;
        
        virtual /* [propget] */ HRESULT STDMETHODCALLTYPE get_CurrentPositionInternal( 
            /* [retval][out] */ double *pCurrentPosition) = 0;
        
        virtual /* [propput] */ HRESULT STDMETHODCALLTYPE put_CompatibilityMode( 
            /* [in] */ long lCompatibilityMode) = 0;
        
        virtual /* [propget] */ HRESULT STDMETHODCALLTYPE get_CompatibilityMode( 
            /* [retval][out] */ long *plCompatibilityMode) = 0;
        
        virtual /* [propget] */ HRESULT STDMETHODCALLTYPE get_DurationInternal( 
            /* [retval][out] */ double *pDuration) = 0;
        
        virtual /* [propget] */ HRESULT STDMETHODCALLTYPE get_SegmentSeek( 
            /* [retval][out] */ long *plSegmentSeek) = 0;
        
        virtual /* [propget] */ HRESULT STDMETHODCALLTYPE get_CanSkip( 
            /* [retval][out] */ VARIANT_BOOL *pbCanSkip) = 0;
        
        virtual /* [propget] */ HRESULT STDMETHODCALLTYPE get_BrowserIsIE4( 
            /* [retval][out] */ VARIANT_BOOL *pbCanSkip) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GoFullScreen( 
            BOOL bFullScreen) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDSPlayerExVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDSPlayerEx * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDSPlayerEx * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDSPlayerEx * This);
        
        HRESULT ( STDMETHODCALLTYPE *PutStatusTextEx )( 
            IDSPlayerEx * This,
            /* [in] */ BSTR wszText,
            /* [in] */ BOOL fOverwrite,
            /* [out][in] */ DWORD *pdwCookie);
        
        HRESULT ( STDMETHODCALLTYPE *FlushStatusBar )( 
            IDSPlayerEx * This);
        
        HRESULT ( STDMETHODCALLTYPE *NavigateBrowserToURL )( 
            IDSPlayerEx * This,
            /* [in] */ BSTR bstrURL,
            /* [in] */ BSTR bstrFrame);
        
        HRESULT ( STDMETHODCALLTYPE *RestartPlaylist )( 
            IDSPlayerEx * This,
            /* [in] */ BOOL fPlay);
        
        HRESULT ( STDMETHODCALLTYPE *PutPreviewModeHelper )( 
            IDSPlayerEx * This,
            VARIANT_BOOL PreviewMode,
            BOOL fPlayWhenFinished);
        
        /* [propput] */ HRESULT ( STDMETHODCALLTYPE *put_UserInteraction )( 
            IDSPlayerEx * This,
            /* [in] */ VARIANT_BOOL bVal);
        
        /* [propget] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentPositionInternal )( 
            IDSPlayerEx * This,
            /* [retval][out] */ double *pCurrentPosition);
        
        /* [propput] */ HRESULT ( STDMETHODCALLTYPE *put_CompatibilityMode )( 
            IDSPlayerEx * This,
            /* [in] */ long lCompatibilityMode);
        
        /* [propget] */ HRESULT ( STDMETHODCALLTYPE *get_CompatibilityMode )( 
            IDSPlayerEx * This,
            /* [retval][out] */ long *plCompatibilityMode);
        
        /* [propget] */ HRESULT ( STDMETHODCALLTYPE *get_DurationInternal )( 
            IDSPlayerEx * This,
            /* [retval][out] */ double *pDuration);
        
        /* [propget] */ HRESULT ( STDMETHODCALLTYPE *get_SegmentSeek )( 
            IDSPlayerEx * This,
            /* [retval][out] */ long *plSegmentSeek);
        
        /* [propget] */ HRESULT ( STDMETHODCALLTYPE *get_CanSkip )( 
            IDSPlayerEx * This,
            /* [retval][out] */ VARIANT_BOOL *pbCanSkip);
        
        /* [propget] */ HRESULT ( STDMETHODCALLTYPE *get_BrowserIsIE4 )( 
            IDSPlayerEx * This,
            /* [retval][out] */ VARIANT_BOOL *pbCanSkip);
        
        HRESULT ( STDMETHODCALLTYPE *GoFullScreen )( 
            IDSPlayerEx * This,
            BOOL bFullScreen);
        
        END_INTERFACE
    } IDSPlayerExVtbl;

    interface IDSPlayerEx
    {
        CONST_VTBL struct IDSPlayerExVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDSPlayerEx_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IDSPlayerEx_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IDSPlayerEx_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IDSPlayerEx_PutStatusTextEx(This,wszText,fOverwrite,pdwCookie)	\
    ( (This)->lpVtbl -> PutStatusTextEx(This,wszText,fOverwrite,pdwCookie) ) 

#define IDSPlayerEx_FlushStatusBar(This)	\
    ( (This)->lpVtbl -> FlushStatusBar(This) ) 

#define IDSPlayerEx_NavigateBrowserToURL(This,bstrURL,bstrFrame)	\
    ( (This)->lpVtbl -> NavigateBrowserToURL(This,bstrURL,bstrFrame) ) 

#define IDSPlayerEx_RestartPlaylist(This,fPlay)	\
    ( (This)->lpVtbl -> RestartPlaylist(This,fPlay) ) 

#define IDSPlayerEx_PutPreviewModeHelper(This,PreviewMode,fPlayWhenFinished)	\
    ( (This)->lpVtbl -> PutPreviewModeHelper(This,PreviewMode,fPlayWhenFinished) ) 

#define IDSPlayerEx_put_UserInteraction(This,bVal)	\
    ( (This)->lpVtbl -> put_UserInteraction(This,bVal) ) 

#define IDSPlayerEx_get_CurrentPositionInternal(This,pCurrentPosition)	\
    ( (This)->lpVtbl -> get_CurrentPositionInternal(This,pCurrentPosition) ) 

#define IDSPlayerEx_put_CompatibilityMode(This,lCompatibilityMode)	\
    ( (This)->lpVtbl -> put_CompatibilityMode(This,lCompatibilityMode) ) 

#define IDSPlayerEx_get_CompatibilityMode(This,plCompatibilityMode)	\
    ( (This)->lpVtbl -> get_CompatibilityMode(This,plCompatibilityMode) ) 

#define IDSPlayerEx_get_DurationInternal(This,pDuration)	\
    ( (This)->lpVtbl -> get_DurationInternal(This,pDuration) ) 

#define IDSPlayerEx_get_SegmentSeek(This,plSegmentSeek)	\
    ( (This)->lpVtbl -> get_SegmentSeek(This,plSegmentSeek) ) 

#define IDSPlayerEx_get_CanSkip(This,pbCanSkip)	\
    ( (This)->lpVtbl -> get_CanSkip(This,pbCanSkip) ) 

#define IDSPlayerEx_get_BrowserIsIE4(This,pbCanSkip)	\
    ( (This)->lpVtbl -> get_BrowserIsIE4(This,pbCanSkip) ) 

#define IDSPlayerEx_GoFullScreen(This,bFullScreen)	\
    ( (This)->lpVtbl -> GoFullScreen(This,bFullScreen) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IDSPlayerEx_INTERFACE_DEFINED__ */


#ifndef __IMediaPlayer_INTERFACE_DEFINED__
#define __IMediaPlayer_INTERFACE_DEFINED__

/* interface IMediaPlayer */
/* [unique][dual][uuid][object] */ 


EXTERN_C const IID IID_IMediaPlayer;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("22D6F311-B0F6-11D0-94AB-0080C74C7E95")
    IMediaPlayer : public IDispatch
    {
    public:
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CurrentPosition( 
            /* [retval][out] */ double *pCurrentPosition) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_CurrentPosition( 
            /* [in] */ double CurrentPosition) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Duration( 
            /* [retval][out] */ double *pDuration) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ImageSourceWidth( 
            /* [retval][out] */ long *pWidth) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ImageSourceHeight( 
            /* [retval][out] */ long *pHeight) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_MarkerCount( 
            /* [retval][out] */ long *pMarkerCount) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CanScan( 
            /* [retval][out] */ VARIANT_BOOL *pCanScan) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CanSeek( 
            /* [retval][out] */ VARIANT_BOOL *pCanSeek) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CanSeekToMarkers( 
            /* [retval][out] */ VARIANT_BOOL *pCanSeekToMarkers) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CurrentMarker( 
            /* [retval][out] */ long *pCurrentMarker) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_CurrentMarker( 
            /* [in] */ long CurrentMarker) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_FileName( 
            /* [retval][out] */ BSTR *pbstrFileName) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_FileName( 
            /* [in] */ BSTR bstrFileName) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SourceLink( 
            /* [retval][out] */ BSTR *pbstrSourceLink) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CreationDate( 
            /* [retval][out] */ DATE *pCreationDate) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ErrorCorrection( 
            /* [retval][out] */ BSTR *pbstrErrorCorrection) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Bandwidth( 
            /* [retval][out] */ long *pBandwidth) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SourceProtocol( 
            /* [retval][out] */ long *pSourceProtocol) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ReceivedPackets( 
            /* [retval][out] */ long *pReceivedPackets) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_RecoveredPackets( 
            /* [retval][out] */ long *pRecoveredPackets) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_LostPackets( 
            /* [retval][out] */ long *pLostPackets) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ReceptionQuality( 
            /* [retval][out] */ long *pReceptionQuality) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_BufferingCount( 
            /* [retval][out] */ long *pBufferingCount) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_IsBroadcast( 
            /* [retval][out] */ VARIANT_BOOL *pIsBroadcast) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_BufferingProgress( 
            /* [retval][out] */ long *pBufferingProgress) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ChannelName( 
            /* [retval][out] */ BSTR *pbstrChannelName) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ChannelDescription( 
            /* [retval][out] */ BSTR *pbstrChannelDescription) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ChannelURL( 
            /* [retval][out] */ BSTR *pbstrChannelURL) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ContactAddress( 
            /* [retval][out] */ BSTR *pbstrContactAddress) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ContactPhone( 
            /* [retval][out] */ BSTR *pbstrContactPhone) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ContactEmail( 
            /* [retval][out] */ BSTR *pbstrContactEmail) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_BufferingTime( 
            /* [retval][out] */ double *pBufferingTime) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_BufferingTime( 
            /* [in] */ double BufferingTime) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_AutoStart( 
            /* [retval][out] */ VARIANT_BOOL *pAutoStart) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_AutoStart( 
            /* [in] */ VARIANT_BOOL AutoStart) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_AutoRewind( 
            /* [retval][out] */ VARIANT_BOOL *pAutoRewind) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_AutoRewind( 
            /* [in] */ VARIANT_BOOL AutoRewind) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Rate( 
            /* [retval][out] */ double *pRate) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_Rate( 
            /* [in] */ double Rate) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SendKeyboardEvents( 
            /* [retval][out] */ VARIANT_BOOL *pSendKeyboardEvents) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_SendKeyboardEvents( 
            /* [in] */ VARIANT_BOOL SendKeyboardEvents) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SendMouseClickEvents( 
            /* [retval][out] */ VARIANT_BOOL *pSendMouseClickEvents) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_SendMouseClickEvents( 
            /* [in] */ VARIANT_BOOL SendMouseClickEvents) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SendMouseMoveEvents( 
            /* [retval][out] */ VARIANT_BOOL *pSendMouseMoveEvents) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_SendMouseMoveEvents( 
            /* [in] */ VARIANT_BOOL SendMouseMoveEvents) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_PlayCount( 
            /* [retval][out] */ long *pPlayCount) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_PlayCount( 
            /* [in] */ long PlayCount) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ClickToPlay( 
            /* [retval][out] */ VARIANT_BOOL *pClickToPlay) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_ClickToPlay( 
            /* [in] */ VARIANT_BOOL ClickToPlay) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_AllowScan( 
            /* [retval][out] */ VARIANT_BOOL *pAllowScan) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_AllowScan( 
            /* [in] */ VARIANT_BOOL AllowScan) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_EnableContextMenu( 
            /* [retval][out] */ VARIANT_BOOL *pEnableContextMenu) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_EnableContextMenu( 
            /* [in] */ VARIANT_BOOL EnableContextMenu) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CursorType( 
            /* [retval][out] */ long *pCursorType) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_CursorType( 
            /* [in] */ long CursorType) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CodecCount( 
            /* [retval][out] */ long *pCodecCount) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_AllowChangeDisplaySize( 
            /* [retval][out] */ VARIANT_BOOL *pAllowChangeDisplaySize) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_AllowChangeDisplaySize( 
            /* [in] */ VARIANT_BOOL AllowChangeDisplaySize) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_IsDurationValid( 
            /* [retval][out] */ VARIANT_BOOL *pIsDurationValid) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_OpenState( 
            /* [retval][out] */ long *pOpenState) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SendOpenStateChangeEvents( 
            /* [retval][out] */ VARIANT_BOOL *pSendOpenStateChangeEvents) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_SendOpenStateChangeEvents( 
            /* [in] */ VARIANT_BOOL SendOpenStateChangeEvents) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SendWarningEvents( 
            /* [retval][out] */ VARIANT_BOOL *pSendWarningEvents) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_SendWarningEvents( 
            /* [in] */ VARIANT_BOOL SendWarningEvents) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SendErrorEvents( 
            /* [retval][out] */ VARIANT_BOOL *pSendErrorEvents) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_SendErrorEvents( 
            /* [in] */ VARIANT_BOOL SendErrorEvents) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_PlayState( 
            /* [retval][out] */ MPPlayStateConstants *pPlayState) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SendPlayStateChangeEvents( 
            /* [retval][out] */ VARIANT_BOOL *pSendPlayStateChangeEvents) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_SendPlayStateChangeEvents( 
            /* [in] */ VARIANT_BOOL SendPlayStateChangeEvents) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_DisplaySize( 
            /* [retval][out] */ MPDisplaySizeConstants *pDisplaySize) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_DisplaySize( 
            /* [in] */ MPDisplaySizeConstants DisplaySize) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_InvokeURLs( 
            /* [retval][out] */ VARIANT_BOOL *pInvokeURLs) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_InvokeURLs( 
            /* [in] */ VARIANT_BOOL InvokeURLs) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_BaseURL( 
            /* [retval][out] */ BSTR *pbstrBaseURL) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_BaseURL( 
            /* [in] */ BSTR bstrBaseURL) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_DefaultFrame( 
            /* [retval][out] */ BSTR *pbstrDefaultFrame) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_DefaultFrame( 
            /* [in] */ BSTR bstrDefaultFrame) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_HasError( 
            /* [retval][out] */ VARIANT_BOOL *pHasError) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ErrorDescription( 
            /* [retval][out] */ BSTR *pbstrErrorDescription) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ErrorCode( 
            /* [retval][out] */ long *pErrorCode) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_AnimationAtStart( 
            /* [retval][out] */ VARIANT_BOOL *pAnimationAtStart) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_AnimationAtStart( 
            /* [in] */ VARIANT_BOOL AnimationAtStart) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_TransparentAtStart( 
            /* [retval][out] */ VARIANT_BOOL *pTransparentAtStart) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_TransparentAtStart( 
            /* [in] */ VARIANT_BOOL TransparentAtStart) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Volume( 
            /* [retval][out] */ long *pVolume) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_Volume( 
            /* [in] */ long Volume) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Balance( 
            /* [retval][out] */ long *pBalance) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_Balance( 
            /* [in] */ long Balance) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ReadyState( 
            /* [retval][out] */ MPReadyStateConstants *pValue) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SelectionStart( 
            /* [retval][out] */ double *pValue) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_SelectionStart( 
            /* [in] */ double Value) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SelectionEnd( 
            /* [retval][out] */ double *pValue) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_SelectionEnd( 
            /* [in] */ double Value) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ShowDisplay( 
            /* [retval][out] */ VARIANT_BOOL *Show) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_ShowDisplay( 
            /* [in] */ VARIANT_BOOL Show) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ShowControls( 
            /* [retval][out] */ VARIANT_BOOL *Show) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_ShowControls( 
            /* [in] */ VARIANT_BOOL Show) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ShowPositionControls( 
            /* [retval][out] */ VARIANT_BOOL *Show) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_ShowPositionControls( 
            /* [in] */ VARIANT_BOOL Show) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ShowTracker( 
            /* [retval][out] */ VARIANT_BOOL *Show) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_ShowTracker( 
            /* [in] */ VARIANT_BOOL Show) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_EnablePositionControls( 
            /* [retval][out] */ VARIANT_BOOL *Enable) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_EnablePositionControls( 
            /* [in] */ VARIANT_BOOL Enable) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_EnableTracker( 
            /* [retval][out] */ VARIANT_BOOL *Enable) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_EnableTracker( 
            /* [in] */ VARIANT_BOOL Enable) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Enabled( 
            /* [retval][out] */ VARIANT_BOOL *pEnabled) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_Enabled( 
            /* [in] */ VARIANT_BOOL Enabled) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_DisplayForeColor( 
            /* [retval][out] */ VB_OLE_COLOR *ForeColor) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_DisplayForeColor( 
            /* [in] */ VB_OLE_COLOR ForeColor) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_DisplayBackColor( 
            /* [retval][out] */ VB_OLE_COLOR *BackColor) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_DisplayBackColor( 
            /* [in] */ VB_OLE_COLOR BackColor) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_DisplayMode( 
            /* [retval][out] */ MPDisplayModeConstants *pValue) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_DisplayMode( 
            /* [in] */ MPDisplayModeConstants Value) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_VideoBorder3D( 
            /* [retval][out] */ VARIANT_BOOL *pVideoBorderWidth) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_VideoBorder3D( 
            /* [in] */ VARIANT_BOOL VideoBorderWidth) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_VideoBorderWidth( 
            /* [retval][out] */ long *pVideoBorderWidth) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_VideoBorderWidth( 
            /* [in] */ long VideoBorderWidth) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_VideoBorderColor( 
            /* [retval][out] */ VB_OLE_COLOR *pVideoBorderWidth) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_VideoBorderColor( 
            /* [in] */ VB_OLE_COLOR VideoBorderColor) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ShowGotoBar( 
            /* [retval][out] */ VARIANT_BOOL *pbool) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_ShowGotoBar( 
            /* [in] */ VARIANT_BOOL vbool) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ShowStatusBar( 
            /* [retval][out] */ VARIANT_BOOL *pbool) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_ShowStatusBar( 
            /* [in] */ VARIANT_BOOL vbool) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ShowCaptioning( 
            /* [retval][out] */ VARIANT_BOOL *pbool) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_ShowCaptioning( 
            /* [in] */ VARIANT_BOOL pbool) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ShowAudioControls( 
            /* [retval][out] */ VARIANT_BOOL *pbool) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_ShowAudioControls( 
            /* [in] */ VARIANT_BOOL bBool) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CaptioningID( 
            /* [retval][out] */ BSTR *pstrText) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_CaptioningID( 
            /* [in] */ BSTR strText) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Mute( 
            /* [retval][out] */ VARIANT_BOOL *vbool) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_Mute( 
            /* [in] */ VARIANT_BOOL vbool) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CanPreview( 
            /* [retval][out] */ VARIANT_BOOL *pCanPreview) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_PreviewMode( 
            /* [retval][out] */ VARIANT_BOOL *pPreviewMode) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_PreviewMode( 
            /* [in] */ VARIANT_BOOL PreviewMode) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_HasMultipleItems( 
            /* [retval][out] */ VARIANT_BOOL *pHasMuliItems) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Language( 
            /* [retval][out] */ long *pLanguage) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_Language( 
            /* [in] */ long Language) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_AudioStream( 
            /* [retval][out] */ long *pStream) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_AudioStream( 
            /* [in] */ long Stream) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SAMIStyle( 
            /* [retval][out] */ BSTR *pbstrStyle) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_SAMIStyle( 
            /* [in] */ BSTR bstrStyle) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SAMILang( 
            /* [retval][out] */ BSTR *pbstrLang) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_SAMILang( 
            /* [in] */ BSTR bstrLang) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SAMIFileName( 
            /* [retval][out] */ BSTR *pbstrFileName) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_SAMIFileName( 
            /* [in] */ BSTR bstrFileName) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_StreamCount( 
            /* [retval][out] */ long *pStreamCount) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ClientId( 
            /* [retval][out] */ BSTR *pbstrClientId) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ConnectionSpeed( 
            /* [retval][out] */ long *plConnectionSpeed) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_AutoSize( 
            /* [retval][out] */ VARIANT_BOOL *pbool) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_AutoSize( 
            /* [in] */ VARIANT_BOOL vbool) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_EnableFullScreenControls( 
            /* [retval][out] */ VARIANT_BOOL *pbVal) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_EnableFullScreenControls( 
            /* [in] */ VARIANT_BOOL bVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_ActiveMovie( 
            /* [retval][out] */ IDispatch **ppdispatch) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_NSPlay( 
            /* [retval][out] */ IDispatch **ppdispatch) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_WindowlessVideo( 
            /* [retval][out] */ VARIANT_BOOL *pbool) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_WindowlessVideo( 
            /* [in] */ VARIANT_BOOL vbool) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Play( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Stop( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Pause( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetMarkerTime( 
            /* [in] */ long MarkerNum,
            /* [retval][out] */ double *pMarkerTime) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetMarkerName( 
            /* [in] */ long MarkerNum,
            /* [retval][out] */ BSTR *pbstrMarkerName) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE AboutBox( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetCodecInstalled( 
            /* [in] */ long CodecNum,
            /* [retval][out] */ VARIANT_BOOL *pCodecInstalled) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetCodecDescription( 
            /* [in] */ long CodecNum,
            /* [retval][out] */ BSTR *pbstrCodecDescription) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetCodecURL( 
            /* [in] */ long CodecNum,
            /* [retval][out] */ BSTR *pbstrCodecURL) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetMoreInfoURL( 
            /* [in] */ MPMoreInfoType MoreInfoType,
            /* [retval][out] */ BSTR *pbstrMoreInfoURL) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetMediaInfoString( 
            /* [in] */ MPMediaInfoType MediaInfoType,
            /* [retval][out] */ BSTR *pbstrMediaInfo) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Cancel( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Open( 
            /* [in] */ BSTR bstrFileName) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE IsSoundCardEnabled( 
            /* [retval][out] */ VARIANT_BOOL *pbSoundCard) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Next( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Previous( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE StreamSelect( 
            /* [in] */ long StreamNum) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE FastForward( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE FastReverse( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetStreamName( 
            /* [in] */ long StreamNum,
            /* [retval][out] */ BSTR *pbstrStreamName) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetStreamGroup( 
            /* [in] */ long StreamNum,
            /* [retval][out] */ long *pStreamGroup) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetStreamSelected( 
            /* [in] */ long StreamNum,
            /* [retval][out] */ VARIANT_BOOL *pStreamSelected) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMediaPlayerVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMediaPlayer * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMediaPlayer * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMediaPlayer * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IMediaPlayer * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IMediaPlayer * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IMediaPlayer * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IMediaPlayer * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentPosition )( 
            IMediaPlayer * This,
            /* [retval][out] */ double *pCurrentPosition);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_CurrentPosition )( 
            IMediaPlayer * This,
            /* [in] */ double CurrentPosition);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Duration )( 
            IMediaPlayer * This,
            /* [retval][out] */ double *pDuration);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ImageSourceWidth )( 
            IMediaPlayer * This,
            /* [retval][out] */ long *pWidth);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ImageSourceHeight )( 
            IMediaPlayer * This,
            /* [retval][out] */ long *pHeight);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_MarkerCount )( 
            IMediaPlayer * This,
            /* [retval][out] */ long *pMarkerCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CanScan )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pCanScan);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CanSeek )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pCanSeek);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CanSeekToMarkers )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pCanSeekToMarkers);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentMarker )( 
            IMediaPlayer * This,
            /* [retval][out] */ long *pCurrentMarker);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_CurrentMarker )( 
            IMediaPlayer * This,
            /* [in] */ long CurrentMarker);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_FileName )( 
            IMediaPlayer * This,
            /* [retval][out] */ BSTR *pbstrFileName);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_FileName )( 
            IMediaPlayer * This,
            /* [in] */ BSTR bstrFileName);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SourceLink )( 
            IMediaPlayer * This,
            /* [retval][out] */ BSTR *pbstrSourceLink);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CreationDate )( 
            IMediaPlayer * This,
            /* [retval][out] */ DATE *pCreationDate);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ErrorCorrection )( 
            IMediaPlayer * This,
            /* [retval][out] */ BSTR *pbstrErrorCorrection);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Bandwidth )( 
            IMediaPlayer * This,
            /* [retval][out] */ long *pBandwidth);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SourceProtocol )( 
            IMediaPlayer * This,
            /* [retval][out] */ long *pSourceProtocol);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ReceivedPackets )( 
            IMediaPlayer * This,
            /* [retval][out] */ long *pReceivedPackets);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_RecoveredPackets )( 
            IMediaPlayer * This,
            /* [retval][out] */ long *pRecoveredPackets);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LostPackets )( 
            IMediaPlayer * This,
            /* [retval][out] */ long *pLostPackets);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ReceptionQuality )( 
            IMediaPlayer * This,
            /* [retval][out] */ long *pReceptionQuality);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_BufferingCount )( 
            IMediaPlayer * This,
            /* [retval][out] */ long *pBufferingCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_IsBroadcast )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pIsBroadcast);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_BufferingProgress )( 
            IMediaPlayer * This,
            /* [retval][out] */ long *pBufferingProgress);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ChannelName )( 
            IMediaPlayer * This,
            /* [retval][out] */ BSTR *pbstrChannelName);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ChannelDescription )( 
            IMediaPlayer * This,
            /* [retval][out] */ BSTR *pbstrChannelDescription);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ChannelURL )( 
            IMediaPlayer * This,
            /* [retval][out] */ BSTR *pbstrChannelURL);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ContactAddress )( 
            IMediaPlayer * This,
            /* [retval][out] */ BSTR *pbstrContactAddress);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ContactPhone )( 
            IMediaPlayer * This,
            /* [retval][out] */ BSTR *pbstrContactPhone);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ContactEmail )( 
            IMediaPlayer * This,
            /* [retval][out] */ BSTR *pbstrContactEmail);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_BufferingTime )( 
            IMediaPlayer * This,
            /* [retval][out] */ double *pBufferingTime);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_BufferingTime )( 
            IMediaPlayer * This,
            /* [in] */ double BufferingTime);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AutoStart )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pAutoStart);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AutoStart )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL AutoStart);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AutoRewind )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pAutoRewind);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AutoRewind )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL AutoRewind);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Rate )( 
            IMediaPlayer * This,
            /* [retval][out] */ double *pRate);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Rate )( 
            IMediaPlayer * This,
            /* [in] */ double Rate);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendKeyboardEvents )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pSendKeyboardEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendKeyboardEvents )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL SendKeyboardEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendMouseClickEvents )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pSendMouseClickEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendMouseClickEvents )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL SendMouseClickEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendMouseMoveEvents )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pSendMouseMoveEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendMouseMoveEvents )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL SendMouseMoveEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlayCount )( 
            IMediaPlayer * This,
            /* [retval][out] */ long *pPlayCount);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_PlayCount )( 
            IMediaPlayer * This,
            /* [in] */ long PlayCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ClickToPlay )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pClickToPlay);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ClickToPlay )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL ClickToPlay);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AllowScan )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pAllowScan);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AllowScan )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL AllowScan);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableContextMenu )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pEnableContextMenu);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableContextMenu )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL EnableContextMenu);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CursorType )( 
            IMediaPlayer * This,
            /* [retval][out] */ long *pCursorType);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_CursorType )( 
            IMediaPlayer * This,
            /* [in] */ long CursorType);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CodecCount )( 
            IMediaPlayer * This,
            /* [retval][out] */ long *pCodecCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AllowChangeDisplaySize )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pAllowChangeDisplaySize);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AllowChangeDisplaySize )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL AllowChangeDisplaySize);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_IsDurationValid )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pIsDurationValid);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OpenState )( 
            IMediaPlayer * This,
            /* [retval][out] */ long *pOpenState);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendOpenStateChangeEvents )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pSendOpenStateChangeEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendOpenStateChangeEvents )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL SendOpenStateChangeEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendWarningEvents )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pSendWarningEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendWarningEvents )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL SendWarningEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendErrorEvents )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pSendErrorEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendErrorEvents )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL SendErrorEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlayState )( 
            IMediaPlayer * This,
            /* [retval][out] */ MPPlayStateConstants *pPlayState);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendPlayStateChangeEvents )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pSendPlayStateChangeEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendPlayStateChangeEvents )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL SendPlayStateChangeEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_DisplaySize )( 
            IMediaPlayer * This,
            /* [retval][out] */ MPDisplaySizeConstants *pDisplaySize);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_DisplaySize )( 
            IMediaPlayer * This,
            /* [in] */ MPDisplaySizeConstants DisplaySize);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_InvokeURLs )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pInvokeURLs);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_InvokeURLs )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL InvokeURLs);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_BaseURL )( 
            IMediaPlayer * This,
            /* [retval][out] */ BSTR *pbstrBaseURL);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_BaseURL )( 
            IMediaPlayer * This,
            /* [in] */ BSTR bstrBaseURL);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultFrame )( 
            IMediaPlayer * This,
            /* [retval][out] */ BSTR *pbstrDefaultFrame);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_DefaultFrame )( 
            IMediaPlayer * This,
            /* [in] */ BSTR bstrDefaultFrame);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HasError )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pHasError);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ErrorDescription )( 
            IMediaPlayer * This,
            /* [retval][out] */ BSTR *pbstrErrorDescription);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ErrorCode )( 
            IMediaPlayer * This,
            /* [retval][out] */ long *pErrorCode);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AnimationAtStart )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pAnimationAtStart);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AnimationAtStart )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL AnimationAtStart);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TransparentAtStart )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pTransparentAtStart);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_TransparentAtStart )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL TransparentAtStart);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Volume )( 
            IMediaPlayer * This,
            /* [retval][out] */ long *pVolume);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Volume )( 
            IMediaPlayer * This,
            /* [in] */ long Volume);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Balance )( 
            IMediaPlayer * This,
            /* [retval][out] */ long *pBalance);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Balance )( 
            IMediaPlayer * This,
            /* [in] */ long Balance);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ReadyState )( 
            IMediaPlayer * This,
            /* [retval][out] */ MPReadyStateConstants *pValue);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SelectionStart )( 
            IMediaPlayer * This,
            /* [retval][out] */ double *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SelectionStart )( 
            IMediaPlayer * This,
            /* [in] */ double Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SelectionEnd )( 
            IMediaPlayer * This,
            /* [retval][out] */ double *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SelectionEnd )( 
            IMediaPlayer * This,
            /* [in] */ double Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowDisplay )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowDisplay )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowControls )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowControls )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowPositionControls )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowPositionControls )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowTracker )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowTracker )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnablePositionControls )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *Enable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnablePositionControls )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL Enable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableTracker )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *Enable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableTracker )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL Enable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Enabled )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pEnabled);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Enabled )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL Enabled);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_DisplayForeColor )( 
            IMediaPlayer * This,
            /* [retval][out] */ VB_OLE_COLOR *ForeColor);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_DisplayForeColor )( 
            IMediaPlayer * This,
            /* [in] */ VB_OLE_COLOR ForeColor);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_DisplayBackColor )( 
            IMediaPlayer * This,
            /* [retval][out] */ VB_OLE_COLOR *BackColor);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_DisplayBackColor )( 
            IMediaPlayer * This,
            /* [in] */ VB_OLE_COLOR BackColor);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_DisplayMode )( 
            IMediaPlayer * This,
            /* [retval][out] */ MPDisplayModeConstants *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_DisplayMode )( 
            IMediaPlayer * This,
            /* [in] */ MPDisplayModeConstants Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_VideoBorder3D )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pVideoBorderWidth);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_VideoBorder3D )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL VideoBorderWidth);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_VideoBorderWidth )( 
            IMediaPlayer * This,
            /* [retval][out] */ long *pVideoBorderWidth);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_VideoBorderWidth )( 
            IMediaPlayer * This,
            /* [in] */ long VideoBorderWidth);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_VideoBorderColor )( 
            IMediaPlayer * This,
            /* [retval][out] */ VB_OLE_COLOR *pVideoBorderWidth);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_VideoBorderColor )( 
            IMediaPlayer * This,
            /* [in] */ VB_OLE_COLOR VideoBorderColor);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowGotoBar )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pbool);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowGotoBar )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL vbool);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowStatusBar )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pbool);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowStatusBar )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL vbool);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowCaptioning )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pbool);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowCaptioning )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL pbool);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowAudioControls )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pbool);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowAudioControls )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL bBool);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CaptioningID )( 
            IMediaPlayer * This,
            /* [retval][out] */ BSTR *pstrText);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_CaptioningID )( 
            IMediaPlayer * This,
            /* [in] */ BSTR strText);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Mute )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *vbool);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Mute )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL vbool);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CanPreview )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pCanPreview);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PreviewMode )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pPreviewMode);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_PreviewMode )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL PreviewMode);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HasMultipleItems )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pHasMuliItems);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Language )( 
            IMediaPlayer * This,
            /* [retval][out] */ long *pLanguage);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Language )( 
            IMediaPlayer * This,
            /* [in] */ long Language);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AudioStream )( 
            IMediaPlayer * This,
            /* [retval][out] */ long *pStream);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AudioStream )( 
            IMediaPlayer * This,
            /* [in] */ long Stream);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SAMIStyle )( 
            IMediaPlayer * This,
            /* [retval][out] */ BSTR *pbstrStyle);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SAMIStyle )( 
            IMediaPlayer * This,
            /* [in] */ BSTR bstrStyle);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SAMILang )( 
            IMediaPlayer * This,
            /* [retval][out] */ BSTR *pbstrLang);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SAMILang )( 
            IMediaPlayer * This,
            /* [in] */ BSTR bstrLang);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SAMIFileName )( 
            IMediaPlayer * This,
            /* [retval][out] */ BSTR *pbstrFileName);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SAMIFileName )( 
            IMediaPlayer * This,
            /* [in] */ BSTR bstrFileName);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_StreamCount )( 
            IMediaPlayer * This,
            /* [retval][out] */ long *pStreamCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ClientId )( 
            IMediaPlayer * This,
            /* [retval][out] */ BSTR *pbstrClientId);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ConnectionSpeed )( 
            IMediaPlayer * This,
            /* [retval][out] */ long *plConnectionSpeed);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AutoSize )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pbool);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AutoSize )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL vbool);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableFullScreenControls )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pbVal);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableFullScreenControls )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL bVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ActiveMovie )( 
            IMediaPlayer * This,
            /* [retval][out] */ IDispatch **ppdispatch);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NSPlay )( 
            IMediaPlayer * This,
            /* [retval][out] */ IDispatch **ppdispatch);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_WindowlessVideo )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pbool);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_WindowlessVideo )( 
            IMediaPlayer * This,
            /* [in] */ VARIANT_BOOL vbool);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Play )( 
            IMediaPlayer * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Stop )( 
            IMediaPlayer * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Pause )( 
            IMediaPlayer * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetMarkerTime )( 
            IMediaPlayer * This,
            /* [in] */ long MarkerNum,
            /* [retval][out] */ double *pMarkerTime);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetMarkerName )( 
            IMediaPlayer * This,
            /* [in] */ long MarkerNum,
            /* [retval][out] */ BSTR *pbstrMarkerName);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *AboutBox )( 
            IMediaPlayer * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetCodecInstalled )( 
            IMediaPlayer * This,
            /* [in] */ long CodecNum,
            /* [retval][out] */ VARIANT_BOOL *pCodecInstalled);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetCodecDescription )( 
            IMediaPlayer * This,
            /* [in] */ long CodecNum,
            /* [retval][out] */ BSTR *pbstrCodecDescription);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetCodecURL )( 
            IMediaPlayer * This,
            /* [in] */ long CodecNum,
            /* [retval][out] */ BSTR *pbstrCodecURL);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetMoreInfoURL )( 
            IMediaPlayer * This,
            /* [in] */ MPMoreInfoType MoreInfoType,
            /* [retval][out] */ BSTR *pbstrMoreInfoURL);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetMediaInfoString )( 
            IMediaPlayer * This,
            /* [in] */ MPMediaInfoType MediaInfoType,
            /* [retval][out] */ BSTR *pbstrMediaInfo);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Cancel )( 
            IMediaPlayer * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Open )( 
            IMediaPlayer * This,
            /* [in] */ BSTR bstrFileName);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *IsSoundCardEnabled )( 
            IMediaPlayer * This,
            /* [retval][out] */ VARIANT_BOOL *pbSoundCard);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Next )( 
            IMediaPlayer * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Previous )( 
            IMediaPlayer * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *StreamSelect )( 
            IMediaPlayer * This,
            /* [in] */ long StreamNum);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *FastForward )( 
            IMediaPlayer * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *FastReverse )( 
            IMediaPlayer * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetStreamName )( 
            IMediaPlayer * This,
            /* [in] */ long StreamNum,
            /* [retval][out] */ BSTR *pbstrStreamName);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetStreamGroup )( 
            IMediaPlayer * This,
            /* [in] */ long StreamNum,
            /* [retval][out] */ long *pStreamGroup);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetStreamSelected )( 
            IMediaPlayer * This,
            /* [in] */ long StreamNum,
            /* [retval][out] */ VARIANT_BOOL *pStreamSelected);
        
        END_INTERFACE
    } IMediaPlayerVtbl;

    interface IMediaPlayer
    {
        CONST_VTBL struct IMediaPlayerVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMediaPlayer_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMediaPlayer_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMediaPlayer_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IMediaPlayer_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IMediaPlayer_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IMediaPlayer_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IMediaPlayer_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IMediaPlayer_get_CurrentPosition(This,pCurrentPosition)	\
    ( (This)->lpVtbl -> get_CurrentPosition(This,pCurrentPosition) ) 

#define IMediaPlayer_put_CurrentPosition(This,CurrentPosition)	\
    ( (This)->lpVtbl -> put_CurrentPosition(This,CurrentPosition) ) 

#define IMediaPlayer_get_Duration(This,pDuration)	\
    ( (This)->lpVtbl -> get_Duration(This,pDuration) ) 

#define IMediaPlayer_get_ImageSourceWidth(This,pWidth)	\
    ( (This)->lpVtbl -> get_ImageSourceWidth(This,pWidth) ) 

#define IMediaPlayer_get_ImageSourceHeight(This,pHeight)	\
    ( (This)->lpVtbl -> get_ImageSourceHeight(This,pHeight) ) 

#define IMediaPlayer_get_MarkerCount(This,pMarkerCount)	\
    ( (This)->lpVtbl -> get_MarkerCount(This,pMarkerCount) ) 

#define IMediaPlayer_get_CanScan(This,pCanScan)	\
    ( (This)->lpVtbl -> get_CanScan(This,pCanScan) ) 

#define IMediaPlayer_get_CanSeek(This,pCanSeek)	\
    ( (This)->lpVtbl -> get_CanSeek(This,pCanSeek) ) 

#define IMediaPlayer_get_CanSeekToMarkers(This,pCanSeekToMarkers)	\
    ( (This)->lpVtbl -> get_CanSeekToMarkers(This,pCanSeekToMarkers) ) 

#define IMediaPlayer_get_CurrentMarker(This,pCurrentMarker)	\
    ( (This)->lpVtbl -> get_CurrentMarker(This,pCurrentMarker) ) 

#define IMediaPlayer_put_CurrentMarker(This,CurrentMarker)	\
    ( (This)->lpVtbl -> put_CurrentMarker(This,CurrentMarker) ) 

#define IMediaPlayer_get_FileName(This,pbstrFileName)	\
    ( (This)->lpVtbl -> get_FileName(This,pbstrFileName) ) 

#define IMediaPlayer_put_FileName(This,bstrFileName)	\
    ( (This)->lpVtbl -> put_FileName(This,bstrFileName) ) 

#define IMediaPlayer_get_SourceLink(This,pbstrSourceLink)	\
    ( (This)->lpVtbl -> get_SourceLink(This,pbstrSourceLink) ) 

#define IMediaPlayer_get_CreationDate(This,pCreationDate)	\
    ( (This)->lpVtbl -> get_CreationDate(This,pCreationDate) ) 

#define IMediaPlayer_get_ErrorCorrection(This,pbstrErrorCorrection)	\
    ( (This)->lpVtbl -> get_ErrorCorrection(This,pbstrErrorCorrection) ) 

#define IMediaPlayer_get_Bandwidth(This,pBandwidth)	\
    ( (This)->lpVtbl -> get_Bandwidth(This,pBandwidth) ) 

#define IMediaPlayer_get_SourceProtocol(This,pSourceProtocol)	\
    ( (This)->lpVtbl -> get_SourceProtocol(This,pSourceProtocol) ) 

#define IMediaPlayer_get_ReceivedPackets(This,pReceivedPackets)	\
    ( (This)->lpVtbl -> get_ReceivedPackets(This,pReceivedPackets) ) 

#define IMediaPlayer_get_RecoveredPackets(This,pRecoveredPackets)	\
    ( (This)->lpVtbl -> get_RecoveredPackets(This,pRecoveredPackets) ) 

#define IMediaPlayer_get_LostPackets(This,pLostPackets)	\
    ( (This)->lpVtbl -> get_LostPackets(This,pLostPackets) ) 

#define IMediaPlayer_get_ReceptionQuality(This,pReceptionQuality)	\
    ( (This)->lpVtbl -> get_ReceptionQuality(This,pReceptionQuality) ) 

#define IMediaPlayer_get_BufferingCount(This,pBufferingCount)	\
    ( (This)->lpVtbl -> get_BufferingCount(This,pBufferingCount) ) 

#define IMediaPlayer_get_IsBroadcast(This,pIsBroadcast)	\
    ( (This)->lpVtbl -> get_IsBroadcast(This,pIsBroadcast) ) 

#define IMediaPlayer_get_BufferingProgress(This,pBufferingProgress)	\
    ( (This)->lpVtbl -> get_BufferingProgress(This,pBufferingProgress) ) 

#define IMediaPlayer_get_ChannelName(This,pbstrChannelName)	\
    ( (This)->lpVtbl -> get_ChannelName(This,pbstrChannelName) ) 

#define IMediaPlayer_get_ChannelDescription(This,pbstrChannelDescription)	\
    ( (This)->lpVtbl -> get_ChannelDescription(This,pbstrChannelDescription) ) 

#define IMediaPlayer_get_ChannelURL(This,pbstrChannelURL)	\
    ( (This)->lpVtbl -> get_ChannelURL(This,pbstrChannelURL) ) 

#define IMediaPlayer_get_ContactAddress(This,pbstrContactAddress)	\
    ( (This)->lpVtbl -> get_ContactAddress(This,pbstrContactAddress) ) 

#define IMediaPlayer_get_ContactPhone(This,pbstrContactPhone)	\
    ( (This)->lpVtbl -> get_ContactPhone(This,pbstrContactPhone) ) 

#define IMediaPlayer_get_ContactEmail(This,pbstrContactEmail)	\
    ( (This)->lpVtbl -> get_ContactEmail(This,pbstrContactEmail) ) 

#define IMediaPlayer_get_BufferingTime(This,pBufferingTime)	\
    ( (This)->lpVtbl -> get_BufferingTime(This,pBufferingTime) ) 

#define IMediaPlayer_put_BufferingTime(This,BufferingTime)	\
    ( (This)->lpVtbl -> put_BufferingTime(This,BufferingTime) ) 

#define IMediaPlayer_get_AutoStart(This,pAutoStart)	\
    ( (This)->lpVtbl -> get_AutoStart(This,pAutoStart) ) 

#define IMediaPlayer_put_AutoStart(This,AutoStart)	\
    ( (This)->lpVtbl -> put_AutoStart(This,AutoStart) ) 

#define IMediaPlayer_get_AutoRewind(This,pAutoRewind)	\
    ( (This)->lpVtbl -> get_AutoRewind(This,pAutoRewind) ) 

#define IMediaPlayer_put_AutoRewind(This,AutoRewind)	\
    ( (This)->lpVtbl -> put_AutoRewind(This,AutoRewind) ) 

#define IMediaPlayer_get_Rate(This,pRate)	\
    ( (This)->lpVtbl -> get_Rate(This,pRate) ) 

#define IMediaPlayer_put_Rate(This,Rate)	\
    ( (This)->lpVtbl -> put_Rate(This,Rate) ) 

#define IMediaPlayer_get_SendKeyboardEvents(This,pSendKeyboardEvents)	\
    ( (This)->lpVtbl -> get_SendKeyboardEvents(This,pSendKeyboardEvents) ) 

#define IMediaPlayer_put_SendKeyboardEvents(This,SendKeyboardEvents)	\
    ( (This)->lpVtbl -> put_SendKeyboardEvents(This,SendKeyboardEvents) ) 

#define IMediaPlayer_get_SendMouseClickEvents(This,pSendMouseClickEvents)	\
    ( (This)->lpVtbl -> get_SendMouseClickEvents(This,pSendMouseClickEvents) ) 

#define IMediaPlayer_put_SendMouseClickEvents(This,SendMouseClickEvents)	\
    ( (This)->lpVtbl -> put_SendMouseClickEvents(This,SendMouseClickEvents) ) 

#define IMediaPlayer_get_SendMouseMoveEvents(This,pSendMouseMoveEvents)	\
    ( (This)->lpVtbl -> get_SendMouseMoveEvents(This,pSendMouseMoveEvents) ) 

#define IMediaPlayer_put_SendMouseMoveEvents(This,SendMouseMoveEvents)	\
    ( (This)->lpVtbl -> put_SendMouseMoveEvents(This,SendMouseMoveEvents) ) 

#define IMediaPlayer_get_PlayCount(This,pPlayCount)	\
    ( (This)->lpVtbl -> get_PlayCount(This,pPlayCount) ) 

#define IMediaPlayer_put_PlayCount(This,PlayCount)	\
    ( (This)->lpVtbl -> put_PlayCount(This,PlayCount) ) 

#define IMediaPlayer_get_ClickToPlay(This,pClickToPlay)	\
    ( (This)->lpVtbl -> get_ClickToPlay(This,pClickToPlay) ) 

#define IMediaPlayer_put_ClickToPlay(This,ClickToPlay)	\
    ( (This)->lpVtbl -> put_ClickToPlay(This,ClickToPlay) ) 

#define IMediaPlayer_get_AllowScan(This,pAllowScan)	\
    ( (This)->lpVtbl -> get_AllowScan(This,pAllowScan) ) 

#define IMediaPlayer_put_AllowScan(This,AllowScan)	\
    ( (This)->lpVtbl -> put_AllowScan(This,AllowScan) ) 

#define IMediaPlayer_get_EnableContextMenu(This,pEnableContextMenu)	\
    ( (This)->lpVtbl -> get_EnableContextMenu(This,pEnableContextMenu) ) 

#define IMediaPlayer_put_EnableContextMenu(This,EnableContextMenu)	\
    ( (This)->lpVtbl -> put_EnableContextMenu(This,EnableContextMenu) ) 

#define IMediaPlayer_get_CursorType(This,pCursorType)	\
    ( (This)->lpVtbl -> get_CursorType(This,pCursorType) ) 

#define IMediaPlayer_put_CursorType(This,CursorType)	\
    ( (This)->lpVtbl -> put_CursorType(This,CursorType) ) 

#define IMediaPlayer_get_CodecCount(This,pCodecCount)	\
    ( (This)->lpVtbl -> get_CodecCount(This,pCodecCount) ) 

#define IMediaPlayer_get_AllowChangeDisplaySize(This,pAllowChangeDisplaySize)	\
    ( (This)->lpVtbl -> get_AllowChangeDisplaySize(This,pAllowChangeDisplaySize) ) 

#define IMediaPlayer_put_AllowChangeDisplaySize(This,AllowChangeDisplaySize)	\
    ( (This)->lpVtbl -> put_AllowChangeDisplaySize(This,AllowChangeDisplaySize) ) 

#define IMediaPlayer_get_IsDurationValid(This,pIsDurationValid)	\
    ( (This)->lpVtbl -> get_IsDurationValid(This,pIsDurationValid) ) 

#define IMediaPlayer_get_OpenState(This,pOpenState)	\
    ( (This)->lpVtbl -> get_OpenState(This,pOpenState) ) 

#define IMediaPlayer_get_SendOpenStateChangeEvents(This,pSendOpenStateChangeEvents)	\
    ( (This)->lpVtbl -> get_SendOpenStateChangeEvents(This,pSendOpenStateChangeEvents) ) 

#define IMediaPlayer_put_SendOpenStateChangeEvents(This,SendOpenStateChangeEvents)	\
    ( (This)->lpVtbl -> put_SendOpenStateChangeEvents(This,SendOpenStateChangeEvents) ) 

#define IMediaPlayer_get_SendWarningEvents(This,pSendWarningEvents)	\
    ( (This)->lpVtbl -> get_SendWarningEvents(This,pSendWarningEvents) ) 

#define IMediaPlayer_put_SendWarningEvents(This,SendWarningEvents)	\
    ( (This)->lpVtbl -> put_SendWarningEvents(This,SendWarningEvents) ) 

#define IMediaPlayer_get_SendErrorEvents(This,pSendErrorEvents)	\
    ( (This)->lpVtbl -> get_SendErrorEvents(This,pSendErrorEvents) ) 

#define IMediaPlayer_put_SendErrorEvents(This,SendErrorEvents)	\
    ( (This)->lpVtbl -> put_SendErrorEvents(This,SendErrorEvents) ) 

#define IMediaPlayer_get_PlayState(This,pPlayState)	\
    ( (This)->lpVtbl -> get_PlayState(This,pPlayState) ) 

#define IMediaPlayer_get_SendPlayStateChangeEvents(This,pSendPlayStateChangeEvents)	\
    ( (This)->lpVtbl -> get_SendPlayStateChangeEvents(This,pSendPlayStateChangeEvents) ) 

#define IMediaPlayer_put_SendPlayStateChangeEvents(This,SendPlayStateChangeEvents)	\
    ( (This)->lpVtbl -> put_SendPlayStateChangeEvents(This,SendPlayStateChangeEvents) ) 

#define IMediaPlayer_get_DisplaySize(This,pDisplaySize)	\
    ( (This)->lpVtbl -> get_DisplaySize(This,pDisplaySize) ) 

#define IMediaPlayer_put_DisplaySize(This,DisplaySize)	\
    ( (This)->lpVtbl -> put_DisplaySize(This,DisplaySize) ) 

#define IMediaPlayer_get_InvokeURLs(This,pInvokeURLs)	\
    ( (This)->lpVtbl -> get_InvokeURLs(This,pInvokeURLs) ) 

#define IMediaPlayer_put_InvokeURLs(This,InvokeURLs)	\
    ( (This)->lpVtbl -> put_InvokeURLs(This,InvokeURLs) ) 

#define IMediaPlayer_get_BaseURL(This,pbstrBaseURL)	\
    ( (This)->lpVtbl -> get_BaseURL(This,pbstrBaseURL) ) 

#define IMediaPlayer_put_BaseURL(This,bstrBaseURL)	\
    ( (This)->lpVtbl -> put_BaseURL(This,bstrBaseURL) ) 

#define IMediaPlayer_get_DefaultFrame(This,pbstrDefaultFrame)	\
    ( (This)->lpVtbl -> get_DefaultFrame(This,pbstrDefaultFrame) ) 

#define IMediaPlayer_put_DefaultFrame(This,bstrDefaultFrame)	\
    ( (This)->lpVtbl -> put_DefaultFrame(This,bstrDefaultFrame) ) 

#define IMediaPlayer_get_HasError(This,pHasError)	\
    ( (This)->lpVtbl -> get_HasError(This,pHasError) ) 

#define IMediaPlayer_get_ErrorDescription(This,pbstrErrorDescription)	\
    ( (This)->lpVtbl -> get_ErrorDescription(This,pbstrErrorDescription) ) 

#define IMediaPlayer_get_ErrorCode(This,pErrorCode)	\
    ( (This)->lpVtbl -> get_ErrorCode(This,pErrorCode) ) 

#define IMediaPlayer_get_AnimationAtStart(This,pAnimationAtStart)	\
    ( (This)->lpVtbl -> get_AnimationAtStart(This,pAnimationAtStart) ) 

#define IMediaPlayer_put_AnimationAtStart(This,AnimationAtStart)	\
    ( (This)->lpVtbl -> put_AnimationAtStart(This,AnimationAtStart) ) 

#define IMediaPlayer_get_TransparentAtStart(This,pTransparentAtStart)	\
    ( (This)->lpVtbl -> get_TransparentAtStart(This,pTransparentAtStart) ) 

#define IMediaPlayer_put_TransparentAtStart(This,TransparentAtStart)	\
    ( (This)->lpVtbl -> put_TransparentAtStart(This,TransparentAtStart) ) 

#define IMediaPlayer_get_Volume(This,pVolume)	\
    ( (This)->lpVtbl -> get_Volume(This,pVolume) ) 

#define IMediaPlayer_put_Volume(This,Volume)	\
    ( (This)->lpVtbl -> put_Volume(This,Volume) ) 

#define IMediaPlayer_get_Balance(This,pBalance)	\
    ( (This)->lpVtbl -> get_Balance(This,pBalance) ) 

#define IMediaPlayer_put_Balance(This,Balance)	\
    ( (This)->lpVtbl -> put_Balance(This,Balance) ) 

#define IMediaPlayer_get_ReadyState(This,pValue)	\
    ( (This)->lpVtbl -> get_ReadyState(This,pValue) ) 

#define IMediaPlayer_get_SelectionStart(This,pValue)	\
    ( (This)->lpVtbl -> get_SelectionStart(This,pValue) ) 

#define IMediaPlayer_put_SelectionStart(This,Value)	\
    ( (This)->lpVtbl -> put_SelectionStart(This,Value) ) 

#define IMediaPlayer_get_SelectionEnd(This,pValue)	\
    ( (This)->lpVtbl -> get_SelectionEnd(This,pValue) ) 

#define IMediaPlayer_put_SelectionEnd(This,Value)	\
    ( (This)->lpVtbl -> put_SelectionEnd(This,Value) ) 

#define IMediaPlayer_get_ShowDisplay(This,Show)	\
    ( (This)->lpVtbl -> get_ShowDisplay(This,Show) ) 

#define IMediaPlayer_put_ShowDisplay(This,Show)	\
    ( (This)->lpVtbl -> put_ShowDisplay(This,Show) ) 

#define IMediaPlayer_get_ShowControls(This,Show)	\
    ( (This)->lpVtbl -> get_ShowControls(This,Show) ) 

#define IMediaPlayer_put_ShowControls(This,Show)	\
    ( (This)->lpVtbl -> put_ShowControls(This,Show) ) 

#define IMediaPlayer_get_ShowPositionControls(This,Show)	\
    ( (This)->lpVtbl -> get_ShowPositionControls(This,Show) ) 

#define IMediaPlayer_put_ShowPositionControls(This,Show)	\
    ( (This)->lpVtbl -> put_ShowPositionControls(This,Show) ) 

#define IMediaPlayer_get_ShowTracker(This,Show)	\
    ( (This)->lpVtbl -> get_ShowTracker(This,Show) ) 

#define IMediaPlayer_put_ShowTracker(This,Show)	\
    ( (This)->lpVtbl -> put_ShowTracker(This,Show) ) 

#define IMediaPlayer_get_EnablePositionControls(This,Enable)	\
    ( (This)->lpVtbl -> get_EnablePositionControls(This,Enable) ) 

#define IMediaPlayer_put_EnablePositionControls(This,Enable)	\
    ( (This)->lpVtbl -> put_EnablePositionControls(This,Enable) ) 

#define IMediaPlayer_get_EnableTracker(This,Enable)	\
    ( (This)->lpVtbl -> get_EnableTracker(This,Enable) ) 

#define IMediaPlayer_put_EnableTracker(This,Enable)	\
    ( (This)->lpVtbl -> put_EnableTracker(This,Enable) ) 

#define IMediaPlayer_get_Enabled(This,pEnabled)	\
    ( (This)->lpVtbl -> get_Enabled(This,pEnabled) ) 

#define IMediaPlayer_put_Enabled(This,Enabled)	\
    ( (This)->lpVtbl -> put_Enabled(This,Enabled) ) 

#define IMediaPlayer_get_DisplayForeColor(This,ForeColor)	\
    ( (This)->lpVtbl -> get_DisplayForeColor(This,ForeColor) ) 

#define IMediaPlayer_put_DisplayForeColor(This,ForeColor)	\
    ( (This)->lpVtbl -> put_DisplayForeColor(This,ForeColor) ) 

#define IMediaPlayer_get_DisplayBackColor(This,BackColor)	\
    ( (This)->lpVtbl -> get_DisplayBackColor(This,BackColor) ) 

#define IMediaPlayer_put_DisplayBackColor(This,BackColor)	\
    ( (This)->lpVtbl -> put_DisplayBackColor(This,BackColor) ) 

#define IMediaPlayer_get_DisplayMode(This,pValue)	\
    ( (This)->lpVtbl -> get_DisplayMode(This,pValue) ) 

#define IMediaPlayer_put_DisplayMode(This,Value)	\
    ( (This)->lpVtbl -> put_DisplayMode(This,Value) ) 

#define IMediaPlayer_get_VideoBorder3D(This,pVideoBorderWidth)	\
    ( (This)->lpVtbl -> get_VideoBorder3D(This,pVideoBorderWidth) ) 

#define IMediaPlayer_put_VideoBorder3D(This,VideoBorderWidth)	\
    ( (This)->lpVtbl -> put_VideoBorder3D(This,VideoBorderWidth) ) 

#define IMediaPlayer_get_VideoBorderWidth(This,pVideoBorderWidth)	\
    ( (This)->lpVtbl -> get_VideoBorderWidth(This,pVideoBorderWidth) ) 

#define IMediaPlayer_put_VideoBorderWidth(This,VideoBorderWidth)	\
    ( (This)->lpVtbl -> put_VideoBorderWidth(This,VideoBorderWidth) ) 

#define IMediaPlayer_get_VideoBorderColor(This,pVideoBorderWidth)	\
    ( (This)->lpVtbl -> get_VideoBorderColor(This,pVideoBorderWidth) ) 

#define IMediaPlayer_put_VideoBorderColor(This,VideoBorderColor)	\
    ( (This)->lpVtbl -> put_VideoBorderColor(This,VideoBorderColor) ) 

#define IMediaPlayer_get_ShowGotoBar(This,pbool)	\
    ( (This)->lpVtbl -> get_ShowGotoBar(This,pbool) ) 

#define IMediaPlayer_put_ShowGotoBar(This,vbool)	\
    ( (This)->lpVtbl -> put_ShowGotoBar(This,vbool) ) 

#define IMediaPlayer_get_ShowStatusBar(This,pbool)	\
    ( (This)->lpVtbl -> get_ShowStatusBar(This,pbool) ) 

#define IMediaPlayer_put_ShowStatusBar(This,vbool)	\
    ( (This)->lpVtbl -> put_ShowStatusBar(This,vbool) ) 

#define IMediaPlayer_get_ShowCaptioning(This,pbool)	\
    ( (This)->lpVtbl -> get_ShowCaptioning(This,pbool) ) 

#define IMediaPlayer_put_ShowCaptioning(This,pbool)	\
    ( (This)->lpVtbl -> put_ShowCaptioning(This,pbool) ) 

#define IMediaPlayer_get_ShowAudioControls(This,pbool)	\
    ( (This)->lpVtbl -> get_ShowAudioControls(This,pbool) ) 

#define IMediaPlayer_put_ShowAudioControls(This,bBool)	\
    ( (This)->lpVtbl -> put_ShowAudioControls(This,bBool) ) 

#define IMediaPlayer_get_CaptioningID(This,pstrText)	\
    ( (This)->lpVtbl -> get_CaptioningID(This,pstrText) ) 

#define IMediaPlayer_put_CaptioningID(This,strText)	\
    ( (This)->lpVtbl -> put_CaptioningID(This,strText) ) 

#define IMediaPlayer_get_Mute(This,vbool)	\
    ( (This)->lpVtbl -> get_Mute(This,vbool) ) 

#define IMediaPlayer_put_Mute(This,vbool)	\
    ( (This)->lpVtbl -> put_Mute(This,vbool) ) 

#define IMediaPlayer_get_CanPreview(This,pCanPreview)	\
    ( (This)->lpVtbl -> get_CanPreview(This,pCanPreview) ) 

#define IMediaPlayer_get_PreviewMode(This,pPreviewMode)	\
    ( (This)->lpVtbl -> get_PreviewMode(This,pPreviewMode) ) 

#define IMediaPlayer_put_PreviewMode(This,PreviewMode)	\
    ( (This)->lpVtbl -> put_PreviewMode(This,PreviewMode) ) 

#define IMediaPlayer_get_HasMultipleItems(This,pHasMuliItems)	\
    ( (This)->lpVtbl -> get_HasMultipleItems(This,pHasMuliItems) ) 

#define IMediaPlayer_get_Language(This,pLanguage)	\
    ( (This)->lpVtbl -> get_Language(This,pLanguage) ) 

#define IMediaPlayer_put_Language(This,Language)	\
    ( (This)->lpVtbl -> put_Language(This,Language) ) 

#define IMediaPlayer_get_AudioStream(This,pStream)	\
    ( (This)->lpVtbl -> get_AudioStream(This,pStream) ) 

#define IMediaPlayer_put_AudioStream(This,Stream)	\
    ( (This)->lpVtbl -> put_AudioStream(This,Stream) ) 

#define IMediaPlayer_get_SAMIStyle(This,pbstrStyle)	\
    ( (This)->lpVtbl -> get_SAMIStyle(This,pbstrStyle) ) 

#define IMediaPlayer_put_SAMIStyle(This,bstrStyle)	\
    ( (This)->lpVtbl -> put_SAMIStyle(This,bstrStyle) ) 

#define IMediaPlayer_get_SAMILang(This,pbstrLang)	\
    ( (This)->lpVtbl -> get_SAMILang(This,pbstrLang) ) 

#define IMediaPlayer_put_SAMILang(This,bstrLang)	\
    ( (This)->lpVtbl -> put_SAMILang(This,bstrLang) ) 

#define IMediaPlayer_get_SAMIFileName(This,pbstrFileName)	\
    ( (This)->lpVtbl -> get_SAMIFileName(This,pbstrFileName) ) 

#define IMediaPlayer_put_SAMIFileName(This,bstrFileName)	\
    ( (This)->lpVtbl -> put_SAMIFileName(This,bstrFileName) ) 

#define IMediaPlayer_get_StreamCount(This,pStreamCount)	\
    ( (This)->lpVtbl -> get_StreamCount(This,pStreamCount) ) 

#define IMediaPlayer_get_ClientId(This,pbstrClientId)	\
    ( (This)->lpVtbl -> get_ClientId(This,pbstrClientId) ) 

#define IMediaPlayer_get_ConnectionSpeed(This,plConnectionSpeed)	\
    ( (This)->lpVtbl -> get_ConnectionSpeed(This,plConnectionSpeed) ) 

#define IMediaPlayer_get_AutoSize(This,pbool)	\
    ( (This)->lpVtbl -> get_AutoSize(This,pbool) ) 

#define IMediaPlayer_put_AutoSize(This,vbool)	\
    ( (This)->lpVtbl -> put_AutoSize(This,vbool) ) 

#define IMediaPlayer_get_EnableFullScreenControls(This,pbVal)	\
    ( (This)->lpVtbl -> get_EnableFullScreenControls(This,pbVal) ) 

#define IMediaPlayer_put_EnableFullScreenControls(This,bVal)	\
    ( (This)->lpVtbl -> put_EnableFullScreenControls(This,bVal) ) 

#define IMediaPlayer_get_ActiveMovie(This,ppdispatch)	\
    ( (This)->lpVtbl -> get_ActiveMovie(This,ppdispatch) ) 

#define IMediaPlayer_get_NSPlay(This,ppdispatch)	\
    ( (This)->lpVtbl -> get_NSPlay(This,ppdispatch) ) 

#define IMediaPlayer_get_WindowlessVideo(This,pbool)	\
    ( (This)->lpVtbl -> get_WindowlessVideo(This,pbool) ) 

#define IMediaPlayer_put_WindowlessVideo(This,vbool)	\
    ( (This)->lpVtbl -> put_WindowlessVideo(This,vbool) ) 

#define IMediaPlayer_Play(This)	\
    ( (This)->lpVtbl -> Play(This) ) 

#define IMediaPlayer_Stop(This)	\
    ( (This)->lpVtbl -> Stop(This) ) 

#define IMediaPlayer_Pause(This)	\
    ( (This)->lpVtbl -> Pause(This) ) 

#define IMediaPlayer_GetMarkerTime(This,MarkerNum,pMarkerTime)	\
    ( (This)->lpVtbl -> GetMarkerTime(This,MarkerNum,pMarkerTime) ) 

#define IMediaPlayer_GetMarkerName(This,MarkerNum,pbstrMarkerName)	\
    ( (This)->lpVtbl -> GetMarkerName(This,MarkerNum,pbstrMarkerName) ) 

#define IMediaPlayer_AboutBox(This)	\
    ( (This)->lpVtbl -> AboutBox(This) ) 

#define IMediaPlayer_GetCodecInstalled(This,CodecNum,pCodecInstalled)	\
    ( (This)->lpVtbl -> GetCodecInstalled(This,CodecNum,pCodecInstalled) ) 

#define IMediaPlayer_GetCodecDescription(This,CodecNum,pbstrCodecDescription)	\
    ( (This)->lpVtbl -> GetCodecDescription(This,CodecNum,pbstrCodecDescription) ) 

#define IMediaPlayer_GetCodecURL(This,CodecNum,pbstrCodecURL)	\
    ( (This)->lpVtbl -> GetCodecURL(This,CodecNum,pbstrCodecURL) ) 

#define IMediaPlayer_GetMoreInfoURL(This,MoreInfoType,pbstrMoreInfoURL)	\
    ( (This)->lpVtbl -> GetMoreInfoURL(This,MoreInfoType,pbstrMoreInfoURL) ) 

#define IMediaPlayer_GetMediaInfoString(This,MediaInfoType,pbstrMediaInfo)	\
    ( (This)->lpVtbl -> GetMediaInfoString(This,MediaInfoType,pbstrMediaInfo) ) 

#define IMediaPlayer_Cancel(This)	\
    ( (This)->lpVtbl -> Cancel(This) ) 

#define IMediaPlayer_Open(This,bstrFileName)	\
    ( (This)->lpVtbl -> Open(This,bstrFileName) ) 

#define IMediaPlayer_IsSoundCardEnabled(This,pbSoundCard)	\
    ( (This)->lpVtbl -> IsSoundCardEnabled(This,pbSoundCard) ) 

#define IMediaPlayer_Next(This)	\
    ( (This)->lpVtbl -> Next(This) ) 

#define IMediaPlayer_Previous(This)	\
    ( (This)->lpVtbl -> Previous(This) ) 

#define IMediaPlayer_StreamSelect(This,StreamNum)	\
    ( (This)->lpVtbl -> StreamSelect(This,StreamNum) ) 

#define IMediaPlayer_FastForward(This)	\
    ( (This)->lpVtbl -> FastForward(This) ) 

#define IMediaPlayer_FastReverse(This)	\
    ( (This)->lpVtbl -> FastReverse(This) ) 

#define IMediaPlayer_GetStreamName(This,StreamNum,pbstrStreamName)	\
    ( (This)->lpVtbl -> GetStreamName(This,StreamNum,pbstrStreamName) ) 

#define IMediaPlayer_GetStreamGroup(This,StreamNum,pStreamGroup)	\
    ( (This)->lpVtbl -> GetStreamGroup(This,StreamNum,pStreamGroup) ) 

#define IMediaPlayer_GetStreamSelected(This,StreamNum,pStreamSelected)	\
    ( (This)->lpVtbl -> GetStreamSelected(This,StreamNum,pStreamSelected) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [propget][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_get_ShowStatusBar_Proxy( 
    IMediaPlayer * This,
    /* [retval][out] */ VARIANT_BOOL *pbool);


void __RPC_STUB IMediaPlayer_get_ShowStatusBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propput][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_put_ShowStatusBar_Proxy( 
    IMediaPlayer * This,
    /* [in] */ VARIANT_BOOL vbool);


void __RPC_STUB IMediaPlayer_put_ShowStatusBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_get_ShowCaptioning_Proxy( 
    IMediaPlayer * This,
    /* [retval][out] */ VARIANT_BOOL *pbool);


void __RPC_STUB IMediaPlayer_get_ShowCaptioning_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propput][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_put_ShowCaptioning_Proxy( 
    IMediaPlayer * This,
    /* [in] */ VARIANT_BOOL pbool);


void __RPC_STUB IMediaPlayer_put_ShowCaptioning_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_get_ShowAudioControls_Proxy( 
    IMediaPlayer * This,
    /* [retval][out] */ VARIANT_BOOL *pbool);


void __RPC_STUB IMediaPlayer_get_ShowAudioControls_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propput][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_put_ShowAudioControls_Proxy( 
    IMediaPlayer * This,
    /* [in] */ VARIANT_BOOL bBool);


void __RPC_STUB IMediaPlayer_put_ShowAudioControls_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_get_CaptioningID_Proxy( 
    IMediaPlayer * This,
    /* [retval][out] */ BSTR *pstrText);


void __RPC_STUB IMediaPlayer_get_CaptioningID_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propput][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_put_CaptioningID_Proxy( 
    IMediaPlayer * This,
    /* [in] */ BSTR strText);


void __RPC_STUB IMediaPlayer_put_CaptioningID_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_get_Mute_Proxy( 
    IMediaPlayer * This,
    /* [retval][out] */ VARIANT_BOOL *vbool);


void __RPC_STUB IMediaPlayer_get_Mute_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propput][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_put_Mute_Proxy( 
    IMediaPlayer * This,
    /* [in] */ VARIANT_BOOL vbool);


void __RPC_STUB IMediaPlayer_put_Mute_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_get_CanPreview_Proxy( 
    IMediaPlayer * This,
    /* [retval][out] */ VARIANT_BOOL *pCanPreview);


void __RPC_STUB IMediaPlayer_get_CanPreview_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_get_PreviewMode_Proxy( 
    IMediaPlayer * This,
    /* [retval][out] */ VARIANT_BOOL *pPreviewMode);


void __RPC_STUB IMediaPlayer_get_PreviewMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propput][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_put_PreviewMode_Proxy( 
    IMediaPlayer * This,
    /* [in] */ VARIANT_BOOL PreviewMode);


void __RPC_STUB IMediaPlayer_put_PreviewMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_get_HasMultipleItems_Proxy( 
    IMediaPlayer * This,
    /* [retval][out] */ VARIANT_BOOL *pHasMuliItems);


void __RPC_STUB IMediaPlayer_get_HasMultipleItems_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_get_Language_Proxy( 
    IMediaPlayer * This,
    /* [retval][out] */ long *pLanguage);


void __RPC_STUB IMediaPlayer_get_Language_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propput][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_put_Language_Proxy( 
    IMediaPlayer * This,
    /* [in] */ long Language);


void __RPC_STUB IMediaPlayer_put_Language_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_get_AudioStream_Proxy( 
    IMediaPlayer * This,
    /* [retval][out] */ long *pStream);


void __RPC_STUB IMediaPlayer_get_AudioStream_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propput][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_put_AudioStream_Proxy( 
    IMediaPlayer * This,
    /* [in] */ long Stream);


void __RPC_STUB IMediaPlayer_put_AudioStream_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_get_SAMIStyle_Proxy( 
    IMediaPlayer * This,
    /* [retval][out] */ BSTR *pbstrStyle);


void __RPC_STUB IMediaPlayer_get_SAMIStyle_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propput][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_put_SAMIStyle_Proxy( 
    IMediaPlayer * This,
    /* [in] */ BSTR bstrStyle);


void __RPC_STUB IMediaPlayer_put_SAMIStyle_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_get_SAMILang_Proxy( 
    IMediaPlayer * This,
    /* [retval][out] */ BSTR *pbstrLang);


void __RPC_STUB IMediaPlayer_get_SAMILang_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propput][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_put_SAMILang_Proxy( 
    IMediaPlayer * This,
    /* [in] */ BSTR bstrLang);


void __RPC_STUB IMediaPlayer_put_SAMILang_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_get_SAMIFileName_Proxy( 
    IMediaPlayer * This,
    /* [retval][out] */ BSTR *pbstrFileName);


void __RPC_STUB IMediaPlayer_get_SAMIFileName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propput][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_put_SAMIFileName_Proxy( 
    IMediaPlayer * This,
    /* [in] */ BSTR bstrFileName);


void __RPC_STUB IMediaPlayer_put_SAMIFileName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_get_StreamCount_Proxy( 
    IMediaPlayer * This,
    /* [retval][out] */ long *pStreamCount);


void __RPC_STUB IMediaPlayer_get_StreamCount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_get_ClientId_Proxy( 
    IMediaPlayer * This,
    /* [retval][out] */ BSTR *pbstrClientId);


void __RPC_STUB IMediaPlayer_get_ClientId_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_get_ConnectionSpeed_Proxy( 
    IMediaPlayer * This,
    /* [retval][out] */ long *plConnectionSpeed);


void __RPC_STUB IMediaPlayer_get_ConnectionSpeed_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_get_AutoSize_Proxy( 
    IMediaPlayer * This,
    /* [retval][out] */ VARIANT_BOOL *pbool);


void __RPC_STUB IMediaPlayer_get_AutoSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propput][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_put_AutoSize_Proxy( 
    IMediaPlayer * This,
    /* [in] */ VARIANT_BOOL vbool);


void __RPC_STUB IMediaPlayer_put_AutoSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_get_EnableFullScreenControls_Proxy( 
    IMediaPlayer * This,
    /* [retval][out] */ VARIANT_BOOL *pbVal);


void __RPC_STUB IMediaPlayer_get_EnableFullScreenControls_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propput][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_put_EnableFullScreenControls_Proxy( 
    IMediaPlayer * This,
    /* [in] */ VARIANT_BOOL bVal);


void __RPC_STUB IMediaPlayer_put_EnableFullScreenControls_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_get_ActiveMovie_Proxy( 
    IMediaPlayer * This,
    /* [retval][out] */ IDispatch **ppdispatch);


void __RPC_STUB IMediaPlayer_get_ActiveMovie_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_get_NSPlay_Proxy( 
    IMediaPlayer * This,
    /* [retval][out] */ IDispatch **ppdispatch);


void __RPC_STUB IMediaPlayer_get_NSPlay_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_get_WindowlessVideo_Proxy( 
    IMediaPlayer * This,
    /* [retval][out] */ VARIANT_BOOL *pbool);


void __RPC_STUB IMediaPlayer_get_WindowlessVideo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propput][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_put_WindowlessVideo_Proxy( 
    IMediaPlayer * This,
    /* [in] */ VARIANT_BOOL vbool);


void __RPC_STUB IMediaPlayer_put_WindowlessVideo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_Play_Proxy( 
    IMediaPlayer * This);


void __RPC_STUB IMediaPlayer_Play_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_Stop_Proxy( 
    IMediaPlayer * This);


void __RPC_STUB IMediaPlayer_Stop_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_Pause_Proxy( 
    IMediaPlayer * This);


void __RPC_STUB IMediaPlayer_Pause_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_GetMarkerTime_Proxy( 
    IMediaPlayer * This,
    /* [in] */ long MarkerNum,
    /* [retval][out] */ double *pMarkerTime);


void __RPC_STUB IMediaPlayer_GetMarkerTime_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_GetMarkerName_Proxy( 
    IMediaPlayer * This,
    /* [in] */ long MarkerNum,
    /* [retval][out] */ BSTR *pbstrMarkerName);


void __RPC_STUB IMediaPlayer_GetMarkerName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_AboutBox_Proxy( 
    IMediaPlayer * This);


void __RPC_STUB IMediaPlayer_AboutBox_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_GetCodecInstalled_Proxy( 
    IMediaPlayer * This,
    /* [in] */ long CodecNum,
    /* [retval][out] */ VARIANT_BOOL *pCodecInstalled);


void __RPC_STUB IMediaPlayer_GetCodecInstalled_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_GetCodecDescription_Proxy( 
    IMediaPlayer * This,
    /* [in] */ long CodecNum,
    /* [retval][out] */ BSTR *pbstrCodecDescription);


void __RPC_STUB IMediaPlayer_GetCodecDescription_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_GetCodecURL_Proxy( 
    IMediaPlayer * This,
    /* [in] */ long CodecNum,
    /* [retval][out] */ BSTR *pbstrCodecURL);


void __RPC_STUB IMediaPlayer_GetCodecURL_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_GetMoreInfoURL_Proxy( 
    IMediaPlayer * This,
    /* [in] */ MPMoreInfoType MoreInfoType,
    /* [retval][out] */ BSTR *pbstrMoreInfoURL);


void __RPC_STUB IMediaPlayer_GetMoreInfoURL_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_GetMediaInfoString_Proxy( 
    IMediaPlayer * This,
    /* [in] */ MPMediaInfoType MediaInfoType,
    /* [retval][out] */ BSTR *pbstrMediaInfo);


void __RPC_STUB IMediaPlayer_GetMediaInfoString_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_Cancel_Proxy( 
    IMediaPlayer * This);


void __RPC_STUB IMediaPlayer_Cancel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_Open_Proxy( 
    IMediaPlayer * This,
    /* [in] */ BSTR bstrFileName);


void __RPC_STUB IMediaPlayer_Open_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_IsSoundCardEnabled_Proxy( 
    IMediaPlayer * This,
    /* [retval][out] */ VARIANT_BOOL *pbSoundCard);


void __RPC_STUB IMediaPlayer_IsSoundCardEnabled_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_Next_Proxy( 
    IMediaPlayer * This);


void __RPC_STUB IMediaPlayer_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_Previous_Proxy( 
    IMediaPlayer * This);


void __RPC_STUB IMediaPlayer_Previous_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_StreamSelect_Proxy( 
    IMediaPlayer * This,
    /* [in] */ long StreamNum);


void __RPC_STUB IMediaPlayer_StreamSelect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_FastForward_Proxy( 
    IMediaPlayer * This);


void __RPC_STUB IMediaPlayer_FastForward_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_FastReverse_Proxy( 
    IMediaPlayer * This);


void __RPC_STUB IMediaPlayer_FastReverse_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_GetStreamName_Proxy( 
    IMediaPlayer * This,
    /* [in] */ long StreamNum,
    /* [retval][out] */ BSTR *pbstrStreamName);


void __RPC_STUB IMediaPlayer_GetStreamName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_GetStreamGroup_Proxy( 
    IMediaPlayer * This,
    /* [in] */ long StreamNum,
    /* [retval][out] */ long *pStreamGroup);


void __RPC_STUB IMediaPlayer_GetStreamGroup_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer_GetStreamSelected_Proxy( 
    IMediaPlayer * This,
    /* [in] */ long StreamNum,
    /* [retval][out] */ VARIANT_BOOL *pStreamSelected);


void __RPC_STUB IMediaPlayer_GetStreamSelected_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IMediaPlayer_INTERFACE_DEFINED__ */


#ifndef __IMediaPlayer2_INTERFACE_DEFINED__
#define __IMediaPlayer2_INTERFACE_DEFINED__

/* interface IMediaPlayer2 */
/* [unique][dual][uuid][object] */ 


EXTERN_C const IID IID_IMediaPlayer2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("20D4F5E0-5475-11d2-9774-0000F80855E6")
    IMediaPlayer2 : public IMediaPlayer
    {
    public:
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_DVD( 
            /* [retval][out] */ IMediaPlayerDvd **ppdispatch) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetMediaParameter( 
            /* [in] */ long EntryNum,
            /* [in] */ BSTR bstrParameterName,
            /* [retval][out] */ BSTR *pbstrParameterValue) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetMediaParameterName( 
            /* [in] */ long EntryNum,
            /* [in] */ long Index,
            /* [retval][out] */ BSTR *pbstrParameterName) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_EntryCount( 
            /* [retval][out] */ long *pNumberEntries) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetCurrentEntry( 
            /* [retval][out] */ long *pEntryNumber) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetCurrentEntry( 
            /* [in] */ long EntryNumber) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ShowDialog( 
            /* [in] */ MPShowDialogConstants mpDialogIndex) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMediaPlayer2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMediaPlayer2 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMediaPlayer2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMediaPlayer2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IMediaPlayer2 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IMediaPlayer2 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IMediaPlayer2 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IMediaPlayer2 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentPosition )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ double *pCurrentPosition);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_CurrentPosition )( 
            IMediaPlayer2 * This,
            /* [in] */ double CurrentPosition);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Duration )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ double *pDuration);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ImageSourceWidth )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *pWidth);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ImageSourceHeight )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *pHeight);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_MarkerCount )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *pMarkerCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CanScan )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pCanScan);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CanSeek )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pCanSeek);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CanSeekToMarkers )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pCanSeekToMarkers);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentMarker )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *pCurrentMarker);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_CurrentMarker )( 
            IMediaPlayer2 * This,
            /* [in] */ long CurrentMarker);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_FileName )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ BSTR *pbstrFileName);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_FileName )( 
            IMediaPlayer2 * This,
            /* [in] */ BSTR bstrFileName);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SourceLink )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ BSTR *pbstrSourceLink);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CreationDate )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ DATE *pCreationDate);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ErrorCorrection )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ BSTR *pbstrErrorCorrection);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Bandwidth )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *pBandwidth);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SourceProtocol )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *pSourceProtocol);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ReceivedPackets )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *pReceivedPackets);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_RecoveredPackets )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *pRecoveredPackets);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LostPackets )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *pLostPackets);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ReceptionQuality )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *pReceptionQuality);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_BufferingCount )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *pBufferingCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_IsBroadcast )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pIsBroadcast);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_BufferingProgress )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *pBufferingProgress);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ChannelName )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ BSTR *pbstrChannelName);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ChannelDescription )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ BSTR *pbstrChannelDescription);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ChannelURL )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ BSTR *pbstrChannelURL);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ContactAddress )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ BSTR *pbstrContactAddress);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ContactPhone )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ BSTR *pbstrContactPhone);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ContactEmail )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ BSTR *pbstrContactEmail);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_BufferingTime )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ double *pBufferingTime);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_BufferingTime )( 
            IMediaPlayer2 * This,
            /* [in] */ double BufferingTime);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AutoStart )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pAutoStart);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AutoStart )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL AutoStart);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AutoRewind )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pAutoRewind);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AutoRewind )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL AutoRewind);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Rate )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ double *pRate);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Rate )( 
            IMediaPlayer2 * This,
            /* [in] */ double Rate);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendKeyboardEvents )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pSendKeyboardEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendKeyboardEvents )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL SendKeyboardEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendMouseClickEvents )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pSendMouseClickEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendMouseClickEvents )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL SendMouseClickEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendMouseMoveEvents )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pSendMouseMoveEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendMouseMoveEvents )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL SendMouseMoveEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlayCount )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *pPlayCount);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_PlayCount )( 
            IMediaPlayer2 * This,
            /* [in] */ long PlayCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ClickToPlay )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pClickToPlay);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ClickToPlay )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL ClickToPlay);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AllowScan )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pAllowScan);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AllowScan )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL AllowScan);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableContextMenu )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pEnableContextMenu);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableContextMenu )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL EnableContextMenu);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CursorType )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *pCursorType);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_CursorType )( 
            IMediaPlayer2 * This,
            /* [in] */ long CursorType);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CodecCount )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *pCodecCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AllowChangeDisplaySize )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pAllowChangeDisplaySize);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AllowChangeDisplaySize )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL AllowChangeDisplaySize);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_IsDurationValid )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pIsDurationValid);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OpenState )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *pOpenState);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendOpenStateChangeEvents )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pSendOpenStateChangeEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendOpenStateChangeEvents )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL SendOpenStateChangeEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendWarningEvents )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pSendWarningEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendWarningEvents )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL SendWarningEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendErrorEvents )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pSendErrorEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendErrorEvents )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL SendErrorEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlayState )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ MPPlayStateConstants *pPlayState);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendPlayStateChangeEvents )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pSendPlayStateChangeEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendPlayStateChangeEvents )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL SendPlayStateChangeEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_DisplaySize )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ MPDisplaySizeConstants *pDisplaySize);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_DisplaySize )( 
            IMediaPlayer2 * This,
            /* [in] */ MPDisplaySizeConstants DisplaySize);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_InvokeURLs )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pInvokeURLs);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_InvokeURLs )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL InvokeURLs);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_BaseURL )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ BSTR *pbstrBaseURL);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_BaseURL )( 
            IMediaPlayer2 * This,
            /* [in] */ BSTR bstrBaseURL);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultFrame )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ BSTR *pbstrDefaultFrame);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_DefaultFrame )( 
            IMediaPlayer2 * This,
            /* [in] */ BSTR bstrDefaultFrame);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HasError )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pHasError);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ErrorDescription )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ BSTR *pbstrErrorDescription);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ErrorCode )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *pErrorCode);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AnimationAtStart )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pAnimationAtStart);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AnimationAtStart )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL AnimationAtStart);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TransparentAtStart )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pTransparentAtStart);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_TransparentAtStart )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL TransparentAtStart);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Volume )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *pVolume);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Volume )( 
            IMediaPlayer2 * This,
            /* [in] */ long Volume);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Balance )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *pBalance);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Balance )( 
            IMediaPlayer2 * This,
            /* [in] */ long Balance);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ReadyState )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ MPReadyStateConstants *pValue);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SelectionStart )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ double *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SelectionStart )( 
            IMediaPlayer2 * This,
            /* [in] */ double Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SelectionEnd )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ double *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SelectionEnd )( 
            IMediaPlayer2 * This,
            /* [in] */ double Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowDisplay )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowDisplay )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowControls )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowControls )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowPositionControls )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowPositionControls )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowTracker )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowTracker )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnablePositionControls )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *Enable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnablePositionControls )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL Enable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableTracker )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *Enable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableTracker )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL Enable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Enabled )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pEnabled);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Enabled )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL Enabled);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_DisplayForeColor )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VB_OLE_COLOR *ForeColor);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_DisplayForeColor )( 
            IMediaPlayer2 * This,
            /* [in] */ VB_OLE_COLOR ForeColor);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_DisplayBackColor )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VB_OLE_COLOR *BackColor);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_DisplayBackColor )( 
            IMediaPlayer2 * This,
            /* [in] */ VB_OLE_COLOR BackColor);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_DisplayMode )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ MPDisplayModeConstants *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_DisplayMode )( 
            IMediaPlayer2 * This,
            /* [in] */ MPDisplayModeConstants Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_VideoBorder3D )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pVideoBorderWidth);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_VideoBorder3D )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL VideoBorderWidth);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_VideoBorderWidth )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *pVideoBorderWidth);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_VideoBorderWidth )( 
            IMediaPlayer2 * This,
            /* [in] */ long VideoBorderWidth);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_VideoBorderColor )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VB_OLE_COLOR *pVideoBorderWidth);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_VideoBorderColor )( 
            IMediaPlayer2 * This,
            /* [in] */ VB_OLE_COLOR VideoBorderColor);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowGotoBar )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pbool);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowGotoBar )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL vbool);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowStatusBar )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pbool);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowStatusBar )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL vbool);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowCaptioning )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pbool);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowCaptioning )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL pbool);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowAudioControls )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pbool);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowAudioControls )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL bBool);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CaptioningID )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ BSTR *pstrText);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_CaptioningID )( 
            IMediaPlayer2 * This,
            /* [in] */ BSTR strText);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Mute )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *vbool);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Mute )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL vbool);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CanPreview )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pCanPreview);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PreviewMode )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pPreviewMode);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_PreviewMode )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL PreviewMode);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HasMultipleItems )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pHasMuliItems);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Language )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *pLanguage);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Language )( 
            IMediaPlayer2 * This,
            /* [in] */ long Language);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AudioStream )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *pStream);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AudioStream )( 
            IMediaPlayer2 * This,
            /* [in] */ long Stream);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SAMIStyle )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ BSTR *pbstrStyle);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SAMIStyle )( 
            IMediaPlayer2 * This,
            /* [in] */ BSTR bstrStyle);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SAMILang )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ BSTR *pbstrLang);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SAMILang )( 
            IMediaPlayer2 * This,
            /* [in] */ BSTR bstrLang);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SAMIFileName )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ BSTR *pbstrFileName);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SAMIFileName )( 
            IMediaPlayer2 * This,
            /* [in] */ BSTR bstrFileName);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_StreamCount )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *pStreamCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ClientId )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ BSTR *pbstrClientId);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ConnectionSpeed )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *plConnectionSpeed);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AutoSize )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pbool);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AutoSize )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL vbool);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableFullScreenControls )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pbVal);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableFullScreenControls )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL bVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ActiveMovie )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ IDispatch **ppdispatch);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NSPlay )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ IDispatch **ppdispatch);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_WindowlessVideo )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pbool);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_WindowlessVideo )( 
            IMediaPlayer2 * This,
            /* [in] */ VARIANT_BOOL vbool);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Play )( 
            IMediaPlayer2 * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Stop )( 
            IMediaPlayer2 * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Pause )( 
            IMediaPlayer2 * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetMarkerTime )( 
            IMediaPlayer2 * This,
            /* [in] */ long MarkerNum,
            /* [retval][out] */ double *pMarkerTime);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetMarkerName )( 
            IMediaPlayer2 * This,
            /* [in] */ long MarkerNum,
            /* [retval][out] */ BSTR *pbstrMarkerName);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *AboutBox )( 
            IMediaPlayer2 * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetCodecInstalled )( 
            IMediaPlayer2 * This,
            /* [in] */ long CodecNum,
            /* [retval][out] */ VARIANT_BOOL *pCodecInstalled);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetCodecDescription )( 
            IMediaPlayer2 * This,
            /* [in] */ long CodecNum,
            /* [retval][out] */ BSTR *pbstrCodecDescription);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetCodecURL )( 
            IMediaPlayer2 * This,
            /* [in] */ long CodecNum,
            /* [retval][out] */ BSTR *pbstrCodecURL);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetMoreInfoURL )( 
            IMediaPlayer2 * This,
            /* [in] */ MPMoreInfoType MoreInfoType,
            /* [retval][out] */ BSTR *pbstrMoreInfoURL);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetMediaInfoString )( 
            IMediaPlayer2 * This,
            /* [in] */ MPMediaInfoType MediaInfoType,
            /* [retval][out] */ BSTR *pbstrMediaInfo);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Cancel )( 
            IMediaPlayer2 * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Open )( 
            IMediaPlayer2 * This,
            /* [in] */ BSTR bstrFileName);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *IsSoundCardEnabled )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ VARIANT_BOOL *pbSoundCard);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Next )( 
            IMediaPlayer2 * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Previous )( 
            IMediaPlayer2 * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *StreamSelect )( 
            IMediaPlayer2 * This,
            /* [in] */ long StreamNum);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *FastForward )( 
            IMediaPlayer2 * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *FastReverse )( 
            IMediaPlayer2 * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetStreamName )( 
            IMediaPlayer2 * This,
            /* [in] */ long StreamNum,
            /* [retval][out] */ BSTR *pbstrStreamName);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetStreamGroup )( 
            IMediaPlayer2 * This,
            /* [in] */ long StreamNum,
            /* [retval][out] */ long *pStreamGroup);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetStreamSelected )( 
            IMediaPlayer2 * This,
            /* [in] */ long StreamNum,
            /* [retval][out] */ VARIANT_BOOL *pStreamSelected);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DVD )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ IMediaPlayerDvd **ppdispatch);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetMediaParameter )( 
            IMediaPlayer2 * This,
            /* [in] */ long EntryNum,
            /* [in] */ BSTR bstrParameterName,
            /* [retval][out] */ BSTR *pbstrParameterValue);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetMediaParameterName )( 
            IMediaPlayer2 * This,
            /* [in] */ long EntryNum,
            /* [in] */ long Index,
            /* [retval][out] */ BSTR *pbstrParameterName);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntryCount )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *pNumberEntries);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetCurrentEntry )( 
            IMediaPlayer2 * This,
            /* [retval][out] */ long *pEntryNumber);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetCurrentEntry )( 
            IMediaPlayer2 * This,
            /* [in] */ long EntryNumber);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ShowDialog )( 
            IMediaPlayer2 * This,
            /* [in] */ MPShowDialogConstants mpDialogIndex);
        
        END_INTERFACE
    } IMediaPlayer2Vtbl;

    interface IMediaPlayer2
    {
        CONST_VTBL struct IMediaPlayer2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMediaPlayer2_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMediaPlayer2_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMediaPlayer2_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IMediaPlayer2_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IMediaPlayer2_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IMediaPlayer2_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IMediaPlayer2_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IMediaPlayer2_get_CurrentPosition(This,pCurrentPosition)	\
    ( (This)->lpVtbl -> get_CurrentPosition(This,pCurrentPosition) ) 

#define IMediaPlayer2_put_CurrentPosition(This,CurrentPosition)	\
    ( (This)->lpVtbl -> put_CurrentPosition(This,CurrentPosition) ) 

#define IMediaPlayer2_get_Duration(This,pDuration)	\
    ( (This)->lpVtbl -> get_Duration(This,pDuration) ) 

#define IMediaPlayer2_get_ImageSourceWidth(This,pWidth)	\
    ( (This)->lpVtbl -> get_ImageSourceWidth(This,pWidth) ) 

#define IMediaPlayer2_get_ImageSourceHeight(This,pHeight)	\
    ( (This)->lpVtbl -> get_ImageSourceHeight(This,pHeight) ) 

#define IMediaPlayer2_get_MarkerCount(This,pMarkerCount)	\
    ( (This)->lpVtbl -> get_MarkerCount(This,pMarkerCount) ) 

#define IMediaPlayer2_get_CanScan(This,pCanScan)	\
    ( (This)->lpVtbl -> get_CanScan(This,pCanScan) ) 

#define IMediaPlayer2_get_CanSeek(This,pCanSeek)	\
    ( (This)->lpVtbl -> get_CanSeek(This,pCanSeek) ) 

#define IMediaPlayer2_get_CanSeekToMarkers(This,pCanSeekToMarkers)	\
    ( (This)->lpVtbl -> get_CanSeekToMarkers(This,pCanSeekToMarkers) ) 

#define IMediaPlayer2_get_CurrentMarker(This,pCurrentMarker)	\
    ( (This)->lpVtbl -> get_CurrentMarker(This,pCurrentMarker) ) 

#define IMediaPlayer2_put_CurrentMarker(This,CurrentMarker)	\
    ( (This)->lpVtbl -> put_CurrentMarker(This,CurrentMarker) ) 

#define IMediaPlayer2_get_FileName(This,pbstrFileName)	\
    ( (This)->lpVtbl -> get_FileName(This,pbstrFileName) ) 

#define IMediaPlayer2_put_FileName(This,bstrFileName)	\
    ( (This)->lpVtbl -> put_FileName(This,bstrFileName) ) 

#define IMediaPlayer2_get_SourceLink(This,pbstrSourceLink)	\
    ( (This)->lpVtbl -> get_SourceLink(This,pbstrSourceLink) ) 

#define IMediaPlayer2_get_CreationDate(This,pCreationDate)	\
    ( (This)->lpVtbl -> get_CreationDate(This,pCreationDate) ) 

#define IMediaPlayer2_get_ErrorCorrection(This,pbstrErrorCorrection)	\
    ( (This)->lpVtbl -> get_ErrorCorrection(This,pbstrErrorCorrection) ) 

#define IMediaPlayer2_get_Bandwidth(This,pBandwidth)	\
    ( (This)->lpVtbl -> get_Bandwidth(This,pBandwidth) ) 

#define IMediaPlayer2_get_SourceProtocol(This,pSourceProtocol)	\
    ( (This)->lpVtbl -> get_SourceProtocol(This,pSourceProtocol) ) 

#define IMediaPlayer2_get_ReceivedPackets(This,pReceivedPackets)	\
    ( (This)->lpVtbl -> get_ReceivedPackets(This,pReceivedPackets) ) 

#define IMediaPlayer2_get_RecoveredPackets(This,pRecoveredPackets)	\
    ( (This)->lpVtbl -> get_RecoveredPackets(This,pRecoveredPackets) ) 

#define IMediaPlayer2_get_LostPackets(This,pLostPackets)	\
    ( (This)->lpVtbl -> get_LostPackets(This,pLostPackets) ) 

#define IMediaPlayer2_get_ReceptionQuality(This,pReceptionQuality)	\
    ( (This)->lpVtbl -> get_ReceptionQuality(This,pReceptionQuality) ) 

#define IMediaPlayer2_get_BufferingCount(This,pBufferingCount)	\
    ( (This)->lpVtbl -> get_BufferingCount(This,pBufferingCount) ) 

#define IMediaPlayer2_get_IsBroadcast(This,pIsBroadcast)	\
    ( (This)->lpVtbl -> get_IsBroadcast(This,pIsBroadcast) ) 

#define IMediaPlayer2_get_BufferingProgress(This,pBufferingProgress)	\
    ( (This)->lpVtbl -> get_BufferingProgress(This,pBufferingProgress) ) 

#define IMediaPlayer2_get_ChannelName(This,pbstrChannelName)	\
    ( (This)->lpVtbl -> get_ChannelName(This,pbstrChannelName) ) 

#define IMediaPlayer2_get_ChannelDescription(This,pbstrChannelDescription)	\
    ( (This)->lpVtbl -> get_ChannelDescription(This,pbstrChannelDescription) ) 

#define IMediaPlayer2_get_ChannelURL(This,pbstrChannelURL)	\
    ( (This)->lpVtbl -> get_ChannelURL(This,pbstrChannelURL) ) 

#define IMediaPlayer2_get_ContactAddress(This,pbstrContactAddress)	\
    ( (This)->lpVtbl -> get_ContactAddress(This,pbstrContactAddress) ) 

#define IMediaPlayer2_get_ContactPhone(This,pbstrContactPhone)	\
    ( (This)->lpVtbl -> get_ContactPhone(This,pbstrContactPhone) ) 

#define IMediaPlayer2_get_ContactEmail(This,pbstrContactEmail)	\
    ( (This)->lpVtbl -> get_ContactEmail(This,pbstrContactEmail) ) 

#define IMediaPlayer2_get_BufferingTime(This,pBufferingTime)	\
    ( (This)->lpVtbl -> get_BufferingTime(This,pBufferingTime) ) 

#define IMediaPlayer2_put_BufferingTime(This,BufferingTime)	\
    ( (This)->lpVtbl -> put_BufferingTime(This,BufferingTime) ) 

#define IMediaPlayer2_get_AutoStart(This,pAutoStart)	\
    ( (This)->lpVtbl -> get_AutoStart(This,pAutoStart) ) 

#define IMediaPlayer2_put_AutoStart(This,AutoStart)	\
    ( (This)->lpVtbl -> put_AutoStart(This,AutoStart) ) 

#define IMediaPlayer2_get_AutoRewind(This,pAutoRewind)	\
    ( (This)->lpVtbl -> get_AutoRewind(This,pAutoRewind) ) 

#define IMediaPlayer2_put_AutoRewind(This,AutoRewind)	\
    ( (This)->lpVtbl -> put_AutoRewind(This,AutoRewind) ) 

#define IMediaPlayer2_get_Rate(This,pRate)	\
    ( (This)->lpVtbl -> get_Rate(This,pRate) ) 

#define IMediaPlayer2_put_Rate(This,Rate)	\
    ( (This)->lpVtbl -> put_Rate(This,Rate) ) 

#define IMediaPlayer2_get_SendKeyboardEvents(This,pSendKeyboardEvents)	\
    ( (This)->lpVtbl -> get_SendKeyboardEvents(This,pSendKeyboardEvents) ) 

#define IMediaPlayer2_put_SendKeyboardEvents(This,SendKeyboardEvents)	\
    ( (This)->lpVtbl -> put_SendKeyboardEvents(This,SendKeyboardEvents) ) 

#define IMediaPlayer2_get_SendMouseClickEvents(This,pSendMouseClickEvents)	\
    ( (This)->lpVtbl -> get_SendMouseClickEvents(This,pSendMouseClickEvents) ) 

#define IMediaPlayer2_put_SendMouseClickEvents(This,SendMouseClickEvents)	\
    ( (This)->lpVtbl -> put_SendMouseClickEvents(This,SendMouseClickEvents) ) 

#define IMediaPlayer2_get_SendMouseMoveEvents(This,pSendMouseMoveEvents)	\
    ( (This)->lpVtbl -> get_SendMouseMoveEvents(This,pSendMouseMoveEvents) ) 

#define IMediaPlayer2_put_SendMouseMoveEvents(This,SendMouseMoveEvents)	\
    ( (This)->lpVtbl -> put_SendMouseMoveEvents(This,SendMouseMoveEvents) ) 

#define IMediaPlayer2_get_PlayCount(This,pPlayCount)	\
    ( (This)->lpVtbl -> get_PlayCount(This,pPlayCount) ) 

#define IMediaPlayer2_put_PlayCount(This,PlayCount)	\
    ( (This)->lpVtbl -> put_PlayCount(This,PlayCount) ) 

#define IMediaPlayer2_get_ClickToPlay(This,pClickToPlay)	\
    ( (This)->lpVtbl -> get_ClickToPlay(This,pClickToPlay) ) 

#define IMediaPlayer2_put_ClickToPlay(This,ClickToPlay)	\
    ( (This)->lpVtbl -> put_ClickToPlay(This,ClickToPlay) ) 

#define IMediaPlayer2_get_AllowScan(This,pAllowScan)	\
    ( (This)->lpVtbl -> get_AllowScan(This,pAllowScan) ) 

#define IMediaPlayer2_put_AllowScan(This,AllowScan)	\
    ( (This)->lpVtbl -> put_AllowScan(This,AllowScan) ) 

#define IMediaPlayer2_get_EnableContextMenu(This,pEnableContextMenu)	\
    ( (This)->lpVtbl -> get_EnableContextMenu(This,pEnableContextMenu) ) 

#define IMediaPlayer2_put_EnableContextMenu(This,EnableContextMenu)	\
    ( (This)->lpVtbl -> put_EnableContextMenu(This,EnableContextMenu) ) 

#define IMediaPlayer2_get_CursorType(This,pCursorType)	\
    ( (This)->lpVtbl -> get_CursorType(This,pCursorType) ) 

#define IMediaPlayer2_put_CursorType(This,CursorType)	\
    ( (This)->lpVtbl -> put_CursorType(This,CursorType) ) 

#define IMediaPlayer2_get_CodecCount(This,pCodecCount)	\
    ( (This)->lpVtbl -> get_CodecCount(This,pCodecCount) ) 

#define IMediaPlayer2_get_AllowChangeDisplaySize(This,pAllowChangeDisplaySize)	\
    ( (This)->lpVtbl -> get_AllowChangeDisplaySize(This,pAllowChangeDisplaySize) ) 

#define IMediaPlayer2_put_AllowChangeDisplaySize(This,AllowChangeDisplaySize)	\
    ( (This)->lpVtbl -> put_AllowChangeDisplaySize(This,AllowChangeDisplaySize) ) 

#define IMediaPlayer2_get_IsDurationValid(This,pIsDurationValid)	\
    ( (This)->lpVtbl -> get_IsDurationValid(This,pIsDurationValid) ) 

#define IMediaPlayer2_get_OpenState(This,pOpenState)	\
    ( (This)->lpVtbl -> get_OpenState(This,pOpenState) ) 

#define IMediaPlayer2_get_SendOpenStateChangeEvents(This,pSendOpenStateChangeEvents)	\
    ( (This)->lpVtbl -> get_SendOpenStateChangeEvents(This,pSendOpenStateChangeEvents) ) 

#define IMediaPlayer2_put_SendOpenStateChangeEvents(This,SendOpenStateChangeEvents)	\
    ( (This)->lpVtbl -> put_SendOpenStateChangeEvents(This,SendOpenStateChangeEvents) ) 

#define IMediaPlayer2_get_SendWarningEvents(This,pSendWarningEvents)	\
    ( (This)->lpVtbl -> get_SendWarningEvents(This,pSendWarningEvents) ) 

#define IMediaPlayer2_put_SendWarningEvents(This,SendWarningEvents)	\
    ( (This)->lpVtbl -> put_SendWarningEvents(This,SendWarningEvents) ) 

#define IMediaPlayer2_get_SendErrorEvents(This,pSendErrorEvents)	\
    ( (This)->lpVtbl -> get_SendErrorEvents(This,pSendErrorEvents) ) 

#define IMediaPlayer2_put_SendErrorEvents(This,SendErrorEvents)	\
    ( (This)->lpVtbl -> put_SendErrorEvents(This,SendErrorEvents) ) 

#define IMediaPlayer2_get_PlayState(This,pPlayState)	\
    ( (This)->lpVtbl -> get_PlayState(This,pPlayState) ) 

#define IMediaPlayer2_get_SendPlayStateChangeEvents(This,pSendPlayStateChangeEvents)	\
    ( (This)->lpVtbl -> get_SendPlayStateChangeEvents(This,pSendPlayStateChangeEvents) ) 

#define IMediaPlayer2_put_SendPlayStateChangeEvents(This,SendPlayStateChangeEvents)	\
    ( (This)->lpVtbl -> put_SendPlayStateChangeEvents(This,SendPlayStateChangeEvents) ) 

#define IMediaPlayer2_get_DisplaySize(This,pDisplaySize)	\
    ( (This)->lpVtbl -> get_DisplaySize(This,pDisplaySize) ) 

#define IMediaPlayer2_put_DisplaySize(This,DisplaySize)	\
    ( (This)->lpVtbl -> put_DisplaySize(This,DisplaySize) ) 

#define IMediaPlayer2_get_InvokeURLs(This,pInvokeURLs)	\
    ( (This)->lpVtbl -> get_InvokeURLs(This,pInvokeURLs) ) 

#define IMediaPlayer2_put_InvokeURLs(This,InvokeURLs)	\
    ( (This)->lpVtbl -> put_InvokeURLs(This,InvokeURLs) ) 

#define IMediaPlayer2_get_BaseURL(This,pbstrBaseURL)	\
    ( (This)->lpVtbl -> get_BaseURL(This,pbstrBaseURL) ) 

#define IMediaPlayer2_put_BaseURL(This,bstrBaseURL)	\
    ( (This)->lpVtbl -> put_BaseURL(This,bstrBaseURL) ) 

#define IMediaPlayer2_get_DefaultFrame(This,pbstrDefaultFrame)	\
    ( (This)->lpVtbl -> get_DefaultFrame(This,pbstrDefaultFrame) ) 

#define IMediaPlayer2_put_DefaultFrame(This,bstrDefaultFrame)	\
    ( (This)->lpVtbl -> put_DefaultFrame(This,bstrDefaultFrame) ) 

#define IMediaPlayer2_get_HasError(This,pHasError)	\
    ( (This)->lpVtbl -> get_HasError(This,pHasError) ) 

#define IMediaPlayer2_get_ErrorDescription(This,pbstrErrorDescription)	\
    ( (This)->lpVtbl -> get_ErrorDescription(This,pbstrErrorDescription) ) 

#define IMediaPlayer2_get_ErrorCode(This,pErrorCode)	\
    ( (This)->lpVtbl -> get_ErrorCode(This,pErrorCode) ) 

#define IMediaPlayer2_get_AnimationAtStart(This,pAnimationAtStart)	\
    ( (This)->lpVtbl -> get_AnimationAtStart(This,pAnimationAtStart) ) 

#define IMediaPlayer2_put_AnimationAtStart(This,AnimationAtStart)	\
    ( (This)->lpVtbl -> put_AnimationAtStart(This,AnimationAtStart) ) 

#define IMediaPlayer2_get_TransparentAtStart(This,pTransparentAtStart)	\
    ( (This)->lpVtbl -> get_TransparentAtStart(This,pTransparentAtStart) ) 

#define IMediaPlayer2_put_TransparentAtStart(This,TransparentAtStart)	\
    ( (This)->lpVtbl -> put_TransparentAtStart(This,TransparentAtStart) ) 

#define IMediaPlayer2_get_Volume(This,pVolume)	\
    ( (This)->lpVtbl -> get_Volume(This,pVolume) ) 

#define IMediaPlayer2_put_Volume(This,Volume)	\
    ( (This)->lpVtbl -> put_Volume(This,Volume) ) 

#define IMediaPlayer2_get_Balance(This,pBalance)	\
    ( (This)->lpVtbl -> get_Balance(This,pBalance) ) 

#define IMediaPlayer2_put_Balance(This,Balance)	\
    ( (This)->lpVtbl -> put_Balance(This,Balance) ) 

#define IMediaPlayer2_get_ReadyState(This,pValue)	\
    ( (This)->lpVtbl -> get_ReadyState(This,pValue) ) 

#define IMediaPlayer2_get_SelectionStart(This,pValue)	\
    ( (This)->lpVtbl -> get_SelectionStart(This,pValue) ) 

#define IMediaPlayer2_put_SelectionStart(This,Value)	\
    ( (This)->lpVtbl -> put_SelectionStart(This,Value) ) 

#define IMediaPlayer2_get_SelectionEnd(This,pValue)	\
    ( (This)->lpVtbl -> get_SelectionEnd(This,pValue) ) 

#define IMediaPlayer2_put_SelectionEnd(This,Value)	\
    ( (This)->lpVtbl -> put_SelectionEnd(This,Value) ) 

#define IMediaPlayer2_get_ShowDisplay(This,Show)	\
    ( (This)->lpVtbl -> get_ShowDisplay(This,Show) ) 

#define IMediaPlayer2_put_ShowDisplay(This,Show)	\
    ( (This)->lpVtbl -> put_ShowDisplay(This,Show) ) 

#define IMediaPlayer2_get_ShowControls(This,Show)	\
    ( (This)->lpVtbl -> get_ShowControls(This,Show) ) 

#define IMediaPlayer2_put_ShowControls(This,Show)	\
    ( (This)->lpVtbl -> put_ShowControls(This,Show) ) 

#define IMediaPlayer2_get_ShowPositionControls(This,Show)	\
    ( (This)->lpVtbl -> get_ShowPositionControls(This,Show) ) 

#define IMediaPlayer2_put_ShowPositionControls(This,Show)	\
    ( (This)->lpVtbl -> put_ShowPositionControls(This,Show) ) 

#define IMediaPlayer2_get_ShowTracker(This,Show)	\
    ( (This)->lpVtbl -> get_ShowTracker(This,Show) ) 

#define IMediaPlayer2_put_ShowTracker(This,Show)	\
    ( (This)->lpVtbl -> put_ShowTracker(This,Show) ) 

#define IMediaPlayer2_get_EnablePositionControls(This,Enable)	\
    ( (This)->lpVtbl -> get_EnablePositionControls(This,Enable) ) 

#define IMediaPlayer2_put_EnablePositionControls(This,Enable)	\
    ( (This)->lpVtbl -> put_EnablePositionControls(This,Enable) ) 

#define IMediaPlayer2_get_EnableTracker(This,Enable)	\
    ( (This)->lpVtbl -> get_EnableTracker(This,Enable) ) 

#define IMediaPlayer2_put_EnableTracker(This,Enable)	\
    ( (This)->lpVtbl -> put_EnableTracker(This,Enable) ) 

#define IMediaPlayer2_get_Enabled(This,pEnabled)	\
    ( (This)->lpVtbl -> get_Enabled(This,pEnabled) ) 

#define IMediaPlayer2_put_Enabled(This,Enabled)	\
    ( (This)->lpVtbl -> put_Enabled(This,Enabled) ) 

#define IMediaPlayer2_get_DisplayForeColor(This,ForeColor)	\
    ( (This)->lpVtbl -> get_DisplayForeColor(This,ForeColor) ) 

#define IMediaPlayer2_put_DisplayForeColor(This,ForeColor)	\
    ( (This)->lpVtbl -> put_DisplayForeColor(This,ForeColor) ) 

#define IMediaPlayer2_get_DisplayBackColor(This,BackColor)	\
    ( (This)->lpVtbl -> get_DisplayBackColor(This,BackColor) ) 

#define IMediaPlayer2_put_DisplayBackColor(This,BackColor)	\
    ( (This)->lpVtbl -> put_DisplayBackColor(This,BackColor) ) 

#define IMediaPlayer2_get_DisplayMode(This,pValue)	\
    ( (This)->lpVtbl -> get_DisplayMode(This,pValue) ) 

#define IMediaPlayer2_put_DisplayMode(This,Value)	\
    ( (This)->lpVtbl -> put_DisplayMode(This,Value) ) 

#define IMediaPlayer2_get_VideoBorder3D(This,pVideoBorderWidth)	\
    ( (This)->lpVtbl -> get_VideoBorder3D(This,pVideoBorderWidth) ) 

#define IMediaPlayer2_put_VideoBorder3D(This,VideoBorderWidth)	\
    ( (This)->lpVtbl -> put_VideoBorder3D(This,VideoBorderWidth) ) 

#define IMediaPlayer2_get_VideoBorderWidth(This,pVideoBorderWidth)	\
    ( (This)->lpVtbl -> get_VideoBorderWidth(This,pVideoBorderWidth) ) 

#define IMediaPlayer2_put_VideoBorderWidth(This,VideoBorderWidth)	\
    ( (This)->lpVtbl -> put_VideoBorderWidth(This,VideoBorderWidth) ) 

#define IMediaPlayer2_get_VideoBorderColor(This,pVideoBorderWidth)	\
    ( (This)->lpVtbl -> get_VideoBorderColor(This,pVideoBorderWidth) ) 

#define IMediaPlayer2_put_VideoBorderColor(This,VideoBorderColor)	\
    ( (This)->lpVtbl -> put_VideoBorderColor(This,VideoBorderColor) ) 

#define IMediaPlayer2_get_ShowGotoBar(This,pbool)	\
    ( (This)->lpVtbl -> get_ShowGotoBar(This,pbool) ) 

#define IMediaPlayer2_put_ShowGotoBar(This,vbool)	\
    ( (This)->lpVtbl -> put_ShowGotoBar(This,vbool) ) 

#define IMediaPlayer2_get_ShowStatusBar(This,pbool)	\
    ( (This)->lpVtbl -> get_ShowStatusBar(This,pbool) ) 

#define IMediaPlayer2_put_ShowStatusBar(This,vbool)	\
    ( (This)->lpVtbl -> put_ShowStatusBar(This,vbool) ) 

#define IMediaPlayer2_get_ShowCaptioning(This,pbool)	\
    ( (This)->lpVtbl -> get_ShowCaptioning(This,pbool) ) 

#define IMediaPlayer2_put_ShowCaptioning(This,pbool)	\
    ( (This)->lpVtbl -> put_ShowCaptioning(This,pbool) ) 

#define IMediaPlayer2_get_ShowAudioControls(This,pbool)	\
    ( (This)->lpVtbl -> get_ShowAudioControls(This,pbool) ) 

#define IMediaPlayer2_put_ShowAudioControls(This,bBool)	\
    ( (This)->lpVtbl -> put_ShowAudioControls(This,bBool) ) 

#define IMediaPlayer2_get_CaptioningID(This,pstrText)	\
    ( (This)->lpVtbl -> get_CaptioningID(This,pstrText) ) 

#define IMediaPlayer2_put_CaptioningID(This,strText)	\
    ( (This)->lpVtbl -> put_CaptioningID(This,strText) ) 

#define IMediaPlayer2_get_Mute(This,vbool)	\
    ( (This)->lpVtbl -> get_Mute(This,vbool) ) 

#define IMediaPlayer2_put_Mute(This,vbool)	\
    ( (This)->lpVtbl -> put_Mute(This,vbool) ) 

#define IMediaPlayer2_get_CanPreview(This,pCanPreview)	\
    ( (This)->lpVtbl -> get_CanPreview(This,pCanPreview) ) 

#define IMediaPlayer2_get_PreviewMode(This,pPreviewMode)	\
    ( (This)->lpVtbl -> get_PreviewMode(This,pPreviewMode) ) 

#define IMediaPlayer2_put_PreviewMode(This,PreviewMode)	\
    ( (This)->lpVtbl -> put_PreviewMode(This,PreviewMode) ) 

#define IMediaPlayer2_get_HasMultipleItems(This,pHasMuliItems)	\
    ( (This)->lpVtbl -> get_HasMultipleItems(This,pHasMuliItems) ) 

#define IMediaPlayer2_get_Language(This,pLanguage)	\
    ( (This)->lpVtbl -> get_Language(This,pLanguage) ) 

#define IMediaPlayer2_put_Language(This,Language)	\
    ( (This)->lpVtbl -> put_Language(This,Language) ) 

#define IMediaPlayer2_get_AudioStream(This,pStream)	\
    ( (This)->lpVtbl -> get_AudioStream(This,pStream) ) 

#define IMediaPlayer2_put_AudioStream(This,Stream)	\
    ( (This)->lpVtbl -> put_AudioStream(This,Stream) ) 

#define IMediaPlayer2_get_SAMIStyle(This,pbstrStyle)	\
    ( (This)->lpVtbl -> get_SAMIStyle(This,pbstrStyle) ) 

#define IMediaPlayer2_put_SAMIStyle(This,bstrStyle)	\
    ( (This)->lpVtbl -> put_SAMIStyle(This,bstrStyle) ) 

#define IMediaPlayer2_get_SAMILang(This,pbstrLang)	\
    ( (This)->lpVtbl -> get_SAMILang(This,pbstrLang) ) 

#define IMediaPlayer2_put_SAMILang(This,bstrLang)	\
    ( (This)->lpVtbl -> put_SAMILang(This,bstrLang) ) 

#define IMediaPlayer2_get_SAMIFileName(This,pbstrFileName)	\
    ( (This)->lpVtbl -> get_SAMIFileName(This,pbstrFileName) ) 

#define IMediaPlayer2_put_SAMIFileName(This,bstrFileName)	\
    ( (This)->lpVtbl -> put_SAMIFileName(This,bstrFileName) ) 

#define IMediaPlayer2_get_StreamCount(This,pStreamCount)	\
    ( (This)->lpVtbl -> get_StreamCount(This,pStreamCount) ) 

#define IMediaPlayer2_get_ClientId(This,pbstrClientId)	\
    ( (This)->lpVtbl -> get_ClientId(This,pbstrClientId) ) 

#define IMediaPlayer2_get_ConnectionSpeed(This,plConnectionSpeed)	\
    ( (This)->lpVtbl -> get_ConnectionSpeed(This,plConnectionSpeed) ) 

#define IMediaPlayer2_get_AutoSize(This,pbool)	\
    ( (This)->lpVtbl -> get_AutoSize(This,pbool) ) 

#define IMediaPlayer2_put_AutoSize(This,vbool)	\
    ( (This)->lpVtbl -> put_AutoSize(This,vbool) ) 

#define IMediaPlayer2_get_EnableFullScreenControls(This,pbVal)	\
    ( (This)->lpVtbl -> get_EnableFullScreenControls(This,pbVal) ) 

#define IMediaPlayer2_put_EnableFullScreenControls(This,bVal)	\
    ( (This)->lpVtbl -> put_EnableFullScreenControls(This,bVal) ) 

#define IMediaPlayer2_get_ActiveMovie(This,ppdispatch)	\
    ( (This)->lpVtbl -> get_ActiveMovie(This,ppdispatch) ) 

#define IMediaPlayer2_get_NSPlay(This,ppdispatch)	\
    ( (This)->lpVtbl -> get_NSPlay(This,ppdispatch) ) 

#define IMediaPlayer2_get_WindowlessVideo(This,pbool)	\
    ( (This)->lpVtbl -> get_WindowlessVideo(This,pbool) ) 

#define IMediaPlayer2_put_WindowlessVideo(This,vbool)	\
    ( (This)->lpVtbl -> put_WindowlessVideo(This,vbool) ) 

#define IMediaPlayer2_Play(This)	\
    ( (This)->lpVtbl -> Play(This) ) 

#define IMediaPlayer2_Stop(This)	\
    ( (This)->lpVtbl -> Stop(This) ) 

#define IMediaPlayer2_Pause(This)	\
    ( (This)->lpVtbl -> Pause(This) ) 

#define IMediaPlayer2_GetMarkerTime(This,MarkerNum,pMarkerTime)	\
    ( (This)->lpVtbl -> GetMarkerTime(This,MarkerNum,pMarkerTime) ) 

#define IMediaPlayer2_GetMarkerName(This,MarkerNum,pbstrMarkerName)	\
    ( (This)->lpVtbl -> GetMarkerName(This,MarkerNum,pbstrMarkerName) ) 

#define IMediaPlayer2_AboutBox(This)	\
    ( (This)->lpVtbl -> AboutBox(This) ) 

#define IMediaPlayer2_GetCodecInstalled(This,CodecNum,pCodecInstalled)	\
    ( (This)->lpVtbl -> GetCodecInstalled(This,CodecNum,pCodecInstalled) ) 

#define IMediaPlayer2_GetCodecDescription(This,CodecNum,pbstrCodecDescription)	\
    ( (This)->lpVtbl -> GetCodecDescription(This,CodecNum,pbstrCodecDescription) ) 

#define IMediaPlayer2_GetCodecURL(This,CodecNum,pbstrCodecURL)	\
    ( (This)->lpVtbl -> GetCodecURL(This,CodecNum,pbstrCodecURL) ) 

#define IMediaPlayer2_GetMoreInfoURL(This,MoreInfoType,pbstrMoreInfoURL)	\
    ( (This)->lpVtbl -> GetMoreInfoURL(This,MoreInfoType,pbstrMoreInfoURL) ) 

#define IMediaPlayer2_GetMediaInfoString(This,MediaInfoType,pbstrMediaInfo)	\
    ( (This)->lpVtbl -> GetMediaInfoString(This,MediaInfoType,pbstrMediaInfo) ) 

#define IMediaPlayer2_Cancel(This)	\
    ( (This)->lpVtbl -> Cancel(This) ) 

#define IMediaPlayer2_Open(This,bstrFileName)	\
    ( (This)->lpVtbl -> Open(This,bstrFileName) ) 

#define IMediaPlayer2_IsSoundCardEnabled(This,pbSoundCard)	\
    ( (This)->lpVtbl -> IsSoundCardEnabled(This,pbSoundCard) ) 

#define IMediaPlayer2_Next(This)	\
    ( (This)->lpVtbl -> Next(This) ) 

#define IMediaPlayer2_Previous(This)	\
    ( (This)->lpVtbl -> Previous(This) ) 

#define IMediaPlayer2_StreamSelect(This,StreamNum)	\
    ( (This)->lpVtbl -> StreamSelect(This,StreamNum) ) 

#define IMediaPlayer2_FastForward(This)	\
    ( (This)->lpVtbl -> FastForward(This) ) 

#define IMediaPlayer2_FastReverse(This)	\
    ( (This)->lpVtbl -> FastReverse(This) ) 

#define IMediaPlayer2_GetStreamName(This,StreamNum,pbstrStreamName)	\
    ( (This)->lpVtbl -> GetStreamName(This,StreamNum,pbstrStreamName) ) 

#define IMediaPlayer2_GetStreamGroup(This,StreamNum,pStreamGroup)	\
    ( (This)->lpVtbl -> GetStreamGroup(This,StreamNum,pStreamGroup) ) 

#define IMediaPlayer2_GetStreamSelected(This,StreamNum,pStreamSelected)	\
    ( (This)->lpVtbl -> GetStreamSelected(This,StreamNum,pStreamSelected) ) 


#define IMediaPlayer2_get_DVD(This,ppdispatch)	\
    ( (This)->lpVtbl -> get_DVD(This,ppdispatch) ) 

#define IMediaPlayer2_GetMediaParameter(This,EntryNum,bstrParameterName,pbstrParameterValue)	\
    ( (This)->lpVtbl -> GetMediaParameter(This,EntryNum,bstrParameterName,pbstrParameterValue) ) 

#define IMediaPlayer2_GetMediaParameterName(This,EntryNum,Index,pbstrParameterName)	\
    ( (This)->lpVtbl -> GetMediaParameterName(This,EntryNum,Index,pbstrParameterName) ) 

#define IMediaPlayer2_get_EntryCount(This,pNumberEntries)	\
    ( (This)->lpVtbl -> get_EntryCount(This,pNumberEntries) ) 

#define IMediaPlayer2_GetCurrentEntry(This,pEntryNumber)	\
    ( (This)->lpVtbl -> GetCurrentEntry(This,pEntryNumber) ) 

#define IMediaPlayer2_SetCurrentEntry(This,EntryNumber)	\
    ( (This)->lpVtbl -> SetCurrentEntry(This,EntryNumber) ) 

#define IMediaPlayer2_ShowDialog(This,mpDialogIndex)	\
    ( (This)->lpVtbl -> ShowDialog(This,mpDialogIndex) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id][propget] */ HRESULT STDMETHODCALLTYPE IMediaPlayer2_get_DVD_Proxy( 
    IMediaPlayer2 * This,
    /* [retval][out] */ IMediaPlayerDvd **ppdispatch);


void __RPC_STUB IMediaPlayer2_get_DVD_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer2_GetMediaParameter_Proxy( 
    IMediaPlayer2 * This,
    /* [in] */ long EntryNum,
    /* [in] */ BSTR bstrParameterName,
    /* [retval][out] */ BSTR *pbstrParameterValue);


void __RPC_STUB IMediaPlayer2_GetMediaParameter_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer2_GetMediaParameterName_Proxy( 
    IMediaPlayer2 * This,
    /* [in] */ long EntryNum,
    /* [in] */ long Index,
    /* [retval][out] */ BSTR *pbstrParameterName);


void __RPC_STUB IMediaPlayer2_GetMediaParameterName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer2_get_EntryCount_Proxy( 
    IMediaPlayer2 * This,
    /* [retval][out] */ long *pNumberEntries);


void __RPC_STUB IMediaPlayer2_get_EntryCount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer2_GetCurrentEntry_Proxy( 
    IMediaPlayer2 * This,
    /* [retval][out] */ long *pEntryNumber);


void __RPC_STUB IMediaPlayer2_GetCurrentEntry_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer2_SetCurrentEntry_Proxy( 
    IMediaPlayer2 * This,
    /* [in] */ long EntryNumber);


void __RPC_STUB IMediaPlayer2_SetCurrentEntry_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMediaPlayer2_ShowDialog_Proxy( 
    IMediaPlayer2 * This,
    /* [in] */ MPShowDialogConstants mpDialogIndex);


void __RPC_STUB IMediaPlayer2_ShowDialog_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IMediaPlayer2_INTERFACE_DEFINED__ */


/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


