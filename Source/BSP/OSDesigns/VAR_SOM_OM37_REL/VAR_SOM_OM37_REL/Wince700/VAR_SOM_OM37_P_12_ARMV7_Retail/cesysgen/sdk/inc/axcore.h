

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0543 */
/* Compiler settings for axcore.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __axcore_h__
#define __axcore_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IPin_FWD_DEFINED__
#define __IPin_FWD_DEFINED__
typedef interface IPin IPin;
#endif 	/* __IPin_FWD_DEFINED__ */


#ifndef __IEnumPins_FWD_DEFINED__
#define __IEnumPins_FWD_DEFINED__
typedef interface IEnumPins IEnumPins;
#endif 	/* __IEnumPins_FWD_DEFINED__ */


#ifndef __IEnumMediaTypes_FWD_DEFINED__
#define __IEnumMediaTypes_FWD_DEFINED__
typedef interface IEnumMediaTypes IEnumMediaTypes;
#endif 	/* __IEnumMediaTypes_FWD_DEFINED__ */


#ifndef __IFilterGraph_FWD_DEFINED__
#define __IFilterGraph_FWD_DEFINED__
typedef interface IFilterGraph IFilterGraph;
#endif 	/* __IFilterGraph_FWD_DEFINED__ */


#ifndef __IEnumFilters_FWD_DEFINED__
#define __IEnumFilters_FWD_DEFINED__
typedef interface IEnumFilters IEnumFilters;
#endif 	/* __IEnumFilters_FWD_DEFINED__ */


#ifndef __IEnumFilterInterfaces_FWD_DEFINED__
#define __IEnumFilterInterfaces_FWD_DEFINED__
typedef interface IEnumFilterInterfaces IEnumFilterInterfaces;
#endif 	/* __IEnumFilterInterfaces_FWD_DEFINED__ */


#ifndef __IMediaFilter_FWD_DEFINED__
#define __IMediaFilter_FWD_DEFINED__
typedef interface IMediaFilter IMediaFilter;
#endif 	/* __IMediaFilter_FWD_DEFINED__ */


#ifndef __IBaseFilter_FWD_DEFINED__
#define __IBaseFilter_FWD_DEFINED__
typedef interface IBaseFilter IBaseFilter;
#endif 	/* __IBaseFilter_FWD_DEFINED__ */


#ifndef __IReferenceClock_FWD_DEFINED__
#define __IReferenceClock_FWD_DEFINED__
typedef interface IReferenceClock IReferenceClock;
#endif 	/* __IReferenceClock_FWD_DEFINED__ */


#ifndef __IReferenceClock2_FWD_DEFINED__
#define __IReferenceClock2_FWD_DEFINED__
typedef interface IReferenceClock2 IReferenceClock2;
#endif 	/* __IReferenceClock2_FWD_DEFINED__ */


#ifndef __IMediaSample_FWD_DEFINED__
#define __IMediaSample_FWD_DEFINED__
typedef interface IMediaSample IMediaSample;
#endif 	/* __IMediaSample_FWD_DEFINED__ */


#ifndef __IMediaSample2_FWD_DEFINED__
#define __IMediaSample2_FWD_DEFINED__
typedef interface IMediaSample2 IMediaSample2;
#endif 	/* __IMediaSample2_FWD_DEFINED__ */


#ifndef __IMemAllocator_FWD_DEFINED__
#define __IMemAllocator_FWD_DEFINED__
typedef interface IMemAllocator IMemAllocator;
#endif 	/* __IMemAllocator_FWD_DEFINED__ */


#ifndef __IMemAllocator2_FWD_DEFINED__
#define __IMemAllocator2_FWD_DEFINED__
typedef interface IMemAllocator2 IMemAllocator2;
#endif 	/* __IMemAllocator2_FWD_DEFINED__ */


#ifndef __IMemInputPin_FWD_DEFINED__
#define __IMemInputPin_FWD_DEFINED__
typedef interface IMemInputPin IMemInputPin;
#endif 	/* __IMemInputPin_FWD_DEFINED__ */


#ifndef __IAMovieSetup_FWD_DEFINED__
#define __IAMovieSetup_FWD_DEFINED__
typedef interface IAMovieSetup IAMovieSetup;
#endif 	/* __IAMovieSetup_FWD_DEFINED__ */


#ifndef __IMediaSeeking_FWD_DEFINED__
#define __IMediaSeeking_FWD_DEFINED__
typedef interface IMediaSeeking IMediaSeeking;
#endif 	/* __IMediaSeeking_FWD_DEFINED__ */


#ifndef __IMediaSeeking2_FWD_DEFINED__
#define __IMediaSeeking2_FWD_DEFINED__
typedef interface IMediaSeeking2 IMediaSeeking2;
#endif 	/* __IMediaSeeking2_FWD_DEFINED__ */


#ifndef __IAudioRenderer_FWD_DEFINED__
#define __IAudioRenderer_FWD_DEFINED__
typedef interface IAudioRenderer IAudioRenderer;
#endif 	/* __IAudioRenderer_FWD_DEFINED__ */


#ifndef __IAudioRendererWaveOut_FWD_DEFINED__
#define __IAudioRendererWaveOut_FWD_DEFINED__
typedef interface IAudioRendererWaveOut IAudioRendererWaveOut;
#endif 	/* __IAudioRendererWaveOut_FWD_DEFINED__ */


#ifndef __IFilterGraphCache_FWD_DEFINED__
#define __IFilterGraphCache_FWD_DEFINED__
typedef interface IFilterGraphCache IFilterGraphCache;
#endif 	/* __IFilterGraphCache_FWD_DEFINED__ */


/* header files for imported files */
#include "unknwn.h"
#include "objidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


/* interface __MIDL_itf_axcore_0000_0000 */
/* [local] */ 

#define CHARS_IN_GUID     39
typedef struct _AMMediaType
    {
    GUID majortype;
    GUID subtype;
    BOOL bFixedSizeSamples;
    BOOL bTemporalCompression;
    ULONG lSampleSize;
    GUID formattype;
    IUnknown *pUnk;
    ULONG cbFormat;
    BYTE *pbFormat;
    } 	AM_MEDIA_TYPE;

typedef 
enum _PinDirection
    {	PINDIR_INPUT	= 0,
	PINDIR_OUTPUT	= ( PINDIR_INPUT + 1 ) 
    } 	PIN_DIRECTION;

#define MAX_PIN_NAME     128
#define MAX_FILTER_NAME  128
typedef LONGLONG REFERENCE_TIME;

typedef double REFTIME;

typedef DWORD_PTR HSEMAPHORE;

typedef DWORD_PTR HEVENT;

typedef struct _AllocatorProperties
    {
    long cBuffers;
    long cbBuffer;
    long cbAlign;
    long cbPrefix;
    } 	ALLOCATOR_PROPERTIES;















extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0000_v0_0_s_ifspec;

#ifndef __IPin_INTERFACE_DEFINED__
#define __IPin_INTERFACE_DEFINED__

/* interface IPin */
/* [unique][uuid][object] */ 

typedef struct _PinInfo
    {
    IBaseFilter *pFilter;
    PIN_DIRECTION dir;
    WCHAR achName[ 128 ];
    } 	PIN_INFO;


EXTERN_C const IID IID_IPin;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("56a86891-0ad4-11ce-b03a-0020af0ba770")
    IPin : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE Connect( 
            /* [in] */ IPin *pReceivePin,
            /* [in] */ const AM_MEDIA_TYPE *pmt) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE ReceiveConnection( 
            /* [in] */ IPin *pConnector,
            /* [in] */ const AM_MEDIA_TYPE *pmt) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Disconnect( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE ConnectedTo( 
            /* [out] */ IPin **pPin) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE ConnectionMediaType( 
            /* [out] */ AM_MEDIA_TYPE *pmt) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE QueryPinInfo( 
            /* [out] */ PIN_INFO *pInfo) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE QueryDirection( 
            /* [out] */ PIN_DIRECTION *pPinDir) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE QueryId( 
            /* [out] */ LPWSTR *Id) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE QueryAccept( 
            /* [in] */ const AM_MEDIA_TYPE *pmt) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE EnumMediaTypes( 
            /* [out] */ IEnumMediaTypes **ppEnum) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE QueryInternalConnections( 
            /* [out] */ IPin **apPin,
            /* [out][in] */ ULONG *nPin) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE EndOfStream( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE BeginFlush( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE EndFlush( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE NewSegment( 
            /* [in] */ REFERENCE_TIME tStart,
            /* [in] */ REFERENCE_TIME tStop,
            /* [in] */ double dRate) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IPinVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IPin * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IPin * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IPin * This);
        
        HRESULT ( STDMETHODCALLTYPE *Connect )( 
            IPin * This,
            /* [in] */ IPin *pReceivePin,
            /* [in] */ const AM_MEDIA_TYPE *pmt);
        
        HRESULT ( STDMETHODCALLTYPE *ReceiveConnection )( 
            IPin * This,
            /* [in] */ IPin *pConnector,
            /* [in] */ const AM_MEDIA_TYPE *pmt);
        
        HRESULT ( STDMETHODCALLTYPE *Disconnect )( 
            IPin * This);
        
        HRESULT ( STDMETHODCALLTYPE *ConnectedTo )( 
            IPin * This,
            /* [out] */ IPin **pPin);
        
        HRESULT ( STDMETHODCALLTYPE *ConnectionMediaType )( 
            IPin * This,
            /* [out] */ AM_MEDIA_TYPE *pmt);
        
        HRESULT ( STDMETHODCALLTYPE *QueryPinInfo )( 
            IPin * This,
            /* [out] */ PIN_INFO *pInfo);
        
        HRESULT ( STDMETHODCALLTYPE *QueryDirection )( 
            IPin * This,
            /* [out] */ PIN_DIRECTION *pPinDir);
        
        HRESULT ( STDMETHODCALLTYPE *QueryId )( 
            IPin * This,
            /* [out] */ LPWSTR *Id);
        
        HRESULT ( STDMETHODCALLTYPE *QueryAccept )( 
            IPin * This,
            /* [in] */ const AM_MEDIA_TYPE *pmt);
        
        HRESULT ( STDMETHODCALLTYPE *EnumMediaTypes )( 
            IPin * This,
            /* [out] */ IEnumMediaTypes **ppEnum);
        
        HRESULT ( STDMETHODCALLTYPE *QueryInternalConnections )( 
            IPin * This,
            /* [out] */ IPin **apPin,
            /* [out][in] */ ULONG *nPin);
        
        HRESULT ( STDMETHODCALLTYPE *EndOfStream )( 
            IPin * This);
        
        HRESULT ( STDMETHODCALLTYPE *BeginFlush )( 
            IPin * This);
        
        HRESULT ( STDMETHODCALLTYPE *EndFlush )( 
            IPin * This);
        
        HRESULT ( STDMETHODCALLTYPE *NewSegment )( 
            IPin * This,
            /* [in] */ REFERENCE_TIME tStart,
            /* [in] */ REFERENCE_TIME tStop,
            /* [in] */ double dRate);
        
        END_INTERFACE
    } IPinVtbl;

    interface IPin
    {
        CONST_VTBL struct IPinVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IPin_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IPin_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IPin_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IPin_Connect(This,pReceivePin,pmt)	\
    ( (This)->lpVtbl -> Connect(This,pReceivePin,pmt) ) 

#define IPin_ReceiveConnection(This,pConnector,pmt)	\
    ( (This)->lpVtbl -> ReceiveConnection(This,pConnector,pmt) ) 

#define IPin_Disconnect(This)	\
    ( (This)->lpVtbl -> Disconnect(This) ) 

#define IPin_ConnectedTo(This,pPin)	\
    ( (This)->lpVtbl -> ConnectedTo(This,pPin) ) 

#define IPin_ConnectionMediaType(This,pmt)	\
    ( (This)->lpVtbl -> ConnectionMediaType(This,pmt) ) 

#define IPin_QueryPinInfo(This,pInfo)	\
    ( (This)->lpVtbl -> QueryPinInfo(This,pInfo) ) 

#define IPin_QueryDirection(This,pPinDir)	\
    ( (This)->lpVtbl -> QueryDirection(This,pPinDir) ) 

#define IPin_QueryId(This,Id)	\
    ( (This)->lpVtbl -> QueryId(This,Id) ) 

#define IPin_QueryAccept(This,pmt)	\
    ( (This)->lpVtbl -> QueryAccept(This,pmt) ) 

#define IPin_EnumMediaTypes(This,ppEnum)	\
    ( (This)->lpVtbl -> EnumMediaTypes(This,ppEnum) ) 

#define IPin_QueryInternalConnections(This,apPin,nPin)	\
    ( (This)->lpVtbl -> QueryInternalConnections(This,apPin,nPin) ) 

#define IPin_EndOfStream(This)	\
    ( (This)->lpVtbl -> EndOfStream(This) ) 

#define IPin_BeginFlush(This)	\
    ( (This)->lpVtbl -> BeginFlush(This) ) 

#define IPin_EndFlush(This)	\
    ( (This)->lpVtbl -> EndFlush(This) ) 

#define IPin_NewSegment(This,tStart,tStop,dRate)	\
    ( (This)->lpVtbl -> NewSegment(This,tStart,tStop,dRate) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IPin_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_axcore_0000_0001 */
/* [local] */ 

typedef IPin *PPIN;



extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0001_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0001_v0_0_s_ifspec;

#ifndef __IEnumPins_INTERFACE_DEFINED__
#define __IEnumPins_INTERFACE_DEFINED__

/* interface IEnumPins */
/* [unique][uuid][object] */ 


EXTERN_C const IID IID_IEnumPins;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("56a86892-0ad4-11ce-b03a-0020af0ba770")
    IEnumPins : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cPins,
            /* [size_is][out] */ IPin **ppPins,
            /* [out] */ ULONG *pcFetched) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cPins) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Reset( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Clone( 
            /* [out] */ IEnumPins **ppEnum) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEnumPinsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IEnumPins * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IEnumPins * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IEnumPins * This);
        
        HRESULT ( STDMETHODCALLTYPE *Next )( 
            IEnumPins * This,
            /* [in] */ ULONG cPins,
            /* [size_is][out] */ IPin **ppPins,
            /* [out] */ ULONG *pcFetched);
        
        HRESULT ( STDMETHODCALLTYPE *Skip )( 
            IEnumPins * This,
            /* [in] */ ULONG cPins);
        
        HRESULT ( STDMETHODCALLTYPE *Reset )( 
            IEnumPins * This);
        
        HRESULT ( STDMETHODCALLTYPE *Clone )( 
            IEnumPins * This,
            /* [out] */ IEnumPins **ppEnum);
        
        END_INTERFACE
    } IEnumPinsVtbl;

    interface IEnumPins
    {
        CONST_VTBL struct IEnumPinsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEnumPins_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IEnumPins_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IEnumPins_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IEnumPins_Next(This,cPins,ppPins,pcFetched)	\
    ( (This)->lpVtbl -> Next(This,cPins,ppPins,pcFetched) ) 

#define IEnumPins_Skip(This,cPins)	\
    ( (This)->lpVtbl -> Skip(This,cPins) ) 

#define IEnumPins_Reset(This)	\
    ( (This)->lpVtbl -> Reset(This) ) 

#define IEnumPins_Clone(This,ppEnum)	\
    ( (This)->lpVtbl -> Clone(This,ppEnum) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IEnumPins_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_axcore_0000_0002 */
/* [local] */ 

typedef IEnumPins *PENUMPINS;



extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0002_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0002_v0_0_s_ifspec;

#ifndef __IEnumMediaTypes_INTERFACE_DEFINED__
#define __IEnumMediaTypes_INTERFACE_DEFINED__

/* interface IEnumMediaTypes */
/* [unique][uuid][object] */ 


EXTERN_C const IID IID_IEnumMediaTypes;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("89c31040-846b-11ce-97d3-00aa0055595a")
    IEnumMediaTypes : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cMediaTypes,
            /* [size_is][out] */ AM_MEDIA_TYPE **ppMediaTypes,
            /* [out] */ ULONG *pcFetched) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cMediaTypes) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Reset( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Clone( 
            /* [out] */ IEnumMediaTypes **ppEnum) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEnumMediaTypesVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IEnumMediaTypes * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IEnumMediaTypes * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IEnumMediaTypes * This);
        
        HRESULT ( STDMETHODCALLTYPE *Next )( 
            IEnumMediaTypes * This,
            /* [in] */ ULONG cMediaTypes,
            /* [size_is][out] */ AM_MEDIA_TYPE **ppMediaTypes,
            /* [out] */ ULONG *pcFetched);
        
        HRESULT ( STDMETHODCALLTYPE *Skip )( 
            IEnumMediaTypes * This,
            /* [in] */ ULONG cMediaTypes);
        
        HRESULT ( STDMETHODCALLTYPE *Reset )( 
            IEnumMediaTypes * This);
        
        HRESULT ( STDMETHODCALLTYPE *Clone )( 
            IEnumMediaTypes * This,
            /* [out] */ IEnumMediaTypes **ppEnum);
        
        END_INTERFACE
    } IEnumMediaTypesVtbl;

    interface IEnumMediaTypes
    {
        CONST_VTBL struct IEnumMediaTypesVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEnumMediaTypes_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IEnumMediaTypes_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IEnumMediaTypes_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IEnumMediaTypes_Next(This,cMediaTypes,ppMediaTypes,pcFetched)	\
    ( (This)->lpVtbl -> Next(This,cMediaTypes,ppMediaTypes,pcFetched) ) 

#define IEnumMediaTypes_Skip(This,cMediaTypes)	\
    ( (This)->lpVtbl -> Skip(This,cMediaTypes) ) 

#define IEnumMediaTypes_Reset(This)	\
    ( (This)->lpVtbl -> Reset(This) ) 

#define IEnumMediaTypes_Clone(This,ppEnum)	\
    ( (This)->lpVtbl -> Clone(This,ppEnum) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IEnumMediaTypes_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_axcore_0000_0003 */
/* [local] */ 

typedef IEnumMediaTypes *PENUMMEDIATYPES;



extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0003_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0003_v0_0_s_ifspec;

#ifndef __IFilterGraph_INTERFACE_DEFINED__
#define __IFilterGraph_INTERFACE_DEFINED__

/* interface IFilterGraph */
/* [unique][uuid][object] */ 


EXTERN_C const IID IID_IFilterGraph;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("56a8689f-0ad4-11ce-b03a-0020af0ba770")
    IFilterGraph : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE AddFilter( 
            /* [in] */ IBaseFilter *pFilter,
            /* [string][in] */ LPCWSTR pName) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE RemoveFilter( 
            /* [in] */ IBaseFilter *pFilter) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE EnumFilters( 
            /* [out] */ IEnumFilters **ppEnum) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE FindFilterByName( 
            /* [string][in] */ LPCWSTR pName,
            /* [out] */ IBaseFilter **ppFilter) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE ConnectDirect( 
            /* [in] */ IPin *ppinOut,
            /* [in] */ IPin *ppinIn,
            /* [in] */ const AM_MEDIA_TYPE *pmt) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Reconnect( 
            /* [in] */ IPin *ppin) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Disconnect( 
            /* [in] */ IPin *ppin) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetDefaultSyncSource( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IFilterGraphVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IFilterGraph * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IFilterGraph * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IFilterGraph * This);
        
        HRESULT ( STDMETHODCALLTYPE *AddFilter )( 
            IFilterGraph * This,
            /* [in] */ IBaseFilter *pFilter,
            /* [string][in] */ LPCWSTR pName);
        
        HRESULT ( STDMETHODCALLTYPE *RemoveFilter )( 
            IFilterGraph * This,
            /* [in] */ IBaseFilter *pFilter);
        
        HRESULT ( STDMETHODCALLTYPE *EnumFilters )( 
            IFilterGraph * This,
            /* [out] */ IEnumFilters **ppEnum);
        
        HRESULT ( STDMETHODCALLTYPE *FindFilterByName )( 
            IFilterGraph * This,
            /* [string][in] */ LPCWSTR pName,
            /* [out] */ IBaseFilter **ppFilter);
        
        HRESULT ( STDMETHODCALLTYPE *ConnectDirect )( 
            IFilterGraph * This,
            /* [in] */ IPin *ppinOut,
            /* [in] */ IPin *ppinIn,
            /* [in] */ const AM_MEDIA_TYPE *pmt);
        
        HRESULT ( STDMETHODCALLTYPE *Reconnect )( 
            IFilterGraph * This,
            /* [in] */ IPin *ppin);
        
        HRESULT ( STDMETHODCALLTYPE *Disconnect )( 
            IFilterGraph * This,
            /* [in] */ IPin *ppin);
        
        HRESULT ( STDMETHODCALLTYPE *SetDefaultSyncSource )( 
            IFilterGraph * This);
        
        END_INTERFACE
    } IFilterGraphVtbl;

    interface IFilterGraph
    {
        CONST_VTBL struct IFilterGraphVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IFilterGraph_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IFilterGraph_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IFilterGraph_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IFilterGraph_AddFilter(This,pFilter,pName)	\
    ( (This)->lpVtbl -> AddFilter(This,pFilter,pName) ) 

#define IFilterGraph_RemoveFilter(This,pFilter)	\
    ( (This)->lpVtbl -> RemoveFilter(This,pFilter) ) 

#define IFilterGraph_EnumFilters(This,ppEnum)	\
    ( (This)->lpVtbl -> EnumFilters(This,ppEnum) ) 

#define IFilterGraph_FindFilterByName(This,pName,ppFilter)	\
    ( (This)->lpVtbl -> FindFilterByName(This,pName,ppFilter) ) 

#define IFilterGraph_ConnectDirect(This,ppinOut,ppinIn,pmt)	\
    ( (This)->lpVtbl -> ConnectDirect(This,ppinOut,ppinIn,pmt) ) 

#define IFilterGraph_Reconnect(This,ppin)	\
    ( (This)->lpVtbl -> Reconnect(This,ppin) ) 

#define IFilterGraph_Disconnect(This,ppin)	\
    ( (This)->lpVtbl -> Disconnect(This,ppin) ) 

#define IFilterGraph_SetDefaultSyncSource(This)	\
    ( (This)->lpVtbl -> SetDefaultSyncSource(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IFilterGraph_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_axcore_0000_0004 */
/* [local] */ 

typedef IFilterGraph *PFILTERGRAPH;



extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0004_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0004_v0_0_s_ifspec;

#ifndef __IEnumFilters_INTERFACE_DEFINED__
#define __IEnumFilters_INTERFACE_DEFINED__

/* interface IEnumFilters */
/* [unique][uuid][object] */ 


EXTERN_C const IID IID_IEnumFilters;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("56a86893-0ad4-11ce-b03a-0020af0ba770")
    IEnumFilters : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cFilters,
            /* [out] */ IBaseFilter **ppFilter,
            /* [out] */ ULONG *pcFetched) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cFilters) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Reset( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Clone( 
            /* [out] */ IEnumFilters **ppEnum) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEnumFiltersVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IEnumFilters * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IEnumFilters * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IEnumFilters * This);
        
        HRESULT ( STDMETHODCALLTYPE *Next )( 
            IEnumFilters * This,
            /* [in] */ ULONG cFilters,
            /* [out] */ IBaseFilter **ppFilter,
            /* [out] */ ULONG *pcFetched);
        
        HRESULT ( STDMETHODCALLTYPE *Skip )( 
            IEnumFilters * This,
            /* [in] */ ULONG cFilters);
        
        HRESULT ( STDMETHODCALLTYPE *Reset )( 
            IEnumFilters * This);
        
        HRESULT ( STDMETHODCALLTYPE *Clone )( 
            IEnumFilters * This,
            /* [out] */ IEnumFilters **ppEnum);
        
        END_INTERFACE
    } IEnumFiltersVtbl;

    interface IEnumFilters
    {
        CONST_VTBL struct IEnumFiltersVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEnumFilters_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IEnumFilters_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IEnumFilters_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IEnumFilters_Next(This,cFilters,ppFilter,pcFetched)	\
    ( (This)->lpVtbl -> Next(This,cFilters,ppFilter,pcFetched) ) 

#define IEnumFilters_Skip(This,cFilters)	\
    ( (This)->lpVtbl -> Skip(This,cFilters) ) 

#define IEnumFilters_Reset(This)	\
    ( (This)->lpVtbl -> Reset(This) ) 

#define IEnumFilters_Clone(This,ppEnum)	\
    ( (This)->lpVtbl -> Clone(This,ppEnum) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IEnumFilters_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_axcore_0000_0005 */
/* [local] */ 

typedef IEnumFilters *PENUMFILTERS;



extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0005_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0005_v0_0_s_ifspec;

#ifndef __IEnumFilterInterfaces_INTERFACE_DEFINED__
#define __IEnumFilterInterfaces_INTERFACE_DEFINED__

/* interface IEnumFilterInterfaces */
/* [unique][helpstring][uuid][local][object][local] */ 


EXTERN_C const IID IID_IEnumFilterInterfaces;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("F845653C-2777-4d84-A50D-26D3397D5C75")
    IEnumFilterInterfaces : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE Next( 
            /* [out] */ IUnknown **ppUnknown,
            /* [in] */ UINT32 cchFilterName,
            /* [size_is][string][out][in] */ wchar_t *pwszFilterName) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Reset( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEnumFilterInterfacesVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IEnumFilterInterfaces * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IEnumFilterInterfaces * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IEnumFilterInterfaces * This);
        
        HRESULT ( STDMETHODCALLTYPE *Next )( 
            IEnumFilterInterfaces * This,
            /* [out] */ IUnknown **ppUnknown,
            /* [in] */ UINT32 cchFilterName,
            /* [size_is][string][out][in] */ wchar_t *pwszFilterName);
        
        HRESULT ( STDMETHODCALLTYPE *Reset )( 
            IEnumFilterInterfaces * This);
        
        END_INTERFACE
    } IEnumFilterInterfacesVtbl;

    interface IEnumFilterInterfaces
    {
        CONST_VTBL struct IEnumFilterInterfacesVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEnumFilterInterfaces_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IEnumFilterInterfaces_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IEnumFilterInterfaces_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IEnumFilterInterfaces_Next(This,ppUnknown,cchFilterName,pwszFilterName)	\
    ( (This)->lpVtbl -> Next(This,ppUnknown,cchFilterName,pwszFilterName) ) 

#define IEnumFilterInterfaces_Reset(This)	\
    ( (This)->lpVtbl -> Reset(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IEnumFilterInterfaces_INTERFACE_DEFINED__ */


#ifndef __IMediaFilter_INTERFACE_DEFINED__
#define __IMediaFilter_INTERFACE_DEFINED__

/* interface IMediaFilter */
/* [unique][uuid][object] */ 

typedef 
enum _FilterState
    {	State_Stopped	= 0,
	State_Paused	= ( State_Stopped + 1 ) ,
	State_Running	= ( State_Paused + 1 ) 
    } 	FILTER_STATE;


EXTERN_C const IID IID_IMediaFilter;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("56a86899-0ad4-11ce-b03a-0020af0ba770")
    IMediaFilter : public IPersist
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE Stop( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Pause( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Run( 
            REFERENCE_TIME tStart) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetState( 
            /* [in] */ DWORD dwMilliSecsTimeout,
            /* [out] */ FILTER_STATE *State) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetSyncSource( 
            /* [in] */ IReferenceClock *pClock) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetSyncSource( 
            /* [out] */ IReferenceClock **pClock) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMediaFilterVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMediaFilter * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMediaFilter * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMediaFilter * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetClassID )( 
            IMediaFilter * This,
            /* [out] */ CLSID *pClassID);
        
        HRESULT ( STDMETHODCALLTYPE *Stop )( 
            IMediaFilter * This);
        
        HRESULT ( STDMETHODCALLTYPE *Pause )( 
            IMediaFilter * This);
        
        HRESULT ( STDMETHODCALLTYPE *Run )( 
            IMediaFilter * This,
            REFERENCE_TIME tStart);
        
        HRESULT ( STDMETHODCALLTYPE *GetState )( 
            IMediaFilter * This,
            /* [in] */ DWORD dwMilliSecsTimeout,
            /* [out] */ FILTER_STATE *State);
        
        HRESULT ( STDMETHODCALLTYPE *SetSyncSource )( 
            IMediaFilter * This,
            /* [in] */ IReferenceClock *pClock);
        
        HRESULT ( STDMETHODCALLTYPE *GetSyncSource )( 
            IMediaFilter * This,
            /* [out] */ IReferenceClock **pClock);
        
        END_INTERFACE
    } IMediaFilterVtbl;

    interface IMediaFilter
    {
        CONST_VTBL struct IMediaFilterVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMediaFilter_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMediaFilter_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMediaFilter_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IMediaFilter_GetClassID(This,pClassID)	\
    ( (This)->lpVtbl -> GetClassID(This,pClassID) ) 


#define IMediaFilter_Stop(This)	\
    ( (This)->lpVtbl -> Stop(This) ) 

#define IMediaFilter_Pause(This)	\
    ( (This)->lpVtbl -> Pause(This) ) 

#define IMediaFilter_Run(This,tStart)	\
    ( (This)->lpVtbl -> Run(This,tStart) ) 

#define IMediaFilter_GetState(This,dwMilliSecsTimeout,State)	\
    ( (This)->lpVtbl -> GetState(This,dwMilliSecsTimeout,State) ) 

#define IMediaFilter_SetSyncSource(This,pClock)	\
    ( (This)->lpVtbl -> SetSyncSource(This,pClock) ) 

#define IMediaFilter_GetSyncSource(This,pClock)	\
    ( (This)->lpVtbl -> GetSyncSource(This,pClock) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMediaFilter_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_axcore_0000_0007 */
/* [local] */ 

typedef IMediaFilter *PMEDIAFILTER;



extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0007_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0007_v0_0_s_ifspec;

#ifndef __IBaseFilter_INTERFACE_DEFINED__
#define __IBaseFilter_INTERFACE_DEFINED__

/* interface IBaseFilter */
/* [unique][uuid][object] */ 

typedef struct _FilterInfo
    {
    WCHAR achName[ 128 ];
    IFilterGraph *pGraph;
    } 	FILTER_INFO;


EXTERN_C const IID IID_IBaseFilter;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("56a86895-0ad4-11ce-b03a-0020af0ba770")
    IBaseFilter : public IMediaFilter
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE EnumPins( 
            /* [out] */ IEnumPins **ppEnum) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE FindPin( 
            /* [string][in] */ LPCWSTR Id,
            /* [out] */ IPin **ppPin) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE QueryFilterInfo( 
            /* [out] */ FILTER_INFO *pInfo) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE JoinFilterGraph( 
            /* [in] */ IFilterGraph *pGraph,
            /* [string][in] */ LPCWSTR pName) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE QueryVendorInfo( 
            /* [string][out] */ LPWSTR *pVendorInfo) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IBaseFilterVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IBaseFilter * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IBaseFilter * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IBaseFilter * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetClassID )( 
            IBaseFilter * This,
            /* [out] */ CLSID *pClassID);
        
        HRESULT ( STDMETHODCALLTYPE *Stop )( 
            IBaseFilter * This);
        
        HRESULT ( STDMETHODCALLTYPE *Pause )( 
            IBaseFilter * This);
        
        HRESULT ( STDMETHODCALLTYPE *Run )( 
            IBaseFilter * This,
            REFERENCE_TIME tStart);
        
        HRESULT ( STDMETHODCALLTYPE *GetState )( 
            IBaseFilter * This,
            /* [in] */ DWORD dwMilliSecsTimeout,
            /* [out] */ FILTER_STATE *State);
        
        HRESULT ( STDMETHODCALLTYPE *SetSyncSource )( 
            IBaseFilter * This,
            /* [in] */ IReferenceClock *pClock);
        
        HRESULT ( STDMETHODCALLTYPE *GetSyncSource )( 
            IBaseFilter * This,
            /* [out] */ IReferenceClock **pClock);
        
        HRESULT ( STDMETHODCALLTYPE *EnumPins )( 
            IBaseFilter * This,
            /* [out] */ IEnumPins **ppEnum);
        
        HRESULT ( STDMETHODCALLTYPE *FindPin )( 
            IBaseFilter * This,
            /* [string][in] */ LPCWSTR Id,
            /* [out] */ IPin **ppPin);
        
        HRESULT ( STDMETHODCALLTYPE *QueryFilterInfo )( 
            IBaseFilter * This,
            /* [out] */ FILTER_INFO *pInfo);
        
        HRESULT ( STDMETHODCALLTYPE *JoinFilterGraph )( 
            IBaseFilter * This,
            /* [in] */ IFilterGraph *pGraph,
            /* [string][in] */ LPCWSTR pName);
        
        HRESULT ( STDMETHODCALLTYPE *QueryVendorInfo )( 
            IBaseFilter * This,
            /* [string][out] */ LPWSTR *pVendorInfo);
        
        END_INTERFACE
    } IBaseFilterVtbl;

    interface IBaseFilter
    {
        CONST_VTBL struct IBaseFilterVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IBaseFilter_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IBaseFilter_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IBaseFilter_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IBaseFilter_GetClassID(This,pClassID)	\
    ( (This)->lpVtbl -> GetClassID(This,pClassID) ) 


#define IBaseFilter_Stop(This)	\
    ( (This)->lpVtbl -> Stop(This) ) 

#define IBaseFilter_Pause(This)	\
    ( (This)->lpVtbl -> Pause(This) ) 

#define IBaseFilter_Run(This,tStart)	\
    ( (This)->lpVtbl -> Run(This,tStart) ) 

#define IBaseFilter_GetState(This,dwMilliSecsTimeout,State)	\
    ( (This)->lpVtbl -> GetState(This,dwMilliSecsTimeout,State) ) 

#define IBaseFilter_SetSyncSource(This,pClock)	\
    ( (This)->lpVtbl -> SetSyncSource(This,pClock) ) 

#define IBaseFilter_GetSyncSource(This,pClock)	\
    ( (This)->lpVtbl -> GetSyncSource(This,pClock) ) 


#define IBaseFilter_EnumPins(This,ppEnum)	\
    ( (This)->lpVtbl -> EnumPins(This,ppEnum) ) 

#define IBaseFilter_FindPin(This,Id,ppPin)	\
    ( (This)->lpVtbl -> FindPin(This,Id,ppPin) ) 

#define IBaseFilter_QueryFilterInfo(This,pInfo)	\
    ( (This)->lpVtbl -> QueryFilterInfo(This,pInfo) ) 

#define IBaseFilter_JoinFilterGraph(This,pGraph,pName)	\
    ( (This)->lpVtbl -> JoinFilterGraph(This,pGraph,pName) ) 

#define IBaseFilter_QueryVendorInfo(This,pVendorInfo)	\
    ( (This)->lpVtbl -> QueryVendorInfo(This,pVendorInfo) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IBaseFilter_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_axcore_0000_0008 */
/* [local] */ 

typedef IBaseFilter *PFILTER;



extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0008_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0008_v0_0_s_ifspec;

#ifndef __IReferenceClock_INTERFACE_DEFINED__
#define __IReferenceClock_INTERFACE_DEFINED__

/* interface IReferenceClock */
/* [unique][uuid][object] */ 


EXTERN_C const IID IID_IReferenceClock;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("56a86897-0ad4-11ce-b03a-0020af0ba770")
    IReferenceClock : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE GetTime( 
            /* [out] */ REFERENCE_TIME *pTime) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE AdviseTime( 
            /* [in] */ REFERENCE_TIME baseTime,
            /* [in] */ REFERENCE_TIME streamTime,
            /* [in] */ HEVENT hEvent,
            /* [out] */ DWORD_PTR *pdwAdviseCookie) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE AdvisePeriodic( 
            /* [in] */ REFERENCE_TIME startTime,
            /* [in] */ REFERENCE_TIME periodTime,
            /* [in] */ HSEMAPHORE hSemaphore,
            /* [out] */ DWORD_PTR *pdwAdviseCookie) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Unadvise( 
            /* [in] */ DWORD_PTR dwAdviseCookie) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IReferenceClockVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IReferenceClock * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IReferenceClock * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IReferenceClock * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTime )( 
            IReferenceClock * This,
            /* [out] */ REFERENCE_TIME *pTime);
        
        HRESULT ( STDMETHODCALLTYPE *AdviseTime )( 
            IReferenceClock * This,
            /* [in] */ REFERENCE_TIME baseTime,
            /* [in] */ REFERENCE_TIME streamTime,
            /* [in] */ HEVENT hEvent,
            /* [out] */ DWORD_PTR *pdwAdviseCookie);
        
        HRESULT ( STDMETHODCALLTYPE *AdvisePeriodic )( 
            IReferenceClock * This,
            /* [in] */ REFERENCE_TIME startTime,
            /* [in] */ REFERENCE_TIME periodTime,
            /* [in] */ HSEMAPHORE hSemaphore,
            /* [out] */ DWORD_PTR *pdwAdviseCookie);
        
        HRESULT ( STDMETHODCALLTYPE *Unadvise )( 
            IReferenceClock * This,
            /* [in] */ DWORD_PTR dwAdviseCookie);
        
        END_INTERFACE
    } IReferenceClockVtbl;

    interface IReferenceClock
    {
        CONST_VTBL struct IReferenceClockVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IReferenceClock_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IReferenceClock_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IReferenceClock_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IReferenceClock_GetTime(This,pTime)	\
    ( (This)->lpVtbl -> GetTime(This,pTime) ) 

#define IReferenceClock_AdviseTime(This,baseTime,streamTime,hEvent,pdwAdviseCookie)	\
    ( (This)->lpVtbl -> AdviseTime(This,baseTime,streamTime,hEvent,pdwAdviseCookie) ) 

#define IReferenceClock_AdvisePeriodic(This,startTime,periodTime,hSemaphore,pdwAdviseCookie)	\
    ( (This)->lpVtbl -> AdvisePeriodic(This,startTime,periodTime,hSemaphore,pdwAdviseCookie) ) 

#define IReferenceClock_Unadvise(This,dwAdviseCookie)	\
    ( (This)->lpVtbl -> Unadvise(This,dwAdviseCookie) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IReferenceClock_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_axcore_0000_0009 */
/* [local] */ 

typedef IReferenceClock *PREFERENCECLOCK;



extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0009_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0009_v0_0_s_ifspec;

#ifndef __IReferenceClock2_INTERFACE_DEFINED__
#define __IReferenceClock2_INTERFACE_DEFINED__

/* interface IReferenceClock2 */
/* [unique][uuid][object] */ 


EXTERN_C const IID IID_IReferenceClock2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("36b73885-c2c8-11cf-8b46-00805f6cef60")
    IReferenceClock2 : public IReferenceClock
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IReferenceClock2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IReferenceClock2 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IReferenceClock2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IReferenceClock2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTime )( 
            IReferenceClock2 * This,
            /* [out] */ REFERENCE_TIME *pTime);
        
        HRESULT ( STDMETHODCALLTYPE *AdviseTime )( 
            IReferenceClock2 * This,
            /* [in] */ REFERENCE_TIME baseTime,
            /* [in] */ REFERENCE_TIME streamTime,
            /* [in] */ HEVENT hEvent,
            /* [out] */ DWORD_PTR *pdwAdviseCookie);
        
        HRESULT ( STDMETHODCALLTYPE *AdvisePeriodic )( 
            IReferenceClock2 * This,
            /* [in] */ REFERENCE_TIME startTime,
            /* [in] */ REFERENCE_TIME periodTime,
            /* [in] */ HSEMAPHORE hSemaphore,
            /* [out] */ DWORD_PTR *pdwAdviseCookie);
        
        HRESULT ( STDMETHODCALLTYPE *Unadvise )( 
            IReferenceClock2 * This,
            /* [in] */ DWORD_PTR dwAdviseCookie);
        
        END_INTERFACE
    } IReferenceClock2Vtbl;

    interface IReferenceClock2
    {
        CONST_VTBL struct IReferenceClock2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IReferenceClock2_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IReferenceClock2_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IReferenceClock2_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IReferenceClock2_GetTime(This,pTime)	\
    ( (This)->lpVtbl -> GetTime(This,pTime) ) 

#define IReferenceClock2_AdviseTime(This,baseTime,streamTime,hEvent,pdwAdviseCookie)	\
    ( (This)->lpVtbl -> AdviseTime(This,baseTime,streamTime,hEvent,pdwAdviseCookie) ) 

#define IReferenceClock2_AdvisePeriodic(This,startTime,periodTime,hSemaphore,pdwAdviseCookie)	\
    ( (This)->lpVtbl -> AdvisePeriodic(This,startTime,periodTime,hSemaphore,pdwAdviseCookie) ) 

#define IReferenceClock2_Unadvise(This,dwAdviseCookie)	\
    ( (This)->lpVtbl -> Unadvise(This,dwAdviseCookie) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IReferenceClock2_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_axcore_0000_0010 */
/* [local] */ 

typedef IReferenceClock2 *PREFERENCECLOCK2;



extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0010_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0010_v0_0_s_ifspec;

#ifndef __IMediaSample_INTERFACE_DEFINED__
#define __IMediaSample_INTERFACE_DEFINED__

/* interface IMediaSample */
/* [unique][uuid][object][local] */ 


EXTERN_C const IID IID_IMediaSample;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("56a8689a-0ad4-11ce-b03a-0020af0ba770")
    IMediaSample : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE GetPointer( 
            /* [out] */ BYTE **ppBuffer) = 0;
        
        virtual long STDMETHODCALLTYPE GetSize( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetTime( 
            /* [out] */ REFERENCE_TIME *pTimeStart,
            /* [out] */ REFERENCE_TIME *pTimeEnd) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetTime( 
            /* [in] */ REFERENCE_TIME *pTimeStart,
            /* [in] */ REFERENCE_TIME *pTimeEnd) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE IsSyncPoint( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetSyncPoint( 
            BOOL bIsSyncPoint) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE IsPreroll( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetPreroll( 
            BOOL bIsPreroll) = 0;
        
        virtual long STDMETHODCALLTYPE GetActualDataLength( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetActualDataLength( 
            long __MIDL__IMediaSample0000) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetMediaType( 
            AM_MEDIA_TYPE **ppMediaType) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetMediaType( 
            AM_MEDIA_TYPE *pMediaType) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE IsDiscontinuity( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetDiscontinuity( 
            BOOL bDiscontinuity) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetMediaTime( 
            /* [out] */ LONGLONG *pTimeStart,
            /* [out] */ LONGLONG *pTimeEnd) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetMediaTime( 
            /* [in] */ LONGLONG *pTimeStart,
            /* [in] */ LONGLONG *pTimeEnd) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMediaSampleVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMediaSample * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMediaSample * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMediaSample * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetPointer )( 
            IMediaSample * This,
            /* [out] */ BYTE **ppBuffer);
        
        long ( STDMETHODCALLTYPE *GetSize )( 
            IMediaSample * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTime )( 
            IMediaSample * This,
            /* [out] */ REFERENCE_TIME *pTimeStart,
            /* [out] */ REFERENCE_TIME *pTimeEnd);
        
        HRESULT ( STDMETHODCALLTYPE *SetTime )( 
            IMediaSample * This,
            /* [in] */ REFERENCE_TIME *pTimeStart,
            /* [in] */ REFERENCE_TIME *pTimeEnd);
        
        HRESULT ( STDMETHODCALLTYPE *IsSyncPoint )( 
            IMediaSample * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetSyncPoint )( 
            IMediaSample * This,
            BOOL bIsSyncPoint);
        
        HRESULT ( STDMETHODCALLTYPE *IsPreroll )( 
            IMediaSample * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetPreroll )( 
            IMediaSample * This,
            BOOL bIsPreroll);
        
        long ( STDMETHODCALLTYPE *GetActualDataLength )( 
            IMediaSample * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetActualDataLength )( 
            IMediaSample * This,
            long __MIDL__IMediaSample0000);
        
        HRESULT ( STDMETHODCALLTYPE *GetMediaType )( 
            IMediaSample * This,
            AM_MEDIA_TYPE **ppMediaType);
        
        HRESULT ( STDMETHODCALLTYPE *SetMediaType )( 
            IMediaSample * This,
            AM_MEDIA_TYPE *pMediaType);
        
        HRESULT ( STDMETHODCALLTYPE *IsDiscontinuity )( 
            IMediaSample * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetDiscontinuity )( 
            IMediaSample * This,
            BOOL bDiscontinuity);
        
        HRESULT ( STDMETHODCALLTYPE *GetMediaTime )( 
            IMediaSample * This,
            /* [out] */ LONGLONG *pTimeStart,
            /* [out] */ LONGLONG *pTimeEnd);
        
        HRESULT ( STDMETHODCALLTYPE *SetMediaTime )( 
            IMediaSample * This,
            /* [in] */ LONGLONG *pTimeStart,
            /* [in] */ LONGLONG *pTimeEnd);
        
        END_INTERFACE
    } IMediaSampleVtbl;

    interface IMediaSample
    {
        CONST_VTBL struct IMediaSampleVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMediaSample_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMediaSample_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMediaSample_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IMediaSample_GetPointer(This,ppBuffer)	\
    ( (This)->lpVtbl -> GetPointer(This,ppBuffer) ) 

#define IMediaSample_GetSize(This)	\
    ( (This)->lpVtbl -> GetSize(This) ) 

#define IMediaSample_GetTime(This,pTimeStart,pTimeEnd)	\
    ( (This)->lpVtbl -> GetTime(This,pTimeStart,pTimeEnd) ) 

#define IMediaSample_SetTime(This,pTimeStart,pTimeEnd)	\
    ( (This)->lpVtbl -> SetTime(This,pTimeStart,pTimeEnd) ) 

#define IMediaSample_IsSyncPoint(This)	\
    ( (This)->lpVtbl -> IsSyncPoint(This) ) 

#define IMediaSample_SetSyncPoint(This,bIsSyncPoint)	\
    ( (This)->lpVtbl -> SetSyncPoint(This,bIsSyncPoint) ) 

#define IMediaSample_IsPreroll(This)	\
    ( (This)->lpVtbl -> IsPreroll(This) ) 

#define IMediaSample_SetPreroll(This,bIsPreroll)	\
    ( (This)->lpVtbl -> SetPreroll(This,bIsPreroll) ) 

#define IMediaSample_GetActualDataLength(This)	\
    ( (This)->lpVtbl -> GetActualDataLength(This) ) 

#define IMediaSample_SetActualDataLength(This,__MIDL__IMediaSample0000)	\
    ( (This)->lpVtbl -> SetActualDataLength(This,__MIDL__IMediaSample0000) ) 

#define IMediaSample_GetMediaType(This,ppMediaType)	\
    ( (This)->lpVtbl -> GetMediaType(This,ppMediaType) ) 

#define IMediaSample_SetMediaType(This,pMediaType)	\
    ( (This)->lpVtbl -> SetMediaType(This,pMediaType) ) 

#define IMediaSample_IsDiscontinuity(This)	\
    ( (This)->lpVtbl -> IsDiscontinuity(This) ) 

#define IMediaSample_SetDiscontinuity(This,bDiscontinuity)	\
    ( (This)->lpVtbl -> SetDiscontinuity(This,bDiscontinuity) ) 

#define IMediaSample_GetMediaTime(This,pTimeStart,pTimeEnd)	\
    ( (This)->lpVtbl -> GetMediaTime(This,pTimeStart,pTimeEnd) ) 

#define IMediaSample_SetMediaTime(This,pTimeStart,pTimeEnd)	\
    ( (This)->lpVtbl -> SetMediaTime(This,pTimeStart,pTimeEnd) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMediaSample_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_axcore_0000_0011 */
/* [local] */ 

typedef IMediaSample *PMEDIASAMPLE;


enum tagAM_SAMPLE_PROPERTY_FLAGS
    {	AM_SAMPLE_SPLICEPOINT	= 0x1,
	AM_SAMPLE_PREROLL	= 0x2,
	AM_SAMPLE_DATADISCONTINUITY	= 0x4,
	AM_SAMPLE_TYPECHANGED	= 0x8,
	AM_SAMPLE_TIMEVALID	= 0x10,
	AM_SAMPLE_TIMEDISCONTINUITY	= 0x40,
	AM_SAMPLE_FLUSH_ON_PAUSE	= 0x80,
	AM_SAMPLE_STOPVALID	= 0x100,
	AM_SAMPLE_ENDOFSTREAM	= 0x200,
	AM_STREAM_MEDIA	= 0,
	AM_STREAM_CONTROL	= 1
    } ;
typedef struct tagAM_SAMPLE2_PROPERTIES
    {
    DWORD cbData;
    DWORD dwTypeSpecificFlags;
    DWORD dwSampleFlags;
    LONG lActual;
    REFERENCE_TIME tStart;
    REFERENCE_TIME tStop;
    DWORD dwStreamId;
    AM_MEDIA_TYPE *pMediaType;
    BYTE *pbBuffer;
    LONG cbBuffer;
    } 	AM_SAMPLE2_PROPERTIES;



extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0011_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0011_v0_0_s_ifspec;

#ifndef __IMediaSample2_INTERFACE_DEFINED__
#define __IMediaSample2_INTERFACE_DEFINED__

/* interface IMediaSample2 */
/* [unique][uuid][object][local] */ 


EXTERN_C const IID IID_IMediaSample2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("36b73884-c2c8-11cf-8b46-00805f6cef60")
    IMediaSample2 : public IMediaSample
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE GetProperties( 
            /* [in] */ DWORD cbProperties,
            /* [size_is][out] */ BYTE *pbProperties) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetProperties( 
            /* [in] */ DWORD cbProperties,
            /* [size_is][in] */ const BYTE *pbProperties) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMediaSample2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMediaSample2 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMediaSample2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMediaSample2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetPointer )( 
            IMediaSample2 * This,
            /* [out] */ BYTE **ppBuffer);
        
        long ( STDMETHODCALLTYPE *GetSize )( 
            IMediaSample2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTime )( 
            IMediaSample2 * This,
            /* [out] */ REFERENCE_TIME *pTimeStart,
            /* [out] */ REFERENCE_TIME *pTimeEnd);
        
        HRESULT ( STDMETHODCALLTYPE *SetTime )( 
            IMediaSample2 * This,
            /* [in] */ REFERENCE_TIME *pTimeStart,
            /* [in] */ REFERENCE_TIME *pTimeEnd);
        
        HRESULT ( STDMETHODCALLTYPE *IsSyncPoint )( 
            IMediaSample2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetSyncPoint )( 
            IMediaSample2 * This,
            BOOL bIsSyncPoint);
        
        HRESULT ( STDMETHODCALLTYPE *IsPreroll )( 
            IMediaSample2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetPreroll )( 
            IMediaSample2 * This,
            BOOL bIsPreroll);
        
        long ( STDMETHODCALLTYPE *GetActualDataLength )( 
            IMediaSample2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetActualDataLength )( 
            IMediaSample2 * This,
            long __MIDL__IMediaSample0000);
        
        HRESULT ( STDMETHODCALLTYPE *GetMediaType )( 
            IMediaSample2 * This,
            AM_MEDIA_TYPE **ppMediaType);
        
        HRESULT ( STDMETHODCALLTYPE *SetMediaType )( 
            IMediaSample2 * This,
            AM_MEDIA_TYPE *pMediaType);
        
        HRESULT ( STDMETHODCALLTYPE *IsDiscontinuity )( 
            IMediaSample2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetDiscontinuity )( 
            IMediaSample2 * This,
            BOOL bDiscontinuity);
        
        HRESULT ( STDMETHODCALLTYPE *GetMediaTime )( 
            IMediaSample2 * This,
            /* [out] */ LONGLONG *pTimeStart,
            /* [out] */ LONGLONG *pTimeEnd);
        
        HRESULT ( STDMETHODCALLTYPE *SetMediaTime )( 
            IMediaSample2 * This,
            /* [in] */ LONGLONG *pTimeStart,
            /* [in] */ LONGLONG *pTimeEnd);
        
        HRESULT ( STDMETHODCALLTYPE *GetProperties )( 
            IMediaSample2 * This,
            /* [in] */ DWORD cbProperties,
            /* [size_is][out] */ BYTE *pbProperties);
        
        HRESULT ( STDMETHODCALLTYPE *SetProperties )( 
            IMediaSample2 * This,
            /* [in] */ DWORD cbProperties,
            /* [size_is][in] */ const BYTE *pbProperties);
        
        END_INTERFACE
    } IMediaSample2Vtbl;

    interface IMediaSample2
    {
        CONST_VTBL struct IMediaSample2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMediaSample2_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMediaSample2_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMediaSample2_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IMediaSample2_GetPointer(This,ppBuffer)	\
    ( (This)->lpVtbl -> GetPointer(This,ppBuffer) ) 

#define IMediaSample2_GetSize(This)	\
    ( (This)->lpVtbl -> GetSize(This) ) 

#define IMediaSample2_GetTime(This,pTimeStart,pTimeEnd)	\
    ( (This)->lpVtbl -> GetTime(This,pTimeStart,pTimeEnd) ) 

#define IMediaSample2_SetTime(This,pTimeStart,pTimeEnd)	\
    ( (This)->lpVtbl -> SetTime(This,pTimeStart,pTimeEnd) ) 

#define IMediaSample2_IsSyncPoint(This)	\
    ( (This)->lpVtbl -> IsSyncPoint(This) ) 

#define IMediaSample2_SetSyncPoint(This,bIsSyncPoint)	\
    ( (This)->lpVtbl -> SetSyncPoint(This,bIsSyncPoint) ) 

#define IMediaSample2_IsPreroll(This)	\
    ( (This)->lpVtbl -> IsPreroll(This) ) 

#define IMediaSample2_SetPreroll(This,bIsPreroll)	\
    ( (This)->lpVtbl -> SetPreroll(This,bIsPreroll) ) 

#define IMediaSample2_GetActualDataLength(This)	\
    ( (This)->lpVtbl -> GetActualDataLength(This) ) 

#define IMediaSample2_SetActualDataLength(This,__MIDL__IMediaSample0000)	\
    ( (This)->lpVtbl -> SetActualDataLength(This,__MIDL__IMediaSample0000) ) 

#define IMediaSample2_GetMediaType(This,ppMediaType)	\
    ( (This)->lpVtbl -> GetMediaType(This,ppMediaType) ) 

#define IMediaSample2_SetMediaType(This,pMediaType)	\
    ( (This)->lpVtbl -> SetMediaType(This,pMediaType) ) 

#define IMediaSample2_IsDiscontinuity(This)	\
    ( (This)->lpVtbl -> IsDiscontinuity(This) ) 

#define IMediaSample2_SetDiscontinuity(This,bDiscontinuity)	\
    ( (This)->lpVtbl -> SetDiscontinuity(This,bDiscontinuity) ) 

#define IMediaSample2_GetMediaTime(This,pTimeStart,pTimeEnd)	\
    ( (This)->lpVtbl -> GetMediaTime(This,pTimeStart,pTimeEnd) ) 

#define IMediaSample2_SetMediaTime(This,pTimeStart,pTimeEnd)	\
    ( (This)->lpVtbl -> SetMediaTime(This,pTimeStart,pTimeEnd) ) 


#define IMediaSample2_GetProperties(This,cbProperties,pbProperties)	\
    ( (This)->lpVtbl -> GetProperties(This,cbProperties,pbProperties) ) 

#define IMediaSample2_SetProperties(This,cbProperties,pbProperties)	\
    ( (This)->lpVtbl -> SetProperties(This,cbProperties,pbProperties) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMediaSample2_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_axcore_0000_0012 */
/* [local] */ 

typedef IMediaSample2 *PMEDIASAMPLE2;

#define AM_GBF_PREVFRAMESKIPPED 1
#define AM_GBF_NOTASYNCPOINT 2
#define AM_GBF_NOWAIT 4
#define AM_GBF_NODDSURFACELOCK 8


extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0012_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0012_v0_0_s_ifspec;

#ifndef __IMemAllocator_INTERFACE_DEFINED__
#define __IMemAllocator_INTERFACE_DEFINED__

/* interface IMemAllocator */
/* [unique][uuid][object] */ 


EXTERN_C const IID IID_IMemAllocator;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("56a8689c-0ad4-11ce-b03a-0020af0ba770")
    IMemAllocator : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE SetProperties( 
            /* [in] */ ALLOCATOR_PROPERTIES *pRequest,
            /* [out] */ ALLOCATOR_PROPERTIES *pActual) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetProperties( 
            /* [out] */ ALLOCATOR_PROPERTIES *pProps) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Commit( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Decommit( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetBuffer( 
            /* [out] */ IMediaSample **ppBuffer,
            /* [in] */ REFERENCE_TIME *pStartTime,
            /* [in] */ REFERENCE_TIME *pEndTime,
            /* [in] */ DWORD dwFlags) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE ReleaseBuffer( 
            /* [in] */ IMediaSample *pBuffer) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMemAllocatorVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMemAllocator * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMemAllocator * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMemAllocator * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetProperties )( 
            IMemAllocator * This,
            /* [in] */ ALLOCATOR_PROPERTIES *pRequest,
            /* [out] */ ALLOCATOR_PROPERTIES *pActual);
        
        HRESULT ( STDMETHODCALLTYPE *GetProperties )( 
            IMemAllocator * This,
            /* [out] */ ALLOCATOR_PROPERTIES *pProps);
        
        HRESULT ( STDMETHODCALLTYPE *Commit )( 
            IMemAllocator * This);
        
        HRESULT ( STDMETHODCALLTYPE *Decommit )( 
            IMemAllocator * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetBuffer )( 
            IMemAllocator * This,
            /* [out] */ IMediaSample **ppBuffer,
            /* [in] */ REFERENCE_TIME *pStartTime,
            /* [in] */ REFERENCE_TIME *pEndTime,
            /* [in] */ DWORD dwFlags);
        
        HRESULT ( STDMETHODCALLTYPE *ReleaseBuffer )( 
            IMemAllocator * This,
            /* [in] */ IMediaSample *pBuffer);
        
        END_INTERFACE
    } IMemAllocatorVtbl;

    interface IMemAllocator
    {
        CONST_VTBL struct IMemAllocatorVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMemAllocator_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMemAllocator_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMemAllocator_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IMemAllocator_SetProperties(This,pRequest,pActual)	\
    ( (This)->lpVtbl -> SetProperties(This,pRequest,pActual) ) 

#define IMemAllocator_GetProperties(This,pProps)	\
    ( (This)->lpVtbl -> GetProperties(This,pProps) ) 

#define IMemAllocator_Commit(This)	\
    ( (This)->lpVtbl -> Commit(This) ) 

#define IMemAllocator_Decommit(This)	\
    ( (This)->lpVtbl -> Decommit(This) ) 

#define IMemAllocator_GetBuffer(This,ppBuffer,pStartTime,pEndTime,dwFlags)	\
    ( (This)->lpVtbl -> GetBuffer(This,ppBuffer,pStartTime,pEndTime,dwFlags) ) 

#define IMemAllocator_ReleaseBuffer(This,pBuffer)	\
    ( (This)->lpVtbl -> ReleaseBuffer(This,pBuffer) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMemAllocator_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_axcore_0000_0013 */
/* [local] */ 

typedef IMemAllocator *PMEMALLOCATOR;



extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0013_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0013_v0_0_s_ifspec;

#ifndef __IMemAllocator2_INTERFACE_DEFINED__
#define __IMemAllocator2_INTERFACE_DEFINED__

/* interface IMemAllocator2 */
/* [unique][uuid][object][local] */ 


EXTERN_C const IID IID_IMemAllocator2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("fa04cdc8-cb49-11d1-a4ef-00c04fb6fa12")
    IMemAllocator2 : public IMemAllocator
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE GetBusyCount( 
            DWORD *pdwBusyCount) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetFreeCount( 
            DWORD *pdwFreeCount) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMemAllocator2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMemAllocator2 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMemAllocator2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMemAllocator2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetProperties )( 
            IMemAllocator2 * This,
            /* [in] */ ALLOCATOR_PROPERTIES *pRequest,
            /* [out] */ ALLOCATOR_PROPERTIES *pActual);
        
        HRESULT ( STDMETHODCALLTYPE *GetProperties )( 
            IMemAllocator2 * This,
            /* [out] */ ALLOCATOR_PROPERTIES *pProps);
        
        HRESULT ( STDMETHODCALLTYPE *Commit )( 
            IMemAllocator2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *Decommit )( 
            IMemAllocator2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetBuffer )( 
            IMemAllocator2 * This,
            /* [out] */ IMediaSample **ppBuffer,
            /* [in] */ REFERENCE_TIME *pStartTime,
            /* [in] */ REFERENCE_TIME *pEndTime,
            /* [in] */ DWORD dwFlags);
        
        HRESULT ( STDMETHODCALLTYPE *ReleaseBuffer )( 
            IMemAllocator2 * This,
            /* [in] */ IMediaSample *pBuffer);
        
        HRESULT ( STDMETHODCALLTYPE *GetBusyCount )( 
            IMemAllocator2 * This,
            DWORD *pdwBusyCount);
        
        HRESULT ( STDMETHODCALLTYPE *GetFreeCount )( 
            IMemAllocator2 * This,
            DWORD *pdwFreeCount);
        
        END_INTERFACE
    } IMemAllocator2Vtbl;

    interface IMemAllocator2
    {
        CONST_VTBL struct IMemAllocator2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMemAllocator2_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMemAllocator2_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMemAllocator2_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IMemAllocator2_SetProperties(This,pRequest,pActual)	\
    ( (This)->lpVtbl -> SetProperties(This,pRequest,pActual) ) 

#define IMemAllocator2_GetProperties(This,pProps)	\
    ( (This)->lpVtbl -> GetProperties(This,pProps) ) 

#define IMemAllocator2_Commit(This)	\
    ( (This)->lpVtbl -> Commit(This) ) 

#define IMemAllocator2_Decommit(This)	\
    ( (This)->lpVtbl -> Decommit(This) ) 

#define IMemAllocator2_GetBuffer(This,ppBuffer,pStartTime,pEndTime,dwFlags)	\
    ( (This)->lpVtbl -> GetBuffer(This,ppBuffer,pStartTime,pEndTime,dwFlags) ) 

#define IMemAllocator2_ReleaseBuffer(This,pBuffer)	\
    ( (This)->lpVtbl -> ReleaseBuffer(This,pBuffer) ) 


#define IMemAllocator2_GetBusyCount(This,pdwBusyCount)	\
    ( (This)->lpVtbl -> GetBusyCount(This,pdwBusyCount) ) 

#define IMemAllocator2_GetFreeCount(This,pdwFreeCount)	\
    ( (This)->lpVtbl -> GetFreeCount(This,pdwFreeCount) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMemAllocator2_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_axcore_0000_0014 */
/* [local] */ 

typedef IMemAllocator2 *PMEMALLOCATOR2;



extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0014_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0014_v0_0_s_ifspec;

#ifndef __IMemInputPin_INTERFACE_DEFINED__
#define __IMemInputPin_INTERFACE_DEFINED__

/* interface IMemInputPin */
/* [unique][uuid][object] */ 


EXTERN_C const IID IID_IMemInputPin;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("56a8689d-0ad4-11ce-b03a-0020af0ba770")
    IMemInputPin : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE GetAllocator( 
            /* [out] */ IMemAllocator **ppAllocator) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE NotifyAllocator( 
            /* [in] */ IMemAllocator *pAllocator,
            /* [in] */ BOOL bReadOnly) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetAllocatorRequirements( 
            /* [out] */ ALLOCATOR_PROPERTIES *pProps) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Receive( 
            /* [in] */ IMediaSample *pSample) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE ReceiveMultiple( 
            /* [size_is][in] */ IMediaSample **pSamples,
            /* [in] */ long nSamples,
            /* [out] */ long *nSamplesProcessed) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE ReceiveCanBlock( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMemInputPinVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMemInputPin * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMemInputPin * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMemInputPin * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetAllocator )( 
            IMemInputPin * This,
            /* [out] */ IMemAllocator **ppAllocator);
        
        HRESULT ( STDMETHODCALLTYPE *NotifyAllocator )( 
            IMemInputPin * This,
            /* [in] */ IMemAllocator *pAllocator,
            /* [in] */ BOOL bReadOnly);
        
        HRESULT ( STDMETHODCALLTYPE *GetAllocatorRequirements )( 
            IMemInputPin * This,
            /* [out] */ ALLOCATOR_PROPERTIES *pProps);
        
        HRESULT ( STDMETHODCALLTYPE *Receive )( 
            IMemInputPin * This,
            /* [in] */ IMediaSample *pSample);
        
        HRESULT ( STDMETHODCALLTYPE *ReceiveMultiple )( 
            IMemInputPin * This,
            /* [size_is][in] */ IMediaSample **pSamples,
            /* [in] */ long nSamples,
            /* [out] */ long *nSamplesProcessed);
        
        HRESULT ( STDMETHODCALLTYPE *ReceiveCanBlock )( 
            IMemInputPin * This);
        
        END_INTERFACE
    } IMemInputPinVtbl;

    interface IMemInputPin
    {
        CONST_VTBL struct IMemInputPinVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMemInputPin_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMemInputPin_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMemInputPin_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IMemInputPin_GetAllocator(This,ppAllocator)	\
    ( (This)->lpVtbl -> GetAllocator(This,ppAllocator) ) 

#define IMemInputPin_NotifyAllocator(This,pAllocator,bReadOnly)	\
    ( (This)->lpVtbl -> NotifyAllocator(This,pAllocator,bReadOnly) ) 

#define IMemInputPin_GetAllocatorRequirements(This,pProps)	\
    ( (This)->lpVtbl -> GetAllocatorRequirements(This,pProps) ) 

#define IMemInputPin_Receive(This,pSample)	\
    ( (This)->lpVtbl -> Receive(This,pSample) ) 

#define IMemInputPin_ReceiveMultiple(This,pSamples,nSamples,nSamplesProcessed)	\
    ( (This)->lpVtbl -> ReceiveMultiple(This,pSamples,nSamples,nSamplesProcessed) ) 

#define IMemInputPin_ReceiveCanBlock(This)	\
    ( (This)->lpVtbl -> ReceiveCanBlock(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMemInputPin_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_axcore_0000_0015 */
/* [local] */ 

typedef IMemInputPin *PMEMINPUTPIN;



extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0015_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0015_v0_0_s_ifspec;

#ifndef __IAMovieSetup_INTERFACE_DEFINED__
#define __IAMovieSetup_INTERFACE_DEFINED__

/* interface IAMovieSetup */
/* [unique][uuid][object] */ 


EXTERN_C const IID IID_IAMovieSetup;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("a3d8cec0-7e5a-11cf-bbc5-00805f6cef20")
    IAMovieSetup : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE Register( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Unregister( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMovieSetupVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMovieSetup * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMovieSetup * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMovieSetup * This);
        
        HRESULT ( STDMETHODCALLTYPE *Register )( 
            IAMovieSetup * This);
        
        HRESULT ( STDMETHODCALLTYPE *Unregister )( 
            IAMovieSetup * This);
        
        END_INTERFACE
    } IAMovieSetupVtbl;

    interface IAMovieSetup
    {
        CONST_VTBL struct IAMovieSetupVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMovieSetup_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAMovieSetup_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAMovieSetup_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAMovieSetup_Register(This)	\
    ( (This)->lpVtbl -> Register(This) ) 

#define IAMovieSetup_Unregister(This)	\
    ( (This)->lpVtbl -> Unregister(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAMovieSetup_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_axcore_0000_0016 */
/* [local] */ 

typedef IAMovieSetup *PAMOVIESETUP;

typedef 
enum AM_SEEKING_SeekingFlags
    {	AM_SEEKING_NoPositioning	= 0,
	AM_SEEKING_AbsolutePositioning	= 0x1,
	AM_SEEKING_RelativePositioning	= 0x2,
	AM_SEEKING_IncrementalPositioning	= 0x3,
	AM_SEEKING_PositioningBitsMask	= 0x3,
	AM_SEEKING_SeekToKeyFrame	= 0x4,
	AM_SEEKING_ReturnTime	= 0x8,
	AM_SEEKING_Segment	= 0x10,
	AM_SEEKING_NoFlush	= 0x20
    } 	AM_SEEKING_SEEKING_FLAGS;

typedef 
enum AM_SEEKING_SeekingCapabilities
    {	AM_SEEKING_CanSeekAbsolute	= 0x1,
	AM_SEEKING_CanSeekForwards	= 0x2,
	AM_SEEKING_CanSeekBackwards	= 0x4,
	AM_SEEKING_CanGetCurrentPos	= 0x8,
	AM_SEEKING_CanGetStopPos	= 0x10,
	AM_SEEKING_CanGetDuration	= 0x20,
	AM_SEEKING_CanPlayBackwards	= 0x40,
	AM_SEEKING_CanDoSegments	= 0x80,
	AM_SEEKING_Source	= 0x100,
	AM_SEEKING_CanSkipForwards	= 0x200,
	AM_SEEKING_CanSkipBackwards	= 0x400,
	AM_SEEKING_CanSkipValid	= 0x800,
	AM_SEEKING_CanResetPlaylist	= 0x1000
    } 	AM_SEEKING_SEEKING_CAPABILITIES;



extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0016_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0016_v0_0_s_ifspec;

#ifndef __IMediaSeeking_INTERFACE_DEFINED__
#define __IMediaSeeking_INTERFACE_DEFINED__

/* interface IMediaSeeking */
/* [unique][uuid][object] */ 


EXTERN_C const IID IID_IMediaSeeking;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("36b73880-c2c8-11cf-8b46-00805f6cef60")
    IMediaSeeking : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE GetCapabilities( 
            /* [out] */ DWORD *pCapabilities) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE CheckCapabilities( 
            /* [out][in] */ DWORD *pCapabilities) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE IsFormatSupported( 
            /* [in] */ const GUID *pFormat) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE QueryPreferredFormat( 
            /* [out] */ GUID *pFormat) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetTimeFormat( 
            /* [out] */ GUID *pFormat) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE IsUsingTimeFormat( 
            /* [in] */ const GUID *pFormat) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetTimeFormat( 
            /* [in] */ const GUID *pFormat) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetDuration( 
            /* [out] */ LONGLONG *pDuration) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetStopPosition( 
            /* [out] */ LONGLONG *pStop) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetCurrentPosition( 
            /* [out] */ LONGLONG *pCurrent) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE ConvertTimeFormat( 
            /* [out] */ LONGLONG *pTarget,
            /* [in] */ const GUID *pTargetFormat,
            /* [in] */ LONGLONG Source,
            /* [in] */ const GUID *pSourceFormat) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetPositions( 
            /* [out][in] */ LONGLONG *pCurrent,
            /* [in] */ DWORD dwCurrentFlags,
            /* [out][in] */ LONGLONG *pStop,
            /* [in] */ DWORD dwStopFlags) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetPositions( 
            /* [out] */ LONGLONG *pCurrent,
            /* [out] */ LONGLONG *pStop) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetAvailable( 
            /* [out] */ LONGLONG *pEarliest,
            /* [out] */ LONGLONG *pLatest) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetRate( 
            /* [in] */ double dRate) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetRate( 
            /* [out] */ double *pdRate) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetPreroll( 
            /* [out] */ LONGLONG *pllPreroll) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMediaSeekingVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMediaSeeking * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMediaSeeking * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMediaSeeking * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetCapabilities )( 
            IMediaSeeking * This,
            /* [out] */ DWORD *pCapabilities);
        
        HRESULT ( STDMETHODCALLTYPE *CheckCapabilities )( 
            IMediaSeeking * This,
            /* [out][in] */ DWORD *pCapabilities);
        
        HRESULT ( STDMETHODCALLTYPE *IsFormatSupported )( 
            IMediaSeeking * This,
            /* [in] */ const GUID *pFormat);
        
        HRESULT ( STDMETHODCALLTYPE *QueryPreferredFormat )( 
            IMediaSeeking * This,
            /* [out] */ GUID *pFormat);
        
        HRESULT ( STDMETHODCALLTYPE *GetTimeFormat )( 
            IMediaSeeking * This,
            /* [out] */ GUID *pFormat);
        
        HRESULT ( STDMETHODCALLTYPE *IsUsingTimeFormat )( 
            IMediaSeeking * This,
            /* [in] */ const GUID *pFormat);
        
        HRESULT ( STDMETHODCALLTYPE *SetTimeFormat )( 
            IMediaSeeking * This,
            /* [in] */ const GUID *pFormat);
        
        HRESULT ( STDMETHODCALLTYPE *GetDuration )( 
            IMediaSeeking * This,
            /* [out] */ LONGLONG *pDuration);
        
        HRESULT ( STDMETHODCALLTYPE *GetStopPosition )( 
            IMediaSeeking * This,
            /* [out] */ LONGLONG *pStop);
        
        HRESULT ( STDMETHODCALLTYPE *GetCurrentPosition )( 
            IMediaSeeking * This,
            /* [out] */ LONGLONG *pCurrent);
        
        HRESULT ( STDMETHODCALLTYPE *ConvertTimeFormat )( 
            IMediaSeeking * This,
            /* [out] */ LONGLONG *pTarget,
            /* [in] */ const GUID *pTargetFormat,
            /* [in] */ LONGLONG Source,
            /* [in] */ const GUID *pSourceFormat);
        
        HRESULT ( STDMETHODCALLTYPE *SetPositions )( 
            IMediaSeeking * This,
            /* [out][in] */ LONGLONG *pCurrent,
            /* [in] */ DWORD dwCurrentFlags,
            /* [out][in] */ LONGLONG *pStop,
            /* [in] */ DWORD dwStopFlags);
        
        HRESULT ( STDMETHODCALLTYPE *GetPositions )( 
            IMediaSeeking * This,
            /* [out] */ LONGLONG *pCurrent,
            /* [out] */ LONGLONG *pStop);
        
        HRESULT ( STDMETHODCALLTYPE *GetAvailable )( 
            IMediaSeeking * This,
            /* [out] */ LONGLONG *pEarliest,
            /* [out] */ LONGLONG *pLatest);
        
        HRESULT ( STDMETHODCALLTYPE *SetRate )( 
            IMediaSeeking * This,
            /* [in] */ double dRate);
        
        HRESULT ( STDMETHODCALLTYPE *GetRate )( 
            IMediaSeeking * This,
            /* [out] */ double *pdRate);
        
        HRESULT ( STDMETHODCALLTYPE *GetPreroll )( 
            IMediaSeeking * This,
            /* [out] */ LONGLONG *pllPreroll);
        
        END_INTERFACE
    } IMediaSeekingVtbl;

    interface IMediaSeeking
    {
        CONST_VTBL struct IMediaSeekingVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMediaSeeking_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMediaSeeking_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMediaSeeking_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IMediaSeeking_GetCapabilities(This,pCapabilities)	\
    ( (This)->lpVtbl -> GetCapabilities(This,pCapabilities) ) 

#define IMediaSeeking_CheckCapabilities(This,pCapabilities)	\
    ( (This)->lpVtbl -> CheckCapabilities(This,pCapabilities) ) 

#define IMediaSeeking_IsFormatSupported(This,pFormat)	\
    ( (This)->lpVtbl -> IsFormatSupported(This,pFormat) ) 

#define IMediaSeeking_QueryPreferredFormat(This,pFormat)	\
    ( (This)->lpVtbl -> QueryPreferredFormat(This,pFormat) ) 

#define IMediaSeeking_GetTimeFormat(This,pFormat)	\
    ( (This)->lpVtbl -> GetTimeFormat(This,pFormat) ) 

#define IMediaSeeking_IsUsingTimeFormat(This,pFormat)	\
    ( (This)->lpVtbl -> IsUsingTimeFormat(This,pFormat) ) 

#define IMediaSeeking_SetTimeFormat(This,pFormat)	\
    ( (This)->lpVtbl -> SetTimeFormat(This,pFormat) ) 

#define IMediaSeeking_GetDuration(This,pDuration)	\
    ( (This)->lpVtbl -> GetDuration(This,pDuration) ) 

#define IMediaSeeking_GetStopPosition(This,pStop)	\
    ( (This)->lpVtbl -> GetStopPosition(This,pStop) ) 

#define IMediaSeeking_GetCurrentPosition(This,pCurrent)	\
    ( (This)->lpVtbl -> GetCurrentPosition(This,pCurrent) ) 

#define IMediaSeeking_ConvertTimeFormat(This,pTarget,pTargetFormat,Source,pSourceFormat)	\
    ( (This)->lpVtbl -> ConvertTimeFormat(This,pTarget,pTargetFormat,Source,pSourceFormat) ) 

#define IMediaSeeking_SetPositions(This,pCurrent,dwCurrentFlags,pStop,dwStopFlags)	\
    ( (This)->lpVtbl -> SetPositions(This,pCurrent,dwCurrentFlags,pStop,dwStopFlags) ) 

#define IMediaSeeking_GetPositions(This,pCurrent,pStop)	\
    ( (This)->lpVtbl -> GetPositions(This,pCurrent,pStop) ) 

#define IMediaSeeking_GetAvailable(This,pEarliest,pLatest)	\
    ( (This)->lpVtbl -> GetAvailable(This,pEarliest,pLatest) ) 

#define IMediaSeeking_SetRate(This,dRate)	\
    ( (This)->lpVtbl -> SetRate(This,dRate) ) 

#define IMediaSeeking_GetRate(This,pdRate)	\
    ( (This)->lpVtbl -> GetRate(This,pdRate) ) 

#define IMediaSeeking_GetPreroll(This,pllPreroll)	\
    ( (This)->lpVtbl -> GetPreroll(This,pllPreroll) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMediaSeeking_INTERFACE_DEFINED__ */


#ifndef __IMediaSeeking2_INTERFACE_DEFINED__
#define __IMediaSeeking2_INTERFACE_DEFINED__

/* interface IMediaSeeking2 */
/* [unique][uuid][object] */ 


EXTERN_C const IID IID_IMediaSeeking2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("ec1136e0-64df-405e-b6b6-173ce09c83dc")
    IMediaSeeking2 : public IMediaSeeking
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE Next( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Previous( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE ResetPlaylist( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMediaSeeking2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMediaSeeking2 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMediaSeeking2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMediaSeeking2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetCapabilities )( 
            IMediaSeeking2 * This,
            /* [out] */ DWORD *pCapabilities);
        
        HRESULT ( STDMETHODCALLTYPE *CheckCapabilities )( 
            IMediaSeeking2 * This,
            /* [out][in] */ DWORD *pCapabilities);
        
        HRESULT ( STDMETHODCALLTYPE *IsFormatSupported )( 
            IMediaSeeking2 * This,
            /* [in] */ const GUID *pFormat);
        
        HRESULT ( STDMETHODCALLTYPE *QueryPreferredFormat )( 
            IMediaSeeking2 * This,
            /* [out] */ GUID *pFormat);
        
        HRESULT ( STDMETHODCALLTYPE *GetTimeFormat )( 
            IMediaSeeking2 * This,
            /* [out] */ GUID *pFormat);
        
        HRESULT ( STDMETHODCALLTYPE *IsUsingTimeFormat )( 
            IMediaSeeking2 * This,
            /* [in] */ const GUID *pFormat);
        
        HRESULT ( STDMETHODCALLTYPE *SetTimeFormat )( 
            IMediaSeeking2 * This,
            /* [in] */ const GUID *pFormat);
        
        HRESULT ( STDMETHODCALLTYPE *GetDuration )( 
            IMediaSeeking2 * This,
            /* [out] */ LONGLONG *pDuration);
        
        HRESULT ( STDMETHODCALLTYPE *GetStopPosition )( 
            IMediaSeeking2 * This,
            /* [out] */ LONGLONG *pStop);
        
        HRESULT ( STDMETHODCALLTYPE *GetCurrentPosition )( 
            IMediaSeeking2 * This,
            /* [out] */ LONGLONG *pCurrent);
        
        HRESULT ( STDMETHODCALLTYPE *ConvertTimeFormat )( 
            IMediaSeeking2 * This,
            /* [out] */ LONGLONG *pTarget,
            /* [in] */ const GUID *pTargetFormat,
            /* [in] */ LONGLONG Source,
            /* [in] */ const GUID *pSourceFormat);
        
        HRESULT ( STDMETHODCALLTYPE *SetPositions )( 
            IMediaSeeking2 * This,
            /* [out][in] */ LONGLONG *pCurrent,
            /* [in] */ DWORD dwCurrentFlags,
            /* [out][in] */ LONGLONG *pStop,
            /* [in] */ DWORD dwStopFlags);
        
        HRESULT ( STDMETHODCALLTYPE *GetPositions )( 
            IMediaSeeking2 * This,
            /* [out] */ LONGLONG *pCurrent,
            /* [out] */ LONGLONG *pStop);
        
        HRESULT ( STDMETHODCALLTYPE *GetAvailable )( 
            IMediaSeeking2 * This,
            /* [out] */ LONGLONG *pEarliest,
            /* [out] */ LONGLONG *pLatest);
        
        HRESULT ( STDMETHODCALLTYPE *SetRate )( 
            IMediaSeeking2 * This,
            /* [in] */ double dRate);
        
        HRESULT ( STDMETHODCALLTYPE *GetRate )( 
            IMediaSeeking2 * This,
            /* [out] */ double *pdRate);
        
        HRESULT ( STDMETHODCALLTYPE *GetPreroll )( 
            IMediaSeeking2 * This,
            /* [out] */ LONGLONG *pllPreroll);
        
        HRESULT ( STDMETHODCALLTYPE *Next )( 
            IMediaSeeking2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *Previous )( 
            IMediaSeeking2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *ResetPlaylist )( 
            IMediaSeeking2 * This);
        
        END_INTERFACE
    } IMediaSeeking2Vtbl;

    interface IMediaSeeking2
    {
        CONST_VTBL struct IMediaSeeking2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMediaSeeking2_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMediaSeeking2_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMediaSeeking2_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IMediaSeeking2_GetCapabilities(This,pCapabilities)	\
    ( (This)->lpVtbl -> GetCapabilities(This,pCapabilities) ) 

#define IMediaSeeking2_CheckCapabilities(This,pCapabilities)	\
    ( (This)->lpVtbl -> CheckCapabilities(This,pCapabilities) ) 

#define IMediaSeeking2_IsFormatSupported(This,pFormat)	\
    ( (This)->lpVtbl -> IsFormatSupported(This,pFormat) ) 

#define IMediaSeeking2_QueryPreferredFormat(This,pFormat)	\
    ( (This)->lpVtbl -> QueryPreferredFormat(This,pFormat) ) 

#define IMediaSeeking2_GetTimeFormat(This,pFormat)	\
    ( (This)->lpVtbl -> GetTimeFormat(This,pFormat) ) 

#define IMediaSeeking2_IsUsingTimeFormat(This,pFormat)	\
    ( (This)->lpVtbl -> IsUsingTimeFormat(This,pFormat) ) 

#define IMediaSeeking2_SetTimeFormat(This,pFormat)	\
    ( (This)->lpVtbl -> SetTimeFormat(This,pFormat) ) 

#define IMediaSeeking2_GetDuration(This,pDuration)	\
    ( (This)->lpVtbl -> GetDuration(This,pDuration) ) 

#define IMediaSeeking2_GetStopPosition(This,pStop)	\
    ( (This)->lpVtbl -> GetStopPosition(This,pStop) ) 

#define IMediaSeeking2_GetCurrentPosition(This,pCurrent)	\
    ( (This)->lpVtbl -> GetCurrentPosition(This,pCurrent) ) 

#define IMediaSeeking2_ConvertTimeFormat(This,pTarget,pTargetFormat,Source,pSourceFormat)	\
    ( (This)->lpVtbl -> ConvertTimeFormat(This,pTarget,pTargetFormat,Source,pSourceFormat) ) 

#define IMediaSeeking2_SetPositions(This,pCurrent,dwCurrentFlags,pStop,dwStopFlags)	\
    ( (This)->lpVtbl -> SetPositions(This,pCurrent,dwCurrentFlags,pStop,dwStopFlags) ) 

#define IMediaSeeking2_GetPositions(This,pCurrent,pStop)	\
    ( (This)->lpVtbl -> GetPositions(This,pCurrent,pStop) ) 

#define IMediaSeeking2_GetAvailable(This,pEarliest,pLatest)	\
    ( (This)->lpVtbl -> GetAvailable(This,pEarliest,pLatest) ) 

#define IMediaSeeking2_SetRate(This,dRate)	\
    ( (This)->lpVtbl -> SetRate(This,dRate) ) 

#define IMediaSeeking2_GetRate(This,pdRate)	\
    ( (This)->lpVtbl -> GetRate(This,pdRate) ) 

#define IMediaSeeking2_GetPreroll(This,pllPreroll)	\
    ( (This)->lpVtbl -> GetPreroll(This,pllPreroll) ) 


#define IMediaSeeking2_Next(This)	\
    ( (This)->lpVtbl -> Next(This) ) 

#define IMediaSeeking2_Previous(This)	\
    ( (This)->lpVtbl -> Previous(This) ) 

#define IMediaSeeking2_ResetPlaylist(This)	\
    ( (This)->lpVtbl -> ResetPlaylist(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMediaSeeking2_INTERFACE_DEFINED__ */


#ifndef __IAudioRenderer_INTERFACE_DEFINED__
#define __IAudioRenderer_INTERFACE_DEFINED__

/* interface IAudioRenderer */
/* [unique][uuid][object] */ 


EXTERN_C const IID IID_IAudioRenderer;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("86612576-6b0a-4033-8374-edbdc59ac25c")
    IAudioRenderer : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE SetDriftRate( 
            /* [in] */ DOUBLE dwRate) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAudioRendererVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAudioRenderer * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAudioRenderer * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAudioRenderer * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetDriftRate )( 
            IAudioRenderer * This,
            /* [in] */ DOUBLE dwRate);
        
        END_INTERFACE
    } IAudioRendererVtbl;

    interface IAudioRenderer
    {
        CONST_VTBL struct IAudioRendererVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAudioRenderer_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAudioRenderer_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAudioRenderer_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAudioRenderer_SetDriftRate(This,dwRate)	\
    ( (This)->lpVtbl -> SetDriftRate(This,dwRate) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAudioRenderer_INTERFACE_DEFINED__ */


#ifndef __IAudioRendererWaveOut_INTERFACE_DEFINED__
#define __IAudioRendererWaveOut_INTERFACE_DEFINED__

/* interface IAudioRendererWaveOut */
/* [unique][uuid][object] */ 


EXTERN_C const IID IID_IAudioRendererWaveOut;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("8244a79e-9986-4187-abcb-3a69542f7cc7")
    IAudioRendererWaveOut : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE SendMessage( 
            /* [in] */ UINT uMsg,
            /* [in] */ DWORD dw1,
            /* [in] */ DWORD dw2) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAudioRendererWaveOutVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAudioRendererWaveOut * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAudioRendererWaveOut * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAudioRendererWaveOut * This);
        
        HRESULT ( STDMETHODCALLTYPE *SendMessage )( 
            IAudioRendererWaveOut * This,
            /* [in] */ UINT uMsg,
            /* [in] */ DWORD dw1,
            /* [in] */ DWORD dw2);
        
        END_INTERFACE
    } IAudioRendererWaveOutVtbl;

    interface IAudioRendererWaveOut
    {
        CONST_VTBL struct IAudioRendererWaveOutVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAudioRendererWaveOut_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAudioRendererWaveOut_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAudioRendererWaveOut_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAudioRendererWaveOut_SendMessage(This,uMsg,dw1,dw2)	\
    ( (This)->lpVtbl -> SendMessage(This,uMsg,dw1,dw2) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAudioRendererWaveOut_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_axcore_0000_0020 */
/* [local] */ 

typedef IMediaSeeking *PMEDIASEEKING;

enum tagAM_MEDIAEVENT_FLAGS
{
    AM_MEDIAEVENT_NONOTIFY = 0x01
};
typedef /* [public][public] */ struct __MIDL___MIDL_itf_axcore_0000_0020_0001
    {
    CLSID clsid;
    WCHAR achName[ 128 ];
    BOOL bDMO;
    } 	FilterCreationInfo;



extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0020_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_axcore_0000_0020_v0_0_s_ifspec;

#ifndef __IFilterGraphCache_INTERFACE_DEFINED__
#define __IFilterGraphCache_INTERFACE_DEFINED__

/* interface IFilterGraphCache */
/* [unique][uuid][object] */ 


EXTERN_C const IID IID_IFilterGraphCache;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("1D8C7CBF-F1D4-461b-B737-7384EA690710")
    IFilterGraphCache : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE ClearGraphCache( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE AddFiltersToGraphCache( 
            /* [in] */ IBaseFilter *pSourceFilter,
            /* [in] */ WCHAR *pExtension) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetFiltersFromGraphCache( 
            /* [in] */ GUID *pType,
            /* [in] */ GUID *pSubtype,
            /* [in] */ WCHAR *pExtension,
            /* [out] */ FilterCreationInfo *pFilterInfo,
            /* [out][in] */ DWORD *pcFilterInfo) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IFilterGraphCacheVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IFilterGraphCache * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IFilterGraphCache * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IFilterGraphCache * This);
        
        HRESULT ( STDMETHODCALLTYPE *ClearGraphCache )( 
            IFilterGraphCache * This);
        
        HRESULT ( STDMETHODCALLTYPE *AddFiltersToGraphCache )( 
            IFilterGraphCache * This,
            /* [in] */ IBaseFilter *pSourceFilter,
            /* [in] */ WCHAR *pExtension);
        
        HRESULT ( STDMETHODCALLTYPE *GetFiltersFromGraphCache )( 
            IFilterGraphCache * This,
            /* [in] */ GUID *pType,
            /* [in] */ GUID *pSubtype,
            /* [in] */ WCHAR *pExtension,
            /* [out] */ FilterCreationInfo *pFilterInfo,
            /* [out][in] */ DWORD *pcFilterInfo);
        
        END_INTERFACE
    } IFilterGraphCacheVtbl;

    interface IFilterGraphCache
    {
        CONST_VTBL struct IFilterGraphCacheVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IFilterGraphCache_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IFilterGraphCache_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IFilterGraphCache_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IFilterGraphCache_ClearGraphCache(This)	\
    ( (This)->lpVtbl -> ClearGraphCache(This) ) 

#define IFilterGraphCache_AddFiltersToGraphCache(This,pSourceFilter,pExtension)	\
    ( (This)->lpVtbl -> AddFiltersToGraphCache(This,pSourceFilter,pExtension) ) 

#define IFilterGraphCache_GetFiltersFromGraphCache(This,pType,pSubtype,pExtension,pFilterInfo,pcFilterInfo)	\
    ( (This)->lpVtbl -> GetFiltersFromGraphCache(This,pType,pSubtype,pExtension,pFilterInfo,pcFilterInfo) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IFilterGraphCache_INTERFACE_DEFINED__ */


/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


