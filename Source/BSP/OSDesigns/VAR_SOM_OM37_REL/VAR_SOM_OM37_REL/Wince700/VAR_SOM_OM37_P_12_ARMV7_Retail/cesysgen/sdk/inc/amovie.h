

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0543 */
/* Compiler settings for amovie.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __amovie_h__
#define __amovie_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IActiveMovie_FWD_DEFINED__
#define __IActiveMovie_FWD_DEFINED__
typedef interface IActiveMovie IActiveMovie;
#endif 	/* __IActiveMovie_FWD_DEFINED__ */


#ifndef __IActiveMovie2_FWD_DEFINED__
#define __IActiveMovie2_FWD_DEFINED__
typedef interface IActiveMovie2 IActiveMovie2;
#endif 	/* __IActiveMovie2_FWD_DEFINED__ */


#ifndef __IActiveMovie3_FWD_DEFINED__
#define __IActiveMovie3_FWD_DEFINED__
typedef interface IActiveMovie3 IActiveMovie3;
#endif 	/* __IActiveMovie3_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"
#include "enums.h"

#ifdef __cplusplus
extern "C"{
#endif 


/* interface __MIDL_itf_amovie_0000_0000 */
/* [local] */ 

typedef /* [public] */ 
enum WindowSizeConstants
    {	amvOriginalSize	= 0,
	amvDoubleOriginalSize	= ( amvOriginalSize + 1 ) ,
	amvOneSixteenthScreen	= ( amvDoubleOriginalSize + 1 ) ,
	amvOneFourthScreen	= ( amvOneSixteenthScreen + 1 ) ,
	amvOneHalfScreen	= ( amvOneFourthScreen + 1 ) 
    } 	WindowSizeConstants;

typedef /* [public] */ 
enum StateConstants
    {	amvNotLoaded	= -1,
	amvStopped	= ( amvNotLoaded + 1 ) ,
	amvPaused	= ( amvStopped + 1 ) ,
	amvRunning	= ( amvPaused + 1 ) 
    } 	StateConstants;

typedef /* [public] */ 
enum DisplayModeConstants
    {	amvTime	= 0,
	amvFrames	= ( amvTime + 1 ) 
    } 	DisplayModeConstants;

typedef /* [public] */ 
enum AppearanceConstants
    {	amvFlat	= 0,
	amv3D	= ( amvFlat + 1 ) 
    } 	AppearanceConstants;

typedef /* [public] */ 
enum BorderStyleConstants
    {	amvNone	= 0,
	amvFixedSingle	= ( amvNone + 1 ) 
    } 	BorderStyleConstants;



extern RPC_IF_HANDLE __MIDL_itf_amovie_0000_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_amovie_0000_0000_v0_0_s_ifspec;

#ifndef __IActiveMovie_INTERFACE_DEFINED__
#define __IActiveMovie_INTERFACE_DEFINED__

/* interface IActiveMovie */
/* [unique][hidden][dual][uuid][object] */ 


EXTERN_C const IID IID_IActiveMovie;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("05589fa2-c356-11ce-bf01-00aa0055595a")
    IActiveMovie : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE AboutBox( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Run( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Pause( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Stop( void) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ImageSourceWidth( 
            /* [retval][out] */ long *pWidth) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ImageSourceHeight( 
            /* [retval][out] */ long *pHeight) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Author( 
            /* [retval][out] */ BSTR *pbstrAuthor) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Title( 
            /* [retval][out] */ BSTR *pbstrTitle) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Copyright( 
            /* [retval][out] */ BSTR *pbstrCopyright) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Description( 
            /* [retval][out] */ BSTR *pbstrDescription) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Rating( 
            /* [retval][out] */ BSTR *pbstrRating) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_FileName( 
            /* [retval][out] */ BSTR *pbstrFileName) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_FileName( 
            /* [in] */ BSTR bstrFileName) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Duration( 
            /* [retval][out] */ double *pValue) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CurrentPosition( 
            /* [retval][out] */ double *pValue) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_CurrentPosition( 
            /* [in] */ double Value) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_PlayCount( 
            /* [retval][out] */ long *pPlayCount) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_PlayCount( 
            /* [in] */ long PlayCount) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SelectionStart( 
            /* [retval][out] */ double *pValue) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_SelectionStart( 
            /* [in] */ double Value) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SelectionEnd( 
            /* [retval][out] */ double *pValue) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_SelectionEnd( 
            /* [in] */ double Value) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CurrentState( 
            /* [retval][out] */ StateConstants *pState) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Rate( 
            /* [retval][out] */ double *pValue) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_Rate( 
            /* [in] */ double Value) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Volume( 
            /* [retval][out] */ long *pValue) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_Volume( 
            /* [in] */ long Value) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Balance( 
            /* [retval][out] */ long *pValue) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_Balance( 
            /* [in] */ long Value) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_EnableContextMenu( 
            /* [retval][out] */ VARIANT_BOOL *pEnable) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_EnableContextMenu( 
            /* [in] */ VARIANT_BOOL Enable) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ShowDisplay( 
            /* [retval][out] */ VARIANT_BOOL *Show) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_ShowDisplay( 
            /* [in] */ VARIANT_BOOL Show) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ShowControls( 
            /* [retval][out] */ VARIANT_BOOL *Show) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_ShowControls( 
            /* [in] */ VARIANT_BOOL Show) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ShowPositionControls( 
            /* [retval][out] */ VARIANT_BOOL *Show) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_ShowPositionControls( 
            /* [in] */ VARIANT_BOOL Show) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ShowSelectionControls( 
            /* [retval][out] */ VARIANT_BOOL *Show) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_ShowSelectionControls( 
            /* [in] */ VARIANT_BOOL Show) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ShowTracker( 
            /* [retval][out] */ VARIANT_BOOL *Show) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_ShowTracker( 
            /* [in] */ VARIANT_BOOL Show) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_EnablePositionControls( 
            /* [retval][out] */ VARIANT_BOOL *Enable) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_EnablePositionControls( 
            /* [in] */ VARIANT_BOOL Enable) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_EnableSelectionControls( 
            /* [retval][out] */ VARIANT_BOOL *Enable) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_EnableSelectionControls( 
            /* [in] */ VARIANT_BOOL Enable) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_EnableTracker( 
            /* [retval][out] */ VARIANT_BOOL *Enable) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_EnableTracker( 
            /* [in] */ VARIANT_BOOL Enable) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_AllowHideDisplay( 
            /* [retval][out] */ VARIANT_BOOL *Show) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_AllowHideDisplay( 
            /* [in] */ VARIANT_BOOL Show) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_AllowHideControls( 
            /* [retval][out] */ VARIANT_BOOL *Show) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_AllowHideControls( 
            /* [in] */ VARIANT_BOOL Show) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_DisplayMode( 
            /* [retval][out] */ DisplayModeConstants *pValue) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_DisplayMode( 
            /* [in] */ DisplayModeConstants Value) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_AllowChangeDisplayMode( 
            /* [retval][out] */ VARIANT_BOOL *fAllow) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_AllowChangeDisplayMode( 
            /* [in] */ VARIANT_BOOL Allow) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_FilterGraph( 
            /* [retval][out] */ IUnknown **ppFilterGraph) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_FilterGraph( 
            /* [in] */ IUnknown *pFilterGraph) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_FilterGraphDispatch( 
            /* [retval][out] */ IDispatch **pDispatch) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_DisplayForeColor( 
            /* [retval][out] */ OLE_COLOR *ForeColor) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_DisplayForeColor( 
            /* [in] */ OLE_COLOR ForeColor) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_DisplayBackColor( 
            /* [retval][out] */ OLE_COLOR *BackColor) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_DisplayBackColor( 
            /* [in] */ OLE_COLOR BackColor) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_MovieWindowSize( 
            /* [retval][out] */ WindowSizeConstants *WindowSize) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_MovieWindowSize( 
            /* [in] */ WindowSizeConstants WindowSize) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_FullScreenMode( 
            /* [retval][out] */ VARIANT_BOOL *pEnable) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_FullScreenMode( 
            /* [in] */ VARIANT_BOOL Enable) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_AutoStart( 
            /* [retval][out] */ VARIANT_BOOL *pEnable) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_AutoStart( 
            /* [in] */ VARIANT_BOOL Enable) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_AutoRewind( 
            /* [retval][out] */ VARIANT_BOOL *pEnable) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_AutoRewind( 
            /* [in] */ VARIANT_BOOL Enable) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_hWnd( 
            /* [retval][out] */ long *hWnd) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Appearance( 
            /* [retval][out] */ AppearanceConstants *pAppearance) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_Appearance( 
            /* [in] */ AppearanceConstants Appearance) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_BorderStyle( 
            /* [retval][out] */ BorderStyleConstants *pBorderStyle) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_BorderStyle( 
            /* [in] */ BorderStyleConstants BorderStyle) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Enabled( 
            /* [retval][out] */ VARIANT_BOOL *pEnabled) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_Enabled( 
            /* [in] */ VARIANT_BOOL Enabled) = 0;
        
        virtual /* [hidden][propget][id] */ HRESULT STDMETHODCALLTYPE get_Info( 
            /* [retval][out] */ long *ppInfo) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IActiveMovieVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IActiveMovie * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IActiveMovie * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IActiveMovie * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IActiveMovie * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IActiveMovie * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IActiveMovie * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IActiveMovie * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *AboutBox )( 
            IActiveMovie * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Run )( 
            IActiveMovie * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Pause )( 
            IActiveMovie * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Stop )( 
            IActiveMovie * This);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ImageSourceWidth )( 
            IActiveMovie * This,
            /* [retval][out] */ long *pWidth);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ImageSourceHeight )( 
            IActiveMovie * This,
            /* [retval][out] */ long *pHeight);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Author )( 
            IActiveMovie * This,
            /* [retval][out] */ BSTR *pbstrAuthor);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Title )( 
            IActiveMovie * This,
            /* [retval][out] */ BSTR *pbstrTitle);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Copyright )( 
            IActiveMovie * This,
            /* [retval][out] */ BSTR *pbstrCopyright);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Description )( 
            IActiveMovie * This,
            /* [retval][out] */ BSTR *pbstrDescription);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Rating )( 
            IActiveMovie * This,
            /* [retval][out] */ BSTR *pbstrRating);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_FileName )( 
            IActiveMovie * This,
            /* [retval][out] */ BSTR *pbstrFileName);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_FileName )( 
            IActiveMovie * This,
            /* [in] */ BSTR bstrFileName);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Duration )( 
            IActiveMovie * This,
            /* [retval][out] */ double *pValue);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentPosition )( 
            IActiveMovie * This,
            /* [retval][out] */ double *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_CurrentPosition )( 
            IActiveMovie * This,
            /* [in] */ double Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlayCount )( 
            IActiveMovie * This,
            /* [retval][out] */ long *pPlayCount);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_PlayCount )( 
            IActiveMovie * This,
            /* [in] */ long PlayCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SelectionStart )( 
            IActiveMovie * This,
            /* [retval][out] */ double *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SelectionStart )( 
            IActiveMovie * This,
            /* [in] */ double Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SelectionEnd )( 
            IActiveMovie * This,
            /* [retval][out] */ double *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SelectionEnd )( 
            IActiveMovie * This,
            /* [in] */ double Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentState )( 
            IActiveMovie * This,
            /* [retval][out] */ StateConstants *pState);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Rate )( 
            IActiveMovie * This,
            /* [retval][out] */ double *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Rate )( 
            IActiveMovie * This,
            /* [in] */ double Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Volume )( 
            IActiveMovie * This,
            /* [retval][out] */ long *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Volume )( 
            IActiveMovie * This,
            /* [in] */ long Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Balance )( 
            IActiveMovie * This,
            /* [retval][out] */ long *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Balance )( 
            IActiveMovie * This,
            /* [in] */ long Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableContextMenu )( 
            IActiveMovie * This,
            /* [retval][out] */ VARIANT_BOOL *pEnable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableContextMenu )( 
            IActiveMovie * This,
            /* [in] */ VARIANT_BOOL Enable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowDisplay )( 
            IActiveMovie * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowDisplay )( 
            IActiveMovie * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowControls )( 
            IActiveMovie * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowControls )( 
            IActiveMovie * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowPositionControls )( 
            IActiveMovie * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowPositionControls )( 
            IActiveMovie * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowSelectionControls )( 
            IActiveMovie * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowSelectionControls )( 
            IActiveMovie * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowTracker )( 
            IActiveMovie * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowTracker )( 
            IActiveMovie * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnablePositionControls )( 
            IActiveMovie * This,
            /* [retval][out] */ VARIANT_BOOL *Enable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnablePositionControls )( 
            IActiveMovie * This,
            /* [in] */ VARIANT_BOOL Enable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableSelectionControls )( 
            IActiveMovie * This,
            /* [retval][out] */ VARIANT_BOOL *Enable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableSelectionControls )( 
            IActiveMovie * This,
            /* [in] */ VARIANT_BOOL Enable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableTracker )( 
            IActiveMovie * This,
            /* [retval][out] */ VARIANT_BOOL *Enable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableTracker )( 
            IActiveMovie * This,
            /* [in] */ VARIANT_BOOL Enable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AllowHideDisplay )( 
            IActiveMovie * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AllowHideDisplay )( 
            IActiveMovie * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AllowHideControls )( 
            IActiveMovie * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AllowHideControls )( 
            IActiveMovie * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_DisplayMode )( 
            IActiveMovie * This,
            /* [retval][out] */ DisplayModeConstants *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_DisplayMode )( 
            IActiveMovie * This,
            /* [in] */ DisplayModeConstants Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AllowChangeDisplayMode )( 
            IActiveMovie * This,
            /* [retval][out] */ VARIANT_BOOL *fAllow);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AllowChangeDisplayMode )( 
            IActiveMovie * This,
            /* [in] */ VARIANT_BOOL Allow);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_FilterGraph )( 
            IActiveMovie * This,
            /* [retval][out] */ IUnknown **ppFilterGraph);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_FilterGraph )( 
            IActiveMovie * This,
            /* [in] */ IUnknown *pFilterGraph);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_FilterGraphDispatch )( 
            IActiveMovie * This,
            /* [retval][out] */ IDispatch **pDispatch);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DisplayForeColor )( 
            IActiveMovie * This,
            /* [retval][out] */ OLE_COLOR *ForeColor);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DisplayForeColor )( 
            IActiveMovie * This,
            /* [in] */ OLE_COLOR ForeColor);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_DisplayBackColor )( 
            IActiveMovie * This,
            /* [retval][out] */ OLE_COLOR *BackColor);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_DisplayBackColor )( 
            IActiveMovie * This,
            /* [in] */ OLE_COLOR BackColor);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_MovieWindowSize )( 
            IActiveMovie * This,
            /* [retval][out] */ WindowSizeConstants *WindowSize);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_MovieWindowSize )( 
            IActiveMovie * This,
            /* [in] */ WindowSizeConstants WindowSize);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_FullScreenMode )( 
            IActiveMovie * This,
            /* [retval][out] */ VARIANT_BOOL *pEnable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_FullScreenMode )( 
            IActiveMovie * This,
            /* [in] */ VARIANT_BOOL Enable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AutoStart )( 
            IActiveMovie * This,
            /* [retval][out] */ VARIANT_BOOL *pEnable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AutoStart )( 
            IActiveMovie * This,
            /* [in] */ VARIANT_BOOL Enable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AutoRewind )( 
            IActiveMovie * This,
            /* [retval][out] */ VARIANT_BOOL *pEnable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AutoRewind )( 
            IActiveMovie * This,
            /* [in] */ VARIANT_BOOL Enable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_hWnd )( 
            IActiveMovie * This,
            /* [retval][out] */ long *hWnd);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Appearance )( 
            IActiveMovie * This,
            /* [retval][out] */ AppearanceConstants *pAppearance);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Appearance )( 
            IActiveMovie * This,
            /* [in] */ AppearanceConstants Appearance);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_BorderStyle )( 
            IActiveMovie * This,
            /* [retval][out] */ BorderStyleConstants *pBorderStyle);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_BorderStyle )( 
            IActiveMovie * This,
            /* [in] */ BorderStyleConstants BorderStyle);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Enabled )( 
            IActiveMovie * This,
            /* [retval][out] */ VARIANT_BOOL *pEnabled);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Enabled )( 
            IActiveMovie * This,
            /* [in] */ VARIANT_BOOL Enabled);
        
        /* [hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Info )( 
            IActiveMovie * This,
            /* [retval][out] */ long *ppInfo);
        
        END_INTERFACE
    } IActiveMovieVtbl;

    interface IActiveMovie
    {
        CONST_VTBL struct IActiveMovieVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IActiveMovie_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IActiveMovie_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IActiveMovie_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IActiveMovie_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IActiveMovie_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IActiveMovie_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IActiveMovie_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IActiveMovie_AboutBox(This)	\
    ( (This)->lpVtbl -> AboutBox(This) ) 

#define IActiveMovie_Run(This)	\
    ( (This)->lpVtbl -> Run(This) ) 

#define IActiveMovie_Pause(This)	\
    ( (This)->lpVtbl -> Pause(This) ) 

#define IActiveMovie_Stop(This)	\
    ( (This)->lpVtbl -> Stop(This) ) 

#define IActiveMovie_get_ImageSourceWidth(This,pWidth)	\
    ( (This)->lpVtbl -> get_ImageSourceWidth(This,pWidth) ) 

#define IActiveMovie_get_ImageSourceHeight(This,pHeight)	\
    ( (This)->lpVtbl -> get_ImageSourceHeight(This,pHeight) ) 

#define IActiveMovie_get_Author(This,pbstrAuthor)	\
    ( (This)->lpVtbl -> get_Author(This,pbstrAuthor) ) 

#define IActiveMovie_get_Title(This,pbstrTitle)	\
    ( (This)->lpVtbl -> get_Title(This,pbstrTitle) ) 

#define IActiveMovie_get_Copyright(This,pbstrCopyright)	\
    ( (This)->lpVtbl -> get_Copyright(This,pbstrCopyright) ) 

#define IActiveMovie_get_Description(This,pbstrDescription)	\
    ( (This)->lpVtbl -> get_Description(This,pbstrDescription) ) 

#define IActiveMovie_get_Rating(This,pbstrRating)	\
    ( (This)->lpVtbl -> get_Rating(This,pbstrRating) ) 

#define IActiveMovie_get_FileName(This,pbstrFileName)	\
    ( (This)->lpVtbl -> get_FileName(This,pbstrFileName) ) 

#define IActiveMovie_put_FileName(This,bstrFileName)	\
    ( (This)->lpVtbl -> put_FileName(This,bstrFileName) ) 

#define IActiveMovie_get_Duration(This,pValue)	\
    ( (This)->lpVtbl -> get_Duration(This,pValue) ) 

#define IActiveMovie_get_CurrentPosition(This,pValue)	\
    ( (This)->lpVtbl -> get_CurrentPosition(This,pValue) ) 

#define IActiveMovie_put_CurrentPosition(This,Value)	\
    ( (This)->lpVtbl -> put_CurrentPosition(This,Value) ) 

#define IActiveMovie_get_PlayCount(This,pPlayCount)	\
    ( (This)->lpVtbl -> get_PlayCount(This,pPlayCount) ) 

#define IActiveMovie_put_PlayCount(This,PlayCount)	\
    ( (This)->lpVtbl -> put_PlayCount(This,PlayCount) ) 

#define IActiveMovie_get_SelectionStart(This,pValue)	\
    ( (This)->lpVtbl -> get_SelectionStart(This,pValue) ) 

#define IActiveMovie_put_SelectionStart(This,Value)	\
    ( (This)->lpVtbl -> put_SelectionStart(This,Value) ) 

#define IActiveMovie_get_SelectionEnd(This,pValue)	\
    ( (This)->lpVtbl -> get_SelectionEnd(This,pValue) ) 

#define IActiveMovie_put_SelectionEnd(This,Value)	\
    ( (This)->lpVtbl -> put_SelectionEnd(This,Value) ) 

#define IActiveMovie_get_CurrentState(This,pState)	\
    ( (This)->lpVtbl -> get_CurrentState(This,pState) ) 

#define IActiveMovie_get_Rate(This,pValue)	\
    ( (This)->lpVtbl -> get_Rate(This,pValue) ) 

#define IActiveMovie_put_Rate(This,Value)	\
    ( (This)->lpVtbl -> put_Rate(This,Value) ) 

#define IActiveMovie_get_Volume(This,pValue)	\
    ( (This)->lpVtbl -> get_Volume(This,pValue) ) 

#define IActiveMovie_put_Volume(This,Value)	\
    ( (This)->lpVtbl -> put_Volume(This,Value) ) 

#define IActiveMovie_get_Balance(This,pValue)	\
    ( (This)->lpVtbl -> get_Balance(This,pValue) ) 

#define IActiveMovie_put_Balance(This,Value)	\
    ( (This)->lpVtbl -> put_Balance(This,Value) ) 

#define IActiveMovie_get_EnableContextMenu(This,pEnable)	\
    ( (This)->lpVtbl -> get_EnableContextMenu(This,pEnable) ) 

#define IActiveMovie_put_EnableContextMenu(This,Enable)	\
    ( (This)->lpVtbl -> put_EnableContextMenu(This,Enable) ) 

#define IActiveMovie_get_ShowDisplay(This,Show)	\
    ( (This)->lpVtbl -> get_ShowDisplay(This,Show) ) 

#define IActiveMovie_put_ShowDisplay(This,Show)	\
    ( (This)->lpVtbl -> put_ShowDisplay(This,Show) ) 

#define IActiveMovie_get_ShowControls(This,Show)	\
    ( (This)->lpVtbl -> get_ShowControls(This,Show) ) 

#define IActiveMovie_put_ShowControls(This,Show)	\
    ( (This)->lpVtbl -> put_ShowControls(This,Show) ) 

#define IActiveMovie_get_ShowPositionControls(This,Show)	\
    ( (This)->lpVtbl -> get_ShowPositionControls(This,Show) ) 

#define IActiveMovie_put_ShowPositionControls(This,Show)	\
    ( (This)->lpVtbl -> put_ShowPositionControls(This,Show) ) 

#define IActiveMovie_get_ShowSelectionControls(This,Show)	\
    ( (This)->lpVtbl -> get_ShowSelectionControls(This,Show) ) 

#define IActiveMovie_put_ShowSelectionControls(This,Show)	\
    ( (This)->lpVtbl -> put_ShowSelectionControls(This,Show) ) 

#define IActiveMovie_get_ShowTracker(This,Show)	\
    ( (This)->lpVtbl -> get_ShowTracker(This,Show) ) 

#define IActiveMovie_put_ShowTracker(This,Show)	\
    ( (This)->lpVtbl -> put_ShowTracker(This,Show) ) 

#define IActiveMovie_get_EnablePositionControls(This,Enable)	\
    ( (This)->lpVtbl -> get_EnablePositionControls(This,Enable) ) 

#define IActiveMovie_put_EnablePositionControls(This,Enable)	\
    ( (This)->lpVtbl -> put_EnablePositionControls(This,Enable) ) 

#define IActiveMovie_get_EnableSelectionControls(This,Enable)	\
    ( (This)->lpVtbl -> get_EnableSelectionControls(This,Enable) ) 

#define IActiveMovie_put_EnableSelectionControls(This,Enable)	\
    ( (This)->lpVtbl -> put_EnableSelectionControls(This,Enable) ) 

#define IActiveMovie_get_EnableTracker(This,Enable)	\
    ( (This)->lpVtbl -> get_EnableTracker(This,Enable) ) 

#define IActiveMovie_put_EnableTracker(This,Enable)	\
    ( (This)->lpVtbl -> put_EnableTracker(This,Enable) ) 

#define IActiveMovie_get_AllowHideDisplay(This,Show)	\
    ( (This)->lpVtbl -> get_AllowHideDisplay(This,Show) ) 

#define IActiveMovie_put_AllowHideDisplay(This,Show)	\
    ( (This)->lpVtbl -> put_AllowHideDisplay(This,Show) ) 

#define IActiveMovie_get_AllowHideControls(This,Show)	\
    ( (This)->lpVtbl -> get_AllowHideControls(This,Show) ) 

#define IActiveMovie_put_AllowHideControls(This,Show)	\
    ( (This)->lpVtbl -> put_AllowHideControls(This,Show) ) 

#define IActiveMovie_get_DisplayMode(This,pValue)	\
    ( (This)->lpVtbl -> get_DisplayMode(This,pValue) ) 

#define IActiveMovie_put_DisplayMode(This,Value)	\
    ( (This)->lpVtbl -> put_DisplayMode(This,Value) ) 

#define IActiveMovie_get_AllowChangeDisplayMode(This,fAllow)	\
    ( (This)->lpVtbl -> get_AllowChangeDisplayMode(This,fAllow) ) 

#define IActiveMovie_put_AllowChangeDisplayMode(This,Allow)	\
    ( (This)->lpVtbl -> put_AllowChangeDisplayMode(This,Allow) ) 

#define IActiveMovie_get_FilterGraph(This,ppFilterGraph)	\
    ( (This)->lpVtbl -> get_FilterGraph(This,ppFilterGraph) ) 

#define IActiveMovie_put_FilterGraph(This,pFilterGraph)	\
    ( (This)->lpVtbl -> put_FilterGraph(This,pFilterGraph) ) 

#define IActiveMovie_get_FilterGraphDispatch(This,pDispatch)	\
    ( (This)->lpVtbl -> get_FilterGraphDispatch(This,pDispatch) ) 

#define IActiveMovie_get_DisplayForeColor(This,ForeColor)	\
    ( (This)->lpVtbl -> get_DisplayForeColor(This,ForeColor) ) 

#define IActiveMovie_put_DisplayForeColor(This,ForeColor)	\
    ( (This)->lpVtbl -> put_DisplayForeColor(This,ForeColor) ) 

#define IActiveMovie_get_DisplayBackColor(This,BackColor)	\
    ( (This)->lpVtbl -> get_DisplayBackColor(This,BackColor) ) 

#define IActiveMovie_put_DisplayBackColor(This,BackColor)	\
    ( (This)->lpVtbl -> put_DisplayBackColor(This,BackColor) ) 

#define IActiveMovie_get_MovieWindowSize(This,WindowSize)	\
    ( (This)->lpVtbl -> get_MovieWindowSize(This,WindowSize) ) 

#define IActiveMovie_put_MovieWindowSize(This,WindowSize)	\
    ( (This)->lpVtbl -> put_MovieWindowSize(This,WindowSize) ) 

#define IActiveMovie_get_FullScreenMode(This,pEnable)	\
    ( (This)->lpVtbl -> get_FullScreenMode(This,pEnable) ) 

#define IActiveMovie_put_FullScreenMode(This,Enable)	\
    ( (This)->lpVtbl -> put_FullScreenMode(This,Enable) ) 

#define IActiveMovie_get_AutoStart(This,pEnable)	\
    ( (This)->lpVtbl -> get_AutoStart(This,pEnable) ) 

#define IActiveMovie_put_AutoStart(This,Enable)	\
    ( (This)->lpVtbl -> put_AutoStart(This,Enable) ) 

#define IActiveMovie_get_AutoRewind(This,pEnable)	\
    ( (This)->lpVtbl -> get_AutoRewind(This,pEnable) ) 

#define IActiveMovie_put_AutoRewind(This,Enable)	\
    ( (This)->lpVtbl -> put_AutoRewind(This,Enable) ) 

#define IActiveMovie_get_hWnd(This,hWnd)	\
    ( (This)->lpVtbl -> get_hWnd(This,hWnd) ) 

#define IActiveMovie_get_Appearance(This,pAppearance)	\
    ( (This)->lpVtbl -> get_Appearance(This,pAppearance) ) 

#define IActiveMovie_put_Appearance(This,Appearance)	\
    ( (This)->lpVtbl -> put_Appearance(This,Appearance) ) 

#define IActiveMovie_get_BorderStyle(This,pBorderStyle)	\
    ( (This)->lpVtbl -> get_BorderStyle(This,pBorderStyle) ) 

#define IActiveMovie_put_BorderStyle(This,BorderStyle)	\
    ( (This)->lpVtbl -> put_BorderStyle(This,BorderStyle) ) 

#define IActiveMovie_get_Enabled(This,pEnabled)	\
    ( (This)->lpVtbl -> get_Enabled(This,pEnabled) ) 

#define IActiveMovie_put_Enabled(This,Enabled)	\
    ( (This)->lpVtbl -> put_Enabled(This,Enabled) ) 

#define IActiveMovie_get_Info(This,ppInfo)	\
    ( (This)->lpVtbl -> get_Info(This,ppInfo) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IActiveMovie_INTERFACE_DEFINED__ */


#ifndef __IActiveMovie2_INTERFACE_DEFINED__
#define __IActiveMovie2_INTERFACE_DEFINED__

/* interface IActiveMovie2 */
/* [unique][hidden][dual][uuid][object] */ 


EXTERN_C const IID IID_IActiveMovie2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("B6CD6554-E9CB-11d0-821F-00A0C91F9CA0")
    IActiveMovie2 : public IActiveMovie
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE IsSoundCardEnabled( 
            /* [retval][out] */ VARIANT_BOOL *pbSoundCard) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ReadyState( 
            /* [retval][out] */ ReadyStateConstants *pValue) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IActiveMovie2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IActiveMovie2 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IActiveMovie2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IActiveMovie2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IActiveMovie2 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IActiveMovie2 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IActiveMovie2 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IActiveMovie2 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *AboutBox )( 
            IActiveMovie2 * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Run )( 
            IActiveMovie2 * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Pause )( 
            IActiveMovie2 * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Stop )( 
            IActiveMovie2 * This);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ImageSourceWidth )( 
            IActiveMovie2 * This,
            /* [retval][out] */ long *pWidth);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ImageSourceHeight )( 
            IActiveMovie2 * This,
            /* [retval][out] */ long *pHeight);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Author )( 
            IActiveMovie2 * This,
            /* [retval][out] */ BSTR *pbstrAuthor);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Title )( 
            IActiveMovie2 * This,
            /* [retval][out] */ BSTR *pbstrTitle);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Copyright )( 
            IActiveMovie2 * This,
            /* [retval][out] */ BSTR *pbstrCopyright);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Description )( 
            IActiveMovie2 * This,
            /* [retval][out] */ BSTR *pbstrDescription);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Rating )( 
            IActiveMovie2 * This,
            /* [retval][out] */ BSTR *pbstrRating);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_FileName )( 
            IActiveMovie2 * This,
            /* [retval][out] */ BSTR *pbstrFileName);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_FileName )( 
            IActiveMovie2 * This,
            /* [in] */ BSTR bstrFileName);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Duration )( 
            IActiveMovie2 * This,
            /* [retval][out] */ double *pValue);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentPosition )( 
            IActiveMovie2 * This,
            /* [retval][out] */ double *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_CurrentPosition )( 
            IActiveMovie2 * This,
            /* [in] */ double Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlayCount )( 
            IActiveMovie2 * This,
            /* [retval][out] */ long *pPlayCount);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_PlayCount )( 
            IActiveMovie2 * This,
            /* [in] */ long PlayCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SelectionStart )( 
            IActiveMovie2 * This,
            /* [retval][out] */ double *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SelectionStart )( 
            IActiveMovie2 * This,
            /* [in] */ double Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SelectionEnd )( 
            IActiveMovie2 * This,
            /* [retval][out] */ double *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SelectionEnd )( 
            IActiveMovie2 * This,
            /* [in] */ double Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentState )( 
            IActiveMovie2 * This,
            /* [retval][out] */ StateConstants *pState);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Rate )( 
            IActiveMovie2 * This,
            /* [retval][out] */ double *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Rate )( 
            IActiveMovie2 * This,
            /* [in] */ double Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Volume )( 
            IActiveMovie2 * This,
            /* [retval][out] */ long *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Volume )( 
            IActiveMovie2 * This,
            /* [in] */ long Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Balance )( 
            IActiveMovie2 * This,
            /* [retval][out] */ long *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Balance )( 
            IActiveMovie2 * This,
            /* [in] */ long Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableContextMenu )( 
            IActiveMovie2 * This,
            /* [retval][out] */ VARIANT_BOOL *pEnable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableContextMenu )( 
            IActiveMovie2 * This,
            /* [in] */ VARIANT_BOOL Enable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowDisplay )( 
            IActiveMovie2 * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowDisplay )( 
            IActiveMovie2 * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowControls )( 
            IActiveMovie2 * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowControls )( 
            IActiveMovie2 * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowPositionControls )( 
            IActiveMovie2 * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowPositionControls )( 
            IActiveMovie2 * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowSelectionControls )( 
            IActiveMovie2 * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowSelectionControls )( 
            IActiveMovie2 * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowTracker )( 
            IActiveMovie2 * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowTracker )( 
            IActiveMovie2 * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnablePositionControls )( 
            IActiveMovie2 * This,
            /* [retval][out] */ VARIANT_BOOL *Enable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnablePositionControls )( 
            IActiveMovie2 * This,
            /* [in] */ VARIANT_BOOL Enable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableSelectionControls )( 
            IActiveMovie2 * This,
            /* [retval][out] */ VARIANT_BOOL *Enable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableSelectionControls )( 
            IActiveMovie2 * This,
            /* [in] */ VARIANT_BOOL Enable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableTracker )( 
            IActiveMovie2 * This,
            /* [retval][out] */ VARIANT_BOOL *Enable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableTracker )( 
            IActiveMovie2 * This,
            /* [in] */ VARIANT_BOOL Enable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AllowHideDisplay )( 
            IActiveMovie2 * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AllowHideDisplay )( 
            IActiveMovie2 * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AllowHideControls )( 
            IActiveMovie2 * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AllowHideControls )( 
            IActiveMovie2 * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_DisplayMode )( 
            IActiveMovie2 * This,
            /* [retval][out] */ DisplayModeConstants *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_DisplayMode )( 
            IActiveMovie2 * This,
            /* [in] */ DisplayModeConstants Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AllowChangeDisplayMode )( 
            IActiveMovie2 * This,
            /* [retval][out] */ VARIANT_BOOL *fAllow);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AllowChangeDisplayMode )( 
            IActiveMovie2 * This,
            /* [in] */ VARIANT_BOOL Allow);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_FilterGraph )( 
            IActiveMovie2 * This,
            /* [retval][out] */ IUnknown **ppFilterGraph);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_FilterGraph )( 
            IActiveMovie2 * This,
            /* [in] */ IUnknown *pFilterGraph);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_FilterGraphDispatch )( 
            IActiveMovie2 * This,
            /* [retval][out] */ IDispatch **pDispatch);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DisplayForeColor )( 
            IActiveMovie2 * This,
            /* [retval][out] */ OLE_COLOR *ForeColor);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DisplayForeColor )( 
            IActiveMovie2 * This,
            /* [in] */ OLE_COLOR ForeColor);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_DisplayBackColor )( 
            IActiveMovie2 * This,
            /* [retval][out] */ OLE_COLOR *BackColor);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_DisplayBackColor )( 
            IActiveMovie2 * This,
            /* [in] */ OLE_COLOR BackColor);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_MovieWindowSize )( 
            IActiveMovie2 * This,
            /* [retval][out] */ WindowSizeConstants *WindowSize);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_MovieWindowSize )( 
            IActiveMovie2 * This,
            /* [in] */ WindowSizeConstants WindowSize);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_FullScreenMode )( 
            IActiveMovie2 * This,
            /* [retval][out] */ VARIANT_BOOL *pEnable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_FullScreenMode )( 
            IActiveMovie2 * This,
            /* [in] */ VARIANT_BOOL Enable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AutoStart )( 
            IActiveMovie2 * This,
            /* [retval][out] */ VARIANT_BOOL *pEnable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AutoStart )( 
            IActiveMovie2 * This,
            /* [in] */ VARIANT_BOOL Enable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AutoRewind )( 
            IActiveMovie2 * This,
            /* [retval][out] */ VARIANT_BOOL *pEnable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AutoRewind )( 
            IActiveMovie2 * This,
            /* [in] */ VARIANT_BOOL Enable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_hWnd )( 
            IActiveMovie2 * This,
            /* [retval][out] */ long *hWnd);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Appearance )( 
            IActiveMovie2 * This,
            /* [retval][out] */ AppearanceConstants *pAppearance);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Appearance )( 
            IActiveMovie2 * This,
            /* [in] */ AppearanceConstants Appearance);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_BorderStyle )( 
            IActiveMovie2 * This,
            /* [retval][out] */ BorderStyleConstants *pBorderStyle);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_BorderStyle )( 
            IActiveMovie2 * This,
            /* [in] */ BorderStyleConstants BorderStyle);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Enabled )( 
            IActiveMovie2 * This,
            /* [retval][out] */ VARIANT_BOOL *pEnabled);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Enabled )( 
            IActiveMovie2 * This,
            /* [in] */ VARIANT_BOOL Enabled);
        
        /* [hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Info )( 
            IActiveMovie2 * This,
            /* [retval][out] */ long *ppInfo);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *IsSoundCardEnabled )( 
            IActiveMovie2 * This,
            /* [retval][out] */ VARIANT_BOOL *pbSoundCard);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ReadyState )( 
            IActiveMovie2 * This,
            /* [retval][out] */ ReadyStateConstants *pValue);
        
        END_INTERFACE
    } IActiveMovie2Vtbl;

    interface IActiveMovie2
    {
        CONST_VTBL struct IActiveMovie2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IActiveMovie2_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IActiveMovie2_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IActiveMovie2_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IActiveMovie2_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IActiveMovie2_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IActiveMovie2_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IActiveMovie2_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IActiveMovie2_AboutBox(This)	\
    ( (This)->lpVtbl -> AboutBox(This) ) 

#define IActiveMovie2_Run(This)	\
    ( (This)->lpVtbl -> Run(This) ) 

#define IActiveMovie2_Pause(This)	\
    ( (This)->lpVtbl -> Pause(This) ) 

#define IActiveMovie2_Stop(This)	\
    ( (This)->lpVtbl -> Stop(This) ) 

#define IActiveMovie2_get_ImageSourceWidth(This,pWidth)	\
    ( (This)->lpVtbl -> get_ImageSourceWidth(This,pWidth) ) 

#define IActiveMovie2_get_ImageSourceHeight(This,pHeight)	\
    ( (This)->lpVtbl -> get_ImageSourceHeight(This,pHeight) ) 

#define IActiveMovie2_get_Author(This,pbstrAuthor)	\
    ( (This)->lpVtbl -> get_Author(This,pbstrAuthor) ) 

#define IActiveMovie2_get_Title(This,pbstrTitle)	\
    ( (This)->lpVtbl -> get_Title(This,pbstrTitle) ) 

#define IActiveMovie2_get_Copyright(This,pbstrCopyright)	\
    ( (This)->lpVtbl -> get_Copyright(This,pbstrCopyright) ) 

#define IActiveMovie2_get_Description(This,pbstrDescription)	\
    ( (This)->lpVtbl -> get_Description(This,pbstrDescription) ) 

#define IActiveMovie2_get_Rating(This,pbstrRating)	\
    ( (This)->lpVtbl -> get_Rating(This,pbstrRating) ) 

#define IActiveMovie2_get_FileName(This,pbstrFileName)	\
    ( (This)->lpVtbl -> get_FileName(This,pbstrFileName) ) 

#define IActiveMovie2_put_FileName(This,bstrFileName)	\
    ( (This)->lpVtbl -> put_FileName(This,bstrFileName) ) 

#define IActiveMovie2_get_Duration(This,pValue)	\
    ( (This)->lpVtbl -> get_Duration(This,pValue) ) 

#define IActiveMovie2_get_CurrentPosition(This,pValue)	\
    ( (This)->lpVtbl -> get_CurrentPosition(This,pValue) ) 

#define IActiveMovie2_put_CurrentPosition(This,Value)	\
    ( (This)->lpVtbl -> put_CurrentPosition(This,Value) ) 

#define IActiveMovie2_get_PlayCount(This,pPlayCount)	\
    ( (This)->lpVtbl -> get_PlayCount(This,pPlayCount) ) 

#define IActiveMovie2_put_PlayCount(This,PlayCount)	\
    ( (This)->lpVtbl -> put_PlayCount(This,PlayCount) ) 

#define IActiveMovie2_get_SelectionStart(This,pValue)	\
    ( (This)->lpVtbl -> get_SelectionStart(This,pValue) ) 

#define IActiveMovie2_put_SelectionStart(This,Value)	\
    ( (This)->lpVtbl -> put_SelectionStart(This,Value) ) 

#define IActiveMovie2_get_SelectionEnd(This,pValue)	\
    ( (This)->lpVtbl -> get_SelectionEnd(This,pValue) ) 

#define IActiveMovie2_put_SelectionEnd(This,Value)	\
    ( (This)->lpVtbl -> put_SelectionEnd(This,Value) ) 

#define IActiveMovie2_get_CurrentState(This,pState)	\
    ( (This)->lpVtbl -> get_CurrentState(This,pState) ) 

#define IActiveMovie2_get_Rate(This,pValue)	\
    ( (This)->lpVtbl -> get_Rate(This,pValue) ) 

#define IActiveMovie2_put_Rate(This,Value)	\
    ( (This)->lpVtbl -> put_Rate(This,Value) ) 

#define IActiveMovie2_get_Volume(This,pValue)	\
    ( (This)->lpVtbl -> get_Volume(This,pValue) ) 

#define IActiveMovie2_put_Volume(This,Value)	\
    ( (This)->lpVtbl -> put_Volume(This,Value) ) 

#define IActiveMovie2_get_Balance(This,pValue)	\
    ( (This)->lpVtbl -> get_Balance(This,pValue) ) 

#define IActiveMovie2_put_Balance(This,Value)	\
    ( (This)->lpVtbl -> put_Balance(This,Value) ) 

#define IActiveMovie2_get_EnableContextMenu(This,pEnable)	\
    ( (This)->lpVtbl -> get_EnableContextMenu(This,pEnable) ) 

#define IActiveMovie2_put_EnableContextMenu(This,Enable)	\
    ( (This)->lpVtbl -> put_EnableContextMenu(This,Enable) ) 

#define IActiveMovie2_get_ShowDisplay(This,Show)	\
    ( (This)->lpVtbl -> get_ShowDisplay(This,Show) ) 

#define IActiveMovie2_put_ShowDisplay(This,Show)	\
    ( (This)->lpVtbl -> put_ShowDisplay(This,Show) ) 

#define IActiveMovie2_get_ShowControls(This,Show)	\
    ( (This)->lpVtbl -> get_ShowControls(This,Show) ) 

#define IActiveMovie2_put_ShowControls(This,Show)	\
    ( (This)->lpVtbl -> put_ShowControls(This,Show) ) 

#define IActiveMovie2_get_ShowPositionControls(This,Show)	\
    ( (This)->lpVtbl -> get_ShowPositionControls(This,Show) ) 

#define IActiveMovie2_put_ShowPositionControls(This,Show)	\
    ( (This)->lpVtbl -> put_ShowPositionControls(This,Show) ) 

#define IActiveMovie2_get_ShowSelectionControls(This,Show)	\
    ( (This)->lpVtbl -> get_ShowSelectionControls(This,Show) ) 

#define IActiveMovie2_put_ShowSelectionControls(This,Show)	\
    ( (This)->lpVtbl -> put_ShowSelectionControls(This,Show) ) 

#define IActiveMovie2_get_ShowTracker(This,Show)	\
    ( (This)->lpVtbl -> get_ShowTracker(This,Show) ) 

#define IActiveMovie2_put_ShowTracker(This,Show)	\
    ( (This)->lpVtbl -> put_ShowTracker(This,Show) ) 

#define IActiveMovie2_get_EnablePositionControls(This,Enable)	\
    ( (This)->lpVtbl -> get_EnablePositionControls(This,Enable) ) 

#define IActiveMovie2_put_EnablePositionControls(This,Enable)	\
    ( (This)->lpVtbl -> put_EnablePositionControls(This,Enable) ) 

#define IActiveMovie2_get_EnableSelectionControls(This,Enable)	\
    ( (This)->lpVtbl -> get_EnableSelectionControls(This,Enable) ) 

#define IActiveMovie2_put_EnableSelectionControls(This,Enable)	\
    ( (This)->lpVtbl -> put_EnableSelectionControls(This,Enable) ) 

#define IActiveMovie2_get_EnableTracker(This,Enable)	\
    ( (This)->lpVtbl -> get_EnableTracker(This,Enable) ) 

#define IActiveMovie2_put_EnableTracker(This,Enable)	\
    ( (This)->lpVtbl -> put_EnableTracker(This,Enable) ) 

#define IActiveMovie2_get_AllowHideDisplay(This,Show)	\
    ( (This)->lpVtbl -> get_AllowHideDisplay(This,Show) ) 

#define IActiveMovie2_put_AllowHideDisplay(This,Show)	\
    ( (This)->lpVtbl -> put_AllowHideDisplay(This,Show) ) 

#define IActiveMovie2_get_AllowHideControls(This,Show)	\
    ( (This)->lpVtbl -> get_AllowHideControls(This,Show) ) 

#define IActiveMovie2_put_AllowHideControls(This,Show)	\
    ( (This)->lpVtbl -> put_AllowHideControls(This,Show) ) 

#define IActiveMovie2_get_DisplayMode(This,pValue)	\
    ( (This)->lpVtbl -> get_DisplayMode(This,pValue) ) 

#define IActiveMovie2_put_DisplayMode(This,Value)	\
    ( (This)->lpVtbl -> put_DisplayMode(This,Value) ) 

#define IActiveMovie2_get_AllowChangeDisplayMode(This,fAllow)	\
    ( (This)->lpVtbl -> get_AllowChangeDisplayMode(This,fAllow) ) 

#define IActiveMovie2_put_AllowChangeDisplayMode(This,Allow)	\
    ( (This)->lpVtbl -> put_AllowChangeDisplayMode(This,Allow) ) 

#define IActiveMovie2_get_FilterGraph(This,ppFilterGraph)	\
    ( (This)->lpVtbl -> get_FilterGraph(This,ppFilterGraph) ) 

#define IActiveMovie2_put_FilterGraph(This,pFilterGraph)	\
    ( (This)->lpVtbl -> put_FilterGraph(This,pFilterGraph) ) 

#define IActiveMovie2_get_FilterGraphDispatch(This,pDispatch)	\
    ( (This)->lpVtbl -> get_FilterGraphDispatch(This,pDispatch) ) 

#define IActiveMovie2_get_DisplayForeColor(This,ForeColor)	\
    ( (This)->lpVtbl -> get_DisplayForeColor(This,ForeColor) ) 

#define IActiveMovie2_put_DisplayForeColor(This,ForeColor)	\
    ( (This)->lpVtbl -> put_DisplayForeColor(This,ForeColor) ) 

#define IActiveMovie2_get_DisplayBackColor(This,BackColor)	\
    ( (This)->lpVtbl -> get_DisplayBackColor(This,BackColor) ) 

#define IActiveMovie2_put_DisplayBackColor(This,BackColor)	\
    ( (This)->lpVtbl -> put_DisplayBackColor(This,BackColor) ) 

#define IActiveMovie2_get_MovieWindowSize(This,WindowSize)	\
    ( (This)->lpVtbl -> get_MovieWindowSize(This,WindowSize) ) 

#define IActiveMovie2_put_MovieWindowSize(This,WindowSize)	\
    ( (This)->lpVtbl -> put_MovieWindowSize(This,WindowSize) ) 

#define IActiveMovie2_get_FullScreenMode(This,pEnable)	\
    ( (This)->lpVtbl -> get_FullScreenMode(This,pEnable) ) 

#define IActiveMovie2_put_FullScreenMode(This,Enable)	\
    ( (This)->lpVtbl -> put_FullScreenMode(This,Enable) ) 

#define IActiveMovie2_get_AutoStart(This,pEnable)	\
    ( (This)->lpVtbl -> get_AutoStart(This,pEnable) ) 

#define IActiveMovie2_put_AutoStart(This,Enable)	\
    ( (This)->lpVtbl -> put_AutoStart(This,Enable) ) 

#define IActiveMovie2_get_AutoRewind(This,pEnable)	\
    ( (This)->lpVtbl -> get_AutoRewind(This,pEnable) ) 

#define IActiveMovie2_put_AutoRewind(This,Enable)	\
    ( (This)->lpVtbl -> put_AutoRewind(This,Enable) ) 

#define IActiveMovie2_get_hWnd(This,hWnd)	\
    ( (This)->lpVtbl -> get_hWnd(This,hWnd) ) 

#define IActiveMovie2_get_Appearance(This,pAppearance)	\
    ( (This)->lpVtbl -> get_Appearance(This,pAppearance) ) 

#define IActiveMovie2_put_Appearance(This,Appearance)	\
    ( (This)->lpVtbl -> put_Appearance(This,Appearance) ) 

#define IActiveMovie2_get_BorderStyle(This,pBorderStyle)	\
    ( (This)->lpVtbl -> get_BorderStyle(This,pBorderStyle) ) 

#define IActiveMovie2_put_BorderStyle(This,BorderStyle)	\
    ( (This)->lpVtbl -> put_BorderStyle(This,BorderStyle) ) 

#define IActiveMovie2_get_Enabled(This,pEnabled)	\
    ( (This)->lpVtbl -> get_Enabled(This,pEnabled) ) 

#define IActiveMovie2_put_Enabled(This,Enabled)	\
    ( (This)->lpVtbl -> put_Enabled(This,Enabled) ) 

#define IActiveMovie2_get_Info(This,ppInfo)	\
    ( (This)->lpVtbl -> get_Info(This,ppInfo) ) 


#define IActiveMovie2_IsSoundCardEnabled(This,pbSoundCard)	\
    ( (This)->lpVtbl -> IsSoundCardEnabled(This,pbSoundCard) ) 

#define IActiveMovie2_get_ReadyState(This,pValue)	\
    ( (This)->lpVtbl -> get_ReadyState(This,pValue) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IActiveMovie2_INTERFACE_DEFINED__ */


#ifndef __IActiveMovie3_INTERFACE_DEFINED__
#define __IActiveMovie3_INTERFACE_DEFINED__

/* interface IActiveMovie3 */
/* [unique][dual][uuid][object] */ 


EXTERN_C const IID IID_IActiveMovie3;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("265EC140-AE62-11d1-8500-00A0C91F9CA0")
    IActiveMovie3 : public IActiveMovie2
    {
    public:
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_MediaPlayer( 
            /* [retval][out] */ IDispatch **ppDispatch) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IActiveMovie3Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IActiveMovie3 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IActiveMovie3 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IActiveMovie3 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IActiveMovie3 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IActiveMovie3 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IActiveMovie3 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IActiveMovie3 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *AboutBox )( 
            IActiveMovie3 * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Run )( 
            IActiveMovie3 * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Pause )( 
            IActiveMovie3 * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Stop )( 
            IActiveMovie3 * This);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ImageSourceWidth )( 
            IActiveMovie3 * This,
            /* [retval][out] */ long *pWidth);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ImageSourceHeight )( 
            IActiveMovie3 * This,
            /* [retval][out] */ long *pHeight);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Author )( 
            IActiveMovie3 * This,
            /* [retval][out] */ BSTR *pbstrAuthor);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Title )( 
            IActiveMovie3 * This,
            /* [retval][out] */ BSTR *pbstrTitle);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Copyright )( 
            IActiveMovie3 * This,
            /* [retval][out] */ BSTR *pbstrCopyright);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Description )( 
            IActiveMovie3 * This,
            /* [retval][out] */ BSTR *pbstrDescription);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Rating )( 
            IActiveMovie3 * This,
            /* [retval][out] */ BSTR *pbstrRating);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_FileName )( 
            IActiveMovie3 * This,
            /* [retval][out] */ BSTR *pbstrFileName);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_FileName )( 
            IActiveMovie3 * This,
            /* [in] */ BSTR bstrFileName);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Duration )( 
            IActiveMovie3 * This,
            /* [retval][out] */ double *pValue);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentPosition )( 
            IActiveMovie3 * This,
            /* [retval][out] */ double *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_CurrentPosition )( 
            IActiveMovie3 * This,
            /* [in] */ double Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlayCount )( 
            IActiveMovie3 * This,
            /* [retval][out] */ long *pPlayCount);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_PlayCount )( 
            IActiveMovie3 * This,
            /* [in] */ long PlayCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SelectionStart )( 
            IActiveMovie3 * This,
            /* [retval][out] */ double *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SelectionStart )( 
            IActiveMovie3 * This,
            /* [in] */ double Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SelectionEnd )( 
            IActiveMovie3 * This,
            /* [retval][out] */ double *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SelectionEnd )( 
            IActiveMovie3 * This,
            /* [in] */ double Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentState )( 
            IActiveMovie3 * This,
            /* [retval][out] */ StateConstants *pState);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Rate )( 
            IActiveMovie3 * This,
            /* [retval][out] */ double *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Rate )( 
            IActiveMovie3 * This,
            /* [in] */ double Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Volume )( 
            IActiveMovie3 * This,
            /* [retval][out] */ long *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Volume )( 
            IActiveMovie3 * This,
            /* [in] */ long Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Balance )( 
            IActiveMovie3 * This,
            /* [retval][out] */ long *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Balance )( 
            IActiveMovie3 * This,
            /* [in] */ long Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableContextMenu )( 
            IActiveMovie3 * This,
            /* [retval][out] */ VARIANT_BOOL *pEnable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableContextMenu )( 
            IActiveMovie3 * This,
            /* [in] */ VARIANT_BOOL Enable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowDisplay )( 
            IActiveMovie3 * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowDisplay )( 
            IActiveMovie3 * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowControls )( 
            IActiveMovie3 * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowControls )( 
            IActiveMovie3 * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowPositionControls )( 
            IActiveMovie3 * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowPositionControls )( 
            IActiveMovie3 * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowSelectionControls )( 
            IActiveMovie3 * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowSelectionControls )( 
            IActiveMovie3 * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowTracker )( 
            IActiveMovie3 * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowTracker )( 
            IActiveMovie3 * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnablePositionControls )( 
            IActiveMovie3 * This,
            /* [retval][out] */ VARIANT_BOOL *Enable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnablePositionControls )( 
            IActiveMovie3 * This,
            /* [in] */ VARIANT_BOOL Enable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableSelectionControls )( 
            IActiveMovie3 * This,
            /* [retval][out] */ VARIANT_BOOL *Enable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableSelectionControls )( 
            IActiveMovie3 * This,
            /* [in] */ VARIANT_BOOL Enable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableTracker )( 
            IActiveMovie3 * This,
            /* [retval][out] */ VARIANT_BOOL *Enable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableTracker )( 
            IActiveMovie3 * This,
            /* [in] */ VARIANT_BOOL Enable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AllowHideDisplay )( 
            IActiveMovie3 * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AllowHideDisplay )( 
            IActiveMovie3 * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AllowHideControls )( 
            IActiveMovie3 * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AllowHideControls )( 
            IActiveMovie3 * This,
            /* [in] */ VARIANT_BOOL Show);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_DisplayMode )( 
            IActiveMovie3 * This,
            /* [retval][out] */ DisplayModeConstants *pValue);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_DisplayMode )( 
            IActiveMovie3 * This,
            /* [in] */ DisplayModeConstants Value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AllowChangeDisplayMode )( 
            IActiveMovie3 * This,
            /* [retval][out] */ VARIANT_BOOL *fAllow);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AllowChangeDisplayMode )( 
            IActiveMovie3 * This,
            /* [in] */ VARIANT_BOOL Allow);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_FilterGraph )( 
            IActiveMovie3 * This,
            /* [retval][out] */ IUnknown **ppFilterGraph);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_FilterGraph )( 
            IActiveMovie3 * This,
            /* [in] */ IUnknown *pFilterGraph);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_FilterGraphDispatch )( 
            IActiveMovie3 * This,
            /* [retval][out] */ IDispatch **pDispatch);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DisplayForeColor )( 
            IActiveMovie3 * This,
            /* [retval][out] */ OLE_COLOR *ForeColor);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DisplayForeColor )( 
            IActiveMovie3 * This,
            /* [in] */ OLE_COLOR ForeColor);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_DisplayBackColor )( 
            IActiveMovie3 * This,
            /* [retval][out] */ OLE_COLOR *BackColor);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_DisplayBackColor )( 
            IActiveMovie3 * This,
            /* [in] */ OLE_COLOR BackColor);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_MovieWindowSize )( 
            IActiveMovie3 * This,
            /* [retval][out] */ WindowSizeConstants *WindowSize);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_MovieWindowSize )( 
            IActiveMovie3 * This,
            /* [in] */ WindowSizeConstants WindowSize);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_FullScreenMode )( 
            IActiveMovie3 * This,
            /* [retval][out] */ VARIANT_BOOL *pEnable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_FullScreenMode )( 
            IActiveMovie3 * This,
            /* [in] */ VARIANT_BOOL Enable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AutoStart )( 
            IActiveMovie3 * This,
            /* [retval][out] */ VARIANT_BOOL *pEnable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AutoStart )( 
            IActiveMovie3 * This,
            /* [in] */ VARIANT_BOOL Enable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AutoRewind )( 
            IActiveMovie3 * This,
            /* [retval][out] */ VARIANT_BOOL *pEnable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AutoRewind )( 
            IActiveMovie3 * This,
            /* [in] */ VARIANT_BOOL Enable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_hWnd )( 
            IActiveMovie3 * This,
            /* [retval][out] */ long *hWnd);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Appearance )( 
            IActiveMovie3 * This,
            /* [retval][out] */ AppearanceConstants *pAppearance);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Appearance )( 
            IActiveMovie3 * This,
            /* [in] */ AppearanceConstants Appearance);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_BorderStyle )( 
            IActiveMovie3 * This,
            /* [retval][out] */ BorderStyleConstants *pBorderStyle);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_BorderStyle )( 
            IActiveMovie3 * This,
            /* [in] */ BorderStyleConstants BorderStyle);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Enabled )( 
            IActiveMovie3 * This,
            /* [retval][out] */ VARIANT_BOOL *pEnabled);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Enabled )( 
            IActiveMovie3 * This,
            /* [in] */ VARIANT_BOOL Enabled);
        
        /* [hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Info )( 
            IActiveMovie3 * This,
            /* [retval][out] */ long *ppInfo);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *IsSoundCardEnabled )( 
            IActiveMovie3 * This,
            /* [retval][out] */ VARIANT_BOOL *pbSoundCard);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ReadyState )( 
            IActiveMovie3 * This,
            /* [retval][out] */ ReadyStateConstants *pValue);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MediaPlayer )( 
            IActiveMovie3 * This,
            /* [retval][out] */ IDispatch **ppDispatch);
        
        END_INTERFACE
    } IActiveMovie3Vtbl;

    interface IActiveMovie3
    {
        CONST_VTBL struct IActiveMovie3Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IActiveMovie3_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IActiveMovie3_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IActiveMovie3_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IActiveMovie3_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IActiveMovie3_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IActiveMovie3_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IActiveMovie3_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IActiveMovie3_AboutBox(This)	\
    ( (This)->lpVtbl -> AboutBox(This) ) 

#define IActiveMovie3_Run(This)	\
    ( (This)->lpVtbl -> Run(This) ) 

#define IActiveMovie3_Pause(This)	\
    ( (This)->lpVtbl -> Pause(This) ) 

#define IActiveMovie3_Stop(This)	\
    ( (This)->lpVtbl -> Stop(This) ) 

#define IActiveMovie3_get_ImageSourceWidth(This,pWidth)	\
    ( (This)->lpVtbl -> get_ImageSourceWidth(This,pWidth) ) 

#define IActiveMovie3_get_ImageSourceHeight(This,pHeight)	\
    ( (This)->lpVtbl -> get_ImageSourceHeight(This,pHeight) ) 

#define IActiveMovie3_get_Author(This,pbstrAuthor)	\
    ( (This)->lpVtbl -> get_Author(This,pbstrAuthor) ) 

#define IActiveMovie3_get_Title(This,pbstrTitle)	\
    ( (This)->lpVtbl -> get_Title(This,pbstrTitle) ) 

#define IActiveMovie3_get_Copyright(This,pbstrCopyright)	\
    ( (This)->lpVtbl -> get_Copyright(This,pbstrCopyright) ) 

#define IActiveMovie3_get_Description(This,pbstrDescription)	\
    ( (This)->lpVtbl -> get_Description(This,pbstrDescription) ) 

#define IActiveMovie3_get_Rating(This,pbstrRating)	\
    ( (This)->lpVtbl -> get_Rating(This,pbstrRating) ) 

#define IActiveMovie3_get_FileName(This,pbstrFileName)	\
    ( (This)->lpVtbl -> get_FileName(This,pbstrFileName) ) 

#define IActiveMovie3_put_FileName(This,bstrFileName)	\
    ( (This)->lpVtbl -> put_FileName(This,bstrFileName) ) 

#define IActiveMovie3_get_Duration(This,pValue)	\
    ( (This)->lpVtbl -> get_Duration(This,pValue) ) 

#define IActiveMovie3_get_CurrentPosition(This,pValue)	\
    ( (This)->lpVtbl -> get_CurrentPosition(This,pValue) ) 

#define IActiveMovie3_put_CurrentPosition(This,Value)	\
    ( (This)->lpVtbl -> put_CurrentPosition(This,Value) ) 

#define IActiveMovie3_get_PlayCount(This,pPlayCount)	\
    ( (This)->lpVtbl -> get_PlayCount(This,pPlayCount) ) 

#define IActiveMovie3_put_PlayCount(This,PlayCount)	\
    ( (This)->lpVtbl -> put_PlayCount(This,PlayCount) ) 

#define IActiveMovie3_get_SelectionStart(This,pValue)	\
    ( (This)->lpVtbl -> get_SelectionStart(This,pValue) ) 

#define IActiveMovie3_put_SelectionStart(This,Value)	\
    ( (This)->lpVtbl -> put_SelectionStart(This,Value) ) 

#define IActiveMovie3_get_SelectionEnd(This,pValue)	\
    ( (This)->lpVtbl -> get_SelectionEnd(This,pValue) ) 

#define IActiveMovie3_put_SelectionEnd(This,Value)	\
    ( (This)->lpVtbl -> put_SelectionEnd(This,Value) ) 

#define IActiveMovie3_get_CurrentState(This,pState)	\
    ( (This)->lpVtbl -> get_CurrentState(This,pState) ) 

#define IActiveMovie3_get_Rate(This,pValue)	\
    ( (This)->lpVtbl -> get_Rate(This,pValue) ) 

#define IActiveMovie3_put_Rate(This,Value)	\
    ( (This)->lpVtbl -> put_Rate(This,Value) ) 

#define IActiveMovie3_get_Volume(This,pValue)	\
    ( (This)->lpVtbl -> get_Volume(This,pValue) ) 

#define IActiveMovie3_put_Volume(This,Value)	\
    ( (This)->lpVtbl -> put_Volume(This,Value) ) 

#define IActiveMovie3_get_Balance(This,pValue)	\
    ( (This)->lpVtbl -> get_Balance(This,pValue) ) 

#define IActiveMovie3_put_Balance(This,Value)	\
    ( (This)->lpVtbl -> put_Balance(This,Value) ) 

#define IActiveMovie3_get_EnableContextMenu(This,pEnable)	\
    ( (This)->lpVtbl -> get_EnableContextMenu(This,pEnable) ) 

#define IActiveMovie3_put_EnableContextMenu(This,Enable)	\
    ( (This)->lpVtbl -> put_EnableContextMenu(This,Enable) ) 

#define IActiveMovie3_get_ShowDisplay(This,Show)	\
    ( (This)->lpVtbl -> get_ShowDisplay(This,Show) ) 

#define IActiveMovie3_put_ShowDisplay(This,Show)	\
    ( (This)->lpVtbl -> put_ShowDisplay(This,Show) ) 

#define IActiveMovie3_get_ShowControls(This,Show)	\
    ( (This)->lpVtbl -> get_ShowControls(This,Show) ) 

#define IActiveMovie3_put_ShowControls(This,Show)	\
    ( (This)->lpVtbl -> put_ShowControls(This,Show) ) 

#define IActiveMovie3_get_ShowPositionControls(This,Show)	\
    ( (This)->lpVtbl -> get_ShowPositionControls(This,Show) ) 

#define IActiveMovie3_put_ShowPositionControls(This,Show)	\
    ( (This)->lpVtbl -> put_ShowPositionControls(This,Show) ) 

#define IActiveMovie3_get_ShowSelectionControls(This,Show)	\
    ( (This)->lpVtbl -> get_ShowSelectionControls(This,Show) ) 

#define IActiveMovie3_put_ShowSelectionControls(This,Show)	\
    ( (This)->lpVtbl -> put_ShowSelectionControls(This,Show) ) 

#define IActiveMovie3_get_ShowTracker(This,Show)	\
    ( (This)->lpVtbl -> get_ShowTracker(This,Show) ) 

#define IActiveMovie3_put_ShowTracker(This,Show)	\
    ( (This)->lpVtbl -> put_ShowTracker(This,Show) ) 

#define IActiveMovie3_get_EnablePositionControls(This,Enable)	\
    ( (This)->lpVtbl -> get_EnablePositionControls(This,Enable) ) 

#define IActiveMovie3_put_EnablePositionControls(This,Enable)	\
    ( (This)->lpVtbl -> put_EnablePositionControls(This,Enable) ) 

#define IActiveMovie3_get_EnableSelectionControls(This,Enable)	\
    ( (This)->lpVtbl -> get_EnableSelectionControls(This,Enable) ) 

#define IActiveMovie3_put_EnableSelectionControls(This,Enable)	\
    ( (This)->lpVtbl -> put_EnableSelectionControls(This,Enable) ) 

#define IActiveMovie3_get_EnableTracker(This,Enable)	\
    ( (This)->lpVtbl -> get_EnableTracker(This,Enable) ) 

#define IActiveMovie3_put_EnableTracker(This,Enable)	\
    ( (This)->lpVtbl -> put_EnableTracker(This,Enable) ) 

#define IActiveMovie3_get_AllowHideDisplay(This,Show)	\
    ( (This)->lpVtbl -> get_AllowHideDisplay(This,Show) ) 

#define IActiveMovie3_put_AllowHideDisplay(This,Show)	\
    ( (This)->lpVtbl -> put_AllowHideDisplay(This,Show) ) 

#define IActiveMovie3_get_AllowHideControls(This,Show)	\
    ( (This)->lpVtbl -> get_AllowHideControls(This,Show) ) 

#define IActiveMovie3_put_AllowHideControls(This,Show)	\
    ( (This)->lpVtbl -> put_AllowHideControls(This,Show) ) 

#define IActiveMovie3_get_DisplayMode(This,pValue)	\
    ( (This)->lpVtbl -> get_DisplayMode(This,pValue) ) 

#define IActiveMovie3_put_DisplayMode(This,Value)	\
    ( (This)->lpVtbl -> put_DisplayMode(This,Value) ) 

#define IActiveMovie3_get_AllowChangeDisplayMode(This,fAllow)	\
    ( (This)->lpVtbl -> get_AllowChangeDisplayMode(This,fAllow) ) 

#define IActiveMovie3_put_AllowChangeDisplayMode(This,Allow)	\
    ( (This)->lpVtbl -> put_AllowChangeDisplayMode(This,Allow) ) 

#define IActiveMovie3_get_FilterGraph(This,ppFilterGraph)	\
    ( (This)->lpVtbl -> get_FilterGraph(This,ppFilterGraph) ) 

#define IActiveMovie3_put_FilterGraph(This,pFilterGraph)	\
    ( (This)->lpVtbl -> put_FilterGraph(This,pFilterGraph) ) 

#define IActiveMovie3_get_FilterGraphDispatch(This,pDispatch)	\
    ( (This)->lpVtbl -> get_FilterGraphDispatch(This,pDispatch) ) 

#define IActiveMovie3_get_DisplayForeColor(This,ForeColor)	\
    ( (This)->lpVtbl -> get_DisplayForeColor(This,ForeColor) ) 

#define IActiveMovie3_put_DisplayForeColor(This,ForeColor)	\
    ( (This)->lpVtbl -> put_DisplayForeColor(This,ForeColor) ) 

#define IActiveMovie3_get_DisplayBackColor(This,BackColor)	\
    ( (This)->lpVtbl -> get_DisplayBackColor(This,BackColor) ) 

#define IActiveMovie3_put_DisplayBackColor(This,BackColor)	\
    ( (This)->lpVtbl -> put_DisplayBackColor(This,BackColor) ) 

#define IActiveMovie3_get_MovieWindowSize(This,WindowSize)	\
    ( (This)->lpVtbl -> get_MovieWindowSize(This,WindowSize) ) 

#define IActiveMovie3_put_MovieWindowSize(This,WindowSize)	\
    ( (This)->lpVtbl -> put_MovieWindowSize(This,WindowSize) ) 

#define IActiveMovie3_get_FullScreenMode(This,pEnable)	\
    ( (This)->lpVtbl -> get_FullScreenMode(This,pEnable) ) 

#define IActiveMovie3_put_FullScreenMode(This,Enable)	\
    ( (This)->lpVtbl -> put_FullScreenMode(This,Enable) ) 

#define IActiveMovie3_get_AutoStart(This,pEnable)	\
    ( (This)->lpVtbl -> get_AutoStart(This,pEnable) ) 

#define IActiveMovie3_put_AutoStart(This,Enable)	\
    ( (This)->lpVtbl -> put_AutoStart(This,Enable) ) 

#define IActiveMovie3_get_AutoRewind(This,pEnable)	\
    ( (This)->lpVtbl -> get_AutoRewind(This,pEnable) ) 

#define IActiveMovie3_put_AutoRewind(This,Enable)	\
    ( (This)->lpVtbl -> put_AutoRewind(This,Enable) ) 

#define IActiveMovie3_get_hWnd(This,hWnd)	\
    ( (This)->lpVtbl -> get_hWnd(This,hWnd) ) 

#define IActiveMovie3_get_Appearance(This,pAppearance)	\
    ( (This)->lpVtbl -> get_Appearance(This,pAppearance) ) 

#define IActiveMovie3_put_Appearance(This,Appearance)	\
    ( (This)->lpVtbl -> put_Appearance(This,Appearance) ) 

#define IActiveMovie3_get_BorderStyle(This,pBorderStyle)	\
    ( (This)->lpVtbl -> get_BorderStyle(This,pBorderStyle) ) 

#define IActiveMovie3_put_BorderStyle(This,BorderStyle)	\
    ( (This)->lpVtbl -> put_BorderStyle(This,BorderStyle) ) 

#define IActiveMovie3_get_Enabled(This,pEnabled)	\
    ( (This)->lpVtbl -> get_Enabled(This,pEnabled) ) 

#define IActiveMovie3_put_Enabled(This,Enabled)	\
    ( (This)->lpVtbl -> put_Enabled(This,Enabled) ) 

#define IActiveMovie3_get_Info(This,ppInfo)	\
    ( (This)->lpVtbl -> get_Info(This,ppInfo) ) 


#define IActiveMovie3_IsSoundCardEnabled(This,pbSoundCard)	\
    ( (This)->lpVtbl -> IsSoundCardEnabled(This,pbSoundCard) ) 

#define IActiveMovie3_get_ReadyState(This,pValue)	\
    ( (This)->lpVtbl -> get_ReadyState(This,pValue) ) 


#define IActiveMovie3_get_MediaPlayer(This,ppDispatch)	\
    ( (This)->lpVtbl -> get_MediaPlayer(This,ppDispatch) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IActiveMovie3_INTERFACE_DEFINED__ */


/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


