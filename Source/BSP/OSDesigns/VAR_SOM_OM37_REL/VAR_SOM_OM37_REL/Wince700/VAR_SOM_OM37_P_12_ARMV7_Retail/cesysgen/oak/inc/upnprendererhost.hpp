//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//
//
// -----------------------------------------------------------------------------
//
//      THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
//      ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//      THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
//      PARTICULAR PURPOSE.
//  
// -----------------------------------------------------------------------------


//////////////////////////////////////////////////////////
//	USAGE
//		Call UpnpRendererHost_t::Initialize before using FriendlyDeviceName
//		and UniqueDeviceName functions.
//
//		Call Unitialize before exiting to make sure upnp renderer auxilary 
//		thread exits before your program's main thread exit
/////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////
//	FUNTIONALITY
//		This is a special purpose renderer host written for NMD feature
//		pack. It solves the cardea's need for serial number/udn match 
//		and provides a way to propage device name changes to media 
//		server.
//
//		It is loading media renderer based on registry information
//		[HKEY_LOCAL_MACHINE\Software\UPnPDevices\MediaRenderer]
//
//		To make the UpnpRendererHost_t host a different media renderer,
//		modify the registry.
//		The UpnpRendererHost_t hosts only one media renderer.
////////////////////////////////////////////////////////


#ifndef		__INCLUDED_UPNPRENDERERHOST_HPP__
#define		__INCLUDED_UPNPRENDERERHOST_HPP__

#ifndef __STRING__
#include	"string.hxx"
#endif // __STRING__

class	UpnpRendererHostImpl_t;

class	UpnpRendererHost_t
{
public:
	static	const	int	s_MAX_FriendlyNameLength = 100;
	
private:
	static	UpnpRendererHostImpl_t*		s_pUpnpRendererHostImpl;

public:
	static
	bool
	Initialize(
		);

	static
	bool
	UnInitialize(
		);

	static
	ce::wstring
	FriendlyDeviceName(
		);

	static
	bool
	FriendlyDeviceName(
		ce::wstring	NewName
		);

	static
	ce::wstring
	UniqueDeviceName(
		);

};


#endif	//__INCLUDED_UPNPRENDERERHOST_HPP__

