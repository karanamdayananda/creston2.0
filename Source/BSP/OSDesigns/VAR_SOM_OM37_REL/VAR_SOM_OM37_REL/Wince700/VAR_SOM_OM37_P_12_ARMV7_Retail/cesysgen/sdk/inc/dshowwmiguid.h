//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft
// premium shared source license agreement under which you licensed
// this source code. If you did not accept the terms of the license
// agreement, you are not authorized to use this source code.
// For the terms of the license, please see the license agreement
// signed by you and Microsoft.
// THE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//
//  DShow WMI Instrumentation Structures and GUIDs

#if !defined(__MFWMIGUID_H__)
#define __MFWMIGUID_H__
#ifdef __cplusplus
extern "C" {
#endif

//
// {8959E8FC-BAE3-44f4-9A62-C50284504A03}
DEFINE_GUID(DSHOWWMIGUID, 
0x8959e8fc, 0xbae3, 0x44f4, 0x9a, 0x62, 0xc5, 0x2, 0x84, 0x50, 0x4a, 0x3);

//==============================
// Event GUIDs are defined here.
//==============================

//------------------------
// Object event
//  This event is reported when an object is created or deleted.
//
// {54A408BF-345E-4307-8B52-1AD937531913}
DEFINE_GUID(DSHOWWMIGUID_OBJECT_EVENT, 
0x54a408bf, 0x345e, 0x4307, 0x8b, 0x52, 0x1a, 0xd9, 0x37, 0x53, 0x19, 0x13);

//------------------------
// Dropping/glitching/starvation events
//
// {D42659AC-94F7-4a72-BBED-8D1E53A5C94A}
DEFINE_GUID(DSHOWWMIGUID_VIDEO_FRAME_GLITCH_EVENT, 
0xd42659ac, 0x94f7, 0x4a72, 0xbb, 0xed, 0x8d, 0x1e, 0x53, 0xa5, 0xc9, 0x4a);

// {BDE2A764-BED5-4632-A108-F0B02D90A073}
DEFINE_GUID(DSHOWWMIGUID_DATA_DROP_EVENT, 
0xbde2a764, 0xbed5, 0x4632, 0xa1, 0x8, 0xf0, 0xb0, 0x2d, 0x90, 0xa0, 0x73);

// {A8894BCF-E056-425a-964C-16770E181756}
DEFINE_GUID(DSHOWWMIGUID_AUDIORENDERER_STARVATION_EVENT, 
0xa8894bcf, 0xe056, 0x425a, 0x96, 0x4c, 0x16, 0x77, 0xe, 0x18, 0x17, 0x56);

//------------------------
// ClockGetTime Event
// {85C88917-519D-456b-9FA8-B6899BBFABF1}
DEFINE_GUID(DSHOWWMIGUID_CLOCK_GETTIME_EVENT, 
0x85c88917, 0x519d, 0x456b, 0x9f, 0xa8, 0xb6, 0x89, 0x9b, 0xbf, 0xab, 0xf1);

//------------------------
// ClockSetTime Event
// {918787B6-2046-4eac-A77D-DC36ABA7EDE1}
DEFINE_GUID(DSHOWWMIGUID_CLOCK_SETTIME_EVENT, 
0x918787b6, 0x2046, 0x4eac, 0xa7, 0x7d, 0xdc, 0x36, 0xab, 0xa7, 0xed, 0xe1);

//------------------------
// Stream event
// {43EB4C9E-A770-45a1-91D8-144657B3EBD2}
DEFINE_GUID(DSHOWWMIGUID_STREAM_EVENT, 
0x43eb4c9e, 0xa770, 0x45a1, 0x91, 0xd8, 0x14, 0x46, 0x57, 0xb3, 0xeb, 0xd2);

//------------------------
// MEDIATYPE change event
// {574FDB59-AB53-4e60-98EB-C735A757159F}
DEFINE_GUID(DSHOWWMIGUID_MEDIATYPE_CHANGE_EVENT, 
0x574fdb59, 0xab53, 0x4e60, 0x98, 0xeb, 0xc7, 0x35, 0xa7, 0x57, 0x15, 0x9f);

//------------------------
// Buffer event
// {12F1BA5C-368B-4491-ADFD-72C0558ADF9C}
DEFINE_GUID(DSHOWWMIGUID_BUFFER_EVENT, 
0x12f1ba5c, 0x368b, 0x4491, 0xad, 0xfd, 0x72, 0xc0, 0x55, 0x8a, 0xdf, 0x9c);

//-------------------------
// Unusual streaming event
// {191E80E7-58E5-4b55-B569-B879ACF9872A}
DEFINE_GUID(DSHOWWMIGUID_UNUSUAL_STREAMING_EVENT, 
0x191e80e7, 0x58e5, 0x4b55, 0xb5, 0x69, 0xb8, 0x79, 0xac, 0xf9, 0x87, 0x2a);

//-------------------------
// Disk events
// {0636675E-F10B-4127-9FB7-FD6DD137CF36}
DEFINE_GUID(DSHOWWMIGUID_DISKIO_REQUEST_EVENT, 
0x636675e, 0xf10b, 0x4127, 0x9f, 0xb7, 0xfd, 0x6d, 0xd1, 0x37, 0xcf, 0x36);

// {71F1BE77-9369-4be1-BA3C-087E0AA0A105}
DEFINE_GUID(DSHOWWMIGUID_DISKIO_COMPLETE_EVENT, 
0x71f1be77, 0x9369, 0x4be1, 0xba, 0x3c, 0x8, 0x7e, 0xa, 0xa0, 0xa1, 0x5);

// {75C0B368-BFFA-4b3e-8004-34627B947070}
DEFINE_GUID(DSHOWWMIGUID_FILE_OPEN_EVENT, 
0x75c0b368, 0xbffa, 0x4b3e, 0x80, 0x4, 0x34, 0x62, 0x7b, 0x94, 0x70, 0x70);

//-------------------------
// Renderer events
// {A71FD694-523D-46b7-86C7-5C6AD041B828}
DEFINE_GUID(DSHOWWMIGUID_RENDER_REND_EVENT, 
0xa71fd694, 0x523d, 0x46b7, 0x86, 0xc7, 0x5c, 0x6a, 0xd0, 0x41, 0xb8, 0x28);

//-------------------------
// Buffer fullness events
// {D5319DB6-91D7-4136-969F-232CA341BEA2}
DEFINE_GUID(DSHOWWMIGUID_AUDIORENDERER_BUFFERFULLNESS_EVENT, 
0xd5319db6, 0x91d7, 0x4136, 0x96, 0x9f, 0x23, 0x2c, 0xa3, 0x41, 0xbe, 0xa2);

//-------------------------
// State change events
// {217A0449-FF1F-4e50-AE79-E20F3C0199BE}
DEFINE_GUID(DSHOWWMIGUID_STATE_CHANGE_EVENT, 
0x217a0449, 0xff1f, 0x4e50, 0xae, 0x79, 0xe2, 0xf, 0x3c, 0x1, 0x99, 0xbe);

//-------------------------
// Netsource events
// {E35D5AB8-84C1-4c8a-BCC7-3CBFDD087176}
DEFINE_GUID(DSHOWWMIGUID_NETSOURCE_EVENT, 
0xe35d5ab8, 0x84c1, 0x4c8a, 0xbc, 0xc7, 0x3c, 0xbf, 0xdd, 0x8, 0x71, 0x76);

//-------------------------
// HTTP Bytestream events
// {1F89DEEF-93DD-4143-9624-6398775CC6BE}
DEFINE_GUID(DSHOWWMIGUID_HTTP_BYTESTREAM_EVENT, 
0x1f89deef, 0x93dd, 0x4143, 0x96, 0x24, 0x63, 0x98, 0x77, 0x5c, 0xc6, 0xbe);

//-------------------------
// SourceResolver events
// {803D39AE-1752-4f20-A4E3-F00AFC856FFD}
DEFINE_GUID(DSHOWWMIGUID_SOURCERESOLUTION_EVENT, 
0x803d39ae, 0x1752, 0x4f20, 0xa4, 0xe3, 0xf0, 0xa, 0xfc, 0x85, 0x6f, 0xfd);

//-------------------------
// Quality Manager events
// {726E9FAD-B975-4d40-A2B6-EE9508BC453D}
DEFINE_GUID(DSHOWWMIGUID_QM_EVENT, 
0x726e9fad, 0xb975, 0x4d40, 0xa2, 0xb6, 0xee, 0x95, 0x8, 0xbc, 0x45, 0x3d);

//-------------------------
// Adjust sample time events
// {873A2476-7943-409c-AA20-18D0037E23F8}
DEFINE_GUID(DSHOWWMIGUID_ADJUST_SAMPLE_TIME_EVENT, 
0x873a2476, 0x7943, 0x409c, 0xaa, 0x20, 0x18, 0xd0, 0x3, 0x7e, 0x23, 0xf8);

//------------------------
// Renderer Events
// {A01E93E9-9082-48f6-A660-A49E60A2073E}
DEFINE_GUID(DSHOWWMIGUID_VIDEO_RENDER_EVENT, 
0xa01e93e9, 0x9082, 0x48f6, 0xa6, 0x60, 0xa4, 0x9e, 0x60, 0xa2, 0x7, 0x3e);

// {5062CCCB-D77F-4de8-B58F-E750CD8D1E2B}
DEFINE_GUID(DSHOWWMI_GUID_AUDIO_RENDER_EVENT, 
0x5062cccb, 0xd77f, 0x4de8, 0xb5, 0x8f, 0xe7, 0x50, 0xcd, 0x8d, 0x1e, 0x2b);

//-------------------------
// Muxer Events
// {F002E858-D408-4eea-8DB9-DC02C1494C80}
DEFINE_GUID(DSHOWWMIGUID_MUXER_EVENT, 
0xf002e858, 0xd408, 0x4eea, 0x8d, 0xb9, 0xdc, 0x2, 0xc1, 0x49, 0x4c, 0x80);

//------------------------
// Timer Late event
// {213E13E2-724B-4a6e-B949-1C04426A1527}
DEFINE_GUID(DSHOWWMIGUID_TIMER_LATE_EVENT, 
0x213e13e2, 0x724b, 0x4a6e, 0xb9, 0x49, 0x1c, 0x4, 0x42, 0x6a, 0x15, 0x27);

//-------------------------
// Lock event
// {8BA1189D-AE62-40b1-B2B6-4944989B1BDA}
DEFINE_GUID(DSHOWWMIGUID_LOCK_EVENT, 
0x8ba1189d, 0xae62, 0x40b1, 0xb2, 0xb6, 0x49, 0x44, 0x98, 0x9b, 0x1b, 0xda);

//-------------------------
// Graph events
// {6FE6BC72-BA71-46a8-8A77-0DD13102B9AF}
DEFINE_GUID(DSHOWWMIGUID_GRAPH_EVENT, 
0x6fe6bc72, 0xba71, 0x46a8, 0x8a, 0x77, 0xd, 0xd1, 0x31, 0x2, 0xb9, 0xaf);

#ifdef __cplusplus
}
#endif

#endif

