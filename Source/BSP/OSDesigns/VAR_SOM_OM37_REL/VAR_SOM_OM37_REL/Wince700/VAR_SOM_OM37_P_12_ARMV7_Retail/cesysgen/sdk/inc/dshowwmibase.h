//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft
// premium shared source license agreement under which you licensed
// this source code. If you did not accept the terms of the license
// agreement, you are not authorized to use this source code.
// For the terms of the license, please see the license agreement
// signed by you and Microsoft.
// THE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//
//*@@@+++@@@@******************************************************************
//
// Microsoft Windows Media Foundation
// Copyright (C) Microsoft Corporation. All rights reserved.
//
//*@@@---@@@@******************************************************************

// Windows XP version of WMI instrumentation headers.

#if !defined(__MFWMIBASE_H__)
#define __MFWMIBASE_H__

#include <wmistr.h>
#include <evntrace.h>

// specify calling convention? !!!
typedef void (*PfnOnStateChanged)(void);

struct WmiInfo
{
    WmiInfo():
        fEventTracingAvailable(FALSE),
        hWmiTrace(NULL)
    {}
        
    BOOL fEventTracingAvailable;
    ULONG ulWmiEnableFlags;
    UCHAR ucWmiEnableLevel;
    TRACEHANDLE hWmiTrace;
    TRACEHANDLE hWmiReg;
    PfnOnStateChanged pfnOnStateChanged;   
};
                                              
BOOL WmiInit(WmiInfo* pWmiInfo, const GUID *pControlGuid, LPCWSTR wzKeyName, PfnOnStateChanged pfnOnStateChanged);
void WmiShutdown(WmiInfo* pWmiInfo);

#endif // __MFWMIBASE_H__

