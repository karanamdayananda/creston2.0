//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//
//+-------------------------------------------------------------------------
//
//  Microsoft Windows Media
//
//
//  File:       wmcodecids.h
//
//--------------------------------------------------------------------------

#ifndef __WMCODECIDS_H_
#define __WMCODECIDS_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


//
// DMO CLSIDs for Windows Media Codecs
//

EXTERN_GUID(CLSID_CWMADecMediaObject,  0x2eeb4adf, 0x4578, 0x4d10, 0xbc, 0xa7, 0xbb, 0x95, 0x5f, 0x56, 0x32, 0x0a);
EXTERN_GUID(CLSID_CWMSDecMediaObject,  0x874131cb, 0x4ecc, 0x443b, 0x89, 0x48, 0x74, 0x6b, 0x89, 0x59, 0x5d, 0x20);
EXTERN_GUID(CLSID_CWMV9EncMediaObject, 0xd23b90d0, 0x144f, 0x46bd, 0x84, 0x1d, 0x59, 0xe4, 0xeb, 0x19, 0xdc, 0x59);
EXTERN_GUID(CLSID_CWMVDecMediaObject,  0x82d353df, 0x90bd, 0x4382, 0x8b, 0xc2, 0x3f, 0x61, 0x92, 0xb7, 0x6e, 0x34);

//
// DMO Filter CLSIDs for Windows Media Codecs
//
EXTERN_GUID(CLSID_CWMAFILTERObject, 0x5bae8e20, 0xd112, 0x4637, 0xb6, 0x03, 0xb4, 0x84, 0x1f, 0xa7, 0x1d, 0x78);
EXTERN_GUID(CLSID_CWMSFILTERObject, 0x521FB373, 0x7654, 0x49F2, 0xBD, 0xB1, 0x0C, 0x6E, 0x66, 0x60, 0x71, 0x4F);
EXTERN_GUID(CLSID_CWMVFilterObject, 0x3ff9f0d8, 0x2ccc, 0x472f, 0xb5, 0x3f, 0xbf, 0x5c, 0xa5, 0x73, 0x1a, 0x39);
EXTERN_GUID(CLSID_CWMV9EncFilterObject, 0x651b4f0a, 0x4340, 0x4526, 0xab, 0xb0, 0x90, 0x87, 0x9e, 0x9b, 0x5, 0x5a);

#endif  // !defined(__WMCODECIDS_H_)
