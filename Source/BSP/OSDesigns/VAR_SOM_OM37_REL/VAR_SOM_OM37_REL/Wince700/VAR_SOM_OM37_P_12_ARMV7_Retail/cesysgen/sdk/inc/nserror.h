//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft
// premium shared source license agreement under which you licensed
// this source code. If you did not accept the terms of the license
// agreement, you are not authorized to use this source code.
// For the terms of the license, please see the license agreement
// signed by you and Microsoft.
// THE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//
/*++

  Microsoft Windows Media Technology
  Copyright (C) Microsoft Corporation, 1999 - 2001.  All Rights Reserved.

Module Name:

    nserror.mc

Abstract:

    Definitions for NetShow events.

Author:


Revision History:

Notes:

    This file is used by the MC tool to generate the nserror.h file

**************************** READ ME ******************************************

 Here are the commented error ranges for the Windows Media Technologies Group


 LEGACY RANGES

     0  -  199 = General NetShow errors

   200  -  399 = NetShow error events

   400  -  599 = NetShow monitor events - NOT NECESSARY ON WINDOWS CE

   600  -  799 = NetShow IMmsAutoServer errors

  1000  - 1199 = NetShow MCMADM errors - NOT NECESSARY ON WINDOWS CE


 NEW RANGES

  2000 -  2999 = ASF (defined in ASFERR.MC)

  3000 -  3999 = Windows Media SDK

  4000 -  4999 = Windows Media Player

  5000 -  5999 = Windows Media Server

  6000 -  6999 = Windows Media HTTP/RTSP result codes (defined in NETERROR.MC)

  7000 -  7999 = Windows Media Tools

  8000 -  8999 = Windows Media Content Discovery

  9000 -  9999 = Windows Media Real Time Collaboration

 10000 - 10999 = Windows Media Digital Rights Management

 11000 - 11999 = Windows Media Setup - NOT NECESSARY ON WINDOWS CE

 12000 - 12999 = Windows Media Networking

**************************** READ ME ******************************************

--*/

#ifndef _NSERROR_H
#define _NSERROR_H


#define STATUS_SEVERITY(hr)  (((hr) >> 30) & 0x3)

//
//  Values are 32 bit values laid out as follows:
//
//   3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1
//   1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  +---+-+-+-----------------------+-------------------------------+
//  |Sev|C|R|     Facility          |               Code            |
//  +---+-+-+-----------------------+-------------------------------+
//
//  where
//
//      Sev - is the severity code
//
//          00 - Success
//          01 - Informational
//          10 - Warning
//          11 - Error
//
//      C - is the Customer code flag
//
//      R - is a reserved bit
//
//      Facility - is the facility code
//
//      Code - is the facility's status code
//
//
// Define the facility codes
//
#define FACILITY_NS_WIN32                0x7
#define FACILITY_NS                      0xD


//
// Define the severity codes
//
#define STATUS_SEVERITY_WARNING          0x2
#define STATUS_SEVERITY_SUCCESS          0x0
#define STATUS_SEVERITY_INFORMATIONAL    0x1
#define STATUS_SEVERITY_ERROR            0x3


//
// MessageId: NS_S_CALLPENDING
//
// MessageText:
//
// The requested operation is pending completion.%0
//
#define NS_S_CALLPENDING                 0x000D0000L


/////////////////////////////////////////////////////////////////////////
//
// NETSHOW Error Events
//
/////////////////////////////////////////////////////////////////////////

//
// MessageId: NS_E_NOCONNECTION
//
// MessageText:
//
// There is no connection established with the Windows Media server. The operation failed.%0
//
#define NS_E_NOCONNECTION                0xC00D0005L

//
// MessageId: NS_E_CANNOTCONNECT
//
// MessageText:
//
// Unable to establish a connection to the server.%0
//
#define NS_E_CANNOTCONNECT               0xC00D0006L

//
// MessageId: NS_E_CANNOTDESTROYTITLE
//
// MessageText:
//
// Unable to destroy the title.%0
//
#define NS_E_CANNOTDESTROYTITLE          0xC00D0007L

//
// MessageId: NS_E_CANNOTRENAMETITLE
//
// MessageText:
//
// Unable to rename the title.%0
//
#define NS_E_CANNOTRENAMETITLE           0xC00D0008L

//
// MessageId: NS_E_CANNOTOFFLINEDISK
//
// MessageText:
//
// Unable to offline disk.%0
//
#define NS_E_CANNOTOFFLINEDISK           0xC00D0009L

//
// MessageId: NS_E_CANNOTONLINEDISK
//
// MessageText:
//
// Unable to online disk.%0
//
#define NS_E_CANNOTONLINEDISK            0xC00D000AL

//
// MessageId: NS_E_NOREGISTEREDWALKER
//
// MessageText:
//
// There is no file parser registered for this type of file.%0
//
#define NS_E_NOREGISTEREDWALKER          0xC00D000BL

//
// MessageId: NS_E_NOFUNNEL
//
// MessageText:
//
// There is no data connection established.%0
//
#define NS_E_NOFUNNEL                    0xC00D000CL

//
// MessageId: NS_E_NO_LOCALPLAY
//
// MessageText:
//
// Failed to load the local play DLL.%0
//
#define NS_E_NO_LOCALPLAY                0xC00D000DL

//
// MessageId: NS_E_NETWORK_BUSY
//
// MessageText:
//
// The network is busy.%0
//
#define NS_E_NETWORK_BUSY                0xC00D000EL

//
// MessageId: NS_E_TOO_MANY_SESS
//
// MessageText:
//
// The server session limit was exceeded.%0
//
#define NS_E_TOO_MANY_SESS               0xC00D000FL

//
// MessageId: NS_E_ALREADY_CONNECTED
//
// MessageText:
//
// The network connection already exists.%0
//
#define NS_E_ALREADY_CONNECTED           0xC00D0010L

//
// MessageId: NS_E_INVALID_INDEX
//
// MessageText:
//
// Index %1 is invalid.%0
//
#define NS_E_INVALID_INDEX               0xC00D0011L

//
// MessageId: NS_E_PROTOCOL_MISMATCH
//
// MessageText:
//
// There is no protocol or protocol version supported by both the client and the server.%0
//
#define NS_E_PROTOCOL_MISMATCH           0xC00D0012L

//
// MessageId: NS_E_TIMEOUT
//
// MessageText:
//
// The server, a computer setup to offer multimedia content to other computers, could not handle your request for multimedia content in a timely manner.  Please try again later.%0
//
#define NS_E_TIMEOUT                     0xC00D0013L

//
// MessageId: NS_E_NET_WRITE
//
// MessageText:
//
// Error writing to the network.%0
//
#define NS_E_NET_WRITE                   0xC00D0014L

//
// MessageId: NS_E_NET_READ
//
// MessageText:
//
// Error reading from the network.%0
//
#define NS_E_NET_READ                    0xC00D0015L

//
// MessageId: NS_E_DISK_WRITE
//
// MessageText:
//
// Error writing to a disk.%0
//
#define NS_E_DISK_WRITE                  0xC00D0016L

//
// MessageId: NS_E_DISK_READ
//
// MessageText:
//
// Error reading from a disk.%0
//
#define NS_E_DISK_READ                   0xC00D0017L

//
// MessageId: NS_E_FILE_WRITE
//
// MessageText:
//
// Error writing to a file.%0
//
#define NS_E_FILE_WRITE                  0xC00D0018L

//
// MessageId: NS_E_FILE_READ
//
// MessageText:
//
// Error reading from a file.%0
//
#define NS_E_FILE_READ                   0xC00D0019L

//
// MessageId: NS_E_FILE_NOT_FOUND
//
// MessageText:
//
// The system cannot find the file specified.%0
//
#define NS_E_FILE_NOT_FOUND              0xC00D001AL

//
// MessageId: NS_E_FILE_EXISTS
//
// MessageText:
//
// The file already exists.%0
//
#define NS_E_FILE_EXISTS                 0xC00D001BL

//
// MessageId: NS_E_INVALID_NAME
//
// MessageText:
//
// The file name, directory name, or volume label syntax is incorrect.%0
//
#define NS_E_INVALID_NAME                0xC00D001CL

//
// MessageId: NS_E_FILE_OPEN_FAILED
//
// MessageText:
//
// Failed to open a file.%0
//
#define NS_E_FILE_OPEN_FAILED            0xC00D001DL

//
// MessageId: NS_E_FILE_ALLOCATION_FAILED
//
// MessageText:
//
// Unable to allocate a file.%0
//
#define NS_E_FILE_ALLOCATION_FAILED      0xC00D001EL

//
// MessageId: NS_E_FILE_INIT_FAILED
//
// MessageText:
//
// Unable to initialize a file.%0
//
#define NS_E_FILE_INIT_FAILED            0xC00D001FL

//
// MessageId: NS_E_FILE_PLAY_FAILED
//
// MessageText:
//
// Unable to play a file.%0
//
#define NS_E_FILE_PLAY_FAILED            0xC00D0020L

//
// MessageId: NS_E_SET_DISK_UID_FAILED
//
// MessageText:
//
// Could not set the disk UID.%0
//
#define NS_E_SET_DISK_UID_FAILED         0xC00D0021L

//
// MessageId: NS_E_INDUCED
//
// MessageText:
//
// An error was induced for testing purposes.%0
//
#define NS_E_INDUCED                     0xC00D0022L

//
// MessageId: NS_E_CCLINK_DOWN
//
// MessageText:
//
// Two Content Servers failed to communicate.%0
//
#define NS_E_CCLINK_DOWN                 0xC00D0023L

//
// MessageId: NS_E_INTERNAL
//
// MessageText:
//
// An unknown error occurred.%0
//
#define NS_E_INTERNAL                    0xC00D0024L

//
// MessageId: NS_E_BUSY
//
// MessageText:
//
// The requested resource is in use.%0
//
#define NS_E_BUSY                        0xC00D0025L

//
// MessageId: NS_E_UNRECOGNIZED_STREAM_TYPE
//
// MessageText:
//
// The specified stream type is not recognized.%0
//
#define NS_E_UNRECOGNIZED_STREAM_TYPE    0xC00D0026L

//
// MessageId: NS_E_NETWORK_SERVICE_FAILURE
//
// MessageText:
//
// The network service provider failed.%0
//
#define NS_E_NETWORK_SERVICE_FAILURE     0xC00D0027L

//
// MessageId: NS_E_NETWORK_RESOURCE_FAILURE
//
// MessageText:
//
// An attempt to acquire a network resource failed.%0
//
#define NS_E_NETWORK_RESOURCE_FAILURE    0xC00D0028L

//
// MessageId: NS_E_CONNECTION_FAILURE
//
// MessageText:
//
// The network connection has failed.%0
//
#define NS_E_CONNECTION_FAILURE          0xC00D0029L

//
// MessageId: NS_E_SHUTDOWN
//
// MessageText:
//
// The session is being terminated locally.%0
//
#define NS_E_SHUTDOWN                    0xC00D002AL

//
// MessageId: NS_E_INVALID_REQUEST
//
// MessageText:
//
// The request is invalid in the current state.%0
//
#define NS_E_INVALID_REQUEST             0xC00D002BL

//
// MessageId: NS_E_INSUFFICIENT_BANDWIDTH
//
// MessageText:
//
// There is insufficient bandwidth available to fulfill the request.%0
//
#define NS_E_INSUFFICIENT_BANDWIDTH      0xC00D002CL

//
// MessageId: NS_E_NOT_REBUILDING
//
// MessageText:
//
// The disk is not rebuilding.%0
//
#define NS_E_NOT_REBUILDING              0xC00D002DL

//
// MessageId: NS_E_LATE_OPERATION
//
// MessageText:
//
// An operation requested for a particular time could not be carried out on schedule.%0
//
#define NS_E_LATE_OPERATION              0xC00D002EL

//
// MessageId: NS_E_INVALID_DATA
//
// MessageText:
//
// Invalid or corrupt data was encountered.%0
//
#define NS_E_INVALID_DATA                0xC00D002FL

//
// MessageId: NS_E_FILE_BANDWIDTH_LIMIT
//
// MessageText:
//
// The bandwidth required to stream a file is higher than the maximum file bandwidth allowed on the server.%0
//
#define NS_E_FILE_BANDWIDTH_LIMIT        0xC00D0030L

//
// MessageId: NS_E_OPEN_FILE_LIMIT
//
// MessageText:
//
// The client cannot have any more files open simultaneously.%0
//
#define NS_E_OPEN_FILE_LIMIT             0xC00D0031L

//
// MessageId: NS_E_BAD_CONTROL_DATA
//
// MessageText:
//
// The server received invalid data from the client on the control connection.%0
//
#define NS_E_BAD_CONTROL_DATA            0xC00D0032L

//
// MessageId: NS_E_NO_STREAM
//
// MessageText:
//
// There is no stream available.%0
//
#define NS_E_NO_STREAM                   0xC00D0033L

//
// MessageId: NS_E_STREAM_END
//
// MessageText:
//
// There is no more data in the stream.%0
//
#define NS_E_STREAM_END                  0xC00D0034L

//
// MessageId: NS_E_SERVER_NOT_FOUND
//
// MessageText:
//
// The specified server could not be found.%0
//
#define NS_E_SERVER_NOT_FOUND            0xC00D0035L

//
// MessageId: NS_E_DUPLICATE_NAME
//
// MessageText:
//
// The specified name is already in use.
//
#define NS_E_DUPLICATE_NAME              0xC00D0036L

//
// MessageId: NS_E_DUPLICATE_ADDRESS
//
// MessageText:
//
// The specified address is already in use.
//
#define NS_E_DUPLICATE_ADDRESS           0xC00D0037L

//
// MessageId: NS_E_BAD_MULTICAST_ADDRESS
//
// MessageText:
//
// The specified address is not a valid multicast address.
//
#define NS_E_BAD_MULTICAST_ADDRESS       0xC00D0038L

//
// MessageId: NS_E_BAD_ADAPTER_ADDRESS
//
// MessageText:
//
// The specified adapter address is invalid.
//
#define NS_E_BAD_ADAPTER_ADDRESS         0xC00D0039L

//
// MessageId: NS_E_BAD_DELIVERY_MODE
//
// MessageText:
//
// The specified delivery mode is invalid.
//
#define NS_E_BAD_DELIVERY_MODE           0xC00D003AL

//
// MessageId: NS_E_INVALID_CHANNEL
//
// MessageText:
//
// The specified station does not exist.
//
#define NS_E_INVALID_CHANNEL             0xC00D003BL

//
// MessageId: NS_E_INVALID_STREAM
//
// MessageText:
//
// The specified stream does not exist.
//
#define NS_E_INVALID_STREAM              0xC00D003CL

//
// MessageId: NS_E_INVALID_ARCHIVE
//
// MessageText:
//
// The specified archive could not be opened.
//
#define NS_E_INVALID_ARCHIVE             0xC00D003DL

//
// MessageId: NS_E_NOTITLES
//
// MessageText:
//
// The system cannot find any titles on the server.%0
//
#define NS_E_NOTITLES                    0xC00D003EL

//
// MessageId: NS_E_INVALID_CLIENT
//
// MessageText:
//
// The system cannot find the client specified.%0
//
#define NS_E_INVALID_CLIENT              0xC00D003FL

//
// MessageId: NS_E_INVALID_BLACKHOLE_ADDRESS
//
// MessageText:
//
// The Blackhole Address is not initialized.%0
//
#define NS_E_INVALID_BLACKHOLE_ADDRESS   0xC00D0040L

//
// MessageId: NS_E_INCOMPATIBLE_FORMAT
//
// MessageText:
//
// The station does not support the stream format.
//
#define NS_E_INCOMPATIBLE_FORMAT         0xC00D0041L

//
// MessageId: NS_E_INVALID_KEY
//
// MessageText:
//
// The specified key is not valid.
//
#define NS_E_INVALID_KEY                 0xC00D0042L

//
// MessageId: NS_E_INVALID_PORT
//
// MessageText:
//
// The specified port is not valid.
//
#define NS_E_INVALID_PORT                0xC00D0043L

//
// MessageId: NS_E_INVALID_TTL
//
// MessageText:
//
// The specified TTL is not valid.
//
#define NS_E_INVALID_TTL                 0xC00D0044L

//
// MessageId: NS_E_STRIDE_REFUSED
//
// MessageText:
//
// The request to fast forward or rewind could not be fulfilled.
//
#define NS_E_STRIDE_REFUSED              0xC00D0045L

//
// IMmsAutoServer Errors
//
//
// MessageId: NS_E_TIGER_FAIL
//
// MessageText:
//
// The Title Server %1 has failed.%0
//
#define NS_E_TIGER_FAIL                  0xC00D0050L


// NOTENOTE!!!
//
// Due to legacy problems these error codes live inside the ASF error code range
//
//
// MessageId: NS_E_NOTHING_TO_DO
//
// MessageText:
//
//  NS_E_NOTHING_TO_DO
//
#define NS_E_NOTHING_TO_DO               0xC00D07F1L

//
// MessageId: NS_E_NO_MULTICAST
//
// MessageText:
//
// Not receiving data from the server.%0
//
#define NS_E_NO_MULTICAST                0xC00D07F2L


/////////////////////////////////////////////////////////////////////////
//
// NETSHOW Error Events
//
// IdRange = 200..399
//
/////////////////////////////////////////////////////////////////////////

//
// MessageId: NS_E_MONITOR_GIVEUP
//
// MessageText:
//
// Netshow Events Monitor is not operational and has been disconnected.%0
//
#define NS_E_MONITOR_GIVEUP              0xC00D00C8L

//
// MessageId: NS_E_REMIRRORED_DISK
//
// MessageText:
//
// Disk %1 is remirrored.%0
//
#define NS_E_REMIRRORED_DISK             0xC00D00C9L

//
// MessageId: NS_E_INSUFFICIENT_DATA
//
// MessageText:
//
// Insufficient data found.%0
//
#define NS_E_INSUFFICIENT_DATA           0xC00D00CAL

//
// MessageId: NS_E_ASSERT
//
// MessageText:
//
// %1 failed in file %2 line %3.%0
//
#define NS_E_ASSERT                      0xC00D00CBL

//
// MessageId: NS_E_BAD_ADAPTER_NAME
//
// MessageText:
//
// The specified adapter name is invalid.%0
//
#define NS_E_BAD_ADAPTER_NAME            0xC00D00CCL

//
// MessageId: NS_E_NOT_LICENSED
//
// MessageText:
//
// The application is not licensed for this feature.%0
//
#define NS_E_NOT_LICENSED                0xC00D00CDL

//
// MessageId: NS_E_NO_SERVER_CONTACT
//
// MessageText:
//
// Unable to contact the server.%0
//
#define NS_E_NO_SERVER_CONTACT           0xC00D00CEL

//
// MessageId: NS_E_TOO_MANY_TITLES
//
// MessageText:
//
// Maximum number of titles exceeded.%0
//
#define NS_E_TOO_MANY_TITLES             0xC00D00CFL

//
// MessageId: NS_E_TITLE_SIZE_EXCEEDED
//
// MessageText:
//
// Maximum size of a title exceeded.%0
//
#define NS_E_TITLE_SIZE_EXCEEDED         0xC00D00D0L

//
// MessageId: NS_E_UDP_DISABLED
//
// MessageText:
//
// UDP protocol not enabled. Not trying %1!ls!.%0
//
#define NS_E_UDP_DISABLED                0xC00D00D1L

//
// MessageId: NS_E_TCP_DISABLED
//
// MessageText:
//
// TCP protocol not enabled. Not trying %1!ls!.%0
//
#define NS_E_TCP_DISABLED                0xC00D00D2L

//
// MessageId: NS_E_HTTP_DISABLED
//
// MessageText:
//
// HTTP protocol not enabled. Not trying %1!ls!.%0
//
#define NS_E_HTTP_DISABLED               0xC00D00D3L

//
// MessageId: NS_E_LICENSE_EXPIRED
//
// MessageText:
//
// The product license has expired.%0
//
#define NS_E_LICENSE_EXPIRED             0xC00D00D4L


//
// Advanced Streaming Format (ASF) codes occupy MessageIds 2000-2999
//
// See ASFErr.mc for more details - please do not define any symbols
// in that range in this file.
//


/////////////////////////////////////////////////////////////////////////
//
// Windows Media SDK Errors
//
// IdRange = 3000-3199
//
/////////////////////////////////////////////////////////////////////////

//
// MessageId: NS_E_LICENSE_REQUIRED
//
// MessageText:
//
// You need a license to perform the requested operation on this media file.%0
//
#define NS_E_LICENSE_REQUIRED            0xC00D0BBEL

//
// MessageId: NS_E_LICENSE_OUTOFDATE
//
// MessageText:
//
// The license for this media file has expired. Get a new license or contact the content provider for further assistance.%0
//
#define NS_E_LICENSE_OUTOFDATE           0xC00D0BC0L

//
// MessageId: NS_E_LICENSE_INCORRECT_RIGHTS
//
// MessageText:
//
// You are not allowed to open this file. Contact the content provider for further assistance.%0
//
#define NS_E_LICENSE_INCORRECT_RIGHTS    0xC00D0BC1L

//
// MessageId: NS_E_OFFLINE_MODE
//
// MessageText:
//
// The requested URL is not available in offline mode.%0
//
#define NS_E_OFFLINE_MODE                0xC00D0BCAL


/////////////////////////////////////////////////////////////////////////
//
// DRM Specific Errors
//
// IdRange = 10000..10999
/////////////////////////////////////////////////////////////////////////
//
// MessageId: NS_E_DRM_INVALID_APPLICATION
//
// MessageText:
//
// A problem has occurred in the Digital Rights Management component. Contact product support for this application.%0
//
#define NS_E_DRM_INVALID_APPLICATION     0xC00D2711L

//
// MessageId: NS_E_DRM_LICENSE_STORE_ERROR
//
// MessageText:
//
// License storage is not working. Contact Microsoft product support.%0
//
#define NS_E_DRM_LICENSE_STORE_ERROR     0xC00D2712L

//
// MessageId: NS_E_DRM_SECURE_STORE_ERROR
//
// MessageText:
//
// Secure storage is not working. Contact Microsoft product support.%0
//
#define NS_E_DRM_SECURE_STORE_ERROR      0xC00D2713L

//
// MessageId: NS_E_DRM_LICENSE_STORE_SAVE_ERROR
//
// MessageText:
//
// License acquisition did not work. Acquire a new license or contact the content provider for further assistance.%0
//
#define NS_E_DRM_LICENSE_STORE_SAVE_ERROR 0xC00D2714L

//
// MessageId: NS_E_DRM_SECURE_STORE_UNLOCK_ERROR
//
// MessageText:
//
// A problem has occurred in the Digital Rights Management component. Contact Microsoft product support.%0
//
#define NS_E_DRM_SECURE_STORE_UNLOCK_ERROR 0xC00D2715L

//
// MessageId: NS_E_DRM_INVALID_CONTENT
//
// MessageText:
//
// The media file is corrupted. Contact the content provider to get a new file.%0
//
#define NS_E_DRM_INVALID_CONTENT         0xC00D2716L

//
// MessageId: NS_E_DRM_UNABLE_TO_OPEN_LICENSE
//
// MessageText:
//
// The license is corrupted. Acquire a new license.%0
//
#define NS_E_DRM_UNABLE_TO_OPEN_LICENSE  0xC00D2717L

//
// MessageId: NS_E_DRM_INVALID_LICENSE
//
// MessageText:
//
// The license is corrupted or invalid. Acquire a new license%0
//
#define NS_E_DRM_INVALID_LICENSE         0xC00D2718L

//
// MessageId: NS_E_DRM_INVALID_MACHINE
//
// MessageText:
//
// Licenses cannot be copied from one computer to another. Use License Management to transfer licenses, or get a new license for the media file.%0
//
#define NS_E_DRM_INVALID_MACHINE         0xC00D2719L

//
// MessageId: NS_E_DRM_LICENSE_EVAL_FAILED
//
// MessageText:
//
// The license is corrupted or invalid. Acquire a new license or reinstall the application.%0
//
#define NS_E_DRM_LICENSE_EVAL_FAILED     0xC00D271AL

//
// MessageId: NS_E_DRM_ENUM_LICENSE_FAILED
//
// MessageText:
//
// License storage is not working. Contact Microsoft product support.%0
//
#define NS_E_DRM_ENUM_LICENSE_FAILED     0xC00D271BL

//
// MessageId: NS_E_DRM_INVALID_LICENSE_REQUEST
//
// MessageText:
//
// The media file is corrupted. Contact the content provider to get a new file.%0
//
#define NS_E_DRM_INVALID_LICENSE_REQUEST 0xC00D271CL

//
// MessageId: NS_E_DRM_UNABLE_TO_INITIALIZE
//
// MessageText:
//
// A problem has occurred in the Digital Rights Management component. Contact Microsoft product support.%0
//
#define NS_E_DRM_UNABLE_TO_INITIALIZE    0xC00D271DL

//
// MessageId: NS_E_DRM_UNABLE_TO_ACQUIRE_LICENSE
//
// MessageText:
//
// The license could not be acquired. Try again later.%0
//
#define NS_E_DRM_UNABLE_TO_ACQUIRE_LICENSE 0xC00D271EL

//
// MessageId: NS_E_DRM_INVALID_LICENSE_ACQUIRED
//
// MessageText:
//
// License acquisition did not work. Acquire a new license or contact the content provider for further assistance.%0
//
#define NS_E_DRM_INVALID_LICENSE_ACQUIRED 0xC00D271FL

//
// MessageId: NS_E_DRM_NO_RIGHTS
//
// MessageText:
//
// The requested operation cannot be performed on this file.%0
//
#define NS_E_DRM_NO_RIGHTS               0xC00D2720L

//
// MessageId: NS_E_DRM_KEY_ERROR
//
// MessageText:
//
// A problem has occurred in the Digital Rights Management component. Contact Microsoft product support.%0.
//
#define NS_E_DRM_KEY_ERROR               0xC00D2721L

//
// MessageId: NS_E_DRM_ENCRYPT_ERROR
//
// MessageText:
//
// A problem has occurred in the Digital Rights Management component. Contact Microsoft product support.%0
//
#define NS_E_DRM_ENCRYPT_ERROR           0xC00D2722L

//
// MessageId: NS_E_DRM_DECRYPT_ERROR
//
// MessageText:
//
// The media file is corrupted. Contact the content provider to get a new file.%0
//
#define NS_E_DRM_DECRYPT_ERROR           0xC00D2723L

//
// MessageId: NS_E_DRM_LICENSE_PREVENTS_STORING
//
// MessageText:
//
// You cannot save this file.%0
//
#define NS_E_DRM_LICENSE_PREVENTS_STORING 0xC00D2724L

//
// MessageId: NS_E_DRM_LICENSE_INVALID_XML
//
// MessageText:
//
// The license is corrupted. Acquire a new license.%0
//
#define NS_E_DRM_LICENSE_INVALID_XML     0xC00D2725L

//
// MessageId: NS_S_DRM_LICENSE_ACQUIRED
//
// MessageText:
//
// Status message: The license was acquired.%0
//
#define NS_S_DRM_LICENSE_ACQUIRED        0x000D2726L

//
// MessageId: NS_S_DRM_INDIVIDUALIZED
//
// MessageText:
//
// Status message: The security upgrade has been completed.%0
//
#define NS_S_DRM_INDIVIDUALIZED          0x000D2727L

//
// MessageId: NS_E_DRM_NEEDS_INDIVIDUALIZATION
//
// MessageText:
//
// A security upgrade is required to perform the operation on this media file.%0
//
#define NS_E_DRM_NEEDS_INDIVIDUALIZATION 0xC00D2728L

//
// MessageId: NS_E_DRM_INDIVIDUALIZATION_FAILED
//
// MessageText:
//
// The security upgrade failed. Try again later.%0
//
#define NS_E_DRM_INDIVIDUALIZATION_FAILED 0xC00D2729L

//
// MessageId: NS_E_DRM_ACTION_NOT_QUERIED
//
// MessageText:
//
// The application cannot perform this action. Contact product support for this application.%0
//
#define NS_E_DRM_ACTION_NOT_QUERIED      0xC00D272AL

//
// MessageId: NS_E_DRM_ACQUIRING_LICENSE
//
// MessageText:
//
// You cannot begin a new license acquisition process until the current one has been completed.%0
//
#define NS_E_DRM_ACQUIRING_LICENSE       0xC00D272BL

//
// MessageId: NS_E_DRM_INDIVIDUALIZING
//
// MessageText:
//
// You cannot begin a new security upgrade until the current one has been completed.%0
//
#define NS_E_DRM_INDIVIDUALIZING         0xC00D272CL

//
// MessageId: NS_E_DRM_ENCRYPTING
//
// MessageText:
//
// A problem has occurred in the Digital Rights Management component. Contact Microsoft product support.%0.
//
#define NS_E_DRM_ENCRYPTING              0xC00D272DL

//
// MessageId: NS_E_DRM_PARAMETERS_MISMATCHED
//
// MessageText:
//
// A problem has occurred in the Digital Rights Management component. Contact Microsoft product support.%0.
//
#define NS_E_DRM_PARAMETERS_MISMATCHED   0xC00D272FL

//
// MessageId: NS_E_DRM_UNABLE_TO_CREATE_LICENSE_OBJECT
//
// MessageText:
//
// A license cannot be created for this media file. Reinstall the application.%0
//
#define NS_E_DRM_UNABLE_TO_CREATE_LICENSE_OBJECT 0xC00D2730L

//
// MessageId: NS_E_DRM_UNABLE_TO_CREATE_INDI_OBJECT
//
// MessageText:
//
// A problem has occurred in the Digital Rights Management component. Contact Microsoft product support.%0.
//
#define NS_E_DRM_UNABLE_TO_CREATE_INDI_OBJECT 0xC00D2731L

//
// MessageId: NS_E_DRM_UNABLE_TO_CREATE_ENCRYPT_OBJECT
//
// MessageText:
//
// A problem has occurred in the Digital Rights Management component. Contact Microsoft product support.%0.
//
#define NS_E_DRM_UNABLE_TO_CREATE_ENCRYPT_OBJECT 0xC00D2732L

//
// MessageId: NS_E_DRM_UNABLE_TO_CREATE_DECRYPT_OBJECT
//
// MessageText:
//
// A problem has occurred in the Digital Rights Management component. Contact Microsoft product support.%0.
//
#define NS_E_DRM_UNABLE_TO_CREATE_DECRYPT_OBJECT 0xC00D2733L

//
// MessageId: NS_E_DRM_UNABLE_TO_CREATE_PROPERTIES_OBJECT
//
// MessageText:
//
// A problem has occurred in the Digital Rights Management component. Contact Microsoft product support.%0.
//
#define NS_E_DRM_UNABLE_TO_CREATE_PROPERTIES_OBJECT 0xC00D2734L

//
// MessageId: NS_E_DRM_UNABLE_TO_CREATE_BACKUP_OBJECT
//
// MessageText:
//
// A problem has occurred in the Digital Rights Management component. Contact Microsoft product support.%0.
//
#define NS_E_DRM_UNABLE_TO_CREATE_BACKUP_OBJECT 0xC00D2735L

//
// MessageId: NS_E_DRM_INDIVIDUALIZE_ERROR
//
// MessageText:
//
// The security upgrade failed. Try again later.%0
//
#define NS_E_DRM_INDIVIDUALIZE_ERROR     0xC00D2736L

//
// MessageId: NS_E_DRM_LICENSE_OPEN_ERROR
//
// MessageText:
//
// License storage is not working. Contact Microsoft product support.%0
//
#define NS_E_DRM_LICENSE_OPEN_ERROR      0xC00D2737L

//
// MessageId: NS_E_DRM_LICENSE_CLOSE_ERROR
//
// MessageText:
//
// License storage is not working. Contact Microsoft product support.%0
//
#define NS_E_DRM_LICENSE_CLOSE_ERROR     0xC00D2738L

//
// MessageId: NS_E_DRM_GET_LICENSE_ERROR
//
// MessageText:
//
// License storage is not working. Contact Microsoft product support.%0
//
#define NS_E_DRM_GET_LICENSE_ERROR       0xC00D2739L

//
// MessageId: NS_E_DRM_QUERY_ERROR
//
// MessageText:
//
// A problem has occurred in the Digital Rights Management component. Contact Microsoft product support.%0.
//
#define NS_E_DRM_QUERY_ERROR             0xC00D273AL

//
// MessageId: NS_E_DRM_REPORT_ERROR
//
// MessageText:
//
// A problem has occurred in the Digital Rights Management component. Contact product support for this application.%0
//
#define NS_E_DRM_REPORT_ERROR            0xC00D273BL

//
// MessageId: NS_E_DRM_GET_LICENSESTRING_ERROR
//
// MessageText:
//
// License storage is not working. Contact Microsoft product support.%0
//
#define NS_E_DRM_GET_LICENSESTRING_ERROR 0xC00D273CL

//
// MessageId: NS_E_DRM_GET_CONTENTSTRING_ERROR
//
// MessageText:
//
// The media file is corrupted. Contact the content provider to get a new file.%0
//
#define NS_E_DRM_GET_CONTENTSTRING_ERROR 0xC00D273DL

//
// MessageId: NS_E_DRM_MONITOR_ERROR
//
// MessageText:
//
// A problem has occurred in the Digital Rights Management component. Try again later.%0
//
#define NS_E_DRM_MONITOR_ERROR           0xC00D273EL

//
// MessageId: NS_E_DRM_UNABLE_TO_SET_PARAMETER
//
// MessageText:
//
// The application has made an invalid call to the Digital Rights Management component. Contact product support for this application.%0
//
#define NS_E_DRM_UNABLE_TO_SET_PARAMETER 0xC00D273FL

//
// MessageId: NS_E_DRM_INVALID_APPDATA
//
// MessageText:
//
// A problem has occurred in the Digital Rights Management component. Contact Microsoft product support.%0.
//
#define NS_E_DRM_INVALID_APPDATA         0xC00D2740L

//
// MessageId: NS_E_DRM_INVALID_APPDATA_VERSION
//
// MessageText:
//
// A problem has occurred in the Digital Rights Management component. Contact product support for this application.%0.
//
#define NS_E_DRM_INVALID_APPDATA_VERSION 0xC00D2741L

//
// MessageId: NS_E_DRM_BACKUP_EXISTS
//
// MessageText:
//
// Licenses are already backed up in this location.%0
//
#define NS_E_DRM_BACKUP_EXISTS           0xC00D2742L

//
// MessageId: NS_E_DRM_BACKUP_CORRUPT
//
// MessageText:
//
// One or more backed-up licenses are missing or corrupt.%0
//
#define NS_E_DRM_BACKUP_CORRUPT          0xC00D2743L

//
// MessageId: NS_E_DRM_BACKUPRESTORE_BUSY
//
// MessageText:
//
// You cannot begin a new backup process until the current process has been completed.%0
//
#define NS_E_DRM_BACKUPRESTORE_BUSY      0xC00D2744L

//
// MessageId: NS_E_DRM_GET_ATTRIBUTE_ERROR
//
// MessageText:
//
// A problem has occurred in the Digital Rights Management component. Contact product support for this application.%0.
//
#define NS_E_DRM_GET_ATTRIBUTE_ERROR     0xC00D2745L

//
// MessageId: NS_S_DRM_MONITOR_CANCELLED
//
// MessageText:
//
// Status message: License monitoring has been cancelled.%0
//
#define NS_S_DRM_MONITOR_CANCELLED       0x000D2746L

//
// MessageId: NS_S_DRM_ACQUIRE_CANCELLED
//
// MessageText:
//
// Status message: License acquisition has been cancelled.%0
//
#define NS_S_DRM_ACQUIRE_CANCELLED       0x000D2747L

//
// MessageId: NS_E_DRM_LICENSE_UNUSABLE
//
// MessageText:
//
// The license is invalid. Contact the content provider for further assistance.%0
//
#define NS_E_DRM_LICENSE_UNUSABLE        0xC00D2748L

//
// MessageId: NS_E_DRM_INVALID_PROPERTY
//
// MessageText:
//
// A required property was not set by the application. Contact product support for this application.%0.
//
#define NS_E_DRM_INVALID_PROPERTY        0xC00D2749L

//
// MessageId: NS_E_DRM_SECURE_STORE_NOT_FOUND
//
// MessageText:
//
// A problem has occurred in the Digital Rights Management component of this application. Try to acquire a license again.%0
//
#define NS_E_DRM_SECURE_STORE_NOT_FOUND  0xC00D274AL

//
// MessageId: NS_E_DRM_CACHED_CONTENT_ERROR
//
// MessageText:
//
// A license cannot be found for this media file. Use License Management to transfer a license for this file from the original computer, or acquire a new license.%0
//
#define NS_E_DRM_CACHED_CONTENT_ERROR    0xC00D274BL

//
// MessageId: NS_E_DRM_INDIVIDUALIZATION_INCOMPLETE
//
// MessageText:
//
// A problem occurred during the security upgrade. Try again later.%0
//
#define NS_E_DRM_INDIVIDUALIZATION_INCOMPLETE 0xC00D274CL

//
// MessageId: NS_E_DRM_DRIVER_AUTH_FAILURE
//
// MessageText:
//
// Certified driver components are required to play this media file. Contact Windows Update to see whether updated drivers are available for your hardware.%0
//
#define NS_E_DRM_DRIVER_AUTH_FAILURE     0xC00D274DL

//
// MessageId: NS_E_DRM_NEED_UPGRADE
//
// MessageText:
//
// A new version of the Digital Rights Management component is required. Contact product support for this application to get the latest version.%0
//
#define NS_E_DRM_NEED_UPGRADE            0xC00D274EL

//
// MessageId: NS_E_DRM_REOPEN_CONTENT
//
// MessageText:
//
// Status message: Reopen the file.%0
//
#define NS_E_DRM_REOPEN_CONTENT          0xC00D274FL

//
// MessageId: NS_E_DRM_DRIVER_DIGIOUT_FAILURE
//
// MessageText:
//
// Certain driver functionality is required to play this media file. Contact Windows Update to see whether updated drivers are available for your hardware.%0
//
#define NS_E_DRM_DRIVER_DIGIOUT_FAILURE  0xC00D2750L

//
// MessageId: NS_E_DRM_INVALID_SECURESTORE_PASSWORD
//
// MessageText:
//
// A problem has occurred in the Digital Rights Management component. Contact Microsoft product support.%0.
//
#define NS_E_DRM_INVALID_SECURESTORE_PASSWORD 0xC00D2751L

//
// MessageId: NS_E_DRM_APPCERT_REVOKED
//
// MessageText:
//
// A problem has occurred in the Digital Rights Management component. Contact Microsoft product support.%0.
//
#define NS_E_DRM_APPCERT_REVOKED         0xC00D2752L

//
// MessageId: NS_E_DRM_RESTORE_FRAUD
//
// MessageText:
//
// You cannot restore your license(s).%0
//
#define NS_E_DRM_RESTORE_FRAUD           0xC00D2753L

//
// MessageId: NS_E_DRM_HARDWARE_INCONSISTENT
//
// MessageText:
//
// The licenses for your media files are corrupted. Contact Microsoft product support.%0
//
#define NS_E_DRM_HARDWARE_INCONSISTENT   0xC00D2754L

//
// MessageId: NS_E_DRM_SDMI_TRIGGER
//
// MessageText:
//
// To transfer this media file, you must upgrade the application.%0
//
#define NS_E_DRM_SDMI_TRIGGER            0xC00D2755L

//
// MessageId: NS_E_DRM_SDMI_NOMORECOPIES
//
// MessageText:
//
// You cannot make any more copies of this media file.%0
//
#define NS_E_DRM_SDMI_NOMORECOPIES       0xC00D2756L

//
// MessageId: NS_E_DRM_UNABLE_TO_CREATE_HEADER_OBJECT
//
// MessageText:
//
// A problem has occurred in the Digital Rights Management component. Contact Microsoft product support.%0
//
#define NS_E_DRM_UNABLE_TO_CREATE_HEADER_OBJECT 0xC00D2757L

//
// MessageId: NS_E_DRM_UNABLE_TO_CREATE_KEYS_OBJECT
//
// MessageText:
//
// A problem has occurred in the Digital Rights Management component. Contact Microsoft product support.%0
//
#define NS_E_DRM_UNABLE_TO_CREATE_KEYS_OBJECT 0xC00D2758L

// This error is never shown to user but needed for program logic.
//
// MessageId: NS_E_DRM_LICENSE_NOTACQUIRED
//
// MessageText:
//
// Unable to obtain license.%0
//
#define NS_E_DRM_LICENSE_NOTACQUIRED     0xC00D2759L

//
// MessageId: NS_E_DRM_UNABLE_TO_CREATE_CODING_OBJECT
//
// MessageText:
//
// A problem has occurred in the Digital Rights Management component. Contact Microsoft product support.%0
//
#define NS_E_DRM_UNABLE_TO_CREATE_CODING_OBJECT 0xC00D275AL

//
// MessageId: NS_E_DRM_UNABLE_TO_CREATE_STATE_DATA_OBJECT
//
// MessageText:
//
// A problem has occurred in the Digital Rights Management component. Contact Microsoft product support.%0
//
#define NS_E_DRM_UNABLE_TO_CREATE_STATE_DATA_OBJECT 0xC00D275BL

//
// MessageId: NS_E_DRM_BUFFER_TOO_SMALL
//
// MessageText:
//
// The buffer supplied is not sufficient.%0
//
#define NS_E_DRM_BUFFER_TOO_SMALL        0xC00D275CL

//
// MessageId: NS_E_DRM_UNSUPPORTED_PROPERTY
//
// MessageText:
//
// The property requested is not supported.%0
//
#define NS_E_DRM_UNSUPPORTED_PROPERTY    0xC00D275DL

//
// MessageId: NS_E_DRM_ERROR_BAD_NET_RESP
//
// MessageText:
//
// The specified server cannot perform the requested operation.%0
//
#define NS_E_DRM_ERROR_BAD_NET_RESP      0xC00D275EL

//
// MessageId: NS_E_DRM_STORE_NOTALLSTORED
//
// MessageText:
//
// Some of the licenses could not be stored.%0
//
#define NS_E_DRM_STORE_NOTALLSTORED      0xC00D275FL

// License Reasons Section
// Error Codes why a license is not usable. Reserve 10200..10300 for this purpose.
// 10200..10249 is for license reported reasons. 10250..10300 is for client detected reasons.
//
// MessageId: NS_E_DRM_LICENSE_EXPIRED
//
// MessageText:
//
// The license for this file has expired and is no longer valid. Contact your content provider for further assistance.%0
//
#define NS_E_DRM_LICENSE_EXPIRED         0xC00D27D8L

//
// MessageId: NS_E_DRM_LICENSE_NOTENABLED
//
// MessageText:
//
// The license for this file is not valid yet, but will be at a future date.%0
//
#define NS_E_DRM_LICENSE_NOTENABLED      0xC00D27D9L

//
// MessageId: NS_E_DRM_LICENSE_APPSECLOW
//
// MessageText:
//
// The license for this file requires a higher level of security than the player you are currently using has. Try using a different player or download a newer version of your current player.%0
//
#define NS_E_DRM_LICENSE_APPSECLOW       0xC00D27DAL

//
// MessageId: NS_E_DRM_STORE_NEEDINDI
//
// MessageText:
//
// The license cannot be stored as it requires security upgrade of Digital Rights Management component.%0
//
#define NS_E_DRM_STORE_NEEDINDI          0xC00D27DBL

//
// MessageId: NS_E_DRM_STORE_NOTALLOWED
//
// MessageText:
//
// Your machine does not meet the requirements for storing the license.%0
//
#define NS_E_DRM_STORE_NOTALLOWED        0xC00D27DCL

//
// MessageId: NS_E_DRM_LICENSE_NOSAP
//
// MessageText:
//
// The license for this file requires a feature that is not supported in your current player or operating system. You can try with newer version of your current player or contact your content provider for further assistance.%0
//
#define NS_E_DRM_LICENSE_NOSAP           0xC00D27DDL

//
// MessageId: NS_E_DRM_LICENSE_NOSVP
//
// MessageText:
//
// The license for this file requires a feature that is not supported in your current player or operating system. You can try with newer version of your current player or contact your content provider for further assistance.%0
//
#define NS_E_DRM_LICENSE_NOSVP           0xC00D27DEL

//
// MessageId: NS_E_DRM_LICENSE_NOWDM
//
// MessageText:
//
// The license for this file requires Windows Driver Model (WDM) audio drivers. Contact your sound card manufacturer for further assistance.%0
//
#define NS_E_DRM_LICENSE_NOWDM           0xC00D27DFL

//
// MessageId: NS_E_DRM_LICENSE_NOTRUSTEDCODEC
//
// MessageText:
//
// The license for this file requires a higher level of security than the player you are currently using has. Try using a different player or download a newer version of your current player.%0
//
#define NS_E_DRM_LICENSE_NOTRUSTEDCODEC  0xC00D27E0L

//
// MessageId: NS_E_DRM_TOO_MANY_INSTANCES
//
// MessageText:
//
// The Digital Rights Management (DRM) sub-system can only be used by one process at a time.  Please close any other applications which may be using DRM, or navigate your browser away from any DRM-related activities, and then try again.%0
//
#define NS_E_DRM_TOO_MANY_INSTANCES      0xC00D27E1L

//
// MessageId: NS_E_DRM_ACQUIRE_LICENSE_VIA_BROWSER
//
// MessageText:
//
// The license for this file could not be acquired silently.  Your browser is being used to try and acquire the license.  Please reopen this file again after you have completed license acquisition.%0
//
#define NS_E_DRM_ACQUIRE_LICENSE_VIA_BROWSER 0xC00D27E2L


/////////////////////////////////////////////////////////////////////////
//
// Windows Media Networking Errors
//
// IdRange = 12000..12999
/////////////////////////////////////////////////////////////////////////
//
// MessageId: NS_E_UNKNOWN_PROTOCOL
//
// MessageText:
//
// The specified protocol is not supported.%0
//
#define NS_E_UNKNOWN_PROTOCOL            0xC00D2EE0L

//
// MessageId: NS_E_REDIRECT_TO_PROXY
//
// MessageText:
//
// The client is redirected to a proxy server.%0
//
#define NS_E_REDIRECT_TO_PROXY           0xC00D2EE1L

//
// MessageId: NS_E_INTERNAL_SERVER_ERROR
//
// MessageText:
//
// The server encountered an unexpected condition which prevented it from fulfilling the request.%0
//
#define NS_E_INTERNAL_SERVER_ERROR       0xC00D2EE2L

//
// MessageId: NS_E_BAD_REQUEST
//
// MessageText:
//
// The request could not be understood by the server.%0
//
#define NS_E_BAD_REQUEST                 0xC00D2EE3L

//
// MessageId: NS_E_ERROR_FROM_PROXY
//
// MessageText:
//
// The proxy experienced an error while attempting to contact the media server.%0
//
#define NS_E_ERROR_FROM_PROXY            0xC00D2EE4L

//
// MessageId: NS_E_PROXY_TIMEOUT
//
// MessageText:
//
// The proxy did not receive a timely response while attempting to contact the media server.%0
//
#define NS_E_PROXY_TIMEOUT               0xC00D2EE5L

//
// MessageId: NS_E_SERVER_UNAVAILABLE
//
// MessageText:
//
// The server is currently unable to handle the request due to a temporary overloading or maintenance of the server.%0
//
#define NS_E_SERVER_UNAVAILABLE          0xC00D2EE6L

//
// MessageId: NS_E_REFUSED_BY_SERVER
//
// MessageText:
//
// The server is refusing to fulfill the requested operation.%0
//
#define NS_E_REFUSED_BY_SERVER           0xC00D2EE7L

//
// MessageId: NS_E_INCOMPATIBLE_SERVER
//
// MessageText:
//
// The server is not a compatible streaming media server.%0
//
#define NS_E_INCOMPATIBLE_SERVER         0xC00D2EE8L

//
// MessageId: NS_E_MULTICAST_DISABLED
//
// MessageText:
//
// The content cannot be streamed because the Multicast protocol has been disabled.%0
//
#define NS_E_MULTICAST_DISABLED          0xC00D2EE9L

//
// MessageId: NS_E_INVALID_REDIRECT
//
// MessageText:
//
// The server redirected the player to an invalid location.%0
//
#define NS_E_INVALID_REDIRECT            0xC00D2EEAL

//
// MessageId: NS_E_ALL_PROTOCOLS_DISABLED
//
// MessageText:
//
// The content cannot be streamed because all protocols have been disabled.%0
//
#define NS_E_ALL_PROTOCOLS_DISABLED      0xC00D2EEBL

//
// MessageId: NS_E_MSBD_NO_LONGER_SUPPORTED
//
// MessageText:
//
// The MSBD protocol is no longer supported. Please use HTTP to connect to the Windows Media stream.%0
//
#define NS_E_MSBD_NO_LONGER_SUPPORTED    0xC00D2EECL

//
// MessageId: NS_E_PROXY_NOT_FOUND
//
// MessageText:
//
// The proxy server could not be located. Please check your proxy server configuration.%0
//
#define NS_E_PROXY_NOT_FOUND             0xC00D2EEDL

//
// MessageId: NS_E_CANNOT_CONNECT_TO_PROXY
//
// MessageText:
//
// Unable to establish a connection to the proxy server. Please check your proxy server configuration.%0
//
#define NS_E_CANNOT_CONNECT_TO_PROXY     0xC00D2EEEL

//
// MessageId: NS_E_SERVER_DNS_TIMEOUT
//
// MessageText:
//
// Unable to locate the media server. The operation timed out.%0
//
#define NS_E_SERVER_DNS_TIMEOUT          0xC00D2EEFL

//
// MessageId: NS_E_PROXY_DNS_TIMEOUT
//
// MessageText:
//
// Unable to locate the proxy server. The operation timed out.%0
//
#define NS_E_PROXY_DNS_TIMEOUT           0xC00D2EF0L

//
// MessageId: NS_E_CLOSED_ON_SUSPEND
//
// MessageText:
//
// Media closed because Windows was shut down.%0
//
#define NS_E_CLOSED_ON_SUSPEND           0xC00D2EF1L

//
// MessageId: NS_E_DRM_NO_SERVER_AVAILABLE
//
// MessageText:
//
// The server name or address could not be resolved.%0
//
#define NS_E_DRM_NO_SERVER_AVAILABLE     0xC00D2EF2L


#endif _NSERROR_H

