

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0543 */
/* Compiler settings for mpdvd.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __mpdvd_h__
#define __mpdvd_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IMediaPlayerDvd_FWD_DEFINED__
#define __IMediaPlayerDvd_FWD_DEFINED__
typedef interface IMediaPlayerDvd IMediaPlayerDvd;
#endif 	/* __IMediaPlayerDvd_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


/* interface __MIDL_itf_mpdvd_0000_0000 */
/* [local] */ 

typedef /* [public] */ 
enum DVD_DomainConstants
    {	dvdDomain_FirstPlay	= 1,
	dvdDomain_VideoManagerMenu	= ( dvdDomain_FirstPlay + 1 ) ,
	dvdDomain_VideoTitleSetMenu	= ( dvdDomain_VideoManagerMenu + 1 ) ,
	dvdDomain_Title	= ( dvdDomain_VideoTitleSetMenu + 1 ) ,
	dvdDomain_Stop	= ( dvdDomain_Title + 1 ) 
    } 	DVD_DomainConstants;

typedef /* [public] */ 
enum DVDMenuIDConstants
    {	dvdMenu_Title	= 2,
	dvdMenu_Root	= ( dvdMenu_Title + 1 ) ,
	dvdMenu_Subpicture	= ( dvdMenu_Root + 1 ) ,
	dvdMenu_Audio	= ( dvdMenu_Subpicture + 1 ) ,
	dvdMenu_Angle	= ( dvdMenu_Audio + 1 ) ,
	dvdMenu_Chapter	= ( dvdMenu_Angle + 1 ) 
    } 	DVDMenuIDConstants;

typedef /* [public] */ 
enum DVDEventConstants
    {	dvdEvent_DomainChange	= ( 0x100 + 0x1 ) ,
	dvdEvent_TitleChange	= ( 0x100 + 0x2 ) ,
	dvdEvent_ChapterStart	= ( 0x100 + 0x3 ) ,
	dvdEvent_AudioStreamChange	= ( 0x100 + 0x4 ) ,
	dvdEvent_SubPictureStreamChange	= ( 0x100 + 0x5 ) ,
	dvdEvent_AngleChange	= ( 0x100 + 0x6 ) ,
	dvdEvent_ButtonChange	= ( 0x100 + 0x7 ) ,
	dvdEvent_ValidUOPSChange	= ( 0x100 + 0x8 ) ,
	dvdEvent_StillOn	= ( 0x100 + 0x9 ) ,
	dvdEvent_StillOff	= ( 0x100 + 0xa ) ,
	dvdEvent_CurrentTime	= ( 0x100 + 0xb ) ,
	dvdEvent_Error	= ( 0x100 + 0xc ) ,
	dvdEvent_Warning	= ( 0x100 + 0xd ) ,
	dvdEvent_ChapterAutoStop	= ( 0x100 + 0xe ) ,
	dvdEvent_NoFirstPlayPGC	= ( 0x100 + 0xf ) ,
	dvdEvent_RateChange	= ( 0x100 + 0x10 ) ,
	dvdEvent_ParentalLevelChange	= ( 0x100 + 0x11 ) ,
	dvdEvent_PlaybackStopped	= ( 0x100 + 0x12 ) ,
	dvdEvent_AnglesAvailable	= ( 0x100 + 0x13 ) 
    } 	DVDEventConstants;



extern RPC_IF_HANDLE __MIDL_itf_mpdvd_0000_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_mpdvd_0000_0000_v0_0_s_ifspec;

#ifndef __IMediaPlayerDvd_INTERFACE_DEFINED__
#define __IMediaPlayerDvd_INTERFACE_DEFINED__

/* interface IMediaPlayerDvd */
/* [dual][uuid][object] */ 


EXTERN_C const IID IID_IMediaPlayerDvd;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("746EB440-3835-11d2-9774-0000F80855E6")
    IMediaPlayerDvd : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ButtonSelectAndActivate( 
            /* [in] */ ULONG uiButton) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE UpperButtonSelect( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE LowerButtonSelect( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE LeftButtonSelect( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RightButtonSelect( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ButtonActivate( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ForwardScan( 
            /* [in] */ double dwSpeed) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE BackwardScan( 
            /* [in] */ double dwSpeed) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE PrevPGSearch( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE TopPGSearch( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE NextPGSearch( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE TitlePlay( 
            /* [in] */ ULONG uiTitle) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ChapterPlay( 
            /* [in] */ ULONG uiTitle,
            /* [in] */ ULONG uiChapter) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ChapterSearch( 
            /* [in] */ ULONG Chapter) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE MenuCall( 
            /* [in] */ DVDMenuIDConstants MenuID) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ResumeFromMenu( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE TimePlay( 
            /* [in] */ ULONG uiTitle,
            /* [in] */ BSTR bstrTime) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE TimeSearch( 
            /* [in] */ BSTR bstrTime) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ChapterPlayAutoStop( 
            /* [in] */ ULONG ulTitle,
            /* [in] */ ULONG ulChapter,
            /* [in] */ ULONG ulChaptersToPlay) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE StillOff( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GoUp( void) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_TotalTitleTime( 
            /* [retval][out] */ BSTR *bstrTime) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetNumberOfChapters( 
            /* [in] */ ULONG ulTitle,
            /* [retval][out] */ ULONG *ulNumChapters) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetAudioLanguage( 
            /* [in] */ ULONG ulStream,
            /* [retval][out] */ BSTR *bstrAudioLang) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetSubpictureLanguage( 
            /* [in] */ ULONG ulStream,
            /* [retval][out] */ BSTR *bstrSubpictureLang) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetAllGPRMs( 
            /* [retval][out] */ VARIANT *vtGPRM) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetAllSPRMs( 
            /* [retval][out] */ VARIANT *vtSPRM) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE UOPValid( 
            /* [in] */ ULONG ulUOP,
            /* [retval][out] */ VARIANT_BOOL *bValid) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ButtonsAvailable( 
            /* [retval][out] */ ULONG *ulButtonsAvailable) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CurrentButton( 
            /* [retval][out] */ ULONG *ulCurrentButton) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_AudioStreamsAvailable( 
            /* [retval][out] */ ULONG *ulAudioStreamsAvailable) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CurrentAudioStream( 
            /* [retval][out] */ ULONG *ulAudioStream) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_CurrentAudioStream( 
            /* [in] */ ULONG ulAudioStream) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CurrentSubpictureStream( 
            /* [retval][out] */ ULONG *ulSubpictureStream) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_CurrentSubpictureStream( 
            /* [in] */ ULONG ulSubpictureStream) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SubpictureStreamsAvailable( 
            /* [retval][out] */ ULONG *ulNumSubpictureStreams) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SubpictureOn( 
            /* [retval][out] */ VARIANT_BOOL *bSubpictureON) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_SubpictureOn( 
            /* [in] */ VARIANT_BOOL bSubpictureON) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_AnglesAvailable( 
            /* [retval][out] */ ULONG *ulAnglesAvailable) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CurrentAngle( 
            /* [retval][out] */ ULONG *ulAngle) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_CurrentAngle( 
            /* [in] */ ULONG ulAngle) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CurrentTitle( 
            /* [retval][out] */ ULONG *ulTitle) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CurrentChapter( 
            /* [retval][out] */ ULONG *ulChapter) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CurrentTime( 
            /* [retval][out] */ BSTR *bstrTime) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_Root( 
            /* [in] */ BSTR bstrPath) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Root( 
            /* [retval][out] */ BSTR *pbstrPath) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_FramesPerSecond( 
            /* [retval][out] */ ULONG *ulFps) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CurrentDomain( 
            /* [retval][out] */ ULONG *ulDomain) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_TitlesAvailable( 
            /* [retval][out] */ ULONG *ulTitles) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_VolumesAvailable( 
            /* [retval][out] */ ULONG *pulVolumes) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CurrentVolume( 
            /* [retval][out] */ ULONG *pulVolume) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CurrentDiscSide( 
            /* [retval][out] */ ULONG *pulDiscSide) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CCActive( 
            /* [retval][out] */ VARIANT_BOOL *bCCActive) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_CCActive( 
            /* [in] */ VARIANT_BOOL bCCActive) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CurrentCCService( 
            /* [retval][out] */ ULONG *pulService) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_CurrentCCService( 
            /* [in] */ ULONG ulService) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_UniqueID( 
            /* [retval][out] */ BSTR *pvtUniqueID) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_ColorKey( 
            /* [retval][out] */ ULONG *pClr) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_ColorKey( 
            /* [in] */ ULONG Clr) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMediaPlayerDvdVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMediaPlayerDvd * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMediaPlayerDvd * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMediaPlayerDvd * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IMediaPlayerDvd * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IMediaPlayerDvd * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IMediaPlayerDvd * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IMediaPlayerDvd * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ButtonSelectAndActivate )( 
            IMediaPlayerDvd * This,
            /* [in] */ ULONG uiButton);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *UpperButtonSelect )( 
            IMediaPlayerDvd * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *LowerButtonSelect )( 
            IMediaPlayerDvd * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *LeftButtonSelect )( 
            IMediaPlayerDvd * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RightButtonSelect )( 
            IMediaPlayerDvd * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ButtonActivate )( 
            IMediaPlayerDvd * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ForwardScan )( 
            IMediaPlayerDvd * This,
            /* [in] */ double dwSpeed);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *BackwardScan )( 
            IMediaPlayerDvd * This,
            /* [in] */ double dwSpeed);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *PrevPGSearch )( 
            IMediaPlayerDvd * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *TopPGSearch )( 
            IMediaPlayerDvd * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *NextPGSearch )( 
            IMediaPlayerDvd * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *TitlePlay )( 
            IMediaPlayerDvd * This,
            /* [in] */ ULONG uiTitle);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ChapterPlay )( 
            IMediaPlayerDvd * This,
            /* [in] */ ULONG uiTitle,
            /* [in] */ ULONG uiChapter);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ChapterSearch )( 
            IMediaPlayerDvd * This,
            /* [in] */ ULONG Chapter);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *MenuCall )( 
            IMediaPlayerDvd * This,
            /* [in] */ DVDMenuIDConstants MenuID);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ResumeFromMenu )( 
            IMediaPlayerDvd * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *TimePlay )( 
            IMediaPlayerDvd * This,
            /* [in] */ ULONG uiTitle,
            /* [in] */ BSTR bstrTime);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *TimeSearch )( 
            IMediaPlayerDvd * This,
            /* [in] */ BSTR bstrTime);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ChapterPlayAutoStop )( 
            IMediaPlayerDvd * This,
            /* [in] */ ULONG ulTitle,
            /* [in] */ ULONG ulChapter,
            /* [in] */ ULONG ulChaptersToPlay);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *StillOff )( 
            IMediaPlayerDvd * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GoUp )( 
            IMediaPlayerDvd * This);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TotalTitleTime )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ BSTR *bstrTime);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetNumberOfChapters )( 
            IMediaPlayerDvd * This,
            /* [in] */ ULONG ulTitle,
            /* [retval][out] */ ULONG *ulNumChapters);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetAudioLanguage )( 
            IMediaPlayerDvd * This,
            /* [in] */ ULONG ulStream,
            /* [retval][out] */ BSTR *bstrAudioLang);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetSubpictureLanguage )( 
            IMediaPlayerDvd * This,
            /* [in] */ ULONG ulStream,
            /* [retval][out] */ BSTR *bstrSubpictureLang);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetAllGPRMs )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ VARIANT *vtGPRM);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetAllSPRMs )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ VARIANT *vtSPRM);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *UOPValid )( 
            IMediaPlayerDvd * This,
            /* [in] */ ULONG ulUOP,
            /* [retval][out] */ VARIANT_BOOL *bValid);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ButtonsAvailable )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ ULONG *ulButtonsAvailable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentButton )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ ULONG *ulCurrentButton);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AudioStreamsAvailable )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ ULONG *ulAudioStreamsAvailable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentAudioStream )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ ULONG *ulAudioStream);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_CurrentAudioStream )( 
            IMediaPlayerDvd * This,
            /* [in] */ ULONG ulAudioStream);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentSubpictureStream )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ ULONG *ulSubpictureStream);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_CurrentSubpictureStream )( 
            IMediaPlayerDvd * This,
            /* [in] */ ULONG ulSubpictureStream);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SubpictureStreamsAvailable )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ ULONG *ulNumSubpictureStreams);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SubpictureOn )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ VARIANT_BOOL *bSubpictureON);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SubpictureOn )( 
            IMediaPlayerDvd * This,
            /* [in] */ VARIANT_BOOL bSubpictureON);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AnglesAvailable )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ ULONG *ulAnglesAvailable);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentAngle )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ ULONG *ulAngle);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_CurrentAngle )( 
            IMediaPlayerDvd * This,
            /* [in] */ ULONG ulAngle);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentTitle )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ ULONG *ulTitle);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentChapter )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ ULONG *ulChapter);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentTime )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ BSTR *bstrTime);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Root )( 
            IMediaPlayerDvd * This,
            /* [in] */ BSTR bstrPath);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Root )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ BSTR *pbstrPath);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_FramesPerSecond )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ ULONG *ulFps);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentDomain )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ ULONG *ulDomain);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TitlesAvailable )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ ULONG *ulTitles);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_VolumesAvailable )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ ULONG *pulVolumes);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentVolume )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ ULONG *pulVolume);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentDiscSide )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ ULONG *pulDiscSide);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CCActive )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ VARIANT_BOOL *bCCActive);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_CCActive )( 
            IMediaPlayerDvd * This,
            /* [in] */ VARIANT_BOOL bCCActive);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentCCService )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ ULONG *pulService);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_CurrentCCService )( 
            IMediaPlayerDvd * This,
            /* [in] */ ULONG ulService);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_UniqueID )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ BSTR *pvtUniqueID);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ColorKey )( 
            IMediaPlayerDvd * This,
            /* [retval][out] */ ULONG *pClr);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ColorKey )( 
            IMediaPlayerDvd * This,
            /* [in] */ ULONG Clr);
        
        END_INTERFACE
    } IMediaPlayerDvdVtbl;

    interface IMediaPlayerDvd
    {
        CONST_VTBL struct IMediaPlayerDvdVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMediaPlayerDvd_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMediaPlayerDvd_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMediaPlayerDvd_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IMediaPlayerDvd_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IMediaPlayerDvd_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IMediaPlayerDvd_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IMediaPlayerDvd_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IMediaPlayerDvd_ButtonSelectAndActivate(This,uiButton)	\
    ( (This)->lpVtbl -> ButtonSelectAndActivate(This,uiButton) ) 

#define IMediaPlayerDvd_UpperButtonSelect(This)	\
    ( (This)->lpVtbl -> UpperButtonSelect(This) ) 

#define IMediaPlayerDvd_LowerButtonSelect(This)	\
    ( (This)->lpVtbl -> LowerButtonSelect(This) ) 

#define IMediaPlayerDvd_LeftButtonSelect(This)	\
    ( (This)->lpVtbl -> LeftButtonSelect(This) ) 

#define IMediaPlayerDvd_RightButtonSelect(This)	\
    ( (This)->lpVtbl -> RightButtonSelect(This) ) 

#define IMediaPlayerDvd_ButtonActivate(This)	\
    ( (This)->lpVtbl -> ButtonActivate(This) ) 

#define IMediaPlayerDvd_ForwardScan(This,dwSpeed)	\
    ( (This)->lpVtbl -> ForwardScan(This,dwSpeed) ) 

#define IMediaPlayerDvd_BackwardScan(This,dwSpeed)	\
    ( (This)->lpVtbl -> BackwardScan(This,dwSpeed) ) 

#define IMediaPlayerDvd_PrevPGSearch(This)	\
    ( (This)->lpVtbl -> PrevPGSearch(This) ) 

#define IMediaPlayerDvd_TopPGSearch(This)	\
    ( (This)->lpVtbl -> TopPGSearch(This) ) 

#define IMediaPlayerDvd_NextPGSearch(This)	\
    ( (This)->lpVtbl -> NextPGSearch(This) ) 

#define IMediaPlayerDvd_TitlePlay(This,uiTitle)	\
    ( (This)->lpVtbl -> TitlePlay(This,uiTitle) ) 

#define IMediaPlayerDvd_ChapterPlay(This,uiTitle,uiChapter)	\
    ( (This)->lpVtbl -> ChapterPlay(This,uiTitle,uiChapter) ) 

#define IMediaPlayerDvd_ChapterSearch(This,Chapter)	\
    ( (This)->lpVtbl -> ChapterSearch(This,Chapter) ) 

#define IMediaPlayerDvd_MenuCall(This,MenuID)	\
    ( (This)->lpVtbl -> MenuCall(This,MenuID) ) 

#define IMediaPlayerDvd_ResumeFromMenu(This)	\
    ( (This)->lpVtbl -> ResumeFromMenu(This) ) 

#define IMediaPlayerDvd_TimePlay(This,uiTitle,bstrTime)	\
    ( (This)->lpVtbl -> TimePlay(This,uiTitle,bstrTime) ) 

#define IMediaPlayerDvd_TimeSearch(This,bstrTime)	\
    ( (This)->lpVtbl -> TimeSearch(This,bstrTime) ) 

#define IMediaPlayerDvd_ChapterPlayAutoStop(This,ulTitle,ulChapter,ulChaptersToPlay)	\
    ( (This)->lpVtbl -> ChapterPlayAutoStop(This,ulTitle,ulChapter,ulChaptersToPlay) ) 

#define IMediaPlayerDvd_StillOff(This)	\
    ( (This)->lpVtbl -> StillOff(This) ) 

#define IMediaPlayerDvd_GoUp(This)	\
    ( (This)->lpVtbl -> GoUp(This) ) 

#define IMediaPlayerDvd_get_TotalTitleTime(This,bstrTime)	\
    ( (This)->lpVtbl -> get_TotalTitleTime(This,bstrTime) ) 

#define IMediaPlayerDvd_GetNumberOfChapters(This,ulTitle,ulNumChapters)	\
    ( (This)->lpVtbl -> GetNumberOfChapters(This,ulTitle,ulNumChapters) ) 

#define IMediaPlayerDvd_GetAudioLanguage(This,ulStream,bstrAudioLang)	\
    ( (This)->lpVtbl -> GetAudioLanguage(This,ulStream,bstrAudioLang) ) 

#define IMediaPlayerDvd_GetSubpictureLanguage(This,ulStream,bstrSubpictureLang)	\
    ( (This)->lpVtbl -> GetSubpictureLanguage(This,ulStream,bstrSubpictureLang) ) 

#define IMediaPlayerDvd_GetAllGPRMs(This,vtGPRM)	\
    ( (This)->lpVtbl -> GetAllGPRMs(This,vtGPRM) ) 

#define IMediaPlayerDvd_GetAllSPRMs(This,vtSPRM)	\
    ( (This)->lpVtbl -> GetAllSPRMs(This,vtSPRM) ) 

#define IMediaPlayerDvd_UOPValid(This,ulUOP,bValid)	\
    ( (This)->lpVtbl -> UOPValid(This,ulUOP,bValid) ) 

#define IMediaPlayerDvd_get_ButtonsAvailable(This,ulButtonsAvailable)	\
    ( (This)->lpVtbl -> get_ButtonsAvailable(This,ulButtonsAvailable) ) 

#define IMediaPlayerDvd_get_CurrentButton(This,ulCurrentButton)	\
    ( (This)->lpVtbl -> get_CurrentButton(This,ulCurrentButton) ) 

#define IMediaPlayerDvd_get_AudioStreamsAvailable(This,ulAudioStreamsAvailable)	\
    ( (This)->lpVtbl -> get_AudioStreamsAvailable(This,ulAudioStreamsAvailable) ) 

#define IMediaPlayerDvd_get_CurrentAudioStream(This,ulAudioStream)	\
    ( (This)->lpVtbl -> get_CurrentAudioStream(This,ulAudioStream) ) 

#define IMediaPlayerDvd_put_CurrentAudioStream(This,ulAudioStream)	\
    ( (This)->lpVtbl -> put_CurrentAudioStream(This,ulAudioStream) ) 

#define IMediaPlayerDvd_get_CurrentSubpictureStream(This,ulSubpictureStream)	\
    ( (This)->lpVtbl -> get_CurrentSubpictureStream(This,ulSubpictureStream) ) 

#define IMediaPlayerDvd_put_CurrentSubpictureStream(This,ulSubpictureStream)	\
    ( (This)->lpVtbl -> put_CurrentSubpictureStream(This,ulSubpictureStream) ) 

#define IMediaPlayerDvd_get_SubpictureStreamsAvailable(This,ulNumSubpictureStreams)	\
    ( (This)->lpVtbl -> get_SubpictureStreamsAvailable(This,ulNumSubpictureStreams) ) 

#define IMediaPlayerDvd_get_SubpictureOn(This,bSubpictureON)	\
    ( (This)->lpVtbl -> get_SubpictureOn(This,bSubpictureON) ) 

#define IMediaPlayerDvd_put_SubpictureOn(This,bSubpictureON)	\
    ( (This)->lpVtbl -> put_SubpictureOn(This,bSubpictureON) ) 

#define IMediaPlayerDvd_get_AnglesAvailable(This,ulAnglesAvailable)	\
    ( (This)->lpVtbl -> get_AnglesAvailable(This,ulAnglesAvailable) ) 

#define IMediaPlayerDvd_get_CurrentAngle(This,ulAngle)	\
    ( (This)->lpVtbl -> get_CurrentAngle(This,ulAngle) ) 

#define IMediaPlayerDvd_put_CurrentAngle(This,ulAngle)	\
    ( (This)->lpVtbl -> put_CurrentAngle(This,ulAngle) ) 

#define IMediaPlayerDvd_get_CurrentTitle(This,ulTitle)	\
    ( (This)->lpVtbl -> get_CurrentTitle(This,ulTitle) ) 

#define IMediaPlayerDvd_get_CurrentChapter(This,ulChapter)	\
    ( (This)->lpVtbl -> get_CurrentChapter(This,ulChapter) ) 

#define IMediaPlayerDvd_get_CurrentTime(This,bstrTime)	\
    ( (This)->lpVtbl -> get_CurrentTime(This,bstrTime) ) 

#define IMediaPlayerDvd_put_Root(This,bstrPath)	\
    ( (This)->lpVtbl -> put_Root(This,bstrPath) ) 

#define IMediaPlayerDvd_get_Root(This,pbstrPath)	\
    ( (This)->lpVtbl -> get_Root(This,pbstrPath) ) 

#define IMediaPlayerDvd_get_FramesPerSecond(This,ulFps)	\
    ( (This)->lpVtbl -> get_FramesPerSecond(This,ulFps) ) 

#define IMediaPlayerDvd_get_CurrentDomain(This,ulDomain)	\
    ( (This)->lpVtbl -> get_CurrentDomain(This,ulDomain) ) 

#define IMediaPlayerDvd_get_TitlesAvailable(This,ulTitles)	\
    ( (This)->lpVtbl -> get_TitlesAvailable(This,ulTitles) ) 

#define IMediaPlayerDvd_get_VolumesAvailable(This,pulVolumes)	\
    ( (This)->lpVtbl -> get_VolumesAvailable(This,pulVolumes) ) 

#define IMediaPlayerDvd_get_CurrentVolume(This,pulVolume)	\
    ( (This)->lpVtbl -> get_CurrentVolume(This,pulVolume) ) 

#define IMediaPlayerDvd_get_CurrentDiscSide(This,pulDiscSide)	\
    ( (This)->lpVtbl -> get_CurrentDiscSide(This,pulDiscSide) ) 

#define IMediaPlayerDvd_get_CCActive(This,bCCActive)	\
    ( (This)->lpVtbl -> get_CCActive(This,bCCActive) ) 

#define IMediaPlayerDvd_put_CCActive(This,bCCActive)	\
    ( (This)->lpVtbl -> put_CCActive(This,bCCActive) ) 

#define IMediaPlayerDvd_get_CurrentCCService(This,pulService)	\
    ( (This)->lpVtbl -> get_CurrentCCService(This,pulService) ) 

#define IMediaPlayerDvd_put_CurrentCCService(This,ulService)	\
    ( (This)->lpVtbl -> put_CurrentCCService(This,ulService) ) 

#define IMediaPlayerDvd_get_UniqueID(This,pvtUniqueID)	\
    ( (This)->lpVtbl -> get_UniqueID(This,pvtUniqueID) ) 

#define IMediaPlayerDvd_get_ColorKey(This,pClr)	\
    ( (This)->lpVtbl -> get_ColorKey(This,pClr) ) 

#define IMediaPlayerDvd_put_ColorKey(This,Clr)	\
    ( (This)->lpVtbl -> put_ColorKey(This,Clr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMediaPlayerDvd_INTERFACE_DEFINED__ */


/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long *, unsigned long            , VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserMarshal(  unsigned long *, unsigned char *, VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserUnmarshal(unsigned long *, unsigned char *, VARIANT * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long *, VARIANT * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


