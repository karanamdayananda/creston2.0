        ��  ��                  �-     �� ��     0         }       "   �  $   $   �  &   '     2   H   P  P   P   �  R   Y      d   r   t  u   �   �  �   �   0-  �   �   t-  �   �   �-  �   �   �-  �   �   �.  �   �   �.  �   �   $/  �   �   H3  �   �   �4  �   �   5  �   �   6  �   �   �6  �   �   7      �7  
    �7       8      $9        t9  *  +  �9  =  =  P:  �  �  �:      �:  �  �  �;  �  �  0=    :  TF  L  l  �O  t  v  4Z  ~  �  l[  �  �  da    :   o  <  s  <{  x  �  �  �  �  t�  �  �  t�  �  �  0�  �  �  x�  �  �  ��  �  (  �  j  ~  8�  �  �  �  <  <  d�  �  �  �  �  �  4�  a	  b	  `�  d	  d	  ܾ  �  �  @�  �  �  ��      D�  �  �  ��  �  �  ��     �       \�     ��  0 0 P�  @ @ |�  p r ��  � �  �  � � ��  � � ��  � �  �  � � L�      �    �
  �T�  @ �!@ ���   � ���   ����  �������   � �(�   � �D�  �����  ����4�  ������  '��/����  ������t�  ň�ƈ���  ψ�ψ���  ������,�  J��J����  ��������   � �D�   �	 �<�   � ���   � ���   � ��    �! �d�  P �P ���  W �W ���  p �p ���  � �� �$�  � �	���  ����  ��,�    � ���  d �m �H�   ���  ��p @�@�$ P�U�H `�a�\ p�p�� ����� ���� ����t ����8 ����  � �,  � �` W �W �|  �	 ��  � �L  	�! 	�� 	�	��  	� 	�   	�# 	� %  0	� 0	�@&  � �'  ��*                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     De bewerking is voltooid.
    Onjuiste functie.
 <   Het systeem kan het opgegeven bestand niet vinden.
    8   Het systeem kan het opgegeven pad niet vinden.
    0   Het systeem kan het bestand niet openen.
     Toegang is geweigerd.
     De ingang is ongeldig.
    0   De opslagbeheerblokken zijn vernietigd.
   L   Onvoldoende opslagruimte beschikbaar om deze opdracht te verwerken.
   8   Het adres van het opslagbeheerblok is ongeldig.
   ,   De verwerkingsomgeving is onjuist.
    D   Poging om een programma te laden met een
onjuiste indeling.
  $   De toegangscode is ongeldig.
  $   De gegevens zijn ongeldig.
    L   Onvoldoende opslagruimte beschikbaar om deze bewerking te voltooien.
  <   Het systeem kan het opgegeven station niet vinden.
    ,   De map kan niet worden verwijderd.
    L   Het systeem kan het bestand niet verplaatsen
naar een ander station.
 $   Er zijn geen bestanden meer.
  0   Het medium is tegen schrijven beveiligd.
  <   Het systeem kan het opgegeven apparaat niet vinden.
   $   Het apparaat is niet klaar.
   0   Het apparaat herkent de opdracht niet.
    4   Gegevensfout (cyclische redundantiecontrole).
 `   Het programma heeft een opdracht verzonden, maar de
lengte van de opdracht is onjuist.
   P   Het station kan een bepaald gebied of spoor
op de schijf niet vinden.
    H   Kan geen toegang verkrijgen tot de opgegeven schijf of diskette.
  8   Het station kan de opgegeven sector niet vinden.
  <   De papierlade van de printer moet worden bijgevuld.
   D   Het systeem kan niet naar het opgegeven apparaat schrijven.
   @   Het systeem kan niet van het opgegeven apparaat lezen.
    D   Een apparaat dat op het systeem is aangesloten, werkt niet.
   p   Het proces heeft geen toegang tot het bestand omdat
het bestand door een ander proces wordt gebruikt.
    �   Het proces heeft geen toegang tot het bestand omdat
een gedeelte van het bestand is vergrendeld door een ander proces.
   `   Het station bevat een onjuiste diskette.
Plaats %2 (volumenummer: %3)
in station %1.
    ,   Te veel bestanden voor delen geopend.
 ,   Het einde van het bestand is bereikt.
    De schijf is vol.
 4   De netwerkopdracht wordt niet ondersteund.
    4   De computer op afstand is niet beschikbaar.
   4   Een identieke naam bestaat al op het netwerk.
 (   Het netwerkpad is niet gevonden.
     Het netwerk is bezet.
 X   De opgegeven bron of het opgegeven netwerkapparaat  is niet langer
beschikbaar.
  @   Het maximale aantal netwerk-BIOS-opdrachten is bereikt.
      Netwerkadapterfout.
   H   De opgegeven server kan de gevraagde bewerking niet
uitvoeren.
       Onverwachte netwerkfout.
  0   De externe adapter is niet compatibel.
    $   De printerwachtrij is vol.
    p   Er is geen ruimte beschikbaar op de server om het in de afdrukwachtrij geplaatste bestand
op te slaan.
   P   Het bestand dat door u in de afdrukwachtrij is geplaatst, is verwijderd.
  <   De opgegeven netwerknaam is niet langer beschikbaar.
  (   Toegang tot netwerk is geweigerd.
     Het brontype is onjuist.
  $   Kan netwerknaam niet vinden.
  d   De limiet voor de naam van de netwerkadapterkaart voor de
lokale computer is overschreden.
   H   De limiet voor het aantal netwerk-BIOS-sessies is overschreden.
   D   De externe server is onderbroken
of wordt momenteel gestart.
 �   Er kunnen op dit moment niet meer verbindingen worden gemaakt met deze externe computer
omdat het aantal mogelijke verbindingen voor de computer al is bereikt.
  D   De opgegeven printer of het schijfapparaat is onderbroken.
       Het bestand bestaat.
  ,   Kan de map of het bestand niet maken.
    Fout op INT 24.
   L   Er is geen opslagruimte beschikbaar om deze opdracht te verwerken.
    0   De lokale apparaatnaam wordt al gebruikt.
 4   Het opgegeven netwerkwachtwoord is onjuist.
       De parameter is onjuist.
  $   Schrijffout op het netwerk.
   <   Het systeem kan momenteel geen
ander proces starten.
 0   Kan geen andere systeemsemafoor maken.
    <   De exclusieve semafoor hoort bij een ander proces.
    <   De semafoor is ingesteld en kan niet worden gesloten.
 8   De semafoor kan niet opnieuw worden ingesteld.
    D   Kan geen exclusieve semaforen opvragen tijdens een interrupt.
 @   Het vorige eigenaarschap van deze semafoor is opgeheven.
  (   Plaats de diskette in station %1.
 L   Het programma is gestopt omdat er geen andere diskette is geplaatst.
  L   De schijf wordt al gebruikt of is vergrendeld door
een ander proces.
     De sluis is be�indigd.
    H   Het systeem kan het opgegeven
apparaat of bestand niet openen.
   $   De bestandsnaam is te lang.
       Onvoldoende schijfruimte.
 D   Er zijn geen interne bestandsaanduidingen meer beschikbaar.
   H   De interne bestandsaanduiding voor  het doelbestand is onjuist.
   <   Het door de toepassing gestuurde signaal is
onjuist.
 `   De parameterwaarde voor de schakeloptie controleren-tijdens-schrijfbewerking is
onjuist.
 <   Het systeem ondersteunt de opgegeven opdracht niet.
   4   Deze functie is alleen geldig in Win32-modus.
 8   De time-outperiode van de semafoor is verlopen.
   P   Het gegevensgebied dat aan een systeemoproep is doorgegeven is te
klein.
 L   De syntaxis van de bestandsnaam, mapnaam of volumenaam is onjuist.
    ,   Het systeemoproepniveau is onjuist.
   (   De schijf heeft geen volumenaam.
  (   Kan opgegeven module niet vinden.
 ,   Kan opgegeven procedure niet vinden.
  @   Er zijn geen subprocessen waar op moet worden gewacht.
    D   De toepassing %1 kan niet worden uitgevoerd in Win32-modus.
   �   Poging een bestandsingang voor een open schijfpartitie te
gebruiken voor een andere bewerking dan onbewerkte schijf-I/O.
 X   Er is geprobeerd de bestandsaanwijzer voor het begin van het bestand te plaatsen.
 \   De bestandsaanwijzer kan niet worden ingesteld op het opgegeven apparaat of bestand.
  t   U kunt een JOIN of SUBST-opdracht
niet gebruiken voor een station dat
eerder samengevoegde stations bevat.
  |   Er is geprobeerd de opdracht
JOIN of SUBST te gebruiken voor een station waarop de opdracht
JOIN al is uitgevoerd.
  |   Er is geprobeerd de opdracht
JOIN of SUBST te gebruiken voor een station waarop de opdracht
SUBST al is uitgevoerd.
 �   Het systeem heeft geprobeerd de opdracht JOIN
ongedaan te maken op een station waarop de opdracht JOIN niet is uitgevoerd.
   �   Het systeem heeft geprobeerd de opdracht SUBST
ongedaan te maken op een station waarop de opdracht SUBST niet is uitgevoerd.
 l   Het systeem heeft geprobeerd een station samen te voegen
met een map op een samengevoegd station.
    �   Het systeem heeft geprobeerd de opdracht SUBST te gebruiken
op een station met een map op een station waarop SUBST al is uitgevoerd.
 x   Het systeem heeft geprobeerd een station samen te voegen met
een map op een station waarop SUBST is uitgevoerd.
  |   Het systeem heeft geprobeerd de opdracht SUBST uit te voeren op een station
met een map op een samengevoegd station.
 L   Het systeem kan de opdrachten JOIN of SUBST momenteel niet uitvoeren.
 t   Het systeem kan de opdracht JOIN of SUBST niet uitvoeren op een station
met een map op hetzelfde station.
    0   De map is geen submap van de hoofdmap.
       De map is niet leeg.
  @   Het opgegeven pad wordt al gebruikt in
een vervanging.
   H   Onvoldoende bronnen beschikbaar om
deze opdracht te verwerken.
   <   Het opgegeven pad kan momenteel niet worden gebruikt.
 �   Er is geprobeerd de opdracht JOIN
of SUBST uit te voeren op een station waarvan een map
op het station de doellocatie is van een eerdere
vervanging.
   t   Tracegegevens van het systeem zijn niet in het
bestand Config.sys opgegeven of traceren is niet toegestaan.
  T   Het aantal opgegeven semafoorgebeurtenissen voor
DosMuxSemWait is onjuist.
   T   DosMuxSemWait is niet uitgevoerd. Er zijn al te veel semaforen
ingesteld.
    ,   De DosMuxSemWait-lijst is onjuist.
    l   De door u ingevoerde volumenaam overschrijdt het
maximum aantal tekens van het doelbestandssysteem.
  (   Kan niet nog een thread maken.
    <   Het ontvangende proces heeft het signaal geweigerd.
   D   Het segment is al verwijderd en kan niet worden vergrendeld.
  (   Het segment is al ontgrendeld.
    0   Het adres voor de thread-ID is onjuist.
   H   De reeks argumenten die aan DosExecPgm is doorgegeven is onjuist.
 (   Het opgegeven pad is ongeldig.
    ,   Er is al een signaal in behandeling.
  D   Er kunnen niet meer threads in het systeem worden gemaakt.
    8   Kan een regio van een bestand niet vergrendelen.
  $   De opgegeven bron is bezet.
   T   Er is geen vergrendelingsopdracht gegeven voor de geleverde annuleringsregio.
 \   Het bestandssysteem ondersteunt geen atomische wijzigingen in het vergrendelingstype.
 @   Het systeem heeft een onjuist segmentnummer aangetroffen.
 4   Het besturingssysteem kan %1 niet uitvoeren.
  0   Kan geen bestand maken dat al bestaat.
    ,   De doorgegeven markering is onjuist.
  <   De opgegeven systeemsemafoornaam is niet gevonden.
    4   Het besturingssysteem kan %1 niet uitvoeren.
  4   Het besturingssysteem kan %1 niet uitvoeren.
  4   Het besturingssysteem kan %1 niet uitvoeren.
  ,   Kan %1 niet uitvoeren in Win32-modus.
 4   Het besturingssysteem kan %1 niet uitvoeren.
  ,   %1 is geen geldige Win32-toepassing.
  4   Het besturingssysteem kan %1 niet uitvoeren.
  4   Het besturingssysteem kan %1 niet uitvoeren.
  L   Het besturingssysteem kan dit toepassingsprogramma
niet uitvoeren.
   l   Het besturingssysteem heeft momenteel niet de juiste
configuratie om deze toepassing uit te voeren.
  4   Het besturingssysteem kan %1 niet uitvoeren.
  L   Het besturingssysteem kan dit toepassingsprogramma
niet uitvoeren.
   L   Het codesegment kan niet groter zijn dan of gelijk zijn aan 64 kB.
    4   Het besturingssysteem kan %1 niet uitvoeren.
  4   Het besturingssysteem kan %1 niet uitvoeren.
  @   Het systeem kan de opgegeven omgevingsoptie
niet vinden.
 L   Er is geen proces in de opdrachtsubstructuur met een
signaalingang.
  4   De bestandsnaam of -toevoeging is te lang.
    (   De ring-2-stack wordt gebruikt.
   |   De globale bestandsnaamtekens * of ? zijn onjuist gebruikt
of er zijn te veel globale bestandsnaamtekens opgegeven.
  4   Het signaal dat wordt verzonden is onjuist.
   (   Kan signaalingang niet instellen.
 H   Het segment is vergrendeld en kan niet opnieuw worden toegewezen.
 h   Er zijn te veel dynamic-link modules gekoppeld aan dit
programma of deze dynamic-link-module.
    ,   Kan LoadModule-oproepen niet nesten.
  l   Het afbeeldingsbestand %1 is geldig, maar is bedoeld voor een ander type computer
dan deze computer.
 ,   De status van de sluis is ongeldig.
       Alle sluizen zijn bezet.
      De sluis wordt gesloten.
  @   Er is geen proces aan het andere uiteinde van de sluis.
   ,   Er zijn meer gegevens beschikbaar.
        De sessie is geannuleerd.
 D   De opgegeven naam van het uitgebreide kenmerk is ongeldig.
    4   De uitgebreide kenmerken zijn inconsistent.
   (   Niet meer gegevens beschikbaar.
   8   De kopieerfuncties kunnen niet worden gebruikt.
       De mapnaam is ongeldig.
   <   De uitgebreide kenmerken passen niet in de buffer.
    `   Het bestand met uitgebreide kenmerken in het gekoppelde bestandssysteem is beschadigd.
    D   Het bestand met de tabel met uitgebreide kenmerken is vol.
    D   De opgegeven ingang voor uitgebreide kenmerken is ongeldig.
   P   Het gekoppelde bestandssysteem ondersteunt geen uitgebreide kenmerken.
    L   Poging tot loslaten van mutex die geen eigendom is van de aanvrager.
  @   Er zijn te veel transacties naar een semafoor verricht.
   P   Een Read/WriteProcessMemory-opdracht is slechts gedeeltelijk uitgevoerd.
  X   Kan het bericht voor berichtnummer 0x%1
niet vinden in berichtenbestand voor %2.
 @   Poging om toegang te verkrijgen tot een ongeldig adres.
   8   Rekenkundige resultaten zijn groter dan 32-bits.
  <   Er is een proces aan het andere eind van de sluis.
    H   Wacht tot een proces het andere eind van de sluis heeft geopend.
  <   Toegang tot de uitgebreide kenmerken is geweigerd.
    x   De I/O-bewerking is afgebroken vanwege het afsluiten van een thread
of vanwege een opdracht van een toepassing.
  D   Overlappende I/O-gebeurtenis heeft geen gesignaleerde status.
 4   Overlappende I/O-bewerking wordt uitgevoerd.
  0   Ongeldige toegang tot geheugenlocatie.
    0   Fout tijdens een bewerking in een pagina.
 0   Recursie te diep. Stack is overgelopen.
   8   Het venster werkt niet op het verzonden bericht.
  (   Kan deze functie niet voltooien.
      Ongeldige markeringen.
    �   Het volume bevat geen herkenbaar bestandssysteem. 
Controleer of alle vereiste bestandssysteemstuurprogramma's zijn geladen en of het 
volume niet is beschadigd.
   t   Het volume voor een bestand is extern dusdanig gewijzigd dat het 
geopende bestand niet langer geldig is.
    P   De gevraagde bewerking kan niet worden uitgevoerd in een volledig scherm.
 T   Er is geprobeerd te verwijzen naar een communicatiesymbool dat niet bestaat.
  8   De configuratieregisterdatabase is beschadigd.
    4   De configuratieregistersleutel is ongeldig.
   8   Kan de configuratieregistersleutel niet openen.
   8   Kan de configuratieregistersleutel niet lezen.
    <   Kan de configuratieregistersleutel niet schrijven.
    �   E�n van de bestanden in de registerdatabase moest worden hersteld
door middel van een logboek of een plaatsvervangende kopie. De herstelbewerking is geslaagd.
     Het register is beschadigd. De structuur van een van de bestanden die
registergegevens bevatten is beschadigd, het systeembeeld van het bestand in het geheugen
is beschadigd of het bestand kon niet worden hersteld omdat de andere
kopie of het log afwezig of beschadigd was.
  �   Onherstelbare fout bij I/O-bewerking die door het register is ge�nitialiseerd.
Het register kan ��n van de bestanden die het beeld bevatten dat het systeem van 
het register heeft, niet inlezen, wegschrijven of leegmaken.
   �   Het systeem heeft geprobeerd een bestand te laden of terug te zetten in het  register, maar het
opgegeven bestand heeft geen registerbestandsindeling.
   d   Ongeldige bewerking geprobeerd op een registersleutel die is gemarkeerd voor verwijdering.
    T   Het systeem kan de benodigde ruimte niet toewijzen in een registerlogboek.
    p   Poging om een symbolische koppeling te maken in een registersleutel die
al subsleutels of waarden heeft.
 X   Kan geen stabiele subsleutel maken onder een tijdelijke bovenliggende sleutel.
    �   Een opdracht om wijzigingen te melden wordt voltooid en de gegevens
 zijn niet in de buffer van de aanvrager geplaatst. De aanvrager moet nu
de bestanden weergeven om de wijzigingen te vinden.
    h   Er is een stopcode gestuurd naar een service waarvan andere gestarte services
afhankelijk zijn.
  H   Het opgegeven besturingselement is niet geldig voor deze service.
 T   De service heeft de start- of stuuropdracht niet op juiste wijze
beantwoord.
 0   Kan geen thread maken voor de service.
    ,   De servicedatabase is vergrendeld.
        De service is al gestart.
 4   De accountnaam is ongeldig of bestaat niet.
   H   De opgegeven service is uitgeschakeld en kan niet worden gestart.
 @   Er is een circulaire service-afhankelijkheid opgegeven.
   <   De opgegeven service is geen ge�nstalleerde service.
  D   De service kan momenteel geen besturingsberichten accepteren.
 $   De service is niet gestart.
   X   Het serviceproces kan geen verbinding tot stand brengen met de servicecontroller.
 P   Uitzondering in de service bij het verwerken van de besturingsopdracht.
   ,   De opgegeven database bestaat niet.
   H   De service heeft een servicespecifieke foutcode geretourneerd.
    ,   Het proces is onverwacht be�indigd.
   D   Afhankelijkheidsservice of -groep kan niet worden gestart.
    @   De service is niet gestart vanwege een aanmeldingsfout.
   4   Na het starten is de service blijven hangen.
  H   De opgegeven vergrendeling van de servicedatabase is ongeldig.
    <   De opgegeven service is voor verwijdering gemarkeerd.
 (   De opgegeven service bestaat al.
  T   Op dit moment werkt het systeem met de laatst bekende juiste configuratie.
    T   De afhankelijkheidsservice bestaat niet of is gemarkeerd
voor verwijdering.
  t   Het gebruik van het huidige opstartproces als de
laatst bekende juiste besturingsserie is al goedgekeurd.
    `   Sinds het systeem voor het laatst is opgestart is niet geprobeerd de service te starten.
  d   De naam wordt al gebruikt als een servicenaam of als de
weergegeven naam van een service.
    �   De account die voor deze service is opgegeven, is verschillend van
de account van andere services die in hetzelfde proces lopen.
 d   Foutacties kunnen alleen worden ingesteld voor Win32-services, niet voor stuurprogramma's.
    �   Deze service wordt uitgevoerd in hetzelfde proces als het besturingsbeheer van de service.
Daardoor kan het besturingsbeheer van de service geen actie ondernemen als het proces van deze 
service onverwacht wordt afgebroken.
 D   Er is geen herstelprogramma geconfigureerd voor deze service.
    Fysiek bandeinde.
 8   Er is een bestandsmarkering op een band bereikt.
  ,   Begin van tape of partitie gevonden.
  @   Het einde van een set bestanden is bereikt op de band.
    (   Niet meer gegevens op de band.
    $   Kan band niet partitioneren.
  �   Als geprobeerd wordt toegang te verkrijgen tot een nieuwe band met een
meervolumepartitie, is de huidige blokgrootte onjuist.
    P   Bij het laden van een band zijn de bandpartitiegegevens niet gevonden.
    D   Kan het uitwerpmechanisme van het medium niet vergrendelen.
   (   Kan het medium niet verwijderen.
  8   Het medium in het station is mogelijk gewijzigd.
  (   De I/O-bus is opnieuw ingesteld.
  $   Geen medium in het station.
   P   Het Unicode-teken heeft geen toewijzing in de multibyte doelcodetabel.
    P   Een initialisatieroutine van de dynamic link library (DLL) is mislukt.
    (   Er wordt een systeem afgesloten.
  `   Kan het uitschakelen van het systeem niet afbreken omdat er geen afsluitproces bezig is.
  H   Kan de opdracht niet uitvoeren door een fout op een I/O-apparaat.
 `   Er is geen serieel apparaat ge�nitialiseerd. Het seri�le stuurprogramma wordt verwijderd.
 �   Kan geen apparaat openen dat een interruptopdracht (IRQ) deelt 
met andere apparaten. Ten minste ��n ander apparaat dat deze IRQ gebruikt,
is al geopend.
   �   Een seri�le I/O-bewerking is uitgevoerd door een andere schrijfbewerking naar een seri�le poort.
(De IOCTL_SERIAL_XOFF_COUNTER heeft nul bereikt.)
   �   Een seri�le I/O-bewerking is voltooid omdat de time-outperiode is verlopen.
(De IOCTL_SERIAL_XOFF_COUNTER heeft nul niet bereikt.)
   <   Er is geen ID-adresmarkering gevonden op de diskette.
 |   Verschil gevonden tussen het sector-ID-veld van de diskette en het
adres van het controllerspoor van de diskette.
    h   De diskettecontroller heeft een fout gemeld die niet is herkend door
het diskettestuurprogramma.
 X   De diskettecontroller heeft inconsistente resultaten in de registers geplaatst.
   �   Kon tijdens het gebruik van de vaste schijf een herkalibreerbewerking niet uitvoeren, zelfs niet na herhaaldelijk proberen.
   |   Kan tijdens het gebruik van de vaste schijf een schijfbewerking niet uitvoeren, zelfs niet na herhaaldelijk proberen.
 |   De schijfcontroller moest opnieuw worden ingesteld bij het gebruik van de vaste schijf, maar
zelfs dat is mislukt.
      Fysiek bandeinde.
 X   Onvoldoende opslagruimte beschikbaar op de server om deze opdracht te verwerken.
  8   Er is een potenti�le impassesituatie vastgesteld.
 T   Het opgegeven basisadres of bestandsoffset heeft niet de juiste
uitlijning.
  |   Een poging de status van de systeemstroomvoorziening te wijzigen is
tegengehouden door een ander (stuur)programma.
   \   De systeem-BIOS heeft de status van de systeemstroomvoorziening niet kunnen wijzigen.
 `   Er is geprobeerd meer koppelingen aan een bestand te maken
dan het systeem ondersteunt.
  P   Voor het opgegeven programma is een nieuwere versie van Windows vereist.
  D   Het opgegeven programma is geen Windows- of MS-DOS-programma.
 L   Kan niet meer dan een exemplaar van het opgegeven programma starten.
  P   Het opgegeven programma is geschreven voor een oudere versie van Windows.
 T   Een van de vereiste bibliotheekbestanden van deze toepassing is beschadigd.
   X   Er is geen toepassing gekoppeld aan het opgegeven bestand voor deze bewerking.
    D   Fout bij het verzenden van de opdracht naar de toepassing.
    T   Kan een van de vereiste bibliotheekbestanden van deze toepassing niet vinden.
 p   Het huidige proces heeft alle door het systeem toegestane ingangen voor Vensterbeheer-objecten gebruikt.
  H   Het bericht kan alleen worden gebruikt bij synchrone bewerkingen.
 4   Het opgegeven bronelement bevat geen media.
   0   Het opgegeven doelelement bevat al media.
 ,   Het opgegeven element bestaat niet.
   T   Het opgegeven element maakt deel uit van een magazijn dat niet aanwezig is.
   `   Het opgegeven apparaat moet opnieuw worden ge�nitialiseerd als gevolg van hardwarefouten.
 t   Het apparaat heeft aangegeven dat schoonmaken vereist is voordat u probeert nieuwe bewerkingen uit te voeren.
 @   Het apparaat heeft aangegeven dat het niet gesloten is.
   (   Het apparaat is niet aangesloten.
     Element is niet gevonden.
 H   Er is geen overeenkomst voor de opgegeven sleutel in de index.
    D   De opgegeven eigenschappenset bestaat niet voor het object.
   0   De opgegeven apparaatnaam is ongeldig.
    l   Er is momenteel geen verbinding met het apparaat, maar het apparaat heeft een opgeslagen verbinding.
  X   Er is geprobeerd een apparaatverbinding op te slaan die al eerder is opgeslagen.
  T   Het opgegeven netwerkpad is door geen enkele netwerkleverancier geaccepteerd.
 @   De opgegeven naam voor de netwerkleverancier is ongeldig.
 8   Kan het netwerkverbindingsprofiel niet openen.
    4   Het netwerkverbindingsprofiel is beschadigd.
  0   Kan noncontainer niet weergeven in lijst.
    Uitgebreide fout.
 <   De notatie van de opgegeven groepsnaam is ongeldig.
   <   De notatie van de opgegeven computernaam is ongeldig.
 @   De notatie van de opgegeven gebeurtenisnaam is ongeldig.
  <   De notatie van de opgegeven domeinnaam is ongeldig.
   <   De notatie van de opgegeven servicenaam is ongeldig.
  <   De notatie van de opgegeven netwerknaam is ongeldig.
  <   De notatie van de opgegeven sharenaam is ongeldig.
    <   De notatie van het opgegeven wachtwoord is ongeldig.
  <   De notatie van de opgegeven berichtnaam is ongeldig.
  D   De notatie van de opgegeven berichtenbestemming is ongeldig.
  T   De opgegeven referenties zijn strijdig met een bestaande groep referenties.
   x   Er is geprobeerd een sessie te beginnen bij een netwerkserver,
maar er zijn al te veel sessies bij die server.
   `   De werkgroep- of domeinnaam wordt al gebruikt door een andere computer op het
netwerk.
   4   Het netwerk is niet aanwezig of niet gestart.
 8   De bewerking is geannuleerd door de gebruiker.
    |   Kan de gevraagde bewerking niet uitvoeren op een bestand met een openstaande, door de gebruiker toegewezen sectie.
    H   De netwerkverbinding is geweigerd door het systeem op afstand.
    <   De netwerkverbinding is zonder problemen afgesloten.
  P   Er is al een adres verbonden met het eindpunt van het netwerktransport.
   P   Er is nog geen adres verbonden met het eindpunt van het netwerktransport.
 \   Er is geprobeerd een bewerking uit te voeren op een niet-bestaande netwerkverbinding.
 `   Er is geprobeerd een ongeldige bewerking uit te voeren op een actieve netwerkverbinding.
  D   Het externe netwerk is niet bereikbaar voor het transport.
    D   Het externe systeem is niet bereikbaar voor het transport.
    D   Het externe systeem ondersteunt het transportprotocol niet.
   `   Er is geen service gestart op het eindpunt van het doelnetwerk
op het externe systeem.
   $   De opdracht is afgebroken.
    D   De netwerkverbinding is afgebroken door het lokale systeem.
   T   Kan de bewerking niet voltooien. De bewerking moet opnieuw worden geprobeerd.
 �   Kan geen verbinding maken met de server, omdat het maximale aantal
gelijktijdige verbindingen voor deze account is bereikt.
  @   Aanmeldingspoging op een ongeldige tijd voor de account.
  <   De account mag zich niet aanmelden vanaf dit station.
 H   Kan het netwerkadres niet gebruiken voor de gevraagde bewerking.
  (   De service is al geregistreerd.
   ,   De opgegeven service bestaat niet.
    \   De gevraagde bewerking is niet uitgevoerd omdat de gebruiker
niet is geverifieerd.
   �   De gevraagde bewerking is niet uitgevoerd omdat de gebruiker
niet was aangemeld bij het netwerk.
De opgegeven service bestaat niet.
 $   Doorgaan met huidige taak.
    l   Er is geprobeerd een initialisatiebewerking uit te voeren terwijl
de initialisatie al voltooid was.
  ,   Er zijn niet meer lokale apparaten.
   X   Niet alle bevoegdheden waarnaar is verwezen, zijn toegewezen aan de aanvrager.
    d   Er heeft een onvolledige toewijzing plaatsgevonden tussen accountnamen en beveiligings-ID's.
  P   Er zijn geen systeemquotalimieten specifiek voor deze account ingesteld.
  \   Er is geen codeersleutel beschikbaar. Er is een bekende codeersleutel geretourneerd.
  �   Het NT-wachtwoord is te complex om te worden omgezet in een LAN Manager-
wachtwoord. Het opgegeven LAN Manager-wachtwoord is een NULL-reeks.
 (   Het revisieniveau is onbekend.
    @   Geeft aan dat twee revisieniveaus niet compatibel zijn.
   X   Deze beveiligings-ID mag niet worden toegewezen als de eigenaar van dit object.
   \   Deze beveiligings-ID mag niet worden toegewezen als primaire groep van een object.
    �   Er is een poging gedaan een ge�miteerd communicatiesymbool te bewerken
door een thread die momenteel geen client imiteert.
   0   De groep mag niet worden uitgeschakeld.
   h   Er zijn momenteel geen aanmeldingsservers beschikbaar om de aanmeldingsopdracht
te verwerken.
    `    Een van de opgegeven aanmeldingssessies bestaat niet. De sessie kan al
zijn be�indigd.
  8    Een van de opgegeven bevoegdheden bestaat niet.
  L    Een van de vereiste bevoegdheden is niet toegekend aan de client.
    <   De opgegeven naam is geen juist gevormde accountnaam.
 ,   De opgegeven gebruiker bestaat al.
    ,   De opgegeven gebruiker bestaat niet.
  (   De opgegeven groep bestaat al.
    (   De opgegeven groep bestaat niet.
  �   De opgegeven gebruikersaccount is al een lid van de opgegeven
groep of de opgegeven groep kan niet worden verwijderd omdat
de groep nog een lid bevat.
  T   De opgegeven gebruikersaccount is geen lid van de opgegeven groepsaccount.
    T   De laatste beheerdersaccount kan niet worden uitgeschakeld
of verwijderd.
    p   Kan het wachtwoord niet bijwerken. De waarde die is opgegeven voor het actieve
wachtwoord is niet juist.
 �   Kan het wachtwoord niet bijwerken. De waarde die is opgegeven voor het nieuwe wachtwoord
bevat waarden die niet in een wachtwoord mogen worden gebruikt.
 l   Kan het wachtwoord niet bijwerken omdat een regel voor het bijwerken van wachtwoorden is
geschonden.
 H   Aanmeldingsfout: onbekende gebruikersnaam of onjuist wachtwoord.
  4   Aanmeldingsfout: gebruikersaccountbeperking.
  T   Aanmeldingsfout: schending van beperking op aanmeldingstijden van de account.
 L   Aanmeldingsfout: gebruiker mag zich niet aanmelden bij deze computer.
 P   Aanmeldingsfout: het wachtwoord voor de opgegeven account is verlopen.
    <   Aanmeldingsfout: account is momenteel uitgeschakeld.
  T   Er is geen toewijzing uitgevoerd tussen accountnamen en beveiligings-ID's.
    8   Er zijn te veel LUID's tegelijkertijd opgevraagd.
 ,   Er zijn geen LUID's meer beschikbaar.
 `   In dit specifieke geval is het subautoriteitgedeelte van een beveiligings-ID ongeldig.
    $   De ACL-structuur is ongeldig.
 8   De structuur van de beveiligings-ID is ongeldig.
  @   De structuur van de beveiligingsdescriptor is ongeldig.
   <   De meegekregen ACL of ACE
kan niet worden opgebouwd.
 ,   De server is momenteel uitgeschakeld.
 ,   De server is momenteel ingeschakeld.
  T   De opgegeven waarde is een ongeldige waarde voor een markeringsautoriteit.
    X   Er is niet meer geheugen beschikbaar voor het bijwerken van beveiligingsgegevens.
 d   De opgegeven kenmerken zijn ongeldig of niet compatibel met de
kenmerken van de hele groep.
  d   Het vereiste imitatieniveau is niet geleverd of het
geleverde imitatieniveau is ongeldig.
    @   Kan een beveiligingstoken op anoniem niveau niet openen.
  8   De opgegeven validatiegegevensklasse is ongeldig.
 $   Het type token is ongeschikt.
 h   Kan een beveiligingsbewerking niet uitvoeren op een
object dat geen gekoppelde beveiling heeft.
  �   Geeft aan dat er geen contact kan worden gelegd met een Windows NT Server of dat
objecten binnen het domein beschermd zijn zodat benodigde
gegevens niet kunnen worden opgehaald.
   h   De SAM-server of de LSA-server had de
verkeerde status om de beveilingsbewerking
uit te voeren.
 T   Het domein had de verkeerde status om de beveilingsbewerking uit te voeren.
   \   Deze bewerking is alleen toegestaan voor de primaire domeincontroller van het domein.
 ,   Het opgegeven domein bestaat niet.
    (   Het opgegeven domein bestaat al.
  d   Poging tot het instellen van meer dan het maximaal toegestane aantal domeinen op een server.
  �   Kan de opgegeven bewerking niet voltooien vanwege een
fatale mediumfout of een beschadigde gegevensstructuur op de schijf.
   8   De SAM-database bevat een interne inconsistentie.
 �   Generieke toegangstypen bevonden zich in een toegangsmasker dat
al aan niet-generieke typen had moeten zijn toegewezen.
  \   Een beveiligingsdescriptor heeft niet de juiste indeling (absoluut of zelfrelatief).
  �   De opgegeven bewerking kan alleen door aanmeldingsprocessen worden gebruikt.
Het aanroepend proces is niet geregistreerd als een aanmeldingsproces.
  X   Kan niet een nieuwe aanmeldingssessie starten met een ID die al in gebruik is.
    4   Een opgegeven validatiepakket is onbekend.
    \   De status van de aanmeldingssessie is niet consistent met de
opgegeven bewerking.
    0   De aanmeldingssessie-ID is al in gebruik.
 H   Een aanmeldingsverzoek bevat een ongeldige aanmeldingstypewaarde.
 `   Kan niet via een benoemde sluis imiteren totdat gegevens 
van die sluis zijn gelezen.
    h   De transactiestatus van een registersubstructuur is niet compatibel met de
opgegeven bewerking.
  0   Interne beveiligingsdatabasebeschadiging.
 @   Kan deze bewerking niet op ingebouwde accounts uitvoeren.
 L   Kan deze bewerking niet op deze ingebouwde speciale groep uitvoeren.
  P   Kan deze bewerking niet op deze ingebouwde speciale gebruiker uitvoeren.
  �   De gebruiker kan niet uit een groep worden verwijderd omdat de groep
momenteel de primaire groep van de gebruiker is.
    8   De token wordt al als primaire token gebruikt.
    0   De opgegeven lokale groep bestaat niet.
   @   De opgegeven accountnaam is geen lid van de lokale groep.
 @   De opgegeven accountnaam is al lid van de lokale groep.
   ,   De opgegeven lokale groep bestaat al.
 �   Aanmeldingsfout: de gebruiker heeft geen toestemming gekregen het opgegeven
aanmeldingstype op deze computer te gebruiken.
   `   Het maximum aantal geheimen dat op ��n systeem mag worden opgeslagen is
overschreden.
    L   De lengte van een geheim overschrijdt de maximaal toegestane waarde.
  8   Interne inconsistentie in de LSA-database (LSA).
  x   De beveiligingscontext van de gebruiker heeft tijdens een aanmeldingspoging te veel
beveiligings-ID's verzameld.
 �   Aanmeldingsfout: de gebruiker heeft geen toestemming gekregen het opgegeven
aanmeldingstype te gebruiken op deze computer.
   h   Er is een kruislings gecodeerd wachtwoord nodig om het wachtwoord van een gebruiker te wijzigen.
  h   Kan een van de nieuwe leden niet toegevoegen aan een lokale groep omdat het lid
niet bestaat.
    x   Kan een van de nieuwe leden niet toegevoegen aan een lokale groep omdat het lid een
onjuist accounttype heeft.
   4   Er zijn te veel beveiligings-ID's opgegeven.
  h   Er is een kruislings gecodeerd wachtwoord nodig om het wachtwoord van deze gebruiker te wijzigen.
 D   Geeft aan dat een ACL geen overerfelijke componenten bevat.
   <   Het bestand of de map is beschadigd en onleesbaar.
    8   De schijfstructuur is beschadigd en onleesbaar.
   P   Er is geen gebruikerssessiesleutel voor de opgegeven aanmeldingssessie.
   �   De service die is gestart, heeft een licentie voor een beperkt aantal verbindingen.
Op dit moment kunnen er niet meer verbindingen gemaakt worden met deze service
omdat het maximum aantal verbindingen dat de service accepteert al is bereikt.
       Ongeldige vensteringang.
      Ongeldige menu-ingang.
        Ongeldige cursoringang.
       Ongeldige tabelingang.
       Ongeldige haakingang.
 H   Ongeldige ingang naar een positiestructuur met meerdere vensters.
 4   Kan subvenster op hoogste niveau niet maken.
  (   Kan vensterklasse niet vinden.
    8   Ongeldig venster; dit hoort bij andere thread.
    ,   Toegangstoets is al geregistreerd.
       Klasse bestaat al.
       Klasse bestaat niet.
  (   Klasse heeft nog open vensters.
      Ongeldige index.
  $   Ongeldige pictogramingang.
    <   Er worden persoonlijke dialoogschermwoorden gebruikt.
 ,   De keuzelijst-id is niet gevonden.
    $   Geen jokertekens gevonden.
    (   Thread heeft geen open klembord.
  ,   Toegangstoets is niet geregistreerd.
  4   Het venster is geen geldig dialoogvenster.
    (   Besturings-ID is niet gevonden.
   \   Ongeldig bericht voor een keuzelijst met invoervak omdat het geen invoervak heeft.
    4   Het venster is geen keuzelijst met invoervak.
 (   Hoogte moet minder dan 256 zijn.
     Ongeldige DC-ingang.
  $   Ongeldig haakproceduretype.
       Ongeldige haakprocedure.
  D   Kan niet-lokale haak niet instellen zonder een module-ingang.
 @   Deze haakprocedure kan alleen globaal worden ingesteld.
   4   De journaalhaakprocedure is al ge�nstalleerd.
 0   De haakprocedure is niet ge�nstalleerd.
   D   Ongeldig bericht voor keuzelijst met enkelvoudige selectie.
   8   LB_SETCOUNT verzonden naar "non-lazy"-keuzelijst.
 0   De keuzelijst ondersteunt geen tabstops.
  L   Kan geen object vernietigen dat door een andere thread is gemaakt.
    0   Subvensters kunnen geen menu's hebben.
    ,   Het venster heeft geen systeemmenu.
   $   Ongeldig berichtvenstertype.
  8   Ongeldige systeemomvattende (SPI_*)-parameter.
        Scherm is al vergrendeld.
 �   Alle ingangen naar vensters in een positiestructuur met meerdere vensters moeten hetzelfde
bovenliggende venster hebben.
 (   Het venster is geen subvenster.
       Ongeldige GW_*-opdracht.
  $   Ongeldige threadaanduiding.
   P   Kan een bericht van een venster dat geen MDI-venster is
niet verwerken.
      Popupmenu is al actief.
   ,   Het venster heeft geen schuifbalk.
    D   Het bereik van de schuifbalk mag niet groter zijn dan 0x7FFF.
 L   Kan het venster niet op de opgegeven manier weergeven of verwijderen.
 P   Er zijn onvoldoende systeembronnen om de gevraagde service uit te voeren.
 P   Er zijn onvoldoende systeembronnen om de gevraagde service uit te voeren.
 P   Er zijn onvoldoende systeembronnen om de gevraagde service uit te voeren.
 H   Er zijn onvoldoende quota om de gevraagde service uit te voeren.
  H   Er zijn onvoldoende quota om de gevraagde service uit te voeren.
  D   Het wisselbestand is te klein om deze opdracht uit te voeren.
 ,   Een menuonderdeel is niet gevonden.
   4   Ongeldige ingang voor toetsenbordindeling.
    $   Type haak is niet toegestaan.
 @   Deze bewerking vereist een interactief vensterstation.
    L   Deze bewerking is geretourneerd omdat de time-outperiode verlopen is.
     Ongeldige monitoringang.
  0   Het gebeurtenislogbestand is beschadigd.
  X   Kan geen gebeurtenislogboekbestand openen. De service Eventlog is niet gestart.
   ,   Het gebeurtenislogboekbestand is vol.
 L   Het gebeurtenislogbestand is gewijzigd sinds de vorige leesbewerking.
 (   De reeksverbinding is ongeldig.
   8   De verbindingsingang is niet van het juiste type.
 (   De verbindingsingang is ongeldig.
 4   De RPC-protocolreeks wordt niet ondersteund.
  (   De RPC-protocolreeks is ongeldig.
 $   De reeks-UUID is ongeldig.
    0   De indeling van het eindpunt is ongeldig.
     Ongeldig netwerkadres.
        Geen eindpunt gevonden.
       Ongeldige time-outwaarde.
 (   De object-UUID is niet gevonden.
  ,   De object-UUID is al geregistreerd.
   ,   Het type-UUID is al geregistreerd.
    (   De RPC-server is al beschikbaar.
  4   Er zijn geen protocolreeksen geregistreerd.
   ,   De RPC-server is niet beschikbaar.
    $   Het beheertype is onbekend.
       Interface is onbekend.
       Geen bindingen.
      Geen protocolreeksen.
 $   Kan het eindpunt niet maken.
  P   Er zijn onvoldoende bronnen beschikbaar om deze bewerking te voltooien.
   ,   De RPC-server is niet beschikbaar.
    H   De RPC-server is te druk bezet om deze bewerking te voltooien.
    (   De netwerkopties zijn ongeldig.
   0   Er is geen actieve RPC in deze thread.
        Kan RPC niet uitvoeren.
   ,   De RPC is mislukt en niet uitgevoerd.
    RPC-protocolfout.
 H   De overdrachtsyntaxis wordt niet ondersteund door de RPC-server.
  ,   Het UUID-type wordt niet ondersteund.
 ,   De identificatielabel is ongeldig.
    (   De matrixgrenzen zijn ongeldig.
   0   De binding bevat geen vermeldingsnaam.
    ,   De syntaxis van de naam is ongeldig.
  8   De syntaxis van de naam wordt niet ondersteund.
   D   Er is geen netwerkadres beschikbaar om een
UUID te maken.
    (   Het eindpunt is een duplicaat.
        Onbekend verificatietype.
 0   Het maximum aantal oproepen is te klein.
     Reeks is te lang.
 0   De RPC-protocolreeks is niet gevonden.
    4   Het procedurenummer valt buiten het bereik.
   4   De binding bevat geen verificatiegegevens.
    4   De service voor verificatie is niet bekend.
   ,   Het verificatieniveau is onbekend.
    (   Ongeldige beveiligingscontext.
    0   De service voor validatie is niet bekend.
 $   De vermelding is ongeldig.
    <   Het servereindpunt kan de bewerking niet uitvoeren.
   D   De eindpunttoewijzer heeft geen eindpunten meer beschikbaar.
  ,   Er zijn geen interfaces ge�xporteerd.
 ,   De vermeldingsnaam is niet volledig.
  $   De versieoptie is ongeldig.
       Er zijn geen leden meer.
  H   Kan de opdracht unexport (export ongedaan maken) niet uitvoeren.
  $   Interface is niet gevonden.
       De vermelding bestaat al.
 $   Vermelding is niet gevonden.
  4   De service voor namen is niet beschikbaar.
    0   De groep met netwerkadressen is ongeldig.
 8   De gevraagde bewerking wordt niet ondersteund.
    P   Er is geen beveiligingscontext beschikbaar waarmee imitatie mogelijk is.
      Interne fout in een RPC.
  @   Poging van de RPC-server een integer door nul te delen.
   (   Adresseringsfout in RPC-server.
   `   Deling door nul veroorzaakt door een bewerking (zwevend-decimaalteken) op de RPC-server.
  @   Een underflow (zwevend decimaalteken) bij de RPC-server.
  @   Een overflow (zwevend decimaalteken) bij de RPC-server.
   T   De lijst met RPC-servers beschikbaar voor binding van auto handles
is leeg.
  ,   Kan tekenvertaaltabel niet openen.
    H   Het bestand met de tekenvertaaltabel is kleiner dan
512 bytes.
   \   Er is een lege contextingang doorgegeven van de client naar de host tijdens
een RPC.
 8   De contextingang is tijdens een RPC gewijzigd.
    T   De verbindingsingangen die zijn doorgegeven aan een RPC komen niet overeen.
   <   Het stub-programma kan de RPC-ingang niet verkrijgen.
 P   Een aanwijzer met de waarde nul is doorgegeven aan het stub-programma.
    4   De opsommingswaarde valt buiten het bereik.
   $   Het aantal bytes is te klein
  <   Het stub-programma heeft onjuiste gegevens ontvangen.
 P   De opgegeven gebruikersbuffer is niet geldig voor de gevraagde bewerking.
 X   Kan het schijfmedium niet herkennen. Het medium is mogelijk niet geformatteerd.
   8   Het werkstation heeft geen vertrouwelijk geheim.
  |   De SAM-database op de Windows NT Server heeft geen
computeraccount voor de vertrouwensrelatie met dit werkstation.
   `   Kan geen vertrouwensrelatie leggen tussen het primaire domein en het vertrouwde
domein.
  \   Kan geen vertrouwensrelatie leggen tussen dit werkstation en het primaire
domein.
    ,   Fout tijdens aanmelden bij netwerk.
   8   Er is al een RPC  uitgezonden voor deze thread.
   `   Poging tot aanmelden maar de service voor het aanmelden op het netwerk was niet gestart.
  0   De account van de gebruiker is verlopen.
  D   De redirector is in gebruik en kan niet worden verwijderd.
    @   Het opgegeven printerstuurprogramma is al ge�nstalleerd.
  (   De opgegeven poort is onbekend.
   (   Onbekend printerstuurprogramma.
   ,   De afdrukprocessor is niet bekend.
    4   Het opgegeven scheidingsbestand is ongeldig.
  ,   De opgegeven prioriteit is ongeldig.
      Ongeldige printernaam.
        De printer bestaat al.
    $   Ongeldige printeropdracht.
    0   Het opgegeven gegevenstype is ongeldig.
   ,   De opgegeven omgeving is ongeldig.
    $   Er zijn geen bindingen meer.
  �   De gebruikte account is een vertrouwelijke interdomeinaccount. Gebruik uw globale of lokale gebruikersaccount voor toegang tot deze server.
   �   De gebruikte account is een computeraccount. Gebruik uw globale of lokale gebruikersaccount voor toegang tot deze server.
 �   De gebruikte account is een server-vertrouwensaccount. Gebruik uw globale of lokale gebruikersaccount voor toegang tot deze server.
   �   De naam of  de beveiligings-ID (SID) van het gespecificeerde domein is tegenstrijdig
met de vertrouwensgegevens voor dat domein.
 @   De server is in gebruik en kan niet worden verwijderd.
    8   Het opgegeven beeldbestand bevat geen bronsectie.
 H   Kan het opgegeven brontype niet vinden in het afbeeldingsbestand.
 H   Kan de opgegeven bronnaam niet vinden in het afbeeldingsbestand.
  D   Kan de opgegeven brontaal-ID niet vinden in het beeldbestand.
 <   Te weinig beschikbaar om deze opdracht te verwerken.
  0   Er zijn geen interfaces geregistreerd.
        De RPC is geannuleerd.
    @   De binding-ingang bevat niet alle benodigde informatie.
   (   Communicatiefout tijdens een RPC.
 @   Het gevraagde verificatieniveau wordt niet ondersteund.
   $   Geen hoofdnaam geregistreerd.
 D   De opgegeven fout is geen geldige foutcode van Windows RPC.
   H   Er is een UUID toegewezen die alleen geldig is op deze computer.
  <   Fout die specifiek is voor een beveiligingspakket.
    $   Thread is niet geannuleerd.
   @   Ongeldige bewerking op de coderings-/decoderingsingang.
   <   Niet-compatibele versie van het serialisatiepakket.
   4   Incompatible versie van het RPC-programma.
    8   Het RPC-sluisobject is ongeldig of beschadigd.
    X   Er is geprobeerd een ongeldige bewerking uit te voeren op een RPC-sluisobject.
    ,   Niet-ondersteunde RPC-sluisversie.
    $   Groepslid is niet gevonden.
   @   Kan database-ingang voor eindpunttoewijzing niet maken.
   (   De object-UUID is de nul-UUID.
    (   De opgegeven tijd is ongeldig.
    8   De opgegeven naam voor het formulier is ongeldig.
 <   De opgegeven grootte voor het formulier is ongeldig.
  4   De opgegeven printeringang is al in gebruik
   ,   De opgegeven printer is verwijderd.
   ,   De status van de printer is ongeldig.
 l   De gebruiker moet zijn/haar wachtwoord wijzigen voordat hij/zij zich voor de eerste keer aanmeldt.
    <   Kan de domeincontroller voor dit domein niet vinden.
  p   De account waarnaar is verwezen, is momenteel vergrendeld en u kunt zich er mogelijk niet op aanmelden.
   4   De opgegeven objectexporter is niet gevonden.
 0   Het opgegeven object is niet gevonden.
    <   De opgegeven object resolver set is niet gevonden.
    P   Er zijn nog gegevens in de opdrachtbuffer die verstuurd moeten worden.
    (   Ongeldige asynchrone RPC-ingang.
  <   Ongeldige asynchrone RPC-ingang voor deze bewerking.
  ,   Het RPC-sluisobject is al gesloten.
   @   De RPC is be�indigd voordat alle sluizen zijn verwerkt.
   @   Er zijn geen gegevens meer beschikbaar van de RPC-sluis.
  $   De pixelindeling is ongeldig.
 0   Het opgegeven stuurprogramma is ongeldig.
 L   De vensterstijl of het klassekenmerk is ongeldig voor deze bewerking.
 D   De gevraagde metabestandsbewerking wordt niet ondersteund.
    @   De gevraagde overdrachtsbewerking wordt niet ondersteund.
 <   De gevraagde knipbewerking wordt niet ondersteund.
    �   De netwerkverbinding is met succes tot stand gekomen, maar de gebruiker moest
gevraagd worden om een ander wachtwoord dan oorspronkelijk was opgegeven.
  0   De opgegeven gebruikersnaam is ongeldig.
  ,   Deze netwerkverbinding bestaat niet.
  T   Deze netwerkverbinding heeft open bestanden of  opdrachten in behandeling.
    (   Er zijn nog actieve verbindingen.
 d   Het apparaat wordt gebruikt door een actief proces. De verbinding kan niet worden verbroken.
  8   De opgegeven afdrukmonitor wordt niet herkend.
    D   Het opgegeven printerstuurprogramma is momenteel in gebruik.
  ,   Het spoolbestand is niet gevonden.
    0   Er is geen StartDocPrinter-oproep gedaan.
 (   Er is geen AddJob-oproep gedaan.
  8   De opgegeven afdrukprocessor is al ge�nstalleerd.
 8   De opgegeven afdrukmonitor is al ge�nstalleerd.
   D   De opgegeven afdrukmonitor heeft de benodigde functies niet.
  @   De opgegeven afdrukmonitor is op het moment in gebruik.
   d   De gevraagde bewerking is niet toegestaan als er taken in de wachtrij staan voor de printer.
  x   De gevraagde bewerking is voltooid. Wijzigingen zullen pas van kracht zijn als het systeem opnieuw gestart is.
    t   De gevraagde bewerking is voltooid. Wijzigingen zullen pas van kracht zijn als de service opnieuw gestart is.
 0   Fout bij het verwerken van de opdracht.
   ,   Kan de lokale WINS niet verwijderen.
  4   Het importeren vanuit het bestand is mislukt.
 T   De reservekopie is mislukt. Is er eerder een volledige reservekopie gemaakt?
  `   De reservekopie is mislukt. Controleer de map waar u de database naar toe wilt kopi�ren.
  0   De naam bestaat niet in de WINS-database.
 L   Replicatie met een niet-geconfigureerde partner is niet toegestaan.
   �   De DHCP-client heeft een IP-adres verkregen dat al gebruikt wordt op het netwerk. De lokale interface zal worden uitgeschakeld totdat de DHCP-client een nieuw adres krijgt.
  x   De clusterbron kan niet naar een andere groep verplaatst worden omdat ook andere bronnen ervan afhankelijk zijn.
  4   Kan clusterbronafhankelijkheid niet vinden.
   d   Kan de clusterbron niet afhankelijk maken van de opgegeven bron omdat deze al afhankelijk is.
 (   De clusterbron is niet on line.
   D   Er is geen clusterknooppunt beschikbaar voor deze bewerking.
  ,   De clusterbron is niet beschikbaar.
   $   Kan clusterbron niet vinden.
  $   De cluster wordt afgesloten.
  @   Kan geen clusterknoopunt afstoten zolang deze on line is.
    Object bestaat al.
    $   Object staat al in de lijst.
  D   De clustergroep is niet beschikbaar voor nieuwe aanvragen.
    (   Kan de clustergroep niet vinden.
  P   Kan de bewerking niet voltooien omdat de clustergroep niet on line is.
    <   Het clusterknooppunt is niet de eigenaar van de bron.
 @   Het clusterknooppunt is niet de eigenaar van de groep.
    @   Kan clusterbron niet maken in de opgegeven bronmonitor.
   D   Clusterbron kan niet on line gezet worden door bronmonitor.
   H   Kan de bewerking niet voltooien omdat de clusterbron on line is.
  \   Kan de clusterbron niet verwijderen of off line zetten omdat deze de quorumbron is.
   \   Het cluster kan van bron geen quorumbron maken omdat deze geen quorumbron kan zijn.
   ,   De clustersoftware wordt afgesloten.
  \   De groep of bron is niet in de juiste staat om de gevraagde bewerking uit te voeren.
  �   De eigenschappen zijn opgeslagen maar sommige wijzigingen worden pas van kracht
als de bron de volgende keer on line gezet wordt.
    t   Het cluster kan van  bron geen quorumbron maken omdat deze geen deel uitmaakt van een gedeelde opslagklasse.
  L   De clusterbron kan niet worden verwijderd omdat dit een kernbron is.
  ,   Kan quorumbron niet on line zetten.
   8   Kan het quorumlogboek niet cre�ren of koppelen.
   (   Het clusterlogboek is beschadigd.
 d   Kan het record niet in het clusterlogboek schrijven omdat het de maximumlengte overschrijdt.
  <   Het clusterlogboek overschrijdt de maximale grootte.
  @   Kan geen checkpointrecord vinden in het clusterlogboek.
   h   Er is onvoldoende schijfruimte aanwezig voor het vastleggen van gebeurtenissen in een logboek.
    P   De lijst van servers voor deze werkgroep is momenteel niet beschikbaar
    \   Het onderliggende bestand is geconverteerd naar een samengestelde bestandsindeling.
   X   De opslagbewerking moet geblokkeerd worden tot er meer gegevens beschikbaar zijn.
 D   De opslagbewerking moet onmiddellijk een nieuwe poging doen.
  P   Het opvangen van het signaal heeft geen invloed op de opslagbewerking.
    L   Meerdere geopend voorkomt consolidatie (commit-actie is geslaagd).
    P   Consolidatie van het opslagbestand is mislukt (commit-actie is geslaagd).
 \   Consolidatie van het opslagbestand is niet van toepassing (commit-actie is geslaagd).
 H   Gebruik de registerdatabase om de gevraagde gegevens te leveren.
     Gelukt, maar statisch
 $   Macintosh-plakboekindeling
       Neerzetten is gelukt.
 ,   Slepen en neerzetten is geannuleerd.
  $   Gebruik de standaardcursor.
   ,   Gegevens hebben dezelfde FORMATETC.
      Beeld is al bevroren.
 (   FORMATETC wordt niet ondersteund.
    Zelfde cache
  ,   Sommige caches zijn niet bijgewerkt.
  ,   Ongeldig werkwoord voor OLE-object
    H   Werkwoordnummer is geldig maar kan werkwoord nu niet uitvoeren
    ,   Ongeldige vensteringang doorgegeven
   L   Bericht is te lang. Het is gedeeltelijk afgekapt voor de weergave.
    4   Kan OLESTREAM niet converteren naar IStorage
  ,   Moniker is tot zichzelf gereduceerd.
  ,   Algemeen voorvoegsel is deze moniker.
 0   Algemeen voorvoegsel is invoer-moniker.
   0   Algemeen voorvoegsel is beide monikers.
   @   Moniker is al geregistreerd in de gestarte objecttabel.
   <   Niet alle aangevraagde interfaces waren beschikbaar.
     Niet ge�mplementeerd
     Onvoldoende geheugen
  ,   Een of meer argumenten zijn ongeldig
  (   Interface wordt niet ondersteund
     Ongeldige aanwijzer
      Ongeldige ingang
      Bewerking is afgebroken
   $   Niet nader omschreven fout
       Algemene toegangsfout
 L   De benodigde gegevens voor deze bewerking zijn nog niet beschikbaar.
  (   Interface wordt niet ondersteund
     Ongeldige aanwijzer
       Bewerking is afgebroken.
  $   Niet nader omschreven fout
    (   Fout bij lokale opslag van thread
 <   Fout bij ophalen van toewijzer van gedeeld geheugen
   4   Fout bij ophalen van toewijzer van geheugen
   ,   Kan klassecache niet initialiseren
    ,   Kan RPC-services niet initialiseren
   ,   Kan TLS-kanaalregeling niet instellen
 ,   Kan TLS-kanaalregeling niet toewijzen
 H   Door gebruiker opgegeven geheugentoewijzer is niet geaccepteerd
   (   De OLE-servicemutex bestaat al
    4   De OLE-servicebestandstoewijzing bestaat al
   <   Kan beeld of bestand voor OLE-service niet toewijzen
  $   Kan OLE-service niet starten
  X   Opnieuw geprobeerd CoInitialize aan te roepen terwijl die "single-threaded" was
   L   Extern actief maken was noodzakelijk, maar het was niet toegestaan
    X   Extern actief maken was noodzakelijk, maar de opgegeven servernaam was ongeldig
   l   De klasse is geconfigureerd om met een andere beveiligings-id uitgevoerd te worden dan de aanroeper.
  P   Het gebruik van Ole1-services die DDE-vensters vereisen is uitgeschakeld.
 t   Een RunAs-specificatie moet  uit <domeinnaam>\gebruikersnaam> bestaan of eenvoudigweg uit <gebruikersnaam>.
   P   Het serverproces kan niet worden gestart. De padnaam is mogelijk onjuist.
 d   Het serverproces kan niet worden gestart. De padnaam is mogelijk onjuist of niet beschikbaar.
 �   Het serverproces kan niet gestart worden omdat de geconfigureerde identiteit onjuist is. Controleer de gebruikersnaam en het wachtwoord.
  8   De client is niet bevoegd deze server te starten.
 H   Kan de service die toegang geeft tot deze server niet starten.
    \   Deze computer kan niet communiceren met de computer die toegang geeft tot de server.
  (   De gestarte server reageert niet.
 P   De registratiegegevens voor deze server zijn inconsistent of incompleet.
  T   De registratiegegevens voor deze interface zijn inconsistent of incompleet.
   8   De gevraagde bewerking wordt niet ondersteund.
    4   Aanroep is geweigerd door de aangeroepene.
    8   Aanroep is geannuleerd door het berichtenfilter.
  h   De aanroeper verzendt een intertask-SendMessage-aanroep en
kan niet aanroepen via PostMessage.
   l   De aanroeper verzendt een asynchrone aanroep en kan geen
uitgaande aanroep doen namens deze aanroep.
 X   Het is niet toegestaan een uitgaande aanroep te doen vanuit het berichtenfilter.
  �   De verbinding is verbroken of in een onjuiste toestand
en kan niet meer worden gebruikt. Andere verbindingen
zijn nog wel geldig.
   �   De server (niet de servertoepassing) is niet beschikbaar
en is verdwenen; alle verbindingen zijn ongeldig. De oproep kan
zijn uitgevoerd.
   `   De aanroeper (client) is verdwenen terwijl de server
een aanroep aan het verwerken was.
  H   Het gegevenspakket met de samengevoegde parameter is niet juist.
  t   De aanroep is niet juist verzonden; de berichtenwachtrij
was vol en is niet leeggemaakt na het plaatsmaken.
  d   De client (aanroeper) kan de parametergegevens niet samenvoegen - te weinig geheugen, etc.
    h   De client (aanroeper) kan de parametergegevens niet uit elkaar halen - te weinig geheugen, etc.
   d   De server (aangeroepene) kan de parametergegevens niet samenvoegen - te weinig geheugen, etc.
 l   De server (aangeroepene) kan de parametergegevens niet uit elkaar halen - te weinig geheugen, etc.
    D   Ontvangen gegevens zijn ongeldig: server- of clientgegevens.
  P   Een bepaalde parameter is ongeldig en kan niet uit elkaar gehaald worden.
 T   Er is geen tweede uitgaande aanroep in hetzelfde kanaal in DDE-conversatie.
   �   De server (niet de servertoepassing) is niet beschikbaar
en is verdwenen. Alle verbindingen zijn ongeldig. De aanroep is niet uitgevoerd.
        Systeemaanroep mislukt.
   P   Kan sommige vereiste bronnen (geheugen, gebeurtenissen...) niet toewijzen
 X   Poging tot het maken van aanroepen in meer dan ��n thread in enkele-threadmodus.
  H   De vereiste interface is niet geregistreerd op het serverobject.
  p   RPC kan de server niet aanroepen of kan het resultaat van het aanroepen van de server niet retourneren.
   0   De server heeft een uitzondering gegeven.
 @   Kan de threadmodus niet wijzigen nadat deze is ingesteld.
 8   De aangeroepen methode bestaat niet op de server.
 P   De verbindingen van het aangeroepen object met de clients zijn verbroken.
 \   Het aangeroepen object wil de aanroep nu niet verwerken. Probeer het later opnieuw.
   H   Het berichtenfilter heeft aangegeven dat de toepassing bezet is.
  8   Het berichtenfilter heeft de aanroep geweigerd.
   L   Een aanroepbesturingsinterface is aangeroepen met ongeldige gegevens.
 |   Er kan geen uitgaande aanroep gedaan worden omdat de toepassing een invoer-synchrone aanroep aan het verzenden is.
    d   De toepassing heeft een interface aangeroepen die samengevoegd was voor een andere thread.
    @   CoInitialize is niet aangeroepen op de actieve thread.
    T   De OLE-versie op de client komt niet overeen met de OLE-versie op de server.
  @   OLE heeft een pakket ontvangen met een ongeldige header.
  D   OLE heeft een pakket ontvangen met een ongeldige extensie.
    D   Het gevraagde object of de gevraagde interface bestaat niet.
  ,   Het gevraagde object bestaat niet.
    @   OLE heeft een verzoek verzonden en wacht op een antwoord.
 @   OLE wacht voordat een verzoek opnieuw wordt verzonden.
    P   Geen toegang tot de context van de oproep totdat de oproep is voltooid.
   D   Ge�miteerde of onbeveiligde oproepen worden niet ondersteund.
 �   De beveiliging moet zijn ge�nitialiseerd voordat interfaces kunnen worden
samengevoegd of uit elkaar gehaald. De beveiliging kan na de initialisatie niet worden gewijzigd.
  �   Er zijn geen beveiligingspakketten ge�nstalleerd op deze machine of de gebruiker is niet
aangemeld of er zijn geen compatibele beveiligingspakketten op de client en de server.
     Toegang is geweigerd.
 @   Externe oproepen zijn niet toegestaan voor dit proces.
    `   Het gebundelde interfacegegevenspakket (OBJREF) heeft een ongeldig of onbekend formaat.
      Interne fout.
    Onbekende interface.
     Lid is niet gevonden.
 $   Parameter is niet gevonden.
       Type komt niet overeen.
      Onbekende naam.
       Geen benoemde argumenten.
     Ongeldig type variabele.
     Uitzondering.
 $   Buiten het huidige bereik.
       Ongeldige index.
     Onbekende taal.
       Geheugen is vergrendeld.
  $   Ongeldig aantal parameters.
       Parameter niet optioneel.
     Ongeldige aangeroepene.
   0   Een verzameling wordt niet ondersteund.
      Buffer is te klein.
   4   Oude indeling of ongeldig type bibliotheek.
   4   Oude indeling of ongeldig type bibliotheek.
   0   Fout bij toegang tot het OLE-register.
    ,   Bibliotheek is niet geregistreerd.
    $   Gebonden aan onbekend type.
   0   Gekwalificeerde naam is niet toegestaan.
  P   Ongeldige verwijzing vooruit, of verwijzing naar niet-gecompileerd type.
      Type komt niet overeen.
       Element is niet gevonden.
    Dubbelzinnige naam.
   ,   Naam bestaat al in de bibliotheek.
       Onbekende LCID.
   8   Functie is niet gedefinieerd in opgegeven DLL.
    0   Onjuiste soort module voor de bewerking.
  ,   Grootte mag 64 kB niet overschrijden.
 (   Dubbele id in erfenishi�rarchie.
  4   Onjuiste erfenisdiepte in standaard-OLE-lid.
      Type komt niet overeen.
   $   Ongeldig aantal argumenten.
      I/O-fout.
 4   Fout bij maken van uniek tijdelijk bestand.
   0   Fout bij laden van type DLL/bibliotheek.
  ,   Eigenschapfuncties niet consistent.
   4   Kringafhankelijkheid tussen typen/modules.
    0   Kan gevraagde bewerking niet uitvoeren.
      Kan %1 niet vinden.
   $   Kan het pad %1 niet vinden.
   8   Onvoldoende bronnen om nog een bestand te openen.
    Toegang geweigerd.
    4   Bewerking geprobeerd op een ongeldig object.
  D   Onvoldoende geheugen beschikbaar om bewerking te voltooien.
      Ongeldige pointer.
       Geen ingangen meer.
   ,   Schijf is tegen schrijven beveiligd.
  $   Fout bij een zoekbewerking.
   0   Schijffout tijdens een schrijfbewerking.
  ,   Schijffout tijdens een leesbewerking.
 $   Schending van deelrechten.
    ,   Schending van vergrendelingsrechten.
     %1 bestaat al.
       Ongeldige parameter.
  <   Onvoldoende schijfruimte om bewerking te voltooien.
   p   Ongeldige schrijfbewerking van een niet-eenvoudige eigenschap naar een set van eenvoudige eigenschappen.
  0   Een API-aanroep is abnormaal be�indigd.
   <   Het bestand %1 is geen geldig samengesteld bestand.
   $   De naam %1 is niet geldig.
       Onverwachte fout.
 ,   Deze functie is niet ge�mplementeerd.
    Ongeldige vlag.
   4   Poging een object te gebruiken dat bezet is.
  <   De opslag is gewijzigd sinds de laatste commit-actie.
 <   Poging een object te gebruiken dat niet meer bestaat.
    Kan niet opslaan.
 \   Het samengestelde bestand %1 is gemaakt met een niet-compatibele versie van opslag.
   T   Het samengestelde bestand %1 is gemaakt met een nieuwere versie van opslag.
   D   Share.exe of gelijkwaardig programma nodig voor de bewerking.
 H   Ongeldige bewerking opgeroepen op niet-bestandsgebaseerde opslag.
 P   Ongeldige bewerking opgeroepen op een object met "extant marshallings".
   (   Het doc-bestand is beschadigd.
    4   OLE32.DLL is op een verkeerd adres geladen.
   H   De bestandsoverdracht werd afgebroken. Het bestand is incompleet.
 ,   De bestandsoverdracht is be�indigd.
   $   Ongeldige OLEVERB-structuur.
  $   Ongeldige "advise"-vlaggen.
   H   Kan niet meer nummeren, omdat de verbonden gegevens ontbreken.
    0   Deze toepassing ontvangt geen "advises".
  0   Geen verbinding voor deze verbindings-id.
 D   Object moet worden gestart om deze bewerking uit te voeren
       Geen cache aanwezig
   $   Niet-ge�nitialiseerd object
   4   Bronklasse van gekoppeld object is gewijzigd
  4   Kan de moniker van het object niet ophalen
    $   Kan niet aan de bron binden
   8   Object is statisch, bewerking is niet toegestaan
  8   Gebruiker heeft het venster Opslaan geannuleerd.
     Ongeldige rechthoek
   @   Compobj.dll is te oud voor de ge�nitialiseerde ole2.dll
       Ongeldige vensteringang
   8   Object bevindt zich niet in een actieve status.
   $   Kan object niet converteren
   `   Kan de bewerking niet uitvoeren omdat het object nog geen opslagruimte heeft gekregen

   (   Ongeldige FORMATETC-structuur.
    ,   Ongeldige DVTARGETDEVICE-structuur
    (   Ongeldige STDGMEDIUM-structuur
    $   Ongeldige STATDATA-structuur
     Ongeldige lindex
     Ongeldige tymed
   $   Ongeldige Klembord-indeling.
     Ongeldige aspect(en)
  D   Parameter tdSize van de DVTARGETDEVICE-structuur is ongeldig.
 4   Object ondersteunt IViewObject-interface niet
 H   Poging een "drop"-doel in te trekken dat niet is geregistreerd
    8   Het venster is al geregistreerd als "drop"-doel
       Ongeldige vensteringang
   L   Klasse ondersteunt geen aggregatie (of klasseobject is op afstand)
    8   ClassFactory kan gevraagde klasse niet leveren
    0   Klasse is niet gelicentieerd voor gebruik
 $   Fout bij opmaken van beeld
    4   Kan de sleutel niet uit het register lezen.
   8   Kan de sleutel niet naar het register schrijven.
  4   Kan de sleutel niet in het register vinden.
   (   Ongeldige waarde voor register
    $   Klasse is niet geregistreerd.
 (   Interface is niet geregistreerd.
     CATID bestaat niet
    (   Beschrijving is niet gevonden.
        Cache is niet bijgewerkt.
 (   Geen werkwoorden voor OLE-object
  ,   Ongeldig werkwoord voor OLE-object
    ,   Ongedaan maken is niet beschikbaar.
   4   Geen ruimte beschikbaar voor hulpprogramma's
  (   OLESTREAM Get-methode is mislukt.
 (   OLESTREAM Put-methode is mislukt.
 @   Inhoud van de OLESTREAM heeft niet de juiste indeling.
    X   Fout in een Windows GDI-aanroep bij het converteren van de bitmap naar een DIB
    <   Inhoud van de IStorage heeft niet de juiste indeling.
 H   Een van de standaardstreams ontbreekt in de inhoud van IStorage
   X   Fout in een Windows GDI-aanroep bij het converteren van de DIB naar een bitmap

      OpenClipboard is mislukt.
 $   EmptyClipboard is mislukt.
        SetClipboard is mislukt.
  0   Gegevens op het Klembord zijn ongeldig.
   $   CloseClipboard is mislukt.
    0   Moniker moet handmatig worden verbonden
   ,   Bewerking heeft deadline overschreden
 $   Moniker moet algemeen zijn.
   (   Bewerking is niet beschikbaar.
       Ongeldige syntaxis.
       Geen object voor moniker
  (   Onjuiste extensie voor bestand.
   ,   Tussenliggende bewerking is mislukt.
      Kan moniker niet binden
       Moniker is niet gebonden
  (   Moniker kan bestand niet openen
   <   Invoer van gebruiker nodig om bewerking uit te voeren
 0   Moniker-klasse heeft geen tegengestelde
   (   Moniker verwijst niet naar opslag
     Geen algemeen voorvoegsel
     Kan moniker niet nummeren
 (   CoInitialize is niet aangeroepen
  (   CoInitialize is al aangeroepen
    (   Kan de objectklasse niet bepalen.
    Ongeldige klassereeks
     Ongeldige interfacereeks
  $   Toepassing is niet gevonden
   8   Kan de toepassing niet meer dan ��n keer starten
  $   Fout in toepassingsprogramma
  0   Dll-bestand voor klasse is niet gevonden.
    Fout in Dll-bestand
   H   Onjuist (onjuiste versie van) besturingssysteem voor toepassing.
  $   Object is niet geregistreerd
  $   Object is al geregistreerd
    ,   Object is niet verbonden met server
   L   Toepassing is gestart maar heeft geen "class-factory" geregistreerd
      Object is vrijgegeven
 $   Kan DCOM-client niet imiteren
 @   Kan de beveiligingscontext van de server niet verkrijgen.
 @   Kan het toegangstoken van de huidige thread niet openen.
  D   Kan geen gebruikersgegevens verkrijgen van een toegangstoken.
 �   De client die IAccessControl::IsAccessPermitted heeft aangeroepen was de vertrouwde gebruiker die was aangewezen voor de methode.
 @   Kan de beveiligingslaag van de client niet verkrijgen.
    L   Kan een discretionaire ACL niet omzetten in een security descriptor
   L   De systeemfunctie AccessCheck heeft de waarde False geretourneerd.
    P   De functie NetAccessDel of NetAccessAdd heeft een foutcode geretourneerd.
 �   Een door de gebruiker opgegeven reeks voor een gebruikersnaam voldoet niet aan de syntaxis <Domein>\<Gebruikersnaam>; het betrof niet de reeks "*".
   D   Een door de gebruiker opgegeven beveiligings-ID is ongeldig.
  P   Kan een 'wide character'-reeks niet converteren naar een multibyte-reeks.
 d   Kan geen beveiligings-ID vinden die overeenkomt met een door de gebruiker opgegeven reeks.
    H   Het uitvoeren van de systeemfunctie LookupAccountSID is mislukt.
  |   Kan geen vertrouwde gebruikersnaam vinden die overeenkomt met een beveiligings-ID die door de gebruiker is opgegeven.
 H   Het uitvoeren van de systeemfunctie LookupAccountName is mislukt.
 <   Kan een serialisatie-ingang niet (opnieuw) instellen.
 8   Kan de naam van de Windows-map niet verkrijgen.
      Pad is te lang.
      Kan geen uuid maken.
      Kan bestand niet maken.
   H   Kan een serialisatie-ingang of een bestandsingang niet sluiten.
   H   Het aantal ACE's in een ACL heeft de systeemlimiet overschreden.
  \   Niet alle DENY_ACCESS ACE's zijn voor de GRANT_ACCESS ACE's geplaatst in de stroom.
   t   De versie van de ACL-indeling in de stroom wordt niet ondersteund door deze implementatie van IAccessControl.
 @   Kan het toegangstoken van het serverproces niet openen.
   L   Kan de ACL in de door de gebruiker opgegeven stroom niet decoderen.
   8   De COM IAccessControl is niet ge�nitialiseerd.
       Algemene toegangsfout
    Ongeldige ingang
     Onvoldoende geheugen
  ,   Een of meer argumenten zijn ongeldig
  (   Kan een klasseobject niet maken
   ,   OLE-service kan object niet binden
    0   RPC-communicatie mislukt met OLE-service
      Onjuist pad naar object
   $   Serveruitvoering is mislukt.
  <   OLE-service kan niet communiceren met de objectserver
 (   Kan moniker-pad niet normaliseren
 H   Objectserver wordt gestopt als OLE-service er contact mee zoekt
   0   Ongeldige root block pointer opgegeven
    8   Ongeldige link pointer in een toewijzingsreeks.
   4   De gevraagde toewijzingsgrootte is te groot
   $   Beschadigde gebruikers-id.
       Beschadigde hash.
    Beschadigde sleutel.
     Beschadigde lengte.
      Beschadigde gegevens.
     Ongeldige handtekening.
   (   Beschadigde versie van provider.
  $   Ongeldig algoritme opgegeven.
 $   Ongeldige vlaggen opgegeven.
      Ongeldig type opgegeven.
  @   Sleutel is niet geldig voor gebruik in opgegeven status.
  D   Hash-teken is niet geldig voor gebruik in opgegeven status.
      Sleutel bestaat niet.
 <   Onvoldoende geheugen beschikbaar voor de bewerking.
      Object bestaat al.
       Toegang geweigerd.
        Kan object niet vinden.
   $   Gegevens zijn al gecodeerd.
   $   Ongeldige provider opgegeven.
 (   Ongeldig providertype opgegeven.
  4   Openbare sleutel van provider is ongeldig.
        Sleutelset bestaat niet
   ,   Providertype is niet gedefinieerd.
    0   Geregistreerd providertype is ongeldig.
   (   Sleutelset is niet gedefinieerd.
  0   Geregistreerde sleutelset is ongeldig.
    D   Providertype komt niet overeen met de geregistreerde waarde.
  ,   De digitale handtekening is corrupt.
  @   Provider-DLL is niet op de juiste wijze ge�nitialiseerd.
  (   Provider-DLL is niet gevonden.
    ,   De sleutelsetparameter is ongeldig.
      Interne fout.
    Basisfout.
    ,   Fout bij cryptografische bewerking.
   0   De cryptografische algoritme is onbekend.
 8   De objectidentifier kent een ongeldige indeling.
     Ongeldig berichttype.
 0   Bericht is niet gecodeerd zoals verwacht.
 @   Het bericht bevat niet het verwachte verificatiekenmerk.
  (   De hashwaarde is niet correct.
    $   De indexwaarde is ongeldig.
   4   De inhoud van het bericht is al gedecodeerd.
  <   De inhoud van het bericht is nog niet gedecodeerd.
    H   Het ingesloten-gegevensbericht bevat niet de opgegeven ontvanger.
 (   Het besturingstype is ongeldig.
   ,   Ongeldige verlener en/of serienummer.
 <   De oorspronkelijke ondertekenaar is niet gevonden.
    8   Het bericht bevat de gevraagde kenmerken niet.
    P   Het doorgestuurde bericht kan de gevraagde gegevens nog niet retourneren.
 D   De opgegeven lengte voor de uitvoergegevens is onvoldoende.
   @   Er is een fout opgetreden bij het coderen of decoderen.
   L   Er is een fout opgetreden bij het lezen of schrijven van het bestand
  4   Kan het object of de eigenschap niet vinden.
  (   Object of eigenschap bestaat al
   D   Er is geen provider opgegeven voor de opslag of het object
    <   Het opgegeven certificaat heeft zichzelf ondertekend.
 @   Het vorige certificaat of de CRL-context is verwijderd.
   P   Geen overeenkomende gegevens gevonden bij poging om het object te vinden.
 `   Het type van het cryptografische bericht dat gedecodeerd wordt is anders dan verwacht.
    <   Het certificaat heeft geen eigenschap priv�-sleutel
   T   Kan geen certificaat vinden met eigenschap priv�-sleutel voor het decoderen.
  H   Bericht is niet cryptografisch of heeft een onjuiste indeling.
    \   Het ondertekende bericht kent geen ondertekenaar in de opgegeven ondertekenaarsindex.
 L   Uiteindelijke afsluiting wacht op resterende vrijgaven of sluitingen.
 4   Certificaat of ondertekening is ingetrokken
   T   Kan geen DLL of ge�xporteerde functie vinden voor verificatie van intrekking.
 t   De aangeroepen functie kan de intrekkingscontrole voor het certificaat of de ondertekening niet uitvoeren.
    t   Aangezien de intrekkingsserver off line is, kan de aangeroepen functie de intrekkingscontrole niet uitvoeren.
 �   Het certificaat of de handtekening die moet worden gecontroleerd, is niet gevonden in de database met intrekkingen op de server.
  4   De tekenreeks bevat een niet-numeriek teken.
  8   De tekenreeks bevat een niet-afdrukbaar teken.
    X   De tekenreeks bevat een teken dat niet in de 7-bits ASCII-tekenset thuishoort.
    \   De reeks bevat een ongeldige X500-naamkenmerksleutel, EID, waarde of scheidingsteken.
 �   Database met foutcodes bij coderen/decoderen OSS-certificaten

Zie asn1code.h voor een definitie van de OSS-runtimefouten. De 
OSS-foutwaarden worden voorafgegaan door CRYPT_E_OSS_ERROR.
 D   De opgegeven vertrouwensprovider is onbekend op dit systeem.
  p   De opgegeven vertrouwensverificatieactie wordt niet ondersteund door de opgegeven vertrouwensprovider.
    �   Het opgegeven formaat  voor het onderwerp wordt niet ondersteund door of is niet bekend bij de opgegeven vertrouwensprovider.
 D   Het onderwerp wordt niet vertrouwd voor de opgegeven actie.
   4   Fout door probleem bij coderingsproces ASN.1.
 8   Fout door probleem bij decoderingsproces ASN.1.
   T   Lezen/schrijven van extensies waar kenmerken werden gevraagd, en andersom.
    (   Onverwachte cryptografische fout
  ,   Kan grootte van gegevens niet bepalen
 D   Kan grootte van gegevens met onbepaalde grootte niet bepalen.
 @   Dit object leest/schrijft geen zelfregulerende gegevens
   0   Geen handtekening aanwezig in onderwerp.
  <   Een vereist certificaat is op dit moment niet geldig.
 T   De geldigheidstermijnen van de certificatieketen zijn niet correct genest.
    h   Certificaat dat alleen gebruikt kan worden als eindeenheid, wordt als CA gebruikt, of andersom.
   D   Lengtebeperking van pad in certificatieketen is overschreden.
 \   Extensie van onbekend type met de aanduiding 'kritiek' is aanwezig in certificaat.
    H   Certificaat is in gebruik voor andere doeleinden dan toegestaan.
  \   Gegeven subcertificaat heeft geen relatie met een van de bovenliggende certificaten.
  H   Certificaat ontbreekt of heeft lege waarde voor essentieel veld.
  �   Certificatieketen met succes doorlopen, maar be�indigd bij een basiscertificaat dat niet vertrouwd wordt door de provider.
    <   Certificaatketen is niet geschakeld zoals zou moeten.
 $   Generieke vertrouwensfout.
    D   Certificaat is expliciet ingetrokken door de verlener ervan.
  