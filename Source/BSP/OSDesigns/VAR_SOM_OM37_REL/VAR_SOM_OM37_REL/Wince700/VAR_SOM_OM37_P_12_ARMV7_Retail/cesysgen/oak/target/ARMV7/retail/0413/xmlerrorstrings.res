        ��  ��                  l�      �� ��     0             � � �   ��0���   �����  �����"  ��L��)   �����=  ��2��?   �����X  �����Y  ����X^   ��<���d  ������tx  ������H|   ����T  �� ����  P������   �����                                                                                                                                                                                                                  4  X M L _ S T R R E S O U R C E _ M A S K  
     �  X M L - b e s t a n d e n   ( * . x m l ) | * . x m l | X S L - b e s t a n d e n   ( * . x s l ) | * . x s l | A l l e   b e s t a n d e n   ( * . * ) | * . * |  
   (  X M L _ E R R O R _ M A S K  
     `  F o u t   b i j   h e t   o p e n e n   v a n   i n v o e r b e s t a n d :   % 1 .  
     h  V e r w i j z i n g   n a a r   n i e t   n a d e r   o m s c h r e v e n   i t e m   % 1 .  
     |  H e t   i t e m   % 1   b e v a t   e e n   o n e i n d i g e   l u s   v a n   i t e m v e r w i j z i n g e n .  
   �  K a n   h e t   s l e u t e l w o o r d   N D A T A   n i e t   g e b r u i k e n   i n   e e n   d e c l a r a t i e   v a n   p a r a m e t e r - i t e m s .  
     �  K a n   a l g e m e e n   g e p a r s e e r d   i t e m   % 1   n i e t   g e b r u i k e n   a l s   d e   w a a r d e   v o o r   k e n m e r k   % 2 .  
   �  K a n   n i e t - g e p a r s e e r d   i t e m   % 1   n i e t   g e b r u i k e n   i n   e e n   i t e m v e r w i j z i n g .  
   �  K a n   n i e t   v e r w i j z e n   n a a r   e e n   e x t e r n   a l g e m e e n   g e p a r s e e r d   i t e m   % 1   i n   e e n   k e n m e r k w a a r d e .  
     P  X M L - p a r s e r   g e s t o p t   d o o r   g e b r u i k e r .  
     T  F o u t   b i j   h e t   p a r s e r e n   v a n   i t e m   % 1 .   % 2  
   �  P a r a m e t e r - i t e m   m o e t   w o r d e n   g e d e f i n i e e r d   v o o r d a t   h e t   w o r d t   g e b r u i k t .  
   �  D e z e l f d e   n a a m   m a g   n i e t   m e e r   d a n   � � n   k e e r   v o o r k o m e n   i n   e e n   d e c l a r a t i e   m e t   g e m e n g d e   i n h o u d :   % 1 .    
     $ D e   i t e m n a m e n ,   P I - d o e l e n ,   n o t a t i e n a m e n   e n   k e n m e r k w a a r d e n   v a n   d e   t y p e n   I D ,   I D R E F ( S ) ,   E N T I T Y ( I E S )   o f   N O T A T I O N   m o g e n   g e e n   d u b b e l e   p u n t   b e v a t t e n .  
     �  H e t   e l e m e n t   % 1   w o r d t   g e b r u i k t ,   m a a r   i s   n i e t   g e d e c l a r e e r d   i n   D T D / S c h e m a .  
   �  H e t   k e n m e r k   % 1   m e t   n a a m r u i m t e   % 2   v e r w i j s t   n a a r   d e   i d   % 3 ,   d i e   n e r g e n s   i n   h e t   d o c u m e n t   i s   g e d e f i n i e e r d .  
   �  F o u t   i n   s t a n d a a r d k e n m e r k w a a r d e   d i e   i s   g e d e f i n i e e r d   i n   D T D / S c h e m a .  
   �  G e r e s e r v e e r d e   n a a m r u i m t e   % 1   k a n   n i e t   o p n i e u w   w o r d e n   g e d e c l a r e e r d .  
   h  E l e m e n t   m a g   v o l g e n s   D T D / S c h e m a   n i e t   l e e g   z i j n .  
     t  I n h o u d   v a n   e l e m e n t   i s   o n v o l l e d i g   v o l g e n s   D T D / S c h e m a .  
     �  D e   n a a m   v a n   h e t   b o v e n s t e   e l e m e n t   m o e t   o v e r e e n k o m e n   m e t   d e   n a a m   v a n   d e   D O C T Y P E - d e c l a r a t i e .  
   p  I n h o u d   v a n   e l e m e n t   i s   o n g e l d i g   v o l g e n s   D T D / S c h e m a .  
     �  H e t   k e n m e r k   % 1   v o o r   d i t   e l e m e n t   i s   n i e t   g e d e f i n i e e r d   i n   D T D / S c h e m a .  
   �  H e t   k e n m e r k   % 1   h e e f t   e e n   w a a r d e   d i e   n i e t   o v e r e e n k o m t   m e t   d e   v a s t e   w a a r d e   d i e   i s   g e d e f i n i e e r d   i n   D T D / S c h e m a .  
   �  H e t   k e n m e r k   % 1   h e e f t   e e n   o n g e l d i g e   w a a r d e   v o l g e n s   D T D / S c h e m a .  
   �  I n   d e z e   c o n t e x t   i s   g e e n   t e k s t   t o e g e s t a a n   v o l g e n s   D T D / S c h e m a .  
     �  E e n   k e n m e r k d e c l a r a t i e   m a g   n i e t   m e e r d e r e   v a s t e   w a a r d e n   b e v a t t e n :   % 1 .  
   L  D e   n o t a t i e   % 1   i s   a l   g e d e c l a r e e r d .  
   P  H e t   e l e m e n t   % 1   i s   a l   g e d e c l a r e e r d .  
     `  V e r w i j z i n g   n a a r   o n g e d e c l a r e e r d   e l e m e n t :   % 1 .  
   |  V e r w i j z i n g   n a a r   o n g e d e c l a r e e r d   n a a m r u i m t e v o o r v o e g s e l :   % 1 .  
   �  G e b r u i k   v a n   s t a n d a a r d d e c l a r a t i e k e n m e r k   v o o r   n a a m r u i m t e   w o r d t   i n   D T D   n i e t   o n d e r s t e u n d .  
   0  O n b e k e n d e   f o u t :   % 1 .  
   P  H e t   v e r e i s t e   k e n m e r k   % 1   o n t b r e e k t .  
     �  D e c l a r a t i e   % 1   b e v a t   e e n   v e r w i j z i n g   n a a r   o n g e d e f i n i e e r d e   n o t a t i e   % 2 .  
   �  K a n   g e e n   m e e r d e r e   i d - k e n m e r k e n   d e f i n i � r e n   v o o r   h e t z e l f d e   e l e m e n t .  
   �  E e n   k e n m e r k   v a n   h e t   t y p e   i d   k a n   g e e n   s t a n d a a r d w a a r d e   h e b b e n .  
     @  D e   t a a l - i d   % 1   i s   o n g e l d i g .  
     H  D e   o p e n b a r e   i d   % 1   i s   o n g e l d i g .  
     0  W o r d t   v e r w a c h t :   % 1 .  
   x  A l l e e n   e e n   s t a n d a a r d n a a m r u i m t e   k a n   e e n   l e g e   U R L   h e b b e n .  
   0  K a n   % 1   n i e t   l a d e n .  
     X  K a n   t e k e n   n i e t   o p s l a a n   i n   c o d e r i n g   % 1 .  
     T  I n h o u d   m o e t   l e e g   z i j n   v o l g e n s   S c h e m a .  
   `  A l l e e n   t e k s t   i s   t o e g e s t a a n   v o l g e n s   S c h e m a .  
     H  O n g e l d i g e   x m l : s p a c e - d e c l a r a t i e .  
   �  E r   w o r d t   v e r w e z e n   n a a r   d e   i d   % 1 ,   d i e   n i e t   i n   h e t   d o c u m e n t   i s   g e d e f i n i e e r d .  
     �  F o u t   i n   g e l d i g h e i d s b e p e r k i n g :   z e l f s t a n d i g e   d o c u m e n t d e c l a r a t i e .  
     �  K a n   g e e n   m e e r v o u d i g e   N O T A T I O N - k e n m e r k e n   d e f i n i � r e n   o p   h e t z e l f d e   e l e m e n t .  
     �  E e n   k e n m e r k   v a n   h e t   t y p e   N O T A T I O N   m o e t   n i e t   w o r d e n   g e d e c l a r e e r d   o p   e e n   e l e m e n t   d a t   i s   g e d e c l a r e e r d   a l s   E M P T Y .  
   �  E r   i s   e e n   n a a m r u i m t e   g e v o n d e n ,   m a a r   d e z e   w o r d t   n i e t   o n d e r s t e u n d   o p   d e   h u i d i g e   l o c a t i e .  
     p  O n j u i s t e   d e f i n i t i e   v o o r   h e t   h o o f d e l e m e n t   i n   S c h e m a .  
   h  E l e m e n t   % 1   i s   n i e t   t o e g e s t a a n   i n   d e z e   c o n t e x t .  
     �  E e n   E l e m e n t T y p e - d e c l a r a t i e   m o e t   e e n   ' n a m e ' - k e n m e r k   b e v a t t e n .  
     x  E e n   e l e m e n t d e c l a r a t i e   m o e t   e e n   ' t y p e ' - k e n m e r k   b e v a t t e n .  
   �  S c h e m a   o n d e r s t e u n t   a l l e e n   v o l g o r d e t y p e   ' s e q ' ,   ' o n e '   e n   ' m a n y ' .  
     �  I n h o u d   m o e t   ' t e x t O n l y '   z i j n   w a n n e e r   g e g e v e n s t y p e   o p   e e n   e l e m e n t t y p e   w o r d t   g e b r u i k t .  
   p  V o l g o r d e   m o e t   ' m a n y '   z i j n   a l s   d e   i n h o u d   ' m i x e d '   i s .  
   �  D e   i n h o u d   m o e t   v a n   h e t   t y p e   ' e m p t y ' ,   ' e l t O n l y ' ,   ' t e x t O n l y '   o f   ' m i x e d '   z i j n .  
   t  D e   w a a r d e   v a n   h e t   m o d e l   m o e t   ' o p e n '   o f   ' c l o s e d '   z i j n .  
   �  K a n   g e e n   o n d e r l i g g e n d e   e l e m e n t e n   b e v a t t e n   o m d a t   i n h o u d   i s   i n g e s t e l d   o p   ' t e x t O n l y ' .  
     p  M o e t   t e n   m i n s t e   � � n   ' e l e m e n t '   i n   e e n   g r o e p   o p g e v e n .  
   �  H e t   k e n m e r k   % 1   w o r d t   n i e t   o n d e r s t e u n d   o p   d e   d e c l a r a t i e   % 2 .  
     �  A t t r i b u t e T y p e - d e c l a r a t i e   m o e t   e e n   ' n a m e ' - k e n m e r k   b e v a t t e n .  
     D  D u b b e l e   k e n m e r k d e c l a r a t i e .      
     \  O n g e l d i g e   w a a r d e   v o o r   k e n m e r k   ' r e q u i r e d ' .  
   D  O n b e k e n d   k e n m e r k g e g e v e n s t y p e .  
   H  D u b b e l e   g e g e v e n s t y p e d e c l a r a t i e .  
   �  E e n   e l e m e n t   m e t   e e n   ' v a l u e s ' - k e n m e r k   m o e t   e e n   t y p e k e n m e r k   m e t   d e   w a a r d e   ' e n u m e r a t i o n '   h e b b e n .  
   �  M o e t   e e n   ' v a l u e s ' - k e n m e r k   o p g e v e n   v o o r   e e n   e l e m e n t   d a t   e e n   t y p e k e n m e r k   m e t   d e   w a a r d e   ' e n u m e r a t i o n '   b e v a t .  
   p  K e n m e r k d e c l a r a t i e   m o e t   e e n   ' t y p e ' - k e n m e r k   b e v a t t e n .  
   t  V e r w i j z i n g   n a a r   e e n   n i e t - g e d e f i n i e e r d   k e n m e r k t y p e   % 1  
     �  E e n   ' g r o u p '   i s   n i e t   t o e g e s t a a n   i n   e e n   E l e m e n t T y p e   m e t   e e n   ' t e x t O n l y ' - i n h o u d s m o d e l .  
     t  H e t   k e n m e r k   % 1   w o r d t   n i e t   o n d e r s t e u n d   v o o r   e e n   g r o e p .  
   l  D e   w a a r d e n   v o o r   h e t   t y p e   ' e n u m e r a t i o n '   o n t b r e k e n .  
   T  D e   s t a n d a a r d w a a r d e   % 1   i s   n i e t   g e l d i g .  
   �  D a t a t y p e   i s   n i e t   t o e g e s t a a n   a l s   h e t   i n h o u d s m o d e l   n i e t   ' t e x t O n l y '   i s .  
     �  O n d e r l i g g e n d   e l e m e n t   i s   n i e t   t o e g e s t a a n   a l s   h e t   i n h o u d s m o d e l   ' e m p t y '   i s .  
     �  O n d e r l i g g e n d   e l e m e n t   i s   n i e t   t o e g e s t a a n   a l s   h e t   g e g e v e n s t y p e   i s   i n g e s t e l d .  
     \  T y p e   o n t b r e e k t   v o o r   h e t   d a t a t y p e - e l e m e n t .  
   x  D e   w a a r d e   v a n   h e t   k e n m e r k   ' m i n O c c u r s '   m o e t   0   o f   1   z i j n .  
   x  D e   w a a r d e   v a n   h e t   k e n m e r k   ' m a x O c c u r s '   m o e t   1   o f   *   z i j n .  
   �  D e   w a a r d e   v a n   h e t   k e n m e r k   ' m a x O c c u r s '   m o e t   *   z i j n   a l s   h e t   k e n m e r k   ' o r d e r '   o p   ' m a n y '   i s   i n g e s t e l d .  
   |  D e   w a a r d e   v a n   h e t   g e g e v e n s t y p e k e n m e r k   m a g   n i e t   l e e g   z i j n .  
   T  D O C T Y P E   i s   n i e t   t o e g e s t a a n   i n   S c h e m a .  
   T  T y p e   m o e t   e e n   k e n m e r k   ' n a m e '   h e b b e n .  
     p  A l s   ' c o n t e n t '   ' m i x e d '   i s ,   m o e t   ' o r d e r '   ' m a n y '   z i j n .  
   @  T y p e   % 1   i s   a l   g e d e f i n i e e r d .  
   H  H e t   k e n m e r k   ' t y p e '   i s   v e r e i s t .  
     T  % 1   i s   g e e n   g e l d i g e   n a a m   o f   t y p e n a a m .  
     H  K e n m e r k   % 1   i s   a l   g e d e f i n i e e r d .  
     �  I n   S c h e m a   w o r d t   v e r w e z e n   n a a r   t y p e   % 1 ,   m a a r   h e t   i s   n i e t   g e d e f i n i e e r d .  
   p  D e z e l f d e   e l e m e n t n a a m   v e r w i j s t   n a a r   e e n   a n d e r   t y p e .  
     d  H e t   k e n m e r k   ' o r d e r '   i s   n i e t   t o e g e s t a a n   o p   % 1 .  
   H  H e t   k e n m e r k t y p e   % 1   i s   o n g e l d i g .  
   t  V e r w i j z i n g   n a a r   e e n   n i e t - g e d e f i n i e e r d   k e n m e r k t y p e   % 1  
     �  H e t   k e n m e r k   ' m i n O c c u r s '   w o r d t   n i e t   o n d e r s t e u n d   v o o r   h e t   e l e m e n t   o p   h e t   h o o g s t e   n i v e a u   i n   S c h e m a .  
     �  H e t   k e n m e r k   ' m a x O c c u r s '   w o r d t   n i e t   o n d e r s t e u n d   v o o r   h e t   e l e m e n t   o p   h e t   h o o g s t e   n i v e a u   i n   S c h e m a .  
     �  H e t   k e n m e r k   ' r e v i s e s '   w o r d t   n i e t   o n d e r s t e u n d   v o o r   h e t   e l e m e n t   o p   h e t   h o o g s t e   n i v e a u   i n   S c h e m a .  
     t  % 1   i s   a l l e e n   t o e g e s t a a n   a l s   d e   i n h o u d   ' t e x t O n l y '   i s .  
     �  " c o n t e n t V a l u e s "   m u s t   h a v e   a t   l e a s t   o n e   " v a l u e "   s u b e l e m e n t .  
     T  D e   w a a r d e   v o o r   m i n L e n g t h   i s   o n g e l d i g .  
   T  D e   w a a r d e   v o o r   m a x L e n g t h   i s   o n g e l d i g .  
   �  D e   w a a r d e   v o o r   m i n L e n g t h   m a g   n i e t   g r o t e r   z i j n   d a n   d e   w a a r d e   v o o r   m a x L e n g t h .  
   t  V e r w i j z i n g   n a a r   n i e t - g e d e f i n i e e r d   i n   l i n e   S c h e m a   % 1 .  
     T  I n   l i n e   S c h e m a   % 1   i s   a l   g e d e f i n i e e r d .  
   \  ' m a x O c c u r s '   o p   k e n m e r k   k a n   a l l e e n   1   z i j n .  
   �  M e e r d e r e   e x e m p l a r e n   v a n   % 1   z i j n   n i e t   t o e g e s t a a n   i n   e e n   t y p e d e f i n i t i e .  
   �  ' m a x I n c l u s i v e '   e n   ' m a x E x c l u s i v e '   k u n n e n   n i e t   b e i d e   i n   e e n   t y p e d e f i n i t i e   v o o r k o m e n .  
     �  ' m i n I n c l u s i v e '   e n   ' m i n E x c l u s i v e '   k u n n e n   n i e t   b e i d e   i n   e e n   t y p e d e f i n i t i e   v o o r k o m e n .  
     P  ' d t : m a x L e n g t h '   i s   a l   g e d e f i n i e e r d .  
     P  ' d t : m i n L e n g t h '   i s   a l   g e d e f i n i e e r d .  
     t  % 1   i s   n i e t   t o e g e s t a a n   w a n n e e r   % 2   o o k   w o r d t   g e b r u i k t .  
     \  D u b b e l e   d e f i n i t i e   v a n   % 1   m e t   d e   n a a m   % 2 .  
     H  W a a r d e   % 1   b e h o o r t   n i e t   t o t   % 2 .  
     P  W a a r d e   % 1   b e h o o r t   n i e t   t o t   # a l l % 2 .  
     @  K e n m e r k   h e e f t   w a a r d e   n o d i g .  
   @  V e r w a c h t   e e n   e n k e l e   w a a r d e .  
   <  b i j   h e t   v e r w e r k e n   v a n   % 1 .  
   p  % 1   v e r o o r z a a k t   e e n   c o n f l i c t   m e t   e e n   b e s t a a n d   t y p e .  
     d  m a x O c c u r s   k a n   n i e t   m i n d e r   z i j n   d a n   m i n O c c u r s .  
   P  % 1   i s   g e e n   g e l d i g e   t y p e v e r w i j z i n g .  
     �  D e   v e r w i j z i n g   g e e f t   t a r g e t N a m e s p a c e   v a n   % 1   o p ,   m a a r   h e t   s c h e m a   g e e f t   % 2   o p .  
    D e   b e p e r k i n g e n   m i n L e n g t h / m a x L e n g t h   k u n n e n   a l l e e n   w o r d e n   o p g e g e v e n   m e t   d e   g e g e v e n s t y p e n   ' s t r i n g ' ,   ' n u m b e r ' ,   ' b i n . h e x '   o f   ' b i n . b a s e 6 4 ' .  
   D  D e   i d   % 1   i s   d u b b e l   a a n w e z i g .  
     l  F o u t   b i j   h e t   p a r s e r e n   v a n   % 1   a l s   g e g e v e n s t y p e   % 2 .  
   p  E r   i s   e e n   n a a m r u i m t e c o n f l i c t   v o o r   d e   n a a m r u i m t e   % 1 .  
   l  K a n   e e n   k e n m e r k   m e t   O b j e c t - w a a r d e   n i e t   u i t b r e i d e n  
   |  K a n   g e e n   2   g e g e v e n s t y p e k e n m e r k e n   h e b b e n   v o o r   � � n   e l e m e n t .  
   T  I n v o e g p o s i t i e k n o o p p u n t   n i e t   g e v o n d e n  
     8  K n o o p p u n t   n i e t   g e v o n d e n  
   �  D e z e   b e w e r k i n g   k a n   n i e t   w o r d e n   u i t g e v o e r d   m e t   e e n   k n o o p p u n t   v a n   h e t   t y p e   % 1 .  
      % 1   i s   g e e n   g e l d i g   k e n m e r k   v o o r   d e   X M L - d e c l a r a t i e .  
 A l l e e n   d e   k e n m e r k e n   ' v e r s i o n ' ,   ' e n c o d i n g '   e n   ' s t a n d a l o n e '   z i j n   t o e g e s t a a n .  
   �  H e t   i s   n i e t   t o e g e s t a a n   o m   e e n   k n o o p p u n t   o f   h e t   b o v e n l i g g e n d e   k n o o p p u n t   o n d e r   h e t   k n o o p p u n t   z e l f   i n   t e   v o e g e n .  
   �  I n v o e g p o s i t i e k n o o p p u n t   m o e t   e e n   o n d e r l i g g e n d   k n o o p p u n t   z i j n   v a n   h e t   k n o o p p u n t   w a a r o n d e r   h e t   w o r d t   i n g e v o e g d .  
     �  K e n m e r k e n   z i j n   n i e t   t o e g e s t a a n   v o o r   k n o o p p u n t e n   v a n   h e t   t y p e   % 1 .  
     �  H e t   p a r a m e t e r k n o o p p u n t   i s   g e e n   o n d e r l i g g e n d   k n o o p p u n t   v a n   d i t   k n o o p p u n t .  
     �  A l s   u   k n o o p p u n t e n   v a n   h e t   t y p e   % 1   w i l t   m a k e n ,   m o e t   u   e e n   g e l d i g e   n a a m   o p g e v e n .  
     L  O n v e r w a c h t e   n a a m r u i m t e p a r a m e t e r .  
     l  D e   v e r e i s t e   p a r a m e t e r   o n t b r e e k t   ( o f   i s   n u l / l e e g ) .  
   T  H e t   n a a m r u i m t e k n o o p p u n t   i s   o n g e l d i g .  
     l  P o g i n g   o m   e e n   a l l e e n - l e z e n   k n o o p p u n t   t e   w i j z i g e n .  
   0  T o e g a n g   g e w e i g e r d .  
     �  K e n m e r k e n   m o e t e n   w o r d e n   v e r w i j d e r d   v o o r d a t   d e z e   a a n   e e n   a n d e r   k n o o p p u n t   w o r d e n   t o e g e v o e g d .  
     x  O n g e l d i g e   g e g e v e n s   v o o r   e e n   k n o o p p u n t   v a n   h e t   t y p e   % 1 .  
     P  B e w e r k i n g   a f g e b r o k e n   d o o r   o p r o e p e r .  
   �  K a n   d e   p o s i t i e   v a n   d e   h e r h a l e r   i n   d e   l i j s t   v a n   k n o o p p u n t e n   n i e t   a c h t e r h a l e n .  
     �  D e   o f f s e t   m o e t   0   o f   g r o t e r   z i j n ,   m a a r   n i e t   g r o t e r   d a n   h e t   a a n t a l   t e k e n s   i n   d e   g e g e v e n s .  
   �  H e t   o p g e g e v e n   k n o o p p u n t   i s   n i e t   a l s   k e n m e r k   o p g e g e v e n   o p   d i t   k n o o p p u n t .  
   �  D e z e   b e w e r k i n g   k a n   n i e t   w o r d e n   u i t g e v o e r d   o p   e e n   D O C T Y P E - k n o o p p u n t .  
   �  K a n   v e r s c h i l l e n d e   t h r e a d i n g - m o d e l l e n   n i e t   i n   e e n   d o c u m e n t   c o m b i n e r e n .  
   X  G e g e v e n s t y p e   % 1   w o r d t   n i e t   o n d e r s t e u n d .  
   X  D e   n a a m   v a n   d e   e i g e n s c h a p   i s   o n g e l d i g .  
     L  D e   e i g e n s c h a p s w a a r d e   i s   o n g e l d i g .  
   @  H e t   o b j e c t   i s   a l l e e n - l e z e n .  
   �  A l l e e n   X M L S c h e m a c a c h e - s c h e m a c o l l e c t i e s   k u n n e n   w o r d e n   g e b r u i k t .  
     \  D e   l e n g t e   i s   g r o t e r   d a n   d e   m a x i m u m l e n g t e .  
   `  D e   l e n g t e   i s   k l e i n e r   d a n   d e   m i n i m u m l e n g t e .  
     �  H e t   v a l i d e r e n   i s   m i s l u k t   o m d a t   h e t   d o c u m e n t   n i e t   p r e c i e s   � � n   h o o f d k n o o p p u n t   b e v a t .  
     �  H e t   v a l i d e r e n   i s   m i s l u k t   o m d a t   h e t   h o o f d e l e m e n t   g e e n   b i j b e h o r e n d   D T D / s c h e m a   h e e f t .  
     <  H e t   v a l i d e r e n   i s   m i s l u k t .  
   �  D e   i n d e x   v e r w i j s t   n a a r   e e n   i t e m   d a t   z i c h   v o o r b i j   h e t   e i n d e   v a n   d e   l i j s t   b e v i n d t .  
     �  A a n   e e n   k n o o p p u n t   v a n   h e t   t y p e   % 1   m a g   n i e t   d e   n a a m   % 2   z i j n   t o e g e w e z e n .  
     l  D e   v e r e i s t e   e i g e n s c h a p   h e e f t   g e e n   g e l d i g e   w a a r d e .  
   �  O n g e l d i g e   b e w e r k i n g   t i j d e n s   d e   u i t v o e r i n g   v a n   e e n   t r a n s f o r m a t i e .  
     h  D e   g e b r u i k e r   h e e f t   d e   t r a n s f o r m a t i e   a f g e b r o k e n .  
   \  H e t   d o c u m e n t   i s   n i e t   v o l l e d i g   g e p a r s e e r d .  
    D i t   o b j e c t   k a n   d e   % 1 - g e b e u r t e n i s   n i e t   o p v a n g e n .   E r   i s   e e n   f o u t   o p g e t r e d e n   t i j d e n s   h e t   o r d e n e n   v a n   d e   I D i s p a t c h - i n t e r f a c e   v a n   h e t   o b j e c t .  
     �  H e t   X S L - o p m a a k m o d e l d o c u m e n t   m o e t   v r i j e   t h r e a d s   b e v a t t e n   z o d a t   h e t   k a n   w o r d e n   g e b r u i k t   m e t   h e t   X S L - s j a b l o o n o b j e c t .  
   �  D e   w a a r d e   v a n   d e   e i g e n s c h a p   S e l e c t i o n N a m e s p a c e s   i s   o n g e l d i g .   A l l e e n   g e l d i g e   x m l n s - k e n m e r k e n   z i j n   t o e g e s t a a n .  
     |  D e z e   n a a m   m a g   n i e t   h e t   t e k e n   % 2   b e v a t t e n :  
  
 % 1 - - > % 2 < - - % 3  
   �  D e z e   n a a m   m a g   n i e t   b e g i n n e n   m e t   h e t   t e k e n   % 2 :  
  
 % 1 - - > % 2 < - - % 3  
   d  E e n   l e g e   t e k e n r e e k s   ' '   i s   g e e n   g e l d i g e   n a a m .  
     D D e   S e r v e r H T T P R e q u e s t - e i g e n s c h a p   k a n   n i e t   w o r d e n   g e b r u i k t   b i j   h e t   a s y n c h r o o n   l a d e n   v a n   e e n   d o c u m e n t   e n   w o r d t   a l l e e n   o n d e r s t e u n d   d o o r   W i n d o w s   N T   4 . 0   e n   h o g e r .  
     �  O v e r l o o p   v a n   d e   X S L - p r o c e s s o r s t a c k   -   d e   o o r z a a k   i s   w a a r s c h i j n l i j k   o n e i n d i g e   s j a b l o o n r e c u r s i e .  
   h  S l e u t e l w o o r d   % 1   m a g   h i e r   n i e t   w o r d e n   g e b r u i k t .  
     �  D e   h o o f d m a p   v a n   e e n   X S L - o p m a a k m o d e l   m o e t   e e n   e l e m e n t   z i j n .  
     X  S l e u t e l w o o r d   % 1   m a g   n i e t   o p   % 2   v o l g e n .  
     8  % 1   i s   g e e n   s c r i p t t a a l .  
     �  D e   a a n   f o r m a t I n d e x   d o o r g e g e v e n   w a a r d e   m o e t   g r o t e r   d a n   0   z i j n .  
   H  T e k e n r e e k s   i n   o n g e l d i g   f o r m a a t .  
   �  r e g e l   =   % 1 ,   k o l o m   =   % 2   ( r e g e l   w o r d t   b e r e k e n d   v a n a f   h e t   b e g i n   v a n   h e t   s c r i p t b l o k ) .  
   x  F o u t   g e r e t o u r n e e r d   d o o r   e i g e n s c h a p -   o f   m e t h o d e a a n r o e p .  
     ,  S y s t e e m f o u t :   % 1 .  
     T  S l e u t e l w o o r d   % 1   m a g   % 3   n i e t   b e v a t t e n .  
   �  S l e u t e l w o o r d   % 1   m a g   n i e t   i n   n a a m r u i m t e   % 2   w o r d e n   g e b r u i k t .  
     x  D e   w a a r d e   v a n   h e t   k e n m e r k   % 1   m a g   a l l e e n   % 2   o f   % 3   z i j n .  
     �  H e t   o p m a a k m o d e l   % 1   k a n   z i c h z e l f   n i e t   d i r e c t   o f   i n d i r e c t   b e v a t t e n .  
   8  % 1   i s   g e e n   g e l d i g e   U R L .  
   �  H e t   o p m a a k m o d e l   b e v a t   g e e n   d o c u m e n t e l e m e n t .   H e t   o p m a a k m o d e l   i s   m o g e l i j k   l e e g   o f   h e t   i s   e e n   o n g e l d i g   X M L - d o c u m e n t .  
   L  F o u t   b i j   h e t   p a r s e r e n   v a n   % 1 .   % 2  
     T  % 1   i s   g e e n   g e l d i g e   p r i o r i t e i t s w a a r d e .  
   �  D e   v e r w i j z i n g   n a a r   d e   v a r i a b e l e   o f   p a r a m e t e r   % 1   m o e t   e e n   l i j s t   m e t   k n o o p p u n t e n   o p l e v e r e n .  
   �  D e   v a r i a b e l e   o f   p a r a m e t e r   % 1   k a n   n i e t   t w e e m a a l   b i n n e n   d e z e l f d e   s j a b l o o n   w o r d e n   g e d e f i n i e e r d .  
     L K a n   e e n   v e r w i j z i n g   n a a r   d e   v a r i a b e l e   o f   p a r a m e t e r   % 1   n i e t   o p l o s s e n .   D e   v a r i a b e l e   o f   p a r a m e t e r   i s   m o g e l i j k   n i e t   g e d e f i n i e e r d   o f   d e z e   v a l t   n i e t   b i n n e n   h e t   b e r e i k .  
     �  N a a m r u i m t e   % 1   m a g   g e e n   s c r i p t b l o k k e n   b e v a t t e n   m e t   a n d e r e   w a a r d e n   v a n   h e t   k e n m e r k   ' t a a l ' .  
     P  N a a m r u i m t e   % 1   b e v a t   g e e n   f u n c t i e s .  
     T  N a a m r u i m t e   % 1   b e v a t   f u n c t i e   % 2   n i e t .  
     ( F u n c t i e   % 1   h e e f t   g e e n   w a a r d e   g e r e t o u r n e e r d ,   o f   d e z e   h e e f t   e e n   w a a r d e   g e r e t o u r n e e r d   d i e   n i e t   n a a r   e e n   X S L - g e g e v e n s t y p e   k a n   w o r d e n   g e c o n v e r t e e r d .  
   �  % 1  
  
 E r   i s   e e n   f o u t   o p g e t r e d e n   t i j d e n s   e e n   a a n r o e p   v a n   e i g e n s c h a p   o f   m e t h o d e   % 2 .  
   �  % 1  
  
 E r   i s   e e n   f o u t   o p g e t r e d e n   m e t   p a r a m e t e r   % 2   t i j d e n s   e e n   a a n r o e p   v a n   e i g e n s c h a p   o f   m e t h o d e   % 3 .  
     h  D e   w a a r d e   v a n   h e t   k e n m e r k   % 1   m a g   n i e t   % 2   z i j n .  
      D e   g l o b a l e   v a r i a b e l e   % 1   m a g   g e e n   d i r e c t e   o f   i n d i r e c t e   v e r w i j z i n g   n a a r   z i c h z e l f   b e v a t t e n .   K r i n g d e f i n i t i e s   z i j n   n i e t   t o e g e s t a a n .  
     l  S l e u t e l w o o r d   % 1   m a g   g e e n   % 2 - k n o o p p u n t e n   b e v a t t e n .  
   �  D e   v a r i a b e l e   o f   p a r a m e t e r   % 1   k a n   n i e t   z o w e l   e e n   g e s e l e c t e e r d   k e n m e r k   a l s   e e n   n i e t - l e g e   i n h o u d   b e v a t t e n .  
   �  S j a b l o o n   m e t   n a a m   % 1   k a n   m a a r   e e n m a a l   w o r d e n   g e d e f i n i e e r d   m e t   d e z e l f d e   i m p o r t v o l g o r d e .  
     t  S j a b l o o n   m e t   n a a m   % 1   b e s t a a t   n i e t   i n   h e t   o p m a a k m o d e l .  
   �  D e   a l g e m e n e   v a r i a b e l e   o f   p a r a m e t e r   % 1   k a n   m a a r   e e n m a a l   w o r d e n   g e d e f i n i e e r d   m e t   d e z e l f d e   i m p o r t v o l g o r d e .  
   �  D e   i n s t r u c t i e   x s l : a p p l y - i m p o r t s   k a n   n i e t   a a n   d e   i n h o u d   v a n   e e n   x s l : f o r - e a c h - i n s t r u c t i e   w o r d e n   t o e g e v o e g d .  
   P E e n   v e r w i j z i n g   n a a r   k e n m e r k s e t   % 1   k a n   n i e t   w o r d e n   o p g e l o s t .   E e n   x s l : a t t r i b u t e - s e t   m e t   d e z e   n a a m   m o e t   o p   h e t   h o o g s t e   n i v e a u   v a n   h e t   o p m a a k m o d e l   w o r d e n   g e d e c l a r e e r d .  
   �  K e n m e r k s e t   % 1   k a n   n i e t   d i r e c t   o f   i n d i r e c t   n a a r   z i c h z e l f   v e r w i j z e n .  
     �  % 1  
  
 E r   i s   e e n   f o u t   o p g e t r e d e n   b i j   h e t   c o m p i l e r e n   v a n   t o e g e v o e g d   o f   g e � m p o r t e e r d   o p m a a k m o d e l   % 2 .  
   D  K e n m e r k   % 1   i s   o n g e l d i g   o p   % 2 .  
   �  x s l : c h o o s e   m o e t   m i n i m a a l   e e n   o n d e r l i g g e n d   x s l : w h e n - i t e m   h e b b e n .  
   �  % 1   i s   g e e n   g e l d i g e   w a a r d e   v o o r   e e n   v o o r v o e g s e l   v a n   e e n   n a a m r u i m t e .  
     d  E l e m e n t   % 1   i s   g e e n   h e r k e n d   e x t e n s i e - e l e m e n t .  
     x  H e t   o p m a a k m o d e l - e l e m e n t   m a g   g e e n   e x t e n s i e - e l e m e n t   z i j n .  
   L E e n   v e r w i j z i n g   n a a r   s l e u t e l   % 1   k a n   n i e t   w o r d e n   o p g e l o s t .   E e n   x s l : k e y - i n s t r u c t i e   m e t   d e z e   n a a m   m o e t   o p   h e t   h o o g s t e   n i v e a u   v a n   h e t   o p m a a k m o d e l   w o r d e n   g e d e f i n i e e r d .  
   `  % 1   i s   g e e n   g e l d i g e   X S L T -   o f   X P a t h - f u n c t i e .  
     �  F u n c t i e   % 1   i n   n a a m r u i m t e   % 2   m o e t   e e n   v e r z a m e l i n g   k n o o p p u n t e n   r e t o u r n e r e n .  
   |  A r g u m e n t   % 1   m o e t   e e n   v e r z a m e l i n g   k n o o p p u n t e n   r e t o u r n e r e n .  
   �  x s l : t e m p l a t e   m a g   g e e n   m o d e - k e n m e r k   h e b b e n   a l s   h e t   g e e n   m a t c h - k e n m e r k   h e e f t .  
   �  D e   f u n c t i e   % 1   ( )   m a g   n i e t   i n   e e n   m a t c h - p a t r o o n   w o r d e n   g e b r u i k t .  
   x  D e   w a a r d e   v a n   h e t   k e n m e r k   % 1   m o e t   e e n   e n k e l   t e k e n   z i j n .  
   �  H e t   k e n m e r k   % 1   o p   x s l : d e c i m a l - f o r m a t   % 2   m a g   n i e t   o p n i e u w   w o r d e n   g e d e f i n i e e r d   m e t   e e n   w a a r d e   v a n   % 3 .  
   p  D e c i m a l e   o p m a a k   % 1   b e s t a a t   n i e t   i n   h e t   o p m a a k m o d e l .  
   �  U i t v o e r i n g   v a n   d e   s c r i p t c o d e   b i n n e n   d i t   o p m a a k m o d e l   w o r d t   n i e t   t o e g e s t a a n   d o o r   d e   b e v e i l i g i n g s i n s t e l l i n g e n .  
   H  V e r w a c h t   t o k e n   % 1   g e v o n d e n   % 2 .  
     4  O n v e r w a c h t   t o k e n   % 1 .  
     \  E x p r e s s i e   r e t o u r n e e r t   g e e n   D O M - k n o o p p u n t .  
   x  E x p r e s s i e   m o e t   e e n   q u e r y   o f   e e n   i n t e g e r c o n s t a n t e   z i j n .  
     D  O n v o l l e d i g e   q u e r y - e x p r e s s i e .  
     0  O n b e k e n d e   m e t h o d e .  
     T  O n v e r w a c h t   t e k e n   i n   q u e r y t e k e n r e e k s .  
     �  % 1   m a g   n i e t   r e c h t s   v a n   /   o f   / /   s t a a n ,   o f   g e b r u i k t   w o r d e n   m e t   | .  
   d  E x p r e s s i e   m o e t   e e n   t e k e n r e e k s c o n s t a n t e   z i j n .  
     T  O b j e c t   o n d e r s t e u n t   d e z e   m e t h o d e   n i e t .  
   |  E x p r e s s i e   k a n   n i e t   o p   d i t   g e g e v e n s t y p e   w o r d e n   i n g e s t e l d .  
     4 D e   X M L - s c r i p t e n g i n e   o n d e r s t e u n t   g e e n   s c r i p t f r a g m e n t e n .   D e z e   f o u t   i s   w a a r s c h i j n l i j k   v e r o o r z a a k t   d o o r   e e n   s c r i p t l a b e l   w a a r v a n   h e t   t a a l k e n m e r k   o p   ' X M L '   i s   i n g e s t e l d ,   o f   w a a r v a n   h e t   t e k s t k e n m e r k   o p   ' t e x t / x m l '   i s   i n g e s t e l d ,   d i e   z i c h   v o o r   e e n   a n d e r e   s c r i p t l a b e l   o p   d e   p a g i n a   b e v i n d t .  
   |  P a r a m e t e r   m o e t   e e n   q u e r y   o f   e e n   t e k e n r e e k s c o n s t a n t e   z i j n .  
   \  P a r a m e t e r   m o e t   e e n   i n t e g e r c o n s t a n t e   z i j n .  
   � < % 1 T A B L E   w i d t h = 4 0 0 >  
 < % 1 P   s t y l e = " f o n t : 1 3 p t / 1 5 p t   v e r d a n a " >   D e   X M L - p a g i n a   k a n   n i e t   w o r d e n   w e e r g e g e v e n  
 < % 1 P   s t y l e = " f o n t : 8 p t / 1 1 p t   v e r d a n a " > K a n   X M L - i n v o e r   n i e t   l e z e n   m e t   o p m a a k m o d e l   % 3 .  
 H e r s t e l   d e   f o u t   e n   t i k   v e r v o l g e n s   o p   d e   k n o p    
 < % 1 a   h r e f = " j a v a s c r i p t : l o c a t i o n . r e l o a d ( ) "   t a r g e t = " _ s e l f " >  
 V e r n i e u w e n < / % 1 a >   o f   p r o b e e r   h e t   l a t e r   o p n i e u w .  
 < % 1 H R > < % 1 P   s t y l e = " f o n t : b o l d   8 p t / 1 1 p t   v e r d a n a " > % 2 < / % 1 P > < % 1 / T A B L E >  
 < % 1 p r e   s t y l e = " f o n t : 1 0 p t / 1 2 p t " > < % 1 f o n t   c o l o r = " b l u e " > % 4 < / % 1 f o n t > < / % 1 p r e >  
     4  R e g e l   % 1 ,   p o s i t i e   % 2  
     l  H e t   X M L - b r o n b e s t a n d   k a n   n i e t   w o r d e n   w e e r g e g e v e n .  
     �  G e l i j k t e k e n   o n t b r e e k t   t u s s e n   k e n m e r k   e n   w a a r d e   v a n   h e t   k e n m e r k .  
   �  E r   w o r d t   e e n   l e t t e r l i j k e   t e k e n r e e k s   v e r w a c h t ,   m a a r   e r   i s   g e e n   b e g i n a a n h a l i n g s t e k e n   g e v o n d e n .  
     d  O n j u i s t e   s y n t a x i s   g e b r u i k t   i n   e e n   o p m e r k i n g .  
     X  E e n   n a a m   b e g i n t   m e t   e e n   o n g e l d i g   t e k e n .  
   P  E e n   n a a m   b e v a t   e e n   o n g e l d i g   t e k e n .  
     �  H e t   t e k e n   <   m a g   n i e t   w o r d e n   g e b r u i k t   i n   e e n   k e n m e r k w a a r d e .  
     `  O n g e l d i g e   s y n t a x i s   v o o r   e e n   X M L - d e c l a r a t i e .  
   t  E r   i s   e e n   o n g e l d i g   t e k e n   g e v o n d e n   i n   d e   t e k s t i n h o u d .  
     @  V e r e i s t e   s p a t i e   o n t b r e e k t .  
     @  H e t   t e k e n   >   w o r d t   v e r w a c h t .  
   H  O n g e l d i g   t e k e n   g e v o n d e n   i n   D T D .  
   t  E e n   o n g e l d i g   t e k e n   i s   g e v o n d e n   i n   e e n   D T D - d e c l a r a t i e .  
   L  E r   w o r d t   e e n   p u n t k o m m a   v e r w a c h t .  
     |  E e n   o n g e l d i g   t e k e n   i s   g e v o n d e n   b i n n e n   e e n   i t e m v e r w i j z i n g .  
   p  A a n t a l   h a a k j e s   o p e n e n   e n   s l u i t e n   k o m t   n i e t   o v e r e e n .  
   <  E r   w o r d t   e e n   [   v e r w a c h t .  
     �  O n g e l d i g e   s y n t a x i s   i n   e e n   v o o r w a a r d e l i j k e   o f   e e n   C D A T A - s e c t i e .  
     $  I n t e r n e   f o u t .  
   `  S p a t i e   i s   n i e t   t o e g e s t a a n   o p   d e z e   l o c a t i e .  
     �  B e s t a n d s e i n d e   b e r e i k t   i n   o n g e l d i g e   s t a t u s   v o o r   h u i d i g e   c o d e r i n g .  
     l  G e m e n g d   i n h o u d s m o d e l   m a g   d i t   t e k e n   n i e t   b e v a t t e n .  
   �  G e m e n g d   i n h o u d s m o d e l   m o e t   g e d e f i n i e e r d   w o r d e n   a l s   n u l   o f   m e e r ( ' * ' ) .  
   H  O n g e l d i g   t e k e n   i n   i n h o u d s m o d e l .  
   ,  H a a k j e   o n t b r e e k t .  
   d  O n g e l d i g   t e k e n   g e v o n d e n   i n   A T T L I S T - o p s o m m i n g .  
   T  O n g e l d i g e   s y n t a x i s   i n   P I - d e c l a r a t i e .  
     �  E e n   e n k e l   o f   e e n   d u b b e l   a a n h a l i n g s t e k e n   ( \ '   o f   \ " )   o n t b r e e k t .  
   |  M e e r d e r e   d u b b e l e   p u n t e n   z i j n   n i e t   t o e g e s t a a n   i n   e e n   n a a m .  
   \  O n g e l d i g   t e k e n   v o o r   e e n   d e c i m a a l   c i j f e r .  
     d  O n g e l d i g   t e k e n   v o o r   e e n   h e x a d e c i m a a l   c i j f e r .  
     8  O n g e l d i g   U n i c o d e - t e k e n .  
   H  S p a t i e   o f   v r a a g t e k e n   v e r w a c h t .  
     <  D e   p a r s e r   i s   o n d e r b r o k e n .  
   4  D e   p a r s e r   i s   g e s t o p t .  
   h  E i n d l a b e l   w o r d t   n i e t   v e r w a c h t   o p   d e z e   l o c a t i e .  
     d  D e   v o l g e n d e   l a b e l s   z i j n   n i e t   a f g e s l o t e n :   % 1 .  
     (  D u b b e l   k e n m e r k .  
   �  E e n   X M L - d o c u m e n t   m a g   m a x i m a a l   � � n   e l e m e n t   v a n   h e t   h o o g s t e   n i v e a u   b e v a t t e n .  
     l  O n g e l d i g   o p   h e t   h o o g s t e   n i v e a u   v a n   h e t   d o c u m e n t .  
     <  O n g e l d i g e   X M L - d e c l a r a t i e .  
   �  X M L - d o c u m e n t   m o e t   e e n   e l e m e n t   h e b b e n   o p   h e t   h o o g s t e   n i v e a u .  
   <  O n v e r w a c h t   b e s t a n d s e i n d e .  
   �  P a r a m e t e r - i t e m s   k u n n e n   n i e t   w o r d e n   g e b r u i k t   b i n n e n   o p m a a k d e c l a r a t i e s   i n   e e n   i n t e r n e   d e e l v e r z a m e l i n g .  
     �  D e   v e r v a n g i n g s t e k s t   v o o r   e e n   p a r a m e t e r - i t e m   m o e t   j u i s t   w o r d e n   g e n e s t   m e t   g r o e p e n   t u s s e n   r o n d e   h a k e n .  
     �  D e   l e t t e r l i j k e   t e k e n r e e k s   ] ] >   i s   n i e t   t o e g e s t a a n   i n   e l e m e n t i n h o u d .  
     p  D e   v e r w e r k i n g   v a n   d e   i n s t r u c t i e   i s   n i e t   a f g e s l o t e n .  
   @  E l e m e n t   i s   n i e t   a f g e s l o t e n .  
   \  I n   h e t   e i n d e l e m e n t   o n t b r e e k t   h e t   t e k e n   > .  
   h  E e n   l e t t e r l i j k e   t e k e n r e e k s   i s   n i e t   a f g e s l o t e n .  
     L  E e n   o p m e r k i n g   i s   n i e t   a f g e s l o t e n .  
   P  E e n   d e c l a r a t i e   i s   n i e t   a f g e s l o t e n .  
     \  E e n   o p m a a k d e c l a r a t i e   i s   n i e t   a f g e s l o t e n .  
     T  E e n   C D A T A - s e c t i e   i s   n i e t   a f g e s l o t e n .  
     T  D e c l a r a t i e   h e e f t   e e n   o n g e l d i g e   n a a m .  
     8  E x t e r n e   i d   i s   o n g e l d i g .  
   p  E e n   X M L - e l e m e n t   i s   n i e t   t o e g e s t a a n   b i n n e n   e e n   D T D .  
     �  H e t   n a a m r u i m t e v o o r v o e g s e l   m a g   n i e t   b e g i n n e n   m e t   d e   g e r e s e r v e e r d e   t e k e n r e e k s   x m l .  
     l  H e t   k e n m e r k   ' v e r s i o n '   i s   v e r e i s t   o p   d e z e   l o c a t i e .  
   p  H e t   k e n m e r k   ' e n c o d i n g '   i s   v e r e i s t   o p   d e z e   l o c a t i e .  
     h  T e n   m i n s t e   � � n   n a a m   i s   v e r e i s t   o p   d e z e   l o c a t i e .  
   �  H e t   o p g e g e v e n   k e n m e r k   w o r d t   n i e t   v e r w a c h t   o p   d e z e   l o c a t i e .  
 H e t   k e n m e r k   i s   m o g e l i j k   h o o f d l e t t e r g e v o e l i g .  
     l  E i n d l a b e l   % 2   k o m t   n i e t   o v e r e e n   m e t   b e g i n l a b e l   % 1 .  
   p  H e t   s y s t e e m   o n d e r s t e u n t   d e   o p g e g e v e n   c o d e r i n g   n i e t .  
   �  O m s c h a k e l i n g   v a n   h u i d i g e   c o d e r i n g   n a a r   o p g e g e v e n   c o d e r i n g   w o r d t   n i e t   o n d e r s t e u n d .  
   D  N D A T A - s l e u t e l w o o r d   o n t b r e e k t .  
   <  I n h o u d s m o d e l   i s   o n g e l d i g .  
   X  O n g e l d i g   t y p e   g e d e f i n i e e r d   i n   A T T L I S T .  
     �  H e t   x m l : s p a c e - k e n m e r k   h e e f t   e e n   o n g e l d i g e   w a a r d e .   D e   e n i g e   g e l d i g e   w a a r d e n   z i j n   ' d e f a u l t '   e n   ' p r e s e r v e ' .  
     �  M e e r d e r e   n a m e n   g e v o n d e n   i n   e e n   k e n m e r k w a a r d e ,   t e r w i j l   e r   m a a r   � � n   w o r d t   v e r w a c h t .  
   �  O n g e l d i g e   A T T D E F - d e c l a r a t i e .   # R E Q U I R E D ,   # I M P L I E D   o f   # F I X E D   w o r d t   v e r w a c h t .  
     �  D e   n a a m   x m l   i s   g e r e s e r v e e r d   e n   m o e t   i n   k l e i n e   l e t t e r s   w o r d e n   g e t y p t .  
     �  V o o r w a a r d e l i j k e   s e c t i e s   z i j n   n i e t   t o e g e s t a a n   i n   e e n   i n t e r n e   d e e l v e r z a m e l i n g .  
     T  C D A T A   i s   n i e t   t o e g e s t a a n   i n   e e n   D T D .  
     �  H e t   k e n m e r k   ' s t a n d a l o n e '   m o e t   d e   w a a r d e   ' y e s '   o f   ' n o '   h e b b e n .  
   �  H e t     k e n m e r k   ' s t a n d a l o n e '   k a n   n i e t   w o r d e n   g e b r u i k t   i n   e x t e r n e   i t e m s .  
     l  D O C T Y P E - d e c l a r a t i e   m a g   n i e t   v o o r k o m e n   i n   e e n   D T D .  
   `  V e r w i j z i n g   n a a r   e e n   n i e t - g e d e f i n i e e r d   i t e m .  
   �  I t e m v e r w i j z i n g   i s   v e r e e n v o u d i g d   n a a r   e e n   o n g e l d i g   t e k e n   v o o r   e e n   n a a m .  
     �  D O C T Y P E - d e c l a r a t i e   m a g   n i e t   b u i t e n   d e   b e g i n s e c t i e   v o o r k o m e n .  
     8  O n g e l d i g   v e r s i e n u m m e r .  
     l  D T D - d e c l a r a t i e   m a g   n i e t   b u i t e n   e e n   D T D   v o o r k o m e n .  
   p  M e e r d e r e   D O C T Y P E - d e c l a r a t i e s   z i j n   n i e t   t o e g e s t a a n .  
     P  F o u t   b i j   h e t   v e r w e r k e n   v a n   b r o n   % 1 .  
   X  E r   i s   e e n   n a a m   v e r e i s t   o p   d e z e   l o c a t i e .  
   D  N o d e T e s t   w o r d t   h i e r   v e r w a c h t .  
   �  D e   f u n c t i e   L a s t ( )   m o e t   w o r d e n   u i t g e v o e r d   o p   e e n   v e r z a m e l i n g   k n o o p p u n t e n .  
     <  B o o l e - p a r a m e t e r   v e r w a c h t .  
   <  N u m m e r p a r a m e t e r   v e r w a c h t .  
   \  E e n   a r g u m e n t   n a a r   d e   o p e r a n d   i s   o n g e l d i g .  
   X  F o u t   b i j   h e t   o p l o s s e n   v a n   d e   v a r i a b e l e .  
   0  E x p r e s s i e   v e r w a c h t .  
   ,  O n g e l d i g e   a s n a a m .  
   �  D e   t e k e n r e e k s w a a r d e   o p   d e z e   l o c a t i e   m o e t   e e n   Q N a m e   o p l e v e r e n .  
   �  H e t   o p m a a k s y m b o o l   % 1   m a g   i n   d e z e   s e c t i e   v a n   e e n   o p m a a k p a t r o o n   n i e t   v o l g e n   o p   h e t   o p m a a k s y m b o o l   % 2 .  
     @  O n g e l d i g   a a n t a l   a r g u m e n t e n .  
   L  H e t   g e t a l   v a l t   b u i t e n   h e t   b e r e i k .  
   �  D e z e   q u e r y   k a n   n i e t   v o l g e n   o p   e e n   q u e r y   d i e   m o g e l i j k   m e e r   d a n   � � n   k n o o p p u n t   r e t o u r n e e r t .  
     D  O n v o l l e d i g e   X P A T H - i n s t r u c t i e .  
   H  O n g e l d i g   o v e r e e n k o m e n d   p a t r o o n .  
   D  K a n   d e z e   Q N a m e   n i e t   o p l o s s e n .  
   �  E r   m o g e n   b i n n e n   d e z e   e x p r e s s i e   g e e n   v a r i a b e l e n   w o r d e n   g e b r u i k t .  
   �  H e t   c o n t e x t k n o o p p u n t   i n   e e n   X S L T - t r a n s f o r m a t i e   m a g   g e e n   t e k s t   z i j n   w a a r u i t   t e k e n s   z i j n   v e r w i j d e r d .  
      E e n   k n o o p p u n t   b i n n e n   % 1   m a g   n i e t   w o r d e n   d o o r g e g e v e n   a l s   h e t   c o n t e x t k n o o p p u n t   v o o r   e e n   X S L T - t r a n s f o r m a t i e   o f   e e n   X P a t h - q u e r y .  
     4 E e n   o n d e r l i g g e n d   t e k s t k n o o p p u n t   v a n   e e n   k e n m e r k   m a g   n i e t   w o r d e n   d o o r g e g e v e n   a l s   h e t   c o n t e x t k n o o p p u n t   v o o r   e e n   X S L T - t r a n s f o r m a t i e   o f   e e n   X P a t h - q u e r y .  
      E e n   k n o o p p u n t   v a n   t y p e   % 1   m a g   n i e t   w o r d e n   d o o r g e g e v e n   a l s   h e t   c o n t e x t k n o o p p u n t   v o o r   e e n   X S L T - t r a n s f o r m a t i e   o f   e e n   X P a t h - q u e r y .  
     �  E e n   s t a n d a a r d k e n m e r k   m a g   n i e t   w o r d e n   d o o r g e g e v e n   a l s   h e t   c o n t e x t k n o o p p u n t   v o o r   e e n   X S L T - t r a n s f o r m a t i e   o f   e e n   X P a t h - q u e r y .  
   