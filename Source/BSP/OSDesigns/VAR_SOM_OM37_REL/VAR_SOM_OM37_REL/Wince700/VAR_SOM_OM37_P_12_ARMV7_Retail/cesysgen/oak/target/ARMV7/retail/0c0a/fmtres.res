        ��  ��                  �;     �� ��     0 
        }       "   �  $   $   �  &   '   �  2   H   ,  P   P   �  R   Y   �  d   r   0  u   �   �  �   �   ,  �   �   T,  �   �   �,  �   �   �,  �   �   d-  �   �   �-  �   �   .  �   �   2  �   �   �3  �   �   4  �   �   05  �   �   06  �   �   L6      �6  
    �6      L7      `8        �8  *  +  �8  =  =  t9  �  �  �9      0:  �  �  �:  �  �  t<    :  �E  L  l  �O  t  v  �Z  ~  �  �[  �  �  �a    :  �o  <  s  |  x  �   �  �  �  ��  �  �  ��  �  �  ��  �  �  D�  �  �  l�  �  (  د  j  ~  0�  �  �  ��  <  <  �  �  �  ��  �  �  ��  a	  b	  ��  d	  d	  p�  �  �  ��  �  �  ,�      �  �  �  ��  �  �  �     T�       ��     <�  0 0 ��  @ @ ��  p r �  � � l�  � � �  � � l�  � � ��  � � ��      ��    �
  ���  @ �!@ �P�   � ���   ����  ��������   � ���   � ���  ������  ���� �  ����p�  '��/����  ������,�  ň�ƈ�`�  ψ�ψ���  �������  J��J����  ��������   � ��   �	 �,   � ��   � ��   � �$   �! �� P �P �� W �W �� p �p � � �� �d � �	�� ��$ ��`   � �� d �m ��
  ��p ��, @�@�� P�U� `�a� p�p�` ����| ����� ����, ����( ����  � �!  � �X! W �W �x!  �	 ��!  � ��#  	�! 	�H$ 	�	��)  	� 	� -   	�# 	�02  0	� 0	�P3  � ��3  ��T7                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 0   La operaci�n se ha completado con �xito.
     Funci�n incorrecta.
   <   El sistema no puede hallar el archivo especificado.
   8   El sistema no puede hallar la ruta especificada.
  ,   El sistema no puede abrir el archivo.
    Acceso denegado.
      Identificador no v�lido.
  D   Los bloques de control de almacenamiento han sido destruidos.
 H   No queda suficiente almacenamiento para procesar este comando.
    D   Direcci�n de bloque de control de almacenamiento no v�lida.
       El entorno es incorrecto.
 H   Se ha intentado cargar un programa con un
formato incorrecto.
    $   C�digo de acceso no v�lido.
      Datos no son v�lidos.
 H   No queda suficiente almacenamiento para completar esta operaci�n.
 @   El sistema no puede hallar el controlador especificado.
   (   No se puede quitar el directorio.
 D   El sistema no puede mover el archivo
a otra unidad de disco.
    No hay m�s archivos.
  0   El medio est� protegido contra escritura.
 @   El sistema no puede hallar el dispositivo especificado.
   $   El dispositivo no est� listo.
 0   El dispositivo no reconoce el comando.
    8   Error de datos (prueba c�clica de redundancia).
   P   El programa lanz� un comando pero
la longitud del comando es incorrecta.
 L   La unidad no puede hallar un �rea
o pista espec�fica en el disco.
    <   No se tiene acceso al disco o disquete especificados.
 8   La unidad no puede hallar el sector requerido.
    (   No queda papel en la impresora.
   D   El sistema no puede escribir en el dispositivo especificado.
  D   El sistema no puede leer desde el dispositivo especificado.
   D   Uno de los dispositivos vinculados al sistema no funciona.
    \   El proceso no tiene acceso al archivo porque
est� siendo utilizado por otro proceso.
 h   El proceso no tiene acceso al archivo porque
otro proceso tiene bloqueada una parte del archivo.
 |   El disco insertado en la unidad no es el correcto.
Inserte %2 (N�mero de serie del volumen: %3)
en la unidad %1.
    8   Hay demasiados archivos abiertos para compartir.
  ,   Se ha alcanzado el final del archivo.
    El disco est� lleno.
  0   La petici�n de la red no est� permitida.
  ,   El equipo remoto no est� disponible.
  ,   Existe un nombre duplicado en la red.
 8   No se ha encontrado la ruta de acceso de la red.
     La red est� ocupada.
  \   El recurso de red o el dispositivo especificados
ya no se encuentran disponibles.
    @   Se ha alcanzado el l�mite del comando de BIOS de la red.
  0   Error hardware en el adaptador de red.
    L   El servidor especificado no puede ejecutar la
operaci�n solicitada.
      Error de red inesperado.
  ,   El adaptador remoto no es compatible.
 (   La cola de impresi�n est� llena.
  \   El servidor no dispone de espacio para almacenar
el archivo que se va a imprimir.
    D   Eliminado el archivo que estaba esperando para imprimirse.
    <   El nombre de red especificado ya no est� disponible.
      Acceso a la red denegado.
 0   El tipo de recurso de red no es correcto.
 8   No se encuentra el nombre de red especificado.
    D   Excedido el l�mite del nombre del adaptador
de red local.
    8   Sobrepasado el l�mite de sesi�n de BIOS de red.
   L   El servidor remoto se ha parado o est� en el
proceso de reiniciarse.
 |   Este equipo remoto no puede realizar m�s conexiones por el momento
ya que ha superado el n�mero m�ximo permitido.
    H   Se ha detenido la impresora o dispositivo de disco especificado.
      Este archivo ya existe.
   4   No se puede crear el directorio o archivo.
       Error en INT 24.
  <   No hay lugar disponible para procesar esta petici�n.
  @   Ya se est� utilizando el nombre del dispositivo local.
    8   La contrase�a de red especificada no es v�lida.
   $   El par�metro no es correcto.
  $   Error de escritura en la red.
 D   El sistema no puede iniciar otro proceso en
este momento.
    4   No se puede crear otro sem�foro de sistema.
   8   Este sem�foro exclusivo pertenece a otro proceso.
 8   Se ha fijado el sem�foro y no se puede cerrar.
    0   No se puede volver a fijar el sem�foro.
   H   No se puede pedir sem�foros exclusivos durante la interrupci�n.
   <   Ha terminado la posesi�n anterior de este sem�foro.
   ,   Introduzca el disco de la unidad %1.
  T   El programa se ha detenido porque no se ha insertado un disquete alternativo.
 @   Ya hay otro proceso utilizando
o bloqueando este disco.
  $   Ha terminado la canalizaci�n.
 H   El sistema no puede abrir el
dispositivo o archivo especificado.
 0   El nombre del archivo es demasiado largo.
 0   No hay espacio suficiente en el disco.
    D   No hay m�s identificadores de archivos internos disponibles.
  H   El identificador de archivo interno de destino no es correcto.
    D   La llamada a IOCTL efectuada por el programa no
es v�lida.
   H   El valor del par�metro de verificar-al-escribir no
es v�lido.
    4   Este comando no es compatible con el sistema.
 4   Esta funci�n s�lo es v�lida en modo Win32.
    8   Ha terminado el intervalo de espera del sem�foro.
 T   El �rea de datos transferida a una llamada del
sistema es demasiado peque�a.
 L   El nombre de archivo, directorio o etiqueta del volumen no es v�lido.
 4   El nivel de llamada del sistema no es v�lido.
 0   El disco no tiene etiqueta de volumen.
    8   No se ha podido encontrar el m�dulo especificado.
 4   No se ha encontrado el proceso especificado.
  $   No hay subprocesos a esperar.
 <   La aplicaci�n %1 no se puede ejecutar en modo Win32.
  �   Se ha intentado usar un identificador de archivos con una partici�n
de disco abierta para una operaci�n distinta de una E/S de disco sin procesar.
   T   Se ha intentado mover el puntero del archivo m�s all� del inicio del archivo.
 `   El puntero del archivo no puede establecerse en el dispositivo o archivo especificado.
    t   No se puede utilizar un comando JOIN o SUBST
para una unidad que contiene
unidades unidas con anterioridad.
 \   Se ha intentado utilizar el comando
JOIN o SUBST en una unidad que ya
estaba unida.
 d   Se ha intentado utilizar el comando
JOIN o SUBST en una unidad que ya
se hab�a sustituido.
  T   El sistema ha intentado eliminar la
uni�n de una unidad que no est� unida.
   T   El sistema ha intentado eliminar la
sustituci�n de una unidad sin sustituir.
 X   El sistema ha intentado unir una unidad
a un directorio de una unidad ya unida.
  `   El sistema ha intentado sustituir una unidad
por un directorio en una unidad sustituida.
 \   El sistema ha intentado unir una unidad
a un directorio de una unidad sustituida.
    `   El sistema ha intentado sustituir una unidad
por un directorio de una unidad ya unida.
   D   El sistema no puede ejecutar JOIN o SUBST en este momento.
    d   El sistema no puede unir o sustituir una
unidad a o por un directorio en la misma unidad.
    @   El directorio no es un subdirectorio del directorio ra�z.
 $   El directorio no est� vac�o.
  H   La ruta de acceso especificada se est�
usando en un sustituto.
   L   No hay suficientes recursos disponibles
para procesar este comando.
  <   No se puede usar la ruta de acceso en este momento.
   �   Se ha intentado unir o sustituir
una unidad donde uno de los
directorios de la unidad es el
destino de una sustituci�n anterior.
   �   La informaci�n de seguimiento del sistema no se ha especificado
en el archivo CONFIG.SYS o no se permite el seguimiento.
 \   El n�mero de eventos del sem�foro especificados
para DosMuxSemWait no es correcto.
   T   No se ha ejecutado DosMuxSemWait. Se han establecido
demasiados sem�foros.
   0   La lista de DosMuxSemWait no es v�lida.
   p   El nombre del volumen introducido sobrepasa el l�mite de
 caracteres del sistema de archivos de destino.
 ,   No se puede crear otro subproceso.
    4   El proceso receptor ha rechazado la se�al.
    @   Ya se ha rechazado el segmento y no se puede bloquear.
    (   El segmento ya est� desbloqueado.
 D   La direcci�n del identificador del subproceso no es correcta.
 D   La cadena de argumentos pasada a DosExecPgm no es correcta.
   4   La ruta de acceso especificada no es v�lida.
  $   Ya hay una se�al pendiente.
   8   No se pueden crear m�s subprocesos en el sistema.
 <   No se ha podido bloquear una regi�n de un archivo.
    8   Ya se est� utilizando el recurso especificado.
    P   No hab�a una petici�n de bloqueo pendiente para la regi�n proporcionada.
  L   El sistema de archivos no admite cambios at�micos al tipo de bloqueo.
 @   El sistema ha detectado un n�mero de segmento incorrecto.
 4   El sistema operativo no puede ejecutar %1.
    4   No se puede crear un archivo que ya existe.
   4   El indicador que se ha pasado no es correcto.
 H   No se encontr� el nombre del sem�foro de sistema especificado.
    4   El sistema operativo no puede ejecutar %1.
    4   El sistema operativo no puede ejecutar %1.
    4   El sistema operativo no puede ejecutar %1.
    0   No se puede ejecutar %1 en modo Win32.
    4   El sistema operativo no puede ejecutar %1.
    ,   %1 no es una aplicaci�n Win32 v�lida.
 4   El sistema operativo no puede ejecutar %1.
    4   El sistema operativo no puede ejecutar %1.
    @   El sistema operativo no puede ejecutar
esta aplicaci�n.
  P   El sistema operativo no est� configurado
para ejecutar esta aplicaci�n.
  4   El sistema operativo no puede ejecutar %1.
    @   El sistema operativo no puede ejecutar
esta aplicaci�n.
  @   El segmento de c�digo no puede ser mayor o igual que 64K.
 4   El sistema operativo no puede ejecutar %1.
    4   El sistema operativo no puede ejecutar %1.
    P   El sistema no encontr� la opci�n de
entorno que se hab�a especificado.
   `   No hay ning�n proceso en el sub�rbol de
comandos que tenga un controlador de se�ales.
    @   El nombre del archivo o la extensi�n es demasiado largo.
  ,   Se est� usando la pila de anillo 2.
   �   Los caracteres globales del nombre de archivo, * o ?, se han escrito
incorrectamente, o bien se han especificado demasiados caracteres globales.
 (   La se�al fijada no es correcta.
   8   No se puede establecer el controlador de se�ales.
 <   El segmento est� bloqueado y no se puede reasignar.
   p   Hay demasiados m�dulos de v�nculos din�micos adjuntos
a este programa o m�dulo de v�nculos din�micos.
    8   No se pueden anidar las llamadas a LoadModule.
    p   El archivo %1 de imagen es v�lido pero es para un tipo de m�quina distinto
que el de la m�quina actual.
  4   El estado de la canalizaci�n no es v�lido.
    <   Todas las instancias de canalizaci�n est�n en uso.
    (   Se est� cerrando la canalizaci�n.
 D   No hay ning�n proceso en el otro extremo de la canalizaci�n.
  $   Hay m�s datos disponibles.
       Se cancel� la sesi�n.
 D   El nombre del atributo extendido especificado no es v�lido.
   4   Los atributos extendidos no son coherentes.
   $   No se dispone de m�s datos.
   4   No se pueden utilizar las funciones de copia.
 0   El nombre del directorio no es v�lido.
    D   No se han podido cargar los atributos extendidos en el b�fer.
 P   El archivo de atributos extendidos en el sistema de archivos est� da�ado.
 D   El archivo de la tabla de atributos extendidos est� lleno.
    <   El identificador del atributo extendido no es v�lido.
 @   El sistema de archivos no permite atributos extendidos.
   P   Se ha intentado liberar una exclusi�n mutua no perteneciente al proceso.
  8   Se han realizado demasiados env�os a un sem�foro.
 L   S�lo se ha completado parte de la petici�n Read/WriteProcessMemory.
   |   El sistema no puede encontrar el texto del mensaje para el n�mero de mensaje 0x%1
en el archivo de mensajes para %2.
 @   Se ha intentado tener acceso a una direcci�n no v�lida.
   8   El resultado aritm�tico sobrepasa los 32 bits.
    <   Hay un proceso en el otro extremo de la canalizaci�n.
 L   Esperando a que un proceso abra el otro extremo de la canalizaci�n.
   0   Acceso denegado al atributo extendido.
    l   La operaci�n de E/S se ha anulado debido a una
salida de subproceso o a una petici�n de aplicaci�n.
  @   El evento de E/S superpuesto no es un estado se�alizado.
  <   Se est� ejecutando la operaci�n de E/S superpuesta.
   8   El acceso a la direcci�n de memoria no es v�lido.
 4   Error al ejecutar la operaci�n de paginaci�n.
 @   Demasiados niveles de recursividad; se desbord� la pila.
  8   La ventana no puede manipular el mensaje enviado.
 ,   No se puede completar esta funci�n.
       Indicadores no v�lidos.
   �   El volumen no contiene un sistema de archivos reconocido.
Aseg�rese de que est�n cargados todos los controladores del sistema de archivos
y de que el volumen no est� da�ado.
   t   Se ha modificado externamente el volumen para un archivo,
de forma que el archivo abierto ya no es v�lido.
   L   Esta operaci�n no se puede ejecutar en el modo de pantalla completa.
  D   Se ha intentado hacer referencia a un token que no existe.
    D   La base de datos del Registro de configuraciones est� da�ada.
 @   La clave del Registro de configuraciones no es v�lida.
    H   No se ha podido abrir la clave del Registro de configuraciones.
   H   No se ha podido leer la clave del Registro de configuraciones.
    L   No se ha podido escribir la clave del Registro de configuraciones.
    �   Se ha tenido que recuperar uno de los archivos del Registro usando un
registro o copia alternativa. Se ha recuperado en su totalidad.
       El Registro est� da�ado. La estructura de uno de los archivos de
datos del Registro est� da�ado o la imagen del sistema del archivo en memoria
est� da�ada, o no se pudo recuperar porque la
copia o el registro alternativo faltan o est�n da�ados.
   �   Error irrecuperable en una operaci�n de E/S iniciada por el Registro.
El Registro no ha podido leer, escribir o vaciar uno de los archivo con
la imagen de sistema del Registro.
    �   El sistema ha intentado cargar o restaurar un archivo en el Registro pero
el archivo especificado no tiene el formato de un archivo de Registros.
    h   Se ha intentado una operaci�n ilegal en una clave de Registro que se ha marcado para eliminaci�n.
 X   El sistema no ha podido asignar el espacio necesario en un registro del Registro.
 p   No se puede crear un v�nculo simb�lico en una clave del Registro
que ya dispone de subclaves o valores.
  L   No se puede crear una subclave estable en una clave padre vol�til.
    �   Se est� completando una solicitud de notificaci�n de cambio y
la informaci�n no est� siendo devuelta al b�fer del autor de la llamada. El autor de la llamada debe enumerar ahora
los archivos para buscar los cambios.
 l   Se ha enviado un control de detenci�n a un servicio del que
dependen otros servicios en ejecuci�n.
   @   El control solicitado no es v�lido para este servicio.
    `   El servicio no ha respondido a la petici�n o inicio del control en
un tiempo adecuado.
   8   No se pudo crear un subproceso para el servicio.
  4   La base de datos de servicios est� bloqueada.
 <   Ya se est� ejecutando una instancia de este servicio.
 8   El nombre de la cuenta no es v�lido o no existe.
  H   El servicio especificado est� desactivado y no se puede iniciar.
  @   Se ha especificado una dependencia de servicio circular.
  D   El servicio especificado no existe como servicio instalado.
   D   El servicio no acepta mensajes de control en este momento.
    (   No se ha iniciado el servicio.
    X   El proceso del servicio no ha podido conectar con el controlador del servicio.
    P   Error de excepci�n en el servicio al manipular la petici�n de control.
    0   La base de datos especificada no existe.
  @   El servicio ha devuelto un error espec�fico del servicio.
 4   El proceso ha terminado de forma inesperada.
  D   No se ha podido iniciar el servicio o grupo de dependencia.
   T   No se ha podido iniciar el servicio debido a un error en el inicio de sesi�n.
 X   Despu�s de iniciarse, el servicio ha quedado en un estado pendiente de inicio.
    P   El bloqueo de la base de datos del servicio especificado no es v�lido.
    D   El servicio especificado se ha marcado para ser eliminado.
    ,   El servicio especificado ya existe.
   d   Se est� ejecutando el sistema con la �ltima configuraci�n que se sabe que no tiene problemas.
 P   El servicio de dependencia no existe o se ha marcado para ser
eliminado.
 �   Ya se ha aceptado la inicializaci�n actual para usarse como el
control de la �ltima configuraci�n que se sabe que no tiene problemas.
    P   No se ha intentado iniciar el servicio desde la �ltima inicializaci�n.
    d   El nombre ya es usado como nombre de servicio o como nombre de presentaci�n
de servicios.
    �   La cuenta especificada para este servicio es diferente de la cuenta
especificada para otros servicios que se est�n ejecutando en el mismo proceso.
   `   S�lo se pueden establecer acciones de error para servicios Win32, no para controladores.
  �   Este servicio se ejecuta en el mismo proceso que el administrador de control de servicios.
Por tanto, el administrador de control de servicios no puede realizar acciones si
el proceso de este servicio finaliza de forma inesperada.
  P   No se ha configurado ning�n programa de recuperaci�n para este servicio.
  ,   Se ha llegado al final de la cinta.
   <   Un acceso de cinta ha alcanzado una marca de archivo.
 @   Se ha encontrado el inicio de una cinta o una partici�n.
  D   Un acceso de cinta alcanz� el final de un grupo de archivos.
  $   No hay m�s datos en la cinta.
 <   No se ha podido efectuar la partici�n de la cinta.
    x   Al tener acceso a una nueva cinta de una partici�n de multivolumen, 
el tama�o de bloque actual no es correcto.
  L   No se ha encontrado la informaci�n de partici�n al cargar la cinta.
   D   No se puede bloquear el mecanismo de expulsi�n de la cinta.
   (   No se puede descargar el medio.
   @   Es posible que hayan cambiado los medios de la unidad.
    (   Se ha restablecido el bus de E/S.
    La unidad est� vac�a.
 \   El car�cter Unicode no se ha asignado en la p�gina de c�digos multibyte de destino.
   X   Error en una rutina de inicializaci�n de biblioteca de v�nculos din�micos (DLL).
  $   Se est� cerrando el sistema.
  T   No se ha podido anular el apagado del sistema porque no se estaba apagando.
   T   No se ha podido ejecutar el proceso debido a un error del dispositivo de E/S.
 \   No se ha inicializado ning�n dispositivo serie. Se descargar� el controlador serie.
   �   No se ha podido abrir un dispositivo que compart�a una solicitud de interrupci�n (IRQ)
con otros dispositivos. Hay al menos otro dispositivo abierto
que utiliza esa solicitud de interrupci�n (IRQ).
   �   Una operaci�n serie de E/S fue completada por otra escritura en el puerto serie.
(El IOCTL_SERIAL_XOFF_COUNTER alcanz� cero.)
    �   Se complet� una operaci�n de E/S a causa de la finalizaci�n del tiempo de espera.
(El IOCTL_SERIAL_XOFF_COUNTER no alcanz� cero.)
    P   No se ha encontrado una marca de identificador de direcci�n en el disco.
  �   Existe una mala adaptaci�n entre el campo del identificador del disco
y la direcci�n de la pista del controlador del disco.
  l   El controlador de disco ha enviado un error que el controlador
de la unidad de disco no reconoce.
    T   El controlador de disco ha devuelto resultados incoherentes en los registros.
 x   Error en la operaci�n de calibraci�n mientras se obten�a acceso al disco duro incluso despu�s de varios intentos.
 t   Error en la operaci�n de disco mientras se obten�a acceso al disco duro incluso despu�s de varios intentos.
   |   Fue preciso reiniciar el controlador de disco mientras se obten�a acceso
al disco duro, pero tambi�n hubo un error.
  (   Ha llegado al final de la cinta.
  T   No hay almacenamiento suficiente en el servidor para procesar este comando.
   H   Se ha detectado una posibilidad de que ocurra un interbloqueo.
    X   La direcci�n base o el desajuste del archivo no tiene la alineaci�n
adecuada.
    h   El intento de cambiar el estado de la energ�a fue impedido por otra
aplicaci�n o controlador.
    L   El BIOS del sistema no pudo cambiar el estado de energ�a del sistema.
 l   Se ha intentado crear m�s v�nculos a un archivo de los que
el sistema de archivos puede soportar.
    P   El programa especificado requiere una versi�n de Windows m�s reciente.
    L   El programa especificado no es un programa de Windows o de MS-DOS.
    L   No se puede iniciar m�s de una instancia del programa especificado.
   T   El programa especificado se escribi� para una versi�n anterior de Windows.
    `   Uno de los archivos de biblioteca necesario para ejecutar esta aplicaci�n est� da�ado .
   \   No hay ninguna aplicaci�n asociada con el archivo especificado para esta operaci�n.
   4   Error al enviar el comando a la aplicaci�n.
   h   No se puede hallar uno de los archivos de biblioteca necesarios para ejecutar esta aplicaci�n.
    x   El proceso actual ha utilizado todos los identificadores permitidos por el sistema para objetos Window Manager.
   H   El mensaje s�lo se puede utilizar con operaciones sincr�nicas.
    8   El elemento de origen indicado no tiene medios.
   <   El elemento de destino indicado no contiene medios.
   (   El elemento indicado no existe.
   L   El elemento indicado es parte de una revista que no est� presente.
    T   El dispositivo indicado requiere ser reiniciado debido a errores de hardware.
 \   El dispositivo ha indicado que debe limpiarlo antes de intentar nuevas operaciones.
   <   El dispositivo ha indicado que su tapa est� abierta.
  (   El dispositivo no est� conectado.
     Elemento no encontrado.
   X   No se ha encontrado ninguna coincidencia con la clave especificada en el �ndice.
  H   El conjunto de propiedades especificado no existe en el objeto.
   0   El nombre del dispositivo no es v�lido.
   H   El dispositivo no est� conectado pero es una conexi�n memorizada.
 P   Se ha intentado memorizar un dispositivo que ya hab�a sido memorizado.
    H   Ning�n proveedor de red ha aceptado la ruta de acceso de la red.
  4   El nombre del proveedor de red no es v�lido.
  8   No se puede abrir el perfil de conexi�n de red.
   4   El perfil de conexi�n a la red est� da�ado.
   4   No se puede enumerar un objeto no contenedor.
    Error extendido.
  @   El formato del nombre de grupo especificado no es v�lido.
 D   El formato del nombre de sistema especificado no es v�lido.
   D   El formato del nombre de suceso especificado no es v�lido.
    D   El formato del nombre de dominio especificado no es v�lido.
   D   El formato del nombre de servicio especificado no es v�lido.
  @   El formato del nombre de red especificado no es v�lido.
   P   El formato del nombre de recurso compartido especificado no es v�lido.
    H   El formato del nombre de contrase�a especificado no es v�lido.
    D   El formato del nombre de mensaje especificado no es v�lido.
   D   El formato del destino de mensaje especificado no es v�lido.
  X   Las credenciales est�n en conflicto con un conjunto de credenciales existentes.
   �   Se ha intentado establecer una sesi�n en un servidor de red
pero ya se han establecido demasiadas sesiones en ese servidor.
  \   El nombre de dominio o grupo de trabajo ya est� en uso por otro equipo
en la red.
    0   Falta la red o bien no se ha iniciado.
    ,   El usuario ha cancelado la operaci�n.
 t   La operaci�n solicitada no se puede realizar en un archivo con una secci�n asignada por un usuario abierta.
   <   El sistema remoto ha rechazado la conexi�n de red.
    4   La conexi�n de red se cerr� correctamente.
    T   El punto final del transporte de red ya tiene una direcci�n asociada con �l.
  L   Todav�a no se ha asociado una direcci�n con el punto final de red.
    D   Se intent� una operaci�n en una conexi�n de red no existente.
 P   Se ha intentado una operaci�n no v�lida en una conexi�n de red activa.
    8   El transporte no puede localizar la red remota.
   <   El transporte no puede localizar el sistema remoto.
   L   El sistema remoto no es compatible con el protocolo de transporte.
    `   No hay ning�n servicio operativo en el punto final de red destino
en el sistema remoto.
  $   La petici�n ha sido anulada.
  @   La conexi�n de red ha sido anulada por el sistema local.
  L   No se pudo completar la operaci�n. Se deber�a volver a intentarlo.
    �   No se pudo realizar una conexi�n con el servidor porque se ha alcanzado
 el n�mero l�mite de conexiones concurrentes para esta cuenta.
   X   Se intenta iniciar una sesi�n en una hora del d�a no autorizada para esta cuenta.
 P   Esta cuenta no est� autorizada para iniciar una sesi�n en esta estaci�n.
  P   No se ha podido usar la direcci�n de red para la operaci�n solicitada.
    (   El servicio ya est� registrado.
   ,   El servicio especificado no existe.
   X   La operaci�n requerida no se ha realizado porque
no se ha reconocido al usuario.
 �   La operaci�n requerida no se ha realizado porque el usuario
no ha iniciado sesi�n en la red.
El servicio especificado no existe.
    ,   Continuar con el trabajo en curso.
    p   Se intent� efectuar una operaci�n de inicializaci�n cuando
la inicializaci�n ya hab�a sido completada.
   (   No hay m�s dispositivos locales.
  d   No todos los privilegios a los que se hace referencia son asignados al autor de la llamada.
   t   No se han efectuado todas las asignaciones entre los nombres de cuenta y los identificadores de seguridad.
    L   No se han especificado l�mites de cuota de sistema para esta cuenta.
  d   No hay ninguna clave de cifrado disponible. Se ha devuelto una clave de cifrado ya conocida.
  �   La contrase�a de NT es demasiado compleja para convertirla a una de LAN Manager.
La contrase�a devuelta por LAN Manager es una cadena nula.
  ,   No se conoce el nivel de revisi�n.
    <   Indica que dos niveles de revisi�n son incompatibles.
 X   No se puede asignar este identificador de seguridad como propietario del objeto.
  `   No se puede asignar este identificador de seguridad como el grupo primario de un objeto.
  �   Un subproceso ha intentado realizar una operaci�n en una imitaci�n de token
cuando dicho subproceso no suplanta a un cliente.
    (   No se puede desactivar el grupo.
  X   No hay ning�n servidor disponible para iniciar la petici�n de inicio
de sesi�n.
  X    No existe el inicio de sesi�n especificado.  Es posible
 que se haya terminado.
 0    No existe el privilegio especificado.
    <    El cliente no dispone de un privilegio requerido.
    8   Este nombre no es un nombre de cuenta apropiado.
  ,   El usuario especificado ya existe.
    ,   El usuario especificado no existe.
    (   El grupo especificado ya existe.
  (   El grupo especificado no existe.
  �   La cuenta de usuario ya es miembro del grupo especificado
o no se puede eliminar el grupo porque todav�a contiene
un miembro.
   L   La cuenta de usuario no es miembro del grupo de cuentas especificado.
 L   No se puede desactivar o eliminar la �ltima cuenta
administrativa.
   d   No se puede actualizar la contrase�a. La contrase�a especificada como
actual no es correcta.
 �   No se puede actualizar la contrase�a. El valor de la nueva contrase�a
contiene valores que no se permiten en las contrase�as.
    l   No se puede actualizar la contrase�a porque se han infringido las normas
del cambio de contrase�a.
   d   Error de inicio de sesi�n: Se desconoce el nombre de usuario o la contrase�a no es correcta.
  H   Error de inicio de sesi�n: Restricci�n en la cuenta de usuario.
   T   Error de inicio de sesi�n: Infracci�n en la restricci�n de la hora de inicio.
 `   Error de inicio de sesi�n: El usuario no tiene permiso para tener acceso a este equipo.
   <   Error de inicio de sesi�n: La contrase�a ha caducado.
 @   Error de inicio de sesi�n: La cuenta est� desactivada.
    h   No se ha efectuado ning�n mapeo entre los nombres de cuenta y los identificadores de seguridad.
   X   Se han pedido demasiados identificadores de usuario local (LUID) al mismo tiempo.
 H   No hay m�s identificadores de usuario local (LUID) disponibles.
   h   La parte de subautoridad del identificador de seguridad no es v�lida para este uso en particular.
 L   La estructura de la lista de control de acceso (ACL) no es v�lida.
    D   La estructura del identificador de seguridad no es v�lida.
    @   La estructura del descriptor de seguridad no es v�lida.
   x   No se ha podido construir la lista de control de acceso (ACL) o la
entrada de control de acceso (ACE) heredada.
  $   El servidor est� desactivado.
 $   El servidor est� activado.
    D   El valor no es v�lido para una autoridad de identificaci�n.
   X   No hay memoria suficiente para la actualizaci�n de la informaci�n de seguridad.
   d   Los atributos no son v�lidos o no son compatibles con los
atributos del grupo en general.
    l   No se ha proporcionado el nivel de suplantaci�n necesario o el nivel
de suplantaci�n no es v�lido.
   @   No se puede abrir un token de seguridad de nivel an�nimo.
 D   La clase de la validaci�n de informaci�n pedida no es v�lida.
 T   El tipo de token no es apropiado para el uso que se ha intentado hacer de �l.
 h   No se puede realizar una operaci�n de seguridad en un objeto
que no tiene seguridad asociada.
    �   Indica que no se ha podido ponerse en contacto con un servidor Windows NT
Server o los objetos del dominio est�n protegidos de tal forma
que no se puede extraer la informaci�n necesaria.
  �   El servidor de autoridad de seguridad local (LSA) o el administrador
de cuentas de seguridad (SAM) no se encontraba en un estado adecuado
para ejecutar la operaci�n de seguridad.
  d   El dominio no se encontraba en el estado adecuado para ejecutar la operaci�n de seguridad.
    L   S�lo se permite esta operaci�n al controlador de dominio principal.
   ,   El dominio especificado no existe.
    ,   El dominio especificado ya existe.
    P   Se ha intentado exceder el l�mite en el n�mero de dominios por servidor.
  �   No se puede completar la operaci�n porque se ha detectado un
error grave del medio o corrupci�n en la estructura de datos del disco.
 T   La base de datos de la cuenta de seguridad contiene inconsistencias internas.
 t   Una m�scara de acceso que deber�a haberse asignado a tipos
o gen�ricos contiene tipos de acceso gen�ricos.
   \   Un descriptor de seguridad no est� en el formato correcto (absoluto o autorrelativo).
 �   Esta acci�n est� restringida s�lo para el uso de procesos de inicio de
sesi�n. El proceso no se ha registrado como un proceso de inicio de sesi�n.
   `   No se puede iniciar un nuevo inicio de sesi�n con un identificador que ya est� en uso.
    @   Se desconoce el paquete de autenticaci�n especificado.
    L   La sesi�n no est� en un estado coherente con la
operaci�n requerida.
 D   Ya se est� utilizando el identificador de inicio de sesi�n.
   `   Una petici�n de inicio de sesi�n conten�a un valor de tipo de inicio de sesi�n no v�lido.
 t   No se puede suplantar con una canalizaci�n con nombre hasta que
no se hayan le�do datos de la canalizaci�n.
  `   El estado de transacci�n de un sub�rbol del Registro no es
compatible con la operaci�n.
  P   Se ha detectado una corrupci�n en la base de datos de seguridad interna.
  D   No se puede ejecutar esta operaci�n en las cuentas internas.
  L   No se puede ejecutar esta operaci�n en este grupo especial integrado.
 P   No se puede ejecutar esta operaci�n en este usuario especial integrado.
   `   No se puede quitar al usuario de un grupo porque es
el grupo primario de este usuario.
   8   El token ya se est� usando como token primario.
   0   El grupo local especificado no existe.
    H   El nombre de cuenta especificado no es miembro del grupo local.
   H   El nombre de cuenta especificado ya es miembro del grupo local.
   0   El grupo local especificado ya existe.
    p   Error de inicio de sesi�n: No se ha concedido al usuario este tipo de inicio
de sesi�n en este equipo.
   `   Se ha excedido el m�ximo n�mero de secretos que se pueden guardar en un
�nico sistema.
   H   La longitud de un secreto excede la longitud m�xima permitida.
    X   Existe una inconsistencia en la base de datos de la autoridad de seguridad local.
 �   Durante el inicio de sesi�n, el contexto de seguridad del usuario ha
acumulado demasiados identificadores de seguridad.
  p   Error de inicio de sesi�n:
No se ha concedido al usuario este tipo de inicio
de sesi�n en este equipo.
   `   Se necesita una contrase�a con cifrado cruzado para cambiar una contrase�a de usuario.
    X   No se pudo agregar un nuevo miembro al grupo local porque el miembro
no existe.
  t   No se pudo agregar el nuevo miembro a un grupo local porque el miembro no tiene
el tipo de cuenta correcto.
  D   Se han especificado demasiados identificadores de seguridad.
  `   Para cambiar esta contrase�a de usuario es necesaria una contrase�a con cifrado cruzado.
  \   Indica que una lista de control de acceso contiene componentes que no pueden heredar.
 @   El archivo o directorio est� da�ado y no se puede leer.
   @   La estructura del disco est� da�ada y no se puede leer.
   X   No existe una clave de sesi�n de usuario para el inicio de sesi�n especificado.
   �   Se est� teniendo acceso a un software con licencia para una cantidad determinada de conexiones.
En este momento no se pueden establecer m�s conexiones con este software
porque ya hay tantas conexiones como el servicio puede aceptar.
    4   El identificador de la ventana no es v�lido.
  0   El identificador del men� no es v�lido.
   0   El identificador del cursor no es v�lido.
 @   El identificador de la tabla de aceleraci�n no es v�lido.
 0   El identificador del enlace no es v�lido.
 \   El identificador de una estructura de posici�n de ventanas m�ltiples no es v�lido.
    @   No se puede crear una ventana secundaria de primer nivel.
 (   Clase de ventana no encontrada.
   8   Ventana no v�lida; pertenece a otro subproceso.
   4   Ya se ha registrado la tecla de aceleraci�n.
     Esta clase ya existe.
    Esta clase no existe.
 0   La clase todav�a tiene ventanas abiertas.
     El �ndice no es v�lido.
   0   El identificador de icono no es v�lido.
   D   Se est�n utilizando palabras privadas de ventana de di�logo.
  <   No se encontr� el identificador de cuadro de lista.
   (   No se han encontrado comodines.
   8   El subproceso no tiene abierto un Portapapeles.
   4   La tecla de aceleraci�n no est� registrada.
   8   Esta ventana no es una ventana de di�logo v�lida.
 0   Identificador de control no encontrado.
   \   El mensaje no es v�lido porque el cuadro combinado no tiene un control de edici�n.
    ,   La ventana no es un cuadro combinado.
 (   La altura debe ser menor de 256.
  H   El identificador del contexto de dispositivo (DC) no es v�lido.
   8   El tipo de procedimiento de enlace no es v�lido.
  0   El procedimiento de enlace no es v�lido.
  P   No se puede establecer un enlace no local sin un identificador de m�dulo.
 P   Este procedimiento de enlace s�lo se puede establecer de forma global.
    <   El procedimiento de enlace diario ya se ha instalado.
 8   El procedimiento de enlace no se ha instalado.
    D   Mensaje no v�lido para cuadro de lista de selecci�n �nica.
    <   LB_SETCOUNT enviado a un cuadro de lista din�mico.
    4   El cuadro de lista no admite tabulaciones.
    D   No se puede destruir un objeto creado por otro subproceso.
    8   Las ventanas secundarias no pueden tener men�s.
   0   La ventana no tiene un men� de sistema.
   4   El estilo del cuadro de mensaje no es v�lido.
 @   El par�metro (SPI_*) para todo el sistema no es v�lido.
   (   La pantalla ya est� bloqueada.
    �   Todos los identificadores de ventana en una estructura de posici�n de
ventanas m�ltiples deben tener el mismo predecesor.
    0   La ventana no es una ventana secundaria.
      Comando GW_* no v�lido.
   4   El identificador del subproceso no es v�lido.
 x   No se puede procesar un mensaje de una ventana que no sea una ventana
de interfaz de m�ltiples documentos (MDI).
 (   El men� emergente ya est� activo.
 4   La ventana no tiene barras de desplazamiento.
 T   El intervalo de las barras de desplazamiento no puede ser mayor que 0x7FFF.
   H   No se puede mostrar o quitar la ventana de la forma especificada.
 X   No hay recursos suficientes en el sistema para completar el servicio solicitado.
  X   No hay recursos suficientes en el sistema para completar el servicio solicitado.
  X   No hay recursos suficientes en el sistema para completar el servicio solicitado.
  H   No hay cuota suficiente para completar el servicio solicitado.
    H   No hay cuota suficiente para completar el servicio solicitado.
    T   El archivo de paginaci�n es demasiado peque�o para completar la operaci�n.
    ,   No se encontr� un elemento de men�.
   <   Identificador de distribuci�n de teclado no v�lido.
       Tipo "hook" no permitido.
 H   Esta operaci�n requiere una estaci�n de ventanas interactivas.
    <   Operaci�n devuelta, el tiempo de espera ha caducado.
  ,   Identificador de monitor no v�lido.
   8   El archivo de registro de sucesos est� da�ado.
    `   No se ha podido abrir el registro de sucesos por lo que no se ha iniciado el servicio.
    4   El archivo de registro de sucesos est� lleno.
 T   El archivo de registro de sucesos ha cambiado entre operaciones de lectura.
   ,   El enlace de cadenas no es v�lido.
    <   El identificador de enlace no es del tipo adecuado.
   0   El identificador de enlace no es v�lido.
  <   La secuencia de protocolo de RPC no est� permitida.
   8   La secuencia de protocolo de RPC no es v�lida.
    H   El identificador �nico universal (UUID) de cadena no es v�lido.
   ,   El formato del extremo no es v�lido.
  ,   La direcci�n de la red no es v�lida.
  ,   No se ha encontrado ning�n extremo.
   8   El valor del intervalo de espera no es v�lido.
    P   No se ha encontrado el identificador �nico universal (UUID) de objeto.
    P   Ya se ha registrado el identificador �nico universal (UUID) de objeto.
    L   Ya se ha registrado el identificador �nico universal (UUID) de tipo.
  ,   El servidor de RPC ya est� en l�nea.
  4   No se han registrado secuencias de protocolo.
 ,   El servidor de RPC no est� en l�nea.
  0   No se conoce el tipo de administrador.
        No se conoce la interfaz.
    No hay enlaces.
   (   No hay secuencias de protocolo.
   $   No se puede crear el extremo.
 L   No se dispone de recursos suficientes para terminar esta operaci�n.
   0   El servidor de RPC no est� disponible.
    P   El servidor de RPC est� demasiado ocupado para terminar esta operaci�n.
   0   Las opciones de la red no son v�lidas.
    L   No hay llamadas a procedimientos remotos activas en este subproceso.
  4   Error en la llamada a procedimiento remoto.
   H   Error en la llamada a procedimiento remoto y no se ha ejecutado.
  D   Error del protocolo de llamada a procedimiento remoto (RPC).
  L   La sintaxis de transferencia no es compatible con el servidor de RPC.
 L   El tipo de identificador �nico universal (UUID) no est� permitido.
        La etiqueta no es v�lida.
 0   Los l�mites de la tabla no son v�lidos.
   0   El enlace no contiene nombre de entrada.
  ,   La sintaxis del nombre no es v�lida.
  0   La sintaxis del nombre no es compatible.
  l   No hay ninguna direcci�n de red disponible para construir un
identificador �nico universal (UUID).
   $   El extremo es un duplicado.
   0   No se conoce el tipo de autenticaci�n.
    <   El n�mero m�ximo de llamadas es demasiado peque�o.
    $   La cadena es demasiado larga.
 <   No se ha encontrado la secuencia de protocolo de RPC.
 H   El n�mero de procedimiento est� fuera del intervalo permitido.
    @   El enlace no tiene ninguna informaci�n de autenticaci�n.
  4   No se conoce el servicio de autenticaci�n.
    0   No se conoce el nivel de autenticaci�n.
   0   El contexto de seguridad no es v�lido.
    4   No se conoce el servicio de autenticaci�n.
        La entrada no es v�lida.
  @   El extremo de servidor no puede ejecutar la operaci�n.
    H   No hay m�s extremos disponibles desde el asignador de extremos.
   (   No se han exportado interfaces.
   0   El nombre de entrada no est� completo.
    ,   La opci�n de versi�n no es v�lida.
       No hay m�s miembros.
      No hay nada que exportar.
     Interfaz no encontrada.
      La entrada ya existe.
     Entrada no encontrada.
    4   El nombre del servicio no est� disponible.
    8   La familia de direcciones de red no es v�lida.
    (   Esta operaci�n no es compatible.
  L   No hay contexto de seguridad disponible para permitir suplantaciones.
 D   Error interno en una llamada a procedimiento remoto (RPC).
    @   El servidor de RPC ha intentado una divisi�n por cero.
    0   Error de direcci�n en el servidor de RPC.
 `   Una operaci�n de coma flotante en el servidor de RPC ha causado una divisi�n por cero.
    @   Subdesbordamiento de coma flotante en el servidor de RPC.
 @   Desbordamiento de coma flotante en el servidor de RPC.
    l   Se ha agotado la lista de servidores RPC disponibles para el
enlace de identificadores autom�ticos.
  P   No se puede abrir el archivo con la tabla de traducci�n de caracteres.
    X   El archivo con la tabla de traducci�n de caracteres tiene menos
de 512 bytes.
    t   Un identificador nulo de contexto se pas� del cliente al host durante
una llamada a procedimiento remoto.
    \   El identificador de contexto ha cambiado durante una llamada de procedimiento remoto.
 `   Los identificadores de enlace pasados a una llamada a procedimiento remoto no concuerdan.
 `   La etiqueta no puede tener acceso al identificador de la llamada de procedimiento remoto.
 T   Se pas� un puntero de referencia nulo a la rutina con c�digo no ejecutable.
   D   El valor de enumeraci�n est� fuera del intervalo permitido.
   0   El n�mero de bytes es demasiado peque�o.
  0   La etiqueta ha recibido datos err�neos.
   @   El b�fer del usuario no es v�lido para esta operaci�n.
    H   El disco no es reconocido. Puede que no se le haya dado formato.
  @   La estaci�n de trabajo no tiene un secreto de confianza.
  �   La base de datos SAM en Windows NT Server no tiene una cuenta de
PC para la relaci�n de confianza de esta estaci�n de trabajo.
   h   Error en la relaci�n de confianza entre el dominio principal
y el dominio en el que se conf�a.
   `   Error en la relaci�n de confianza entre la estaci�n de trabajo
y el dominio principal.
   0   Error en el inicio de sesi�n de la red.
   P   Ya hay una llamada a procedimiento remoto en curso para este subproceso.
  l   Se ha intentado iniciar la sesi�n pero el servicio de inicio de sesi�n de la red no se ha iniciado.
   ,   La cuenta del usuario ha caducado.
    <   Se est� usando el redirector y no se puede descargar.
 8   El controlador de impresora ya est� instalado.
    ,   No se conoce el puerto especificado.
  0   No se conoce el controlador de impresora.
 0   No se conoce el procesador de impresora.
  8   El archivo separador especificado no es v�lido.
   0   La prioridad especificada no es v�lida.
   0   El nombre de la impresora no es v�lido.
       Esta impresora ya existe.
 ,   El comando de impresora no es v�lido.
 4   El tipo de dato especificado no es v�lido.
    ,   El entorno especificado no es v�lido.
    No hay m�s enlaces.
   �   La cuenta usada es una cuenta de confianza entre dominios. Use su cuenta normal o la cuenta local para tener acceso a este servidor.
  x   La cuenta usada es una cuenta de PC. Use su cuenta normal o la cuenta remota para tener acceso a este servidor.
   �   La cuenta utilizada es una cuenta de confianza de servidor. Utilice su cuenta de usuario global o local para tener acceso a este servidor.
    �   El nombre o el identificador de seguridad (SID) del dominio no es coherente
con la informaci�n de confianza para ese dominio.
    @   Se est� utilizando el servidor y no se puede descargar.
   <   El archivo de imagen no conten�a secci�n de recursos.
 X   El tipo de recurso especificado no se puede encontrar en el archivo de imagen.
    X   El nombre de recurso especificado no se puede encontrar en el archivo de imagen.
  d   No se ha podido encontrar el recurso de identificador de lenguaje en el archivo de imagen.
    D   No se dispone de cuota suficiente para procesar este comando.
 (   No se han registrado interfaces.
  <   La llamada a procedimiento remoto se ha cancelado.
    L   El identificador de enlace no contiene toda la informaci�n requerida.
 L   Error de comunicaciones durante una llamada a procedimiento remoto.
   <   El nivel de autenticaci�n requerido no es compatible.
 ,   No se ha registrado nombre principal.
 L   El error especificado no es un error de c�digo de Windows RPC v�lido.
 @   Se ha asignado un UUID que es v�lido s�lo en este equipo.
 0   Error espec�fico de paquete de seguridad.
 (   El subproceso no se ha cancelado.
 P   Operaci�n no v�lida en el identificador de codificaci�n/descodificaci�n.
  <   Versi�n incompatible del paquete de serializaci�n.
    ,   Versi�n incompatible del stub RPC.
    @   El objeto de canalizaci�n RPC no es v�lido o est� da�ado.
 P   Se ha intentado una operaci�n no v�lida en un objeto de canalizaci�n RPC.
 0   Versi�n de canalizaci�n RPC no admitida.
  0   No se ha encontrado el miembro del grupo.
 T   No se pudo crear la entrada de la base de datos del asignador de extremos.
    L   El identificador �nico universal (UUID) de objeto es el UUID nulo.
    ,   La hora especificada no es v�lida.
    8   El nombre del formato especificado no es v�lido.
  <   El tama�o de formulario especificado no es v�lido.
    H   El identificador especificado de la impresora ya se ha atendido
   4   Se ha eliminado la impresora especificada.
    0   El estado de la impresora no es v�lido.
   X   El usuario debe cambiar su contrase�a antes de iniciar la sesi�n por primera vez.
 P   No se ha podido encontrar el controlador de dominio para este dominio.
    P   La cuenta a que se hace referencia est� bloqueada y no se puede utilizar.
 8   El exportador de objetos especificado no existe.
  4   No se ha encontrado el objeto especificado.
   L   No se ha hallado el conjunto de resoluci�n de objetos especificado.
   H   Algunos datos permanecen para ser enviados en el b�fer requerido.
 L   Identificador llamada a procedimiento remoto asincr�nico no v�lido.
   P   Identificador de llamada RPC asincr�nica no v�lido para esta operaci�n.
   8   Ya se ha cerrado el objeto de canalizaci�n RPC.
   T   La llamada RPC finaliz� antes de que se procesaran todas las canalizaciones.
  <   No hay m�s datos disponibles de la canalizaci�n RPC.
  (   El formato de p�xel no es v�lido.
 0   El controlador especificado no es v�lido.
 T   El estilo de ventana o el atributo de clase no es v�lido para esta operaci�n.
 @   La operaci�n de metarchivo solicitada no es compatible.
   D   La operaci�n de transformaci�n solicitada no es compatible.
   D   La operaci�n de copia de datos solicitada no es compatible.
   �   Se estableci� la conexi�n de red correctamente pero se ha solicitado una
contrase�a distinta de la especificada inicialmente.
    ,   El nombre de usuario no es v�lido.
    (   No existe esta conexi�n de red.
   L   Esta conexi�n de red tiene archivos abiertos o peticiones pendientes.
 ,   Todav�a existen conexiones activas.
   T   Hay un proceso activo utilizando el dispositivo y no se puede desconectar.
    <   El monitor de impresi�n especificado es desconocido.
  @   El controlador de impresora especificado ya est� en uso.
  <   No se ha encontrado el archivo de cola de impresi�n.
  4   No se realiz� una llamada a StartDocPrinter.
  0   No se ha generado una llamada a AddJob.
   D   El procesador de impresi�n especificado ya ha sido instalado.
 D   El monitor de impresi�n especificado ya ha sido instalado.
    P   El monitor de impresi�n especificado no tiene las funciones requeridas.
   T   El monitor de impresi�n especificado est� siendo utilizado en este momento.
   D   La operaci�n no est� permitida cuando hay trabajos en espera.
 h   La operaci�n ha tenido �xito. Los cambios no ser�n efectivos hasta que el sistema no se reinicie.
 t   La operaci�n se ha realizado con �xito. Los cambios no se har�n efectivos hasta que el servicio se reinicie.
  <   WINS encontr� un error mientras procesaba el comando.
 ,   WINS local no puede ser eliminado.
    ,   Error al importar desde el archivo.
   `   Error en la copia de seguridad. �Se ha realizado antes una copia de seguridad completa?
   d   Error en la copia de seguridad. Compruebe el directorio en el que guarda la base de datos.
    8   El nombre no existe en la base de datos de WINS.
  H   No se permite la replicaci�n con un duplicador no configurado.
    �   El cliente DHCP ha obtenido una direcci�n DHCP en uso en la red. La interfaz local se deshabilitar� hasta que el cliente DHCP pueda conseguir una nueva direcci�n.
    `   El recurso cl�ster no se puede mover a otro grupo porque hay recursos dependientes de �l.
 <   No se encuentra la dependencia del recurso cl�ster.
   t   El recurso cl�ster no se puede hacer depender del recurso especificado por que ya existe dicha dependencia.
   ,   El recurso cl�ster no est� conectado.
 D   Un nodo de cl�steres no est� disponible para esta operaci�n.
  0   El recurso cl�ster no est� disponible.
    0   No se pudo encontrar el recurso cl�ster.
  $   El cl�ster se est� cerrando.
  X   No se puede expulsar desde el cl�ster un nodo de cl�ster cuando est� conectado.
      El objeto ya existe.
  (   El objeto ya est� en la lista.
    <   El grupo de cl�steres no admite nuevas peticiones.
    0   No se encuentra el grupo de cl�steres.
    \   No se puede completar la operaci�n porque el grupo de cl�steres no est� conectado.
    <   El nodo de cl�steres no es el poseedor del recurso.
   <   El nodo de cl�steres no es el poseedor del recurso.
   T   El recurso cl�ster no se pudo crear en el monitor de recursos especificado.
   L   No se pudo conectar el recurso cl�ster por el monitor de recursos.
    T   No se pudo completar la operaci�n por que el recurso cl�ster est� conectado.
  P   El recurso de qu�rum no se ha eliminado porque es el recurso de qu�rum.
   p   El cl�ster no pudo convertir el recurso en un recurso de qu�rum porque no puede ser un recurso de qu�rum.
 4   El software de cl�steres se est� cerrando.
    `   El grupo o recurso no est� en el estado apropiado para realizar la operaci�n solicitada.
  �   Se han guardado las propiedades pero no todos los cambios surtir�n efecto
hasta la pr�xima vez que el recurso se conecte.
    �   El cl�ster no pudo convertir el recurso en un recurso de qu�rum porque no pertenece a una clase de almacenamiento compartida.
 P   No se pudo eliminar el recurso cl�ster porque es un recurso principal.
    8   El recurso de qu�rum no se ha podido conectar.
    L   No se ha podido crear o montar correctamente el registro de quorum.
   ,   El registro de cl�steres est� da�ado.
 h   No se ha podido registrar los datos en el registro de cl�ster porque excede del tama�o m�ximo.
    8   El registro de cl�ster excede su tama�o m�ximo.
   H   No se encontr� registro de control en el registro del cl�ster.
    @   No hay suficiente espacio en el disco para el registro.
   L   No se dispone de la lista de servidores para este grupo de trabajo
    D   Un archivo fue convertido al formato de archivo compuesto.
    `   La operaci�n de almacenamiento deber�a bloquearse hasta que haya m�s datos disponibles.
   P   La operaci�n de almacenamiento deber�a de reintentarse inmediatamente.
    P   El evento sink notificado no influir� en la operaci�n de almacenamiento.
  L   M�ltiples aperturas evitan la consolidaci�n. (Registro confirmado).
   X   Error en la consolidaci�n del archivo de almacenamiento. (Registro finalizado).
   `   La consolidaci�n del archivo de almacenamiento no es apropiada. (Registro finalizado).
    T   Use la base de datos del Registro para suministrar la informaci�n requerida
       Realizado, pero est�tico
  ,   Formato de Portapapeles de Macintosh
  (   Se realiz� el arrastre con �xito
  ,   Se cancel� la operaci�n de arrastre
   (   Utilice el cursor predeterminado
  ,   Los datos tienen el mismo FORMATETC
       La vista ya est� detenida
 $   FORMATETC no es compatible
       La misma cach�
    ,   Algunas cach�s no est�n actualizadas
  ,   Verbo no v�lido para el objeto OLE
    D   El n�mero de verb es v�lido pero no puede ser realizado ahora
 <   Se ha pasado un identificador de ventana no v�lido
    T   El mensaje es demasiado largo; se ha tenido que truncar antes de ser mostrado
 4   No se puede convertir OLESTREAM a IStorage
    (   Se ha reducido moniker a s� mismo
 ,   El prefijo com�n est� en este moniker
 8   El prefijo com�n est� en el moniker de entrada
    0   El prefijo com�n est� en ambos monikers
   H   Moniker ya est� registrado en la tabla de objetos de ejecuci�n
    @   No todas las interfaces requeridas estaban disponibles
       No implementado
       No hay suficiente memoria
 ,   Uno o m�s argumentos no son v�lidos
       Interfaz no compatible
       Puntero no v�lido
     Identificador no v�lido
      Operaci�n anulada
    Error no especificado
 (   Error general de acceso denegado
  T   El dato necesario para completar esta operaci�n no est� disponible todav�a.
       Interfaz no compatible
       Puntero no v�lido
    Operaci�n anulada
    Error no especificado
 4   Error de almacenamiento del subproceso local
  <   Obtener el error del asignador de memoria compartido
  0   Obtener el error del asignador de memoria
 4   No se puede inicializar la cach� de la clase
  4   No se puede inicializar los servicios de RPC
  \   No se puede establecer el control del canal de almacenamiento local del subproceso
    X   No se puede asignar el control del canal de almacenamiento local del subproceso
   L   El asignador de memoria suministrado por el usuario no es aceptable
   ,   El mutex de servicio de OLE ya existe
 <   La asignaci�n de archivo del servicio OLE ya existe
   H   No se puede asignar una vista del archivo para el servicio de OLE
 4   Error al intentar iniciar el servicio de OLE
  \   Hubo un intento de llamar a CoInitialize una segunda vez durante un subproceso �nico.
 D   Se hizo necesaria una activaci�n remota pero no se permiti�
   p   Se hizo necesaria una activaci�n remota pero no fue v�lido el nombre del servidor que se  proporcionado
   t   La clase se configura como un identificador de seguridad distinto del correspondiente al autor de la llamada
  P   El uso de los servicios Ole1 que requiere ventanas DDE no est� activado
   t   Una especificaci�n RunAs deber� ser <nombre de dominio>\<nombre de usuario> o simplemente <nombre de usuario>
 h   El proceso del servidor no ha podido iniciarse. El nombre de v�a de acceso puede ser incorrecto.
  �   El proceso del servidor no ha podido iniciarse como una identidad configurada. El nombre de v�a de acceso puede ser incorrecto o no estar disponible.
 �   El proceso del servidor no ha podido iniciarse. La identidad configurada no es correcta. Compruebe el nombre del usuario y la contrase�a.
 8   No se permite al cliente iniciar este servidor.
   L   No se ha podido iniciar el servicio que este servidor proporciona.
    P   Este equipo no ha podido comunicarse con el que proporciona el servidor.
  D   El servidor no ha respondido despu�s de que ha sido iniciado.
 X   La informaci�n de registro para este servidor es incoherente o est� incompleta.
   X   La informaci�n de registro para esta interfaz es incoherente o est� incompleta.
   <   La operaci�n que se ha intentado no est� permitida.
   4   La llamada fue rechazada por el destinatario.
 <   La llamada fue cancelada por el filtro de mensaje.
    t   El autor de la llamada est� lanzando una llamada SendMessage entre tareas y
no puede llamar v�a PostMessage.
 �   El autor de la llamada est� ejecutando una llamada asincr�nica no pudiendo
realizar una llamada en nombre de esta llamada.
   8   No se puede llamar dentro del filtro de mensaje.
  �   La conexi�n ha finalizado o est� en un estado fantasma
y no puede ser usado nunca m�s. Otras conexiones
son v�lidas todav�a.
    �   El destinatario (servidor [no una aplicaci�n de servidor]) no est� disponible
ni presente; las conexiones ya no son v�lidas. La llamada puede
haberse ejecutado.
    x   El autor de la llamada (cliente) desapareci� mientras el destinatario (servidor) estaba
procesando una llamada.
  T   El paquete de datos con los datos de par�metros en el b�fer es incorrecto.
    |   La llamada no fue transmitida apropiadamente; la cola de mensaje
estaba llena y no se vaci� despu�s de la retenci�n.
 d   El cliente (autor de la llamada) no puede copiar los datos de par�metros - memoria baja, etc.
 d   El cliente (autor de la llamada) no puede recoger los datos de vuelta - memoria baja, etc.
    \   El servidor (destinatario) no puede copiar los datos de vuelta - memoria baja, etc.
   `   El servidor (destinatario) no puede recoger los datos de par�metros - memoria baja, etc.
  \   Los datos recibidos no son v�lidos; puede que sea el servidor o los datos de cliente.
 L   Un par�metro determinado no es v�lido y no puede ser (des)ordenado.
   X   No hay una segunda llamada de salida en el mismo canal en la conversaci�n DDE.
    �   El destinatario (servidor [no una aplicaci�n de servidor]) no est� disponible
ni presente; las conexiones no son v�lidas. La llamada no se ejecut�.
  (   Error en la llamada de sistema.
   L   No se pudo asignar alg�n recurso requerido (memoria, eventos, ...)
    T   Se han intentado hacer llamadas en m�s de un subproceso en el modo sencillo.
  H   La interfaz requerida no est� registrada en el objeto servidor.
   d   RPC no pudo llamar al servidor o no pudo devolver los resultados de la llamada al servidor.
   (   El servidor lanz� una excepci�n.
  L   No se puede cambiar el modo de subproceso despu�s de establecerlo.
    4   El m�todo llamado no existe en el servidor.
   <   El objeto invocado ha desconectado de sus clientes.
   T   El objeto invocado eligi� no procesar ahora la llamada. Int�ntelo m�s tarde.
  D   El filtro de mensaje indic� que la aplicaci�n est� ocupada.
   0   El filtro de mensaje rechaz� la llamada.
  L   Se llam� a una interfaz de control de llamadas con datos no v�lidos.
  �   No se puede hacer una llamada de salida desde la aplicaci�n que est� ejecutando una llamada de sincronizaci�n de entrada.
 `   La aplicaci�n llam� a una interfaz que estaba en el b�fer para un diferente subproceso.
   @   CoInitialize no ha sido llamado en el actual subproceso.
  H   La versi�n de OLE en las m�quinas cliente y servidor no coincide.
 @   OLE ha recibido un paquete con una cabecera no v�lida.
    @   OLE ha recibido un paquete con una extensi�n no v�lida.
   4   El objeto o interfaz requeridos no existen.
   (   El objeto requerido no existe.
    D   OLE ha enviado una petici�n y est� esperando una respuesta.
   D   OLE est� esperando antes de volver a intentar una petici�n.
   d   No se puede tener acceso al contexto de llamada despu�s de que se ha completado la llamada.
   @   La suplantaci�n no es compatible con llamadas no seguras.
 �   Se debe inicializar la seguridad antes de que las interfaces sean cifradas o
descifradas. Una vez inicializada, no se puede cambiar.
 �   No se ha instalado ning�n paquete de seguridad en esta m�quina, o el usuario no ha iniciado la sesi�n
o no hay paquetes de seguridad compatibles entre el cliente y el servidor.
    Acceso denegado.
  @   Las llamadas remotas no est�n permitidas en este proceso.
 p   El paquete de datos de la interfaz cifrado (marshaled) (OBJREF) tiene un formato no v�lido o desconocido.
    Error interno.
       Interfaz desconocida.
 (   No se ha encontrado el miembro.
   (   No se ha encontrado el par�metro.
    Tipo incorrecto.
     Nombre desconocido.
       Argumentos sin nombrar.
   $   Tipo de variable incorrecto.
  $   Ha ocurrido una excepci�n.
    $   Fuera del intervalo actual.
       El �ndice no es v�lido.
      Lenguaje desconocido.
 $   La memoria est� bloqueada.
    (   N�mero de par�metros no v�lido.
       Par�metro no opcional.
        Destinatario no v�lido.
   (   No compatible con una selecci�n.
      B�fer demasiado peque�o.
  8   Formato antiguo o tipo de biblioteca no v�lida.
   8   Formato antiguo o tipo de biblioteca no v�lida.
   0   Error al tener acceso al Registro OLE.
        Biblioteca no registrada.
 (   Enlazado a un tipo desconocido.
   (   Nombre certificado no permitido.
  H   Referencia de avance no v�lida o referencia a tipo no compilado.
     Tipo incorrecto.
      Elemento no encontrado.
      Nombre ambiguo.
   ,   El nombre ya existe en la biblioteca.
    LCID desconocido.
 @   Funci�n no definida en la biblioteca DLL especificada.
    4   Tipo de m�dulo incorrecto para la operaci�n.
  ,   El tama�o no puede exceder de 64k.
    4   Id. duplicado en la jerarqu�a de la herencia.
 D   Fondo de herencia incorrecto en el hmember est�ndar de OLE.
      Tipo incorrecto.
  (   N�mero de argumentos no v�lido.
      Error de E/S.
 0   Error al crear el archivo temporal �nico.
 4   Error al cargar la biblioteca de tipo/DLL.
    ,   Funciones de propiedad incoherentes.
  4   Dependencia circular entre tipos y m�dulos.
   4   No se puede ejecutar la operaci�n solicitada.
     No se puede encontrar %1.
 ,   No se puede encontrar la ruta  %1.
    <   No hay suficientes recursos para abrir otro archivo.
     Acceso denegado.
  <   Se ha intentado una operaci�n en un objeto no v�lido.
 H   No hay suficiente memoria disponible para completar la operaci�n.
 $   Error de puntero no v�lido.
   ,   No hay m�s entradas para devolver.
    0   El disco est� protegido contra escritura.
 0   Error durante la operaci�n de b�squeda.
   <   Error de disco durante una operaci�n de escritura.
    8   Error de disco durante una operaci�n de lectura.
  (   Infracci�n de recurso compartido.
     Infracci�n de bloqueo.
       %1 ya existe.
 $   Error de par�metro no v�lido.
 L   No hay suficiente espacio en el disco para completar la operaci�n.
    P   Escritura ilegal de propiedad no simple para equipo de propiedad simple.
  8   Una llamada a API ha salido de manera an�mala.
    8   El archivo %1 no es un archivo compuesto v�lido.
  $   El nombre %1 no es v�lido.
       Error inesperado.
 ,   Esta funci�n no est� implementada.
    $   Error de indicador no v�lido.
 8   Se ha intentado usar un objeto que est� ocupado.
  D   El almacenamiento ha sido cambiado desde el �ltimo registro.
  8   Se ha intentado usar un objeto que ya no existe.
     No se puede guardar.
  \   El archivo compuesto %1 fue producido con una versi�n de almacenamiento incompatible.
 \   El archivo compuesto %1 fue producido con una versi�n de almacenamiento m�s reciente.
 <   Se requiere share.exe o un equivalente para operar.
   H   Operaci�n no v�lida en un almacenamiento no basado en archivo.
    H   Operaci�n no v�lida en un objeto con ordenamientos existentes.
        Se ha da�ado el docfile.
  <   OLE32.DLL se ha cargado con una direcci�n err�nea.
    \   La carga del archivo ha sido anulada de manera an�mala. El archivo est� incompleto.
   0   La carga del archivo ha sido terminada.
   $   Estructura OLEVERB no v�lida
  (   Indicadores de ayuda no v�lidos
   H   Ya no se puede enumerar porque se han perdido los datos asociados
 0   Esta implementaci�n no admite sugerencias
 <   No hay conexi�n para este identificador de conexi�n
   8   Necesita ejecutar el objeto para esta operaci�n
   (   No hay cach� con la que operar
        Objeto no inicializado
    8   La clase origen del objeto vinculado ha cambiado
  0   No se puede obtener el moniker del objeto
 (   No se puede enlazar con el origen
 <   El objeto es est�tico; la operaci�n no est� permitida
 8   El usuario cancel� el cuadro de di�logo Guardar
      Rect�ngulo no v�lido
  P   compobj.dll es demasiado antiguo para el archivo ole2.dll inicializado
    ,   Identificador de ventana no v�lido
    D   El objeto no se encuentra en ninguno de los estados activos
   (   No se puede convertir el objeto
   \   No se puede ejecutar esta operaci�n, el objeto no tiene todav�a un almacenamiento

   (   Estructura FORMATETC no v�lida
    ,   Estructura DVTARGETDEVICE no v�lida
   (   Estructura STDGMEDIUM no v�lida
   $   Estructura STATDATA no v�lida
    lindex no v�lido
     tymed no v�lido
   (   formato de Portapapeles no v�lido
     aspecto(s) no v�lido(s)
   H   El par�metro tdSize de la estructura DVTARGETDEVICE no es v�lido
  @   El objeto no es compatible con la interfaz IViewObject
    H   Intentando revocar un destino eliminado que no ha sido registrado
 H   Esta ventana ya hab�a sido registrada como un origen eliminado
    ,   Identificador de ventana no v�lido
    X   La clase no es compatible con agregaciones (o el objeto de la clase es remoto)
    @   ClassFactory no puede suministrar a la clase requerida
    0   No tiene licencia para utilizar la clase
      Error al dibujar la vista
 8   No se ha podido leer la clave desde el Registro
   4   No se ha podido escribir la clave al Registro
 8   No se ha podido encontrar la clave en el Registro
 (   valor no v�lido para el Registro
     Clase no registrada
       Interfaz no registrada
       CATID no existe
   ,   No se ha encontrado una descripci�n
      Cach� no actualizada
  (   No hay verbos para el objeto OLE
  ,   Verbo no v�lido para el objeto OLE
    $   Deshacer no est� disponible
   8   No hay espacio disponible para las herramientas
   0   Error en el m�todo de obtener OLESTREAM
   0   Error en el m�todo de situar OLESTREAM
    D   Los contenidos de OLESTREAM no est� en el formato correcto
    d   Ha habido un error en la llamada de GDI de Windows cuando se convert�a el mapa de bits a DIB
  D   Los contenidos del IStorage no est�n en el formato correcto
   H   Se han perdido los contenidos de ISorage en las cadenas est�ndar
  h   Ha habido un error en la llamada de GDI de Windows cuando se convert�a el DIB a mapa de bits.

       Error en OpenClipboard
        Error en EmptyClipboard
      Error en SetClipboard
 4   Los datos en el portapapeles no son v�lidos
       Error en CloseClipboard
   4   Moniker necesita ser conectado manualmente
    0   La operaci�n excedi� del l�mite fijado
    $   Moniker necesita ser gen�rico
     Operaci�n no disponible
      Sintaxis no v�lida
    $   No hay objeto para moniker
    ,   Extensi�n incorrecta para el archivo
  (   Error en la operaci�n intermedia
      Moniker no es enlazable
       Moniker no es enlazado
    (   Moniker no puede abrir un archivo
 H   Entrada de usuario requerida para que la operaci�n tenga �xito
    (   La clase Moniker no tiene inversa
 0   Moniker no se refiere al almacenamiento
       No hay un prefijo com�n
   (   Moniker no ha podido ser numerada
 (   No se ha llamado a CoInitialize.
  (   CoInitialize ya ha sido llamado.
  4   No se puede determinar la clase del objeto
        Cadena clase no v�lida
        Cadena interfaz no v�lida
 (   No se ha encontrado la aplicaci�n
 8   La aplicaci�n no se puede ejecutar m�s de una vez
 0   Alg�n error en el programa de aplicaci�n
  <   No se ha encontrado la biblioteca DLL para la clase
   $   Error en la biblioteca DLL
    <   SO incorrecto o versi�n incorrecta para la aplicaci�n
 $   El objeto no est� registrado
  $   El objeto ya est� registrado
  0   El objeto no est� conectado al servidor
   H   La aplicaci�n fue iniciada pero no registr� una f�brica de clase
  $   El objeto ha sido liberado
    ,   No se pudo suplantar al cliente DCOM
  @   No se pudo conseguir contexto de seguridad del servidor
   @   No se pudo abrir el token de acceso del subproceso actual
 L   No se pudo obtener informaci�n del usuario desde un token de acceso
   �   El cliente que llam� a la funci�n "IAccessControl::IsAccessPermitted" no era el elemento de confianza (trustee) que se ha proporcionado al m�todo
 <   No se pudo obtener la manta de seguridad del cliente
  h   No se pudo establecer una lista de control de acceso discrecional en un descriptor de seguridad
   H   La funci�n del sistema AccessCheck ha retornado un valor falso
    D   NetAccessDel o NetAccessAdd han retornado un c�digo de error.
 �   Una de las cadenas de confianza dadas por el usuario no se ajustan a la sintaxis <Dominio>\<Nombre> y no era la cadena "*"
    \   Uno de los identificadores de seguridad proporcionados por el usuario no era v�lido
   T   No se pudo convertir una cadena de confianza de car�cter ancho a multibyte
    �   No se puede encontrar un identificador de seguridad que corresponda a una de las cadenas de confianza proporcionadas por el usuario
   ,   Error en la funci�n LookupAccountSID
  �   No se puede encontrar un nombre de confianza que corresponda a uno de los identificadores de seguridad proporcionados por el usuario
  ,   Error en la funci�n LookupAccountName
 L   No se pudo establecer o restablecer un identificador de serializaci�n
 4   No se pudo obtener el directorio de Windows
   ,   La ruta de acceso es demasiado larga
  $   No se pudo generar un uuid.
   $   No se pudo crear un archivo
   H   No se pudo cerrar un identificador de serializaci�n o de archivos
 d   El n�mero de ACEs en una lista de control de acceso (ACL) excede de los l�mites del sistema
   `   No todos los ACEs DENY_ACCESS est�n ordenados frente a los ACEs GRANT_ACCESS en la cadena
 �   La versi�n del formato de las listas de control de acceso en la cadena no es compatible con esta implementaci�n de lAccessControl
 D   No se pudo abrir el token de acceso del proceso del servidor
  h   No se puede descodificar la lista de control de acceso en la cadena proporcionada por el usuario
  @   El objeto COM de tipo lAccessControl no se ha iniciado
    (   Error general de acceso denegado
      Identificador no v�lido
       No hay suficiente memoria
 ,   Uno o m�s argumentos no son v�lidos
   4   Error al intentar crear un objeto de clase
    8   El servicio de OLE no ha podido enlazar el objeto
 <   Error en la comunicaci�n RPC con el servicio de OLE
   (   Ruta incorrecta para el objeto
    (   Error en la ejecuci�n de servidor
 H   El servidor de OLE no pudo comunicarse con el servidor de objeto
  4   La ruta de Moniker no pudo ser normalizada
    d   El servidor de objeto se va a detener cuando el servicio de OLE se ponga en contacto con �l
   @   Se ha especificado un puntero de bloque ra�z no v�lido
    L   La asignaci�n de la cadena conten�a un puntero de v�nculo no v�lido
   @   El tama�o de la asignaci�n requerida era demasiado grande
    UID incorrecta.
      Hash incorrecto.
     Clave incorrecta.
    Longitud incorrecta.
     Datos incorrectos.
       Firma incorrecta.
 (   Proveedor de versi�n incorrecto.
  ,   Algoritmo especificado no es v�lido.
  0   Indicadores especificados no son v�lidos.
 (   Tipo especificado no es v�lido.
   @   Clave no v�lida para utilizar en el estado especificado.
  @   Hash no v�lido para utilizar en el estado especificado.
      La clave no existe.
   @   No hay suficiente memoria disponible para la operaci�n.
      El objeto ya existe.
     Acceso denegado.
  (   No se ha encontrado el objeto.
    $   Los datos ya est�n cifrados.
  4   Se ha especificado un proveedor no v�lido.
    <   Se ha especificado un tipo de proveedor no v�lido.
    4   La clave p�blica del proveedor no es v�lida.
  (   El conjunto de claves no existe
   (   Tipo de proveedor no definido.
    4   El tipo de proveedor registrado no es v�lido.
 0   El conjunto de claves no est� definido.
   8   El conjunto de claves registrado no es v�lido.
    @   El tipo de proveedor no coincide con el valor registrado.
 0   El archivo de firma digital  est� da�ado.
 D   La DLL proveedora no se ha podido inicializar correctamente.
  0   No se ha encontrado la DLL proveedora.
    8   El par�metro del conjunto de claves no es v�lido.
    Error interno.
       Error de base.
    4   Error en una operaci�n de cifrado de mensaje.
 (   Algoritmo de cifrado desconocido.
 8   Formato incorrecto del identificador de objeto.
   $   Tipo de mensaje no v�lido.
    0   Mensaje no codificado como se esperaba.
   D   Al mensaje le falta el atributo esperado de autenticaci�n.
    $   El valor hash no es correcto.
 ,   El valor del �ndice no es correcto.
   4   Ya se ha descifrado el contenido del mensaje.
 <   Todav�a no se ha descifrado el contenido del mensaje.
 D   El mensaje envuelto no contiene el destinatario especificado.
 (   El tipo de control no es v�lido.
  8   El remitente o el n�mero de serie no son v�lidos.
 ,   No se encuentra el firmante original.
 4   El mensaje no tiene los atributos requeridos.
 L   El mensaje enviado a�n no ha podido devolver los datos solicitados.
   0   Longitud de datos de salida insuficiente.
 ,   Error al codificar o descodificar.
    0   Error leyendo o escribiendo al archivo.
   0   No se ha encontrado el objeto o propiedad
 (   El objeto o propiedad ya existe
   H   No se ha especificado proveedor para el contenedor o el objeto.
   4   El certificado especificado est� autofirmado.
 @   Se ha eliminado el certificado anterior o contexto CRL.
   0   No se encontraron objetos coincidentes.
   \   El tipo de mensaje cifrado que se est� descodificando es diferente de lo esperado.
    @   El certificado no tiene una propiedad de clave privada.
   X   No hay certificados con propiedad de clave privada para permitir su descifrado.
   <   No hay mensaje cifrado o tiene formato incorrecto.
    X   El mensaje firmado no dispone de firmante en el �ndice de firmantes especificado
  L   Cierre definitivo pendiente de liberaciones o cierres adicionales.
    0   Se ha revocado el certificado o su firma
  @   No hay funci�n exportada o DLL para comprobar el revoque.
 T   La funci�n no ha podido comprobar el revoque en el certificado o la firma.
    `   Puesto que el servidor de revoque no estaba disponible, no se pudo comprobar el revoque.
  h   No se ha encontrado el certificado o la firma en la base de datos de servidores de revocaci�n.
    4   La cadena contiene un car�cter no num�rico.
   4   La cadena contiene un car�cter no imprimible.
 T   La cadena contiene un car�cter que no pertenece a la tabla ASCII de 7 bit.
    d   La cadena contiene una clave, oid, valor o delimitador de atributo de nombre X500 no v�lido.
  �   Error: de codificaci�n del certificado OSS

Consulte asn1code.h para ver los errores OSS en tiempo de ejecuci�n. El
desplazamiento es CRYPT_E_OSS_ERROR.
   P   El proveedor de confianza especificado no es conocido en este sistema.
    l   La acci�n de comprobaci�n de confianza no es compatible con el proveedor de confianza especificado.
   |   La forma especificada para el sujeto no es la compatible o la conocida por el proveedor de confianza especificado.
    `   No se ha establecido una relaci�n de confianza con el sujeto para la acci�n especificada.
 H   Error debido a un problema en el proceso de codificaci�n ASN.1.
   L   Error debido a un problema en el proceso de descodificaci�n ASN.1.
    X   Leyendo/ escribiendo extensiones donde los atributos son apropiados y viceversa.
  (   Error de cifrado no especificado.
 0   No se pudo determinar el tama�o del dato.
 H   No se pudo determinar el tama�o del dato de tama�o indefinido.
    <   Este objeto no lee y escribe datos auto-ajustables.
   0   No hay ninguna firma presente en el tema.
 H   No hay ning�n certificado requerido dentro de su per�odo v�lido.
  \   Los per�odos v�lidos de la cadena de certificaci�n no se anidan de forma correcta.
    h   Un certificado que s�lo puede ser usado como entidad final se est� usando como un CA o viceversa.
 x   Se ha producido una infracci�n en la restricci�n del tama�o de la ruta de acceso en la cadena de certificaci�n.
   `   Una extensi�n de tipo desconocido considerada 'cr�tica' est� presente en un certificado.
  \   Se est� usando un certificado para un prop�sito distinto del que le est� permitido.
   L   Un certificado primario del actual no ha emitido el certificado hijo.
 p   Falta un certificado o tiene un valor vac�o para un campo importante, como un tema o nombre del emisor.
   �   Una cadena de certificaci�n procesada correctamente, pero terminada en un certificado ra�z en el que no conf�a el proveedor de confianza.
 h   Una cadena de certificados no se encaden� como deber�a en una cierta aplicaci�n de encadenaci�n.
  $   Error gen�rico de confianza.
  H   El certificado ha sido revocado expresamente por su expedidor.
    