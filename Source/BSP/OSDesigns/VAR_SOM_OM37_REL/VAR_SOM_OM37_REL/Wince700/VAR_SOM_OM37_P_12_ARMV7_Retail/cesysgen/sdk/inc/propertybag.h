//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft
// premium shared source license agreement under which you licensed
// this source code. If you did not accept the terms of the license
// agreement, you are not authorized to use this source code.
// For the terms of the license, please see the license agreement
// signed by you and Microsoft.
// THE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//
//==========================================================================;
//
//  THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
//  KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
//  PURPOSE.
//
//
//--------------------------------------------------------------------------;

#ifndef _PROPERTYBAG_H_
#define _PROPERTYBAG_H_

#if (_MSC_VER >= 1000)
#pragma once
#endif


class CPropertyBagPair; // Internal structure used by CPropertyBag


//=====================================================================
//=====================================================================
// Defines CPropertyBag
//
// A class providing basic IPropertyBag support.
//
// This implementation supplies a basic implementation of the
// IPropertyBag interface. Where the IPropertyBag interface leaves
// behavior choices up to the implementation, this implementation
// uses whatever behavior is the easiest to meet the needs of the
// interface.
//=====================================================================
//=====================================================================
class CPropertyBag : public CUnknown, public IPropertyBag
{
public:
    CPropertyBag(LPUNKNOWN pUnk);
    virtual ~CPropertyBag();

    static CUnknown* CreateInstance(LPUNKNOWN pUnk, HRESULT* pHr);

    DECLARE_IUNKNOWN;
    // override this to say what interfaces we support
    STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void** ppv);

    HRESULT STDMETHODCALLTYPE
    Read(
        LPCOLESTR pszPropName,
        VARIANT* pVar,
        IErrorLog* pErrorLog
        );

    HRESULT STDMETHODCALLTYPE
    Write(
        LPCOLESTR pszPropName,
        VARIANT* pVar
        );

private:
    CPropertyBagPair* Find(LPCOLESTR pszPropName);

    CGenericList<CPropertyBagPair> m_pPairList;
};

#endif // _PROPERTYBAG_H_