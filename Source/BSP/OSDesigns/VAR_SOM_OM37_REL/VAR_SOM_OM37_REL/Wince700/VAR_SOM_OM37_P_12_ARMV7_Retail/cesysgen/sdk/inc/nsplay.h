

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0543 */
/* Compiler settings for nsplay.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __nsplay_h__
#define __nsplay_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __INSOPlay_FWD_DEFINED__
#define __INSOPlay_FWD_DEFINED__
typedef interface INSOPlay INSOPlay;
#endif 	/* __INSOPlay_FWD_DEFINED__ */


#ifndef __INSPlay_FWD_DEFINED__
#define __INSPlay_FWD_DEFINED__
typedef interface INSPlay INSPlay;
#endif 	/* __INSPlay_FWD_DEFINED__ */


#ifndef __INSPlay1_FWD_DEFINED__
#define __INSPlay1_FWD_DEFINED__
typedef interface INSPlay1 INSPlay1;
#endif 	/* __INSPlay1_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


/* interface __MIDL_itf_nsplay_0000_0000 */
/* [local] */ 

typedef /* [public] */ 
enum NSOStateConstants
    {	nsoStopped	= 0,
	nsoPaused	= ( nsoStopped + 1 ) ,
	nsoRunning	= ( nsoPaused + 1 ) 
    } 	NSOStateConstants;

typedef /* [public] */ 
enum NSOControlConstants
    {	nsoNone	= 0,
	nsoSimple	= ( nsoNone + 1 ) ,
	nsoFull	= ( nsoSimple + 1 ) 
    } 	NSOControlConstants;

typedef /* [public] */ 
enum NSODisplayConstants
    {	nsoDefaultSize	= 0,
	nsoHalfSize	= ( nsoDefaultSize + 1 ) ,
	nsoDoubleSize	= ( nsoHalfSize + 1 ) 
    } 	NSODisplayConstants;

typedef /* [public] */ 
enum NSPlayStateConstants
    {	nsStopped	= 0,
	nsPaused	= ( nsStopped + 1 ) ,
	nsPlaying	= ( nsPaused + 1 ) ,
	nsWaiting	= ( nsPlaying + 1 ) 
    } 	NSPlayStateConstants;

typedef /* [public] */ 
enum NSControlConstants
    {	nsNone	= 0,
	nsSimple	= ( nsNone + 1 ) ,
	nsFull	= ( nsSimple + 1 ) 
    } 	NSControlConstants;

typedef /* [public] */ 
enum NSDisplayConstants
    {	nsDefaultSize	= 0,
	nsHalfSize	= ( nsDefaultSize + 1 ) ,
	nsDoubleSize	= ( nsHalfSize + 1 ) 
    } 	NSDisplayConstants;

typedef /* [public] */ 
enum NSProtocolConstants
    {	nsMulticast	= 1,
	nsMulticastPlus	= ( nsMulticast + 1 ) ,
	nsUDP	= ( nsMulticastPlus + 1 ) ,
	nsTCP	= ( nsUDP + 1 ) ,
	nsDistribution	= ( nsTCP + 1 ) ,
	nsHTTP	= ( nsDistribution + 1 ) ,
	nsFile	= ( nsHTTP + 1 ) 
    } 	NSProtocolConstants;

typedef /* [public] */ 
enum NSOpenStateConstants
    {	nsClosed	= 0,
	nsLoadingASX	= ( nsClosed + 1 ) ,
	nsLoadingNSC	= ( nsLoadingASX + 1 ) ,
	nsLocating	= ( nsLoadingNSC + 1 ) ,
	nsConnecting	= ( nsLocating + 1 ) ,
	nsOpening	= ( nsConnecting + 1 ) ,
	nsOpen	= ( nsOpening + 1 ) 
    } 	NSOpenStateConstants;

typedef /* [public] */ 
enum NSWarningTypeConstants
    {	nsNoAudioDevice	= 0,
	nsUnknownStreamFormat	= ( nsNoAudioDevice + 1 ) ,
	nsPlaylistItemFailure	= ( nsUnknownStreamFormat + 1 ) ,
	nsVideoNotRendered	= ( nsPlaylistItemFailure + 1 ) ,
	nsAudioNotRendered	= ( nsVideoNotRendered + 1 ) ,
	nsMissingCodec	= ( nsAudioNotRendered + 1 ) 
    } 	NSWarningTypeConstants;



extern RPC_IF_HANDLE __MIDL_itf_nsplay_0000_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_nsplay_0000_0000_v0_0_s_ifspec;

#ifndef __INSOPlay_INTERFACE_DEFINED__
#define __INSOPlay_INTERFACE_DEFINED__

/* interface INSOPlay */
/* [hidden][dual][uuid][object] */ 


EXTERN_C const IID IID_INSOPlay;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("2179C5D1-EBFF-11cf-B6FD-00AA00B4E220")
    INSOPlay : public IDispatch
    {
    public:
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ImageSourceWidth( 
            /* [retval][out] */ long *pWidth) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ImageSourceHeight( 
            /* [retval][out] */ long *pHeight) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Duration( 
            /* [retval][out] */ double *pDuration) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Author( 
            /* [retval][out] */ BSTR *pbstrAuthor) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Copyright( 
            /* [retval][out] */ BSTR *pbstrCopyright) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Description( 
            /* [retval][out] */ BSTR *pbstrDescription) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Rating( 
            /* [retval][out] */ BSTR *pbstrRating) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Title( 
            /* [retval][out] */ BSTR *pbstrTitle) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SourceLink( 
            /* [retval][out] */ BSTR *pbstrSourceLink) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_MarkerCount( 
            /* [retval][out] */ long *pMarkerCount) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CanScan( 
            /* [retval][out] */ VARIANT_BOOL *pCanScan) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CanSeek( 
            /* [retval][out] */ VARIANT_BOOL *pCanSeek) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CanSeekToMarkers( 
            /* [retval][out] */ VARIANT_BOOL *pCanSeekToMarkers) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CreationDate( 
            /* [retval][out] */ DATE *pCreationDate) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Bandwidth( 
            /* [retval][out] */ long *pBandwidth) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ErrorCorrection( 
            /* [retval][out] */ BSTR *pbstrErrorCorrection) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_AutoStart( 
            /* [retval][out] */ VARIANT_BOOL *pAutoStart) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_AutoStart( 
            /* [in] */ VARIANT_BOOL AutoStart) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_AutoRewind( 
            /* [retval][out] */ VARIANT_BOOL *pAutoRewind) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_AutoRewind( 
            /* [in] */ VARIANT_BOOL AutoRewind) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_InvokeURLs( 
            /* [retval][out] */ VARIANT_BOOL *pInvokeURLs) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_InvokeURLs( 
            /* [in] */ VARIANT_BOOL InvokeURLs) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_EnableContextMenu( 
            /* [retval][out] */ VARIANT_BOOL *pEnableContextMenu) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_EnableContextMenu( 
            /* [in] */ VARIANT_BOOL EnableContextMenu) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_TransparentAtStart( 
            /* [retval][out] */ VARIANT_BOOL *pTransparentAtStart) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_TransparentAtStart( 
            /* [in] */ VARIANT_BOOL TransparentAtStart) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ClickToPlay( 
            /* [retval][out] */ VARIANT_BOOL *pClickToPlay) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_ClickToPlay( 
            /* [in] */ VARIANT_BOOL ClickToPlay) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_FileName( 
            /* [retval][out] */ BSTR *pbstrFileName) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_FileName( 
            /* [in] */ BSTR bstrFileName) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CurrentPosition( 
            /* [retval][out] */ double *pCurrentPosition) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_CurrentPosition( 
            /* [in] */ double CurrentPosition) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Rate( 
            /* [retval][out] */ double *pRate) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_Rate( 
            /* [in] */ double Rate) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CurrentMarker( 
            /* [retval][out] */ long *pCurrentMarker) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_CurrentMarker( 
            /* [in] */ long CurrentMarker) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_PlayCount( 
            /* [retval][out] */ long *pPlayCount) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_PlayCount( 
            /* [in] */ long PlayCount) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CurrentState( 
            /* [retval][out] */ long *pCurrentState) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_DisplaySize( 
            /* [retval][out] */ long *pDisplaySize) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_DisplaySize( 
            /* [in] */ long DisplaySize) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_MainWindow( 
            /* [retval][out] */ long *pMainWindow) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_AllowScan( 
            /* [retval][out] */ VARIANT_BOOL *pAllowScan) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_AllowScan( 
            /* [in] */ VARIANT_BOOL AllowScan) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SendKeyboardEvents( 
            /* [retval][out] */ VARIANT_BOOL *pSendKeyboardEvents) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_SendKeyboardEvents( 
            /* [in] */ VARIANT_BOOL SendKeyboardEvents) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SendMouseClickEvents( 
            /* [retval][out] */ VARIANT_BOOL *pSendMouseClickEvents) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_SendMouseClickEvents( 
            /* [in] */ VARIANT_BOOL SendMouseClickEvents) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SendMouseMoveEvents( 
            /* [retval][out] */ VARIANT_BOOL *pSendMouseMoveEvents) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_SendMouseMoveEvents( 
            /* [in] */ VARIANT_BOOL SendMouseMoveEvents) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SendStateChangeEvents( 
            /* [retval][out] */ VARIANT_BOOL *pSendStateChangeEvents) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_SendStateChangeEvents( 
            /* [in] */ VARIANT_BOOL SendStateChangeEvents) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ReceivedPackets( 
            /* [retval][out] */ long *pReceivedPackets) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_RecoveredPackets( 
            /* [retval][out] */ long *pRecoveredPackets) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_LostPackets( 
            /* [retval][out] */ long *pLostPackets) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ReceptionQuality( 
            /* [retval][out] */ long *pReceptionQuality) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_BufferingCount( 
            /* [retval][out] */ long *pBufferingCount) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CursorType( 
            /* [retval][out] */ long *pCursorType) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_CursorType( 
            /* [in] */ long CursorType) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_AnimationAtStart( 
            /* [retval][out] */ VARIANT_BOOL *pAnimationAtStart) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_AnimationAtStart( 
            /* [in] */ VARIANT_BOOL AnimationAtStart) = 0;
        
        virtual /* [hidden][propget][id] */ HRESULT STDMETHODCALLTYPE get_AnimationOnStop( 
            /* [retval][out] */ VARIANT_BOOL *pAnimationOnStop) = 0;
        
        virtual /* [hidden][propput][id] */ HRESULT STDMETHODCALLTYPE put_AnimationOnStop( 
            /* [in] */ VARIANT_BOOL AnimationOnStop) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Play( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Pause( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Stop( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetMarkerTime( 
            /* [in] */ long MarkerNum,
            /* [retval][out] */ double *pMarkerTime) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetMarkerName( 
            /* [in] */ long MarkerNum,
            /* [retval][out] */ BSTR *pbstrMarkerName) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct INSOPlayVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            INSOPlay * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            INSOPlay * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            INSOPlay * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            INSOPlay * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            INSOPlay * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            INSOPlay * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            INSOPlay * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ImageSourceWidth )( 
            INSOPlay * This,
            /* [retval][out] */ long *pWidth);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ImageSourceHeight )( 
            INSOPlay * This,
            /* [retval][out] */ long *pHeight);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Duration )( 
            INSOPlay * This,
            /* [retval][out] */ double *pDuration);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Author )( 
            INSOPlay * This,
            /* [retval][out] */ BSTR *pbstrAuthor);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Copyright )( 
            INSOPlay * This,
            /* [retval][out] */ BSTR *pbstrCopyright);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Description )( 
            INSOPlay * This,
            /* [retval][out] */ BSTR *pbstrDescription);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Rating )( 
            INSOPlay * This,
            /* [retval][out] */ BSTR *pbstrRating);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Title )( 
            INSOPlay * This,
            /* [retval][out] */ BSTR *pbstrTitle);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SourceLink )( 
            INSOPlay * This,
            /* [retval][out] */ BSTR *pbstrSourceLink);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_MarkerCount )( 
            INSOPlay * This,
            /* [retval][out] */ long *pMarkerCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CanScan )( 
            INSOPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pCanScan);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CanSeek )( 
            INSOPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pCanSeek);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CanSeekToMarkers )( 
            INSOPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pCanSeekToMarkers);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CreationDate )( 
            INSOPlay * This,
            /* [retval][out] */ DATE *pCreationDate);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Bandwidth )( 
            INSOPlay * This,
            /* [retval][out] */ long *pBandwidth);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ErrorCorrection )( 
            INSOPlay * This,
            /* [retval][out] */ BSTR *pbstrErrorCorrection);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AutoStart )( 
            INSOPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pAutoStart);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AutoStart )( 
            INSOPlay * This,
            /* [in] */ VARIANT_BOOL AutoStart);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AutoRewind )( 
            INSOPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pAutoRewind);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AutoRewind )( 
            INSOPlay * This,
            /* [in] */ VARIANT_BOOL AutoRewind);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_InvokeURLs )( 
            INSOPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pInvokeURLs);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_InvokeURLs )( 
            INSOPlay * This,
            /* [in] */ VARIANT_BOOL InvokeURLs);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableContextMenu )( 
            INSOPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pEnableContextMenu);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableContextMenu )( 
            INSOPlay * This,
            /* [in] */ VARIANT_BOOL EnableContextMenu);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TransparentAtStart )( 
            INSOPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pTransparentAtStart);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_TransparentAtStart )( 
            INSOPlay * This,
            /* [in] */ VARIANT_BOOL TransparentAtStart);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ClickToPlay )( 
            INSOPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pClickToPlay);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ClickToPlay )( 
            INSOPlay * This,
            /* [in] */ VARIANT_BOOL ClickToPlay);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_FileName )( 
            INSOPlay * This,
            /* [retval][out] */ BSTR *pbstrFileName);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_FileName )( 
            INSOPlay * This,
            /* [in] */ BSTR bstrFileName);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentPosition )( 
            INSOPlay * This,
            /* [retval][out] */ double *pCurrentPosition);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_CurrentPosition )( 
            INSOPlay * This,
            /* [in] */ double CurrentPosition);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Rate )( 
            INSOPlay * This,
            /* [retval][out] */ double *pRate);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Rate )( 
            INSOPlay * This,
            /* [in] */ double Rate);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentMarker )( 
            INSOPlay * This,
            /* [retval][out] */ long *pCurrentMarker);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_CurrentMarker )( 
            INSOPlay * This,
            /* [in] */ long CurrentMarker);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlayCount )( 
            INSOPlay * This,
            /* [retval][out] */ long *pPlayCount);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_PlayCount )( 
            INSOPlay * This,
            /* [in] */ long PlayCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentState )( 
            INSOPlay * This,
            /* [retval][out] */ long *pCurrentState);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_DisplaySize )( 
            INSOPlay * This,
            /* [retval][out] */ long *pDisplaySize);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_DisplaySize )( 
            INSOPlay * This,
            /* [in] */ long DisplaySize);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_MainWindow )( 
            INSOPlay * This,
            /* [retval][out] */ long *pMainWindow);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AllowScan )( 
            INSOPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pAllowScan);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AllowScan )( 
            INSOPlay * This,
            /* [in] */ VARIANT_BOOL AllowScan);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendKeyboardEvents )( 
            INSOPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pSendKeyboardEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendKeyboardEvents )( 
            INSOPlay * This,
            /* [in] */ VARIANT_BOOL SendKeyboardEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendMouseClickEvents )( 
            INSOPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pSendMouseClickEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendMouseClickEvents )( 
            INSOPlay * This,
            /* [in] */ VARIANT_BOOL SendMouseClickEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendMouseMoveEvents )( 
            INSOPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pSendMouseMoveEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendMouseMoveEvents )( 
            INSOPlay * This,
            /* [in] */ VARIANT_BOOL SendMouseMoveEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendStateChangeEvents )( 
            INSOPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pSendStateChangeEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendStateChangeEvents )( 
            INSOPlay * This,
            /* [in] */ VARIANT_BOOL SendStateChangeEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ReceivedPackets )( 
            INSOPlay * This,
            /* [retval][out] */ long *pReceivedPackets);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_RecoveredPackets )( 
            INSOPlay * This,
            /* [retval][out] */ long *pRecoveredPackets);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LostPackets )( 
            INSOPlay * This,
            /* [retval][out] */ long *pLostPackets);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ReceptionQuality )( 
            INSOPlay * This,
            /* [retval][out] */ long *pReceptionQuality);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_BufferingCount )( 
            INSOPlay * This,
            /* [retval][out] */ long *pBufferingCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CursorType )( 
            INSOPlay * This,
            /* [retval][out] */ long *pCursorType);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_CursorType )( 
            INSOPlay * This,
            /* [in] */ long CursorType);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AnimationAtStart )( 
            INSOPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pAnimationAtStart);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AnimationAtStart )( 
            INSOPlay * This,
            /* [in] */ VARIANT_BOOL AnimationAtStart);
        
        /* [hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AnimationOnStop )( 
            INSOPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pAnimationOnStop);
        
        /* [hidden][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AnimationOnStop )( 
            INSOPlay * This,
            /* [in] */ VARIANT_BOOL AnimationOnStop);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Play )( 
            INSOPlay * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Pause )( 
            INSOPlay * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Stop )( 
            INSOPlay * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetMarkerTime )( 
            INSOPlay * This,
            /* [in] */ long MarkerNum,
            /* [retval][out] */ double *pMarkerTime);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetMarkerName )( 
            INSOPlay * This,
            /* [in] */ long MarkerNum,
            /* [retval][out] */ BSTR *pbstrMarkerName);
        
        END_INTERFACE
    } INSOPlayVtbl;

    interface INSOPlay
    {
        CONST_VTBL struct INSOPlayVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define INSOPlay_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define INSOPlay_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define INSOPlay_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define INSOPlay_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define INSOPlay_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define INSOPlay_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define INSOPlay_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define INSOPlay_get_ImageSourceWidth(This,pWidth)	\
    ( (This)->lpVtbl -> get_ImageSourceWidth(This,pWidth) ) 

#define INSOPlay_get_ImageSourceHeight(This,pHeight)	\
    ( (This)->lpVtbl -> get_ImageSourceHeight(This,pHeight) ) 

#define INSOPlay_get_Duration(This,pDuration)	\
    ( (This)->lpVtbl -> get_Duration(This,pDuration) ) 

#define INSOPlay_get_Author(This,pbstrAuthor)	\
    ( (This)->lpVtbl -> get_Author(This,pbstrAuthor) ) 

#define INSOPlay_get_Copyright(This,pbstrCopyright)	\
    ( (This)->lpVtbl -> get_Copyright(This,pbstrCopyright) ) 

#define INSOPlay_get_Description(This,pbstrDescription)	\
    ( (This)->lpVtbl -> get_Description(This,pbstrDescription) ) 

#define INSOPlay_get_Rating(This,pbstrRating)	\
    ( (This)->lpVtbl -> get_Rating(This,pbstrRating) ) 

#define INSOPlay_get_Title(This,pbstrTitle)	\
    ( (This)->lpVtbl -> get_Title(This,pbstrTitle) ) 

#define INSOPlay_get_SourceLink(This,pbstrSourceLink)	\
    ( (This)->lpVtbl -> get_SourceLink(This,pbstrSourceLink) ) 

#define INSOPlay_get_MarkerCount(This,pMarkerCount)	\
    ( (This)->lpVtbl -> get_MarkerCount(This,pMarkerCount) ) 

#define INSOPlay_get_CanScan(This,pCanScan)	\
    ( (This)->lpVtbl -> get_CanScan(This,pCanScan) ) 

#define INSOPlay_get_CanSeek(This,pCanSeek)	\
    ( (This)->lpVtbl -> get_CanSeek(This,pCanSeek) ) 

#define INSOPlay_get_CanSeekToMarkers(This,pCanSeekToMarkers)	\
    ( (This)->lpVtbl -> get_CanSeekToMarkers(This,pCanSeekToMarkers) ) 

#define INSOPlay_get_CreationDate(This,pCreationDate)	\
    ( (This)->lpVtbl -> get_CreationDate(This,pCreationDate) ) 

#define INSOPlay_get_Bandwidth(This,pBandwidth)	\
    ( (This)->lpVtbl -> get_Bandwidth(This,pBandwidth) ) 

#define INSOPlay_get_ErrorCorrection(This,pbstrErrorCorrection)	\
    ( (This)->lpVtbl -> get_ErrorCorrection(This,pbstrErrorCorrection) ) 

#define INSOPlay_get_AutoStart(This,pAutoStart)	\
    ( (This)->lpVtbl -> get_AutoStart(This,pAutoStart) ) 

#define INSOPlay_put_AutoStart(This,AutoStart)	\
    ( (This)->lpVtbl -> put_AutoStart(This,AutoStart) ) 

#define INSOPlay_get_AutoRewind(This,pAutoRewind)	\
    ( (This)->lpVtbl -> get_AutoRewind(This,pAutoRewind) ) 

#define INSOPlay_put_AutoRewind(This,AutoRewind)	\
    ( (This)->lpVtbl -> put_AutoRewind(This,AutoRewind) ) 

#define INSOPlay_get_InvokeURLs(This,pInvokeURLs)	\
    ( (This)->lpVtbl -> get_InvokeURLs(This,pInvokeURLs) ) 

#define INSOPlay_put_InvokeURLs(This,InvokeURLs)	\
    ( (This)->lpVtbl -> put_InvokeURLs(This,InvokeURLs) ) 

#define INSOPlay_get_EnableContextMenu(This,pEnableContextMenu)	\
    ( (This)->lpVtbl -> get_EnableContextMenu(This,pEnableContextMenu) ) 

#define INSOPlay_put_EnableContextMenu(This,EnableContextMenu)	\
    ( (This)->lpVtbl -> put_EnableContextMenu(This,EnableContextMenu) ) 

#define INSOPlay_get_TransparentAtStart(This,pTransparentAtStart)	\
    ( (This)->lpVtbl -> get_TransparentAtStart(This,pTransparentAtStart) ) 

#define INSOPlay_put_TransparentAtStart(This,TransparentAtStart)	\
    ( (This)->lpVtbl -> put_TransparentAtStart(This,TransparentAtStart) ) 

#define INSOPlay_get_ClickToPlay(This,pClickToPlay)	\
    ( (This)->lpVtbl -> get_ClickToPlay(This,pClickToPlay) ) 

#define INSOPlay_put_ClickToPlay(This,ClickToPlay)	\
    ( (This)->lpVtbl -> put_ClickToPlay(This,ClickToPlay) ) 

#define INSOPlay_get_FileName(This,pbstrFileName)	\
    ( (This)->lpVtbl -> get_FileName(This,pbstrFileName) ) 

#define INSOPlay_put_FileName(This,bstrFileName)	\
    ( (This)->lpVtbl -> put_FileName(This,bstrFileName) ) 

#define INSOPlay_get_CurrentPosition(This,pCurrentPosition)	\
    ( (This)->lpVtbl -> get_CurrentPosition(This,pCurrentPosition) ) 

#define INSOPlay_put_CurrentPosition(This,CurrentPosition)	\
    ( (This)->lpVtbl -> put_CurrentPosition(This,CurrentPosition) ) 

#define INSOPlay_get_Rate(This,pRate)	\
    ( (This)->lpVtbl -> get_Rate(This,pRate) ) 

#define INSOPlay_put_Rate(This,Rate)	\
    ( (This)->lpVtbl -> put_Rate(This,Rate) ) 

#define INSOPlay_get_CurrentMarker(This,pCurrentMarker)	\
    ( (This)->lpVtbl -> get_CurrentMarker(This,pCurrentMarker) ) 

#define INSOPlay_put_CurrentMarker(This,CurrentMarker)	\
    ( (This)->lpVtbl -> put_CurrentMarker(This,CurrentMarker) ) 

#define INSOPlay_get_PlayCount(This,pPlayCount)	\
    ( (This)->lpVtbl -> get_PlayCount(This,pPlayCount) ) 

#define INSOPlay_put_PlayCount(This,PlayCount)	\
    ( (This)->lpVtbl -> put_PlayCount(This,PlayCount) ) 

#define INSOPlay_get_CurrentState(This,pCurrentState)	\
    ( (This)->lpVtbl -> get_CurrentState(This,pCurrentState) ) 

#define INSOPlay_get_DisplaySize(This,pDisplaySize)	\
    ( (This)->lpVtbl -> get_DisplaySize(This,pDisplaySize) ) 

#define INSOPlay_put_DisplaySize(This,DisplaySize)	\
    ( (This)->lpVtbl -> put_DisplaySize(This,DisplaySize) ) 

#define INSOPlay_get_MainWindow(This,pMainWindow)	\
    ( (This)->lpVtbl -> get_MainWindow(This,pMainWindow) ) 

#define INSOPlay_get_AllowScan(This,pAllowScan)	\
    ( (This)->lpVtbl -> get_AllowScan(This,pAllowScan) ) 

#define INSOPlay_put_AllowScan(This,AllowScan)	\
    ( (This)->lpVtbl -> put_AllowScan(This,AllowScan) ) 

#define INSOPlay_get_SendKeyboardEvents(This,pSendKeyboardEvents)	\
    ( (This)->lpVtbl -> get_SendKeyboardEvents(This,pSendKeyboardEvents) ) 

#define INSOPlay_put_SendKeyboardEvents(This,SendKeyboardEvents)	\
    ( (This)->lpVtbl -> put_SendKeyboardEvents(This,SendKeyboardEvents) ) 

#define INSOPlay_get_SendMouseClickEvents(This,pSendMouseClickEvents)	\
    ( (This)->lpVtbl -> get_SendMouseClickEvents(This,pSendMouseClickEvents) ) 

#define INSOPlay_put_SendMouseClickEvents(This,SendMouseClickEvents)	\
    ( (This)->lpVtbl -> put_SendMouseClickEvents(This,SendMouseClickEvents) ) 

#define INSOPlay_get_SendMouseMoveEvents(This,pSendMouseMoveEvents)	\
    ( (This)->lpVtbl -> get_SendMouseMoveEvents(This,pSendMouseMoveEvents) ) 

#define INSOPlay_put_SendMouseMoveEvents(This,SendMouseMoveEvents)	\
    ( (This)->lpVtbl -> put_SendMouseMoveEvents(This,SendMouseMoveEvents) ) 

#define INSOPlay_get_SendStateChangeEvents(This,pSendStateChangeEvents)	\
    ( (This)->lpVtbl -> get_SendStateChangeEvents(This,pSendStateChangeEvents) ) 

#define INSOPlay_put_SendStateChangeEvents(This,SendStateChangeEvents)	\
    ( (This)->lpVtbl -> put_SendStateChangeEvents(This,SendStateChangeEvents) ) 

#define INSOPlay_get_ReceivedPackets(This,pReceivedPackets)	\
    ( (This)->lpVtbl -> get_ReceivedPackets(This,pReceivedPackets) ) 

#define INSOPlay_get_RecoveredPackets(This,pRecoveredPackets)	\
    ( (This)->lpVtbl -> get_RecoveredPackets(This,pRecoveredPackets) ) 

#define INSOPlay_get_LostPackets(This,pLostPackets)	\
    ( (This)->lpVtbl -> get_LostPackets(This,pLostPackets) ) 

#define INSOPlay_get_ReceptionQuality(This,pReceptionQuality)	\
    ( (This)->lpVtbl -> get_ReceptionQuality(This,pReceptionQuality) ) 

#define INSOPlay_get_BufferingCount(This,pBufferingCount)	\
    ( (This)->lpVtbl -> get_BufferingCount(This,pBufferingCount) ) 

#define INSOPlay_get_CursorType(This,pCursorType)	\
    ( (This)->lpVtbl -> get_CursorType(This,pCursorType) ) 

#define INSOPlay_put_CursorType(This,CursorType)	\
    ( (This)->lpVtbl -> put_CursorType(This,CursorType) ) 

#define INSOPlay_get_AnimationAtStart(This,pAnimationAtStart)	\
    ( (This)->lpVtbl -> get_AnimationAtStart(This,pAnimationAtStart) ) 

#define INSOPlay_put_AnimationAtStart(This,AnimationAtStart)	\
    ( (This)->lpVtbl -> put_AnimationAtStart(This,AnimationAtStart) ) 

#define INSOPlay_get_AnimationOnStop(This,pAnimationOnStop)	\
    ( (This)->lpVtbl -> get_AnimationOnStop(This,pAnimationOnStop) ) 

#define INSOPlay_put_AnimationOnStop(This,AnimationOnStop)	\
    ( (This)->lpVtbl -> put_AnimationOnStop(This,AnimationOnStop) ) 

#define INSOPlay_Play(This)	\
    ( (This)->lpVtbl -> Play(This) ) 

#define INSOPlay_Pause(This)	\
    ( (This)->lpVtbl -> Pause(This) ) 

#define INSOPlay_Stop(This)	\
    ( (This)->lpVtbl -> Stop(This) ) 

#define INSOPlay_GetMarkerTime(This,MarkerNum,pMarkerTime)	\
    ( (This)->lpVtbl -> GetMarkerTime(This,MarkerNum,pMarkerTime) ) 

#define INSOPlay_GetMarkerName(This,MarkerNum,pbstrMarkerName)	\
    ( (This)->lpVtbl -> GetMarkerName(This,MarkerNum,pbstrMarkerName) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __INSOPlay_INTERFACE_DEFINED__ */


#ifndef __INSPlay_INTERFACE_DEFINED__
#define __INSPlay_INTERFACE_DEFINED__

/* interface INSPlay */
/* [hidden][dual][uuid][object] */ 


EXTERN_C const IID IID_INSPlay;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("E7C4BE80-7960-11d0-B727-00AA00B4E220")
    INSPlay : public INSOPlay
    {
    public:
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ChannelName( 
            /* [retval][out] */ BSTR *pbstrChannelName) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ChannelDescription( 
            /* [retval][out] */ BSTR *pbstrChannelDescription) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ChannelURL( 
            /* [retval][out] */ BSTR *pbstrChannelURL) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ContactAddress( 
            /* [retval][out] */ BSTR *pbstrContactAddress) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ContactPhone( 
            /* [retval][out] */ BSTR *pbstrContactPhone) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ContactEmail( 
            /* [retval][out] */ BSTR *pbstrContactEmail) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_AllowChangeDisplaySize( 
            /* [retval][out] */ VARIANT_BOOL *pAllowChangeDisplaySize) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_AllowChangeDisplaySize( 
            /* [in] */ VARIANT_BOOL AllowChangeDisplaySize) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CodecCount( 
            /* [retval][out] */ long *pCodecCount) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_IsBroadcast( 
            /* [retval][out] */ VARIANT_BOOL *pIsBroadcast) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_IsDurationValid( 
            /* [retval][out] */ VARIANT_BOOL *pIsDurationValid) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SourceProtocol( 
            /* [retval][out] */ long *pSourceProtocol) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_OpenState( 
            /* [retval][out] */ long *pOpenState) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SendOpenStateChangeEvents( 
            /* [retval][out] */ VARIANT_BOOL *pSendOpenStateChangeEvents) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_SendOpenStateChangeEvents( 
            /* [in] */ VARIANT_BOOL SendOpenStateChangeEvents) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SendWarningEvents( 
            /* [retval][out] */ VARIANT_BOOL *pSendWarningEvents) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_SendWarningEvents( 
            /* [in] */ VARIANT_BOOL SendWarningEvents) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SendErrorEvents( 
            /* [retval][out] */ VARIANT_BOOL *pSendErrorEvents) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_SendErrorEvents( 
            /* [in] */ VARIANT_BOOL SendErrorEvents) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_HasError( 
            /* [retval][out] */ VARIANT_BOOL *pHasError) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ErrorDescription( 
            /* [retval][out] */ BSTR *pbstrErrorDescription) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ErrorCode( 
            /* [retval][out] */ long *pErrorCode) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_PlayState( 
            /* [retval][out] */ long *pPlayState) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_SendPlayStateChangeEvents( 
            /* [retval][out] */ VARIANT_BOOL *pSendPlayStateChangeEvents) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_SendPlayStateChangeEvents( 
            /* [in] */ VARIANT_BOOL SendPlayStateChangeEvents) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_BufferingTime( 
            /* [retval][out] */ double *pBufferingTime) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_BufferingTime( 
            /* [in] */ double BufferingTime) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_UseFixedUDPPort( 
            /* [retval][out] */ VARIANT_BOOL *pUseFixedUDPPort) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_UseFixedUDPPort( 
            /* [in] */ VARIANT_BOOL UseFixedUDPPort) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_FixedUDPPort( 
            /* [retval][out] */ long *pFixedUDPPort) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_FixedUDPPort( 
            /* [in] */ long FixedUDPPort) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_UseHTTPProxy( 
            /* [retval][out] */ VARIANT_BOOL *pUseHTTPProxy) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_UseHTTPProxy( 
            /* [in] */ VARIANT_BOOL UseHTTPProxy) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_EnableAutoProxy( 
            /* [retval][out] */ VARIANT_BOOL *pEnableAutoProxy) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_EnableAutoProxy( 
            /* [in] */ VARIANT_BOOL EnableAutoProxy) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_HTTPProxyHost( 
            /* [retval][out] */ BSTR *pbstrHTTPProxyHost) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_HTTPProxyHost( 
            /* [in] */ BSTR bstrHTTPProxyHost) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_HTTPProxyPort( 
            /* [retval][out] */ long *pHTTPProxyPort) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_HTTPProxyPort( 
            /* [in] */ long HTTPProxyPort) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_EnableMulticast( 
            /* [retval][out] */ VARIANT_BOOL *pEnableMulticast) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_EnableMulticast( 
            /* [in] */ VARIANT_BOOL EnableMulticast) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_EnableUDP( 
            /* [retval][out] */ VARIANT_BOOL *pEnableUDP) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_EnableUDP( 
            /* [in] */ VARIANT_BOOL EnableUDP) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_EnableTCP( 
            /* [retval][out] */ VARIANT_BOOL *pEnableTCP) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_EnableTCP( 
            /* [in] */ VARIANT_BOOL EnableTCP) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_EnableHTTP( 
            /* [retval][out] */ VARIANT_BOOL *pEnableHTTP) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_EnableHTTP( 
            /* [in] */ VARIANT_BOOL EnableHTTP) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_BufferingProgress( 
            /* [retval][out] */ long *pBufferingProgress) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_BaseURL( 
            /* [retval][out] */ BSTR *pbstrBaseURL) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_BaseURL( 
            /* [in] */ BSTR bstrBaseURL) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_DefaultFrame( 
            /* [retval][out] */ BSTR *pbstrDefaultFrame) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_DefaultFrame( 
            /* [in] */ BSTR bstrDefaultFrame) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE AboutBox( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Cancel( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetCodecInstalled( 
            /* [in] */ long CodecNum,
            /* [retval][out] */ VARIANT_BOOL *pCodecInstalled) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetCodecDescription( 
            /* [in] */ long CodecNum,
            /* [retval][out] */ BSTR *pbstrCodecDescription) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetCodecURL( 
            /* [in] */ long CodecNum,
            /* [retval][out] */ BSTR *pbstrCodecURL) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Open( 
            /* [in] */ BSTR bstrFileName) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct INSPlayVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            INSPlay * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            INSPlay * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            INSPlay * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            INSPlay * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            INSPlay * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            INSPlay * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            INSPlay * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ImageSourceWidth )( 
            INSPlay * This,
            /* [retval][out] */ long *pWidth);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ImageSourceHeight )( 
            INSPlay * This,
            /* [retval][out] */ long *pHeight);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Duration )( 
            INSPlay * This,
            /* [retval][out] */ double *pDuration);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Author )( 
            INSPlay * This,
            /* [retval][out] */ BSTR *pbstrAuthor);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Copyright )( 
            INSPlay * This,
            /* [retval][out] */ BSTR *pbstrCopyright);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Description )( 
            INSPlay * This,
            /* [retval][out] */ BSTR *pbstrDescription);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Rating )( 
            INSPlay * This,
            /* [retval][out] */ BSTR *pbstrRating);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Title )( 
            INSPlay * This,
            /* [retval][out] */ BSTR *pbstrTitle);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SourceLink )( 
            INSPlay * This,
            /* [retval][out] */ BSTR *pbstrSourceLink);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_MarkerCount )( 
            INSPlay * This,
            /* [retval][out] */ long *pMarkerCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CanScan )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pCanScan);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CanSeek )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pCanSeek);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CanSeekToMarkers )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pCanSeekToMarkers);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CreationDate )( 
            INSPlay * This,
            /* [retval][out] */ DATE *pCreationDate);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Bandwidth )( 
            INSPlay * This,
            /* [retval][out] */ long *pBandwidth);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ErrorCorrection )( 
            INSPlay * This,
            /* [retval][out] */ BSTR *pbstrErrorCorrection);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AutoStart )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pAutoStart);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AutoStart )( 
            INSPlay * This,
            /* [in] */ VARIANT_BOOL AutoStart);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AutoRewind )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pAutoRewind);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AutoRewind )( 
            INSPlay * This,
            /* [in] */ VARIANT_BOOL AutoRewind);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_InvokeURLs )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pInvokeURLs);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_InvokeURLs )( 
            INSPlay * This,
            /* [in] */ VARIANT_BOOL InvokeURLs);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableContextMenu )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pEnableContextMenu);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableContextMenu )( 
            INSPlay * This,
            /* [in] */ VARIANT_BOOL EnableContextMenu);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TransparentAtStart )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pTransparentAtStart);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_TransparentAtStart )( 
            INSPlay * This,
            /* [in] */ VARIANT_BOOL TransparentAtStart);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ClickToPlay )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pClickToPlay);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ClickToPlay )( 
            INSPlay * This,
            /* [in] */ VARIANT_BOOL ClickToPlay);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_FileName )( 
            INSPlay * This,
            /* [retval][out] */ BSTR *pbstrFileName);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_FileName )( 
            INSPlay * This,
            /* [in] */ BSTR bstrFileName);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentPosition )( 
            INSPlay * This,
            /* [retval][out] */ double *pCurrentPosition);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_CurrentPosition )( 
            INSPlay * This,
            /* [in] */ double CurrentPosition);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Rate )( 
            INSPlay * This,
            /* [retval][out] */ double *pRate);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Rate )( 
            INSPlay * This,
            /* [in] */ double Rate);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentMarker )( 
            INSPlay * This,
            /* [retval][out] */ long *pCurrentMarker);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_CurrentMarker )( 
            INSPlay * This,
            /* [in] */ long CurrentMarker);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlayCount )( 
            INSPlay * This,
            /* [retval][out] */ long *pPlayCount);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_PlayCount )( 
            INSPlay * This,
            /* [in] */ long PlayCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentState )( 
            INSPlay * This,
            /* [retval][out] */ long *pCurrentState);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_DisplaySize )( 
            INSPlay * This,
            /* [retval][out] */ long *pDisplaySize);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_DisplaySize )( 
            INSPlay * This,
            /* [in] */ long DisplaySize);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_MainWindow )( 
            INSPlay * This,
            /* [retval][out] */ long *pMainWindow);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AllowScan )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pAllowScan);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AllowScan )( 
            INSPlay * This,
            /* [in] */ VARIANT_BOOL AllowScan);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendKeyboardEvents )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pSendKeyboardEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendKeyboardEvents )( 
            INSPlay * This,
            /* [in] */ VARIANT_BOOL SendKeyboardEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendMouseClickEvents )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pSendMouseClickEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendMouseClickEvents )( 
            INSPlay * This,
            /* [in] */ VARIANT_BOOL SendMouseClickEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendMouseMoveEvents )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pSendMouseMoveEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendMouseMoveEvents )( 
            INSPlay * This,
            /* [in] */ VARIANT_BOOL SendMouseMoveEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendStateChangeEvents )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pSendStateChangeEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendStateChangeEvents )( 
            INSPlay * This,
            /* [in] */ VARIANT_BOOL SendStateChangeEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ReceivedPackets )( 
            INSPlay * This,
            /* [retval][out] */ long *pReceivedPackets);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_RecoveredPackets )( 
            INSPlay * This,
            /* [retval][out] */ long *pRecoveredPackets);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LostPackets )( 
            INSPlay * This,
            /* [retval][out] */ long *pLostPackets);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ReceptionQuality )( 
            INSPlay * This,
            /* [retval][out] */ long *pReceptionQuality);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_BufferingCount )( 
            INSPlay * This,
            /* [retval][out] */ long *pBufferingCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CursorType )( 
            INSPlay * This,
            /* [retval][out] */ long *pCursorType);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_CursorType )( 
            INSPlay * This,
            /* [in] */ long CursorType);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AnimationAtStart )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pAnimationAtStart);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AnimationAtStart )( 
            INSPlay * This,
            /* [in] */ VARIANT_BOOL AnimationAtStart);
        
        /* [hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AnimationOnStop )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pAnimationOnStop);
        
        /* [hidden][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AnimationOnStop )( 
            INSPlay * This,
            /* [in] */ VARIANT_BOOL AnimationOnStop);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Play )( 
            INSPlay * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Pause )( 
            INSPlay * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Stop )( 
            INSPlay * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetMarkerTime )( 
            INSPlay * This,
            /* [in] */ long MarkerNum,
            /* [retval][out] */ double *pMarkerTime);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetMarkerName )( 
            INSPlay * This,
            /* [in] */ long MarkerNum,
            /* [retval][out] */ BSTR *pbstrMarkerName);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ChannelName )( 
            INSPlay * This,
            /* [retval][out] */ BSTR *pbstrChannelName);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ChannelDescription )( 
            INSPlay * This,
            /* [retval][out] */ BSTR *pbstrChannelDescription);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ChannelURL )( 
            INSPlay * This,
            /* [retval][out] */ BSTR *pbstrChannelURL);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ContactAddress )( 
            INSPlay * This,
            /* [retval][out] */ BSTR *pbstrContactAddress);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ContactPhone )( 
            INSPlay * This,
            /* [retval][out] */ BSTR *pbstrContactPhone);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ContactEmail )( 
            INSPlay * This,
            /* [retval][out] */ BSTR *pbstrContactEmail);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AllowChangeDisplaySize )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pAllowChangeDisplaySize);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AllowChangeDisplaySize )( 
            INSPlay * This,
            /* [in] */ VARIANT_BOOL AllowChangeDisplaySize);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CodecCount )( 
            INSPlay * This,
            /* [retval][out] */ long *pCodecCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_IsBroadcast )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pIsBroadcast);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_IsDurationValid )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pIsDurationValid);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SourceProtocol )( 
            INSPlay * This,
            /* [retval][out] */ long *pSourceProtocol);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OpenState )( 
            INSPlay * This,
            /* [retval][out] */ long *pOpenState);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendOpenStateChangeEvents )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pSendOpenStateChangeEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendOpenStateChangeEvents )( 
            INSPlay * This,
            /* [in] */ VARIANT_BOOL SendOpenStateChangeEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendWarningEvents )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pSendWarningEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendWarningEvents )( 
            INSPlay * This,
            /* [in] */ VARIANT_BOOL SendWarningEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendErrorEvents )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pSendErrorEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendErrorEvents )( 
            INSPlay * This,
            /* [in] */ VARIANT_BOOL SendErrorEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HasError )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pHasError);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ErrorDescription )( 
            INSPlay * This,
            /* [retval][out] */ BSTR *pbstrErrorDescription);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ErrorCode )( 
            INSPlay * This,
            /* [retval][out] */ long *pErrorCode);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlayState )( 
            INSPlay * This,
            /* [retval][out] */ long *pPlayState);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendPlayStateChangeEvents )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pSendPlayStateChangeEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendPlayStateChangeEvents )( 
            INSPlay * This,
            /* [in] */ VARIANT_BOOL SendPlayStateChangeEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_BufferingTime )( 
            INSPlay * This,
            /* [retval][out] */ double *pBufferingTime);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_BufferingTime )( 
            INSPlay * This,
            /* [in] */ double BufferingTime);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_UseFixedUDPPort )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pUseFixedUDPPort);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_UseFixedUDPPort )( 
            INSPlay * This,
            /* [in] */ VARIANT_BOOL UseFixedUDPPort);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_FixedUDPPort )( 
            INSPlay * This,
            /* [retval][out] */ long *pFixedUDPPort);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_FixedUDPPort )( 
            INSPlay * This,
            /* [in] */ long FixedUDPPort);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_UseHTTPProxy )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pUseHTTPProxy);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_UseHTTPProxy )( 
            INSPlay * This,
            /* [in] */ VARIANT_BOOL UseHTTPProxy);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableAutoProxy )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pEnableAutoProxy);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableAutoProxy )( 
            INSPlay * This,
            /* [in] */ VARIANT_BOOL EnableAutoProxy);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HTTPProxyHost )( 
            INSPlay * This,
            /* [retval][out] */ BSTR *pbstrHTTPProxyHost);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_HTTPProxyHost )( 
            INSPlay * This,
            /* [in] */ BSTR bstrHTTPProxyHost);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HTTPProxyPort )( 
            INSPlay * This,
            /* [retval][out] */ long *pHTTPProxyPort);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_HTTPProxyPort )( 
            INSPlay * This,
            /* [in] */ long HTTPProxyPort);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableMulticast )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pEnableMulticast);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableMulticast )( 
            INSPlay * This,
            /* [in] */ VARIANT_BOOL EnableMulticast);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableUDP )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pEnableUDP);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableUDP )( 
            INSPlay * This,
            /* [in] */ VARIANT_BOOL EnableUDP);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableTCP )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pEnableTCP);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableTCP )( 
            INSPlay * This,
            /* [in] */ VARIANT_BOOL EnableTCP);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableHTTP )( 
            INSPlay * This,
            /* [retval][out] */ VARIANT_BOOL *pEnableHTTP);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableHTTP )( 
            INSPlay * This,
            /* [in] */ VARIANT_BOOL EnableHTTP);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_BufferingProgress )( 
            INSPlay * This,
            /* [retval][out] */ long *pBufferingProgress);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_BaseURL )( 
            INSPlay * This,
            /* [retval][out] */ BSTR *pbstrBaseURL);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_BaseURL )( 
            INSPlay * This,
            /* [in] */ BSTR bstrBaseURL);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultFrame )( 
            INSPlay * This,
            /* [retval][out] */ BSTR *pbstrDefaultFrame);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_DefaultFrame )( 
            INSPlay * This,
            /* [in] */ BSTR bstrDefaultFrame);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *AboutBox )( 
            INSPlay * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Cancel )( 
            INSPlay * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetCodecInstalled )( 
            INSPlay * This,
            /* [in] */ long CodecNum,
            /* [retval][out] */ VARIANT_BOOL *pCodecInstalled);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetCodecDescription )( 
            INSPlay * This,
            /* [in] */ long CodecNum,
            /* [retval][out] */ BSTR *pbstrCodecDescription);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetCodecURL )( 
            INSPlay * This,
            /* [in] */ long CodecNum,
            /* [retval][out] */ BSTR *pbstrCodecURL);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Open )( 
            INSPlay * This,
            /* [in] */ BSTR bstrFileName);
        
        END_INTERFACE
    } INSPlayVtbl;

    interface INSPlay
    {
        CONST_VTBL struct INSPlayVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define INSPlay_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define INSPlay_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define INSPlay_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define INSPlay_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define INSPlay_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define INSPlay_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define INSPlay_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define INSPlay_get_ImageSourceWidth(This,pWidth)	\
    ( (This)->lpVtbl -> get_ImageSourceWidth(This,pWidth) ) 

#define INSPlay_get_ImageSourceHeight(This,pHeight)	\
    ( (This)->lpVtbl -> get_ImageSourceHeight(This,pHeight) ) 

#define INSPlay_get_Duration(This,pDuration)	\
    ( (This)->lpVtbl -> get_Duration(This,pDuration) ) 

#define INSPlay_get_Author(This,pbstrAuthor)	\
    ( (This)->lpVtbl -> get_Author(This,pbstrAuthor) ) 

#define INSPlay_get_Copyright(This,pbstrCopyright)	\
    ( (This)->lpVtbl -> get_Copyright(This,pbstrCopyright) ) 

#define INSPlay_get_Description(This,pbstrDescription)	\
    ( (This)->lpVtbl -> get_Description(This,pbstrDescription) ) 

#define INSPlay_get_Rating(This,pbstrRating)	\
    ( (This)->lpVtbl -> get_Rating(This,pbstrRating) ) 

#define INSPlay_get_Title(This,pbstrTitle)	\
    ( (This)->lpVtbl -> get_Title(This,pbstrTitle) ) 

#define INSPlay_get_SourceLink(This,pbstrSourceLink)	\
    ( (This)->lpVtbl -> get_SourceLink(This,pbstrSourceLink) ) 

#define INSPlay_get_MarkerCount(This,pMarkerCount)	\
    ( (This)->lpVtbl -> get_MarkerCount(This,pMarkerCount) ) 

#define INSPlay_get_CanScan(This,pCanScan)	\
    ( (This)->lpVtbl -> get_CanScan(This,pCanScan) ) 

#define INSPlay_get_CanSeek(This,pCanSeek)	\
    ( (This)->lpVtbl -> get_CanSeek(This,pCanSeek) ) 

#define INSPlay_get_CanSeekToMarkers(This,pCanSeekToMarkers)	\
    ( (This)->lpVtbl -> get_CanSeekToMarkers(This,pCanSeekToMarkers) ) 

#define INSPlay_get_CreationDate(This,pCreationDate)	\
    ( (This)->lpVtbl -> get_CreationDate(This,pCreationDate) ) 

#define INSPlay_get_Bandwidth(This,pBandwidth)	\
    ( (This)->lpVtbl -> get_Bandwidth(This,pBandwidth) ) 

#define INSPlay_get_ErrorCorrection(This,pbstrErrorCorrection)	\
    ( (This)->lpVtbl -> get_ErrorCorrection(This,pbstrErrorCorrection) ) 

#define INSPlay_get_AutoStart(This,pAutoStart)	\
    ( (This)->lpVtbl -> get_AutoStart(This,pAutoStart) ) 

#define INSPlay_put_AutoStart(This,AutoStart)	\
    ( (This)->lpVtbl -> put_AutoStart(This,AutoStart) ) 

#define INSPlay_get_AutoRewind(This,pAutoRewind)	\
    ( (This)->lpVtbl -> get_AutoRewind(This,pAutoRewind) ) 

#define INSPlay_put_AutoRewind(This,AutoRewind)	\
    ( (This)->lpVtbl -> put_AutoRewind(This,AutoRewind) ) 

#define INSPlay_get_InvokeURLs(This,pInvokeURLs)	\
    ( (This)->lpVtbl -> get_InvokeURLs(This,pInvokeURLs) ) 

#define INSPlay_put_InvokeURLs(This,InvokeURLs)	\
    ( (This)->lpVtbl -> put_InvokeURLs(This,InvokeURLs) ) 

#define INSPlay_get_EnableContextMenu(This,pEnableContextMenu)	\
    ( (This)->lpVtbl -> get_EnableContextMenu(This,pEnableContextMenu) ) 

#define INSPlay_put_EnableContextMenu(This,EnableContextMenu)	\
    ( (This)->lpVtbl -> put_EnableContextMenu(This,EnableContextMenu) ) 

#define INSPlay_get_TransparentAtStart(This,pTransparentAtStart)	\
    ( (This)->lpVtbl -> get_TransparentAtStart(This,pTransparentAtStart) ) 

#define INSPlay_put_TransparentAtStart(This,TransparentAtStart)	\
    ( (This)->lpVtbl -> put_TransparentAtStart(This,TransparentAtStart) ) 

#define INSPlay_get_ClickToPlay(This,pClickToPlay)	\
    ( (This)->lpVtbl -> get_ClickToPlay(This,pClickToPlay) ) 

#define INSPlay_put_ClickToPlay(This,ClickToPlay)	\
    ( (This)->lpVtbl -> put_ClickToPlay(This,ClickToPlay) ) 

#define INSPlay_get_FileName(This,pbstrFileName)	\
    ( (This)->lpVtbl -> get_FileName(This,pbstrFileName) ) 

#define INSPlay_put_FileName(This,bstrFileName)	\
    ( (This)->lpVtbl -> put_FileName(This,bstrFileName) ) 

#define INSPlay_get_CurrentPosition(This,pCurrentPosition)	\
    ( (This)->lpVtbl -> get_CurrentPosition(This,pCurrentPosition) ) 

#define INSPlay_put_CurrentPosition(This,CurrentPosition)	\
    ( (This)->lpVtbl -> put_CurrentPosition(This,CurrentPosition) ) 

#define INSPlay_get_Rate(This,pRate)	\
    ( (This)->lpVtbl -> get_Rate(This,pRate) ) 

#define INSPlay_put_Rate(This,Rate)	\
    ( (This)->lpVtbl -> put_Rate(This,Rate) ) 

#define INSPlay_get_CurrentMarker(This,pCurrentMarker)	\
    ( (This)->lpVtbl -> get_CurrentMarker(This,pCurrentMarker) ) 

#define INSPlay_put_CurrentMarker(This,CurrentMarker)	\
    ( (This)->lpVtbl -> put_CurrentMarker(This,CurrentMarker) ) 

#define INSPlay_get_PlayCount(This,pPlayCount)	\
    ( (This)->lpVtbl -> get_PlayCount(This,pPlayCount) ) 

#define INSPlay_put_PlayCount(This,PlayCount)	\
    ( (This)->lpVtbl -> put_PlayCount(This,PlayCount) ) 

#define INSPlay_get_CurrentState(This,pCurrentState)	\
    ( (This)->lpVtbl -> get_CurrentState(This,pCurrentState) ) 

#define INSPlay_get_DisplaySize(This,pDisplaySize)	\
    ( (This)->lpVtbl -> get_DisplaySize(This,pDisplaySize) ) 

#define INSPlay_put_DisplaySize(This,DisplaySize)	\
    ( (This)->lpVtbl -> put_DisplaySize(This,DisplaySize) ) 

#define INSPlay_get_MainWindow(This,pMainWindow)	\
    ( (This)->lpVtbl -> get_MainWindow(This,pMainWindow) ) 

#define INSPlay_get_AllowScan(This,pAllowScan)	\
    ( (This)->lpVtbl -> get_AllowScan(This,pAllowScan) ) 

#define INSPlay_put_AllowScan(This,AllowScan)	\
    ( (This)->lpVtbl -> put_AllowScan(This,AllowScan) ) 

#define INSPlay_get_SendKeyboardEvents(This,pSendKeyboardEvents)	\
    ( (This)->lpVtbl -> get_SendKeyboardEvents(This,pSendKeyboardEvents) ) 

#define INSPlay_put_SendKeyboardEvents(This,SendKeyboardEvents)	\
    ( (This)->lpVtbl -> put_SendKeyboardEvents(This,SendKeyboardEvents) ) 

#define INSPlay_get_SendMouseClickEvents(This,pSendMouseClickEvents)	\
    ( (This)->lpVtbl -> get_SendMouseClickEvents(This,pSendMouseClickEvents) ) 

#define INSPlay_put_SendMouseClickEvents(This,SendMouseClickEvents)	\
    ( (This)->lpVtbl -> put_SendMouseClickEvents(This,SendMouseClickEvents) ) 

#define INSPlay_get_SendMouseMoveEvents(This,pSendMouseMoveEvents)	\
    ( (This)->lpVtbl -> get_SendMouseMoveEvents(This,pSendMouseMoveEvents) ) 

#define INSPlay_put_SendMouseMoveEvents(This,SendMouseMoveEvents)	\
    ( (This)->lpVtbl -> put_SendMouseMoveEvents(This,SendMouseMoveEvents) ) 

#define INSPlay_get_SendStateChangeEvents(This,pSendStateChangeEvents)	\
    ( (This)->lpVtbl -> get_SendStateChangeEvents(This,pSendStateChangeEvents) ) 

#define INSPlay_put_SendStateChangeEvents(This,SendStateChangeEvents)	\
    ( (This)->lpVtbl -> put_SendStateChangeEvents(This,SendStateChangeEvents) ) 

#define INSPlay_get_ReceivedPackets(This,pReceivedPackets)	\
    ( (This)->lpVtbl -> get_ReceivedPackets(This,pReceivedPackets) ) 

#define INSPlay_get_RecoveredPackets(This,pRecoveredPackets)	\
    ( (This)->lpVtbl -> get_RecoveredPackets(This,pRecoveredPackets) ) 

#define INSPlay_get_LostPackets(This,pLostPackets)	\
    ( (This)->lpVtbl -> get_LostPackets(This,pLostPackets) ) 

#define INSPlay_get_ReceptionQuality(This,pReceptionQuality)	\
    ( (This)->lpVtbl -> get_ReceptionQuality(This,pReceptionQuality) ) 

#define INSPlay_get_BufferingCount(This,pBufferingCount)	\
    ( (This)->lpVtbl -> get_BufferingCount(This,pBufferingCount) ) 

#define INSPlay_get_CursorType(This,pCursorType)	\
    ( (This)->lpVtbl -> get_CursorType(This,pCursorType) ) 

#define INSPlay_put_CursorType(This,CursorType)	\
    ( (This)->lpVtbl -> put_CursorType(This,CursorType) ) 

#define INSPlay_get_AnimationAtStart(This,pAnimationAtStart)	\
    ( (This)->lpVtbl -> get_AnimationAtStart(This,pAnimationAtStart) ) 

#define INSPlay_put_AnimationAtStart(This,AnimationAtStart)	\
    ( (This)->lpVtbl -> put_AnimationAtStart(This,AnimationAtStart) ) 

#define INSPlay_get_AnimationOnStop(This,pAnimationOnStop)	\
    ( (This)->lpVtbl -> get_AnimationOnStop(This,pAnimationOnStop) ) 

#define INSPlay_put_AnimationOnStop(This,AnimationOnStop)	\
    ( (This)->lpVtbl -> put_AnimationOnStop(This,AnimationOnStop) ) 

#define INSPlay_Play(This)	\
    ( (This)->lpVtbl -> Play(This) ) 

#define INSPlay_Pause(This)	\
    ( (This)->lpVtbl -> Pause(This) ) 

#define INSPlay_Stop(This)	\
    ( (This)->lpVtbl -> Stop(This) ) 

#define INSPlay_GetMarkerTime(This,MarkerNum,pMarkerTime)	\
    ( (This)->lpVtbl -> GetMarkerTime(This,MarkerNum,pMarkerTime) ) 

#define INSPlay_GetMarkerName(This,MarkerNum,pbstrMarkerName)	\
    ( (This)->lpVtbl -> GetMarkerName(This,MarkerNum,pbstrMarkerName) ) 


#define INSPlay_get_ChannelName(This,pbstrChannelName)	\
    ( (This)->lpVtbl -> get_ChannelName(This,pbstrChannelName) ) 

#define INSPlay_get_ChannelDescription(This,pbstrChannelDescription)	\
    ( (This)->lpVtbl -> get_ChannelDescription(This,pbstrChannelDescription) ) 

#define INSPlay_get_ChannelURL(This,pbstrChannelURL)	\
    ( (This)->lpVtbl -> get_ChannelURL(This,pbstrChannelURL) ) 

#define INSPlay_get_ContactAddress(This,pbstrContactAddress)	\
    ( (This)->lpVtbl -> get_ContactAddress(This,pbstrContactAddress) ) 

#define INSPlay_get_ContactPhone(This,pbstrContactPhone)	\
    ( (This)->lpVtbl -> get_ContactPhone(This,pbstrContactPhone) ) 

#define INSPlay_get_ContactEmail(This,pbstrContactEmail)	\
    ( (This)->lpVtbl -> get_ContactEmail(This,pbstrContactEmail) ) 

#define INSPlay_get_AllowChangeDisplaySize(This,pAllowChangeDisplaySize)	\
    ( (This)->lpVtbl -> get_AllowChangeDisplaySize(This,pAllowChangeDisplaySize) ) 

#define INSPlay_put_AllowChangeDisplaySize(This,AllowChangeDisplaySize)	\
    ( (This)->lpVtbl -> put_AllowChangeDisplaySize(This,AllowChangeDisplaySize) ) 

#define INSPlay_get_CodecCount(This,pCodecCount)	\
    ( (This)->lpVtbl -> get_CodecCount(This,pCodecCount) ) 

#define INSPlay_get_IsBroadcast(This,pIsBroadcast)	\
    ( (This)->lpVtbl -> get_IsBroadcast(This,pIsBroadcast) ) 

#define INSPlay_get_IsDurationValid(This,pIsDurationValid)	\
    ( (This)->lpVtbl -> get_IsDurationValid(This,pIsDurationValid) ) 

#define INSPlay_get_SourceProtocol(This,pSourceProtocol)	\
    ( (This)->lpVtbl -> get_SourceProtocol(This,pSourceProtocol) ) 

#define INSPlay_get_OpenState(This,pOpenState)	\
    ( (This)->lpVtbl -> get_OpenState(This,pOpenState) ) 

#define INSPlay_get_SendOpenStateChangeEvents(This,pSendOpenStateChangeEvents)	\
    ( (This)->lpVtbl -> get_SendOpenStateChangeEvents(This,pSendOpenStateChangeEvents) ) 

#define INSPlay_put_SendOpenStateChangeEvents(This,SendOpenStateChangeEvents)	\
    ( (This)->lpVtbl -> put_SendOpenStateChangeEvents(This,SendOpenStateChangeEvents) ) 

#define INSPlay_get_SendWarningEvents(This,pSendWarningEvents)	\
    ( (This)->lpVtbl -> get_SendWarningEvents(This,pSendWarningEvents) ) 

#define INSPlay_put_SendWarningEvents(This,SendWarningEvents)	\
    ( (This)->lpVtbl -> put_SendWarningEvents(This,SendWarningEvents) ) 

#define INSPlay_get_SendErrorEvents(This,pSendErrorEvents)	\
    ( (This)->lpVtbl -> get_SendErrorEvents(This,pSendErrorEvents) ) 

#define INSPlay_put_SendErrorEvents(This,SendErrorEvents)	\
    ( (This)->lpVtbl -> put_SendErrorEvents(This,SendErrorEvents) ) 

#define INSPlay_get_HasError(This,pHasError)	\
    ( (This)->lpVtbl -> get_HasError(This,pHasError) ) 

#define INSPlay_get_ErrorDescription(This,pbstrErrorDescription)	\
    ( (This)->lpVtbl -> get_ErrorDescription(This,pbstrErrorDescription) ) 

#define INSPlay_get_ErrorCode(This,pErrorCode)	\
    ( (This)->lpVtbl -> get_ErrorCode(This,pErrorCode) ) 

#define INSPlay_get_PlayState(This,pPlayState)	\
    ( (This)->lpVtbl -> get_PlayState(This,pPlayState) ) 

#define INSPlay_get_SendPlayStateChangeEvents(This,pSendPlayStateChangeEvents)	\
    ( (This)->lpVtbl -> get_SendPlayStateChangeEvents(This,pSendPlayStateChangeEvents) ) 

#define INSPlay_put_SendPlayStateChangeEvents(This,SendPlayStateChangeEvents)	\
    ( (This)->lpVtbl -> put_SendPlayStateChangeEvents(This,SendPlayStateChangeEvents) ) 

#define INSPlay_get_BufferingTime(This,pBufferingTime)	\
    ( (This)->lpVtbl -> get_BufferingTime(This,pBufferingTime) ) 

#define INSPlay_put_BufferingTime(This,BufferingTime)	\
    ( (This)->lpVtbl -> put_BufferingTime(This,BufferingTime) ) 

#define INSPlay_get_UseFixedUDPPort(This,pUseFixedUDPPort)	\
    ( (This)->lpVtbl -> get_UseFixedUDPPort(This,pUseFixedUDPPort) ) 

#define INSPlay_put_UseFixedUDPPort(This,UseFixedUDPPort)	\
    ( (This)->lpVtbl -> put_UseFixedUDPPort(This,UseFixedUDPPort) ) 

#define INSPlay_get_FixedUDPPort(This,pFixedUDPPort)	\
    ( (This)->lpVtbl -> get_FixedUDPPort(This,pFixedUDPPort) ) 

#define INSPlay_put_FixedUDPPort(This,FixedUDPPort)	\
    ( (This)->lpVtbl -> put_FixedUDPPort(This,FixedUDPPort) ) 

#define INSPlay_get_UseHTTPProxy(This,pUseHTTPProxy)	\
    ( (This)->lpVtbl -> get_UseHTTPProxy(This,pUseHTTPProxy) ) 

#define INSPlay_put_UseHTTPProxy(This,UseHTTPProxy)	\
    ( (This)->lpVtbl -> put_UseHTTPProxy(This,UseHTTPProxy) ) 

#define INSPlay_get_EnableAutoProxy(This,pEnableAutoProxy)	\
    ( (This)->lpVtbl -> get_EnableAutoProxy(This,pEnableAutoProxy) ) 

#define INSPlay_put_EnableAutoProxy(This,EnableAutoProxy)	\
    ( (This)->lpVtbl -> put_EnableAutoProxy(This,EnableAutoProxy) ) 

#define INSPlay_get_HTTPProxyHost(This,pbstrHTTPProxyHost)	\
    ( (This)->lpVtbl -> get_HTTPProxyHost(This,pbstrHTTPProxyHost) ) 

#define INSPlay_put_HTTPProxyHost(This,bstrHTTPProxyHost)	\
    ( (This)->lpVtbl -> put_HTTPProxyHost(This,bstrHTTPProxyHost) ) 

#define INSPlay_get_HTTPProxyPort(This,pHTTPProxyPort)	\
    ( (This)->lpVtbl -> get_HTTPProxyPort(This,pHTTPProxyPort) ) 

#define INSPlay_put_HTTPProxyPort(This,HTTPProxyPort)	\
    ( (This)->lpVtbl -> put_HTTPProxyPort(This,HTTPProxyPort) ) 

#define INSPlay_get_EnableMulticast(This,pEnableMulticast)	\
    ( (This)->lpVtbl -> get_EnableMulticast(This,pEnableMulticast) ) 

#define INSPlay_put_EnableMulticast(This,EnableMulticast)	\
    ( (This)->lpVtbl -> put_EnableMulticast(This,EnableMulticast) ) 

#define INSPlay_get_EnableUDP(This,pEnableUDP)	\
    ( (This)->lpVtbl -> get_EnableUDP(This,pEnableUDP) ) 

#define INSPlay_put_EnableUDP(This,EnableUDP)	\
    ( (This)->lpVtbl -> put_EnableUDP(This,EnableUDP) ) 

#define INSPlay_get_EnableTCP(This,pEnableTCP)	\
    ( (This)->lpVtbl -> get_EnableTCP(This,pEnableTCP) ) 

#define INSPlay_put_EnableTCP(This,EnableTCP)	\
    ( (This)->lpVtbl -> put_EnableTCP(This,EnableTCP) ) 

#define INSPlay_get_EnableHTTP(This,pEnableHTTP)	\
    ( (This)->lpVtbl -> get_EnableHTTP(This,pEnableHTTP) ) 

#define INSPlay_put_EnableHTTP(This,EnableHTTP)	\
    ( (This)->lpVtbl -> put_EnableHTTP(This,EnableHTTP) ) 

#define INSPlay_get_BufferingProgress(This,pBufferingProgress)	\
    ( (This)->lpVtbl -> get_BufferingProgress(This,pBufferingProgress) ) 

#define INSPlay_get_BaseURL(This,pbstrBaseURL)	\
    ( (This)->lpVtbl -> get_BaseURL(This,pbstrBaseURL) ) 

#define INSPlay_put_BaseURL(This,bstrBaseURL)	\
    ( (This)->lpVtbl -> put_BaseURL(This,bstrBaseURL) ) 

#define INSPlay_get_DefaultFrame(This,pbstrDefaultFrame)	\
    ( (This)->lpVtbl -> get_DefaultFrame(This,pbstrDefaultFrame) ) 

#define INSPlay_put_DefaultFrame(This,bstrDefaultFrame)	\
    ( (This)->lpVtbl -> put_DefaultFrame(This,bstrDefaultFrame) ) 

#define INSPlay_AboutBox(This)	\
    ( (This)->lpVtbl -> AboutBox(This) ) 

#define INSPlay_Cancel(This)	\
    ( (This)->lpVtbl -> Cancel(This) ) 

#define INSPlay_GetCodecInstalled(This,CodecNum,pCodecInstalled)	\
    ( (This)->lpVtbl -> GetCodecInstalled(This,CodecNum,pCodecInstalled) ) 

#define INSPlay_GetCodecDescription(This,CodecNum,pbstrCodecDescription)	\
    ( (This)->lpVtbl -> GetCodecDescription(This,CodecNum,pbstrCodecDescription) ) 

#define INSPlay_GetCodecURL(This,CodecNum,pbstrCodecURL)	\
    ( (This)->lpVtbl -> GetCodecURL(This,CodecNum,pbstrCodecURL) ) 

#define INSPlay_Open(This,bstrFileName)	\
    ( (This)->lpVtbl -> Open(This,bstrFileName) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE INSPlay_Cancel_Proxy( 
    INSPlay * This);


void __RPC_STUB INSPlay_Cancel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INSPlay_GetCodecInstalled_Proxy( 
    INSPlay * This,
    /* [in] */ long CodecNum,
    /* [retval][out] */ VARIANT_BOOL *pCodecInstalled);


void __RPC_STUB INSPlay_GetCodecInstalled_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INSPlay_GetCodecDescription_Proxy( 
    INSPlay * This,
    /* [in] */ long CodecNum,
    /* [retval][out] */ BSTR *pbstrCodecDescription);


void __RPC_STUB INSPlay_GetCodecDescription_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INSPlay_GetCodecURL_Proxy( 
    INSPlay * This,
    /* [in] */ long CodecNum,
    /* [retval][out] */ BSTR *pbstrCodecURL);


void __RPC_STUB INSPlay_GetCodecURL_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INSPlay_Open_Proxy( 
    INSPlay * This,
    /* [in] */ BSTR bstrFileName);


void __RPC_STUB INSPlay_Open_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __INSPlay_INTERFACE_DEFINED__ */


#ifndef __INSPlay1_INTERFACE_DEFINED__
#define __INSPlay1_INTERFACE_DEFINED__

/* interface INSPlay1 */
/* [hidden][dual][uuid][object] */ 


EXTERN_C const IID IID_INSPlay1;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("265EC141-AE62-11d1-8500-00A0C91F9CA0")
    INSPlay1 : public INSPlay
    {
    public:
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_MediaPlayer( 
            /* [retval][out] */ IDispatch **ppdispatch) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct INSPlay1Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            INSPlay1 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            INSPlay1 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            INSPlay1 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            INSPlay1 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            INSPlay1 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            INSPlay1 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            INSPlay1 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ImageSourceWidth )( 
            INSPlay1 * This,
            /* [retval][out] */ long *pWidth);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ImageSourceHeight )( 
            INSPlay1 * This,
            /* [retval][out] */ long *pHeight);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Duration )( 
            INSPlay1 * This,
            /* [retval][out] */ double *pDuration);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Author )( 
            INSPlay1 * This,
            /* [retval][out] */ BSTR *pbstrAuthor);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Copyright )( 
            INSPlay1 * This,
            /* [retval][out] */ BSTR *pbstrCopyright);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Description )( 
            INSPlay1 * This,
            /* [retval][out] */ BSTR *pbstrDescription);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Rating )( 
            INSPlay1 * This,
            /* [retval][out] */ BSTR *pbstrRating);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Title )( 
            INSPlay1 * This,
            /* [retval][out] */ BSTR *pbstrTitle);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SourceLink )( 
            INSPlay1 * This,
            /* [retval][out] */ BSTR *pbstrSourceLink);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_MarkerCount )( 
            INSPlay1 * This,
            /* [retval][out] */ long *pMarkerCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CanScan )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pCanScan);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CanSeek )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pCanSeek);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CanSeekToMarkers )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pCanSeekToMarkers);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CreationDate )( 
            INSPlay1 * This,
            /* [retval][out] */ DATE *pCreationDate);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Bandwidth )( 
            INSPlay1 * This,
            /* [retval][out] */ long *pBandwidth);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ErrorCorrection )( 
            INSPlay1 * This,
            /* [retval][out] */ BSTR *pbstrErrorCorrection);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AutoStart )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pAutoStart);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AutoStart )( 
            INSPlay1 * This,
            /* [in] */ VARIANT_BOOL AutoStart);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AutoRewind )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pAutoRewind);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AutoRewind )( 
            INSPlay1 * This,
            /* [in] */ VARIANT_BOOL AutoRewind);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_InvokeURLs )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pInvokeURLs);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_InvokeURLs )( 
            INSPlay1 * This,
            /* [in] */ VARIANT_BOOL InvokeURLs);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableContextMenu )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pEnableContextMenu);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableContextMenu )( 
            INSPlay1 * This,
            /* [in] */ VARIANT_BOOL EnableContextMenu);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TransparentAtStart )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pTransparentAtStart);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_TransparentAtStart )( 
            INSPlay1 * This,
            /* [in] */ VARIANT_BOOL TransparentAtStart);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ClickToPlay )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pClickToPlay);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ClickToPlay )( 
            INSPlay1 * This,
            /* [in] */ VARIANT_BOOL ClickToPlay);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_FileName )( 
            INSPlay1 * This,
            /* [retval][out] */ BSTR *pbstrFileName);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_FileName )( 
            INSPlay1 * This,
            /* [in] */ BSTR bstrFileName);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentPosition )( 
            INSPlay1 * This,
            /* [retval][out] */ double *pCurrentPosition);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_CurrentPosition )( 
            INSPlay1 * This,
            /* [in] */ double CurrentPosition);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Rate )( 
            INSPlay1 * This,
            /* [retval][out] */ double *pRate);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Rate )( 
            INSPlay1 * This,
            /* [in] */ double Rate);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentMarker )( 
            INSPlay1 * This,
            /* [retval][out] */ long *pCurrentMarker);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_CurrentMarker )( 
            INSPlay1 * This,
            /* [in] */ long CurrentMarker);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlayCount )( 
            INSPlay1 * This,
            /* [retval][out] */ long *pPlayCount);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_PlayCount )( 
            INSPlay1 * This,
            /* [in] */ long PlayCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentState )( 
            INSPlay1 * This,
            /* [retval][out] */ long *pCurrentState);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_DisplaySize )( 
            INSPlay1 * This,
            /* [retval][out] */ long *pDisplaySize);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_DisplaySize )( 
            INSPlay1 * This,
            /* [in] */ long DisplaySize);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_MainWindow )( 
            INSPlay1 * This,
            /* [retval][out] */ long *pMainWindow);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AllowScan )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pAllowScan);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AllowScan )( 
            INSPlay1 * This,
            /* [in] */ VARIANT_BOOL AllowScan);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendKeyboardEvents )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pSendKeyboardEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendKeyboardEvents )( 
            INSPlay1 * This,
            /* [in] */ VARIANT_BOOL SendKeyboardEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendMouseClickEvents )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pSendMouseClickEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendMouseClickEvents )( 
            INSPlay1 * This,
            /* [in] */ VARIANT_BOOL SendMouseClickEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendMouseMoveEvents )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pSendMouseMoveEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendMouseMoveEvents )( 
            INSPlay1 * This,
            /* [in] */ VARIANT_BOOL SendMouseMoveEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendStateChangeEvents )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pSendStateChangeEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendStateChangeEvents )( 
            INSPlay1 * This,
            /* [in] */ VARIANT_BOOL SendStateChangeEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ReceivedPackets )( 
            INSPlay1 * This,
            /* [retval][out] */ long *pReceivedPackets);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_RecoveredPackets )( 
            INSPlay1 * This,
            /* [retval][out] */ long *pRecoveredPackets);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LostPackets )( 
            INSPlay1 * This,
            /* [retval][out] */ long *pLostPackets);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ReceptionQuality )( 
            INSPlay1 * This,
            /* [retval][out] */ long *pReceptionQuality);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_BufferingCount )( 
            INSPlay1 * This,
            /* [retval][out] */ long *pBufferingCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CursorType )( 
            INSPlay1 * This,
            /* [retval][out] */ long *pCursorType);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_CursorType )( 
            INSPlay1 * This,
            /* [in] */ long CursorType);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AnimationAtStart )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pAnimationAtStart);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AnimationAtStart )( 
            INSPlay1 * This,
            /* [in] */ VARIANT_BOOL AnimationAtStart);
        
        /* [hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AnimationOnStop )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pAnimationOnStop);
        
        /* [hidden][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AnimationOnStop )( 
            INSPlay1 * This,
            /* [in] */ VARIANT_BOOL AnimationOnStop);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Play )( 
            INSPlay1 * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Pause )( 
            INSPlay1 * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Stop )( 
            INSPlay1 * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetMarkerTime )( 
            INSPlay1 * This,
            /* [in] */ long MarkerNum,
            /* [retval][out] */ double *pMarkerTime);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetMarkerName )( 
            INSPlay1 * This,
            /* [in] */ long MarkerNum,
            /* [retval][out] */ BSTR *pbstrMarkerName);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ChannelName )( 
            INSPlay1 * This,
            /* [retval][out] */ BSTR *pbstrChannelName);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ChannelDescription )( 
            INSPlay1 * This,
            /* [retval][out] */ BSTR *pbstrChannelDescription);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ChannelURL )( 
            INSPlay1 * This,
            /* [retval][out] */ BSTR *pbstrChannelURL);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ContactAddress )( 
            INSPlay1 * This,
            /* [retval][out] */ BSTR *pbstrContactAddress);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ContactPhone )( 
            INSPlay1 * This,
            /* [retval][out] */ BSTR *pbstrContactPhone);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ContactEmail )( 
            INSPlay1 * This,
            /* [retval][out] */ BSTR *pbstrContactEmail);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AllowChangeDisplaySize )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pAllowChangeDisplaySize);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AllowChangeDisplaySize )( 
            INSPlay1 * This,
            /* [in] */ VARIANT_BOOL AllowChangeDisplaySize);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CodecCount )( 
            INSPlay1 * This,
            /* [retval][out] */ long *pCodecCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_IsBroadcast )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pIsBroadcast);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_IsDurationValid )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pIsDurationValid);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SourceProtocol )( 
            INSPlay1 * This,
            /* [retval][out] */ long *pSourceProtocol);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OpenState )( 
            INSPlay1 * This,
            /* [retval][out] */ long *pOpenState);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendOpenStateChangeEvents )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pSendOpenStateChangeEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendOpenStateChangeEvents )( 
            INSPlay1 * This,
            /* [in] */ VARIANT_BOOL SendOpenStateChangeEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendWarningEvents )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pSendWarningEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendWarningEvents )( 
            INSPlay1 * This,
            /* [in] */ VARIANT_BOOL SendWarningEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendErrorEvents )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pSendErrorEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendErrorEvents )( 
            INSPlay1 * This,
            /* [in] */ VARIANT_BOOL SendErrorEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HasError )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pHasError);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ErrorDescription )( 
            INSPlay1 * This,
            /* [retval][out] */ BSTR *pbstrErrorDescription);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ErrorCode )( 
            INSPlay1 * This,
            /* [retval][out] */ long *pErrorCode);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlayState )( 
            INSPlay1 * This,
            /* [retval][out] */ long *pPlayState);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SendPlayStateChangeEvents )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pSendPlayStateChangeEvents);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SendPlayStateChangeEvents )( 
            INSPlay1 * This,
            /* [in] */ VARIANT_BOOL SendPlayStateChangeEvents);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_BufferingTime )( 
            INSPlay1 * This,
            /* [retval][out] */ double *pBufferingTime);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_BufferingTime )( 
            INSPlay1 * This,
            /* [in] */ double BufferingTime);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_UseFixedUDPPort )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pUseFixedUDPPort);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_UseFixedUDPPort )( 
            INSPlay1 * This,
            /* [in] */ VARIANT_BOOL UseFixedUDPPort);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_FixedUDPPort )( 
            INSPlay1 * This,
            /* [retval][out] */ long *pFixedUDPPort);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_FixedUDPPort )( 
            INSPlay1 * This,
            /* [in] */ long FixedUDPPort);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_UseHTTPProxy )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pUseHTTPProxy);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_UseHTTPProxy )( 
            INSPlay1 * This,
            /* [in] */ VARIANT_BOOL UseHTTPProxy);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableAutoProxy )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pEnableAutoProxy);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableAutoProxy )( 
            INSPlay1 * This,
            /* [in] */ VARIANT_BOOL EnableAutoProxy);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HTTPProxyHost )( 
            INSPlay1 * This,
            /* [retval][out] */ BSTR *pbstrHTTPProxyHost);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_HTTPProxyHost )( 
            INSPlay1 * This,
            /* [in] */ BSTR bstrHTTPProxyHost);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HTTPProxyPort )( 
            INSPlay1 * This,
            /* [retval][out] */ long *pHTTPProxyPort);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_HTTPProxyPort )( 
            INSPlay1 * This,
            /* [in] */ long HTTPProxyPort);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableMulticast )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pEnableMulticast);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableMulticast )( 
            INSPlay1 * This,
            /* [in] */ VARIANT_BOOL EnableMulticast);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableUDP )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pEnableUDP);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableUDP )( 
            INSPlay1 * This,
            /* [in] */ VARIANT_BOOL EnableUDP);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableTCP )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pEnableTCP);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableTCP )( 
            INSPlay1 * This,
            /* [in] */ VARIANT_BOOL EnableTCP);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EnableHTTP )( 
            INSPlay1 * This,
            /* [retval][out] */ VARIANT_BOOL *pEnableHTTP);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableHTTP )( 
            INSPlay1 * This,
            /* [in] */ VARIANT_BOOL EnableHTTP);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_BufferingProgress )( 
            INSPlay1 * This,
            /* [retval][out] */ long *pBufferingProgress);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_BaseURL )( 
            INSPlay1 * This,
            /* [retval][out] */ BSTR *pbstrBaseURL);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_BaseURL )( 
            INSPlay1 * This,
            /* [in] */ BSTR bstrBaseURL);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultFrame )( 
            INSPlay1 * This,
            /* [retval][out] */ BSTR *pbstrDefaultFrame);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_DefaultFrame )( 
            INSPlay1 * This,
            /* [in] */ BSTR bstrDefaultFrame);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *AboutBox )( 
            INSPlay1 * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Cancel )( 
            INSPlay1 * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetCodecInstalled )( 
            INSPlay1 * This,
            /* [in] */ long CodecNum,
            /* [retval][out] */ VARIANT_BOOL *pCodecInstalled);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetCodecDescription )( 
            INSPlay1 * This,
            /* [in] */ long CodecNum,
            /* [retval][out] */ BSTR *pbstrCodecDescription);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetCodecURL )( 
            INSPlay1 * This,
            /* [in] */ long CodecNum,
            /* [retval][out] */ BSTR *pbstrCodecURL);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Open )( 
            INSPlay1 * This,
            /* [in] */ BSTR bstrFileName);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MediaPlayer )( 
            INSPlay1 * This,
            /* [retval][out] */ IDispatch **ppdispatch);
        
        END_INTERFACE
    } INSPlay1Vtbl;

    interface INSPlay1
    {
        CONST_VTBL struct INSPlay1Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define INSPlay1_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define INSPlay1_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define INSPlay1_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define INSPlay1_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define INSPlay1_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define INSPlay1_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define INSPlay1_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define INSPlay1_get_ImageSourceWidth(This,pWidth)	\
    ( (This)->lpVtbl -> get_ImageSourceWidth(This,pWidth) ) 

#define INSPlay1_get_ImageSourceHeight(This,pHeight)	\
    ( (This)->lpVtbl -> get_ImageSourceHeight(This,pHeight) ) 

#define INSPlay1_get_Duration(This,pDuration)	\
    ( (This)->lpVtbl -> get_Duration(This,pDuration) ) 

#define INSPlay1_get_Author(This,pbstrAuthor)	\
    ( (This)->lpVtbl -> get_Author(This,pbstrAuthor) ) 

#define INSPlay1_get_Copyright(This,pbstrCopyright)	\
    ( (This)->lpVtbl -> get_Copyright(This,pbstrCopyright) ) 

#define INSPlay1_get_Description(This,pbstrDescription)	\
    ( (This)->lpVtbl -> get_Description(This,pbstrDescription) ) 

#define INSPlay1_get_Rating(This,pbstrRating)	\
    ( (This)->lpVtbl -> get_Rating(This,pbstrRating) ) 

#define INSPlay1_get_Title(This,pbstrTitle)	\
    ( (This)->lpVtbl -> get_Title(This,pbstrTitle) ) 

#define INSPlay1_get_SourceLink(This,pbstrSourceLink)	\
    ( (This)->lpVtbl -> get_SourceLink(This,pbstrSourceLink) ) 

#define INSPlay1_get_MarkerCount(This,pMarkerCount)	\
    ( (This)->lpVtbl -> get_MarkerCount(This,pMarkerCount) ) 

#define INSPlay1_get_CanScan(This,pCanScan)	\
    ( (This)->lpVtbl -> get_CanScan(This,pCanScan) ) 

#define INSPlay1_get_CanSeek(This,pCanSeek)	\
    ( (This)->lpVtbl -> get_CanSeek(This,pCanSeek) ) 

#define INSPlay1_get_CanSeekToMarkers(This,pCanSeekToMarkers)	\
    ( (This)->lpVtbl -> get_CanSeekToMarkers(This,pCanSeekToMarkers) ) 

#define INSPlay1_get_CreationDate(This,pCreationDate)	\
    ( (This)->lpVtbl -> get_CreationDate(This,pCreationDate) ) 

#define INSPlay1_get_Bandwidth(This,pBandwidth)	\
    ( (This)->lpVtbl -> get_Bandwidth(This,pBandwidth) ) 

#define INSPlay1_get_ErrorCorrection(This,pbstrErrorCorrection)	\
    ( (This)->lpVtbl -> get_ErrorCorrection(This,pbstrErrorCorrection) ) 

#define INSPlay1_get_AutoStart(This,pAutoStart)	\
    ( (This)->lpVtbl -> get_AutoStart(This,pAutoStart) ) 

#define INSPlay1_put_AutoStart(This,AutoStart)	\
    ( (This)->lpVtbl -> put_AutoStart(This,AutoStart) ) 

#define INSPlay1_get_AutoRewind(This,pAutoRewind)	\
    ( (This)->lpVtbl -> get_AutoRewind(This,pAutoRewind) ) 

#define INSPlay1_put_AutoRewind(This,AutoRewind)	\
    ( (This)->lpVtbl -> put_AutoRewind(This,AutoRewind) ) 

#define INSPlay1_get_InvokeURLs(This,pInvokeURLs)	\
    ( (This)->lpVtbl -> get_InvokeURLs(This,pInvokeURLs) ) 

#define INSPlay1_put_InvokeURLs(This,InvokeURLs)	\
    ( (This)->lpVtbl -> put_InvokeURLs(This,InvokeURLs) ) 

#define INSPlay1_get_EnableContextMenu(This,pEnableContextMenu)	\
    ( (This)->lpVtbl -> get_EnableContextMenu(This,pEnableContextMenu) ) 

#define INSPlay1_put_EnableContextMenu(This,EnableContextMenu)	\
    ( (This)->lpVtbl -> put_EnableContextMenu(This,EnableContextMenu) ) 

#define INSPlay1_get_TransparentAtStart(This,pTransparentAtStart)	\
    ( (This)->lpVtbl -> get_TransparentAtStart(This,pTransparentAtStart) ) 

#define INSPlay1_put_TransparentAtStart(This,TransparentAtStart)	\
    ( (This)->lpVtbl -> put_TransparentAtStart(This,TransparentAtStart) ) 

#define INSPlay1_get_ClickToPlay(This,pClickToPlay)	\
    ( (This)->lpVtbl -> get_ClickToPlay(This,pClickToPlay) ) 

#define INSPlay1_put_ClickToPlay(This,ClickToPlay)	\
    ( (This)->lpVtbl -> put_ClickToPlay(This,ClickToPlay) ) 

#define INSPlay1_get_FileName(This,pbstrFileName)	\
    ( (This)->lpVtbl -> get_FileName(This,pbstrFileName) ) 

#define INSPlay1_put_FileName(This,bstrFileName)	\
    ( (This)->lpVtbl -> put_FileName(This,bstrFileName) ) 

#define INSPlay1_get_CurrentPosition(This,pCurrentPosition)	\
    ( (This)->lpVtbl -> get_CurrentPosition(This,pCurrentPosition) ) 

#define INSPlay1_put_CurrentPosition(This,CurrentPosition)	\
    ( (This)->lpVtbl -> put_CurrentPosition(This,CurrentPosition) ) 

#define INSPlay1_get_Rate(This,pRate)	\
    ( (This)->lpVtbl -> get_Rate(This,pRate) ) 

#define INSPlay1_put_Rate(This,Rate)	\
    ( (This)->lpVtbl -> put_Rate(This,Rate) ) 

#define INSPlay1_get_CurrentMarker(This,pCurrentMarker)	\
    ( (This)->lpVtbl -> get_CurrentMarker(This,pCurrentMarker) ) 

#define INSPlay1_put_CurrentMarker(This,CurrentMarker)	\
    ( (This)->lpVtbl -> put_CurrentMarker(This,CurrentMarker) ) 

#define INSPlay1_get_PlayCount(This,pPlayCount)	\
    ( (This)->lpVtbl -> get_PlayCount(This,pPlayCount) ) 

#define INSPlay1_put_PlayCount(This,PlayCount)	\
    ( (This)->lpVtbl -> put_PlayCount(This,PlayCount) ) 

#define INSPlay1_get_CurrentState(This,pCurrentState)	\
    ( (This)->lpVtbl -> get_CurrentState(This,pCurrentState) ) 

#define INSPlay1_get_DisplaySize(This,pDisplaySize)	\
    ( (This)->lpVtbl -> get_DisplaySize(This,pDisplaySize) ) 

#define INSPlay1_put_DisplaySize(This,DisplaySize)	\
    ( (This)->lpVtbl -> put_DisplaySize(This,DisplaySize) ) 

#define INSPlay1_get_MainWindow(This,pMainWindow)	\
    ( (This)->lpVtbl -> get_MainWindow(This,pMainWindow) ) 

#define INSPlay1_get_AllowScan(This,pAllowScan)	\
    ( (This)->lpVtbl -> get_AllowScan(This,pAllowScan) ) 

#define INSPlay1_put_AllowScan(This,AllowScan)	\
    ( (This)->lpVtbl -> put_AllowScan(This,AllowScan) ) 

#define INSPlay1_get_SendKeyboardEvents(This,pSendKeyboardEvents)	\
    ( (This)->lpVtbl -> get_SendKeyboardEvents(This,pSendKeyboardEvents) ) 

#define INSPlay1_put_SendKeyboardEvents(This,SendKeyboardEvents)	\
    ( (This)->lpVtbl -> put_SendKeyboardEvents(This,SendKeyboardEvents) ) 

#define INSPlay1_get_SendMouseClickEvents(This,pSendMouseClickEvents)	\
    ( (This)->lpVtbl -> get_SendMouseClickEvents(This,pSendMouseClickEvents) ) 

#define INSPlay1_put_SendMouseClickEvents(This,SendMouseClickEvents)	\
    ( (This)->lpVtbl -> put_SendMouseClickEvents(This,SendMouseClickEvents) ) 

#define INSPlay1_get_SendMouseMoveEvents(This,pSendMouseMoveEvents)	\
    ( (This)->lpVtbl -> get_SendMouseMoveEvents(This,pSendMouseMoveEvents) ) 

#define INSPlay1_put_SendMouseMoveEvents(This,SendMouseMoveEvents)	\
    ( (This)->lpVtbl -> put_SendMouseMoveEvents(This,SendMouseMoveEvents) ) 

#define INSPlay1_get_SendStateChangeEvents(This,pSendStateChangeEvents)	\
    ( (This)->lpVtbl -> get_SendStateChangeEvents(This,pSendStateChangeEvents) ) 

#define INSPlay1_put_SendStateChangeEvents(This,SendStateChangeEvents)	\
    ( (This)->lpVtbl -> put_SendStateChangeEvents(This,SendStateChangeEvents) ) 

#define INSPlay1_get_ReceivedPackets(This,pReceivedPackets)	\
    ( (This)->lpVtbl -> get_ReceivedPackets(This,pReceivedPackets) ) 

#define INSPlay1_get_RecoveredPackets(This,pRecoveredPackets)	\
    ( (This)->lpVtbl -> get_RecoveredPackets(This,pRecoveredPackets) ) 

#define INSPlay1_get_LostPackets(This,pLostPackets)	\
    ( (This)->lpVtbl -> get_LostPackets(This,pLostPackets) ) 

#define INSPlay1_get_ReceptionQuality(This,pReceptionQuality)	\
    ( (This)->lpVtbl -> get_ReceptionQuality(This,pReceptionQuality) ) 

#define INSPlay1_get_BufferingCount(This,pBufferingCount)	\
    ( (This)->lpVtbl -> get_BufferingCount(This,pBufferingCount) ) 

#define INSPlay1_get_CursorType(This,pCursorType)	\
    ( (This)->lpVtbl -> get_CursorType(This,pCursorType) ) 

#define INSPlay1_put_CursorType(This,CursorType)	\
    ( (This)->lpVtbl -> put_CursorType(This,CursorType) ) 

#define INSPlay1_get_AnimationAtStart(This,pAnimationAtStart)	\
    ( (This)->lpVtbl -> get_AnimationAtStart(This,pAnimationAtStart) ) 

#define INSPlay1_put_AnimationAtStart(This,AnimationAtStart)	\
    ( (This)->lpVtbl -> put_AnimationAtStart(This,AnimationAtStart) ) 

#define INSPlay1_get_AnimationOnStop(This,pAnimationOnStop)	\
    ( (This)->lpVtbl -> get_AnimationOnStop(This,pAnimationOnStop) ) 

#define INSPlay1_put_AnimationOnStop(This,AnimationOnStop)	\
    ( (This)->lpVtbl -> put_AnimationOnStop(This,AnimationOnStop) ) 

#define INSPlay1_Play(This)	\
    ( (This)->lpVtbl -> Play(This) ) 

#define INSPlay1_Pause(This)	\
    ( (This)->lpVtbl -> Pause(This) ) 

#define INSPlay1_Stop(This)	\
    ( (This)->lpVtbl -> Stop(This) ) 

#define INSPlay1_GetMarkerTime(This,MarkerNum,pMarkerTime)	\
    ( (This)->lpVtbl -> GetMarkerTime(This,MarkerNum,pMarkerTime) ) 

#define INSPlay1_GetMarkerName(This,MarkerNum,pbstrMarkerName)	\
    ( (This)->lpVtbl -> GetMarkerName(This,MarkerNum,pbstrMarkerName) ) 


#define INSPlay1_get_ChannelName(This,pbstrChannelName)	\
    ( (This)->lpVtbl -> get_ChannelName(This,pbstrChannelName) ) 

#define INSPlay1_get_ChannelDescription(This,pbstrChannelDescription)	\
    ( (This)->lpVtbl -> get_ChannelDescription(This,pbstrChannelDescription) ) 

#define INSPlay1_get_ChannelURL(This,pbstrChannelURL)	\
    ( (This)->lpVtbl -> get_ChannelURL(This,pbstrChannelURL) ) 

#define INSPlay1_get_ContactAddress(This,pbstrContactAddress)	\
    ( (This)->lpVtbl -> get_ContactAddress(This,pbstrContactAddress) ) 

#define INSPlay1_get_ContactPhone(This,pbstrContactPhone)	\
    ( (This)->lpVtbl -> get_ContactPhone(This,pbstrContactPhone) ) 

#define INSPlay1_get_ContactEmail(This,pbstrContactEmail)	\
    ( (This)->lpVtbl -> get_ContactEmail(This,pbstrContactEmail) ) 

#define INSPlay1_get_AllowChangeDisplaySize(This,pAllowChangeDisplaySize)	\
    ( (This)->lpVtbl -> get_AllowChangeDisplaySize(This,pAllowChangeDisplaySize) ) 

#define INSPlay1_put_AllowChangeDisplaySize(This,AllowChangeDisplaySize)	\
    ( (This)->lpVtbl -> put_AllowChangeDisplaySize(This,AllowChangeDisplaySize) ) 

#define INSPlay1_get_CodecCount(This,pCodecCount)	\
    ( (This)->lpVtbl -> get_CodecCount(This,pCodecCount) ) 

#define INSPlay1_get_IsBroadcast(This,pIsBroadcast)	\
    ( (This)->lpVtbl -> get_IsBroadcast(This,pIsBroadcast) ) 

#define INSPlay1_get_IsDurationValid(This,pIsDurationValid)	\
    ( (This)->lpVtbl -> get_IsDurationValid(This,pIsDurationValid) ) 

#define INSPlay1_get_SourceProtocol(This,pSourceProtocol)	\
    ( (This)->lpVtbl -> get_SourceProtocol(This,pSourceProtocol) ) 

#define INSPlay1_get_OpenState(This,pOpenState)	\
    ( (This)->lpVtbl -> get_OpenState(This,pOpenState) ) 

#define INSPlay1_get_SendOpenStateChangeEvents(This,pSendOpenStateChangeEvents)	\
    ( (This)->lpVtbl -> get_SendOpenStateChangeEvents(This,pSendOpenStateChangeEvents) ) 

#define INSPlay1_put_SendOpenStateChangeEvents(This,SendOpenStateChangeEvents)	\
    ( (This)->lpVtbl -> put_SendOpenStateChangeEvents(This,SendOpenStateChangeEvents) ) 

#define INSPlay1_get_SendWarningEvents(This,pSendWarningEvents)	\
    ( (This)->lpVtbl -> get_SendWarningEvents(This,pSendWarningEvents) ) 

#define INSPlay1_put_SendWarningEvents(This,SendWarningEvents)	\
    ( (This)->lpVtbl -> put_SendWarningEvents(This,SendWarningEvents) ) 

#define INSPlay1_get_SendErrorEvents(This,pSendErrorEvents)	\
    ( (This)->lpVtbl -> get_SendErrorEvents(This,pSendErrorEvents) ) 

#define INSPlay1_put_SendErrorEvents(This,SendErrorEvents)	\
    ( (This)->lpVtbl -> put_SendErrorEvents(This,SendErrorEvents) ) 

#define INSPlay1_get_HasError(This,pHasError)	\
    ( (This)->lpVtbl -> get_HasError(This,pHasError) ) 

#define INSPlay1_get_ErrorDescription(This,pbstrErrorDescription)	\
    ( (This)->lpVtbl -> get_ErrorDescription(This,pbstrErrorDescription) ) 

#define INSPlay1_get_ErrorCode(This,pErrorCode)	\
    ( (This)->lpVtbl -> get_ErrorCode(This,pErrorCode) ) 

#define INSPlay1_get_PlayState(This,pPlayState)	\
    ( (This)->lpVtbl -> get_PlayState(This,pPlayState) ) 

#define INSPlay1_get_SendPlayStateChangeEvents(This,pSendPlayStateChangeEvents)	\
    ( (This)->lpVtbl -> get_SendPlayStateChangeEvents(This,pSendPlayStateChangeEvents) ) 

#define INSPlay1_put_SendPlayStateChangeEvents(This,SendPlayStateChangeEvents)	\
    ( (This)->lpVtbl -> put_SendPlayStateChangeEvents(This,SendPlayStateChangeEvents) ) 

#define INSPlay1_get_BufferingTime(This,pBufferingTime)	\
    ( (This)->lpVtbl -> get_BufferingTime(This,pBufferingTime) ) 

#define INSPlay1_put_BufferingTime(This,BufferingTime)	\
    ( (This)->lpVtbl -> put_BufferingTime(This,BufferingTime) ) 

#define INSPlay1_get_UseFixedUDPPort(This,pUseFixedUDPPort)	\
    ( (This)->lpVtbl -> get_UseFixedUDPPort(This,pUseFixedUDPPort) ) 

#define INSPlay1_put_UseFixedUDPPort(This,UseFixedUDPPort)	\
    ( (This)->lpVtbl -> put_UseFixedUDPPort(This,UseFixedUDPPort) ) 

#define INSPlay1_get_FixedUDPPort(This,pFixedUDPPort)	\
    ( (This)->lpVtbl -> get_FixedUDPPort(This,pFixedUDPPort) ) 

#define INSPlay1_put_FixedUDPPort(This,FixedUDPPort)	\
    ( (This)->lpVtbl -> put_FixedUDPPort(This,FixedUDPPort) ) 

#define INSPlay1_get_UseHTTPProxy(This,pUseHTTPProxy)	\
    ( (This)->lpVtbl -> get_UseHTTPProxy(This,pUseHTTPProxy) ) 

#define INSPlay1_put_UseHTTPProxy(This,UseHTTPProxy)	\
    ( (This)->lpVtbl -> put_UseHTTPProxy(This,UseHTTPProxy) ) 

#define INSPlay1_get_EnableAutoProxy(This,pEnableAutoProxy)	\
    ( (This)->lpVtbl -> get_EnableAutoProxy(This,pEnableAutoProxy) ) 

#define INSPlay1_put_EnableAutoProxy(This,EnableAutoProxy)	\
    ( (This)->lpVtbl -> put_EnableAutoProxy(This,EnableAutoProxy) ) 

#define INSPlay1_get_HTTPProxyHost(This,pbstrHTTPProxyHost)	\
    ( (This)->lpVtbl -> get_HTTPProxyHost(This,pbstrHTTPProxyHost) ) 

#define INSPlay1_put_HTTPProxyHost(This,bstrHTTPProxyHost)	\
    ( (This)->lpVtbl -> put_HTTPProxyHost(This,bstrHTTPProxyHost) ) 

#define INSPlay1_get_HTTPProxyPort(This,pHTTPProxyPort)	\
    ( (This)->lpVtbl -> get_HTTPProxyPort(This,pHTTPProxyPort) ) 

#define INSPlay1_put_HTTPProxyPort(This,HTTPProxyPort)	\
    ( (This)->lpVtbl -> put_HTTPProxyPort(This,HTTPProxyPort) ) 

#define INSPlay1_get_EnableMulticast(This,pEnableMulticast)	\
    ( (This)->lpVtbl -> get_EnableMulticast(This,pEnableMulticast) ) 

#define INSPlay1_put_EnableMulticast(This,EnableMulticast)	\
    ( (This)->lpVtbl -> put_EnableMulticast(This,EnableMulticast) ) 

#define INSPlay1_get_EnableUDP(This,pEnableUDP)	\
    ( (This)->lpVtbl -> get_EnableUDP(This,pEnableUDP) ) 

#define INSPlay1_put_EnableUDP(This,EnableUDP)	\
    ( (This)->lpVtbl -> put_EnableUDP(This,EnableUDP) ) 

#define INSPlay1_get_EnableTCP(This,pEnableTCP)	\
    ( (This)->lpVtbl -> get_EnableTCP(This,pEnableTCP) ) 

#define INSPlay1_put_EnableTCP(This,EnableTCP)	\
    ( (This)->lpVtbl -> put_EnableTCP(This,EnableTCP) ) 

#define INSPlay1_get_EnableHTTP(This,pEnableHTTP)	\
    ( (This)->lpVtbl -> get_EnableHTTP(This,pEnableHTTP) ) 

#define INSPlay1_put_EnableHTTP(This,EnableHTTP)	\
    ( (This)->lpVtbl -> put_EnableHTTP(This,EnableHTTP) ) 

#define INSPlay1_get_BufferingProgress(This,pBufferingProgress)	\
    ( (This)->lpVtbl -> get_BufferingProgress(This,pBufferingProgress) ) 

#define INSPlay1_get_BaseURL(This,pbstrBaseURL)	\
    ( (This)->lpVtbl -> get_BaseURL(This,pbstrBaseURL) ) 

#define INSPlay1_put_BaseURL(This,bstrBaseURL)	\
    ( (This)->lpVtbl -> put_BaseURL(This,bstrBaseURL) ) 

#define INSPlay1_get_DefaultFrame(This,pbstrDefaultFrame)	\
    ( (This)->lpVtbl -> get_DefaultFrame(This,pbstrDefaultFrame) ) 

#define INSPlay1_put_DefaultFrame(This,bstrDefaultFrame)	\
    ( (This)->lpVtbl -> put_DefaultFrame(This,bstrDefaultFrame) ) 

#define INSPlay1_AboutBox(This)	\
    ( (This)->lpVtbl -> AboutBox(This) ) 

#define INSPlay1_Cancel(This)	\
    ( (This)->lpVtbl -> Cancel(This) ) 

#define INSPlay1_GetCodecInstalled(This,CodecNum,pCodecInstalled)	\
    ( (This)->lpVtbl -> GetCodecInstalled(This,CodecNum,pCodecInstalled) ) 

#define INSPlay1_GetCodecDescription(This,CodecNum,pbstrCodecDescription)	\
    ( (This)->lpVtbl -> GetCodecDescription(This,CodecNum,pbstrCodecDescription) ) 

#define INSPlay1_GetCodecURL(This,CodecNum,pbstrCodecURL)	\
    ( (This)->lpVtbl -> GetCodecURL(This,CodecNum,pbstrCodecURL) ) 

#define INSPlay1_Open(This,bstrFileName)	\
    ( (This)->lpVtbl -> Open(This,bstrFileName) ) 


#define INSPlay1_get_MediaPlayer(This,ppdispatch)	\
    ( (This)->lpVtbl -> get_MediaPlayer(This,ppdispatch) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id][propget] */ HRESULT STDMETHODCALLTYPE INSPlay1_get_MediaPlayer_Proxy( 
    INSPlay1 * This,
    /* [retval][out] */ IDispatch **ppdispatch);


void __RPC_STUB INSPlay1_get_MediaPlayer_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __INSPlay1_INTERFACE_DEFINED__ */


/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


