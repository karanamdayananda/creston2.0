

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0543 */
/* Compiler settings for bmedia.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __bmedia_h__
#define __bmedia_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IServiceProvider_FWD_DEFINED__
#define __IServiceProvider_FWD_DEFINED__
typedef interface IServiceProvider IServiceProvider;
#endif 	/* __IServiceProvider_FWD_DEFINED__ */


#ifndef __IAsyncPProt_FWD_DEFINED__
#define __IAsyncPProt_FWD_DEFINED__
typedef interface IAsyncPProt IAsyncPProt;
#endif 	/* __IAsyncPProt_FWD_DEFINED__ */


#ifndef __IAsyncMHandler_FWD_DEFINED__
#define __IAsyncMHandler_FWD_DEFINED__
typedef interface IAsyncMHandler IAsyncMHandler;
#endif 	/* __IAsyncMHandler_FWD_DEFINED__ */


#ifndef __IDirectControl_FWD_DEFINED__
#define __IDirectControl_FWD_DEFINED__
typedef interface IDirectControl IDirectControl;
#endif 	/* __IDirectControl_FWD_DEFINED__ */


#ifndef __IDirectControlView_FWD_DEFINED__
#define __IDirectControlView_FWD_DEFINED__
typedef interface IDirectControlView IDirectControlView;
#endif 	/* __IDirectControlView_FWD_DEFINED__ */


#ifndef __IDirectContainer_FWD_DEFINED__
#define __IDirectContainer_FWD_DEFINED__
typedef interface IDirectContainer IDirectContainer;
#endif 	/* __IDirectContainer_FWD_DEFINED__ */


#ifndef __IRadioView_FWD_DEFINED__
#define __IRadioView_FWD_DEFINED__
typedef interface IRadioView IRadioView;
#endif 	/* __IRadioView_FWD_DEFINED__ */


#ifndef __IRadioPlayer_FWD_DEFINED__
#define __IRadioPlayer_FWD_DEFINED__
typedef interface IRadioPlayer IRadioPlayer;
#endif 	/* __IRadioPlayer_FWD_DEFINED__ */


#ifndef __IRadioServer_FWD_DEFINED__
#define __IRadioServer_FWD_DEFINED__
typedef interface IRadioServer IRadioServer;
#endif 	/* __IRadioServer_FWD_DEFINED__ */


#ifndef __IRadioServerControl_FWD_DEFINED__
#define __IRadioServerControl_FWD_DEFINED__
typedef interface IRadioServerControl IRadioServerControl;
#endif 	/* __IRadioServerControl_FWD_DEFINED__ */


#ifndef __IRadioBand_FWD_DEFINED__
#define __IRadioBand_FWD_DEFINED__
typedef interface IRadioBand IRadioBand;
#endif 	/* __IRadioBand_FWD_DEFINED__ */


#ifndef __IMediaBindStream_FWD_DEFINED__
#define __IMediaBindStream_FWD_DEFINED__
typedef interface IMediaBindStream IMediaBindStream;
#endif 	/* __IMediaBindStream_FWD_DEFINED__ */


#ifndef __IMediaPlayerListener_FWD_DEFINED__
#define __IMediaPlayerListener_FWD_DEFINED__
typedef interface IMediaPlayerListener IMediaPlayerListener;
#endif 	/* __IMediaPlayerListener_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


/* interface __MIDL_itf_bmedia_0000_0000 */
/* [local] */ 

//=--------------------------------------------------------------------------=
// ServProv.h
//=--------------------------------------------------------------------------=
// (C) Copyright 1995-1999 Microsoft Corporation.  All Rights Reserved.
//
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//=--------------------------------------------------------------------------=

#pragma comment(lib,"uuid.lib")

//---------------------------------------------------------------------------=
// IServiceProvider Interfaces.




extern RPC_IF_HANDLE __MIDL_itf_bmedia_0000_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_bmedia_0000_0000_v0_0_s_ifspec;

#ifndef __IServiceProvider_INTERFACE_DEFINED__
#define __IServiceProvider_INTERFACE_DEFINED__

/* interface IServiceProvider */
/* [unique][uuid][object] */ 

typedef /* [unique] */ IServiceProvider *LPSERVICEPROVIDER;

#if (_MSC_VER >= 1100) && defined(__cplusplus) && !defined(CINTERFACE)
    EXTERN_C const IID IID_IServiceProvider;
    extern "C++"
    {
        MIDL_INTERFACE("6d5140c1-7436-11ce-8034-00aa006009fa")
        IServiceProvider : public IUnknown
        {
        public:
            virtual /* [local] */ HRESULT STDMETHODCALLTYPE QueryService( 
                /* [in] */ REFGUID guidService,
                /* [in] */ REFIID riid,
                /* [out] */ void __RPC_FAR *__RPC_FAR *ppvObject) = 0;
            
            template <class Q>
            HRESULT STDMETHODCALLTYPE QueryService(REFGUID guidService, Q** pp)
            {
                return QueryService(guidService, __uuidof(Q), (void **)pp);
            }
        };
    }

    /* [call_as] */ HRESULT STDMETHODCALLTYPE IServiceProvider_RemoteQueryService_Proxy( 
        IServiceProvider __RPC_FAR * This,
        /* [in] */ REFGUID guidService,
        /* [in] */ REFIID riid,
        /* [iid_is][out] */ IUnknown __RPC_FAR *__RPC_FAR *ppvObject);

    void __RPC_STUB IServiceProvider_RemoteQueryService_Stub(
        IRpcStubBuffer *This,
        IRpcChannelBuffer *_pRpcChannelBuffer,
        PRPC_MESSAGE _pRpcMessage,
        DWORD *_pdwStubPhase);

#else // VC6 Hack

EXTERN_C const IID IID_IServiceProvider;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("6d5140c1-7436-11ce-8034-00aa006009fa")
    IServiceProvider : public IUnknown
    {
    public:
        virtual /* [local] */ HRESULT STDMETHODCALLTYPE QueryService( 
            /* [in] */ REFGUID guidService,
            /* [in] */ REFIID riid,
            /* [out] */ void **ppvObject) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IServiceProviderVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IServiceProvider * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IServiceProvider * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IServiceProvider * This);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *QueryService )( 
            IServiceProvider * This,
            /* [in] */ REFGUID guidService,
            /* [in] */ REFIID riid,
            /* [out] */ void **ppvObject);
        
        END_INTERFACE
    } IServiceProviderVtbl;

    interface IServiceProvider
    {
        CONST_VTBL struct IServiceProviderVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IServiceProvider_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IServiceProvider_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IServiceProvider_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IServiceProvider_QueryService(This,guidService,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryService(This,guidService,riid,ppvObject) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [call_as] */ HRESULT STDMETHODCALLTYPE IServiceProvider_RemoteQueryService_Proxy( 
    IServiceProvider * This,
    /* [in] */ REFGUID guidService,
    /* [in] */ REFIID riid,
    /* [iid_is][out] */ IUnknown **ppvObject);


void __RPC_STUB IServiceProvider_RemoteQueryService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IServiceProvider_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_bmedia_0000_0001 */
/* [local] */ 

#endif // VC6 Hack


extern RPC_IF_HANDLE __MIDL_itf_bmedia_0000_0001_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_bmedia_0000_0001_v0_0_s_ifspec;

#ifndef __IAsyncPProt_INTERFACE_DEFINED__
#define __IAsyncPProt_INTERFACE_DEFINED__

/* interface IAsyncPProt */
/* [unique][dual][uuid][object] */ 


EXTERN_C const IID IID_IAsyncPProt;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("3DA2AA3A-3D96-11D2-9BD2-204C4F4F5020")
    IAsyncPProt : public IDispatch
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IAsyncPProtVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAsyncPProt * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAsyncPProt * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAsyncPProt * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAsyncPProt * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAsyncPProt * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAsyncPProt * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAsyncPProt * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } IAsyncPProtVtbl;

    interface IAsyncPProt
    {
        CONST_VTBL struct IAsyncPProtVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAsyncPProt_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAsyncPProt_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAsyncPProt_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAsyncPProt_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAsyncPProt_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAsyncPProt_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAsyncPProt_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAsyncPProt_INTERFACE_DEFINED__ */


#ifndef __IAsyncMHandler_INTERFACE_DEFINED__
#define __IAsyncMHandler_INTERFACE_DEFINED__

/* interface IAsyncMHandler */
/* [unique][dual][uuid][object] */ 


EXTERN_C const IID IID_IAsyncMHandler;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("3DA2AA3D-3D96-11D2-9BD2-204C4F4F5020")
    IAsyncMHandler : public IDispatch
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IAsyncMHandlerVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAsyncMHandler * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAsyncMHandler * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAsyncMHandler * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAsyncMHandler * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAsyncMHandler * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAsyncMHandler * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAsyncMHandler * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } IAsyncMHandlerVtbl;

    interface IAsyncMHandler
    {
        CONST_VTBL struct IAsyncMHandlerVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAsyncMHandler_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAsyncMHandler_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAsyncMHandler_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAsyncMHandler_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAsyncMHandler_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAsyncMHandler_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAsyncMHandler_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAsyncMHandler_INTERFACE_DEFINED__ */


#ifndef __IDirectControl_INTERFACE_DEFINED__
#define __IDirectControl_INTERFACE_DEFINED__

/* interface IDirectControl */
/* [unique][dual][uuid][object] */ 


EXTERN_C const IID IID_IDirectControl;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("39A2C2A5-4778-11D2-9BDB-204C4F4F5020")
    IDirectControl : public IDispatch
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE CreateView( 
            BSTR bszClsid) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE DestroyAllViews( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDirectControlVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDirectControl * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDirectControl * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDirectControl * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IDirectControl * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IDirectControl * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IDirectControl * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IDirectControl * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        HRESULT ( STDMETHODCALLTYPE *CreateView )( 
            IDirectControl * This,
            BSTR bszClsid);
        
        HRESULT ( STDMETHODCALLTYPE *DestroyAllViews )( 
            IDirectControl * This);
        
        END_INTERFACE
    } IDirectControlVtbl;

    interface IDirectControl
    {
        CONST_VTBL struct IDirectControlVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDirectControl_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IDirectControl_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IDirectControl_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IDirectControl_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IDirectControl_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IDirectControl_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IDirectControl_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IDirectControl_CreateView(This,bszClsid)	\
    ( (This)->lpVtbl -> CreateView(This,bszClsid) ) 

#define IDirectControl_DestroyAllViews(This)	\
    ( (This)->lpVtbl -> DestroyAllViews(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IDirectControl_INTERFACE_DEFINED__ */


#ifndef __IDirectControlView_INTERFACE_DEFINED__
#define __IDirectControlView_INTERFACE_DEFINED__

/* interface IDirectControlView */
/* [unique][dual][uuid][object] */ 


EXTERN_C const IID IID_IDirectControlView;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("39A2C2FF-4778-11D2-9BDB-204C4F4F5020")
    IDirectControlView : public IDispatch
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE IsTimerNeeded( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE OnTimer( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE put_Visible( 
            VARIANT_BOOL bVisible) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE OnFocusChange( 
            VARIANT_BOOL bFocus) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDirectControlViewVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDirectControlView * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDirectControlView * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDirectControlView * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IDirectControlView * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IDirectControlView * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IDirectControlView * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IDirectControlView * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        HRESULT ( STDMETHODCALLTYPE *IsTimerNeeded )( 
            IDirectControlView * This);
        
        HRESULT ( STDMETHODCALLTYPE *OnTimer )( 
            IDirectControlView * This);
        
        HRESULT ( STDMETHODCALLTYPE *put_Visible )( 
            IDirectControlView * This,
            VARIANT_BOOL bVisible);
        
        HRESULT ( STDMETHODCALLTYPE *OnFocusChange )( 
            IDirectControlView * This,
            VARIANT_BOOL bFocus);
        
        END_INTERFACE
    } IDirectControlViewVtbl;

    interface IDirectControlView
    {
        CONST_VTBL struct IDirectControlViewVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDirectControlView_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IDirectControlView_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IDirectControlView_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IDirectControlView_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IDirectControlView_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IDirectControlView_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IDirectControlView_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IDirectControlView_IsTimerNeeded(This)	\
    ( (This)->lpVtbl -> IsTimerNeeded(This) ) 

#define IDirectControlView_OnTimer(This)	\
    ( (This)->lpVtbl -> OnTimer(This) ) 

#define IDirectControlView_put_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> put_Visible(This,bVisible) ) 

#define IDirectControlView_OnFocusChange(This,bFocus)	\
    ( (This)->lpVtbl -> OnFocusChange(This,bFocus) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IDirectControlView_INTERFACE_DEFINED__ */


#ifndef __IDirectContainer_INTERFACE_DEFINED__
#define __IDirectContainer_INTERFACE_DEFINED__

/* interface IDirectContainer */
/* [unique][uuid][object] */ 


EXTERN_C const IID IID_IDirectContainer;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("39A2C2A8-4778-11D2-9BDB-204C4F4F5020")
    IDirectContainer : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE CreateControl( 
            BSTR bszClsid,
            DWORD dwClsContext,
            IUnknown **ppunk,
            DWORD dwWindowStyle) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetServiceProvider( 
            IServiceProvider *pspSet) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetIInputObjectSite( 
            IUnknown *pios) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE ShowControl( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE HideControl( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE IsControlCreated( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE DestroyControl( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDirectContainerVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDirectContainer * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDirectContainer * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDirectContainer * This);
        
        HRESULT ( STDMETHODCALLTYPE *CreateControl )( 
            IDirectContainer * This,
            BSTR bszClsid,
            DWORD dwClsContext,
            IUnknown **ppunk,
            DWORD dwWindowStyle);
        
        HRESULT ( STDMETHODCALLTYPE *SetServiceProvider )( 
            IDirectContainer * This,
            IServiceProvider *pspSet);
        
        HRESULT ( STDMETHODCALLTYPE *SetIInputObjectSite )( 
            IDirectContainer * This,
            IUnknown *pios);
        
        HRESULT ( STDMETHODCALLTYPE *ShowControl )( 
            IDirectContainer * This);
        
        HRESULT ( STDMETHODCALLTYPE *HideControl )( 
            IDirectContainer * This);
        
        HRESULT ( STDMETHODCALLTYPE *IsControlCreated )( 
            IDirectContainer * This);
        
        HRESULT ( STDMETHODCALLTYPE *DestroyControl )( 
            IDirectContainer * This);
        
        END_INTERFACE
    } IDirectContainerVtbl;

    interface IDirectContainer
    {
        CONST_VTBL struct IDirectContainerVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDirectContainer_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IDirectContainer_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IDirectContainer_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IDirectContainer_CreateControl(This,bszClsid,dwClsContext,ppunk,dwWindowStyle)	\
    ( (This)->lpVtbl -> CreateControl(This,bszClsid,dwClsContext,ppunk,dwWindowStyle) ) 

#define IDirectContainer_SetServiceProvider(This,pspSet)	\
    ( (This)->lpVtbl -> SetServiceProvider(This,pspSet) ) 

#define IDirectContainer_SetIInputObjectSite(This,pios)	\
    ( (This)->lpVtbl -> SetIInputObjectSite(This,pios) ) 

#define IDirectContainer_ShowControl(This)	\
    ( (This)->lpVtbl -> ShowControl(This) ) 

#define IDirectContainer_HideControl(This)	\
    ( (This)->lpVtbl -> HideControl(This) ) 

#define IDirectContainer_IsControlCreated(This)	\
    ( (This)->lpVtbl -> IsControlCreated(This) ) 

#define IDirectContainer_DestroyControl(This)	\
    ( (This)->lpVtbl -> DestroyControl(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IDirectContainer_INTERFACE_DEFINED__ */


#ifndef __IRadioView_INTERFACE_DEFINED__
#define __IRadioView_INTERFACE_DEFINED__

/* interface IRadioView */
/* [unique][dual][uuid][object] */ 


EXTERN_C const IID IID_IRadioView;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("847B4DF4-4B61-11D2-9BDB-204C4F4F5020")
    IRadioView : public IDispatch
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IRadioViewVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IRadioView * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IRadioView * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IRadioView * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IRadioView * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IRadioView * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IRadioView * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IRadioView * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } IRadioViewVtbl;

    interface IRadioView
    {
        CONST_VTBL struct IRadioViewVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IRadioView_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IRadioView_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IRadioView_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IRadioView_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IRadioView_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IRadioView_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IRadioView_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IRadioView_INTERFACE_DEFINED__ */


#ifndef __IRadioPlayer_INTERFACE_DEFINED__
#define __IRadioPlayer_INTERFACE_DEFINED__

/* interface IRadioPlayer */
/* [unique][dual][uuid][object] */ 


EXTERN_C const IID IID_IRadioPlayer;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("9C2263AF-3E3C-11D2-9BD3-204C4F4F5020")
    IRadioPlayer : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE BindRadioMemory( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ReleaseRadio( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RegisterEvent( 
            /* [in] */ BSTR bszEvent,
            /* [retval][out] */ LONG *plRegister) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RegisterWindow( 
            /* [in] */ LONG lHWND,
            /* [in] */ DWORD dwMessage,
            /* [in] */ DWORD dwCodeSet,
            /* [retval][out] */ LONG *plRegister) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetSection( 
            /* [retval][out] */ BSTR *bszSection) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Unregister( 
            /* [in] */ LONG lRegister) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetInstanceCount( 
            /* [retval][out] */ LONG *plInstances) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Play( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Stop( void) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_Url( 
            /* [in] */ BSTR wszUrl) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_Volume( 
            /* [in] */ LONG lVolumeSet) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_Mute( 
            /* [in] */ VARIANT_BOOL fMuteSet) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetStatus( 
            /* [out] */ LONG *plVolume,
            /* [out] */ LONG *pfMute,
            /* [out] */ LONG *pfPlay,
            /* [out] */ BSTR *__MIDL__IRadioPlayer0000,
            /* [out] */ BSTR *__MIDL__IRadioPlayer0001,
            /* [out] */ BSTR *__MIDL__IRadioPlayer0002,
            /* [out] */ BSTR *__MIDL__IRadioPlayer0003,
            /* [out] */ BSTR *__MIDL__IRadioPlayer0004,
            /* [out] */ BSTR *__MIDL__IRadioPlayer0005,
            /* [out] */ BSTR *__MIDL__IRadioPlayer0006) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetState( 
            /* [out] */ LONG *plOpenState,
            /* [out] */ LONG *pfBuffering,
            /* [out] */ LONG *plBufferingPercent,
            /* [out] */ LONG *plQuality) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IRadioPlayerVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IRadioPlayer * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IRadioPlayer * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IRadioPlayer * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IRadioPlayer * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IRadioPlayer * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IRadioPlayer * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IRadioPlayer * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *BindRadioMemory )( 
            IRadioPlayer * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ReleaseRadio )( 
            IRadioPlayer * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RegisterEvent )( 
            IRadioPlayer * This,
            /* [in] */ BSTR bszEvent,
            /* [retval][out] */ LONG *plRegister);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RegisterWindow )( 
            IRadioPlayer * This,
            /* [in] */ LONG lHWND,
            /* [in] */ DWORD dwMessage,
            /* [in] */ DWORD dwCodeSet,
            /* [retval][out] */ LONG *plRegister);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetSection )( 
            IRadioPlayer * This,
            /* [retval][out] */ BSTR *bszSection);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Unregister )( 
            IRadioPlayer * This,
            /* [in] */ LONG lRegister);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetInstanceCount )( 
            IRadioPlayer * This,
            /* [retval][out] */ LONG *plInstances);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Play )( 
            IRadioPlayer * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Stop )( 
            IRadioPlayer * This);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Url )( 
            IRadioPlayer * This,
            /* [in] */ BSTR wszUrl);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Volume )( 
            IRadioPlayer * This,
            /* [in] */ LONG lVolumeSet);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Mute )( 
            IRadioPlayer * This,
            /* [in] */ VARIANT_BOOL fMuteSet);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetStatus )( 
            IRadioPlayer * This,
            /* [out] */ LONG *plVolume,
            /* [out] */ LONG *pfMute,
            /* [out] */ LONG *pfPlay,
            /* [out] */ BSTR *__MIDL__IRadioPlayer0000,
            /* [out] */ BSTR *__MIDL__IRadioPlayer0001,
            /* [out] */ BSTR *__MIDL__IRadioPlayer0002,
            /* [out] */ BSTR *__MIDL__IRadioPlayer0003,
            /* [out] */ BSTR *__MIDL__IRadioPlayer0004,
            /* [out] */ BSTR *__MIDL__IRadioPlayer0005,
            /* [out] */ BSTR *__MIDL__IRadioPlayer0006);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetState )( 
            IRadioPlayer * This,
            /* [out] */ LONG *plOpenState,
            /* [out] */ LONG *pfBuffering,
            /* [out] */ LONG *plBufferingPercent,
            /* [out] */ LONG *plQuality);
        
        END_INTERFACE
    } IRadioPlayerVtbl;

    interface IRadioPlayer
    {
        CONST_VTBL struct IRadioPlayerVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IRadioPlayer_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IRadioPlayer_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IRadioPlayer_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IRadioPlayer_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IRadioPlayer_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IRadioPlayer_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IRadioPlayer_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IRadioPlayer_BindRadioMemory(This)	\
    ( (This)->lpVtbl -> BindRadioMemory(This) ) 

#define IRadioPlayer_ReleaseRadio(This)	\
    ( (This)->lpVtbl -> ReleaseRadio(This) ) 

#define IRadioPlayer_RegisterEvent(This,bszEvent,plRegister)	\
    ( (This)->lpVtbl -> RegisterEvent(This,bszEvent,plRegister) ) 

#define IRadioPlayer_RegisterWindow(This,lHWND,dwMessage,dwCodeSet,plRegister)	\
    ( (This)->lpVtbl -> RegisterWindow(This,lHWND,dwMessage,dwCodeSet,plRegister) ) 

#define IRadioPlayer_GetSection(This,bszSection)	\
    ( (This)->lpVtbl -> GetSection(This,bszSection) ) 

#define IRadioPlayer_Unregister(This,lRegister)	\
    ( (This)->lpVtbl -> Unregister(This,lRegister) ) 

#define IRadioPlayer_GetInstanceCount(This,plInstances)	\
    ( (This)->lpVtbl -> GetInstanceCount(This,plInstances) ) 

#define IRadioPlayer_Play(This)	\
    ( (This)->lpVtbl -> Play(This) ) 

#define IRadioPlayer_Stop(This)	\
    ( (This)->lpVtbl -> Stop(This) ) 

#define IRadioPlayer_put_Url(This,wszUrl)	\
    ( (This)->lpVtbl -> put_Url(This,wszUrl) ) 

#define IRadioPlayer_put_Volume(This,lVolumeSet)	\
    ( (This)->lpVtbl -> put_Volume(This,lVolumeSet) ) 

#define IRadioPlayer_put_Mute(This,fMuteSet)	\
    ( (This)->lpVtbl -> put_Mute(This,fMuteSet) ) 

#define IRadioPlayer_GetStatus(This,plVolume,pfMute,pfPlay,__MIDL__IRadioPlayer0000,__MIDL__IRadioPlayer0001,__MIDL__IRadioPlayer0002,__MIDL__IRadioPlayer0003,__MIDL__IRadioPlayer0004,__MIDL__IRadioPlayer0005,__MIDL__IRadioPlayer0006)	\
    ( (This)->lpVtbl -> GetStatus(This,plVolume,pfMute,pfPlay,__MIDL__IRadioPlayer0000,__MIDL__IRadioPlayer0001,__MIDL__IRadioPlayer0002,__MIDL__IRadioPlayer0003,__MIDL__IRadioPlayer0004,__MIDL__IRadioPlayer0005,__MIDL__IRadioPlayer0006) ) 

#define IRadioPlayer_GetState(This,plOpenState,pfBuffering,plBufferingPercent,plQuality)	\
    ( (This)->lpVtbl -> GetState(This,plOpenState,pfBuffering,plBufferingPercent,plQuality) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IRadioPlayer_INTERFACE_DEFINED__ */


#ifndef __IRadioServer_INTERFACE_DEFINED__
#define __IRadioServer_INTERFACE_DEFINED__

/* interface IRadioServer */
/* [unique][dual][uuid][object] */ 


EXTERN_C const IID IID_IRadioServer;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("9C2263A0-3E3C-11D2-9BD3-204C4F4F5020")
    IRadioServer : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE BindToRadio( 
            /* [in] */ BSTR wszRadio,
            /* [retval][out] */ IRadioPlayer **ppServer) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE IsRadioExists( 
            /* [in] */ BSTR wszRadio) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE LaunchStandardUrl( 
            /* [in] */ BSTR bszUrl,
            /* [in] */ IUnknown *pBrowser) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IRadioServerVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IRadioServer * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IRadioServer * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IRadioServer * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IRadioServer * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IRadioServer * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IRadioServer * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IRadioServer * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *BindToRadio )( 
            IRadioServer * This,
            /* [in] */ BSTR wszRadio,
            /* [retval][out] */ IRadioPlayer **ppServer);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *IsRadioExists )( 
            IRadioServer * This,
            /* [in] */ BSTR wszRadio);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *LaunchStandardUrl )( 
            IRadioServer * This,
            /* [in] */ BSTR bszUrl,
            /* [in] */ IUnknown *pBrowser);
        
        END_INTERFACE
    } IRadioServerVtbl;

    interface IRadioServer
    {
        CONST_VTBL struct IRadioServerVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IRadioServer_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IRadioServer_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IRadioServer_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IRadioServer_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IRadioServer_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IRadioServer_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IRadioServer_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IRadioServer_BindToRadio(This,wszRadio,ppServer)	\
    ( (This)->lpVtbl -> BindToRadio(This,wszRadio,ppServer) ) 

#define IRadioServer_IsRadioExists(This,wszRadio)	\
    ( (This)->lpVtbl -> IsRadioExists(This,wszRadio) ) 

#define IRadioServer_LaunchStandardUrl(This,bszUrl,pBrowser)	\
    ( (This)->lpVtbl -> LaunchStandardUrl(This,bszUrl,pBrowser) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IRadioServer_INTERFACE_DEFINED__ */


#ifndef __IRadioServerControl_INTERFACE_DEFINED__
#define __IRadioServerControl_INTERFACE_DEFINED__

/* interface IRadioServerControl */
/* [unique][dual][uuid][object] */ 


EXTERN_C const IID IID_IRadioServerControl;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("8E718889-423F-11D2-876E-00A0C9082467")
    IRadioServerControl : public IDispatch
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IRadioServerControlVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IRadioServerControl * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IRadioServerControl * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IRadioServerControl * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IRadioServerControl * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IRadioServerControl * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IRadioServerControl * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IRadioServerControl * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } IRadioServerControlVtbl;

    interface IRadioServerControl
    {
        CONST_VTBL struct IRadioServerControlVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IRadioServerControl_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IRadioServerControl_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IRadioServerControl_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IRadioServerControl_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IRadioServerControl_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IRadioServerControl_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IRadioServerControl_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IRadioServerControl_INTERFACE_DEFINED__ */


#ifndef __IRadioBand_INTERFACE_DEFINED__
#define __IRadioBand_INTERFACE_DEFINED__

/* interface IRadioBand */
/* [unique][dual][uuid][object] */ 


EXTERN_C const IID IID_IRadioBand;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("8E718881-423F-11D2-876E-00A0C9082467")
    IRadioBand : public IDispatch
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE Create( 
            LONG *phwnd,
            LONG hwndParent) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IRadioBandVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IRadioBand * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IRadioBand * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IRadioBand * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IRadioBand * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IRadioBand * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IRadioBand * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IRadioBand * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        HRESULT ( STDMETHODCALLTYPE *Create )( 
            IRadioBand * This,
            LONG *phwnd,
            LONG hwndParent);
        
        END_INTERFACE
    } IRadioBandVtbl;

    interface IRadioBand
    {
        CONST_VTBL struct IRadioBandVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IRadioBand_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IRadioBand_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IRadioBand_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IRadioBand_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IRadioBand_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IRadioBand_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IRadioBand_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IRadioBand_Create(This,phwnd,hwndParent)	\
    ( (This)->lpVtbl -> Create(This,phwnd,hwndParent) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IRadioBand_INTERFACE_DEFINED__ */


#ifndef __IMediaBindStream_INTERFACE_DEFINED__
#define __IMediaBindStream_INTERFACE_DEFINED__

/* interface IMediaBindStream */
/* [object][unique][dual][uuid] */ 


EXTERN_C const IID IID_IMediaBindStream;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("920F0DE3-91C5-11d2-828F-00C04FC99D4E")
    IMediaBindStream : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE LoadMoniker( 
            /* [in] */ BSTR bszTransferContext,
            /* [in] */ BSTR bszUrl) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMediaBindStreamVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMediaBindStream * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMediaBindStream * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMediaBindStream * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IMediaBindStream * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IMediaBindStream * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IMediaBindStream * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IMediaBindStream * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *LoadMoniker )( 
            IMediaBindStream * This,
            /* [in] */ BSTR bszTransferContext,
            /* [in] */ BSTR bszUrl);
        
        END_INTERFACE
    } IMediaBindStreamVtbl;

    interface IMediaBindStream
    {
        CONST_VTBL struct IMediaBindStreamVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMediaBindStream_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMediaBindStream_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMediaBindStream_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IMediaBindStream_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IMediaBindStream_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IMediaBindStream_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IMediaBindStream_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IMediaBindStream_LoadMoniker(This,bszTransferContext,bszUrl)	\
    ( (This)->lpVtbl -> LoadMoniker(This,bszTransferContext,bszUrl) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMediaBindStream_INTERFACE_DEFINED__ */


#ifndef __IMediaPlayerListener_INTERFACE_DEFINED__
#define __IMediaPlayerListener_INTERFACE_DEFINED__

/* interface IMediaPlayerListener */
/* [unique][uuid][object] */ 


EXTERN_C const IID IID_IMediaPlayerListener;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("33222211-5E5E-11d2-9E8E-0000F8085981")
    IMediaPlayerListener : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE PlayStateChanged( 
            /* [in] */ long lNewState) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Buffering( 
            /* [in] */ VARIANT_BOOL fStart) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE BufferPercent( 
            /* [in] */ long lBufferPercent) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE OpenStateChanged( 
            /* [in] */ long lOpenState) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE MediaInfoChanged( 
            /* [in] */ BSTR bstrShowTitle,
            /* [in] */ BSTR bstrClipTitle,
            /* [in] */ BSTR bstrClipAuthor,
            /* [in] */ BSTR bstrClipCopyright,
            /* [in] */ BSTR bstrStationURL) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE QualityChanged( 
            /* [in] */ long lQuality) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Error( 
            /* [in] */ BSTR bstrError) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMediaPlayerListenerVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMediaPlayerListener * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMediaPlayerListener * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMediaPlayerListener * This);
        
        HRESULT ( STDMETHODCALLTYPE *PlayStateChanged )( 
            IMediaPlayerListener * This,
            /* [in] */ long lNewState);
        
        HRESULT ( STDMETHODCALLTYPE *Buffering )( 
            IMediaPlayerListener * This,
            /* [in] */ VARIANT_BOOL fStart);
        
        HRESULT ( STDMETHODCALLTYPE *BufferPercent )( 
            IMediaPlayerListener * This,
            /* [in] */ long lBufferPercent);
        
        HRESULT ( STDMETHODCALLTYPE *OpenStateChanged )( 
            IMediaPlayerListener * This,
            /* [in] */ long lOpenState);
        
        HRESULT ( STDMETHODCALLTYPE *MediaInfoChanged )( 
            IMediaPlayerListener * This,
            /* [in] */ BSTR bstrShowTitle,
            /* [in] */ BSTR bstrClipTitle,
            /* [in] */ BSTR bstrClipAuthor,
            /* [in] */ BSTR bstrClipCopyright,
            /* [in] */ BSTR bstrStationURL);
        
        HRESULT ( STDMETHODCALLTYPE *QualityChanged )( 
            IMediaPlayerListener * This,
            /* [in] */ long lQuality);
        
        HRESULT ( STDMETHODCALLTYPE *Error )( 
            IMediaPlayerListener * This,
            /* [in] */ BSTR bstrError);
        
        END_INTERFACE
    } IMediaPlayerListenerVtbl;

    interface IMediaPlayerListener
    {
        CONST_VTBL struct IMediaPlayerListenerVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMediaPlayerListener_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMediaPlayerListener_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMediaPlayerListener_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IMediaPlayerListener_PlayStateChanged(This,lNewState)	\
    ( (This)->lpVtbl -> PlayStateChanged(This,lNewState) ) 

#define IMediaPlayerListener_Buffering(This,fStart)	\
    ( (This)->lpVtbl -> Buffering(This,fStart) ) 

#define IMediaPlayerListener_BufferPercent(This,lBufferPercent)	\
    ( (This)->lpVtbl -> BufferPercent(This,lBufferPercent) ) 

#define IMediaPlayerListener_OpenStateChanged(This,lOpenState)	\
    ( (This)->lpVtbl -> OpenStateChanged(This,lOpenState) ) 

#define IMediaPlayerListener_MediaInfoChanged(This,bstrShowTitle,bstrClipTitle,bstrClipAuthor,bstrClipCopyright,bstrStationURL)	\
    ( (This)->lpVtbl -> MediaInfoChanged(This,bstrShowTitle,bstrClipTitle,bstrClipAuthor,bstrClipCopyright,bstrStationURL) ) 

#define IMediaPlayerListener_QualityChanged(This,lQuality)	\
    ( (This)->lpVtbl -> QualityChanged(This,lQuality) ) 

#define IMediaPlayerListener_Error(This,bstrError)	\
    ( (This)->lpVtbl -> Error(This,bstrError) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMediaPlayerListener_INTERFACE_DEFINED__ */


/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

/* [local] */ HRESULT STDMETHODCALLTYPE IServiceProvider_QueryService_Proxy( 
    IServiceProvider * This,
    /* [in] */ REFGUID guidService,
    /* [in] */ REFIID riid,
    /* [out] */ void **ppvObject);


/* [call_as] */ HRESULT STDMETHODCALLTYPE IServiceProvider_QueryService_Stub( 
    IServiceProvider * This,
    /* [in] */ REFGUID guidService,
    /* [in] */ REFIID riid,
    /* [iid_is][out] */ IUnknown **ppvObject);



/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


