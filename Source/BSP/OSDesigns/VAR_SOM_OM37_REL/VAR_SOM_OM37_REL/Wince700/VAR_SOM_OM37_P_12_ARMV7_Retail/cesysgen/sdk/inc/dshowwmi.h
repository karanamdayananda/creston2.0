//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft
// premium shared source license agreement under which you licensed
// this source code. If you did not accept the terms of the license
// agreement, you are not authorized to use this source code.
// For the terms of the license, please see the license agreement
// signed by you and Microsoft.
// THE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//

#pragma once

#include <dshowWmiBase.h>
extern WmiInfo g_WmiInfo;

#include <dshowwmistr.h>

typedef unsigned __int64 QWORD;

extern DBGPARAM dpCurSettings;

#define CELID_DSHOW_BASE        (CELID_USER + 0x300)
#define CELID_DSHOW_PACKET      (CELID_DSHOW_BASE)              // deprecated, old perf logging style
#define CELID_DSHOW_OBJECT      (CELID_DSHOW_BASE + 1)          // deprecated, old perf logging style
#define CELID_DSHOW_EVENT_TRACE (CELID_DSHOW_BASE + 2)
#define CELID_DSHOW_MAX         (CELID_DSHOW_BASE + 3)

__inline void TraceEvent (TRACEHANDLE TraceHandle, EVENT_TRACE_HEADER *pEventHeader)
{
    LARGE_INTEGER liCounter;
    liCounter.QuadPart = 0;
    QueryPerformanceCounter(&liCounter);

    pEventHeader->ThreadId = GetCurrentThreadId();
    pEventHeader->ProcessId = GetCurrentProcessId();
    pEventHeader->TimeStamp = liCounter;
    CeLogData (TRUE, CELID_DSHOW_EVENT_TRACE, pEventHeader, pEventHeader->Size, 1, CELZONE_MISC, 0, FALSE);
}


#define INVALID_SAMPLE_TIME _I64_MAX
#define INVALID_SAMPLE_DURATION _I64_MAX
#define INVALID_COUNT_BYTE  _I32_MIN
#define DONT_CARE_TIME      _I64_MIN // when time does not matter

#define NULL_SCENARIO_IID  {0, 0, 0, {0, 0, 0, 0, 0, 0, 0, 0} }
#define SCENARIOIID_IS_NULL(id) ((id.Data1==0)&&(id.Data2==0)&&(id.Data3==0)&&\
                                (id.Data4[0]==0)&&(id.Data4[1]==0)&&(id.Data4[2]==0)&&\
                                (id.Data4[3]==0)&&(id.Data4[4]==0)&&(id.Data4[5]==0)&&\
                                (id.Data4[6]==0)&&(id.Data4[7]==0))

typedef GUID    SCENARIOIID ;
typedef GUID *  PSCENARIOIID ;

inline ULONGLONG WMI_COUNTER( void ) {
    LARGE_INTEGER liCounter;
    liCounter.QuadPart = 0;
    if (g_WmiInfo.ucWmiEnableLevel > 0) {
        QueryPerformanceCounter(&liCounter);
    }
    return (ULONGLONG)liCounter.QuadPart;
}

#define WMI_MEASURE_START ULONGLONG ullStart = WMI_COUNTER()
#define WMI_MEASURE_END (WMI_COUNTER() - ullStart)
#define WMI_IS_LOGGING (g_WmiInfo.ulWmiEnableLevel > 0)

#define FILL_DSHOWPERFINFO_COMMON( perfData, dwReporterTag, eventGuid, eventType, pObject ) \
        FILL_DSHOWPERFINFO_COMMON_PTR( &perfData, sizeof(perfData), dwReporterTag, eventGuid, eventType, pObject)

#define FILL_DSHOWPERFINFO_COMMON_PTR( pperfData, dwSize, dwTag, eventGuid, eventType, pObj ){\
        ZeroMemory(pperfData, dwSize);                                        \
        (pperfData)->hdr.wmiHeader.Size         = (USHORT)(dwSize);           \
        (pperfData)->hdr.wmiHeader.Flags        = WNODE_FLAG_TRACED_GUID;     \
        (pperfData)->hdr.wmiHeader.Guid         = (eventGuid);                \
        (pperfData)->hdr.wmiHeader.Class.Type   = (eventType);                \
        (pperfData)->hdr.pObject                = (DSHOWWMI_PTR)(pObj);          \
        (pperfData)->hdr.dwReporterTag          = (dwTag);                    \
    }


//
// These boolean macros are defined so that expensive events can be by-passed by provider
// modules quickly when logging is not enabled.
// WMILOG_* in-line functions use thes too so we can adjust the control easily at this place.
// The convention is b##WMILOG_* to signify a boolean. As more events are added, at some
// point we might add conditions to the expressions to finer control what get logged. Finer
// controls may use masks and more levels.
//

#ifdef SHIP_BUILD

// Critical events
#define bWMILOG_AUDIORENDER_STARVATION  (0)
#define bWMILOG_VIDEO_FRAME_GLITCH      (0)
#define bWMILOG_DATA_DROP               (0)
#define bWMILOG_STATE_CHANGE            (0)
#define bWMILOG_CLOCK_SETTIME           (0)

// Warning events
#define bWMILOG_MEDIATYPE_CHANGE        (0)
#define bWMILOG_UNUSUAL_STREAMING_EVENT (0)
#define bWMILOG_FILE_OPEN               (0)
#define bWMILOG_NETSOURCE               (0)
#define bWMILOG_ADJUST_SAMPLE_TIME      (0)
#define bWMILOG_TIMER_LATE              (0)

// Trace events
#define bWMILOG_BUFFER                  (0)
#define bWMILOG_OBJECT                  (0)
#define bWMILOG_DISKIO_REQUEST          (0)
#define bWMILOG_DISKIO_COMPLETE         (0)
#define bWMILOG_RENDERER_REND           (0)
#define bWMILOG_MUXER                   (0)

// Need to use events (warning level)
#define bWMILOG_AUDIORENDERER_BUFFERFULLNESS (0)
#define bWMILOG_SOURCERESOLUTION             (0)
#define bWMILOG_QM                           (0)
#define bWMILOG_VIDEO_RENDER                 (0)
#define bWMILOG_AUDIO_RENDER                 (0)

// Need to use events (verbose level)
#define bWMILOG_CLOCK_GETTIME           (0)
#define bWMILOG_LOCK                    (0)
#define bWMILOG_STREAM                  (0)
#define bWMILOG_HTTP_BYTESTREAM         (0)


#define WMILOG_OBJECT(dwReporterTag,lActionType,pObject,lObjectType) (void(0))
#define WMILOG_AUDIORENDER_STARVATION(dwReporterTag,pObject,ullCount) (void(0))
#define WMILOG_VIDEO_FRAME_GLITCH(dwReporterTag,pObject,dwObjectID,pSample,llSampleTime,llTargetSystemTime,llOffset) (void(0))
#define WMILOG_DATA_DROP(dwReporterTag,dwDataType,pObject,dwObjectID,llSampleTime,cbDropped,dwReasons) (void(0))
#define WMILOG_CLOCK_GETTIME(dwReporterTag,pObject,llTime) (void(0))
#define WMILOG_CLOCK_SETTIME(dwReporterTag,pObject,llTime,llDelta) (void(0))
#define WMILOG_STREAM(dwReporterTag,lStreamType,pObject) (void(0))
#define WMILOG_MEDIATYPE_CHANGE(dwReporterTag,pObject,pStream,cbNewType,pbNewType) (void(0))
#define WMILOG_BUFFER(dwReporterTag,pObject,Type,dwObjectCategory,pStream,llTimestamp,pClock,pSample,cbBuffer,cbSample,llDuration) (void(0))
#define WMILOG_UNUSUAL_STREAMING_EVENT(dwReporterTag,Type,pObject,dwEvent) (void(0))
#define WMILOG_FILE_OPEN(dwReporterTag,pObject,h,pwszName) (void(0))
#define WMILOG_FILE_CLOSE(dwReporterTag,pObject,h,pwszName) (void(0))
#define WMILOG_DISKIO_REQUEST(dwReporterTag,Type,pObject,h,qwOffset,bCount) (void(0))
#define WMILOG_DISKIO_COMPLETE(dwReporterTag,Type,pObject,h) (void(0))
#define WMILOG_RENDERER_REND(dwReporterTag,Type,pObject,pSample,llTime,llDuration,llClock) (void(0))
#define WMILOG_RENDERER_MIX(dwReporterTag,Type,pObject,pSample,llTime,llDuration,llClock) \
    WMILOG_RENDERER_REND(dwReporterTag,Type,pObject,pSample,llTime,llDuration,llClock)

#define WMILOG_AUDIORENDERER_BUFFERFULLNESS(dwReporterTag,pObject,dwFullness) (void(0))
#define WMILOG_STATE_CHANGE(dwReporterTag,pObject,dwStateChange) (void(0))
#define WMILOG_NETSOURCE(dwReporterTag,pObject,Type,Arg1,Arg2) (void(0))
#define WMILOG_HTTP_BYTESTREAM(dwReporterTag,pObject,Type) (void(0))
#define WMILOG_SOURCERESOLUTION(dwReporterTag,pObject,Type,pwszURL,pObjectCreated,hr) (void(0))
#define WMILOG_QM(dwReporterTag,pObject,Type,dwKnobId,dwPrevLevel,dwNewLevel,llDropTime) (void(0))
#define WMILOG_ADJUST_SAMPLE_TIME(dwReporterTag,pObject,ucType,llOriginalSampleTime,llAdjustment) (void(0))
#define WMILOG_VIDEO_RENDER(dwReporterTag,pObject,pSample,llSampleTime,llSampleDuration,llClockTime,hwndVideo,dwRefreshRate,dwWidth,dwHeight,dwLeft,dwTop,dwRight,dwBottom,dwLeft1,dwTop1,dwRight1,dwBottom1) (void(0))
#define WMILOG_AUDIO_RENDER(dwReporterTag,pObject,pSample,llTime,llDuration,llMasterTime,llDeviceTime) (void(0))
#define WMILOG_MUXER(dwReportTag,pObject,Type,wStreamNumber,llSampleTime,cbSample,llPacketNumber,llPacketSendTime,cbPacket) (void(0))
#define WMILOG_TIMER_LATE(dwReporterTag,pObject,llEventTime,llLateBy,pCallback) (void(0))
#define WMILOG_LOCK(dwReporterTag,pObject,Type,pLockObject) (void(0))
#define WMILOG_GRAPH(dwReporterTag,pObject,Type,hr) (void(0))

DWORD inline SetFilePointerLogged(
    HANDLE hFile, 
    LONG lDistanceToMove, 
    PLONG lpDistanceToMoveHigh, 
    DWORD dwMoveMethod, 
    DWORD dwReporterTag = DSHOWWMITAG_WIN32FILE)
{
    return SetFilePointer (hFile, lDistanceToMove, lpDistanceToMoveHigh, dwMoveMethod);
}


BOOL inline WriteFileLogged (
    HANDLE hFile, 
    LPCVOID lpBuffer, 
    DWORD nNumberOfBytesToWrite, 
    LPDWORD lpNumberOfBytesWritten, 
    LPOVERLAPPED lpOverlapped, 
    DWORD dwReporterTag = DSHOWWMITAG_WIN32FILE)
{
    return WriteFile (hFile, lpBuffer, nNumberOfBytesToWrite, lpNumberOfBytesWritten, lpOverlapped);
}


BOOL inline ReadFileLogged(
    HANDLE hFile, 
    LPVOID lpBuffer, 
    DWORD nNumberOfBytesToRead, 
    LPDWORD lpNumberOfBytesRead, 
    LPOVERLAPPED lpOverlapped, 
    DWORD dwReporterTag = DSHOWWMITAG_WIN32FILE)
{
    return ReadFile (hFile, lpBuffer, nNumberOfBytesToRead, lpNumberOfBytesRead, lpOverlapped);
}

#else   // !SHIP_BUILD

#define WMILOG_LEVEL_DISABLED 0
#define WMILOG_LEVEL_CRITICAL 1
#define WMILOG_LEVEL_WARNING  2
#define WMILOG_LEVEL_TRACE    3
#define WMILOG_LEVEL_VERBOSE  4

// Critical events
#define bWMILOG_AUDIORENDER_STARVATION  (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_CRITICAL)
#define bWMILOG_VIDEO_FRAME_GLITCH      (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_CRITICAL)
#define bWMILOG_DATA_DROP               (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_CRITICAL)
#define bWMILOG_STATE_CHANGE            (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_CRITICAL)
#define bWMILOG_CLOCK_SETTIME           (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_CRITICAL)

// Warning events
#define bWMILOG_MEDIATYPE_CHANGE        (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_WARNING)
#define bWMILOG_UNUSUAL_STREAMING_EVENT (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_WARNING)
#define bWMILOG_FILE_OPEN               (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_WARNING)
#define bWMILOG_NETSOURCE               (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_WARNING)
#define bWMILOG_ADJUST_SAMPLE_TIME      (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_WARNING)
#define bWMILOG_TIMER_LATE              (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_WARNING)

// Trace events
#define bWMILOG_BUFFER                  (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_TRACE)
#define bWMILOG_OBJECT                  (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_TRACE)
#define bWMILOG_DISKIO_REQUEST          (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_TRACE)
#define bWMILOG_DISKIO_COMPLETE         (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_TRACE)
#define bWMILOG_RENDERER_REND           (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_TRACE)
#define bWMILOG_MUXER                   (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_TRACE)

// Need to use events (warning level)
#define bWMILOG_AUDIORENDERER_BUFFERFULLNESS (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_WARNING)
#define bWMILOG_SOURCERESOLUTION             (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_WARNING)
#define bWMILOG_QM                           (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_WARNING)
#define bWMILOG_VIDEO_RENDER                 (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_WARNING)
#define bWMILOG_AUDIO_RENDER                 (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_WARNING)
#define bWMILOG_GRAPH                        (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_WARNING)

// Need to use events (verbose level)
#define bWMILOG_CLOCK_GETTIME           (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_VERBOSE)
#define bWMILOG_LOCK                    (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_VERBOSE)
#define bWMILOG_STREAM                  (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_VERBOSE)
#define bWMILOG_HTTP_BYTESTREAM         (g_WmiInfo.ucWmiEnableLevel >= WMILOG_LEVEL_VERBOSE)


void inline WMILOG_OBJECT( 
    DWORD dwReporterTag, 
    UCHAR lActionType, 
    PVOID pObject, 
    DSHOWWMI_OBJECT_TYPE lObjectType )
{
    if ( bWMILOG_OBJECT )
    {
        DSHOWPERFINFO_OBJECT    perfData;
        FILL_DSHOWPERFINFO_COMMON( perfData, dwReporterTag, DSHOWWMIGUID_OBJECT_EVENT, lActionType, pObject);
        perfData.eObjectType = lObjectType;
        TraceEvent(g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader);
    }
}

void inline WMILOG_AUDIORENDER_STARVATION(
    DWORD dwReporterTag, 
    PVOID pObject, 
    DWORDLONG ullCount )
{
    if ( bWMILOG_AUDIORENDER_STARVATION )
    {
        DSHOWPERFINFO_AUDIORENDERER_STARVATION    perfData;
        FILL_DSHOWPERFINFO_COMMON( perfData, dwReporterTag, DSHOWWMIGUID_AUDIORENDERER_STARVATION_EVENT, 0, pObject);
        perfData.ullByteCount               = (ullCount);
        TraceEvent(g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader);
   }
}

void inline WMILOG_VIDEO_FRAME_GLITCH(
      DWORD dwReporterTag, 
      PVOID pObject, 
      DWORD dwObjectID,
      PVOID pSample, 
      LONGLONG llSampleTime, 
      LONGLONG llTargetSystemTime, 
      LONGLONG llOffset)
{
    if ( bWMILOG_VIDEO_FRAME_GLITCH )
    {
        DSHOWPERFINFO_VIDEO_FRAME_GLITCH    perfData;
        FILL_DSHOWPERFINFO_COMMON( perfData, dwReporterTag, DSHOWWMIGUID_VIDEO_FRAME_GLITCH_EVENT, 0, pObject);
        perfData.pSample                    = (DSHOWWMI_PTR)pSample;
        perfData.dwObjectID                 = dwObjectID;
        perfData.llSampleTime               = llSampleTime;
        perfData.llTargetSystemTime         = llTargetSystemTime;
        perfData.llOffset                   = llOffset;
        TraceEvent(g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader);
   }
}

void inline WMILOG_DATA_DROP(
    DWORD dwReporterTag,
    DWORD dwDataType,
    PVOID pObject,
    DWORD dwObjectID,
    LONGLONG llSampleTime,
    DWORD cbDropped,
    DWORD dwReasons)
{
    if ( bWMILOG_DATA_DROP )
    {
        DSHOWPERFINFO_DATA_DROP    perfData;
        FILL_DSHOWPERFINFO_COMMON( perfData, dwReporterTag, DSHOWWMIGUID_DATA_DROP_EVENT, 0, pObject);
        perfData.dwDataType                 = dwDataType;
        perfData.llSampleTime               = llSampleTime;
        perfData.cbDropped                  = cbDropped;
        perfData.dwReasons                  = dwReasons;
        perfData.dwObjectID                 = dwObjectID;
        TraceEvent(g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader);
    }
}

void inline WMILOG_CLOCK_GETTIME( 
    DWORD dwReporterTag, 
    PVOID pObject, 
    LONGLONG llTime )
{
    if ( bWMILOG_CLOCK_GETTIME )
    {
        DSHOWPERFINFO_CLOCK_GETTIME perfData;
        FILL_DSHOWPERFINFO_COMMON( perfData, dwReporterTag, DSHOWWMIGUID_CLOCK_GETTIME_EVENT, 0, pObject);
        perfData.llClockTime                = llTime;
        TraceEvent(g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader);
    }
}

void inline WMILOG_CLOCK_SETTIME( 
     DWORD dwReporterTag, 
     PVOID pObject, 
     LONGLONG llTime, 
     LONGLONG llDelta )
{
    if ( bWMILOG_CLOCK_SETTIME )
    {
        DSHOWPERFINFO_CLOCK_SETTIME perfData;
        FILL_DSHOWPERFINFO_COMMON( perfData, dwReporterTag, DSHOWWMIGUID_CLOCK_SETTIME_EVENT, 0, pObject);
        perfData.llDelta                    = llDelta;
        perfData.llClockTime                = llTime;
        TraceEvent(g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader);
    }
}

void inline WMILOG_STREAM( 
    DWORD dwReporterTag, 
    DWORD lStreamType, 
    PVOID pObject )
{
    if ( bWMILOG_STREAM )
    {
        DSHOWPERFINFO_STREAM    perfData;
        FILL_DSHOWPERFINFO_COMMON( perfData, dwReporterTag, DSHOWWMIGUID_STREAM_EVENT, (UCHAR)lStreamType, pObject);
        TraceEvent(g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader);
    }
}

void inline WMILOG_MEDIATYPE_CHANGE( 
    DWORD dwReporterTag, 
    PVOID pObject, 
    PVOID pStream, 
    DWORD cbNewType, 
    __in_bcount(cbNewType) const BYTE *pbNewType )
{
    if ( bWMILOG_MEDIATYPE_CHANGE )
    {
        DSHOWPERFINFO_MEDIATYPE_CHANGE    perfData;
        FILL_DSHOWPERFINFO_COMMON( perfData, dwReporterTag, DSHOWWMIGUID_MEDIATYPE_CHANGE_EVENT, 0, pObject);
        perfData.pStream        = (DSHOWWMI_PTR) pStream;
        perfData.cbNewType      = cbNewType;
        memcpy(perfData.pbNewType, pbNewType, cbNewType > MAXMEDIATYPE ? MAXMEDIATYPE : cbNewType);
        TraceEvent(g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader);
    }
}


void inline WMILOG_BUFFER(
    DWORD       dwReporterTag,
    PVOID       pObject,
    UCHAR       Type,
    DWORD       dwObjectCategory,
    PVOID       pStream,
    LONGLONG    llTimestamp,
    PVOID       pClock,
    PVOID       pSample,
    DWORD       cbTotalBuffer,
    DWORD       cbUsedBuffer,
    LONGLONG    llDuration )
{
    //
    // If we were using flags, the condition (g_ulWmiEnableFlags &  DSHOW_WMI_FLAG_SAMPLES)
    // could be checked in addition to the level condition being checked here.
    //
    if ( bWMILOG_BUFFER )
    {
        DSHOWPERFINFO_BUFFER    perfData;
        FILL_DSHOWPERFINFO_COMMON( perfData, dwReporterTag, DSHOWWMIGUID_BUFFER_EVENT, Type, pObject);
        perfData.dwObjectCategory           = dwObjectCategory;
        perfData.pStream                    = (DSHOWWMI_PTR) pStream;
        perfData.llTimestamp                = llTimestamp;
        perfData.pBuffer                    = (DSHOWWMI_PTR) pSample;
        perfData.cbUsedBuffer               = cbUsedBuffer;
        perfData.llDuration                 = llDuration;
        TraceEvent(g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader);
    }
}


void inline WMILOG_UNUSUAL_STREAMING_EVENT(
    DWORD dwReporterTag,
    UCHAR Type,
    PVOID pObject,
    DWORD dwEvent)
{
    if ( bWMILOG_UNUSUAL_STREAMING_EVENT )
    {
        DSHOWPERFINFO_UNUSUAL_STREAMING_EVENT perfData;
        FILL_DSHOWPERFINFO_COMMON( perfData, dwReporterTag, DSHOWWMIGUID_UNUSUAL_STREAMING_EVENT, Type, pObject);
        perfData.dwEvent      = (dwEvent);
        TraceEvent(g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader);
    }
}

void inline WMILOG_FILE_OPEN( 
    DWORD dwReporterTag, 
    PVOID pObject, 
    HANDLE h, 
    LPCWSTR pwszName )
{
    if ( bWMILOG_FILE_OPEN )
    {
        DWORD dwLen = min(lstrlenW(pwszName), MAX_PATH);
        DWORD dwSize = sizeof(DSHOWPERFINFO_FILE_OPEN) + dwLen * sizeof(WCHAR);
        BYTE *pb = new BYTE[dwSize];
        if (pb)
        {
            DSHOWPERFINFO_FILE_OPEN *pperfData = (DSHOWPERFINFO_FILE_OPEN *)pb;
            FILL_DSHOWPERFINFO_COMMON_PTR(pperfData, dwSize, dwReporterTag, DSHOWWMIGUID_FILE_OPEN_EVENT, 0, pObject);
            pperfData->hFile = (DSHOWWMI_HANDLE) (h);
            CopyMemory(pperfData->szName, (pwszName), dwLen * sizeof(WCHAR));
            pperfData->szName[dwLen] = 0;
            TraceEvent(g_WmiInfo.hWmiTrace, &pperfData->hdr.wmiHeader);
            delete [] pb;
        }
    }
}

void inline WMILOG_FILE_CLOSE( 
    DWORD dwReporterTag, 
    PVOID pObject, 
    HANDLE h, 
    LPCWSTR pwszName )
{
    if ( bWMILOG_FILE_OPEN )
    {
        DWORD dwLen = pwszName ? min(lstrlenW(pwszName), MAX_PATH) : 0;
        DWORD dwSize = sizeof(DSHOWPERFINFO_FILE_CLOSE) + dwLen * sizeof(WCHAR);
        BYTE *pb = new BYTE[dwSize];
        if (pb)
        {
            DSHOWPERFINFO_FILE_OPEN *pperfData = (DSHOWPERFINFO_FILE_OPEN *)pb;
            FILL_DSHOWPERFINFO_COMMON_PTR(pperfData, dwSize, dwReporterTag, DSHOWWMIGUID_FILE_OPEN_EVENT, 0, pObject);
            pperfData->hFile = (DSHOWWMI_HANDLE) (h);
            if (dwLen)
            {
                CopyMemory(pperfData->szName, (pwszName), dwLen * sizeof(WCHAR));
            }
            pperfData->szName[dwLen] = 0;
            TraceEvent(g_WmiInfo.hWmiTrace, &pperfData->hdr.wmiHeader);
            delete [] pb;
        }
    }
}

void inline WMILOG_DISKIO_REQUEST( 
    DWORD dwReporterTag, 
    UCHAR Type, 
    PVOID pObject, 
    HANDLE h, 
    QWORD qwOffset, 
    DWORD bCount )
{
    if ( bWMILOG_DISKIO_REQUEST )
    {
        DSHOWPERFINFO_DISKIO_REQUEST    perfData;
        FILL_DSHOWPERFINFO_COMMON( perfData, dwReporterTag, DSHOWWMIGUID_DISKIO_REQUEST_EVENT, Type, pObject);
        perfData.hFile                      = (DSHOWWMI_HANDLE) (h);
        perfData.llOffset                   = qwOffset;
        perfData.byteCount                  = (bCount);
        TraceEvent(g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader);
    }
}

void inline WMILOG_DISKIO_COMPLETE( 
    DWORD dwReporterTag, 
    UCHAR Type, 
    PVOID pObject, 
    HANDLE h )
{
    if ( bWMILOG_DISKIO_COMPLETE )
    {
        DSHOWPERFINFO_DISKIO_COMPLETE    perfData;
        FILL_DSHOWPERFINFO_COMMON( perfData, dwReporterTag, DSHOWWMIGUID_DISKIO_COMPLETE_EVENT, Type, pObject);
        perfData.hFile                      = (DSHOWWMI_HANDLE) (h);
        TraceEvent(g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader);
    }
}


void inline WMILOG_RENDERER_REND( 
    DWORD dwReporterTag, 
    UCHAR Type, 
    PVOID pObject, 
    PVOID pSample, 
    LONGLONG llTime, 
    LONGLONG llDuration, 
    LONGLONG llClock )
{
    if ( bWMILOG_RENDERER_REND )
    {
        DSHOWPERFINFO_RENDERER_REND perfData;
        FILL_DSHOWPERFINFO_COMMON( perfData, dwReporterTag, DSHOWWMIGUID_RENDER_REND_EVENT, Type, pObject);
        perfData.pSample            = (DSHOWWMI_PTR)(pSample);
        perfData.llSampleTime       = (llTime);
        perfData.llSampleDuration   = (llDuration);
        perfData.llClockTime        = (llClock);
        TraceEvent(g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader);
    }
}

#define WMILOG_RENDERER_MIX( dwReporterTag, Type, pObject, pSample, llTime, llDuration, llClock ) \
    WMILOG_RENDERER_REND( dwReporterTag, Type, pObject, pSample, llTime, llDuration, llClock )

void inline WMILOG_AUDIORENDERER_BUFFERFULLNESS( 
    DWORD dwReporterTag, 
    PVOID pObject, 
    DWORD dwFullness)
{
    if (bWMILOG_AUDIORENDERER_BUFFERFULLNESS)
    {
        DSHOWPERFINFO_AUDIORENDERER_BUFFERFULLNESS perfData;
        FILL_DSHOWPERFINFO_COMMON( perfData, dwReporterTag, DSHOWWMIGUID_AUDIORENDERER_BUFFERFULLNESS_EVENT, 0, pObject);
        perfData.msBuffer = dwFullness;
        TraceEvent(g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader);
    }
}

void inline WMILOG_STATE_CHANGE( 
    DWORD dwReporterTag, 
    PVOID pObject, 
    DWORD dwStateChange)
{
    if ( bWMILOG_STATE_CHANGE )
    {
        DSHOWPERFINFO_STATE_CHANGE perfData;
        FILL_DSHOWPERFINFO_COMMON( perfData, dwReporterTag, DSHOWWMIGUID_STATE_CHANGE_EVENT, (UCHAR)dwStateChange, pObject);
        TraceEvent(g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader);
    }
}

void inline WMILOG_NETSOURCE( 
    DWORD dwReporterTag, 
    PVOID pObject, 
    UCHAR Type, 
    DWORD Arg1, 
    DWORD Arg2 )
{
    if ( bWMILOG_NETSOURCE)
    {
        DSHOWPERFINFO_NETSOURCE  perfData;
        FILL_DSHOWPERFINFO_COMMON( perfData, dwReporterTag, DSHOWWMIGUID_NETSOURCE_EVENT, Type, pObject);
        perfData.Arg1 = Arg1;
        perfData.Arg2 = Arg2;
        TraceEvent(g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader);
    }
}

void inline WMILOG_HTTP_BYTESTREAM( 
    DWORD dwReporterTag, 
    PVOID pObject, 
    UCHAR Type )
{
    if ( bWMILOG_HTTP_BYTESTREAM)
    {
        DSHOWPERFINFO_HTTP_BYTESTREAM perfData;
        FILL_DSHOWPERFINFO_COMMON( perfData, dwReporterTag, DSHOWWMIGUID_HTTP_BYTESTREAM_EVENT, Type, pObject );
        TraceEvent( g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader );
    }
}

void inline WMILOG_SOURCERESOLUTION(
    DWORD dwReporterTag, 
    PVOID pObject, 
    UCHAR Type, 
    LPCWSTR pwszURL, 
    PVOID pObjectCreated, 
    HRESULT hr )
{
    if ( bWMILOG_SOURCERESOLUTION )
    {
        DSHOWPERFINFO_SOURCERESOLUTION perfData;
        FILL_DSHOWPERFINFO_COMMON( perfData, dwReporterTag, DSHOWWMIGUID_SOURCERESOLUTION_EVENT, Type, pObject );
        perfData.pwszURL = pwszURL;
        perfData.pObjectCreated = pObjectCreated;
        perfData.hr = hr;
        TraceEvent( g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader );
    }
}

void inline WMILOG_QM(
    DWORD dwReporterTag, 
    PVOID pObject, 
    UCHAR Type, 
    DWORD dwKnobId, 
    DWORD dwPrevLevel, 
    DWORD dwNewLevel, 
    LONGLONG llDropTime )
{
    if ( bWMILOG_QM )
    {
        DSHOWPERFINFO_QM perfData;
        FILL_DSHOWPERFINFO_COMMON( perfData, dwReporterTag, DSHOWWMIGUID_QM_EVENT, Type, pObject );
        perfData.dwKnobId = dwKnobId;
        perfData.dwPrevLevel = dwPrevLevel;
        perfData.dwNewLevel = dwNewLevel;
        perfData.llDropTime = llDropTime;
        TraceEvent( g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader );
    }
}

void inline WMILOG_ADJUST_SAMPLE_TIME(
    DWORD       dwReporterTag,
    PVOID       pObject,
    UCHAR       ucType,
    LONGLONG    llOriginalSampleTime,
    LONGLONG    llAdjustment )
{
    if ( bWMILOG_ADJUST_SAMPLE_TIME )
    {
        DSHOWPERFINFO_ADJUST_SAMPLE_TIME    perfData;

        FILL_DSHOWPERFINFO_COMMON(
                perfData,
                dwReporterTag,
                DSHOWWMIGUID_ADJUST_SAMPLE_TIME_EVENT,
                ucType,
                pObject);

        perfData.llOriginalSampleTime = llOriginalSampleTime;
        perfData.llAdjustment = llAdjustment;
        TraceEvent(g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader);
    }
}

void inline WMILOG_VIDEO_RENDER(
    DWORD       dwReporterTag,
    PVOID       pObject,
    PVOID       pSample,
    LONGLONG    llSampleTime,
    LONGLONG    llSampleDuration,
    LONGLONG    llClockTime,
    HANDLE      hwndVideo,
    DWORD       dwRefreshRate,
    DWORD       dwWidth,
    DWORD       dwHeight,
    DWORD       dwLeft,
    DWORD       dwTop,
    DWORD       dwRight,
    DWORD       dwBottom,
    DWORD       dwLeft1,
    DWORD       dwTop1,
    DWORD       dwRight1,
    DWORD       dwBottom1
    )
{
    if ( bWMILOG_VIDEO_RENDER )
    {
        DSHOWPERFINFO_VIDEO_RENDER    perfData;

        FILL_DSHOWPERFINFO_COMMON(
                perfData,
                dwReporterTag,
                DSHOWWMIGUID_VIDEO_RENDER_EVENT,
                0,
                pObject);

        perfData.hwndVideo = (DSHOWWMI_HANDLE)hwndVideo;
        perfData.llSampleTime = llSampleTime;
        perfData.llSampleDuration = llSampleDuration;
        perfData.llClockTime = llClockTime;
        perfData.pSample = (DSHOWWMI_PTR)pSample;
        perfData.dwRefreshRate = dwRefreshRate;
        perfData.dwWidth = dwWidth;
        perfData.dwHeight = dwHeight;
        perfData.dwLeft = dwLeft;
        perfData.dwTop = dwTop;
        perfData.dwRight = dwRight;
        perfData.dwBottom = dwBottom;
        perfData.dwLeft1 = dwLeft1;
        perfData.dwTop1 = dwTop1;
        perfData.dwRight1 = dwRight1;
        perfData.dwBottom1 = dwBottom1;

        TraceEvent(g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader);
    }
}

void inline
WMILOG_AUDIO_RENDER(
    DWORD dwReporterTag,
    PVOID pObject,
    PVOID pSample,
    LONGLONG llTime,
    LONGLONG llDuration,
    LONGLONG llMasterTime,
    LONGLONG llDeviceTime)
{
    if ( bWMILOG_AUDIO_RENDER )
    {
        DSHOWPERFINFO_AUDIO_RENDER perfData;
        FILL_DSHOWPERFINFO_COMMON( perfData, dwReporterTag, DSHOWWMI_GUID_AUDIO_RENDER_EVENT, 0, pObject);
        perfData.pSample            = (DSHOWWMI_PTR)(pSample);
        perfData.llSampleTime       = (llTime);
        perfData.llSampleDuration   = (llDuration);
        perfData.llMasterTime       = (llMasterTime);
        perfData.llDeviceTime       = (llDeviceTime);
        TraceEvent(g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader);
    }
}


void inline WMILOG_MUXER(   DWORD dwReportTag,
                            PVOID pObject,
                            UCHAR Type,
                            DWORD wStreamNumber,
                            LONGLONG llSampleTime,
                            DWORD cbSample,
                            LONGLONG llPacketNumber,
                            LONGLONG llPacketSendTime,
                            DWORD cbPacket )
{
    if( bWMILOG_MUXER )
    {
        DSHOWPERFINFO_MUXER perfData;
        FILL_DSHOWPERFINFO_COMMON( perfData, dwReportTag, DSHOWWMIGUID_MUXER_EVENT, Type, pObject );
        perfData.wStreamNumber = wStreamNumber;
        perfData.llSampleTime = llSampleTime;
        perfData.cbSample = cbSample;
        perfData.llPacketNumber = llPacketNumber;
        perfData.llPacketSendTime = llPacketSendTime;
        perfData.cbPacket = cbPacket;
        TraceEvent( g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader );
    }
}


void inline WMILOG_TIMER_LATE(
    DWORD       dwReporterTag,
    PVOID       pObject,
    DWORD       dwObjectID,
    LONGLONG    llEventTime,
    LONGLONG    llLateBy)
{
    if( bWMILOG_TIMER_LATE )
    {
        DSHOWPERFINFO_TIMER_LATE perfData;
        FILL_DSHOWPERFINFO_COMMON( perfData, dwReporterTag, DSHOWWMIGUID_TIMER_LATE_EVENT, 0, pObject );
        perfData.llEventTime = llEventTime;
        perfData.llLateBy = llLateBy;
        perfData.dwObjectID = dwObjectID;
        TraceEvent(g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader);
    }
}


void inline WMILOG_LOCK(
    DWORD      dwReporterTag,
    PVOID      pObject,
    UCHAR      Type,     // Acquire, Acquired, Released
    PVOID      pLockObject)
{
    if( bWMILOG_LOCK )
    {
        DSHOWPERFINFO_LOCK perfData;
        FILL_DSHOWPERFINFO_COMMON( perfData, dwReporterTag, DSHOWWMIGUID_LOCK_EVENT, Type, pObject );
        perfData.pLockObject = pLockObject;
        TraceEvent(g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader);
    }
}


void inline WMILOG_GRAPH(
    DWORD dwReporterTag, 
    PVOID pObject, 
    UCHAR Type, 
    HRESULT hr )
{
    if ( bWMILOG_GRAPH ) {
        DSHOWPERFINFO_GRAPH perfData;
        FILL_DSHOWPERFINFO_COMMON( perfData, dwReporterTag, DSHOWWMIGUID_GRAPH_EVENT, Type, pObject );
        perfData.hr = hr;
        TraceEvent( g_WmiInfo.hWmiTrace, &perfData.hdr.wmiHeader );
    }
}

DWORD inline SetFilePointerLogged(
    HANDLE hFile, 
    LONG lDistanceToMove, 
    PLONG lpDistanceToMoveHigh, 
    DWORD dwMoveMethod, 
    DWORD dwReporterTag = DSHOWWMITAG_WIN32FILE)
{
    DWORD dwRet;

    if (bWMILOG_DISKIO_REQUEST)
    {
        QWORD qwPosition;

        qwPosition = lpDistanceToMoveHigh ? ((QWORD)*lpDistanceToMoveHigh) << 32: 0;
        qwPosition|= lDistanceToMove;

        WMILOG_DISKIO_REQUEST(dwReporterTag,
                                 DSHOWWMI_DISKIO_SEEK,
                                 NULL,
                                 hFile,
                                 qwPosition,
                                 (DWORD)-1);
    }

    dwRet = SetFilePointer (hFile, lDistanceToMove, lpDistanceToMoveHigh, dwMoveMethod);

    // WMILOG: log how long as well
    WMILOG_DISKIO_COMPLETE(dwReporterTag,
                           DSHOWWMI_DISKIO_SEEK,
                           NULL,
                           hFile);
    return dwRet;
}


BOOL inline WriteFileLogged (
    HANDLE hFile, 
    LPCVOID lpBuffer, 
    DWORD nNumberOfBytesToWrite, 
    LPDWORD lpNumberOfBytesWritten, 
    LPOVERLAPPED lpOverlapped, 
    DWORD dwReporterTag = DSHOWWMITAG_WIN32FILE)
{
    BOOL bRet;

    WMILOG_DISKIO_REQUEST(dwReporterTag,
                          DSHOWWMI_DISKIO_WRITE,
                          NULL,
                          hFile,
                          (QWORD)-1,
                          nNumberOfBytesToWrite);

    bRet = WriteFile (hFile, lpBuffer, nNumberOfBytesToWrite, lpNumberOfBytesWritten, lpOverlapped);

    // WMILOG: log how long as well
    WMILOG_DISKIO_COMPLETE(dwReporterTag,
                           DSHOWWMI_DISKIO_WRITE,
                           NULL,
                           hFile);
    return bRet;
}


BOOL inline ReadFileLogged(
    HANDLE hFile, 
    LPVOID lpBuffer, 
    DWORD nNumberOfBytesToRead, 
    LPDWORD lpNumberOfBytesRead, 
    LPOVERLAPPED lpOverlapped, 
    DWORD dwReporterTag = DSHOWWMITAG_WIN32FILE)
{
    BOOL bRet;

    WMILOG_DISKIO_REQUEST(dwReporterTag,
                             DSHOWWMI_DISKIO_READ,
                             NULL,
                             hFile,
                             (QWORD)-1,
                             nNumberOfBytesToRead);

    bRet = ReadFile (hFile, lpBuffer, nNumberOfBytesToRead, lpNumberOfBytesRead, lpOverlapped);

    // WMILOG: log how long as well
    WMILOG_DISKIO_COMPLETE(dwReporterTag,
                           DSHOWWMI_DISKIO_READ,
                           NULL,
                           hFile);
    return bRet;
}

#endif // !SHIP_BUILD


