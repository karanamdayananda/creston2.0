        ��  ��                  �      �� ��     0	          9 U s a g e : 
 	 m s m q a d m   < c o m m a n d >   < p a r a m e t e r s > 
 	 o r 
 	 m s m q a d m   h e l p   � C o m m a n d s : 
 	 h e l p   -   p r i n t   t h i s   t e x t 
 	 m a n   -   m a n u a l   c o m m a n d   m o d e 
 	 e n u m   q u e u e s   -   e n u m e r a t e   q u e u e s 
 	 e n u m   m e s s a g e s   < q u e u e   n u m b e r >   -   e n u m e r a t e   m e s s a g e s   i n   a   q u e u e    E r r o r :   l o w   o n   m e m o r y .    E r r o r :   C a n n o t   o p e n   f i l e   % s 
   ' M S M Q   s e r v e r   n o t   p r e s e n t   o n   t h i s   s y s t e m   7 I l l e g a l   m e s s a g e   n u m b e r   % s   -   c h e c k   m e s s a g e   e n u m e r a t i o n .   2 I l l e g a l   q u e u e   n u m b e r   % s   -   c h e c k   q u e u e   e n u m e r a t i o n   3 S y n t a x   e r r o r .   U s e   h e l p   t o   r e t r i e v e   c o m m a n d   s y n t a x .   + O p e r a t i o n   f a i l e d .   N o   e r r o r   c o d e   a v a i l a b l e .   " O p e r a t i o n   f a i l e d .   E r r o r   c o d e   % 0 8 x    O p e n   q u e u e s :      I n c o r r e c t   g u i d   s p e c   % s .    C a n n o t   c r e a t e   % s .    F i l e   % s   a l r e a d y   e x i s t s .     �
      �� ��     0	         Y    N   } D i r e c t o r y   % s   e x i s t s   a n d   m a y   c o n t a i n   i n f o r m a t i o n   t h a t   w i l l   c o r r u p t   n e w   i n s t a l l a t i o n .   P l e a s e   b a c k u p   a n d   r e m o v e   t h i s   d i r e c t o r y .   0 E r r o r :   C a n n o t   c r e a t e   r e g i s t r y   k e y .   S t a t u s   % 0 8 x .   , E r r o r :   C a n n o t   s e t   % s   k e y   v a l u e .   S t a t u s   % 0 8 x   ( E r r o r :   C a n n o t   i n i t   s o c k e t s .   S t a t u s   % 0 8 x    E r r o r :   C a n n o t   g e t   h o s t   i n f o .   w U s e : 
 	 r e g i s t e r   { g u i d   < g u i d > ,   d i r   < b a s e   d i r e c t o r y > ,   f r s   < s e r v e r   n a m e > ,   b i n a r y ,   s m t p ,   t r u s t }   t o   s e t   r e g i s t r y   p a r a m e t e r s .   � U s e : 
 	 r e g i s t e r   c h a n g e   { p o r t   < n u m b e r > ,   p i n g p o r t   < n u m b e r > ,   n e t r e t r y   < n u m b e r > , 
 	 	 c o n n e c t r e t r y   < n u m b e r s > ,   q u o t a   < n u m b e r ,   i n   K > ,   d i r   < b a s e d i r > ,   f r s   < f r s   s e r v e r > }     t o   c h a n g e   r e g i s t r y   p a r a m e t e r s .   0 C a n n o t   o p e n   r e g i s t r y   k e y .   M u s t   c r e a t e   i t   f i r s t .   � 	 e m p t y   q u e u e   < q u e u e   n u m b e r >   -   e m p t y   q u e u e 
 	 d e l e t e   q u e u e   < q u e u e   n u m b e r >   -   d e l e t e   q u e u e 
 	 d e l e t e   m e s s a g e   < m e s s a g e   n u m b e r >   -   d e l e t e   m e s s a g e   � 	 n e t   c o n n e c t   -   f o r c e   r e s c a n n i n g   f o r   c o n n e c t i o n s 
 	 n e t   d i s c o n n e c t   -   f o r c e   a l l   c o n n e c t i o n s   s t o p p e d 
 	 n e t   t r a c k   o n / o f f   -   e n a b l e / d i s a b l e   n e t w o r k   t r a c k i n g   � 	 s t o p   -   s t o p   M S M Q   s e r v i c e 
 	 s t a r t   -   s t a r t   M S M Q   s e r v i c e 
 	 c o n s o l e   -   s t a r t   M S M Q   c o n s o l e 
 	 s t a t u s   -   o b t a i n   m a c h i n e   s t a t u s 
 	 t i d y   -   f o r c e   r e s o u r c e   m a i n t e n a n c e   � 	 r e g i s t e r   -   c o n f i g u r e   M S M Q   s e r v i c e 
 	 l o g s i z e   < n u m b e r >   -   s i z e   o f   l o g   f i l e   i n   K   ( 0   i s   i n f i n i t e ) 
 . . . w h e r e   m e s s a g e / q u e u e   n u m b e r s   a r e   t h e s e   i n   l a t e s t   e n u m e r a t i o n . 
 F l a g s : 
 	 - s   -   s i l e n t   e x e c u t i o n ,   n o   o u t p u t .   ; P l e a s e   r e s t a r t   M S M Q   s e r v i c e   f o r   t h e   c h a n g e   t o   t a k e   e f f e c t .   ( U s a g e : 
 	 d e b u g   [ o u t p u t ]   { o n ,   o f f }   < l i s t >           �� ��     0	         L i s t   f o r   o u t p u t :    L i s t   f o r   m o d e s :   } o r , 
 	 r e g i s t e r   [ u n ] i n s t a l l   < s e r v i c e >   t o   i n s t a l l / u n i n s t a l l   M S M Q 
 	 	   a n d   w h e t h e r   t o   i n s t a l l   M S M Q   a s   d r i v e r   ( d e f a u l t )   o r   s e r v i c e .   Y o r , 
 	 r e g i s t e r   c l e a n u p   t o   r e m o v e   a l l   t r a c e s   o f   M S M Q   ( i n c l u d i n g   m e s s a g e s )   f r o m   t h e   s y s t e m .    E r r o r :   I n v a l i d   F R S   n a m e .   H E r r o r :   R e g i s t r y   k e y   a l r e a d y   e x i s t s .   U s e   ' r e g i s t e r   c h a n g e '   t o   m o d i f y   i t .   � 	 e n a b l e   { s r m p ,   b i n a r y ,   t r u s t }   -   e n a b l e   M S M Q   p r o t o c o l s   a n d   n e t w o r k   t r u s t   l e v e l 
 	 d i s a b l e   { s r m p ,   b i n a r y ,   t r u s t }   -   d i s a b l e   M S M Q   p r o t o c o l s   o r   m a k e   n e t w o r k   u n t r u s t e d 
                       