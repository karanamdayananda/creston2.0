        ��  ��                  �      �� ��    0	         A p p l i c a t i o n    F o l d e r    % s   F i l e    F i l e   	 S h o r t c u t    S y s t e m   F o l d e r    U n k n o w n   T y p e   M T h e r e   i s   n o t   e n o u g h   m e m o r y .   P l e a s e   e x i t   s o m e   r u n n i n g   p r o g r a m s   a n d   t r y   a g a i n .    O u t   O f   M e m o r y   E r r o r    S h o r t c u t   t o   % s               ,      �� ��    0	         O p e n    S a v e   A s    U p    L i s t    D e t a i l s   2 F i l e   d o e s   n o t   e x i s t .     D o   y o u   w a n t   t o   c r e a t e   ' % s ' ?   S ' % s '   c o u l d   n o t   b e   f o u n d .     M a k e   s u r e   t h a t   t h e   c o r r e c t   p a t h   a n d   f i l e   n a m e   a r e   g i v e n .   " ' % s ' 
 T h i s   f i l e   n a m e   i s   n o t   v a l i d .   R T h e   f o l d e r   ' % s '   i s   n o t   a c c e s s i b l e . 
 P l e a s e   v e r i f y   t h a t   t h e   c o r r e c t   p a t h   w a s   g i v e n .   : T h e   p a t h   i s   t o o   l o n g .   P l e a s e   s e l e c t   a   s h o r t e r   f i l e   n a m e . 
   6 ' % s '   T h i s   f i l e   a l r e a d y   e x i s t s .   R e p l a c e   e x i s t i n g   f i l e ?             �      �� ��    0	         E r r o r   c o d e   % d . 
 
 % s    C o p y   
 % 1   -   % 3 % 2   / T h e   f i l e   ' % s '   i s   t o o   b i g   f o r   t h e   R e c y c l e   B i n .      N e w   F o l d e r    % s   ( % d )                 M T h e r e   i s   n o t   e n o u g h   m e m o r y .   P l e a s e   e x i t   s o m e   r u n n i n g   p r o g r a m s   a n d   t r y   a g a i n .   ) 
 
 F i l e s   l o c a t e d   i n   R O M   c a n n o t   b e   c o p i e d .   p A c c e s s   i s   d e n i e d . 
 
 M a k e   s u r e   t h e   d i s k   i s   n o t   f u l l   o r   w r i t e - p r o t e c t e d   a n d   t h a t   t h e   f i l e   i s   n o t   c u r r e n t l y   i n   u s e .   �      �� ��    0	          n ' % s '   i s   a   s y s t e m   f o l d e r   a n d   i s   r e q u i r e d   f o r   W i n d o w s   t o   r u n   p r o p e r l y . 
 
 I t   c a n n o t   b e   d e l e t e d ,   m o v e d   o r   r e n a m e d .         K 
 
 T h e   d e s t i n a t i o n   f o l d e r   a l r e a d y   c o n t a i n s   a   f i l e   o r   f o l d e r   w i t h   t h a t   n a m e .                         �       �� ��    0	                I 
 
 A   f i l e   n a m e   c a n n o t   c o n t a i n   a n y   o f   t h e   f o l l o w i n g   c h a r a c t e r s : 
 \ / :   * ? " < > |                           �      �� ��    0	        ; 
 
 T h e   d e s t i n a t i o n   f o l d e r   i s   t h e   s a m e   a s   t h e   s o u r c e   f o l d e r .   > 
 
 T h e   d e s t i n a t i o n   f o l d e r   i s   a   s u b f o l d e r   o f   t h e   s o u r c e   f o l d e r .   d C a n n o t   r e a d   f r o m   t h e   s o u r c e   f i l e   o r   d i s k . 
 
 M a k e   s u r e   t h a t   t h e   c o r r e c t   p a t h   a n d   f i l e   n a m e   a r e   g i v e n .   g T h e r e   i s   n o t   e n o u g h   f r e e   d i s k   s p a c e . 
 
 D e l e t e   o n e   o r   m o r e   f i l e s   t o   f r e e   d i s k   s p a c e ,   a n d   t h e n   t r y   a g a i n .   U 
 
 T h e   f i l e   n a m e   y o u   s p e c i f i e d   i s   i n v a l i d   o r   t o o   l o n g .   S p e c i f y   a   d i f f e r e n t   f i l e   n a m e .   R T h e r e   h a s   b e e n   a   s h a r i n g   v i o l a t i o n 
 
 T h e   s o u r c e   o r   d e s t i n a t i o n   f i l e   m a y   b e   i n   u s e .                         �       �� ��    0	                               C a n n o t   c o p y   ' % s ' :      C a n n o t   d e l e t e   ' % s ' :        C a n n o t   m o v e   ' % s ' :      C a n n o t   r e n a m e   ' % s ' :     �       �� ��    0	        * 
 
 F i l e s   l o c a t e d   i n   R O M   c a n n o t   b e   d e l e t e d .   ( 
 
 F i l e s   l o c a t e d   i n   R O M   c a n n o t   b e   m o v e d .                               �       �� ��	    0	                    S F a i l e d   t o   i n i t i z e   t h e   R e c y c l e   B i n .   A l l   d e l e t e   o p e r a t i o n s   w i l l   n o t   b e   s a v e d   t o   d i s k                       *       �� ��Q    0	                               M e n u             