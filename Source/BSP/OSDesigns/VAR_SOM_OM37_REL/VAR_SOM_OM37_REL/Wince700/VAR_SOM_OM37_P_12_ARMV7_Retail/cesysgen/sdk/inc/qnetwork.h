

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0543 */
/* Compiler settings for qnetwork.odl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__


#ifndef __qnetwork_h__
#define __qnetwork_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IAMNetShowConfig_FWD_DEFINED__
#define __IAMNetShowConfig_FWD_DEFINED__
typedef interface IAMNetShowConfig IAMNetShowConfig;
#endif 	/* __IAMNetShowConfig_FWD_DEFINED__ */


#ifndef __IAMChannelInfo_FWD_DEFINED__
#define __IAMChannelInfo_FWD_DEFINED__
typedef interface IAMChannelInfo IAMChannelInfo;
#endif 	/* __IAMChannelInfo_FWD_DEFINED__ */


#ifndef __IAMNetworkStatus_FWD_DEFINED__
#define __IAMNetworkStatus_FWD_DEFINED__
typedef interface IAMNetworkStatus IAMNetworkStatus;
#endif 	/* __IAMNetworkStatus_FWD_DEFINED__ */


#ifndef __IAMExtendedSeeking_FWD_DEFINED__
#define __IAMExtendedSeeking_FWD_DEFINED__
typedef interface IAMExtendedSeeking IAMExtendedSeeking;
#endif 	/* __IAMExtendedSeeking_FWD_DEFINED__ */


#ifndef __IAMNetShowExProps_FWD_DEFINED__
#define __IAMNetShowExProps_FWD_DEFINED__
typedef interface IAMNetShowExProps IAMNetShowExProps;
#endif 	/* __IAMNetShowExProps_FWD_DEFINED__ */


#ifndef __IAMExtendedErrorInfo_FWD_DEFINED__
#define __IAMExtendedErrorInfo_FWD_DEFINED__
typedef interface IAMExtendedErrorInfo IAMExtendedErrorInfo;
#endif 	/* __IAMExtendedErrorInfo_FWD_DEFINED__ */


#ifndef __IAMMediaContent_FWD_DEFINED__
#define __IAMMediaContent_FWD_DEFINED__
typedef interface IAMMediaContent IAMMediaContent;
#endif 	/* __IAMMediaContent_FWD_DEFINED__ */


#ifndef __IAMMediaContent2_FWD_DEFINED__
#define __IAMMediaContent2_FWD_DEFINED__
typedef interface IAMMediaContent2 IAMMediaContent2;
#endif 	/* __IAMMediaContent2_FWD_DEFINED__ */


#ifndef __IAMMediaContentEx_FWD_DEFINED__
#define __IAMMediaContentEx_FWD_DEFINED__
typedef interface IAMMediaContentEx IAMMediaContentEx;
#endif 	/* __IAMMediaContentEx_FWD_DEFINED__ */


#ifndef __IAMNetShowPreroll_FWD_DEFINED__
#define __IAMNetShowPreroll_FWD_DEFINED__
typedef interface IAMNetShowPreroll IAMNetShowPreroll;
#endif 	/* __IAMNetShowPreroll_FWD_DEFINED__ */


#ifndef __IDShowPlugin_FWD_DEFINED__
#define __IDShowPlugin_FWD_DEFINED__
typedef interface IDShowPlugin IDShowPlugin;
#endif 	/* __IDShowPlugin_FWD_DEFINED__ */


#ifndef __IAMSecureMediaContent_FWD_DEFINED__
#define __IAMSecureMediaContent_FWD_DEFINED__
typedef interface IAMSecureMediaContent IAMSecureMediaContent;
#endif 	/* __IAMSecureMediaContent_FWD_DEFINED__ */


#ifdef __cplusplus
extern "C"{
#endif 



#ifndef __QuartzNetTypeLib_LIBRARY_DEFINED__
#define __QuartzNetTypeLib_LIBRARY_DEFINED__

/* library QuartzNetTypeLib */
/* [version][lcid][helpstring][uuid] */ 

typedef 
enum __MIDL___MIDL_itf_qnetwork_0001_0002_0001
    {	AM_EXSEEK_CANSEEK	= 1,
	AM_EXSEEK_CANSCAN	= 2,
	AM_EXSEEK_MARKERSEEK	= 4,
	AM_EXSEEK_SCANWITHOUTCLOCK	= 8,
	AM_EXSEEK_NOSTANDARDREPAINT	= 16,
	AM_EXSEEK_BUFFERING	= 32,
	AM_EXSEEK_SENDS_VIDEOFRAMEREADY	= 64
    } 	AMExtendedSeekingCapabilities;


DEFINE_GUID(LIBID_QuartzNetTypeLib,0x56a868b1,0x0ad4,0x11ce,0xb0,0x3a,0x00,0x20,0xaf,0x0b,0xa7,0x70);

#ifndef __IAMNetShowConfig_INTERFACE_DEFINED__
#define __IAMNetShowConfig_INTERFACE_DEFINED__

/* interface IAMNetShowConfig */
/* [object][dual][oleautomation][helpstring][uuid] */ 


DEFINE_GUID(IID_IAMNetShowConfig,0xFA2AA8F1,0x8B62,0x11d0,0xA5,0x20,0x00,0x00,0x00,0x00,0x00,0x00);

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("FA2AA8F1-8B62-11d0-A520-000000000000")
    IAMNetShowConfig : public IDispatch
    {
    public:
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_BufferingTime( 
            /* [retval][out] */ double *pBufferingTime) = 0;
        
        virtual /* [propput] */ HRESULT STDMETHODCALLTYPE put_BufferingTime( 
            /* [in] */ double BufferingTime) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_UseFixedUDPPort( 
            /* [retval][out] */ VARIANT_BOOL *pUseFixedUDPPort) = 0;
        
        virtual /* [propput] */ HRESULT STDMETHODCALLTYPE put_UseFixedUDPPort( 
            /* [in] */ VARIANT_BOOL UseFixedUDPPort) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_FixedUDPPort( 
            /* [retval][out] */ long *pFixedUDPPort) = 0;
        
        virtual /* [propput] */ HRESULT STDMETHODCALLTYPE put_FixedUDPPort( 
            /* [in] */ long FixedUDPPort) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_UseHTTPProxy( 
            /* [retval][out] */ VARIANT_BOOL *pUseHTTPProxy) = 0;
        
        virtual /* [propput] */ HRESULT STDMETHODCALLTYPE put_UseHTTPProxy( 
            /* [in] */ VARIANT_BOOL UseHTTPProxy) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_EnableAutoProxy( 
            /* [retval][out] */ VARIANT_BOOL *pEnableAutoProxy) = 0;
        
        virtual /* [propput] */ HRESULT STDMETHODCALLTYPE put_EnableAutoProxy( 
            /* [in] */ VARIANT_BOOL EnableAutoProxy) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_HTTPProxyHost( 
            /* [retval][out] */ BSTR *pbstrHTTPProxyHost) = 0;
        
        virtual /* [propput] */ HRESULT STDMETHODCALLTYPE put_HTTPProxyHost( 
            /* [in] */ BSTR bstrHTTPProxyHost) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_HTTPProxyPort( 
            /* [retval][out] */ long *pHTTPProxyPort) = 0;
        
        virtual /* [propput] */ HRESULT STDMETHODCALLTYPE put_HTTPProxyPort( 
            /* [in] */ long HTTPProxyPort) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_EnableMulticast( 
            /* [retval][out] */ VARIANT_BOOL *pEnableMulticast) = 0;
        
        virtual /* [propput] */ HRESULT STDMETHODCALLTYPE put_EnableMulticast( 
            /* [in] */ VARIANT_BOOL EnableMulticast) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_EnableUDP( 
            /* [retval][out] */ VARIANT_BOOL *pEnableUDP) = 0;
        
        virtual /* [propput] */ HRESULT STDMETHODCALLTYPE put_EnableUDP( 
            /* [in] */ VARIANT_BOOL EnableUDP) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_EnableTCP( 
            /* [retval][out] */ VARIANT_BOOL *pEnableTCP) = 0;
        
        virtual /* [propput] */ HRESULT STDMETHODCALLTYPE put_EnableTCP( 
            /* [in] */ VARIANT_BOOL EnableTCP) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_EnableHTTP( 
            /* [retval][out] */ VARIANT_BOOL *pEnableHTTP) = 0;
        
        virtual /* [propput] */ HRESULT STDMETHODCALLTYPE put_EnableHTTP( 
            /* [in] */ VARIANT_BOOL EnableHTTP) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMNetShowConfigVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMNetShowConfig * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMNetShowConfig * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMNetShowConfig * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAMNetShowConfig * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAMNetShowConfig * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAMNetShowConfig * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAMNetShowConfig * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BufferingTime )( 
            IAMNetShowConfig * This,
            /* [retval][out] */ double *pBufferingTime);
        
        /* [propput] */ HRESULT ( STDMETHODCALLTYPE *put_BufferingTime )( 
            IAMNetShowConfig * This,
            /* [in] */ double BufferingTime);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UseFixedUDPPort )( 
            IAMNetShowConfig * This,
            /* [retval][out] */ VARIANT_BOOL *pUseFixedUDPPort);
        
        /* [propput] */ HRESULT ( STDMETHODCALLTYPE *put_UseFixedUDPPort )( 
            IAMNetShowConfig * This,
            /* [in] */ VARIANT_BOOL UseFixedUDPPort);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_FixedUDPPort )( 
            IAMNetShowConfig * This,
            /* [retval][out] */ long *pFixedUDPPort);
        
        /* [propput] */ HRESULT ( STDMETHODCALLTYPE *put_FixedUDPPort )( 
            IAMNetShowConfig * This,
            /* [in] */ long FixedUDPPort);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UseHTTPProxy )( 
            IAMNetShowConfig * This,
            /* [retval][out] */ VARIANT_BOOL *pUseHTTPProxy);
        
        /* [propput] */ HRESULT ( STDMETHODCALLTYPE *put_UseHTTPProxy )( 
            IAMNetShowConfig * This,
            /* [in] */ VARIANT_BOOL UseHTTPProxy);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_EnableAutoProxy )( 
            IAMNetShowConfig * This,
            /* [retval][out] */ VARIANT_BOOL *pEnableAutoProxy);
        
        /* [propput] */ HRESULT ( STDMETHODCALLTYPE *put_EnableAutoProxy )( 
            IAMNetShowConfig * This,
            /* [in] */ VARIANT_BOOL EnableAutoProxy);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_HTTPProxyHost )( 
            IAMNetShowConfig * This,
            /* [retval][out] */ BSTR *pbstrHTTPProxyHost);
        
        /* [propput] */ HRESULT ( STDMETHODCALLTYPE *put_HTTPProxyHost )( 
            IAMNetShowConfig * This,
            /* [in] */ BSTR bstrHTTPProxyHost);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_HTTPProxyPort )( 
            IAMNetShowConfig * This,
            /* [retval][out] */ long *pHTTPProxyPort);
        
        /* [propput] */ HRESULT ( STDMETHODCALLTYPE *put_HTTPProxyPort )( 
            IAMNetShowConfig * This,
            /* [in] */ long HTTPProxyPort);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_EnableMulticast )( 
            IAMNetShowConfig * This,
            /* [retval][out] */ VARIANT_BOOL *pEnableMulticast);
        
        /* [propput] */ HRESULT ( STDMETHODCALLTYPE *put_EnableMulticast )( 
            IAMNetShowConfig * This,
            /* [in] */ VARIANT_BOOL EnableMulticast);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_EnableUDP )( 
            IAMNetShowConfig * This,
            /* [retval][out] */ VARIANT_BOOL *pEnableUDP);
        
        /* [propput] */ HRESULT ( STDMETHODCALLTYPE *put_EnableUDP )( 
            IAMNetShowConfig * This,
            /* [in] */ VARIANT_BOOL EnableUDP);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_EnableTCP )( 
            IAMNetShowConfig * This,
            /* [retval][out] */ VARIANT_BOOL *pEnableTCP);
        
        /* [propput] */ HRESULT ( STDMETHODCALLTYPE *put_EnableTCP )( 
            IAMNetShowConfig * This,
            /* [in] */ VARIANT_BOOL EnableTCP);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_EnableHTTP )( 
            IAMNetShowConfig * This,
            /* [retval][out] */ VARIANT_BOOL *pEnableHTTP);
        
        /* [propput] */ HRESULT ( STDMETHODCALLTYPE *put_EnableHTTP )( 
            IAMNetShowConfig * This,
            /* [in] */ VARIANT_BOOL EnableHTTP);
        
        END_INTERFACE
    } IAMNetShowConfigVtbl;

    interface IAMNetShowConfig
    {
        CONST_VTBL struct IAMNetShowConfigVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMNetShowConfig_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAMNetShowConfig_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAMNetShowConfig_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAMNetShowConfig_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAMNetShowConfig_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAMNetShowConfig_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAMNetShowConfig_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAMNetShowConfig_get_BufferingTime(This,pBufferingTime)	\
    ( (This)->lpVtbl -> get_BufferingTime(This,pBufferingTime) ) 

#define IAMNetShowConfig_put_BufferingTime(This,BufferingTime)	\
    ( (This)->lpVtbl -> put_BufferingTime(This,BufferingTime) ) 

#define IAMNetShowConfig_get_UseFixedUDPPort(This,pUseFixedUDPPort)	\
    ( (This)->lpVtbl -> get_UseFixedUDPPort(This,pUseFixedUDPPort) ) 

#define IAMNetShowConfig_put_UseFixedUDPPort(This,UseFixedUDPPort)	\
    ( (This)->lpVtbl -> put_UseFixedUDPPort(This,UseFixedUDPPort) ) 

#define IAMNetShowConfig_get_FixedUDPPort(This,pFixedUDPPort)	\
    ( (This)->lpVtbl -> get_FixedUDPPort(This,pFixedUDPPort) ) 

#define IAMNetShowConfig_put_FixedUDPPort(This,FixedUDPPort)	\
    ( (This)->lpVtbl -> put_FixedUDPPort(This,FixedUDPPort) ) 

#define IAMNetShowConfig_get_UseHTTPProxy(This,pUseHTTPProxy)	\
    ( (This)->lpVtbl -> get_UseHTTPProxy(This,pUseHTTPProxy) ) 

#define IAMNetShowConfig_put_UseHTTPProxy(This,UseHTTPProxy)	\
    ( (This)->lpVtbl -> put_UseHTTPProxy(This,UseHTTPProxy) ) 

#define IAMNetShowConfig_get_EnableAutoProxy(This,pEnableAutoProxy)	\
    ( (This)->lpVtbl -> get_EnableAutoProxy(This,pEnableAutoProxy) ) 

#define IAMNetShowConfig_put_EnableAutoProxy(This,EnableAutoProxy)	\
    ( (This)->lpVtbl -> put_EnableAutoProxy(This,EnableAutoProxy) ) 

#define IAMNetShowConfig_get_HTTPProxyHost(This,pbstrHTTPProxyHost)	\
    ( (This)->lpVtbl -> get_HTTPProxyHost(This,pbstrHTTPProxyHost) ) 

#define IAMNetShowConfig_put_HTTPProxyHost(This,bstrHTTPProxyHost)	\
    ( (This)->lpVtbl -> put_HTTPProxyHost(This,bstrHTTPProxyHost) ) 

#define IAMNetShowConfig_get_HTTPProxyPort(This,pHTTPProxyPort)	\
    ( (This)->lpVtbl -> get_HTTPProxyPort(This,pHTTPProxyPort) ) 

#define IAMNetShowConfig_put_HTTPProxyPort(This,HTTPProxyPort)	\
    ( (This)->lpVtbl -> put_HTTPProxyPort(This,HTTPProxyPort) ) 

#define IAMNetShowConfig_get_EnableMulticast(This,pEnableMulticast)	\
    ( (This)->lpVtbl -> get_EnableMulticast(This,pEnableMulticast) ) 

#define IAMNetShowConfig_put_EnableMulticast(This,EnableMulticast)	\
    ( (This)->lpVtbl -> put_EnableMulticast(This,EnableMulticast) ) 

#define IAMNetShowConfig_get_EnableUDP(This,pEnableUDP)	\
    ( (This)->lpVtbl -> get_EnableUDP(This,pEnableUDP) ) 

#define IAMNetShowConfig_put_EnableUDP(This,EnableUDP)	\
    ( (This)->lpVtbl -> put_EnableUDP(This,EnableUDP) ) 

#define IAMNetShowConfig_get_EnableTCP(This,pEnableTCP)	\
    ( (This)->lpVtbl -> get_EnableTCP(This,pEnableTCP) ) 

#define IAMNetShowConfig_put_EnableTCP(This,EnableTCP)	\
    ( (This)->lpVtbl -> put_EnableTCP(This,EnableTCP) ) 

#define IAMNetShowConfig_get_EnableHTTP(This,pEnableHTTP)	\
    ( (This)->lpVtbl -> get_EnableHTTP(This,pEnableHTTP) ) 

#define IAMNetShowConfig_put_EnableHTTP(This,EnableHTTP)	\
    ( (This)->lpVtbl -> put_EnableHTTP(This,EnableHTTP) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAMNetShowConfig_INTERFACE_DEFINED__ */


#ifndef __IAMChannelInfo_INTERFACE_DEFINED__
#define __IAMChannelInfo_INTERFACE_DEFINED__

/* interface IAMChannelInfo */
/* [object][dual][oleautomation][helpstring][uuid] */ 


DEFINE_GUID(IID_IAMChannelInfo,0xFA2AA8F2,0x8B62,0x11d0,0xA5,0x20,0x00,0x00,0x00,0x00,0x00,0x00);

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("FA2AA8F2-8B62-11d0-A520-000000000000")
    IAMChannelInfo : public IDispatch
    {
    public:
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_ChannelName( 
            /* [retval][out] */ BSTR *pbstrChannelName) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_ChannelDescription( 
            /* [retval][out] */ BSTR *pbstrChannelDescription) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_ChannelURL( 
            /* [retval][out] */ BSTR *pbstrChannelURL) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_ContactAddress( 
            /* [retval][out] */ BSTR *pbstrContactAddress) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_ContactPhone( 
            /* [retval][out] */ BSTR *pbstrContactPhone) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_ContactEmail( 
            /* [retval][out] */ BSTR *pbstrContactEmail) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMChannelInfoVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMChannelInfo * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMChannelInfo * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMChannelInfo * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAMChannelInfo * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAMChannelInfo * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAMChannelInfo * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAMChannelInfo * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ChannelName )( 
            IAMChannelInfo * This,
            /* [retval][out] */ BSTR *pbstrChannelName);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ChannelDescription )( 
            IAMChannelInfo * This,
            /* [retval][out] */ BSTR *pbstrChannelDescription);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ChannelURL )( 
            IAMChannelInfo * This,
            /* [retval][out] */ BSTR *pbstrChannelURL);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ContactAddress )( 
            IAMChannelInfo * This,
            /* [retval][out] */ BSTR *pbstrContactAddress);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ContactPhone )( 
            IAMChannelInfo * This,
            /* [retval][out] */ BSTR *pbstrContactPhone);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ContactEmail )( 
            IAMChannelInfo * This,
            /* [retval][out] */ BSTR *pbstrContactEmail);
        
        END_INTERFACE
    } IAMChannelInfoVtbl;

    interface IAMChannelInfo
    {
        CONST_VTBL struct IAMChannelInfoVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMChannelInfo_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAMChannelInfo_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAMChannelInfo_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAMChannelInfo_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAMChannelInfo_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAMChannelInfo_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAMChannelInfo_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAMChannelInfo_get_ChannelName(This,pbstrChannelName)	\
    ( (This)->lpVtbl -> get_ChannelName(This,pbstrChannelName) ) 

#define IAMChannelInfo_get_ChannelDescription(This,pbstrChannelDescription)	\
    ( (This)->lpVtbl -> get_ChannelDescription(This,pbstrChannelDescription) ) 

#define IAMChannelInfo_get_ChannelURL(This,pbstrChannelURL)	\
    ( (This)->lpVtbl -> get_ChannelURL(This,pbstrChannelURL) ) 

#define IAMChannelInfo_get_ContactAddress(This,pbstrContactAddress)	\
    ( (This)->lpVtbl -> get_ContactAddress(This,pbstrContactAddress) ) 

#define IAMChannelInfo_get_ContactPhone(This,pbstrContactPhone)	\
    ( (This)->lpVtbl -> get_ContactPhone(This,pbstrContactPhone) ) 

#define IAMChannelInfo_get_ContactEmail(This,pbstrContactEmail)	\
    ( (This)->lpVtbl -> get_ContactEmail(This,pbstrContactEmail) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAMChannelInfo_INTERFACE_DEFINED__ */


#ifndef __IAMNetworkStatus_INTERFACE_DEFINED__
#define __IAMNetworkStatus_INTERFACE_DEFINED__

/* interface IAMNetworkStatus */
/* [object][dual][oleautomation][helpstring][uuid] */ 


DEFINE_GUID(IID_IAMNetworkStatus,0xFA2AA8F3,0x8B62,0x11d0,0xA5,0x20,0x00,0x00,0x00,0x00,0x00,0x00);

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("FA2AA8F3-8B62-11d0-A520-000000000000")
    IAMNetworkStatus : public IDispatch
    {
    public:
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_ReceivedPackets( 
            /* [retval][out] */ long *pReceivedPackets) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_RecoveredPackets( 
            /* [retval][out] */ long *pRecoveredPackets) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_LostPackets( 
            /* [retval][out] */ long *pLostPackets) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_ReceptionQuality( 
            /* [retval][out] */ long *pReceptionQuality) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_BufferingCount( 
            /* [retval][out] */ long *pBufferingCount) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_IsBroadcast( 
            /* [retval][out] */ VARIANT_BOOL *pIsBroadcast) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_BufferingProgress( 
            /* [retval][out] */ long *pBufferingProgress) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMNetworkStatusVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMNetworkStatus * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMNetworkStatus * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMNetworkStatus * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAMNetworkStatus * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAMNetworkStatus * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAMNetworkStatus * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAMNetworkStatus * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ReceivedPackets )( 
            IAMNetworkStatus * This,
            /* [retval][out] */ long *pReceivedPackets);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RecoveredPackets )( 
            IAMNetworkStatus * This,
            /* [retval][out] */ long *pRecoveredPackets);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LostPackets )( 
            IAMNetworkStatus * This,
            /* [retval][out] */ long *pLostPackets);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ReceptionQuality )( 
            IAMNetworkStatus * This,
            /* [retval][out] */ long *pReceptionQuality);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BufferingCount )( 
            IAMNetworkStatus * This,
            /* [retval][out] */ long *pBufferingCount);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_IsBroadcast )( 
            IAMNetworkStatus * This,
            /* [retval][out] */ VARIANT_BOOL *pIsBroadcast);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BufferingProgress )( 
            IAMNetworkStatus * This,
            /* [retval][out] */ long *pBufferingProgress);
        
        END_INTERFACE
    } IAMNetworkStatusVtbl;

    interface IAMNetworkStatus
    {
        CONST_VTBL struct IAMNetworkStatusVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMNetworkStatus_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAMNetworkStatus_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAMNetworkStatus_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAMNetworkStatus_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAMNetworkStatus_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAMNetworkStatus_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAMNetworkStatus_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAMNetworkStatus_get_ReceivedPackets(This,pReceivedPackets)	\
    ( (This)->lpVtbl -> get_ReceivedPackets(This,pReceivedPackets) ) 

#define IAMNetworkStatus_get_RecoveredPackets(This,pRecoveredPackets)	\
    ( (This)->lpVtbl -> get_RecoveredPackets(This,pRecoveredPackets) ) 

#define IAMNetworkStatus_get_LostPackets(This,pLostPackets)	\
    ( (This)->lpVtbl -> get_LostPackets(This,pLostPackets) ) 

#define IAMNetworkStatus_get_ReceptionQuality(This,pReceptionQuality)	\
    ( (This)->lpVtbl -> get_ReceptionQuality(This,pReceptionQuality) ) 

#define IAMNetworkStatus_get_BufferingCount(This,pBufferingCount)	\
    ( (This)->lpVtbl -> get_BufferingCount(This,pBufferingCount) ) 

#define IAMNetworkStatus_get_IsBroadcast(This,pIsBroadcast)	\
    ( (This)->lpVtbl -> get_IsBroadcast(This,pIsBroadcast) ) 

#define IAMNetworkStatus_get_BufferingProgress(This,pBufferingProgress)	\
    ( (This)->lpVtbl -> get_BufferingProgress(This,pBufferingProgress) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAMNetworkStatus_INTERFACE_DEFINED__ */


#ifndef __IAMExtendedSeeking_INTERFACE_DEFINED__
#define __IAMExtendedSeeking_INTERFACE_DEFINED__

/* interface IAMExtendedSeeking */
/* [object][dual][oleautomation][helpstring][uuid] */ 


DEFINE_GUID(IID_IAMExtendedSeeking,0xFA2AA8F9,0x8B62,0x11d0,0xA5,0x20,0x00,0x00,0x00,0x00,0x00,0x00);

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("FA2AA8F9-8B62-11d0-A520-000000000000")
    IAMExtendedSeeking : public IDispatch
    {
    public:
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_ExSeekCapabilities( 
            /* [retval][out] */ long *pExCapabilities) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_MarkerCount( 
            /* [retval][out] */ long *pMarkerCount) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_CurrentMarker( 
            /* [retval][out] */ long *pCurrentMarker) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetMarkerTime( 
            /* [in] */ long MarkerNum,
            /* [retval][out] */ double *pMarkerTime) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetMarkerName( 
            /* [in] */ long MarkerNum,
            /* [retval][out] */ BSTR *pbstrMarkerName) = 0;
        
        virtual /* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE put_PlaybackSpeed( 
            /* [in] */ double Speed) = 0;
        
        virtual /* [propget] */ HRESULT STDMETHODCALLTYPE get_PlaybackSpeed( 
            /* [retval][out] */ double *pSpeed) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMExtendedSeekingVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMExtendedSeeking * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMExtendedSeeking * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMExtendedSeeking * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAMExtendedSeeking * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAMExtendedSeeking * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAMExtendedSeeking * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAMExtendedSeeking * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ExSeekCapabilities )( 
            IAMExtendedSeeking * This,
            /* [retval][out] */ long *pExCapabilities);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MarkerCount )( 
            IAMExtendedSeeking * This,
            /* [retval][out] */ long *pMarkerCount);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentMarker )( 
            IAMExtendedSeeking * This,
            /* [retval][out] */ long *pCurrentMarker);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetMarkerTime )( 
            IAMExtendedSeeking * This,
            /* [in] */ long MarkerNum,
            /* [retval][out] */ double *pMarkerTime);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetMarkerName )( 
            IAMExtendedSeeking * This,
            /* [in] */ long MarkerNum,
            /* [retval][out] */ BSTR *pbstrMarkerName);
        
        /* [helpstring][propput] */ HRESULT ( STDMETHODCALLTYPE *put_PlaybackSpeed )( 
            IAMExtendedSeeking * This,
            /* [in] */ double Speed);
        
        /* [propget] */ HRESULT ( STDMETHODCALLTYPE *get_PlaybackSpeed )( 
            IAMExtendedSeeking * This,
            /* [retval][out] */ double *pSpeed);
        
        END_INTERFACE
    } IAMExtendedSeekingVtbl;

    interface IAMExtendedSeeking
    {
        CONST_VTBL struct IAMExtendedSeekingVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMExtendedSeeking_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAMExtendedSeeking_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAMExtendedSeeking_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAMExtendedSeeking_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAMExtendedSeeking_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAMExtendedSeeking_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAMExtendedSeeking_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAMExtendedSeeking_get_ExSeekCapabilities(This,pExCapabilities)	\
    ( (This)->lpVtbl -> get_ExSeekCapabilities(This,pExCapabilities) ) 

#define IAMExtendedSeeking_get_MarkerCount(This,pMarkerCount)	\
    ( (This)->lpVtbl -> get_MarkerCount(This,pMarkerCount) ) 

#define IAMExtendedSeeking_get_CurrentMarker(This,pCurrentMarker)	\
    ( (This)->lpVtbl -> get_CurrentMarker(This,pCurrentMarker) ) 

#define IAMExtendedSeeking_GetMarkerTime(This,MarkerNum,pMarkerTime)	\
    ( (This)->lpVtbl -> GetMarkerTime(This,MarkerNum,pMarkerTime) ) 

#define IAMExtendedSeeking_GetMarkerName(This,MarkerNum,pbstrMarkerName)	\
    ( (This)->lpVtbl -> GetMarkerName(This,MarkerNum,pbstrMarkerName) ) 

#define IAMExtendedSeeking_put_PlaybackSpeed(This,Speed)	\
    ( (This)->lpVtbl -> put_PlaybackSpeed(This,Speed) ) 

#define IAMExtendedSeeking_get_PlaybackSpeed(This,pSpeed)	\
    ( (This)->lpVtbl -> get_PlaybackSpeed(This,pSpeed) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAMExtendedSeeking_INTERFACE_DEFINED__ */


#ifndef __IAMNetShowExProps_INTERFACE_DEFINED__
#define __IAMNetShowExProps_INTERFACE_DEFINED__

/* interface IAMNetShowExProps */
/* [object][dual][oleautomation][helpstring][uuid] */ 


DEFINE_GUID(IID_IAMNetShowExProps,0xFA2AA8F5,0x8B62,0x11d0,0xA5,0x20,0x00,0x00,0x00,0x00,0x00,0x00);

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("FA2AA8F5-8B62-11d0-A520-000000000000")
    IAMNetShowExProps : public IDispatch
    {
    public:
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_SourceProtocol( 
            /* [retval][out] */ long *pSourceProtocol) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_Bandwidth( 
            /* [retval][out] */ long *pBandwidth) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_ErrorCorrection( 
            /* [retval][out] */ BSTR *pbstrErrorCorrection) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_CodecCount( 
            /* [retval][out] */ long *pCodecCount) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCodecInstalled( 
            /* [in] */ long CodecNum,
            /* [retval][out] */ VARIANT_BOOL *pCodecInstalled) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCodecDescription( 
            /* [in] */ long CodecNum,
            /* [retval][out] */ BSTR *pbstrCodecDescription) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCodecURL( 
            /* [in] */ long CodecNum,
            /* [retval][out] */ BSTR *pbstrCodecURL) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_CreationDate( 
            /* [retval][out] */ DATE *pCreationDate) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_SourceLink( 
            /* [retval][out] */ BSTR *pbstrSourceLink) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMNetShowExPropsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMNetShowExProps * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMNetShowExProps * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMNetShowExProps * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAMNetShowExProps * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAMNetShowExProps * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAMNetShowExProps * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAMNetShowExProps * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SourceProtocol )( 
            IAMNetShowExProps * This,
            /* [retval][out] */ long *pSourceProtocol);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Bandwidth )( 
            IAMNetShowExProps * This,
            /* [retval][out] */ long *pBandwidth);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ErrorCorrection )( 
            IAMNetShowExProps * This,
            /* [retval][out] */ BSTR *pbstrErrorCorrection);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CodecCount )( 
            IAMNetShowExProps * This,
            /* [retval][out] */ long *pCodecCount);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetCodecInstalled )( 
            IAMNetShowExProps * This,
            /* [in] */ long CodecNum,
            /* [retval][out] */ VARIANT_BOOL *pCodecInstalled);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetCodecDescription )( 
            IAMNetShowExProps * This,
            /* [in] */ long CodecNum,
            /* [retval][out] */ BSTR *pbstrCodecDescription);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetCodecURL )( 
            IAMNetShowExProps * This,
            /* [in] */ long CodecNum,
            /* [retval][out] */ BSTR *pbstrCodecURL);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CreationDate )( 
            IAMNetShowExProps * This,
            /* [retval][out] */ DATE *pCreationDate);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SourceLink )( 
            IAMNetShowExProps * This,
            /* [retval][out] */ BSTR *pbstrSourceLink);
        
        END_INTERFACE
    } IAMNetShowExPropsVtbl;

    interface IAMNetShowExProps
    {
        CONST_VTBL struct IAMNetShowExPropsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMNetShowExProps_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAMNetShowExProps_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAMNetShowExProps_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAMNetShowExProps_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAMNetShowExProps_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAMNetShowExProps_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAMNetShowExProps_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAMNetShowExProps_get_SourceProtocol(This,pSourceProtocol)	\
    ( (This)->lpVtbl -> get_SourceProtocol(This,pSourceProtocol) ) 

#define IAMNetShowExProps_get_Bandwidth(This,pBandwidth)	\
    ( (This)->lpVtbl -> get_Bandwidth(This,pBandwidth) ) 

#define IAMNetShowExProps_get_ErrorCorrection(This,pbstrErrorCorrection)	\
    ( (This)->lpVtbl -> get_ErrorCorrection(This,pbstrErrorCorrection) ) 

#define IAMNetShowExProps_get_CodecCount(This,pCodecCount)	\
    ( (This)->lpVtbl -> get_CodecCount(This,pCodecCount) ) 

#define IAMNetShowExProps_GetCodecInstalled(This,CodecNum,pCodecInstalled)	\
    ( (This)->lpVtbl -> GetCodecInstalled(This,CodecNum,pCodecInstalled) ) 

#define IAMNetShowExProps_GetCodecDescription(This,CodecNum,pbstrCodecDescription)	\
    ( (This)->lpVtbl -> GetCodecDescription(This,CodecNum,pbstrCodecDescription) ) 

#define IAMNetShowExProps_GetCodecURL(This,CodecNum,pbstrCodecURL)	\
    ( (This)->lpVtbl -> GetCodecURL(This,CodecNum,pbstrCodecURL) ) 

#define IAMNetShowExProps_get_CreationDate(This,pCreationDate)	\
    ( (This)->lpVtbl -> get_CreationDate(This,pCreationDate) ) 

#define IAMNetShowExProps_get_SourceLink(This,pbstrSourceLink)	\
    ( (This)->lpVtbl -> get_SourceLink(This,pbstrSourceLink) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAMNetShowExProps_INTERFACE_DEFINED__ */


#ifndef __IAMExtendedErrorInfo_INTERFACE_DEFINED__
#define __IAMExtendedErrorInfo_INTERFACE_DEFINED__

/* interface IAMExtendedErrorInfo */
/* [object][dual][oleautomation][helpstring][uuid] */ 


DEFINE_GUID(IID_IAMExtendedErrorInfo,0xFA2AA8F6,0x8B62,0x11d0,0xA5,0x20,0x00,0x00,0x00,0x00,0x00,0x00);

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("FA2AA8F6-8B62-11d0-A520-000000000000")
    IAMExtendedErrorInfo : public IDispatch
    {
    public:
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_HasError( 
            /* [retval][out] */ VARIANT_BOOL *pHasError) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_ErrorDescription( 
            /* [retval][out] */ BSTR *pbstrErrorDescription) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_ErrorCode( 
            /* [retval][out] */ long *pErrorCode) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMExtendedErrorInfoVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMExtendedErrorInfo * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMExtendedErrorInfo * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMExtendedErrorInfo * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAMExtendedErrorInfo * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAMExtendedErrorInfo * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAMExtendedErrorInfo * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAMExtendedErrorInfo * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_HasError )( 
            IAMExtendedErrorInfo * This,
            /* [retval][out] */ VARIANT_BOOL *pHasError);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ErrorDescription )( 
            IAMExtendedErrorInfo * This,
            /* [retval][out] */ BSTR *pbstrErrorDescription);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ErrorCode )( 
            IAMExtendedErrorInfo * This,
            /* [retval][out] */ long *pErrorCode);
        
        END_INTERFACE
    } IAMExtendedErrorInfoVtbl;

    interface IAMExtendedErrorInfo
    {
        CONST_VTBL struct IAMExtendedErrorInfoVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMExtendedErrorInfo_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAMExtendedErrorInfo_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAMExtendedErrorInfo_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAMExtendedErrorInfo_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAMExtendedErrorInfo_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAMExtendedErrorInfo_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAMExtendedErrorInfo_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAMExtendedErrorInfo_get_HasError(This,pHasError)	\
    ( (This)->lpVtbl -> get_HasError(This,pHasError) ) 

#define IAMExtendedErrorInfo_get_ErrorDescription(This,pbstrErrorDescription)	\
    ( (This)->lpVtbl -> get_ErrorDescription(This,pbstrErrorDescription) ) 

#define IAMExtendedErrorInfo_get_ErrorCode(This,pErrorCode)	\
    ( (This)->lpVtbl -> get_ErrorCode(This,pErrorCode) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAMExtendedErrorInfo_INTERFACE_DEFINED__ */


#ifndef __IAMMediaContent_INTERFACE_DEFINED__
#define __IAMMediaContent_INTERFACE_DEFINED__

/* interface IAMMediaContent */
/* [object][dual][oleautomation][helpstring][uuid] */ 


DEFINE_GUID(IID_IAMMediaContent,0xFA2AA8F4,0x8B62,0x11d0,0xA5,0x20,0x00,0x00,0x00,0x00,0x00,0x00);

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("FA2AA8F4-8B62-11d0-A520-000000000000")
    IAMMediaContent : public IDispatch
    {
    public:
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_AuthorName( 
            /* [retval][out] */ BSTR *pbstrAuthorName) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_Title( 
            /* [retval][out] */ BSTR *pbstrTitle) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_Rating( 
            /* [retval][out] */ BSTR *pbstrRating) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_Description( 
            /* [retval][out] */ BSTR *pbstrDescription) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_Copyright( 
            /* [retval][out] */ BSTR *pbstrCopyright) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_BaseURL( 
            /* [retval][out] */ BSTR *pbstrBaseURL) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_LogoURL( 
            /* [retval][out] */ BSTR *pbstrLogoURL) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_LogoIconURL( 
            /* [retval][out] */ BSTR *pbstrLogoURL) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_WatermarkURL( 
            /* [retval][out] */ BSTR *pbstrWatermarkURL) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_MoreInfoURL( 
            /* [retval][out] */ BSTR *pbstrMoreInfoURL) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_MoreInfoBannerImage( 
            /* [retval][out] */ BSTR *pbstrMoreInfoBannerImage) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_MoreInfoBannerURL( 
            /* [retval][out] */ BSTR *pbstrMoreInfoBannerURL) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_MoreInfoText( 
            /* [retval][out] */ BSTR *pbstrMoreInfoText) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMMediaContentVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMMediaContent * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMMediaContent * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMMediaContent * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAMMediaContent * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAMMediaContent * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAMMediaContent * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAMMediaContent * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AuthorName )( 
            IAMMediaContent * This,
            /* [retval][out] */ BSTR *pbstrAuthorName);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Title )( 
            IAMMediaContent * This,
            /* [retval][out] */ BSTR *pbstrTitle);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Rating )( 
            IAMMediaContent * This,
            /* [retval][out] */ BSTR *pbstrRating);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Description )( 
            IAMMediaContent * This,
            /* [retval][out] */ BSTR *pbstrDescription);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Copyright )( 
            IAMMediaContent * This,
            /* [retval][out] */ BSTR *pbstrCopyright);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BaseURL )( 
            IAMMediaContent * This,
            /* [retval][out] */ BSTR *pbstrBaseURL);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LogoURL )( 
            IAMMediaContent * This,
            /* [retval][out] */ BSTR *pbstrLogoURL);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LogoIconURL )( 
            IAMMediaContent * This,
            /* [retval][out] */ BSTR *pbstrLogoURL);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_WatermarkURL )( 
            IAMMediaContent * This,
            /* [retval][out] */ BSTR *pbstrWatermarkURL);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MoreInfoURL )( 
            IAMMediaContent * This,
            /* [retval][out] */ BSTR *pbstrMoreInfoURL);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MoreInfoBannerImage )( 
            IAMMediaContent * This,
            /* [retval][out] */ BSTR *pbstrMoreInfoBannerImage);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MoreInfoBannerURL )( 
            IAMMediaContent * This,
            /* [retval][out] */ BSTR *pbstrMoreInfoBannerURL);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MoreInfoText )( 
            IAMMediaContent * This,
            /* [retval][out] */ BSTR *pbstrMoreInfoText);
        
        END_INTERFACE
    } IAMMediaContentVtbl;

    interface IAMMediaContent
    {
        CONST_VTBL struct IAMMediaContentVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMMediaContent_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAMMediaContent_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAMMediaContent_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAMMediaContent_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAMMediaContent_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAMMediaContent_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAMMediaContent_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAMMediaContent_get_AuthorName(This,pbstrAuthorName)	\
    ( (This)->lpVtbl -> get_AuthorName(This,pbstrAuthorName) ) 

#define IAMMediaContent_get_Title(This,pbstrTitle)	\
    ( (This)->lpVtbl -> get_Title(This,pbstrTitle) ) 

#define IAMMediaContent_get_Rating(This,pbstrRating)	\
    ( (This)->lpVtbl -> get_Rating(This,pbstrRating) ) 

#define IAMMediaContent_get_Description(This,pbstrDescription)	\
    ( (This)->lpVtbl -> get_Description(This,pbstrDescription) ) 

#define IAMMediaContent_get_Copyright(This,pbstrCopyright)	\
    ( (This)->lpVtbl -> get_Copyright(This,pbstrCopyright) ) 

#define IAMMediaContent_get_BaseURL(This,pbstrBaseURL)	\
    ( (This)->lpVtbl -> get_BaseURL(This,pbstrBaseURL) ) 

#define IAMMediaContent_get_LogoURL(This,pbstrLogoURL)	\
    ( (This)->lpVtbl -> get_LogoURL(This,pbstrLogoURL) ) 

#define IAMMediaContent_get_LogoIconURL(This,pbstrLogoURL)	\
    ( (This)->lpVtbl -> get_LogoIconURL(This,pbstrLogoURL) ) 

#define IAMMediaContent_get_WatermarkURL(This,pbstrWatermarkURL)	\
    ( (This)->lpVtbl -> get_WatermarkURL(This,pbstrWatermarkURL) ) 

#define IAMMediaContent_get_MoreInfoURL(This,pbstrMoreInfoURL)	\
    ( (This)->lpVtbl -> get_MoreInfoURL(This,pbstrMoreInfoURL) ) 

#define IAMMediaContent_get_MoreInfoBannerImage(This,pbstrMoreInfoBannerImage)	\
    ( (This)->lpVtbl -> get_MoreInfoBannerImage(This,pbstrMoreInfoBannerImage) ) 

#define IAMMediaContent_get_MoreInfoBannerURL(This,pbstrMoreInfoBannerURL)	\
    ( (This)->lpVtbl -> get_MoreInfoBannerURL(This,pbstrMoreInfoBannerURL) ) 

#define IAMMediaContent_get_MoreInfoText(This,pbstrMoreInfoText)	\
    ( (This)->lpVtbl -> get_MoreInfoText(This,pbstrMoreInfoText) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAMMediaContent_INTERFACE_DEFINED__ */


#ifndef __IAMMediaContent2_INTERFACE_DEFINED__
#define __IAMMediaContent2_INTERFACE_DEFINED__

/* interface IAMMediaContent2 */
/* [object][dual][oleautomation][helpstring][uuid] */ 


DEFINE_GUID(IID_IAMMediaContent2,0x817FF170,0xC535,0x11d2,0x9C,0x67,0x00,0xA0,0xC9,0x8C,0x04,0x78);

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("817FF170-C535-11d2-9C67-00A0C98C0478")
    IAMMediaContent2 : public IAMMediaContent
    {
    public:
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_MoreInfoBannerBitmap( 
            /* [retval][out] */ LONG *pMoreInfoBannerBitmap) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMMediaContent2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMMediaContent2 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMMediaContent2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMMediaContent2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAMMediaContent2 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAMMediaContent2 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAMMediaContent2 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAMMediaContent2 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AuthorName )( 
            IAMMediaContent2 * This,
            /* [retval][out] */ BSTR *pbstrAuthorName);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Title )( 
            IAMMediaContent2 * This,
            /* [retval][out] */ BSTR *pbstrTitle);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Rating )( 
            IAMMediaContent2 * This,
            /* [retval][out] */ BSTR *pbstrRating);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Description )( 
            IAMMediaContent2 * This,
            /* [retval][out] */ BSTR *pbstrDescription);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Copyright )( 
            IAMMediaContent2 * This,
            /* [retval][out] */ BSTR *pbstrCopyright);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BaseURL )( 
            IAMMediaContent2 * This,
            /* [retval][out] */ BSTR *pbstrBaseURL);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LogoURL )( 
            IAMMediaContent2 * This,
            /* [retval][out] */ BSTR *pbstrLogoURL);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LogoIconURL )( 
            IAMMediaContent2 * This,
            /* [retval][out] */ BSTR *pbstrLogoURL);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_WatermarkURL )( 
            IAMMediaContent2 * This,
            /* [retval][out] */ BSTR *pbstrWatermarkURL);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MoreInfoURL )( 
            IAMMediaContent2 * This,
            /* [retval][out] */ BSTR *pbstrMoreInfoURL);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MoreInfoBannerImage )( 
            IAMMediaContent2 * This,
            /* [retval][out] */ BSTR *pbstrMoreInfoBannerImage);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MoreInfoBannerURL )( 
            IAMMediaContent2 * This,
            /* [retval][out] */ BSTR *pbstrMoreInfoBannerURL);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MoreInfoText )( 
            IAMMediaContent2 * This,
            /* [retval][out] */ BSTR *pbstrMoreInfoText);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MoreInfoBannerBitmap )( 
            IAMMediaContent2 * This,
            /* [retval][out] */ LONG *pMoreInfoBannerBitmap);
        
        END_INTERFACE
    } IAMMediaContent2Vtbl;

    interface IAMMediaContent2
    {
        CONST_VTBL struct IAMMediaContent2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMMediaContent2_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAMMediaContent2_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAMMediaContent2_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAMMediaContent2_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAMMediaContent2_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAMMediaContent2_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAMMediaContent2_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAMMediaContent2_get_AuthorName(This,pbstrAuthorName)	\
    ( (This)->lpVtbl -> get_AuthorName(This,pbstrAuthorName) ) 

#define IAMMediaContent2_get_Title(This,pbstrTitle)	\
    ( (This)->lpVtbl -> get_Title(This,pbstrTitle) ) 

#define IAMMediaContent2_get_Rating(This,pbstrRating)	\
    ( (This)->lpVtbl -> get_Rating(This,pbstrRating) ) 

#define IAMMediaContent2_get_Description(This,pbstrDescription)	\
    ( (This)->lpVtbl -> get_Description(This,pbstrDescription) ) 

#define IAMMediaContent2_get_Copyright(This,pbstrCopyright)	\
    ( (This)->lpVtbl -> get_Copyright(This,pbstrCopyright) ) 

#define IAMMediaContent2_get_BaseURL(This,pbstrBaseURL)	\
    ( (This)->lpVtbl -> get_BaseURL(This,pbstrBaseURL) ) 

#define IAMMediaContent2_get_LogoURL(This,pbstrLogoURL)	\
    ( (This)->lpVtbl -> get_LogoURL(This,pbstrLogoURL) ) 

#define IAMMediaContent2_get_LogoIconURL(This,pbstrLogoURL)	\
    ( (This)->lpVtbl -> get_LogoIconURL(This,pbstrLogoURL) ) 

#define IAMMediaContent2_get_WatermarkURL(This,pbstrWatermarkURL)	\
    ( (This)->lpVtbl -> get_WatermarkURL(This,pbstrWatermarkURL) ) 

#define IAMMediaContent2_get_MoreInfoURL(This,pbstrMoreInfoURL)	\
    ( (This)->lpVtbl -> get_MoreInfoURL(This,pbstrMoreInfoURL) ) 

#define IAMMediaContent2_get_MoreInfoBannerImage(This,pbstrMoreInfoBannerImage)	\
    ( (This)->lpVtbl -> get_MoreInfoBannerImage(This,pbstrMoreInfoBannerImage) ) 

#define IAMMediaContent2_get_MoreInfoBannerURL(This,pbstrMoreInfoBannerURL)	\
    ( (This)->lpVtbl -> get_MoreInfoBannerURL(This,pbstrMoreInfoBannerURL) ) 

#define IAMMediaContent2_get_MoreInfoText(This,pbstrMoreInfoText)	\
    ( (This)->lpVtbl -> get_MoreInfoText(This,pbstrMoreInfoText) ) 


#define IAMMediaContent2_get_MoreInfoBannerBitmap(This,pMoreInfoBannerBitmap)	\
    ( (This)->lpVtbl -> get_MoreInfoBannerBitmap(This,pMoreInfoBannerBitmap) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAMMediaContent2_INTERFACE_DEFINED__ */


#ifndef __IAMMediaContentEx_INTERFACE_DEFINED__
#define __IAMMediaContentEx_INTERFACE_DEFINED__

/* interface IAMMediaContentEx */
/* [object][dual][oleautomation][helpstring][uuid] */ 


DEFINE_GUID(IID_IAMMediaContentEx,0xCE8F78C1,0x74D9,0x11d2,0xB0,0x9D,0x00,0xA0,0xC9,0xA8,0x11,0x17);

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("CE8F78C1-74D9-11d2-B09D-00A0C9A81117")
    IAMMediaContentEx : public IDispatch
    {
    public:
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_MediaParameter( 
            /* [in] */ long EntryNum,
            /* [in] */ BSTR bstrName,
            /* [retval][out] */ BSTR *pbstrValue) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_MediaParameterName( 
            /* [in] */ long EntryNum,
            /* [in] */ long Index,
            /* [retval][out] */ BSTR *pbstrName) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_PlaylistCount( 
            /* [retval][out] */ long *pNumberEntries) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMMediaContentExVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMMediaContentEx * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMMediaContentEx * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMMediaContentEx * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAMMediaContentEx * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAMMediaContentEx * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAMMediaContentEx * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAMMediaContentEx * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MediaParameter )( 
            IAMMediaContentEx * This,
            /* [in] */ long EntryNum,
            /* [in] */ BSTR bstrName,
            /* [retval][out] */ BSTR *pbstrValue);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MediaParameterName )( 
            IAMMediaContentEx * This,
            /* [in] */ long EntryNum,
            /* [in] */ long Index,
            /* [retval][out] */ BSTR *pbstrName);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PlaylistCount )( 
            IAMMediaContentEx * This,
            /* [retval][out] */ long *pNumberEntries);
        
        END_INTERFACE
    } IAMMediaContentExVtbl;

    interface IAMMediaContentEx
    {
        CONST_VTBL struct IAMMediaContentExVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMMediaContentEx_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAMMediaContentEx_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAMMediaContentEx_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAMMediaContentEx_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAMMediaContentEx_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAMMediaContentEx_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAMMediaContentEx_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAMMediaContentEx_get_MediaParameter(This,EntryNum,bstrName,pbstrValue)	\
    ( (This)->lpVtbl -> get_MediaParameter(This,EntryNum,bstrName,pbstrValue) ) 

#define IAMMediaContentEx_get_MediaParameterName(This,EntryNum,Index,pbstrName)	\
    ( (This)->lpVtbl -> get_MediaParameterName(This,EntryNum,Index,pbstrName) ) 

#define IAMMediaContentEx_get_PlaylistCount(This,pNumberEntries)	\
    ( (This)->lpVtbl -> get_PlaylistCount(This,pNumberEntries) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAMMediaContentEx_INTERFACE_DEFINED__ */


#ifndef __IAMNetShowPreroll_INTERFACE_DEFINED__
#define __IAMNetShowPreroll_INTERFACE_DEFINED__

/* interface IAMNetShowPreroll */
/* [object][dual][oleautomation][helpstring][uuid] */ 


DEFINE_GUID(IID_IAMNetShowPreroll,0xAAE7E4E2,0x6388,0x11D1,0x8D,0x93,0x00,0x60,0x97,0xC9,0xA2,0xB2);

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("AAE7E4E2-6388-11D1-8D93-006097C9A2B2")
    IAMNetShowPreroll : public IDispatch
    {
    public:
        virtual /* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE put_Preroll( 
            /* [in] */ VARIANT_BOOL fPreroll) = 0;
        
        virtual /* [propget] */ HRESULT STDMETHODCALLTYPE get_Preroll( 
            /* [retval][out] */ VARIANT_BOOL *pfPreroll) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMNetShowPrerollVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMNetShowPreroll * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMNetShowPreroll * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMNetShowPreroll * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAMNetShowPreroll * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAMNetShowPreroll * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAMNetShowPreroll * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAMNetShowPreroll * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Preroll )( 
            IAMNetShowPreroll * This,
            /* [in] */ VARIANT_BOOL fPreroll);
        
        /* [propget] */ HRESULT ( STDMETHODCALLTYPE *get_Preroll )( 
            IAMNetShowPreroll * This,
            /* [retval][out] */ VARIANT_BOOL *pfPreroll);
        
        END_INTERFACE
    } IAMNetShowPrerollVtbl;

    interface IAMNetShowPreroll
    {
        CONST_VTBL struct IAMNetShowPrerollVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMNetShowPreroll_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAMNetShowPreroll_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAMNetShowPreroll_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAMNetShowPreroll_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAMNetShowPreroll_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAMNetShowPreroll_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAMNetShowPreroll_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAMNetShowPreroll_put_Preroll(This,fPreroll)	\
    ( (This)->lpVtbl -> put_Preroll(This,fPreroll) ) 

#define IAMNetShowPreroll_get_Preroll(This,pfPreroll)	\
    ( (This)->lpVtbl -> get_Preroll(This,pfPreroll) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAMNetShowPreroll_INTERFACE_DEFINED__ */


#ifndef __IDShowPlugin_INTERFACE_DEFINED__
#define __IDShowPlugin_INTERFACE_DEFINED__

/* interface IDShowPlugin */
/* [object][helpstring][uuid] */ 


DEFINE_GUID(IID_IDShowPlugin,0x4746B7C8,0x700E,0x11d1,0xBE,0xCC,0x00,0xC0,0x4F,0xB6,0xE9,0x37);

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("4746B7C8-700E-11d1-BECC-00C04FB6E937")
    IDShowPlugin : public IUnknown
    {
    public:
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_URL( 
            /* [retval][out] */ BSTR *pURL) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_UserAgent( 
            /* [retval][out] */ BSTR *pUserAgent) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDShowPluginVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDShowPlugin * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDShowPlugin * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDShowPlugin * This);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_URL )( 
            IDShowPlugin * This,
            /* [retval][out] */ BSTR *pURL);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UserAgent )( 
            IDShowPlugin * This,
            /* [retval][out] */ BSTR *pUserAgent);
        
        END_INTERFACE
    } IDShowPluginVtbl;

    interface IDShowPlugin
    {
        CONST_VTBL struct IDShowPluginVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDShowPlugin_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IDShowPlugin_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IDShowPlugin_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IDShowPlugin_get_URL(This,pURL)	\
    ( (This)->lpVtbl -> get_URL(This,pURL) ) 

#define IDShowPlugin_get_UserAgent(This,pUserAgent)	\
    ( (This)->lpVtbl -> get_UserAgent(This,pUserAgent) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IDShowPlugin_INTERFACE_DEFINED__ */


#ifndef __IAMSecureMediaContent_INTERFACE_DEFINED__
#define __IAMSecureMediaContent_INTERFACE_DEFINED__

/* interface IAMSecureMediaContent */
/* [object][oleautomation][helpstring][uuid] */ 


DEFINE_GUID(IID_IAMSecureMediaContent,0x36F0A694,0xC536,0x11d2,0x9C,0x67,0x00,0xA0,0xC9,0x8C,0x04,0x78);

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("36F0A694-C536-11d2-9C67-00A0C98C0478")
    IAMSecureMediaContent : public IUnknown
    {
    public:
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_IsSecure( 
            /* [retval][out] */ VARIANT_BOOL *pfIsSecure) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_Tooltip( 
            /* [retval][out] */ BSTR *pbstrTooltip) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMSecureMediaContentVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMSecureMediaContent * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMSecureMediaContent * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMSecureMediaContent * This);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_IsSecure )( 
            IAMSecureMediaContent * This,
            /* [retval][out] */ VARIANT_BOOL *pfIsSecure);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Tooltip )( 
            IAMSecureMediaContent * This,
            /* [retval][out] */ BSTR *pbstrTooltip);
        
        END_INTERFACE
    } IAMSecureMediaContentVtbl;

    interface IAMSecureMediaContent
    {
        CONST_VTBL struct IAMSecureMediaContentVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMSecureMediaContent_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAMSecureMediaContent_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAMSecureMediaContent_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAMSecureMediaContent_get_IsSecure(This,pfIsSecure)	\
    ( (This)->lpVtbl -> get_IsSecure(This,pfIsSecure) ) 

#define IAMSecureMediaContent_get_Tooltip(This,pbstrTooltip)	\
    ( (This)->lpVtbl -> get_Tooltip(This,pbstrTooltip) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAMSecureMediaContent_INTERFACE_DEFINED__ */

#endif /* __QuartzNetTypeLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


