// GetBspVersion.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

//Below structures is used to return BSP Version to Application.
typedef struct BspVersion
{
    DWORD Major;
    DWORD Minor;
    DWORD Release;
    DWORD Build;
}BSP_VERSION,*PBSP_VERSION;

typedef BOOL (*GetBSPVersion)(PBSP_VERSION pBspVersion);
HINSTANCE lHndlInterfaceSDKDll;

int _tmain(int argc, TCHAR *argv[], TCHAR *envp[])
{
	RETAILMSG(1, (TEXT("\r\n +-------------------------------------------------------------------+")));
	RETAILMSG(1, (TEXT("\r\n |                    Get BSP Version -------------------------------- |")));
	RETAILMSG(1, (TEXT("\r\n +-------------------------------------------------------------------+\r\n \r\n ")));

	BSP_VERSION BspVersion, *pBspVersion;
	GetBSPVersion pfGetBSPVersion;
	
	lHndlInterfaceSDKDll = LoadLibrary(L"InterfaceSDK.dll");
	if(lHndlInterfaceSDKDll==NULL)
    {
       RETAILMSG(1,(TEXT("-----Fail to LOAD EncoderSDK.DLL------ \t\r\n")));
	}

	//Encoder DLL WriteRelayBit function Handle
	pfGetBSPVersion= (GetBSPVersion)GetProcAddress(lHndlInterfaceSDKDll, L"GetBSPVersion");
	
	if(pfGetBSPVersion==NULL)
	{
		   RETAILMSG(1,(TEXT("-----Fail to Open pfGetBSPVersion Function Handle from EncoderSDK.DLL------ \t\r\n")));
		   _tprintf(_T("-----Fail to Open pfGetBSPVersion !\n"));
	}
	
	if(pfGetBSPVersion(&BspVersion));
	{
		pBspVersion = &BspVersion; 
	   RETAILMSG(1,(TEXT("Application->GetBspVersion: Bootloader Version %d.%d.%d.%d \r\n"),pBspVersion->Major, pBspVersion->Minor, pBspVersion->Build, pBspVersion->Release));

	   _tprintf(_T("Application->GetBspVersion: Bootloader Version %d.%d.%d.%d!\n"),pBspVersion->Major, pBspVersion->Minor, pBspVersion->Build, pBspVersion->Release);
	}

	Sleep(1000);
    return 0;
}

