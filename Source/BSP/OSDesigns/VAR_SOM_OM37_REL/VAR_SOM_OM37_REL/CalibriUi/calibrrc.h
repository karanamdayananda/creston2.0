//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//
/* Resource identifiers for the CalibrUi module */

#define C_CALIBRATION1_LINES    5

#define IDS_CALIBRATION_1_1    0x100
#define IDS_CALIBRATION_1_2    0x101
#define IDS_CALIBRATION_1_3    0x102
#define IDS_CALIBRATION_1_4    0x103
#define IDS_CALIBRATION_1_5    0x104


#define IDS_CALIBRATION_1_1_NK    0x110
#define IDS_CALIBRATION_1_2_NK    0x111
#define IDS_CALIBRATION_1_3_NK    0x112
#define IDS_CALIBRATION_1_4_NK    0x113
#define IDS_CALIBRATION_1_5_NK    0x114

#define C_CALIBRATION2_LINES    5

#define IDS_CALIBRATION_2_1    0x120
#define IDS_CALIBRATION_2_2    0x121
#define IDS_CALIBRATION_2_3    0x122
#define IDS_CALIBRATION_2_4    0x123
#define IDS_CALIBRATION_2_5    0x124

#define IDS_CALIBRATION_2_1_NK    0x130
#define IDS_CALIBRATION_2_2_NK    0x131
#define IDS_CALIBRATION_2_3_NK    0x132
#define IDS_CALIBRATION_2_4_NK    0x133
#define IDS_CALIBRATION_2_5_NK    0x134

#define IDS_CALIBRATION_TIMEOUT     0x140

