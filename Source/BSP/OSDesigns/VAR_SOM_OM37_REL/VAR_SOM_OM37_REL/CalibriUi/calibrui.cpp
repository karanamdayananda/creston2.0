//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//
#include <windows.h>
#include <calibrui.h>
#include <calibrrc.h>

#define C_CALIBRATION_MSG_BUF_CHARS 128

static TOUCH_CALIBRATE_STATE    *s_ptcs;    //    Current touch calibration State
static HINSTANCE s_hinst;

#define USE_ENTER_ESC_MASK (KBDI_KEYBOARD_PRESENT|KBDI_KEYBOARD_ENABLED|KBDI_KEYBOARD_ENTER_ESC)


static
BOOL
UseEnterEsc(
    void
    )
{
    return (GetKeyboardStatus() & USE_ENTER_ESC_MASK) == USE_ENTER_ESC_MASK;
}




/*++

    TouchCalibrateUI_Initialize:


--*/
void 
TouchCalibrateUI_Initialize(
    TOUCH_CALIBRATE_STATE    *ptcs,
    HINSTANCE    hinst
    )
{
    s_ptcs = ptcs;
    s_hinst = hinst;
}



static
void
TextSize(
    HDC        hdc,
    PCTSTR    pszStr,
    int        cChars,
    SIZE    *pSize
    )
{
    if ( !GetTextExtentExPointW(
                hdc,
                pszStr,
                cChars,
                0,     // nMaxExtent
                NULL, // lpnFit
                NULL,
                pSize) )
        {
        ERRORMSG(1, (TEXT("GetLastError: %d (dec)\r\n"), GetLastError()));
        ASSERT(0);
        }

    return;

}



static
void
TouchCalibrateDrawScreenText(
    HDC        hdc,
    int        cLines,
    UINT    IdStringBase
    )
{
    int        VertSpacing = 0;
    TCHAR    buf[C_CALIBRATION_MSG_BUF_CHARS];
    int        cChars;
    int        xText, yText = 0;
    SIZE    Size;
    int        i;


    //    Clear the screen.
    Rectangle(hdc, 0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN));

    //    Display each line of instructions.
    for ( i = 0; i < cLines; i++ )
        {
        if ( LoadString(s_hinst, IdStringBase+i, buf, C_CALIBRATION_MSG_BUF_CHARS) == 0 )
            {
            continue;
            }
        cChars = _tcslen(buf);

        //    Figure text position.
        TextSize(hdc, buf, cChars, &Size);
        xText = GetSystemMetrics(SM_CXSCREEN)/2 - Size.cx/2;    //    Center horizontally

        //    If first pass through, figure vertical spacing,
        //    else just skip to the next line.
        if ( VertSpacing == 0 )
            {
            VertSpacing = Size.cy + Size.cy/10;
            yText = VertSpacing;        // Skip a line at the top
            }
        else
            {
            yText += VertSpacing;
            }

        //    Draw this line.
        ExtTextOut(
                  hdc,
                  xText, yText,
                  NULL, NULL,     //  rectangle options
                  buf, cChars,
                  NULL);
        }

    return;
}





/*++

    TouchCalibrateUI_DrawMainScreen:


--*/
void
TouchCalibrateUI_DrawMainScreen(
    HDC    hdc
    )
{
    UINT     IdStringBase;

    //    Figure out string resources based on whether keyboard is enabled.
    if ( UseEnterEsc() )
        {
        IdStringBase = IDS_CALIBRATION_1_1;
        }
    else
        {
        IdStringBase = IDS_CALIBRATION_1_1_NK;
        }

    TouchCalibrateDrawScreenText(hdc, C_CALIBRATION1_LINES, IdStringBase);

    return;
}




/*++

    TouchCalibrateUI_DrawConfirmationScreen:


--*/
void
TouchCalibrateUI_DrawConfirmationScreen(
    HDC    hdc
    )

{
    UINT     IdStringBase;

    //    Figure out string resources based on whether keyboard is enabled.
    if ( UseEnterEsc() )
        {
        IdStringBase = IDS_CALIBRATION_2_1;
        }
    else
        {
        IdStringBase = IDS_CALIBRATION_2_1_NK;
        }

    TouchCalibrateDrawScreenText(hdc, C_CALIBRATION2_LINES, IdStringBase);

    return;
}






/*++

    TouchCalibrateUI_HandleUserInputMessage:


--*/
void
TouchCalibrateUI_HandleUserInputMessage(
    UINT    Message, 
    UINT    Param1, 
    UINT    Param2
    )
{
    static BOOL bSawDown = FALSE;

    //    Ignore everything until actually confirming.
    if ( *s_ptcs != TCS_CONFIRMING )
        {
        bSawDown = FALSE;
        return;
        }

    //    Wait for Esc or Enter key or additional tap
    switch (Message) {
    case WM_SYSKEYDOWN :
        if ( VK_RETURN == Param1 )
            {
            TouchCalibrateUI_Done(TCS_ACCEPTED);
            }
        else if ( VK_ESCAPE == Param1 )
            {
            TouchCalibrateUI_Done(TCS_REJECTED);
            }
        break;
    case WM_LBUTTONDOWN :
        bSawDown = TRUE;
        break;
    case WM_LBUTTONUP :
        if ( bSawDown )
            {
            TouchCalibrateUI_Done(TCS_ACCEPTED);
            }
        break;
    }

    return;
}



void
TouchCalibrateUI_WaitForConfirmation(
    HANDLE    hevt,
    HDC        hdcConfirmation
    )
{
    int        TimeOut;
    DWORD    dwEventTimeOut;

    //    Wait here until TouchCalibrateUI_HandleUserInputMessage signals.
    //    If the keyboard is not enabled, we wait until a tap or for 30 seconds
    //    to decide what to do.  If there is a tap, the calibration is accepted.
    //    If we time out after 30 seconds, the calibration is rejected.

    //    If Keyboard is enabled, set timeout to INFINITE.
    //    If not, set the timeout to 1 sec and display a count down.
    dwEventTimeOut = UseEnterEsc() ? INFINITE : 1000;

    //    Assume a 1 sec timeout.  If the timeout is infinite, it won't
    //    matter.
    for ( TimeOut = 30 ; TimeOut >= 0 ; TimeOut--)
        {
        TCHAR    buf[C_CALIBRATION_MSG_BUF_CHARS];
        TCHAR    bufFormat[C_CALIBRATION_MSG_BUF_CHARS];
        int        cChars;
        int        xText, yText;
        SIZE    Size;

        WaitForSingleObject(hevt, dwEventTimeOut);

        //    If no longer in the confirming state (for whatever reason)
        //    we're done.
        if ( *s_ptcs != TCS_CONFIRMING )
            {
            break;
            }


        // Display countdown.
        LoadString(s_hinst, IDS_CALIBRATION_TIMEOUT, bufFormat, C_CALIBRATION_MSG_BUF_CHARS);
        StringCchPrintf(buf,C_CALIBRATION_MSG_BUF_CHARS,bufFormat,TimeOut);
        cChars = _tcslen(buf);
        TextSize (hdcConfirmation, buf, cChars, &Size);
        xText = GetSystemMetrics(SM_CXSCREEN)/2 - Size.cx/2;    //    Center
        yText = (Size.cy + Size.cy/10)*10;                        //    Set line.
        ExtTextOut(
                  hdcConfirmation,
                  xText, yText,
                  NULL, NULL, // rectangle options
                  buf, cChars,
                  NULL);

        }

    return;
}

