// SetActivePartition.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

typedef BOOL (*SetActivePartiton)(int);
typedef DWORD (*GetActivePartiton)(void);


HINSTANCE lHndlInterfaceSDKDll;

int _tmain(int argc, TCHAR *argv[], TCHAR *envp[])
{
	DWORD dwGpio , i=0;
	DWORD dwActivePartition = 0;
    _tprintf(_T("Set Active Partition!\n"));\

	if(argc<2) //If there is no parameter, load default value
	{
		RETAILMSG(1, (L"No Active Partition Specified \r\n"));
		goto EXIT;
	}
	else if(argc>2 || wcscmp(argv[1],L"?") == 0)
	{
		RETAILMSG(1, (L"Usage:\r\n"));
		RETAILMSG(1, (L"Help: SetActivePartition ?\r\n"));
		RETAILMSG(1, (L"SetActivePartition <Active Partition number >\r\n"));
		RETAILMSG(1, (L"i.e. SetActivePartition 2 --> this will Active Partition 2 and On next Boot It'll Boot from Second SD partition.\r\n"));
		goto EXIT;
	}

	
	dwActivePartition = (DWORD)_wtoi(argv[1]);

	RETAILMSG(1,(TEXT("dwActivePartition [%d]\t\r\n"),dwActivePartition));
	
	RETAILMSG(1, (TEXT("\r\n +-------------------------------------------------------------------+")));
	RETAILMSG(1, (TEXT("\r\n |                    Set SD Active Partition                                |")));
	RETAILMSG(1, (TEXT("\r\n +-------------------------------------------------------------------+\r\n \r\n ")));

	GetActivePartiton pfGetActivePartiton;
	SetActivePartiton pfSetActivePartiton;

	lHndlInterfaceSDKDll = LoadLibrary(L"InterfaceSDK.dll");
	if(lHndlInterfaceSDKDll==NULL)
    {
       RETAILMSG(1,(TEXT("-----Fail to LOAD InterfaceSDK.DLL------ \t\r\n")));
	   goto EXIT;
	}
	
	//Encoder DLL WriteRelayBit function Handle
	pfGetActivePartiton= (GetActivePartiton)GetProcAddress(lHndlInterfaceSDKDll,L"GetActivePartiton");
	pfSetActivePartiton= (SetActivePartiton)GetProcAddress(lHndlInterfaceSDKDll,L"SetActivePartiton");
	
	if((pfGetActivePartiton==NULL) || (pfSetActivePartiton==NULL))
	{
		   RETAILMSG(1,(TEXT("-----Fail to Open pfGetActivePartiton/pfSetActivePartiton Function Handle from InterfaceSDK.DLL------ \t\r\n")));
	}

	RETAILMSG(1,(TEXT("Current Active Boot Partition [0x%d]  \t\r\n"),pfGetActivePartiton()));
	
	pfSetActivePartiton(dwActivePartition);
	RETAILMSG(1,(TEXT("Make Active Boot Partition to [0x%d] \t\r\n"),dwActivePartition));
	
	dwActivePartition = pfGetActivePartiton();
	RETAILMSG(1,(TEXT("After Change: Active Boot Partition [0x%d]  \t\r\n"),dwActivePartition));

EXIT:
    return 0;
}
