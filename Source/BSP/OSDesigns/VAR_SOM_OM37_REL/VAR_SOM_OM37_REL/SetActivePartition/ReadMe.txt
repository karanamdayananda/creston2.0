////////////////////////////////////////////////////////////////////////

FEATURE NAME:



MANUFACTURER:



CONTACT INFO:



DATE:



VERSION:



SIZE:



DESCRIPTION:



NOTES:



////////////////////////////////////////////////////////////////////////


========================================================================
       CONSOLE APPLICATION : SetActivePartition
========================================================================


The Subproject Wizard has created this SetActivePartition application for you.  

This file contains a summary of what you will find in each of the files that
make up your SetActivePartition application.

SetActivePartition.pbpxml
    This file contains information at the subproject level and
    is used to build a single subproject for an OS design.
    Other users can share the subproject (.pbpxml) file.

SetActivePartition.cpp
    This is the main application source file.

/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named SetActivePartition.pch and a precompiled types file named StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
Other notes:

The Subproject Wizard uses "TODO:" to indicate parts of the source code you
should add to or customize.

/////////////////////////////////////////////////////////////////////////////
