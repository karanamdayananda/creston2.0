// SetUserPasswords.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "ntlmssp.h"

#define USER1		L"DataAnalyst"  
#define USER1_PASS	L"5k0ryj_P()ezd"  
#define USER2		L"Novatek"  
#define USER2_PASS	L"5nezhnyj_K()m"  

int _tmain(int argc, TCHAR *argv[], TCHAR *envp[])
{
	BOOL bRet = NTLMSetUserInfo(USER1, USER1_PASS);  

	if (bRet == FALSE)  
		RETAILMSG(1,(TEXT("-----Failed to set USER1 info------ \t\r\n")));  
	else  
		RETAILMSG(1,(TEXT("USER1 info updated \t\r\n")));

	bRet = NTLMSetUserInfo(USER2, USER2_PASS);  

	if (bRet == FALSE)  
		RETAILMSG(1,(TEXT("-----Failed to set USER2 info------ \t\r\n")));  
	else  
		RETAILMSG(1,(TEXT("USER2 info updated \t\r\n")));

	return 0;
}