// PWM_Test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>
#include <commctrl.h>

#include "pwm.h"

// PWM period of 128 can be used by both PWM0 and PWM1. However, this is the 
// only value acceptable by PWM1.
#define PWM_PERIOD 128

int _tmain(int argc, _TCHAR* argv[])
{
	PWM_CLOCK_INTERVALS sPwmClockIntervals;
    HANDLE      hDrvContext;
    BOOL  fEnable;
    DWORD dwBuffSz;

	RETAILMSG(1, (TEXT("PWM_Test: Start\r\n")));
    // Open the PWM1 driver (PMIC PWM)
	hDrvContext = CreateFile(L"PWM1:", GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
    if (hDrvContext == NULL) {
		RETAILMSG(1, (TEXT("ERROR: Can't open driver\r\n")));
        return -1;
    }
	RETAILMSG(1, (TEXT("PWM_Test: PWM1\r\n")));
	// for PMIC PWM (PWM1:) OnInterval can not be 0 or greater then OffInterval, if equal OffInterval output will be const ON.
	// TPS65930 TRM 7.4.2.2
	sPwmClockIntervals.OnInterval = 1; 
    sPwmClockIntervals.OffInterval = PWM_PERIOD/2;
    DeviceIoControl(hDrvContext, PWM_IOCTL_SET_CLOCK, &sPwmClockIntervals, sizeof(PWM_CLOCK_INTERVALS), NULL, 0, &dwBuffSz, NULL);
    // enable PWM wave output
    fEnable = TRUE;
    DeviceIoControl(hDrvContext, PWM_IOCTL_ENABLE, &fEnable, sizeof(BOOL), NULL, 0, &dwBuffSz, NULL);
		//return 0;
	Sleep(4000);
	for (int i=1;i<=PWM_PERIOD;i++)
	{
		sPwmClockIntervals.OnInterval = 1;
		sPwmClockIntervals.OffInterval = i+1;
		DeviceIoControl(hDrvContext, PWM_IOCTL_SET_CLOCK, &sPwmClockIntervals, sizeof(PWM_CLOCK_INTERVALS), NULL, 0, &dwBuffSz, NULL);
		Sleep(100);
	}
	
	Sleep(4000);
    for (int i=PWM_PERIOD-1;i>0;i--)
	{
		sPwmClockIntervals.OnInterval = 1;
		sPwmClockIntervals.OffInterval = i;
		DeviceIoControl(hDrvContext, PWM_IOCTL_SET_CLOCK, &sPwmClockIntervals, sizeof(PWM_CLOCK_INTERVALS), NULL, 0, &dwBuffSz, NULL);
		Sleep(100);
	}

	sPwmClockIntervals.OnInterval = 1; 
	sPwmClockIntervals.OffInterval = PWM_PERIOD/2;
	DeviceIoControl(hDrvContext, PWM_IOCTL_SET_CLOCK, &sPwmClockIntervals, sizeof(PWM_CLOCK_INTERVALS), NULL, 0, &dwBuffSz, NULL);
	Sleep(4000);
	fEnable = FALSE;
	DeviceIoControl(hDrvContext, PWM_IOCTL_ENABLE, &fEnable, sizeof(BOOL), NULL, 0, &dwBuffSz, NULL);

	// close driver
    CloseHandle(hDrvContext);

	// Open the PWM0 driver (OMAP GPT9 PWM)
	hDrvContext = CreateFile(L"PWM0:", GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
	if (hDrvContext == NULL) {
		RETAILMSG(1, (TEXT("ERROR: Can't open driver\r\n")));
		return -1;
	}
	RETAILMSG(1, (TEXT("PWM_Test: PWM0\r\n")));
	// For PWM0: Sum of the OnInterval and OffInterval defines PWM period 
	// (frequency).Timer source clock is 26 MHz, so if the sum of OnInterval and 
	// OffInterval is 128, PWM frequency will be 26000000/128 = 203125 Hz.
	// When OnInterval (or both intervals) is 0, PWM output is constant 0. When
	// OffInterval is 0 PWM output is constant 1 regardless of non-zero OnInterval.
	// Since timer registers are 32-bit wide, above sum must be less than 0xFFFFFFFF.
	sPwmClockIntervals.OnInterval = PWM_PERIOD/2; 
	sPwmClockIntervals.OffInterval = PWM_PERIOD/2;
	DeviceIoControl(hDrvContext, PWM_IOCTL_SET_CLOCK, &sPwmClockIntervals, sizeof(PWM_CLOCK_INTERVALS), NULL, 0, &dwBuffSz, NULL);
	// enable PWM wave output
	fEnable = TRUE;
	DeviceIoControl(hDrvContext, PWM_IOCTL_ENABLE, &fEnable, sizeof(BOOL), NULL, 0, &dwBuffSz, NULL);
	//return 0;
	Sleep(4000);
	for (int i=0;i<=PWM_PERIOD;i++)
	{
		sPwmClockIntervals.OnInterval = i;
		sPwmClockIntervals.OffInterval = PWM_PERIOD-i;
		DeviceIoControl(hDrvContext, PWM_IOCTL_SET_CLOCK, &sPwmClockIntervals, sizeof(PWM_CLOCK_INTERVALS), NULL, 0, &dwBuffSz, NULL);
		Sleep(100);
	}

	Sleep(4000);
	for (int i=PWM_PERIOD;i>=0;i--)
	{
		sPwmClockIntervals.OnInterval = i;
		sPwmClockIntervals.OffInterval = PWM_PERIOD-i;
		DeviceIoControl(hDrvContext, PWM_IOCTL_SET_CLOCK, &sPwmClockIntervals, sizeof(PWM_CLOCK_INTERVALS), NULL, 0, &dwBuffSz, NULL);
		Sleep(100);
	}
	
	sPwmClockIntervals.OnInterval = PWM_PERIOD/2; 
	sPwmClockIntervals.OffInterval = PWM_PERIOD/2;
	DeviceIoControl(hDrvContext, PWM_IOCTL_SET_CLOCK, &sPwmClockIntervals, sizeof(PWM_CLOCK_INTERVALS), NULL, 0, &dwBuffSz, NULL);
	Sleep(4000);
	fEnable = FALSE;
	DeviceIoControl(hDrvContext, PWM_IOCTL_ENABLE, &fEnable, sizeof(BOOL), NULL, 0, &dwBuffSz, NULL);

	// close driver
	CloseHandle(hDrvContext);

    return 0;
}

