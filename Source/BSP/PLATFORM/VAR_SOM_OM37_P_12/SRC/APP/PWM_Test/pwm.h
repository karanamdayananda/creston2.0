#ifndef __PWM_IF_H__
#define __PWM_IF_H__

#define PWM_DRV_NAME	L"PWM0:"

// PWM IoControl Opcodes
#define PWM_IOCTL_BASE			0x3a200000
#define PWM_IOCTL_ENABLE		((PWM_IOCTL_BASE)+0)
#define PWM_IOCTL_SET_CLOCK		((PWM_IOCTL_BASE)+1)
#define PWM_IOCTL_GET_CLOCK		((PWM_IOCTL_BASE)+2)

// PWM cycle has 2 parameters: OnInterval and OffInterval. Sum of the OnInterval 
// and OffInterval defines PWM period (frequency). Both OnTime and OffTime are 
// numbers of timer source clock cycles. Timer source clock is 26 MHz, so if the 
// sum of OnInterval and OffInterval is 500, PWM frequency will be 26000000/500 = 52 kHz.
// Since timer registers are 32-bit wide, above sum must be less than 0xFFFFFFFF.
typedef struct __PWM_CLOCK_INTERVALS
{
	UINT32	OnInterval;
	UINT32	OffInterval;
} PWM_CLOCK_INTERVALS, *PPWM_CLOCK_INTERVALS;

#endif // __PWM_IF_H__
