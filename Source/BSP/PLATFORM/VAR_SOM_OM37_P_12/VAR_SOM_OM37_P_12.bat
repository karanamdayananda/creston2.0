REM
REM              Texas Instruments OMAP(TM) Platform Software
REM  (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
REM
REM  Use of this software is controlled by the terms and conditions found
REM  in the license agreement under which this software has been supplied.
REM

REM --------------------------------------------------------------------------
REM Build Environment
REM --------------------------------------------------------------------------

REM Tell build system what to build in SOC folder
set BUILD_OPTIONS=COMMON_TI_V1_VSC

REM Always copy binaries to flat release directory
set WINCEREL=1
REM Generate .cod, .lst files
REM set WINCECOD=1

REM ----OS SPECIFIC VERSION SETTINGS----------

REM if "%SG_OUTPUT_ROOT%" == "" (set SG_OUTPUT_ROOT=%_PROJECTROOT%\cesysgen)

REM set _PLATCOMMONLIB=%_PLATFORMROOT%\common\lib
REM set _TARGETPLATLIB=%_TARGETPLATROOT%\lib
REM set _EBOOTLIBS=%SG_OUTPUT_ROOT%\oak\lib
REM set _KITLLIBS=%SG_OUTPUT_ROOT%\oak\lib

set _RAWFILETGT=%SG_OUTPUT_ROOT%\platform\%_TGTPLAT%\target

REM --------------------------------------------------------------------------
REM Initial Operating Point - VDD1 voltage, MPU (CPU) and IVA speeds
REM --------------------------------------------------------------------------

REM Select initial operating point (CPU and IVA speed, VDD1 voltage).
REM Note that this controls the operating point selected by the bootloader.
REM If the power management subsystem is enabled, the initial operating point
REM it uses is controlled by registry entries.
REM The following are choices for 37xx family
REM Use 4 for MPU[1000Mhz @ 1.375V], IVA2[800Mhz @ 1.375V], CORE[400Mhz @ 1.1375V] (OPMTM)
REM Use 3 for MPU[800Mhz  @ 1.2625V], IVA2[660Mhz @ 1.2625V], CORE[400Mhz @ 1.1375V] (OPM120)
REM Use 2 for MPU[600Mhz  @ 1.1000V], IVA2[520Mhz @ 1.1000V], CORE[400Mhz @ 1.1375V] (OPM100)
REM Use 1 for MPU[300Mhz  @ 0.9375V], IVA2[260Mhz @ 0.9375V], CORE[400Mhz @ 1.1375V] (OPM50)
set BSP_OPM_SELECT_37XX=4

REM The following are choices for 35xx family
REM Use 6 for MPU[720Mhz @ 1.350V], IVA2[520Mhz @ 1.350V], CORE[332Mhz @ 1.15V]
REM Use 5 for MPU[600Mhz @ 1.350V], IVA2[430Mhz @ 1.350V], CORE[332Mhz @ 1.15V]
REM Use 4 for MPU[550Mhz @ 1.275V], IVA2[400Mhz @ 1.275V], CORE[332Mhz @ 1.15V]
REM Use 3 for MPU[500Mhz @ 1.200V], IVA2[360Mhz @ 1.200V], CORE[332Mhz @ 1.15V]
REM Use 2 for MPU[250Mhz @ 1.000V], IVA2[180Mhz @ 1.000V], CORE[332Mhz @ 1.15V]
REM Use 1 for MPU[125Mhz @ 0.975V], IVA2[ 90Mhz @ 0.975V], CORE[332Mhz @ 1.05V]
set BSP_OPM_SELECT_35XX=5

set BSP_DISPALY_URT=1
set BSP_DISPALY_EDT_CTP=
set BSP_DISPALY_DVI_800W_600H=
set BSP_DISPALY_DVI_1024W_768H=
set BSP_DISPALY_DVI_1280W_720H=
set BSP_DISPALY_EBOOT_GENERIC=
set BSP_DSS_SPREAD=1

REM DSS spread spectrum only supports Variscite's LCDs
if "%BSP_DISPALY_URT%"=="1" goto AFTER_SPREAD
if "%BSP_DISPALY_EDT_CTP%"=="1" goto AFTER_SPREAD
set BSP_DSS_SPREAD=
: AFTER_SPREAD

set DYNAMIC_OS_BOOT=1

REM comment out to remove retail msgs
set BSP_RETAIL_MSGS=1

REM --------------------------------------------------------------------------
REM Misc. settings
REM --------------------------------------------------------------------------


REM TI BSP builds its own ceddk.dll. Setting this IMG var excludes default CEDDK from the OS image.
set IMGNODFLTDDK=1

REM This BSP targets by default the EVM2 board
set BSP_EVM2=1

REM Use this to enable 4 bit mmc cards
set BSP_EMMCFEATURE=1

REM --------------------------------------------------------------------------
REM Memory Settings
REM --------------------------------------------------------------------------

REM ES3.0 silicon (CPU marked JY192, 128MB SDRAM) does not support SDRAM bank 1
REM ES3.1 silicon (CPU marked JW256, 256MB SDRAM) supports SDRAM bank 1

REM Set to support 128MB of memory in SDRAM bank 1
REM Note that EVM SDRAM bank 0 is always assumed to be 128MB.
REM Note that if this variable is changed, a clean build and XLDR/EBOOT update is required.
set BSP_SDRAM_BANK1_ENABLE=1
REM Set to 1 for 512MB RAM SOMs (default 256MB)
set BSP_SDRAM_512MB=
REM Set to 1 for 128MB RAM SOMs (default 256MB)
set BSP_SDRAM_128MB=
if /i "%BSP_SDRAM_128MB%"=="1" set BSP_SDRAM_BANK1_ENABLE=

set BSP_OMAP_GPIO=1
set BSP_OMAP_PWM=1
set BSP_TPS659XX=1
set BSP_TPS659XX_GPIO=1
set BSP_TPS659XX_PWM=1
set BSP_TPS659XX_WAVE=1
set BSP_OMAP_MCBSP2=1
set BSP_OMAP_SDMA=1
if /i "%BSP_TPS659XX_WAVE%"=="1" set SYSGEN_AUDIO=1
set BSP_OMAP_MCSPI1=1
set BSP_OMAP_DSS=1
set BSP_TSC2046_TOUCH=1
set BSP_OMAP_NAND=1
REM set BSP_SMSC911X=1
set BSP_SDHC=1
set BSP_TPS659XX_RTC=1
set BSP_OMAP_USB_HOST2=1
set BSP_OMAP_MUSBOTG=1
set BSP_OMAP_MUSBFN=1
set BSP_OMAP_MUSBOTG=1
set BSP_OMAP_MUSBOTG_TPS659xx=1
set SYSGEN_USB=1
set SYSGEN_USBFN=1
set SYSGEN_KBD_US=1
set SYSGEN_NLS_EN_US=1
set BSP_OMAP_MUSBHCD=1
set BSP_NO_SCREENSAVER=1
set BSP_OMAP_UART1=1
set BSP_OMAP_UART2=1
set BSP_NO_SCREEN_SAVER=1
set BSP_NOI2CPROXY2=1
REM set BSP_NOI2CPROXY3=1
REM set BSP_OMAP_VRFB=1
set BSP_SDHIGHSPEEDSUPPORT_SDHC1=1


REM Set/Unset this variable to add/remove persistent regisrty support.
REM Note that if this variable is changed, a resysgen is required.
REM Mount NAND flash as root file system instead of the Object Store.
REM Enable persistent storage (registry, file cache, etc)
REM Default registry flush period is every 2 minutes, or on suspend
REM See: [HKEY_LOCAL_MACHINE\System\ObjectStore\RegFlush\FlushPeriod]
set IMGREGHIVE=1
echo "SET PRJ variables because IMGREGHIVE=%IMGREGHIVE%"
if /i "%IMGREGHIVE%"=="1" set PRJ_ENABLE_FSREGHIVE=1
if /i "%IMGREGHIVE%"=="1" set PRJ_ENABLE_REGFLUSH_THREAD=1
if /i "%IMGREGHIVE%"=="1" set PRJ_BOOTDEVICE_MSFLASH=1


REM Set to assemble NEON library which requires ARM assembler with arm arch7 support
set ASSEMBLER_ARM_ARCH7_SUPPORT=1

:EXIT
