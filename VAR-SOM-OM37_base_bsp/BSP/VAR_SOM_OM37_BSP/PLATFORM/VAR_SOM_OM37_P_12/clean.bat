@echo off

@rem delete transient build files
echo.
echo *********************************
echo Deleting transient build files
echo *********************************
@del /s /q build.dat
@del /s /q build.wrn
@del /s /q build.err
@del /s /q build.log
@del /s /q *.bif

@rd /s /q target
@rd /s /q lib

@rem delete obj directories
@for /D /R %%i in (obj) do @rd /s /q %%i

