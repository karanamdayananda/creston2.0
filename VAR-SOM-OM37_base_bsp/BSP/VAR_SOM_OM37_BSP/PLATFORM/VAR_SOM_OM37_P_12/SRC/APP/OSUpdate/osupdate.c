//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES.
//

// Copyright Texas Instruments, Inc. 2011

#pragma warning(push)
#pragma warning(disable : 4115 6067)
#include <windows.h>
#include "oalex.h"
#include "boot_args.h"

void RetailPrint(wchar_t *pszFormat, ...);
//extern int	CreateArgvArgc(TCHAR *pProgName, TCHAR *argv[20], TCHAR *pCmdLine);

#define OSU_IOCTL_BASE				0x35300000
#define OSU_IOCTL_SET_FILE			((OSU_IOCTL_BASE)+0)
#define OSU_IOCTL_UPDATE			((OSU_IOCTL_BASE)+1)
#define OSU_IOCTL_UPDATE_EBOOT		((OSU_IOCTL_BASE)+2)
#define OSU_IOCTL_UPDATE_LOGO		((OSU_IOCTL_BASE)+3)
#define OSU_IOCTL_READ_BOOTARGS		((OSU_IOCTL_BASE)+4)
#define OSU_IOCTL_WRITE_BOOTARGS	((OSU_IOCTL_BASE)+5)

TCHAR	strOsImage[MAX_PATH] = L"\\Hard Disk\\NK.nb0";
TCHAR	strOsEboot[MAX_PATH] = L"\\Hard Disk\\Eboot.nb0";
TCHAR	strOsLogo[MAX_PATH] = L"\\Hard Disk\\logo.bmp";
BOOT_CFG BootArgs;

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPWSTR lpCmdLine, int nCmShow)
{
	HANDLE		hDrvContext;
	DWORD		dwErr=1;
	DWORD		dwRet;
    
    UNREFERENCED_PARAMETER(hInst);
    UNREFERENCED_PARAMETER(hPrevInst);
    UNREFERENCED_PARAMETER(nCmShow);

	// Open OSU driver
	hDrvContext = 
        CreateFile(L"OSU0:", GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);

	if (hDrvContext == INVALID_HANDLE_VALUE) {
		RETAILMSG(1, (TEXT("ERROR: Can't open OSU driver\r\n")));
		return -1;
	}	

	RETAILMSG(1, (TEXT("Reading boot config\r\n")));
	if (!DeviceIoControl(hDrvContext, OSU_IOCTL_READ_BOOTARGS, NULL, 0, &BootArgs, sizeof(BootArgs), &dwRet, NULL))
	{
		RETAILMSG(1, (L"ERROR: can't read Boot_CFG, err = %d\r\n", GetLastError()));
	}
	RETAILMSG(1, (L"Boot Args ver %d signature %x\r\n", BootArgs.version, BootArgs.signature));
	
	if (!DeviceIoControl(hDrvContext, OSU_IOCTL_WRITE_BOOTARGS, &BootArgs, sizeof(BootArgs), NULL, 0, &dwRet, NULL))
	{
		RETAILMSG(1, (L"ERROR: can't write Boot_CFG, err = %d\r\n", GetLastError()));
	}
	
	RetailPrint(TEXT("Updating Eboot from %s\n"), strOsEboot);
	DeviceIoControl(hDrvContext, OSU_IOCTL_SET_FILE, strOsEboot, _tcslen(strOsEboot)*sizeof(TCHAR), NULL, 0, NULL, NULL);	

	// start update process
	if (!DeviceIoControl(hDrvContext, OSU_IOCTL_UPDATE_EBOOT, NULL, 0, NULL, 0, NULL, NULL))
	{		
		LPVOID lpMsgBuf = NULL;
		DWORD dw = GetLastError(); 

		// REQUIRES SYSGEN_FMTMSG AND SYSGEN_FMTRES
		dwErr = FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | 
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			dw,
			0,
			(LPTSTR) &lpMsgBuf,
			512, NULL );

		// Display the error message		
		RETAILMSG(1, (L"ERROR: can't update EBOOT, err = %d/%d, %s\r\n", dw, dwErr, lpMsgBuf));		
	}

	RetailPrint(TEXT("Updating Logo from %s\n"), strOsLogo);

	DeviceIoControl(hDrvContext, OSU_IOCTL_SET_FILE, strOsLogo, _tcslen(strOsLogo)*sizeof(TCHAR), NULL, 0, NULL, NULL);	

	// start update process
	if (!DeviceIoControl(hDrvContext, OSU_IOCTL_UPDATE_LOGO, NULL, 0, NULL, 0, NULL, NULL))
	{		
		LPVOID lpMsgBuf = NULL;
		DWORD dw = GetLastError(); 

		// REQUIRES SYSGEN_FMTMSG AND SYSGEN_FMTRES
		dwErr = FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | 
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			dw,
			0,
			(LPTSTR) &lpMsgBuf,
			512, NULL );

		// Display the error message		
		RETAILMSG(1, (L"ERROR: can't update Logo, err = %d/%d, %s\r\n", dw, dwErr, lpMsgBuf));		
	}

	RetailPrint(TEXT("Updating NK image from %s\n"), strOsImage);
	// set file name holding new OS image
	DeviceIoControl(hDrvContext, OSU_IOCTL_SET_FILE, strOsImage, _tcslen(strOsImage)*sizeof(TCHAR), NULL, 0, NULL, NULL);
	// start update process
	if (!DeviceIoControl(hDrvContext, OSU_IOCTL_UPDATE, NULL, 0, NULL, 0, NULL, NULL))
	{		
		LPVOID lpMsgBuf = NULL;
		DWORD dw = GetLastError(); 

		// REQUIRES SYSGEN_FMTMSG AND SYSGEN_FMTRES
		dwErr = FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | 
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			dw,
			0,
			(LPTSTR) &lpMsgBuf,
			512, NULL );

		// Display the error message		
		RETAILMSG(1, (L"ERROR: can't update OS, err = %d/%d, %s\r\n", dw, dwErr, lpMsgBuf));		
	}

    return 0;
}

///////////////////////////////////////////////////////////////////////////////

void RetailPrint(wchar_t *pszFormat, ...)
{
    va_list al;
    wchar_t szTemp[2048];
    wchar_t szTempFormat[2048];

    va_start(al, pszFormat);
    vwprintf(pszFormat, al);
    // Show message on RETAILMSG
    swprintf(szTempFormat, L"OSUPDATE: %s\r", pszFormat);
    pszFormat = szTempFormat;
    vswprintf(szTemp, pszFormat, al);
    RETAILMSG(1, (szTemp));

    va_end(al);
}
#pragma warning(pop)
