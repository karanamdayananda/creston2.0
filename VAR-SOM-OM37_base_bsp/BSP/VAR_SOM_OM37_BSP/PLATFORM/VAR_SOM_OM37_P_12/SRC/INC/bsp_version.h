//
//  File:  bsp_version.h
//
#ifndef __BSP_VERSION_H
#define __BSP_VERSION_H

#define BSP_VERSION_MAJOR       1
#define BSP_VERSION_MINOR       3
#define BSP_VERSION_QFES		4
#define BSP_VERSION_INCREMENTAL 01

#define BSP_VERSION_STRING      L"VAR-SOM-OM37 CE7 1.3.4"

#endif
