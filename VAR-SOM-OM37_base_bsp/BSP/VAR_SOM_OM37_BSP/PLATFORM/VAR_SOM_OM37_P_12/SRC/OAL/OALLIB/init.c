// All rights reserved ADENEO EMBEDDED 2010
// Copyright (c) 2007, 2008 BSQUARE Corporation. All rights reserved.

/*
================================================================================
*             Texas Instruments OMAP(TM) Platform Software
* (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
*
================================================================================
*/

#include "bsp.h"
#include "oalex.h"
#include "ceddkex.h"

#include "oal_alloc.h"
#include "oal_i2c.h"
#include "bsp_cfg.h"
#include "oal_padcfg.h"
#include "bsp_padcfg.h"
#include "oal_clock.h"
#include "oal_gptimer.h"

#include "s35390_rtc.h"
#include "omap_cpuver.h"

#ifdef TEST_TPS65023
#include "triton.h"
#include "tps65023.h"
#include "sdk_i2c.h"
#endif

#if (_WINCEOSVER >= 700)
#include <vfpSupport.h>
#include <bldver.h>
#endif

//------------------------------------------------------------------------------
//  External functions

extern DWORD GetCp15ControlRegister(void);
extern DWORD GetCp15AuxiliaryControlRegister(void);
extern DWORD OALGetL2Aux(void);
extern void EnableUnalignedAccess(void);

extern LPCWSTR g_oalIoCtlPlatformType;
extern LPCWSTR g_oalIoCtlPlatformName;
extern LPCWSTR g_oalIoCtlProcessorName;

//------------------------------------------------------------------------------
//  Global FixUp variables
//
//
const volatile DWORD dwOEMDrWatsonSize     = 0x0004B000;
const volatile DWORD dwOEMHighSecurity     = OEM_HIGH_SECURITY_GP;

//Those variable are used to tell whether the system should claime the BANK1 and CMEM Dsp RAM area for itself
// They are updating during the image generation (not at compilation time).
const volatile DWORD dwBank1Enabled     = (DWORD)-1;
const volatile DWORD dwCMemDSPEnabled   = (DWORD)-1;
const volatile DWORD dwRamdiskEnabled   = (DWORD)-1;
const volatile DWORD dwDSP720pEnabled   = (DWORD)-1;


//------------------------------------------------------------------------------
//  Global variables

//-----------------------------------------------------------------------------
//
//  Global:  g_CpuFamily
//
//  Set during OEMInit to indicate CPU family.
//
DWORD g_dwCpuFamily;

//-----------------------------------------------------------------------------
//
//  Global:  g_CpuFamily
//
//  Set during OEMInit to indicate CPU family.
//

DWORD g_dwCpuRevision = (DWORD)CPU_REVISION_UNKNOWN;

//------------------------------------------------------------------------------
//
//  Global:  dwOEMSRAMStartOffset
//
//  offset to start of SRAM where SRAM routines will be copied to.
//  Reinitialized in config.bib (FIXUPVAR)
//
DWORD dwOEMSRAMStartOffset = 0x00008000;

#if 0
//------------------------------------------------------------------------------
//
//  Global:  dwOEMVModeSetupTime
//
//  Setup time for DVS transitions. Reinitialized in config.bib (FIXUPVAR)
//
DWORD dwOEMVModeSetupTime = 2;
#endif
//------------------------------------------------------------------------------
//
//  Global:  dwOEMPRCMCLKSSetupTime
//
//  Timethe PRCM waits for system clock stabilization.
//  Reinitialized in config.bib (FIXUPVAR)
//
const volatile DWORD dwOEMPRCMCLKSSetupTime = 0x140;//0x2;

//------------------------------------------------------------------------------
//
//  Global:  dwOEMMPUContextRestore
//
//  location to store context restore information from off mode (PA)
//
const volatile DWORD dwOEMMPUContextRestore = CPU_INFO_ADDR_PA;

//------------------------------------------------------------------------------
//
//  Global:  dwOEMMaxIdlePeriod
//
//  maximum idle period during OS Idle in milliseconds
//
DWORD dwOEMMaxIdlePeriod = 1000;

//------------------------------------------------------------------------------

//extern DWORD gdwFailPowerPaging;
//extern DWORD cbNKPagingPoolSize;

//------------------------------------------------------------------------------
//
//  Global:  g_oalKitlEnabled
//
//  Save kitl state return by KITL intialization
//
//
DWORD g_oalKitlEnabled;

//-----------------------------------------------------------------------------
//
//  Global:  g_oalRetailMsgEnable
//
//  Used to enable retail messages
//
BOOL   g_oalRetailMsgEnable = FALSE;

//-----------------------------------------------------------------------------
//
//  Global:  g_ResumeRTC
//
//  Used to inform RTC code that a resume occured
//
BOOL g_ResumeRTC = FALSE;

//-----------------------------------------------------------------------------
//
//  Global:  g_dwMeasuredSysClkFreq
//
//  The measured SysClk frequency
//
extern DWORD g_dwMeasuredSysClkFreq;

//-----------------------------------------------------------------------------
//
//  Global:  g_pTimerRegs
//
//  32K timer register
//
extern OMAP_GPTIMER_REGS* g_pTimerRegs;
//-----------------------------------------------------------------------------
//
//  Global:  gDevice_prefix
//
//  BSP_prefix
//

CHAR  *gDevice_prefix;

extern  UINT32 g_oalIoCtlClockSpeed;

//------------------------------------------------------------------------------
//  Local functions
//
static void OALGPIOSetDefaultValues();
static void OALCalibrateSysClk();
#ifndef BSP_SDRAM_512MB
static DWORD OEMEnumExtensionDRAM(PMEMORY_SECTION pMemSections,DWORD cMemSections);
#endif

#ifdef BSP_SDRAM_512MB
#define DEVICE_RAM_512_PA 0x90000000 // upper 256mb of 512mb RAM boundry
	#if (UNDER_CE >= 700)
		//-----------------------------------------------------------------------------
		// Global Variables
		RAMTableEntry g_RamTableEntry[] = 
		{
			//physical memory start address >> 8, memory size, attribute must be 0
			{(DEVICE_RAM_512_PA) >> 8, 256 * 1024 * 1024, 0},
			{(IMAGE_CMEM_CA) >> 8, 0, 0},
			{(IMAGE_DSP_CA) >> 8, 0, 0},			
			{(IMAGE_WINCE_RAM_BANK1_CA) >> 8, 0, 0},		
			{(IMAGE_DSP_720P_CA) >> 8, 0, 0},		
		};

		RamTable g_RAMTable = {MAKELONG(CE_MINOR_VER, CE_MAJOR_VER), sizeof(g_RamTableEntry) / sizeof(g_RamTableEntry[0]), g_RamTableEntry};

		//-----------------------------------------------------------------------------
		//
		// Function: OEMGetRamTable
		// This function is implemented by the OEM to return the OEMRamTable structure, 
		// which allows your platform to support more than 512 MB of physical memory.
		// Parameters:
		//
		// Returns: 
		// Returns an OEMRamTable structure, as defined in %_WINCEROOT%\Public\Common\Oak\Inc\OEMGlobal.h.
		//
		//
		//-----------------------------------------------------------------------------
		PCRamTable OEMGetRamTable(void)
		{
			UINT32 count = 1;

			OALMSG(1, (L"OAL: OEMGetRamTable \r\n"));
			// If CMEM region is not used for the DSP, give it to the OS
			if (dwCMemDSPEnabled != 1)	{
				g_RamTableEntry[count].RamAttributes 		= 0;
				g_RamTableEntry[count].ShiftedRamBase 	= IMAGE_CMEM_CA >> 8;
				g_RamTableEntry[count].RamSize 			= IMAGE_CMEM_SIZE;
				count++;
			
			// If DSP region is not used for the DSP, give it to the OS
				g_RamTableEntry[count].RamAttributes 		= 0;
				g_RamTableEntry[count].ShiftedRamBase 	= IMAGE_DSP_CA >> 8;
				g_RamTableEntry[count].RamSize 			= IMAGE_DSP_SIZE;
				count++;
			}  

			g_RamTableEntry[count].RamAttributes		= 0;
			g_RamTableEntry[count].ShiftedRamBase	= IMAGE_WINCE_RAM_BANK1_CA >> 8;
			g_RamTableEntry[count].RamSize			= IMAGE_WINCE_RAM_BANK1_SIZE;
			count++;
			
			if (dwDSP720pEnabled != 1)	{
				
				g_RamTableEntry[count].RamAttributes		= 0;
				g_RamTableEntry[count].ShiftedRamBase	= IMAGE_DSP_720P_CA >> 8;
				g_RamTableEntry[count].RamSize			= IMAGE_DSP_720P_SIZE;
				count++;
			}				
			   
					
			g_RAMTable.dwNumEntries=count;
			return &g_RAMTable;
		}
	#endif
#endif	

//------------------------------------------------------------------------------
//
//  Function:  OEMInit
//
//  This is Windows CE OAL initialization function. It is called from kernel
//  after basic initialization is made.
//
VOID
OEMInit(
    )
{    
    BOOL           *pColdBoot;

    UINT32         CpuRevision;	
    static const PAD_INFO gpioPads[] = {GPIO_PADS END_OF_PAD_ARRAY};
    static const PAD_INFO gpioPads_37xx[] = {GPIO_PADS_37XX END_OF_PAD_ARRAY};	
    static UCHAR allocationPool[2048];
	OMAP_SYSC_GENERAL_REGS	   *pSysCtrlGenReg = NULL;
	OMAP_SYSC_GENERAL_WKUP_REGS_DM3730 *pSysWKUPReg;


    //----------------------------------------------------------------------
    // Initialize OAL log zones
    //----------------------------------------------------------------------

    OALLogSetZones( //(0xFFFF & ~((1<<OAL_LOG_CACHE)|(1<<OAL_LOG_INTR)))|
    //           (1<<OAL_LOG_VERBOSE)  |
    //           (1<<OAL_LOG_INFO)     |
               (1<<OAL_LOG_ERROR)    |
               (1<<OAL_LOG_WARN)     
    //           (1<<OAL_LOG_IOCTL)    |
    //           (1<<OAL_LOG_FUNC)     |
    //           (1<<OAL_LOG_INTR)     |    
               );

    OALMSG(1, (L"+OEMInit\r\n"));

    //----------------------------------------------------------------------
    // Initialize the OAL memory allocation system (TI code)
    //----------------------------------------------------------------------
    OALLocalAllocInit(allocationPool,sizeof(allocationPool));

    //----------------------------------------------------------------------
    // Determion CPU revison
    //----------------------------------------------------------------------
    CpuRevision = Get_CPUVersion();
    g_dwCpuRevision = CpuRevision & CPU_REVISION_MASK;
    g_dwCpuFamily = (CpuRevision >> CPU_FAMILY_SHIFT) & CPU_REVISION_MASK;
    /* save CPU family names */
    if(g_dwCpuFamily == CPU_FAMILY_DM37XX)
    {
        g_oalIoCtlPlatformType = L"VAR-OM37";
        g_oalIoCtlPlatformName = L"VAR-OM37";
        g_oalIoCtlProcessorName   = L"VAR-OM37";	  
        gDevice_prefix = "VAR-OM37-";
    }
    else if (g_dwCpuFamily == CPU_FAMILY_OMAP35XX) 
    {
        g_oalIoCtlPlatformType = L"VAR-OM35";
        g_oalIoCtlPlatformName = L"VAR-OM35";		
        g_oalIoCtlProcessorName   = L"VAR-OM35";	
        gDevice_prefix = "VAR-OM35-";
    }
    else
    {
        OALMSG(OAL_ERROR, (L"OEMInit: unknow CPU Family %d\r\n", g_dwCpuFamily));
        g_oalIoCtlPlatformType = L"EVM";
        g_oalIoCtlPlatformName = L"EVM";		
        g_oalIoCtlProcessorName   = L"OMAP3";	
        gDevice_prefix = "EVM-";
		
    }
    OALMSG(OAL_ERROR, (L"OAL: CPU revision 0x%x:%s\r\n", g_dwCpuRevision, g_oalIoCtlProcessorName));

    OALMSG(1, (L"OAL: CPU L2 Aux register 0x%x\r\n", OALGetL2Aux()));
    //----------------------------------------------------------------------
    // Update platform specific variables
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // Update kernel variables
    //----------------------------------------------------------------------

    dwNKDrWatsonSize = dwOEMDrWatsonSize;

    // Alarm has resolution 10 seconds (actually has 1 second resolution, 
	// but setting alarm too close to suspend will cause problems).
    dwNKAlarmResolutionMSec = 10000;

    // Set extension functions
    pOEMIsProcessorFeaturePresent = OALIsProcessorFeaturePresent;
    pfnOEMSetMemoryAttributes     = OALSetMemoryAttributes;

#ifndef BSP_SDRAM_512MB
    g_pOemGlobal->pfnEnumExtensionDRAM = OEMEnumExtensionDRAM;
#endif
    //----------------------------------------------------------------------
    // Windows Mobile backward compatibility issue...
    //----------------------------------------------------------------------
/*
    switch (dwOEMTargetProject)
        {
        case OEM_TARGET_PROJECT_SMARTFON:
        case OEM_TARGET_PROJECT_WPC:
            CEProcessorType = PROCESSOR_STRONGARM;
            break;
        }
*/
    //----------------------------------------------------------------------
    // Initialize cache globals
    //----------------------------------------------------------------------

    OALCacheGlobalsInit();
    
    EnableUnalignedAccess();
    
    #ifdef DEBUG
        OALMSG(1, (L"CPU CP15 Control Register = 0x%x\r\n", GetCp15ControlRegister()));
        OALMSG(1, (L"CPU CP15 Auxiliary Control Register = 0x%x\r\n", GetCp15AuxiliaryControlRegister()));
    #endif
    //----------------------------------------------------------------------
    // Initialize PAD cfg
    //----------------------------------------------------------------------
    OALPadCfgInit();
	
    //----------------------------------------------------------------------
    // configure pin mux
    //----------------------------------------------------------------------
    ConfigurePadArray(BSPGetAllPadsInfo());
    //----------------------------------------------------------------------
    // Initialize Power Domains
    //----------------------------------------------------------------------
    
    OALPowerInit();

    //----------------------------------------------------------------------
    // Initialize Vector Floating Point co-processor
    //----------------------------------------------------------------------

#if (_WINCEOSVER >= 700)
        VfpOemInit(g_pOemGlobal, VFP_AUTO_DETECT_FPSID);
#else
    OALVFPInitialize(g_pOemGlobal);
#endif

#ifdef BSP_SDRAM_512MB
	#if (UNDER_CE >= 700)
		//enable RAM >= 512M 
		g_pOemGlobal->pfnGetOEMRamTable = OEMGetRamTable; 
	#endif
#endif
    //----------------------------------------------------------------------
    // Initialize interrupt
    //----------------------------------------------------------------------

    if (!OALIntrInit())
        {
        OALMSG(OAL_ERROR, (
            L"ERROR: OEMInit: failed to initialize interrupts\r\n"
            ));
        goto cleanUp;
        }

    //----------------------------------------------------------------------
    // Initialize system clock
    //----------------------------------------------------------------------

    if (!OALTimerInit(1, 0, 0))
        {
        OALMSG(OAL_ERROR, (
            L"ERROR: OEMInit: Failed to initialize system clock\r\n"
            ));
        goto cleanUp;
        }

    // Configure the pads for the DSS (to keep the splashscreen active)
    // do not request it, it may make the DSS driver fail to load (because it will not ba able to request its pads)
    ConfigurePadArray(BSPGetDevicePadInfo(OMAP_DEVICE_DSS));
    //same thing for the UART3 (used for our OAL serial output
    ConfigurePadArray(BSPGetDevicePadInfo(OMAP_DEVICE_UART1));
    ConfigurePadArray(BSPGetDevicePadInfo(OMAP_DEVICE_UART2));
    ConfigurePadArray(BSPGetDevicePadInfo(OMAP_DEVICE_UART3));
    ConfigurePadArray(BSPGetDevicePadInfo(OMAP_DEVICE_HSOTGUSB));

    //all other pads are to be requested (GPMC is never reserved by drivers, I2C is handled by the kernel)
    // GPIOs reservation may be split on per-GPIO basis and moved into the drivers that needs the GPIO. TBD
    if (!RequestDevicePads(OMAP_DEVICE_GPMC)) OALMSG(OAL_ERROR, (TEXT("Failed to request pads for GPMC\r\n")));
    if (!RequestDevicePads(OMAP_DEVICE_I2C1)) OALMSG(OAL_ERROR, (TEXT("Failed to request pads for I2C1\r\n")));
    if (!RequestDevicePads(OMAP_DEVICE_I2C2)) OALMSG(OAL_ERROR, (TEXT("Failed to request pads for I2C2\r\n")));
    if (!RequestDevicePads(OMAP_DEVICE_I2C3)) OALMSG(OAL_ERROR, (TEXT("Failed to request pads for I2C3\r\n")));
	  
  
	// Enable use of GPIO126, GPIO129
	pSysCtrlGenReg = OALPAtoUA(OMAP_SYSC_GENERAL_REGS_PA);
    pSysWKUPReg = OALPAtoUA(OMAP_SYSC_GENERAL_WKUP_REGS_DM3730_PA);
    
    SETREG32(&pSysCtrlGenReg->CONTROL_PBIAS_LITE, 0x303);  // Enable PBIAS
    SETREG32(&pSysWKUPReg->CONTROL_WKUP_CTRL, 0x40); // Enable VDDS bias


    if(g_dwCpuFamily == CPU_FAMILY_DM37XX)
    {
        if (!RequestAndConfigurePadArray(gpioPads_37xx)) OALMSG(OAL_ERROR, (TEXT("Failed to request pads for the GPIOs\r\n")));
    }
    else if(g_dwCpuFamily == CPU_FAMILY_OMAP35XX)
    {
        if (!RequestAndConfigurePadArray(gpioPads)) OALMSG(OAL_ERROR, (TEXT("Failed to request pads for the GPIOs\r\n")));
    }
    else
    {
        OALMSG(OAL_ERROR, (
            L"ERROR: OEMInit: CPU family %d is not supported\r\n", g_dwCpuFamily
            ));
    }

    GPIOInit();

    //----------------------------------------------------------------------
    // Set GPIOs default values (like the buffers' OE)
    //----------------------------------------------------------------------
    OALGPIOSetDefaultValues();

    //----------------------------------------------------------------------
    // Initialize SRAM Functions
    //----------------------------------------------------------------------
    OALSRAMFnInit();
    
    //----------------------------------------------------------------------
    // kSYS_CLK calibration
    // Now compute the real kSYS_CLK clock value. 
    //----------------------------------------------------------------------
    OALCalibrateSysClk();

    //----------------------------------------------------------------------
    // Initialize high performance counter and profiling function pointers
    //----------------------------------------------------------------------
    OALPerformanceTimerInit();



    //----------------------------------------------------------------------
    // Initialize KITL
    //----------------------------------------------------------------------

    g_oalKitlEnabled = KITLIoctl(IOCTL_KITL_STARTUP, NULL, 0, NULL, 0, NULL);

    //----------------------------------------------------------------------
    // Initialize the watchdog
    //----------------------------------------------------------------------
#ifdef BSP_OMAP_WATCHDOG
    OALWatchdogInit(BSP_WATCHDOG_PERIOD_MILLISECONDS,BSP_WATCHDOG_THREAD_PRIORITY);
#endif

    //----------------------------------------------------------------------
    // Check for retail messages enabled
    //----------------------------------------------------------------------

// Retreive from main batch file
#if BSP_RETAIL_MSGS
		  g_oalRetailMsgEnable = TRUE;
#else
		  g_oalRetailMsgEnable = FALSE;
#endif


    //----------------------------------------------------------------------
    // Deinitialize serial debug
    //----------------------------------------------------------------------

//        OEMDeinitDebugSerial();

// not available under CE6
#if (_WINCEOSVER >= 700)
    //----------------------------------------------------------------------
    // Make Page Tables walk L2 cacheable. There are 2 new fields in OEMGLOBAL
    // that we need to update:
    // dwTTBRCacheBits - the bits to set for TTBR to change page table walk
    //                   to be L2 cacheable. (Cortex-A8 TRM, section 3.2.31)
    //                   Set this to be "Outer Write-Back, Write-Allocate".
    // dwPageTableCacheBits - bits to indicate cacheability to access Level
    //                   L2 page table. We need to set it to "inner no cache,
    //                   outer write-back, write-allocate. i.e.
    //                      TEX = 0b101, and C=B=0.
    //                   (ARM1176 TRM, section 6.11.2, figure 6.7, small (4k) page)
    //----------------------------------------------------------------------
    g_pOemGlobal->dwTTBRCacheBits = 0x8;            // TTBR RGN set to 0b01 - outer write back, write-allocate
    g_pOemGlobal->dwPageTableCacheBits = 0x140;     // Page table cacheability uses 1BB/AA format, where AA = 0b00 (inner non-cached)
#endif

    g_oalIoCtlClockSpeed = Get_CPUMaxSpeed(g_dwCpuFamily); //get MPU clock rate
    g_oalIoCtlPlatformManufacturer = L"Variscite LTD";
    g_oalIoCtlPlatformName = L"VAR-SOM-OM37";

    //----------------------------------------------------------------------
    // Check for a clean boot of device
    //----------------------------------------------------------------------
    pColdBoot = OALArgsQuery(OAL_ARGS_QUERY_COLDBOOT);
    if ((pColdBoot == NULL)|| ((pColdBoot != NULL) && *pColdBoot))
        NKForceCleanBoot();
cleanUp:
    OALMSG(1, (L"-OEMInit\r\n"));
}

//------------------------------------------------------------------------------

void
OALCalibrateSysClk()
{
    DWORD dw32k_prev,dw32k, dw32k_diff;
    DWORD dwSysk_prev,dwSysk, dwSys_diff;
    DWORD dwOld;
    OMAP_DEVICE gptPerfDevice = BSPGetGPTPerfDevice();
    OMAP_GPTIMER_REGS   *pPerfTimer = OALPAtoUA(GetAddressByDevice(gptPerfDevice));
    EnableDeviceClocks(gptPerfDevice, TRUE);

    // configure performance timer
    //---------------------------------------------------
    // Soft reset GPTIMER and wait until finished
    SETREG32(&pPerfTimer->TIOCP, SYSCONFIG_SOFTRESET);
    while ((INREG32(&pPerfTimer->TISTAT) & GPTIMER_TISTAT_RESETDONE) == 0);
 
    // Enable smart idle and autoidle
    // Set clock activity - FCLK can be  switched off, 
    // L4 interface clock is maintained during wkup.
    OUTREG32(&pPerfTimer->TIOCP, 
        0x200 | SYSCONFIG_SMARTIDLE|SYSCONFIG_ENAWAKEUP|
            SYSCONFIG_AUTOIDLE);
    // clear interrupts
    OUTREG32(&pPerfTimer->TISR, 0x00000000);

    //  Start the timer.  Also set for auto reload
    SETREG32(&pPerfTimer->TCLR, GPTIMER_TCLR_ST);
    while ((INREG32(&pPerfTimer->TWPS) & GPTIMER_TWPS_TCLR) != 0);
    
#if SHOW_SYS_CLOCK_VARIATION
    int i;
    for (i=0; i<100;i++)
    {
#endif

    dwOld = OALTimerGetReg(&g_pTimerRegs->TCRR);
    do 
    {
        dwSysk_prev = INREG32(&pPerfTimer->TCRR); 
        dw32k_prev = OALTimerGetReg(&g_pTimerRegs->TCRR);
    } while (dw32k_prev == dwOld);

    OALStall(100000);

    dwOld = OALTimerGetReg(&g_pTimerRegs->TCRR);
    do
    {
        dwSysk = INREG32(&pPerfTimer->TCRR);
        dw32k = OALTimerGetReg(&g_pTimerRegs->TCRR);
    } while (dw32k == dwOld);

    dw32k_diff = dw32k - dw32k_prev;
    dwSys_diff = dwSysk - dwSysk_prev;
    
    g_dwMeasuredSysClkFreq =  (DWORD) (((INT64)dwSys_diff * 32768) / ((INT64)dw32k_diff)) ;

    DEBUGMSG(1,(L"SysClock calibrate Frequency = %d\r\n", g_dwMeasuredSysClkFreq));    

#if SHOW_SYS_CLOCK_VARIATION
    }
#endif

    EnableDeviceClocks(gptPerfDevice, FALSE);

}

//------------------------------------------------------------------------------


DWORD
OALMux_UpdateOnDeviceStateChange(
    UINT devId,
    UINT oldState,
    UINT newState,
    BOOL bPreStateChange
    )
{
    UNREFERENCED_PARAMETER(devId);
    UNREFERENCED_PARAMETER(oldState);
    UNREFERENCED_PARAMETER(newState);
    UNREFERENCED_PARAMETER(bPreStateChange);
    return (DWORD) -1;
}
void
OALMux_InitMuxTable(
    )
{
}

void EnableDebugSerialClock()
{               
#define OMAP_PRCM_PER_CM_REGS_PA            0x48005000
#define CM_CLKEN_UART3                      (1 << 11)

    OMAP_CM_REGS* pCmRegs;
    pCmRegs = (OMAP_CM_REGS*) (OMAP_PRCM_PER_CM_REGS_PA);
    SETREG32(&pCmRegs->CM_FCLKEN_xxx, CM_CLKEN_UART3);
    SETREG32(&pCmRegs->CM_ICLKEN_xxx, CM_CLKEN_UART3);
    while (INREG32(&pCmRegs->CM_IDLEST1_xxx) & CM_IDLEST_ST_UART3);
}

//------------------------------------------------------------------------------
// External Variables
extern DEVICE_IFC_GPIO Omap_Gpio;
extern DEVICE_IFC_GPIO Tps659xx_Gpio;
extern UINT32 g_ffContextSaveMask;

void BSPGpioInit()
{
   BSPInsertGpioDevice(0,&Omap_Gpio,NULL);
   BSPInsertGpioDevice(TRITON_GPIO_PINID_START,&Tps659xx_Gpio,NULL);
}

VOID MmUnmapIoSpace( 
  PVOID BaseAddress, 
  ULONG NumberOfBytes 
)
{
    UNREFERENCED_PARAMETER(BaseAddress);
    UNREFERENCED_PARAMETER(NumberOfBytes);
}

PVOID MmMapIoSpace( 
  PHYSICAL_ADDRESS PhysicalAddress, 
  ULONG NumberOfBytes, 
  BOOLEAN CacheEnable 
)
{
    UNREFERENCED_PARAMETER(NumberOfBytes);
    return OALPAtoVA(PhysicalAddress.LowPart,CacheEnable);
}
void
HalContextUpdateDirtyRegister(
                              UINT32 ffRegister
                              )
{
    g_ffContextSaveMask |= ffRegister;
}

void OALGPIOSetDefaultValues()
{
	HANDLE hGpio;
	hGpio = GPIOOpen();

	GPIOClrBit(hGpio,BSP_CB14_USB_HOST_ENn);
	GPIOSetMode(hGpio, BSP_CB14_USB_HOST_ENn,GPIO_DIR_OUTPUT);

	
	GPIOSetBit(hGpio,BSP_CB21_USB_HOST_ENn);
	GPIOSetMode(hGpio, BSP_CB21_USB_HOST_ENn,GPIO_DIR_OUTPUT);

	GPIOSetBit(hGpio,USB_TRANSCEIVER_RESET);	
	GPIOSetMode(hGpio, USB_TRANSCEIVER_RESET,GPIO_DIR_OUTPUT);
}
#ifndef BSP_SDRAM_512MB
DWORD OEMEnumExtensionDRAM(PMEMORY_SECTION pMemSections,DWORD cMemSections)
{
    DWORD cSections = 0;
    // If BANK1 is enabled, give it the OS
#if (!defined BSP_SDRAM_128MB)   
    if ((cSections < cMemSections) && (dwBank1Enabled == 1))
    {
        pMemSections[cSections].dwFlags = 0;
        pMemSections[cSections].dwStart = IMAGE_WINCE_RAM_BANK1_CA;
        pMemSections[cSections].dwLen = IMAGE_WINCE_RAM_BANK1_SIZE;
        cSections++;
    }
    if ((cSections < cMemSections) && (dwBank1Enabled == 1) && (dwDSP720pEnabled != 1))
    {
        pMemSections[cSections].dwFlags = 0;
        pMemSections[cSections].dwStart = IMAGE_DSP_720P_CA;
        pMemSections[cSections].dwLen = IMAGE_DSP_720P_SIZE;
        cSections++;
    }
    // If CMEM_DSP region is not used for the DSP, give it to the OS
    if ((cSections < cMemSections) && (dwCMemDSPEnabled != 1))
    {
        pMemSections[cSections].dwFlags = 0;
        pMemSections[cSections].dwStart = IMAGE_CMEM_CA;
        pMemSections[cSections].dwLen = IMAGE_CMEM_SIZE;
        cSections++;
    }
    if ((cSections < cMemSections) && (dwCMemDSPEnabled != 1))
    {
        pMemSections[cSections].dwFlags = 0;
        pMemSections[cSections].dwStart = IMAGE_DSP_CA;
        pMemSections[cSections].dwLen = IMAGE_DSP_SIZE;
        cSections++;
    }
#endif
    
    return cSections;
}
#endif
