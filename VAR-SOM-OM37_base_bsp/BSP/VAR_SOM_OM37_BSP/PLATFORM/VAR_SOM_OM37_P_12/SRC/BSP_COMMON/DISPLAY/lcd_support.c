// All rights reserved ADENEO EMBEDDED 2010
// Copyright (c) 2007, 2008 BSQUARE Corporation. All rights reserved.

#include "bsp.h"
#include "sdk_gpio.h"

#include <lcd_cfg.h>

static HANDLE g_hGpio = NULL;
static DWORD g_gpioLcdPower;
static DWORD g_gpioLcdIni;
static DWORD g_gpioLcdResB;


BOOL LcdInitGpio(void)
{
    // Configure Backlight/Power pins as outputs
    g_hGpio = GPIOOpen();


    GPIOSetBit(g_hGpio,BSP_LCD_BACKLIGHT_EN);
    GPIOSetMode(g_hGpio, BSP_LCD_BACKLIGHT_EN,GPIO_DIR_OUTPUT);



	GPIOClrBit(g_hGpio,BSP_HDMI_EN);
	GPIOSetMode(g_hGpio, BSP_HDMI_EN,GPIO_DIR_OUTPUT);




	GPIOClrBit(g_hGpio,BSP_LCD_POWER_ENn);
	GPIOSetMode(g_hGpio, BSP_LCD_POWER_ENn,GPIO_DIR_OUTPUT);





	return TRUE;
}

BOOL LcdDeinitGpio(void)
{
	// Close GPIO driver
    GPIOClose(g_hGpio);

    return TRUE;
}

void LcdPowerControl(BOOL bEnable)
{    

UNREFERENCED_PARAMETER(bEnable);
}

void LcdResBControl(BOOL bEnable)
{
	UNREFERENCED_PARAMETER(bEnable);

}

void LcdIniControl(BOOL bEnable)
{
	UNREFERENCED_PARAMETER(bEnable);

}

// Screen should be in rotated to lanscape mode, 640x480 for use with DVI
void LcdDviEnableControl(BOOL bEnable)
{
	UNREFERENCED_PARAMETER(bEnable);

}

/*
void LcdStall(DWORD dwMicroseconds)
{
    OALStall(dwMicroseconds);
}

void LcdSleep(DWORD dwMilliseconds)
{
    OALStall(1000 * dwMilliseconds);
}
*/