// All rights reserved ADENEO EMBEDDED 2010
/*
================================================================================
*             Texas Instruments OMAP(TM) Platform Software
* (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
*
================================================================================
*/
//
//  File: _debug.h
//

#ifndef ___DEBUG_H
#define ___DEBUG_H

#ifndef ZONE_ERROR
#define ZONE_ERROR 1
#endif
#endif //___DEBUG_H

