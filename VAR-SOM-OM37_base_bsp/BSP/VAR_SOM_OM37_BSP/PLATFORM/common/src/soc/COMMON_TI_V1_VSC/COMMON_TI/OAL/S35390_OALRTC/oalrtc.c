// All rights reserved ADENEO EMBEDDED 2010
// Copyright (c) 2007, 2008 BSQUARE Corporation. All rights reserved.

/*
================================================================================
*             Texas Instruments OMAP(TM) Platform Software
* (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
*
================================================================================
*/
//
//  File:  oalrtc.c
//
//  This file implements OAL real time module. 
//
//
#include "omap.h"
#include "oalex.h"
#include <nkintr.h>
#include "sdk_i2c.h"
#include "s35390_rtc.h"

/* Register map */
/* rtc section */
#define ISL12057_REG_SC  0x00
#define ISL12057_REG_MN  0x01
#define ISL12057_REG_HR  0x02
#define ISL12057_REG_HR_MIL     (1<<6)	/* 24h/12h mode */
#define ISL12057_REG_HR_PM      (1<<5)	/* PM/AM bit in 12h mode */
#define ISL12057_REG_DW  0x03
#define ISL12057_REG_DT  0x04
#define ISL12057_REG_MO  0x05
#define ISL12057_REG_YR  0x06
#define ISL12057_RTC_SECTION_LEN 7




/* alarm1 section */
#define ISL12057_REG_A1SC 0x07
#define ISL12057_REG_A1MN 0x08
#define ISL12057_REG_A1HR 0x09
#define ISL12057_REG_A1DT 0x0a
#define ISL12057_ALARM1_SECTION_LEN 4

/* alarm2 section */
#define ISL12057_REG_A2MN 0x0b
#define ISL12057_REG_A2HR 0x0c
#define ISL12057_REG_A2DT 0x0d
#define ISL12057_ALARM2_SECTION_LEN 3

/* control/status section */
#define ISL12057_REG_INT 0x0e
#define ISL12057_REG_SR  0x0f
//
//  Define:  RTC_BASE_YEAR
//
//  Delta from which RTC counts years
//  Resolution of RTC years is from 2000 to 2099
//
#define RTC_BASE_YEAR_MIN       2000    // must be divisible by 4
#define RTC_BASE_YEAR_MAX       2099


// I2C defines
#define RTC_I2C_DEV					OMAP_DEVICE_I2C1
#define I2C_ADDR_RTC				0x0030
#define I2C_BAUD_RTC				0

// Command defines
#define RTC_CMD_STATUS_1            (0x0)
#define RTC_CMD_STATUS_2            (0x1)
#define RTC_CMD_REAL_TIME_DATA_1    (0x2)
#define RTC_CMD_REAL_TIME_DATA_2    (0x3)
#define RTC_CMD_INT1                (0x4)
#define RTC_CMD_INT2                (0x5)
#define RTC_CMD_CLOCK_CORRECTION    (0x6)
#define RTC_CMD_FREE                (0x7)

// Bitmask defines for Status Register 1
#define RTC_RESET                   (0x80)
#define RTC_24_HOUR_FORMAT          (0x40)
#define RTC_BLD                     (0x02)
#define RTC_POC                     (0x01)

// Bitmask defines for Status Register 2
#define RTC_NO_INTERRUPTS           (0x00)
#define RTC_ALARM_INTERRUPT			(0x20)
#define RTC_TEST                    (0x01)

// Buffer offset defines for Real-Time Data 1
#define RTC_RTD1_YEAR               (0x0)
#define RTC_RTD1_MONTH              (0x1)
#define RTC_RTD1_DAY                (0x2)
#define RTC_RTD1_DAY_OF_WEEK        (0x3)
#define RTC_RTD1_HOUR               (0x4)
#define RTC_RTD1_MINUTE             (0x5)
#define RTC_RTD1_SECOND             (0x6)
#define RTC_AMPM_BIT                (1<<1)

// Buffer offset defines for Real-Time Data 2
#define RTC_RTD2_HOUR               (0x0)
#define RTC_RTD2_MINUTE             (0x1)
#define RTC_RTD2_SECOND             (0x2)

// Buffer offset defines for INT registers
#define RTC_INT_WEEK				(0x0)
#define RTC_INT_HOUR				(0x1)
#define RTC_INT_MINUTE				(0x2)

// Bitmask defines for INT registers
#define RTC_ALARM_ENABLE			(0x1)

// Bitmask defines for Clock Correction
#define RTC_NO_CLOCK_CORRECTION     (0x0)

// Base year
#define RTC_DEFAULT_BASE_YEAR		(2000)

// Max year
#define RTC_MAX_YEARS				(99)

// Global variables
static void *g_hI2C = NULL;
static UINT16 g_devAddr;
static BOOL g_NeedReinit;
static UINT16 g_baseYear;

static BYTE BCDtoBIN(BYTE bcd)
{    
    return (bcd >> 4) * 10 + (bcd & 0x0F);
}

static BYTE reverse(BYTE b)
{
    int i;
    BYTE j;
    BYTE result=0;

    for (i=0x1,j=0x80;i<0x100;i<<=1,j>>=1)
    {
        if (b & i)
        {
            result |= j;
        }
    }
    return result;
}
static BYTE ReversedBCDtoBIN(BYTE bcd)
{    
    BYTE reversed = reverse(bcd);
    return (reversed >> 4) * 10 + (reversed & 0x0F);
}
static BYTE BINtoReversedBCD(BYTE binary)
{
    BYTE unit,deci;
    deci = binary / 10;
    unit = binary - (deci * 10);    
    return reverse((deci << 4) | (unit));
}

BOOL RTCI2CWrite(UINT16 subAddr, const VOID *pBuffer, UINT32 size)
{
    I2CSetSlaveAddress(g_hI2C, g_devAddr + subAddr);
    return (I2CWrite(g_hI2C, 0, pBuffer, size) == size) ? TRUE : FALSE;
}
BOOL RTCI2CRead(UINT16 subAddr, VOID *pBuffer, UINT32 size)
{
    I2CSetSlaveAddress(g_hI2C, g_devAddr + subAddr);
    return (I2CRead(g_hI2C, 0, pBuffer, size) == size) ? TRUE : FALSE;
}
#if 0
BOOL RTC_GetTime(LPSYSTEMTIME time)
{
    UCHAR receiveBuffer[7];
	static SYSTEMTIME pST;
	static UINT8 keepms=1;

	if (g_hI2C == NULL)
		return FALSE;
    if (RTCI2CRead(ISL12057_REG_SC, receiveBuffer, 7) == FALSE)
    {
        OALMSG(OAL_ERROR, (L"RTC_GetTime(): Failed to read the date/time from the RTC.\r\n"));
		return FALSE;
	}

	time->wSecond = BCDtoBIN(receiveBuffer[ISL12057_REG_SC]);
	time->wMinute = BCDtoBIN(receiveBuffer[ISL12057_REG_MN]);

	/* HR field has a more complex interpretation */
	{
		const UCHAR _hr = receiveBuffer[ISL12057_REG_HR];
		if (_hr & ISL12057_REG_HR_MIL)	/* 24h format */
			time->wHour = BCDtoBIN(_hr & 0x3f);
		else {
			/* 12h format */
			time->wHour = BCDtoBIN(_hr & 0x1f);
			if (_hr & ISL12057_REG_HR_PM)	/* PM flag set */
				time->wHour += 12;
		}
	}

	time->wDay = BCDtoBIN(receiveBuffer[ISL12057_REG_DT]);
	time->wMonth = BCDtoBIN(receiveBuffer[ISL12057_REG_MO]);	/* rtc starts at 1 */
	time->wYear = BCDtoBIN(receiveBuffer[ISL12057_REG_YR]) + RTC_BASE_YEAR_MIN;
	time->wDayOfWeek = BCDtoBIN(receiveBuffer[ISL12057_REG_DW]);
	time->wMilliseconds = 0;

	if (pST.wSecond == time->wSecond && pST.wMinute == time->wMinute )
	{
		time->wMilliseconds = keepms++;
	}
	else
		keepms=1;

	pST = *time;

	OALMSG(1, (L"ms %d s %d, m %d	", time->wMilliseconds , time->wSecond , time->wMinute));
	return TRUE;
}
#else
BOOL RTC_GetTime(LPSYSTEMTIME time)
{
	static UINT32 i=0;


	time->wSecond =(WORD) (i++ % 60);
	time->wMinute = (WORD) i / 60;
	time->wHour = 0;
	time->wDay = 1;
	time->wMonth = 1;	/* rtc starts at 1 */
	time->wYear = 2011;
	time->wDayOfWeek = 0;
	time->wMilliseconds = 0;

	OALMSG(1, (L"s %d, m %d	", time->wSecond , time->wMinute));
	return TRUE;
}
#endif

BOOL RTC_SetTime(LPSYSTEMTIME time)
{
	OALMSG(1, (L"RTC_SetTime()\r\n"));


	return TRUE;
}



BOOL OALS35390RTCInit(OMAP_DEVICE i2cdev,DWORD i2cBaudIndex,UINT16 slaveAddress,BOOL fEnableAsWakeUpSource)
{    
    UCHAR status;
	OALMSG(1, (L"OALISL12057RTCInit+\r\n"));

    g_NeedReinit = FALSE;
	g_baseYear = RTC_BASE_YEAR_MIN;

    // Open I2C instance
    g_hI2C = I2COpen(i2cdev);
    if (g_hI2C == NULL)
    {
        return FALSE;
    }
    g_devAddr = slaveAddress;
    I2CSetSlaveAddress(g_hI2C, slaveAddress);
    // Set baud rate
    I2CSetBaudIndex(g_hI2C, i2cBaudIndex);	
    // Set sub address mode
	I2CSetSubAddressMode(g_hI2C, I2C_SUBADDRESS_MODE_0);

    // Get the BLD and POC flags that will tell if the circuit muste be reinitialized    
	// Read status register 1	
	if (RTCI2CRead(ISL12057_REG_SR, &status, 1) == FALSE)
    {
        OALMSG(1, (L"OALIoCtlHalInitRTC(): Failed to read status register 1 after reset.\r\n"));
        return FALSE;
    }


	OALMSG(1, (L"OALISL12057RTCInit-\r\n"));

    return TRUE;
}

//------------------------------------------------------------------------------
//
//  Function:  OALIoCtlHalInitRTC
//
//  This function is called by WinCE OS to initialize the time after boot. 
//  Input buffer contains SYSTEMTIME structure with default time value.
//
//
BOOL
OALIoCtlHalInitRTC(
    UINT32 code, 
    VOID *pInBuffer, 
    UINT32 inSize, 
    VOID *pOutBuffer, 
    UINT32 outSize, 
    UINT32 *pOutSize
    )
{
	SYSTEMTIME *pGivenTime = (LPSYSTEMTIME)pInBuffer;

	UNREFERENCED_PARAMETER(code);
	UNREFERENCED_PARAMETER(inSize);
	UNREFERENCED_PARAMETER(pOutBuffer);
	UNREFERENCED_PARAMETER(outSize);
	UNREFERENCED_PARAMETER(pOutSize);


	// Initialize the time if needed
	if (g_NeedReinit && pGivenTime != NULL)
	{
        g_NeedReinit = FALSE;

		RTC_SetTime(pGivenTime);
	}

	return TRUE;
}

//------------------------------------------------------------------------------
//
//  Function:  OEMGetRealTime
//
//  This function is called by the kernel to retrieve the time from
//  the real-time clock.
//
BOOL
OEMGetRealTime(
    SYSTEMTIME *pSystemTime
    ) 
{
    if (g_NeedReinit)
    {
        return FALSE;
    }
	return RTC_GetTime(pSystemTime);
}

//------------------------------------------------------------------------------
//
//  Function:  OEMSetRealTime
//
//  This function is called by the kernel to set the real-time clock. A secure
//  timer requirement means that the time change is noted in baseOffset and
//  used to compute the time delta from the non-alterable RTC in T2
//
BOOL
OEMSetRealTime(
    SYSTEMTIME *pSystemTime
    ) 
{
    BOOL fResult = RTC_SetTime(pSystemTime);

    if (fResult && g_NeedReinit)
    {
        g_NeedReinit = FALSE;
    }
	return fResult;
}

//------------------------------------------------------------------------------
//
//  Function:  OEMSetAlarmTime
//
//  This function is called by the kernel to set the real-time clock alarm.
//
BOOL
OEMSetAlarmTime(
    SYSTEMTIME *pSystemTime
    ) 
{
 
 

	return TRUE;
}
